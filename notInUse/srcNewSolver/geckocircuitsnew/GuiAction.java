/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author andy
 */
public class GuiAction implements MouseListener, MouseMotionListener {
    private final ElementInterface _element;

    private ElementMode _mode = ElementMode.NORMAL;

    public ElementMode getModus() {
        return _mode;
    }

    public GuiAction(ElementInterface element) {
        _element = element;
    }

    public void mouseClicked(MouseEvent e) {
//        switch(_mode) {
//            case NORMAL:
//                _mode = ElementMode.IN_EDIT;
//                break;
//            case IN_EDIT:
//                _mode = ElementMode.NORMAL;
//                break;
//            default:
//                assert false;
//        }
    }

    public void mousePressed(MouseEvent e) {
        _mode = ElementMode.IN_EDIT;
        _element.repaint();
    }

    public void mouseReleased(MouseEvent e) {
        _mode = ElementMode.NORMAL;
        _element.repaint();
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
                Point pt = e.getLocationOnScreen();
                pt.y -=50;
                _element.x.setValue((pt.x) / CircuitSheet._dpix.getValue() );
                _element.y.setValue((pt.y) / CircuitSheet._dpix.getValue() );
                _element.repaint();
    }

    public void mouseMoved(MouseEvent e) {
    }

    public static enum ElementMode {
        IN_EDIT,
        NORMAL
    };

    public void doTask(MouseEvent me) {

    }

}
