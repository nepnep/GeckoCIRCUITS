/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class ChessBoard extends JFrame implements MouseListener, MouseMotionListener
{
	JLayeredPane layeredPane;
	JPanel chessBoard;
	JLabel chessPiece;
	int xAdjustment;
	int yAdjustment;

	public ChessBoard()
	{
		Dimension boardSize = new Dimension(600, 600);

		//  Use a Layered Pane for this this application

		layeredPane = new JLayeredPane();
		getContentPane().add(layeredPane);
		layeredPane.setPreferredSize( boardSize );
		layeredPane.addMouseListener( this );
		layeredPane.addMouseMotionListener( this );

		//  Add a chess board to the Layered Pane

		chessBoard = new JPanel();
		layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
		chessBoard.setLayout( new GridLayout(8, 8) );
		chessBoard.setPreferredSize( boardSize );
		chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);

		for (int i = 0; i < 64; i++)
		{
			JPanel square = new JPanel( new BorderLayout() );
			chessBoard.add( square );

			int row = (i / 8) % 2;
			if (row == 0)
				square.setBackground( i % 2 == 0 ? Color.red : Color.white );
			else
				square.setBackground( i % 2 == 0 ? Color.white : Color.red );
		}

		// Add a few pieces to the board
                JLabel piece = new JLabel( new ImageIcon("/home/andy/dukewavered.gif") );
		JPanel panel = (JPanel)chessBoard.getComponent( 0 );
		panel.add( piece );
		piece = new JLabel( new ImageIcon("/home/andy/dukewavered.gif") );
		panel = (JPanel)chessBoard.getComponent( 15 );
		panel.add( piece );
	}

	/*
	**  Add the selected chess piece to the dragging layer so it can be moved
	*/
	public void mousePressed(MouseEvent e)
	{
		chessPiece = null;
		Component c =  chessBoard.findComponentAt(e.getX(), e.getY());

		if (c instanceof JPanel) return;

		Point parentLocation = c.getParent().getLocation();
		xAdjustment = parentLocation.x - e.getX();
		yAdjustment = parentLocation.y - e.getY();
		chessPiece = (JLabel)c;
		chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
		chessPiece.setSize(chessPiece.getWidth(), chessPiece.getHeight());
		layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);
	}

	/*
	**  Move the chess piece around
	*/
	public void mouseDragged(MouseEvent me)
	{
		if (chessPiece == null) return;

		chessPiece.setLocation(me.getX() + xAdjustment, me.getY() + yAdjustment);
	 }

	/*
	**  Drop the chess piece back onto the chess board
	*/
	public void mouseReleased(MouseEvent e)
	{
		if (chessPiece == null) return;

		chessPiece.setVisible(false);
		Component c =  chessBoard.findComponentAt(e.getX(), e.getY());

		if (c instanceof JLabel)
		{
			Container parent = c.getParent();
			parent.remove(0);
			parent.add( chessPiece );
		}
		else
		{
			Container parent = (Container)c;
			parent.add( chessPiece );
		}

		chessPiece.setVisible(true);
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

	public static void main(String[] args)
	{
		JFrame frame = new ChessBoard();
		frame.setDefaultCloseOperation( DISPOSE_ON_CLOSE );
		frame.pack();
		frame.setResizable( false );
		frame.setLocationRelativeTo( null );
		frame.setVisible(true);
	 }
}
