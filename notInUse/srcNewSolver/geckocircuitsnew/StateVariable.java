/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geckocircuitsnew;

import gecko.GeckoCIRCUITS.elements.*;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author anstupar
 */
public class StateVariable {
    
    String _varName;
    ElementInterface _modelElement;
    private double _stateVarValue;
    private int _stateVarIndex;
    
    //for control elements that share control variables
    private List<StateVariable> _dependentStateVariables = null;
    private boolean _hasDependentVariables = false;
    
    public StateVariable(ElementCapacitor cap) {
        _modelElement = cap;
        _varName = cap._elementName.getValue();
        _stateVarValue = cap.capacitor.getInitialValue();
    }
    
    public StateVariable(ElementInductor ind) {
        _modelElement = ind;
        _varName = ind._elementName.getValue();
        _stateVarValue = ind.inductor.getInitialValue();
    }
    
    public StateVariable(ElementControl contr, int stateVarIndex) {
        _modelElement = contr;
        _stateVarIndex = stateVarIndex;
        _varName = contr._elementName.getValue() + "_state" + stateVarIndex;
        _stateVarValue = contr.getStateVar(stateVarIndex);
    }
    
    public double getStateVarValue() {
        
        if (_modelElement instanceof ElementCapacitor) {
            _stateVarValue = ((ElementCapacitor) _modelElement).capacitor.getVoltage();
        }
        else if (_modelElement instanceof ElementInductor) {
            _stateVarValue = ((ElementInductor) _modelElement).inductor.getCurrent();
        }
        else if ((_modelElement instanceof ElementControl)) {
            _stateVarValue = ((ElementControl) _modelElement).getStateVar(_stateVarIndex);
        }
        
        return _stateVarValue;
    }
    
    public void setStateVarValue(double value) {
        _stateVarValue = value;
        
        if (_modelElement instanceof ElementCapacitor) {
            ((ElementCapacitor) _modelElement).initialValue.setValue(_stateVarValue);
        }
        else if (_modelElement instanceof ElementInductor) {
            ((ElementInductor) _modelElement).initCurrent.setValue(_stateVarValue);
        }
        else if ((_modelElement instanceof ElementControl)) {
            ((ElementControl) _modelElement).setStateVar(_stateVarValue,_stateVarIndex);
        }
        
        if (_hasDependentVariables) {
            for (StateVariable depVar : _dependentStateVariables) {
                depVar.setStateVarValue(value);
            }
        }
        
        
    }
    
    public double getMaxAbsVal() {
        
        if (_modelElement instanceof ElementCapacitor) {
            return ((ElementCapacitor) _modelElement).capacitor.getMaxAbsVal();
        }
        else if (_modelElement instanceof ElementInductor) {
            return ((ElementInductor) _modelElement).inductor.getMaxAbsVal();
        }
        else if ((_modelElement instanceof ElementControl)) {
            return ((ElementControl) _modelElement).getStateVarMaxAbs(_stateVarIndex);
        }
        
        return 0;
    }
    
    public void addDependentStateVariable(StateVariable stateVar) {
        
        if (_dependentStateVariables == null) {
            _dependentStateVariables = new ArrayList<StateVariable>();
            _hasDependentVariables = true;
        }
        
        _dependentStateVariables.add(stateVar);
        
    }
            
    
}
