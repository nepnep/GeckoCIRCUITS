/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geckocircuitsnew;

import java.awt.Point;

public enum Orientation {

    NORTH(501),
    EAST(502),
    SOUTH(503),
    WEST(504);
    private int _number = 501;

    Orientation(int number) {
        _number = number;
    }

    /**
     *
     * @param number: ordinal value, made for backwards compatibility
     * @return
     */
    public static Orientation getOri(int number) {
        switch (number) {
            case 0:
                return NORTH;
            case 1:
                return EAST;
            case 2:
                return SOUTH;
            case 3:
                return WEST;
            case 501:
                return NORTH;
            case 502:
                return EAST;
            case 503:
                return SOUTH;
            case 504:
                return WEST;
            case -1:
                return SOUTH;
            default:
                assert false : number;
                return NORTH;
        }
    }

    public Orientation nextRotation() {
        switch (this) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            default:
                assert false;
                return NORTH;
        }

    }

    /**
     *
     * @return rotation angle in Radians
     */
    public double getRotationAngle() {
        switch (this) {
            case NORTH:
                return 1.5 * Math.PI - Math.PI / 2;
            case SOUTH:
                return 0.5 * Math.PI - Math.PI / 2;
            case EAST:
                return -Math.PI / 2;
            case WEST:
                return Math.PI - Math.PI / 2;
            default:
                assert false;
        }
        return 0;
    }

    public static Orientation getDirection(final Point start, final Point end) {
        if (start.x == end.x) {
            if (start.y > end.y) {
                return NORTH;
            } else {
                return SOUTH;
            }

        } else {
            if (start.x > end.x) {
                return WEST;
            } else {
                return EAST;
            }
        }
    }

    public boolean isHorizontal() {
        return this == WEST || this == EAST;
    }
}