/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew;

import gecko.GeckoCIRCUITS.allg.Typ;
import java.io.File;
import java.util.StringTokenizer;

/**
 *
 * @author andy
 */
public class DatenSpeicher {

    /**
     * reads a textblock, including spaces. In contradiction, LeseAsciiString would
     * return only the first token! If a \n character appears, a newline is done.
     * @param ascii
     * @return
     */
    public static String leseASCII_TextBlock(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        String identifier = stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        String wert = ascii;
        if (stk.hasMoreElements()) {
            // remove first token, the rest is the String to read in.
            wert = ascii.substring(identifier.length() + 1);
            wert = wert.replaceAll("\\\\n", "\n");
        }

        return wert;
    }

    public static String[] makeStringArray(String in) {
        //------------------
        int[] zeilenUmbruchZeiger = new int[in.length()];
        int zeilenAnzahl = 0;
        for (int i1 = 0; i1 < in.length() - 1; i1++) {
            if (in.substring(i1).startsWith("\n")) {
                zeilenUmbruchZeiger[zeilenAnzahl] = i1;
                zeilenAnzahl++;
            }
        }
        String[] out = new String[zeilenAnzahl];
        for (int i1 = 0; i1 < zeilenAnzahl - 1; i1++) {
            out[i1] = in.substring(zeilenUmbruchZeiger[i1] + 1, zeilenUmbruchZeiger[i1 + 1]);
            
        }
        out[zeilenAnzahl - 1] = in.substring(zeilenUmbruchZeiger[zeilenAnzahl - 1] + 1);
        //------------------
        return out;
    }


    // aus dem ASCII-String werden die Daten zurueckgelesen -->
    public static boolean leseASCII_boolean(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Boolean(stk.nextToken()).booleanValue());
    }

    public static int leseASCII_int(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Integer(stk.nextToken()).intValue());
    }

    public static double leseASCII_double(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Double(stk.nextToken()).doubleValue());
    }

    public static String leseASCII_String(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        String wert = stk.nextToken();
        return wert;
    }

    public static boolean[] leseASCII_booleanArray1(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        boolean[] wert = new boolean[stk.countTokens() - 1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1 = 0; i1 < wert.length; i1++) {
            String zz = stk.nextToken();
            if ((i1 == 0) && (zz.equals("null"))) {
                return null;
            }
            wert[i1] = (new Boolean(zz)).booleanValue();
        }
        return wert;
    }

    public static int[] leseASCII_intArray1(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        int[] wert = new int[stk.countTokens() - 1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1 = 0; i1 < wert.length; i1++) {
            String zz = stk.nextToken();
            if ((i1 == 0) && (zz.equals("null"))) {
                return null;
            }
            wert[i1] = (new Integer(zz)).intValue();
        }
        return wert;
    }

    public static double[] leseASCII_doubleArray1(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        double[] wert = new double[stk.countTokens() - 1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1 = 0; i1 < wert.length; i1++) {
            String zz = stk.nextToken();
            if ((i1 == 0) && (zz.equals("null"))) {
                return null;
            }
            wert[i1] = (new Double(zz)).doubleValue();
        }
        return wert;
    }

    public static String[] leseASCII_StringArray1(String ascii) {
        int erstesPlenk = ascii.indexOf(" ");
        String asciiDaten = ascii.substring(ascii.indexOf(" "));
        StringTokenizer stk = new StringTokenizer(asciiDaten, Typ.SEPARATOR_ASCII_STRINGARRAY);
        stk.nextToken();  // erster Wert wird uebersprungen
        String[] wert = new String[stk.countTokens()];
        for (int i1 = 0; i1 < wert.length; i1++) {
            wert[i1] = stk.nextToken();
            if (wert[i1].equals(Typ.NIX)) {
                wert[i1] = "";
            }
        }
        return wert;
    }

    public static double[][] leseASCII_doubleArray2(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l = (new Integer(stk.nextToken())).intValue();
        int l0 = (new Integer(stk.nextToken())).intValue();
        double[][] wert = new double[l][l0];
        for (int i1 = 0; i1 < l; i1++) {
            for (int i2 = 0; i2 < l0; i2++) {
                wert[i1][i2] = (new Double(stk.nextToken())).doubleValue();
            }
        }
        return wert;
    }

    public static int[][] leseASCII_intArray2(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l = (new Integer(stk.nextToken())).intValue();
        int l0 = (new Integer(stk.nextToken())).intValue();
        int[][] wert = new int[l][l0];
        for (int i1 = 0; i1 < l; i1++) {
            for (int i2 = 0; i2 < l0; i2++) {
                wert[i1][i2] = (new Integer(stk.nextToken())).intValue();
            }
        }
        return wert;
    }

    public static boolean[][] leseASCII_booleanArray2(String ascii) {
        StringTokenizer stk = new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l = (new Integer(stk.nextToken())).intValue();
        int l0 = (new Integer(stk.nextToken())).intValue();
        boolean[][] wert = new boolean[l][l0];
        for (int i1 = 0; i1 < l; i1++) {
            for (int i2 = 0; i2 < l0; i2++) {
                wert[i1][i2] = (new Boolean(stk.nextToken())).booleanValue();
            }
        }
        return wert;
    }

// ************************************************************************************
    // Hilfsfunktionen:

    // Aenderungen in relativen und absoluten Pfaden werden beruecksichtigt -->


    // datnamAbsolutIPES   ... neuer aktueller absoluter pfad+name der *.ipes-Datei, die die Topologie/Schaltungssimulation enthaelt
    // datnamAbsLoadIPES   ... gespeicherter pfad+name der *.ipes-Datei, die die Topologie/Schaltungssimulation enthaelt
    // datnamAbsLoadDETAIL ... gespeicherter pfad+name einer Datei, die Zusatzinfo enhaelt, zB. Halbleiter-Info oder Ersatzmodelle wie TH_MODUL
    // return-String       ... neuer aktueller absoluter pfad+name der Datei mit den Zusatzinfos
    //
    public static String lokalisiereRelativenPfad (String datnamAbsolutIPES, String datnamAbsLoadDETAIL) {
        String datnamAbsLoadIPES= Typ.datnamAbsLoadIPES;
        //-------------------------
        // (1) Ist die Pfadstruktur unveraendert? Kann man den alten (gespeicherten) absoluten Pfad der Zusatzdatei verwenden?
        if ((new File(datnamAbsLoadDETAIL)).exists()) return datnamAbsLoadDETAIL;
        //-------------------------
        // (2) Die alte Datei 'datnamAbsLoadDETAIL' wurde nicht gefunden.
        // Hat sich die Pfadstruktur geaendert und muss der neue Pfad gefunden werden?
        // (a) Durchsuche die Unterverzeichnisses von *.ipes -->
        try {
            String localRootAlt= (new File(datnamAbsLoadIPES)).getParent();
            String localRootNeu= (new File(datnamAbsolutIPES)).getParent();
            String relativerPfadDETAIL= datnamAbsLoadDETAIL.substring(localRootAlt.length());
            String neuerPfadDETAIL= localRootNeu+relativerPfadDETAIL;
            if ((new File(neuerPfadDETAIL)).exists()) return neuerPfadDETAIL;
        } catch (Exception e) {}
        //---------
        // (b) Durchsuche systematisch die Festplatte, ob es eventuell irgendwo die Zusatzinfo-Datei gibt -->
        // ... noch zu implementieren ...
        //-------------------------
        return new String(Typ.DATNAM_NOT_DEFINED);
    }

}
