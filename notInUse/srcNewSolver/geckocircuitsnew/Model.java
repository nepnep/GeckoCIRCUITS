/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geckocircuitsnew;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.elements.ElementConnection;
import Utilities.ModelMVC.ModelMVC;
import gecko.geckocircuitsnew.animation.CurrentTopologyContainer;
import gecko.GeckoCIRCUITS.elements.ElementCircuit;
import gecko.GeckoCIRCUITS.elements.ElementConnectionCircuit;
import gecko.GeckoCIRCUITS.elements.ElementConnectionControl;
import gecko.GeckoCIRCUITS.elements.ElementControl;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.elements.ElementTransformer;
import gecko.GeckoCIRCUITS.elements.SubSheetable;
import gecko.geckocircuitsnew.circuit.MatrixSolver.SolverType;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
public class Model {

    public final ModelMVC<String> filename = new ModelMVC<String>("");
    public CircuitSheet sheet;// = new CircuitSheet(1000, 1000);
    
    public final ModelMVC<Double> dt = new ModelMVC<Double>(1e-7);
    public final static ModelMVC<Double> tEnd = new ModelMVC<Double>(1e-3);
    public final ModelMVC<Double> tBR = new ModelMVC<Double>(-1.0);

    //add fields for adaptive step-width feature - min. timestep, max. timestep
    public final ModelMVC<Double> dt_min = new ModelMVC<Double>(1e-7);
    public final ModelMVC<Double> dt_max = new ModelMVC<Double>(1e-3);
    public final ModelMVC<Boolean> dtvar = new ModelMVC<Boolean>(false);
    public final ModelMVC<Boolean> synched = new ModelMVC<Boolean>(true);
    public final ModelMVC<Boolean> minAfterSync = new ModelMVC<Boolean>(false);
    public final ModelMVC<Integer> attempts = new ModelMVC<Integer>(10);
    public final ModelMVC<Double> LTEmax = new ModelMVC<Double>(10e-12);
    public final ModelMVC<Boolean> checkControlLTE = new ModelMVC<Boolean>(true);

    
    
    //type of solver => by default, backwards Euler
    public final ModelMVC<SolverType> solverType = new ModelMVC<SolverType>(SolverType.BE);

    public boolean saveHistory = false;

    public boolean testStep = false; //set to true when trying a new step width

    public ArrayList<ElementInterface> modelElements = new ArrayList<ElementInterface>(); //list containing all the elements in the model (Andrija)

    public CurrentTopologyContainer _TopologyContainer;
    
    public Model(String filename) {
        sheet = new CircuitSheet(1000, 1000, this);
        if (filename != null) {
            File file = new File(filename);
            openFile(file); 
        }
        dtvar.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                saveHistory = dtvar.getValue();
            }
    });
        
       /*for(ElementInterface elem: ElementInterface.allElements) {
         if(elem.getSheet() == null || elem.getSheet() == sheet) {
                sheet.add(elem);
            }

        }*/

//        final ElementTransformer trafo = new ElementTransformer(sheet);
//
//        trafo.addMouseListener(new MouseListener() {
//
//                        public void mouseClicked(MouseEvent e) {
//                            if(e.getClickCount() == 2) {
//                                CircuitSheet sub = ((SubSheetable) trafo).getSubSheet();
//                                CircuitSheet.setActiveSheet(sub);
//
//                            }
//                        }
//
//                        public void mousePressed(MouseEvent e) {
//
//                        }
//
//                        public void mouseReleased(MouseEvent e) {
//                        }
//
//                        public void mouseEntered(MouseEvent e) {
//                        }
//
//                        public void mouseExited(MouseEvent e) {
//                        }
//                    });
//
//
//        trafo.x.setValue(20);
//        trafo.y.setValue(50);
//        sheet.add(trafo);
        
        _TopologyContainer = new CurrentTopologyContainer();
        
    }

    private void openFile(File file) {
        GZIPInputStream in1 = null;
        try {
            in1 = new GZIPInputStream(new FileInputStream(file));
            BufferedReader in = new BufferedReader(new InputStreamReader(in1));
            Vector datVec = new Vector();
            String z = null;
            while ((z = in.readLine()) != null) {
                
                datVec.addElement(z);
            }
            in.close();
            String[] zeile = new String[datVec.size()];
            for (int i1 = 0; i1 < datVec.size(); i1++) {
                zeile[i1] = (String) datVec.elementAt(i1);                
            }
            
            importASCII(zeile);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in1.close();
            } catch (IOException ex) {
                Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        }


    void importASCII(String[] ascii) {
        // pro Textzeile in der ASCII-Datei gibt es einen String
        //------------------
        String zz = "";
        StringTokenizer stk = null;
        for (int i1 = 0; i1 < ascii.length; i1++) {
            //System.out.println(i1+"   "+ascii[i1]);
            if (ascii[i1].startsWith("verbindungLeistungskreisANZAHL ")) {
            }
            if (ascii[i1].startsWith("staticZaehlerReglerBlockINIT[] ")) {
//                zz = ascii[i1].substring("staticZaehlerReglerBlockINIT[]".length()).trim();
//                stk = new StringTokenizer(zz, " ");
//                staticZaehlerReglerBlockINIT = new int[stk.countTokens()];
//                for (int i2 = 0; i2 < staticZaehlerReglerBlockINIT.length; i2++) {
//                    staticZaehlerReglerBlockINIT[i2] = (new Integer(stk.nextToken())).intValue();
//                }
            }
            if (ascii[i1].startsWith("staticZaehlerThermBlockINIT[] ")) {
//                zz = ascii[i1].substring("staticZaehlerThermBlockINIT[]".length()).trim();
//                stk = new StringTokenizer(zz, " ");
//                staticZaehlerThermBlockINIT = new int[stk.countTokens()];
//                for (int i2 = 0; i2 < staticZaehlerThermBlockINIT.length; i2++) {
//                    staticZaehlerThermBlockINIT[i2] = (new Integer(stk.nextToken())).intValue();
//                }
            }
//        DatenSpeicher.appendAsString(asc.append("\noptimizerName"), optimizerParameterData.getNameOpt());
//        DatenSpeicher.appendAsString(asc.append("\noptimizerValue"), optimizerParameterData.getValueOpt());
            //
            if (ascii[i1].startsWith("DtStor ")) {
//                String datGesp = DatenSpeicher.leseASCII_String(ascii[i1]);
//                Date datumGespeichert = null;
//                try {
//                    datumGespeichert = dFormat.parse(datGesp);
//                    long zeitZweiTageVorher = datumGespeichert.getTime() - (3600 * 1000 * 24 * 2);
//                    Date datumZweiTageVorher = new Date(zeitZweiTageVorher);
//                    Date jetzt = new Date();
//                    if (jetzt.before(datumZweiTageVorher)) {
//                        zeitlizenzOK = false;  // Fehler bei der Lizenz!
//                    }                    //System.out.println("datumGespeichert= "+datumGespeichert+"\tdatumZweiTageVorher= "+datumZweiTageVorher+"\tjetzt= "+jetzt);
//                } catch (ParseException pe) {
//                }
            }
            if (ascii[i1].startsWith("tDURATION ")) {
                tEnd.setValue(DatenSpeicher.leseASCII_double(ascii[i1]));
            }
            if (ascii[i1].startsWith("dt ")) {
                dt.setValue(DatenSpeicher.leseASCII_double(ascii[i1]));
            }
            if (ascii[i1].startsWith("dtMIN ")) {
                dt_min.setValue(DatenSpeicher.leseASCII_double(ascii[i1]));
            }
            if (ascii[i1].startsWith("dtMAX ")) {
                dt_max.setValue(DatenSpeicher.leseASCII_double(ascii[i1]));
            }
            if (ascii[i1].startsWith("varstep ")) { //flag for whether to use variable step width
                dtvar.setValue(DatenSpeicher.leseASCII_boolean(ascii[i1]));
            }
            if (ascii[i1].startsWith("tPAUSE ")) {
//                tPAUSE = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("Tss ")) {
//                Tss = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("maxIterationsNewton ")) {
//                maxIterationsNewton = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("maxErrorNewton ")) {
//                maxErrorNewton = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("path ")) {
//                Typ.datnamAbsLoadIPES = ascii[i1].substring((new String("path ").length()));  // wichtig, weil Pfadname Leerzeichen enthalten kann
            }
            //
            if (ascii[i1].startsWith("dpix ")) {
//                dpix = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("fontSize ")) {
//                fontSize = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("fontTyp ")) {
                //fontTyp = asci/i[i1].substring((new String("fontTyp ")).length());  // wichtig, falls FontName Leerzeichen enthaelt!
            }
            if (ascii[i1].startsWith("fensterWidth ")) {
                //fensterWidth = /DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("fensterHeight ")) {
                //fensterHeight = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("worksheetSize ")) {
                //worksheetSize = DatenSpeicher.leseASCII_String(ascii[i1]);
            }
            //
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_NAME ")) {
                //SchematischeEingabe2.ANSICHT_SHOW_LK_NAME = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_PARAMETER ")) {
                //SchematischeEingabe2.ANSICHT_SHOW_LK_PARAMETER = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_FLOWDIR ")) {
//                SchematischeEingabe2.ANSICHT_SHOW_LK_FLOWDIR = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_TEXTLINIE ")) {
  //              SchematischeEingabe2.ANSICHT_SHOW_LK_TEXTLINIE = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_NAME ")) {
    //            SchematischeEingabe2.ANSICHT_SHOW_THERM_NAME = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_PARAMETER ")) {
      //          SchematischeEingabe2.ANSICHT_SHOW_THERM_PARAMETER = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_FLOWDIR ")) {
 //               SchematischeEingabe2.ANSICHT_SHOW_THERM_FLOWDIR = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_TEXTLINIE ")) {
   //             SchematischeEingabe2.ANSICHT_SHOW_THERM_TEXTLINIE = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_NAME ")) {
     //           SchematischeEingabe2.ANSICHT_SHOW_CONTROL_NAME = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_PARAMETER ")) {
       //         SchematischeEingabe2.ANSICHT_SHOW_CONTROL_PARAMETER = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_TEXTLINIE ")) {
         //       SchematischeEingabe2.ANSICHT_SHOW_CONTROL_TEXTLINIE = DatenSpeicher.leseASCII_boolean(ascii[i1]);
            }
        }
        //------------------

        //------------------
//        e = new ArrayList<ElementLK>();

        //------------------
        //
        int[] zeilenZeiger = new int[1];
        String txt = "";
        //
        while (zeilenZeiger[0] < ascii.length) {
            //------------------------
            String elementName = ascii[zeilenZeiger[0]];
            ElementConnection.fabric(elementName, ascii, zeilenZeiger, sheet);            
            if (ascii[zeilenZeiger[0]].startsWith("e ")) {
                zeilenZeiger[0] += 2;
                txt = "";
                while (!ascii[zeilenZeiger[0]].startsWith("<\\ElementLK>")) {
                    txt += "\n" + ascii[zeilenZeiger[0]];
                    zeilenZeiger[0]++;
                }
                final ElementInterface newElement = ElementCircuit.fabric(txt, sheet);
                if(newElement instanceof SubSheetable) {
                    newElement.addMouseListener(new MouseListener() {

                        public void mouseClicked(MouseEvent e) {
                            System.out.println("clicked");
                            if(e.getClickCount() == 2) {
                                CircuitSheet sub = ((SubSheetable) newElement).getSubSheet();
                                CircuitSheet.setActiveSheet(sub);

                            }
                        }

                        public void mousePressed(MouseEvent e) {

                        }

                        public void mouseReleased(MouseEvent e) {
                        }

                        public void mouseEntered(MouseEvent e) {
                        }

                        public void mouseExited(MouseEvent e) {
                        }
                    });
                }

            }
            //------------------------
            if (ascii[zeilenZeiger[0]].startsWith("c ")) {
                zeilenZeiger[0] += 2;
                txt = "";
                while (!ascii[zeilenZeiger[0]].startsWith("<\\ElementCONTROL>")) {
                    txt += "\n" + ascii[zeilenZeiger[0]];
                    zeilenZeiger[0]++;
                }

                ElementInterface newElement = ElementControl.fabric(txt, sheet);

      //          controlElements.add(new ElementCONTROL(txt));
            }
            //------------------------
            if (ascii[zeilenZeiger[0]].startsWith("eTH ")) {
                zeilenZeiger[0] += 2;
                txt = "";
                while (!ascii[zeilenZeiger[0]].startsWith("<\\ElementTHERM>")) {
                    txt += "\n" + ascii[zeilenZeiger[0]];
                    zeilenZeiger[0]++;
                }
                final ElementInterface newElement = ElementCircuit.fabric(txt, sheet);
            }
            //------------------------
            /*
            if (ascii[zeilenZeiger].startsWith("<GeckoOPTIMIZER>")) {
            zeilenZeiger += 1;
            geckoOpt_code_ascii= "";
            while (! ascii[zeilenZeiger].startsWith("<\\GeckoOPTIMIZER>")) {
            geckoOpt_code_ascii += "\n"+ascii[zeilenZeiger];
            zeilenZeiger++;
            }
            }
             */
            //------------------------
            zeilenZeiger[0]++;
        }
    }
}
