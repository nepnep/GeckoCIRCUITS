/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geckocircuitsnew;

import gecko.GeckoCIRCUITS.allg.GetJarPath;
import gecko.GeckoCIRCUITS.allg.Typ;
//import gecko.math.matrix.DoubleMatrix;
import java.net.URL;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author andy
 */
public class Main {

    private static GetJarPath gjp;
    public static URL urlApplet;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        gjp = new GetJarPath(Main.class);
        Typ.PFAD_JAR_HOME = GetJarPath.getJarPath();
        Typ.PFAD_PICS_URL = gjp.getPathPICS();


//        try {
//            // Set cross-platform Java L&F (also called "Metal")
//            /*
//             * for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) { System.out.println(info.getName()); if
//             * ("Nimbus".equals(info.getName())) { UIManager.setLookAndFeel(info.getClassName()); break; } }
//             */
//            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
//        } catch (UnsupportedLookAndFeelException e) {
//            // handle exception
//        } catch (ClassNotFoundException e) {
//            // handle exception
//        } catch (InstantiationException e) {
//            // handle exception
//        } catch (IllegalAccessException e) {
//            // handle exception
//        }
        //String fileName = "Z:/Damping.ipes";
        //String fileName = "Z:/optimization/optimizertest/buck.ipes";
        //String fileName = "Z:/optimization/optimizertest/something.ipes";
        //String fileName = "Z:/variablestep_test/buck.ipes";
        //String fileName = "Z:/variablestep_test/signal.ipes";
        //String fileName = "Z:/variablestep_test/signalsawtooth.ipes";
        //String fileName = "Z:/variablestep_test/signalrect.ipes";
        //String fileName = "Z:/variablestep_test/signalnoscope.ipes";
        //String fileName = "Z:/variablestep_test/signalswitch.ipes";
        //String fileName = "Z:/variablestep_test/signalswitchnoscope.ipes";
        //String fileName = "Z:/variablestep_test/varcap.ipes";
        //String fileName = "Z:/variablestep_test/PFC1_nostaticvarjavablock.ipes";
        //String fileName = "Z:/variablestep_test/PFC1_staticcap.ipes";
        //String fileName = "Z:/variablestep_test/buckloop.ipes";
        //String fileName = "/home/andy/GeckoCIRCUITS/Examples/Education_ETHZ/ex_1.ipes";
        //String fileName = "Z:/GeckoCIRCUITS/examples_education_ETHZ/ex_2.ipes";
        //String fileName = "Z:/variablestep_test/examplepfc.ipes";
        //String fileName = "Z:/steady state/boost_noresistor.ipes";
        //String fileName = "Z:/variablestep_test/LC.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_SEPIC_CPM_CCM_notuning.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_Buck_CPM_CCM.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_Buck_PWM_CCM_lowovershoot.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_Buck_PWM_CCM_someovershoot.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_Buck_PWM_CCM_moderateovershoot.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_Buck_PWM_CCM_largeovershoot.ipes";
        //String fileName = "Z:/steady state/case study examples/DCDC_SEPIC_CPM_CCM_slowcontrol.ipes";
        // String fileName = "Z:/steady state/case study examples/ACDC_PFCBoost.ipes";
        //String fileName = "/home/zimmi/Desktop/GeckoCIRCUITS/Examples/education_www.ipes.ethz.ch/buckboost_simple.ipes";
        //String fileName = "/home/zimmi/Desktop/GeckoCIRCUITS/Examples/Topologies/BuckBoost_const_dutyCycle.ipes";
        //String fileName = "Z:/staff/anstupar/GeckoCIRCUITS/examples_education_ETHZ/LESI_R2.ipes";
        //String fileName = "/home/zimmi/Desktop/GeckoCIRCUITS/Examples/Topologies/three-phase_ViennaRectifier_simpleControl_250kW.ipes";
        //String fileName = "/home/andreas/GeckoCIRCUITS/Examples/Topologies/BuckBoost_const_dutyCycle.ipes";

        //String fileName = "/home/andreas/debug.ipes";
        //String fileName = "/home/andreas/GeckoCIRCUITS/Examples/Topologies/three-phase_ACAC_sparsematrixConverter_junction_temperature.ipes";
        //String fileName = "/home/andreas/GeckoCIRCUITS/Examples/Topologies/ViennaRectifier1_10kW_thermal.ipes";

//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/Topologies/BuckBoost_const_dutyCycle.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/Topologies/three-phase_ViennaRectifier_simpleControl_250kW.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/Topologies/debugVienna.ipes";
        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/Topologies/three-phase_ACAC_sparsematrixConverter_junction_temperature.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/Topologies/dq_control_PMSM.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/debuging/debug.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/debuging/debug2.ipes";
//        String fileName = "C:/Users/Zimmi/Desktop/GeckoCode/GeckoCIRCUITS/Examples/debuging/debug3.ipes";

        if (args.length == 1) {
            fileName = args[0];
        }

        MainFrame mf = new MainFrame(fileName);
        mf.setVisible(true);

        //mf.listModelElements();
        //mf.iterateSimTest();
        //mf.optimizeBuck();

        //mf.differentTimeStepTest();
        calculateMachineEpsilonDouble();

        /*
         * double[][] testmatrix = new double[3][3]; testmatrix[0][0] = 1; testmatrix[0][1] = 2; testmatrix[0][2] = 3;
         * testmatrix[1][0] = 2; testmatrix[1][1] = 5; testmatrix[1][2] = 3; testmatrix[2][0] = 1; testmatrix[2][1] = 0;
         * testmatrix[2][2] = 8; System.out.println("Input matrix:"); for (int i = 0; i < 3; i++) { for (int j = 0; j < 3; j++)
         * System.out.print(testmatrix[i][j] + " "); System.out.print("\n"); }
         *
         * double[][] inverse = DoubleMatrix.inverse(testmatrix); System.out.println("Inverse matrix:"); for (int i = 0; i < 3;
         * i++) { for (int j = 0; j < 3; j++) System.out.print(inverse[i][j] + " "); System.out.print("\n"); }
         */
    }

    private static void calculateMachineEpsilonDouble() {
        double machEps = 1.0d;

        do {
            machEps /= 2.0d;
        } while ((double) (1.0 + (machEps / 2.0)) != 1.0);

        System.out.println("Calculated machine epsilon: " + machEps);
    }
}
