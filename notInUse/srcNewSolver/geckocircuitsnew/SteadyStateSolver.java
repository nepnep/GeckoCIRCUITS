/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew;

import geckocircuitsnew.GeckoSCRIPT.SimulationAccess;
import gecko.GeckoCIRCUITS.elements.*;
import geckocircuitsnew.Model;
import java.util.List;
import java.util.ArrayList;
import gecko.geckocircuitsnew.circuit.Capacitor;
import gecko.geckocircuitsnew.circuit.Inductor;
import gecko.math.matrix.DoubleMatrix;
import gecko.math.matrix.Matrix;
import gecko.math.matrix.QRDecomposition;
import gecko.geckocircuitsnew.circuit.SwitchStateRecord;
import gecko.geckocircuitsnew.circuit.SwitchState;
/**
 *
 * @author anstupar
 */
public class SteadyStateSolver {

    private SimulationAccess _sim;
    private Model _circuitModel;

    private double[] _periods;
    private List<ElementCapacitor> _capacitors;
    private List<ElementInductor> _inductors;
    private List<ElementControl> _controlElements; //control elements, but only those with internal states
    
    private List<StateVariable> _stateVars;
    
    //private double _dampingFactor = 0;

    public SteadyStateSolver(Model circuitModel)
    {
        _sim = new SimulationAccess(circuitModel,false);
        _circuitModel = circuitModel;
    }

    public void initSteadyStateMonitoring()
    {
        double[] frequencies = _sim.getFrequencies();
        _periods = new double[frequencies.length];

        for (int i = 0; i < frequencies.length; i++)
            _periods[i] = 1.0 / frequencies[i];

        _capacitors = _sim.getCapacitors();
        _inductors = _sim.getInductors();
        _controlElements = new ArrayList<ElementControl>();
        ArrayList<ElementControl> allControlBlocks = _sim.getControlElements();
        
        for (ElementControl elem : allControlBlocks) {
            if (elem.hasInternalStates()) {
                _controlElements.add(elem);
            }
        }
    }
    
    //construct state variables list
    private void constructStateVariableVector() {
        
        _stateVars = new ArrayList<StateVariable>();
        int noOfControlStates;
        
        
        for (ElementCapacitor cap : _capacitors) {
            _stateVars.add(new StateVariable(cap));
        }
        
        for (ElementInductor ind : _inductors) {
            _stateVars.add(new StateVariable(ind));
        }
        
        for (ElementControl contr : _controlElements) {
            noOfControlStates = contr.getState().length;
            
            for (int i = 0; i < noOfControlStates; i++) {
                _stateVars.add(new StateVariable(contr,i));
            }
        }
    }
    
    //get state variable vector
    private double[] getStateVector() {
        double[] stateVector = new double[_stateVars.size()];
        
        int i = 0;
        for (StateVariable stVar : _stateVars) {
            System.out.print(i+": " + stVar._varName + " ");
            stateVector[i] = stVar.getStateVarValue();
            i++;
        }
        System.out.print("\n");
        
        return stateVector;
    }
    
  
    private void setStateVector(double[] vector) {
        
        StateVariable var;
        
        for (int i = 0; i < vector.length; i++) {
            var = _stateVars.get(i);
            var.setStateVarValue(vector[i]);
        }
        
    }
    
    private double getDeltaForVar(int index, double etaError, double deltaBound) {
        
        double maxAbsVal = (_stateVars.get(index)).getMaxAbsVal();
        
        double delta = Math.sqrt(etaError)*Math.max(maxAbsVal,deltaBound);
        
        return delta;
    }
    
    //checks if any state vectors are equal to one another (non-zero), and then reduces the total number of state vectors
    private double[] checkForRedundantVars(double[] originalStateVector) {
        
        double[] newStateVector;
        List<StateVariable> newStateVarList = new ArrayList<StateVariable>();
        List<Double> newStateVectorList = new ArrayList<Double>();
        double[] simResult = getStateVector();
        StateVariable stateVar;
        
        for (int i = 0; i < simResult.length; i++) {
            stateVar = _stateVars.get(i);
            if (stateVar != null) {
                newStateVarList.add(_stateVars.get(i));
                newStateVectorList.add(originalStateVector[i]);
                
                for (int j = i + 1; j < simResult.length; j++) {
                    if ((simResult[i] != 0.0) && (simResult[i] == simResult[j])) {
                        //add variable j as dependent to variable i
                        stateVar.addDependentStateVariable(_stateVars.get(j));
                        _stateVars.set(j, null);
                    }
                }
            }
        }
        newStateVector = new double[newStateVectorList.size()];
        for (int k = 0; k < newStateVector.length; k++) {
            newStateVector[k] = newStateVectorList.get(k);
        }
        _stateVars = newStateVarList;
        return newStateVector;
        
    }

    private double[] constructSteadyStateVector()
    {
        //add all capacitor voltages and inductor currents
        double[] steadyStateVector = new double[_capacitors.size() + _inductors.size()];

        int i = 0;

        for (ElementCapacitor elem : _capacitors)
        {
            steadyStateVector[i] = elem.capacitor.getVoltage();
            i++;
        }

        for (ElementInductor elem : _inductors)
        {
            steadyStateVector[i] = elem.inductor.getCurrent();
            i++;
        }

        return steadyStateVector;
    }

    //compare two steady state vectors, return the difference
    private double[] compareVectors(double[] vector1, double[] vector2)
    {
        double[] diff = new double[vector1.length];
        double absdiff;

        //calculate % difference for each element, and take the larger % difference
        for (int i = 0; i < vector1.length; i++) {
            absdiff = Math.abs(vector1[i] - vector2[i]);
            diff[i] = Math.max(Math.abs(absdiff/vector1[i]), Math.abs(absdiff/vector2[i]));
        }

        return diff;
    }
    
    //compares two steady state vectors, ignoring control state vectors
    private double[] compareVectorsCircuitOnly(double[] vector1, double[] vector2)
    {
        //state variables are ordered - first circuit, after that control
        int noOfCircuitVars = 0;
        for (int j = 0; j < vector1.length; j++) {
            if (_stateVars.get(j)._modelElement instanceof ElementControl)
                break;
            noOfCircuitVars++;
        }
        
        double[] diff = new double[noOfCircuitVars];
        double absdiff;

        //calculate % difference for each element, and take the larger % difference
        for (int i = 0; i < noOfCircuitVars; i++) {
            absdiff = Math.abs(vector1[i] - vector2[i]);
            diff[i] = Math.max(Math.abs(absdiff/vector1[i]), Math.abs(absdiff/vector2[i]));
        }

        return diff;
    }
    
    //evaluate difference vector, return true if steady-state condition is satisfied
    //take as argument allowed error (difference between 2 vectors), and also comparison option
    //option 1 - take average of all differences
    //option 2 - make sure every difference is below threshold
    //option 3 - make sure most differences are below threshold, and those that are not are below threshold*2
    //option 4 - use norm of error vector
    private boolean evaluateVector(double[] diff, double allowed_error, int option)
    {
        boolean steadystate;

        if (option <= 1)
        {
            double errorTot = 0;
            double avgError;

            for (int i = 0; i < diff.length; i++)
            {
                errorTot += diff[i];
            }

            avgError = errorTot/diff.length;

            if (avgError <= allowed_error)
                steadystate = true;
            else
                steadystate = false;
        }
        else if (option == 2)
        {
            steadystate = true;

            for (int i = 0; i < diff.length; i++)
            {
                if (diff[i] > allowed_error)
                {
                    steadystate = false;
                    break;
                }
            }
        }
        else if (option == 3)
        {
            int noOfPointsBelowError = 0;
            int noOfPointsBelowTwiceError = 0;
            int noOfPointsAboveTwiceError = 0;

            for (int i = 0; i < diff.length; i++)
            {
                if (diff[i] <= allowed_error)
                    noOfPointsBelowError++;
                else if (diff[i] <= (2.0*allowed_error))
                    noOfPointsBelowTwiceError++;
                else
                    noOfPointsAboveTwiceError++;
            }

            //number of points which must be below allowed error
            int requiredPrecision = (int) Math.round(diff.length*0.75);

            if ((noOfPointsBelowError >= requiredPrecision) && (noOfPointsBelowTwiceError <= (diff.length - requiredPrecision)) && (noOfPointsAboveTwiceError == 0))
                steadystate = true;
            else
                steadystate = false;
        }
        else 
        {
            double norm = 0;
            for (int i = 0; i < diff.length; i++)
            {
                norm += diff[i]*diff[i];
            }
            norm = Math.sqrt(norm);
            if (norm <= allowed_error)
                steadystate = true;
            else
                steadystate = false;
        }

        return steadystate;
    }

    //method which simulates (transient) until a steady state is found
    //if dt is set to -1, dt saved in model file is used
    public void detectSteadyStateFromSimulation(double dt)
    {
        //_sim.initSim();

        if (dt == -1)
            dt = _circuitModel.dt.getValue();

        double time = 0;

        initSteadyStateMonitoring();
        //for the setting of initial values including control variables
        constructStateVariableVector();
        double[] initialStateVector = {0, 0, 0, 0, 0};
        //double[] initialStateVector = {6.05, 0.619, -83.2e-3, 5.79, -83.2e-3}; //buck voltage mode pwm "exact" steady state
        //double[] initialStateVector = {6.6, 0.66, -0.011e-3, 6.611, -0.011e-3}; 
        //double[] initialStateVector = {6.0554, 0.620697, 0.0554905, 5.8037, 0.0554905}; //exact steady state for buck voltage mode pwm with PREVIOUS control state, i.e. the ones used to calculate the full control state captured in last step of simulation
        //double[] initialStateVector = {6.0562105316070145, 0.6207926130190716, -0.05605881600842988, 5.803452266745228, -0.05605881600842988}; //exact steady state for buck voltage pwm for control state variables as recorded in last step of simulation, i.e. one step later than above
        setStateVector(initialStateVector);
        _sim.initSim();
        
        double[] steadyStateVector;
        double[] steadyStateVectorNew;
        double[] vectorDifference;

        long startTime = (new java.util.Date()).getTime();

        //simulate first for a while without checking steady state
        for (int i = 0; i < 20; i++)
        {
            time = _sim.simOneStep(time, dt);
        }

        boolean steadyStateReached = false;
        double potentialCycleStartTime;
        int potentialCyclesPassed = 0;
        double period = 0;
         for (int i = 0; i < _periods.length; i++)
            System.out.println("Potential period: " + _periods[i]);

        steadyStateVector = constructSteadyStateVector();
        potentialCycleStartTime = time;
        while ((!steadyStateReached) && (time < _circuitModel.tEnd.getValue()))
        {

            time = _sim.simOneStep(time, dt);
            
            for (int i = 0; i < _periods.length; i++)
            {
                if (Math.abs((time - potentialCycleStartTime) - _periods[i]) <= (2.1*dt)/*(dt/1000)*/)
                {
                    //System.out.println("time - potentialCycleStartTime = " + (time - potentialCycleStartTime));
                    //System.out.println("checking for steady state at time = " + time);
                    potentialCyclesPassed++;
                    steadyStateVectorNew = constructSteadyStateVector();
                    vectorDifference = compareVectors(steadyStateVector,steadyStateVectorNew);
                    /*for (int j = 0; j < vectorDifference.length; j++)
                        System.out.println("diff[" + j + "] = " + vectorDifference[j]);*/
                    steadyStateReached = evaluateVector(vectorDifference,0.0001,2);
                    if (steadyStateReached)
                    {
                        period = time - potentialCycleStartTime;//_periods[i];
                        break;
                    }
                }
            }
            if (potentialCyclesPassed == _periods.length*5)
            {
                steadyStateVector = constructSteadyStateVector();
                potentialCycleStartTime = time;
                potentialCyclesPassed = 0;
            } 
            
        }
        if (steadyStateReached)
        {
            System.out.println("Steady state reached at " + time + " seconds with period of oscillation: " + period + " seconds.");
            double periodsSimulated = Math.round(time / period);
            System.out.println("This is approximately " + periodsSimulated + " periods simulated to steady state.");
            double[] stateVector = getStateVector();
            for (int l = 0; l < stateVector.length; l++) {
                System.out.print(stateVector[l] + " ");
            }
            System.out.print("\n");
        }
        else
        {
            System.out.println("Steady state could not be reached.");
        }

        long endTime = (new java.util.Date()).getTime();
        long simLength = endTime - startTime;
        System.out.println("simulation duration: " + simLength);
    }
    
    
    //try to identify steady state using cross correlation
    public void detectSteadyStateCrossCorrelation(double dt) {
        if (dt == -1) {
            dt = _circuitModel.dt.getValue();
        }
        
        double time = 0;
        
        initSteadyStateMonitoring();
        constructStateVariableVector();
        //initiliaze to zero
        double[] initialStateVector = new double[_stateVars.size()];
        setStateVector(initialStateVector);
        int l = 0;
        for (StateVariable stVar : _stateVars) {
            System.out.print(l+": " + stVar._varName + " ");
            l++;
        }
        System.out.print("\n");
        _sim.initSim();
        
        //simulate first for a while without checking steady state
        for (int i = 0; i < 20; i++)
        {
            time = _sim.simOneStep(time, dt);
        }
        
        for (int i = 0; i < _periods.length; i++)
            System.out.println("Potential period: " + _periods[i]);
        
        boolean steadyStateReached = false;
        double startTimePeriod1 = time;
        double startTimePeriod2 = -1;
        CorrelationValues crossCorrMeanRatio;
        double targetCrossCorrelation = 1.0;
        double allowedError = 0.01;
        double allowedMeanError = 0.01;
        double period = 0;
        
        //to store state variable waveforms
        ArrayList stateVectorWaveforms = new ArrayList();
        
        //IMPORTANT - figure out later how to deal with multiple possible periods
        while ((!steadyStateReached) && (time < _circuitModel.tEnd.getValue()))
        {
            time = _sim.simOneStep(time,dt);
            stateVectorWaveforms.add(constructSteadyStateVector());
            
            if ((Math.abs((time - startTimePeriod1) - _periods[0]) < dt) && startTimePeriod2 < 0) {
                startTimePeriod2 = time;
            }
            else if (Math.abs((time - startTimePeriod1) - 2.0*_periods[0]) < dt) {
                //check if we have an even number of steps saved - if not, do one more step
                if (stateVectorWaveforms.size() % 2 != 0) {
                    time = _sim.simOneStep(time,dt);
                }
                crossCorrMeanRatio = getCrossCorrelation(stateVectorWaveforms);
                steadyStateReached = evaluateCorrelations(crossCorrMeanRatio,targetCrossCorrelation,allowedError,allowedMeanError);
                System.out.println("Start Time Period 1: " + startTimePeriod1);
                System.out.println("End Time Period 1/Start Time Period 2: " + startTimePeriod2);
                System.out.println("End Time Period 2: " + time);
                System.out.print("Cross correlations:");
                for (int i = 0; i < crossCorrMeanRatio.crossCorrelations.length; i++) {
                    System.out.print(" " + crossCorrMeanRatio.crossCorrelations[i]);
                }
                System.out.print("\n");
                System.out.print("Mean ratios:");
                for (int i = 0; i < crossCorrMeanRatio.meanRatios.length; i++) {
                    System.out.print(" " + crossCorrMeanRatio.meanRatios[i]);
                }
                System.out.print("\n");
                //if steady state not reached:
                if (!steadyStateReached) {
                    //reset start time
                    startTimePeriod1 = startTimePeriod2;
                    startTimePeriod2 = time;
                    //throw away first half of the state vector waveform data (the 1st period)
                    int newFirstIndex = stateVectorWaveforms.size() / 2 + 1;
                    //stateVectorWaveforms.removeRange(0,newFirstIndex);
                    stateVectorWaveforms.subList(0,newFirstIndex).clear();
                }
                else {
                    period = time - startTimePeriod2;
                }
            }
            
        }
        
        if (steadyStateReached)
        {
            System.out.println("Steady state reached at " + time + " seconds with period of oscillation: " + period + " seconds.");
            double periodsSimulated = Math.round(time / period);
            System.out.println("This is approximately " + periodsSimulated + " periods simulated to steady state.");
            double[] stateVector = getStateVector();
            for (int i = 0; i < stateVector.length; i++) {
                System.out.print(stateVector[i] + " ");
            }
            System.out.print("\n");
        }
        else
        {
            System.out.println("Steady state could not be reached.");
        }
    }
    
    //method for calculating the cross-correlation between 2 potential periods of a simulation
    private CorrelationValues getCrossCorrelation(ArrayList waveforms) {
        double[] crossCorrelations;
        double[] meanRatios;
        
        int size = waveforms.size();
        int offset = size / 2; //size is always an even number
        
        double[] t1mean, t2mean;
        double[] currentT1, currentT2;
        
        currentT1 = (double[]) waveforms.get(0);
        currentT2 = (double[]) waveforms.get(offset);
        t1mean = new double[currentT1.length];
        t2mean = new double[currentT2.length];
        System.arraycopy(currentT1, 0, t1mean, 0, currentT1.length);
        System.arraycopy(currentT2, 0, t2mean, 0, currentT2.length);
        
        //compute the means of the two periods
        for (int i = 1; i < offset; i++) {
            currentT1 = (double[]) waveforms.get(i);
            currentT2 = (double[]) waveforms.get(i+offset);
            for (int j = 0; j < currentT1.length; j++) {
                t1mean[j] += currentT1[j];
                t2mean[j] += currentT2[j];
            }
        }
        
        meanRatios = new double[t1mean.length];
        double meanDiff;
        for (int i = 0; i < t1mean.length; i++) {
            t1mean[i] = t1mean[i] / offset;
            t2mean[i] = t2mean[i] / offset;
            System.out.println("t1mean[" + i + "] = " + t1mean[i] + " t2mean[" + i + "] = " + t2mean[i]);
            meanDiff = Math.abs(t1mean[i]-t2mean[i]);
            meanRatios[i] = Math.max(Math.abs(meanDiff/t1mean[i]),Math.abs(meanDiff/t2mean[i]));
        }
        
        double[] sumT1, sumT2, sumT1T2;
        sumT1 = new double[t1mean.length];
        sumT2 = new double[t2mean.length];
        sumT1T2 = new double[t1mean.length];
        
        //compute the series (numerator and denominator of cross-correlation formula)
        for (int i = 0; i < offset; i++) {
            currentT1 = (double[]) waveforms.get(i);
            currentT2 = (double[]) waveforms.get(i+offset);
            for (int j = 0; j < t1mean.length; j++) {
                sumT1[j] += (currentT1[j] - t1mean[j]) * (currentT1[j] - t1mean[j]);
                sumT2[j] += (currentT2[j] - t2mean[j]) * (currentT2[j] - t2mean[j]);
                sumT1T2[j] += (currentT1[j] - t1mean[j]) * (currentT2[j] - t2mean[j]);
            }
        }
        
        //calculate cross correlation for each vector element
        crossCorrelations = new double[t1mean.length]; //return cross correlations plus means of the two series
        
        for (int i = 0; i < crossCorrelations.length; i++) {
            crossCorrelations[i] = sumT1T2[i] / (Math.sqrt(sumT1[i]*sumT2[i]));
        }
                
        return (new CorrelationValues(crossCorrelations,meanRatios));
    }
    
    //evaluate if cross correlation of steady state vectors in 2 periods satisfies the target correlation (i.e. steady state)
    private boolean evaluateCorrelations(CorrelationValues crossCorrMeanRatios, double target, double errorCrossCorr, double errorMean) {
        boolean correlationsWithinBounds = true;
        
        for (int i = 0; i < crossCorrMeanRatios.crossCorrelations.length; i++) {
            if (Math.abs(crossCorrMeanRatios.crossCorrelations[i] - target) > errorCrossCorr) {
                correlationsWithinBounds = false;
                break;
            }
        }
        
        for (int i = 0; i < crossCorrMeanRatios.meanRatios.length; i++) {
            if (crossCorrMeanRatios.meanRatios[i] > errorMean) {
                correlationsWithinBounds = false;
                break;
            }
        }
        
        return correlationsWithinBounds;
    }
    
    
    
    
    //methods to implement quasi-Newton steady state solver with Broyden's update
    public void solveSteadyStateQuasiNewton(double dt) {
        
        if (dt < 0) {
            dt = _circuitModel.dt.getValue();
        }
        else {
            _circuitModel.dt.setValue(dt);
        }
        
        //get estimate of simulation error based on machine epsilon
        double etaError = 10e-6;//110000.0*calculateMachineEpsilonDouble();
        //lower bound for perturbation step for Jacobian calculation
        double deltaBound = 10e-3;
        double allowedError = 0.001;
        int errorEvalOption = 2;
        int maxSimRuns;
        double globalCritFactor = 1e-4;
        int lambdaIterations;
        
        initSteadyStateMonitoring();
        constructStateVariableVector();
        //record switch states of circuit
        SwitchStateRecord circuitStates = new SwitchStateRecord(_circuitModel);
        /*_sim.initSim();
        _sim.recordCircuitState(true, circuitStates);*/
        ArrayList<ArrayList<SwitchState>> statesDuringSimulation;
        
        int noOfStateVars = _stateVars.size();
        double[][] iMatrix = constructIMatrix(noOfStateVars); // identity matrix for initial Jacobian calculation
        double[][] jacobian = new double[noOfStateVars][noOfStateVars]; //Jacobian for quasi-Newton method
        double[][] jacobianInv;
        double[] stateVector; 
        double[] stateVectorNext;
        double[] iterationResult;
        double[] jacIterationResult;
        double[] jacDifference;
        int totalSimRuns = 0;
        int extraGlobalRuns = 0;
        boolean steadyStateReached = false;
        double[] error;
        
        double deltaVar;
        double[] iColIMatrixDelta = new double[noOfStateVars];
        double norm2;
        double[] iterationDiff;
        double[] stateDiff;
        double[] newtonStep; //J^(-1)*(x(k)-F(x(k))
        double[] stateIterDiff;
        
        boolean global = false;//true;
        double lambda = 1.0;
        double minf;
        double initialSlope;
        double minfNext;
        double lambdaPrev, lambdaNew;
        double minfNextPrev;
        boolean stepOK;
        
        boolean isCircular = false;
        int initCycles = 0;
        double initSimTime = 0;
        
        maxSimRuns = noOfStateVars*20;
        
        //for pausing after each iteration
        java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
        
        
        System.out.println("identity matrix:");
        for (int j = 0; j < noOfStateVars; j++) {
                for (int l = 0; l < noOfStateVars; l++) {
                    System.out.print(iMatrix[j][l] + " ");
                }
                System.out.print("\n");
            }
        
        for (int i = 0; i < _periods.length && !steadyStateReached; i++) {
            
            System.out.println("Trying to reach steady state with potential period: " + _periods[i]);
            
            //_circuitModel.tEnd.setValue(10*dt);
            
            //set initial state vector
            double[] initialStateVector = {0, 0, 0, 0, 0};
            //double[] initialStateVector = {6.05, 0.619, -83.2e-3, 5.79, -83.2e-3}; //buck voltage mode pwm "exact" steady state
            //double[] initialStateVector = {6.6, 0.66, -0.011e-3, 6.611, -0.011e-3};
            //double[] initialStateVector = {8.291, 0.8, -2.295, 3.147, -2.295}; //buck voltage mode pwm after initial switching sequence established
            //double[] initialStateVector = {5.9974, 0.5999, 1.182e-3, 5.7536, 1.182e-3}; //buck voltage mode pwm average values (over several periods) after steady state reached in transient simulation
            //double[] initialStateVector = {6.0001, 0.6000, 94.17e-6, 5.7523, 94.17e-6}; //buck voltage mode pwm average values (over ONE periods) after steady state reached in transient simulation
            //double[] initialStateVector = {6.0, 0.0, 0.0, 0.0, 0.0}; //buck voltage mode pwm
            //double[] initialStateVector = {11.9653, 10.021, 0.0347, 10.3789}; //buck cpm "exact" steady state
            //double[] initialStateVector = {12.0, 10.0, 0.0, 10.0}; //buck cpm 
            //double[] initialStateVector = {48.5, 25.0, 1.2, 2.3, 0, 0, 0}; //sepic cpm
            //double[] initialStateVector = {48.0, 25.0, 1.2, 2.3, 0.08, 3.5, 0.08}; //sepic cpm
            //double[] initialStateVector = {23.0, 25.0, 1.3, 1.2, 0.01, 1.3, 0.01}; //sepic cpm quasi-state analysis derived
            //double[] initialStateVector = {47.2324, 24.9209, 1.5898, 2.7365, 0.0791, 4.3257, 0.0791}; //sepic cpm "exact" steady state
            setStateVector(initialStateVector);
            _sim.initSim();
            _sim.recordCircuitState(true, circuitStates);
            //check how circuit state changes after initializing simulation (in TDSimulation we call calculateInitialState(1e-9)
            stateVector = getStateVector();
            System.out.println("Initial state vector after circuit solver executes calculateInitialState(1e-9):");
            for (int l = 0; l < stateVector.length; l++) {
                System.out.print(stateVector[l] + " ");
            }
            //simulate for a few steps first to get some values for initial control conditions
            //_sim.startSim();
            
            //First simulate until circular topology is found
            //_circuitModel.tEnd.setValue(_periods[i]);
            double startTime = 0;
            while (!isCircular && initSimTime < _circuitModel.tEnd.getValue()) {
  
                initSimTime = _sim.simOneStep(initSimTime, dt);
                //initCycles++;
                statesDuringSimulation = circuitStates.getOrderedListOfCircuitStates();
                if (Math.abs(startTime - initSimTime) >= (_periods[i])) {
                     isCircular = isCircular(statesDuringSimulation,_periods[i],dt,initSimTime);
                     startTime = initSimTime;
                  }
            }
            stateVector = getStateVector();
            statesDuringSimulation = circuitStates.getOrderedListOfCircuitStates();
            System.out.println("Number of circuit states recorded during last period of pre-simulation: " + statesDuringSimulation.size());
            for (ArrayList<SwitchState> circuitState : statesDuringSimulation) {
                System.out.println("Circuit state at t = " + circuitState.get(0).getTime());
                for (SwitchState deviceState : circuitState) {
                    System.out.println(deviceState.toString());
                }
            }
            initCycles = (int) Math.round(initSimTime / _periods[i]);
            if (isCircular) {
                System.out.println("Circular topology found. Proceeding with N-R iterations.");
                System.out.println("Cycles simulated until circularity found: " + initCycles);
            }
            else {
                System.out.println("Circular topology not found in " + initCycles + " cycles of simulation.");
                break;
            }
            
            circuitStates.reInit();
            //now set to full simulation time
            _circuitModel.tEnd.setValue(_periods[i]);
            //here check for redundant state variables - if some have the same nonzero value, remove them
            stateVector = checkForRedundantVars(stateVector);
            if (stateVector.length != noOfStateVars) {
                noOfStateVars = _stateVars.size();
                iMatrix = constructIMatrix(noOfStateVars);
                jacobian = new double[noOfStateVars][noOfStateVars];
                iColIMatrixDelta = new double[noOfStateVars];
                maxSimRuns = noOfStateVars*30;
            }
            //stateVector = getStateVector();
            setStateVector(stateVector);
            System.out.println("Initial state vector for N-R iterations:");
            for (int l = 0; l < stateVector.length; l++) {
                System.out.print(stateVector[l] + " ");
            }
            System.out.print("\n");
            //now do initialization as in step by step simulation to see how vector derived from previous SIMULATION (so state incl. implicit information
            //about switch states) changes after calling calculateInitialState(1e-9)
           /* _sim.initSim();
            double[] stateVectorAfterInit = getStateVector();
            System.out.println("Initial state vector for N-R iterations AFTER calling calculateInitialState(1e-9):");
            for (int l = 0; l < stateVectorAfterInit.length; l++) {
                System.out.print(stateVectorAfterInit[l] + " ");
            }
            System.out.print("\n");*/
            
            //simulate from given initial conditions, the first time
            _sim.startSim();
            totalSimRuns++;
            
            
            
            //get simulation results
            iterationResult = getStateVector();
            for (int l = 0; l < iterationResult.length; l++) {
                System.out.print(iterationResult[l] + " ");
            }
            System.out.print("\n");
            //initial jacobian calculation using finite difference method
            for (int j = 0; j < noOfStateVars; j++) {
                //get perturbation step to use in formula
                deltaVar = getDeltaForVar(j,etaError,deltaBound);
                System.out.println("deltaVar for state var. " + j + " = " + deltaVar);
                //construct perturbation vector
                for (int k = 0; k < noOfStateVars; k++) {
                    iColIMatrixDelta[k] = iMatrix[k][j]*deltaVar;
                }
                System.out.println("deltaVar*ui:");
                for (int l = 0; l < iColIMatrixDelta.length; l++) {
                    System.out.println(iColIMatrixDelta[l]);
                }
                //now set state vector to perturbed value
                setStateVector(addVectors(stateVector,iColIMatrixDelta));
                double[] v = addVectors(stateVector,iColIMatrixDelta);
                System.out.println("jacobian column " + j + " perturbation vector:");
                for (int l = 0; l < v.length; l++) {
                    System.out.print(v[l] + " ");
                }
                System.out.print("\n");
                //now run simulation with this vector
                circuitStates.reInit();
                _sim.startSim();
                totalSimRuns++;
                //get results
                jacIterationResult = getStateVector();
                System.out.println("jacobian column " + j + " simulation result vector:");
                for (int l = 0; l < jacIterationResult.length; l++) {
                    System.out.print(jacIterationResult[l] + " ");
                }
                System.out.print("\n");
                jacDifference = subtractVectors(jacIterationResult,iterationResult);
                System.out.println("jacobian column " + j + " simulation results difference vector:");
                for (int l = 0; l < jacDifference.length; l++) {
                    System.out.print(jacDifference[l] + " ");
                }
                System.out.print("\n");
                //construct Jacobian column j
                for (int k = 0; k < noOfStateVars; k++) {
                    jacobian[k][j] = iMatrix[k][j] - (jacDifference[k]/deltaVar);
                }
            }
            System.out.println("jacobian after initial iterations:");
            for (int j = 0; j < noOfStateVars; j++) {
                for (int l = 0; l < noOfStateVars; l++) {
                    System.out.print(jacobian[j][l] + " ");
                }
                System.out.print("\n");
            }
            
            lambdaIterations = noOfStateVars;
            
            //now iterate to find solution
            while (!steadyStateReached && (totalSimRuns < maxSimRuns)) {
                //find inverse of jacobian
                //need to copy because inverse method destroys argument
                jacobianInv = DoubleMatrix.inverse(matrixCopy(jacobian));
                System.out.println("jacobian inverse:");
                for (int j = 0; j < noOfStateVars; j++) {
                    for (int l = 0; l < noOfStateVars; l++) {
                        System.out.print(jacobianInv[j][l] + " ");
                    }
                System.out.print("\n");
                }
                //now find next state vector to use, formuma x(k+1) = x(k) - (invJ)*(x(k) - F(x(k))
                stateIterDiff = subtractVectors(stateVector,iterationResult);
                System.out.println("difference xk - F(xk):");
                for (int l = 0; l < stateIterDiff.length; l++) {
                    System.out.print(stateIterDiff[l] + " ");
                }
                System.out.print("\n");
                minf = 0.5*normOfVector2(stateIterDiff);
                initialSlope = minf*(-2.0);
                System.out.println("0.5*Norm^2 of difference xk - F(xk) = " + minf);
                System.out.println("Initial slope = " + initialSlope);
                newtonStep = multMatrixByVector(jacobianInv,stateIterDiff);
                System.out.println("difference xk - F(xk) * jacobian inverse:");
                for (int l = 0; l < newtonStep.length; l++) {
                    System.out.print(newtonStep[l] + " ");
                }
                System.out.print("\n");
                stateVectorNext = subtractVectors(stateVector,multVectorByScalar(lambda,newtonStep));
                System.out.println("next state vector:");
                for (int l = 0; l < stateVectorNext.length; l++) {
                    System.out.print(stateVectorNext[l] + " ");
                }
                System.out.print("\n");
                //now set initial conditions to new stateVector and simulate again
                setStateVector(stateVectorNext);
                circuitStates.reInit();
                _sim.startSim();
                /*_sim.initSim();
                double stime = 0;
                while (stime < _periods[i]) {
                    stime = _sim.simOneStep(stime, dt);
                }*/
                totalSimRuns++;
                //get results
                iterationResult = getStateVector();
                System.out.println("sim. result with new state vector:");
                for (int l = 0; l < iterationResult.length; l++) {
                    System.out.print(iterationResult[l] + " ");
                }
                System.out.print("\n");
                error = compareVectorsCircuitOnly(stateVectorNext,iterationResult);
                System.out.println("steady state error:");
                for (int l = 0; l < error.length; l++) {
                    System.out.print(error[l] + " ");
                }
                System.out.print("\n");
                steadyStateReached = evaluateVector(error,allowedError,errorEvalOption);
                
                //if using global convergence strategy, check whether newton step was adequate, change if necessary
                if (!steadyStateReached && global) {
                    iterationDiff = subtractVectors(stateVectorNext,iterationResult);
                    minfNext = 0.5*normOfVector2(iterationDiff);
                    stepOK = stepSatisfactory(initialSlope,minf,minfNext,lambda,globalCritFactor);
                    if (!stepOK) {
                        //adjust lambda, check if step ok, if not, adjust again
                        lambdaPrev = lambda;
                        minfNextPrev = minfNext;
                        for (int j = 0; j < lambdaIterations && !stepOK; j++) {
                            //first do quadratic adjustment of lambda
                            if (j == 0) {
                                lambda = lambdaQuad(initialSlope,minf,minfNext,lambda);
                            }
                            //all subsequent times do cubic adjustment
                            else {
                                lambdaNew = lambdaCubic(initialSlope,minf,minfNext,minfNextPrev,lambda,lambdaPrev);
                                lambdaPrev = lambda;
                                lambda = lambdaNew;
                                minfNextPrev = minfNext;
                            }
                            //calculate the next state vector using the new lambda (gives new step)
                            stateVectorNext = subtractVectors(stateVector,multVectorByScalar(lambda,newtonStep));
                            System.out.println("next state vector:");
                            for (int l = 0; l < stateVectorNext.length; l++) {
                                System.out.print(stateVectorNext[l] + " ");
                            }
                            //simulate
                            setStateVector(stateVectorNext);
                            _sim.startSim();
                            totalSimRuns++;
                            extraGlobalRuns++;
                            iterationResult = getStateVector();
                            //get difference again
                            iterationDiff = subtractVectors(stateVectorNext,iterationResult);
                            minfNext = 0.5*normOfVector2(iterationDiff);
                            System.out.println("sim. result with new state vector:");
                            for (int l = 0; l < iterationResult.length; l++) {
                                System.out.print(iterationResult[l] + " ");
                            }
                            System.out.print("\n");
                            //check again if step is OK
                            stepOK = stepSatisfactory(initialSlope,minf,minfNext,lambda,globalCritFactor);
                        }
                        //if we've made an ok step, we can check for steady state again
                        error = compareVectorsCircuitOnly(stateVectorNext,iterationResult);
                        System.out.println("steady state error (after global newton step adjustment):");
                        for (int l = 0; l < error.length; l++) {
                            System.out.print(error[l] + " ");
                        }
                        System.out.print("\n");
                        steadyStateReached = evaluateVector(error,allowedError,errorEvalOption);
                        //reset lambda
                        lambda = 1.0;
                    }
                }
                
                if (!steadyStateReached) { //use Broyden's update for Jacobian
                    stateDiff = subtractVectors(stateVectorNext,stateVector);
                    System.out.println("difference stateVectorNext - stateVector");
                    for (int l = 0; l < stateDiff.length; l++) {
                        System.out.print(stateDiff[l] + " ");
                    }
                    System.out.print("\n");
                    iterationDiff = subtractVectors(stateVectorNext,iterationResult);
                    System.out.println("difference stateVectorNext - iterationResult");
                    for (int l = 0; l < iterationDiff.length; l++) {
                        System.out.print(iterationDiff[l] + " ");
                    }
                    System.out.print("\n");
                    norm2 = normOfVector2(stateDiff);
                    System.out.println("norm squared of difference stateVectorNext - stateVector: " + norm2);
                    for (int k = 0; k < noOfStateVars; k++) {
                        for (int j = 0; j < noOfStateVars; j++) {
                            jacobian[k][j] = jacobian[k][j] + ((iterationDiff[k]*stateDiff[j])/norm2);
                        }
                    }
                    System.out.println("new jacobian:");
                    for (int j = 0; j < noOfStateVars; j++) {
                        for (int l = 0; l < noOfStateVars; l++) {
                            System.out.print(jacobian[j][l] + " ");
                        }
                    System.out.print("\n");
                    }
                    stateVector = stateVectorNext;
                    /*System.out.println("Press enter to continue: ");
                    try {
                    String x = in.readLine(); }
                    catch (Exception e) {
                        System.out.println("I/O error!");
                    }*/
                }
            }
            
            if (steadyStateReached) {
                System.out.println("Steady state reached after " + totalSimRuns + " simulations of steady state period.");
                for (int l = 0; l < stateVector.length; l++) {
                    System.out.print(stateVector[l] + " ");
                }
                System.out.print("\n");
                System.out.println(extraGlobalRuns + " simulations of steady state period performed due to global convergence strategy.");
                statesDuringSimulation = circuitStates.getOrderedListOfCircuitStates();
                System.out.println("Number of circuit states recorded: " + statesDuringSimulation.size());
                for (ArrayList<SwitchState> circuitState : statesDuringSimulation) {
                    System.out.println("Circuit state at t = " + circuitState.get(0).getTime());
                    for (SwitchState deviceState : circuitState) {
                        System.out.println(deviceState.toString());
                    }
                }
                isCircular = isCircular(statesDuringSimulation);
                System.out.println("Circular topology: " + isCircular);
                System.out.println("Total periods simulated until steady state: " + (initCycles+totalSimRuns));
                
            }
            else {
                System.out.println("Steady state not reached after " + maxSimRuns + " simulations of steady state period.");
            }
                
            
        }
    }
    
    public void solveSteadyStateAitken(double dt) {
        
        if (dt < 0) {
            dt = _circuitModel.dt.getValue();
        }
        else {
            _circuitModel.dt.setValue(dt);
        }
        
        double[] stateVector = new double[0];
        int noOfStateVars;
        double[][] Ematrix;
        double[][] Amatrix;
        double[] ARowsTVector;
        double[] eVector;
        boolean steadyStateReached = false;
        double[] stateVectorPrev;
        double[] stateVectorDiff;
        int totalSimRuns = 0;
        double[][] EmatrixInv;
        double[][] IMatrix;
        double[][] IADiff, IADiffInv;
        double[] AxPrev, xAxPrevDiff;
        double[] steadyStateVector;
        double[] error;
        double allowedError = 0.0001;
        int errorEvalOption = 2;
        double origTEnd = _circuitModel.tEnd.getValue();
        
        initSteadyStateMonitoring();
        constructStateVariableVector();
        
        //double[] initialStateVector = {6.0001, 0.6000, 94.17e-6, 5.7523, 94.17e-6}; //buck voltage mode pwm average values (over ONE period) after steady state reached in transient simulation
        //double[] initialStateVector = {6.0, 0.5, 0.0, 0.0, 0.0}; //buck voltage mode pwm
        //double[] initialStateVector = {6.05, 0.619, -83.2e-3, 5.79, -83.2e-3}; //buck voltage mode pwm "exact" steady state
        //double[] initialStateVector = {8.291, 0.8, -2.295, 3.147, -2.295}; //buck voltage mode pwm after initial switching sequence established
        //double[] initialStateVector = {5.9974, 0.5999, 1.182e-3, 5.7536, 1.182e-3}; //buck voltage mode pwm average values (over several periods) after steady state reached in transient simulation
        double[] initialStateVector = {4.0, 0.4, 0.0, 0.0, 0.0}; //buck voltage mode pwm
        
        
        for (int i = 0; i < _periods.length && !steadyStateReached; i++) {
            
            System.out.println("Trying to reach steady state with potential period: " + _periods[i]);
            
            setStateVector(initialStateVector);
            
            //identify number of state variables
            _circuitModel.tEnd.setValue(_periods[i]);
            _sim.startSim();
            totalSimRuns++;
            stateVector = getStateVector();
        
            System.out.println("Initial state vector after 1st period of simulation:");
            for (int l = 0; l < stateVector.length; l++) {
              System.out.print(stateVector[l] + " ");
            }
            System.out.print("\n");
 
            //check if some vector have the same non-zero values, eliminate duplicates
            stateVector = checkForRedundantVars(stateVector);
            noOfStateVars = _stateVars.size();
            
            System.out.println("Number of state variables: " + noOfStateVars);
            
            //E is an n^2 x n^2 matrix
            Ematrix = new double[noOfStateVars*noOfStateVars][noOfStateVars*noOfStateVars];
            //e is an n^2 x 1 matrix
            eVector = new double[noOfStateVars*noOfStateVars];
            //A is an n x n matrix
            Amatrix = new double[noOfStateVars][noOfStateVars];
            //Column vector of rows of A transpose is an n^2 x 1 matrix
            ARowsTVector = new double[noOfStateVars*noOfStateVars];
            
            stateVectorPrev = initialStateVector;
            
            stateVectorDiff = subtractVectors(stateVector,stateVectorPrev); //this is is e1
            for (int k = 0; k < noOfStateVars; k++) {
                for (int m = 0; m < noOfStateVars; m++) {
                    Ematrix[k][m+k*noOfStateVars] = stateVectorDiff[m];
                }
            }
            
            //now simulate for another n periods (for n+1 total)
            for (int j = 1; j <= noOfStateVars; j++) {
                setStateVector(stateVector);
                stateVectorPrev = stateVector;
                _sim.startSim();
                totalSimRuns++;
                stateVector = getStateVector();
                
                stateVectorDiff = subtractVectors(stateVector,stateVectorPrev);
                for (int k = 0; k < noOfStateVars && (j < noOfStateVars); k++) {
                    for (int m = 0; m < noOfStateVars; m++) {
                        Ematrix[k+j*noOfStateVars][m+k*noOfStateVars] = stateVectorDiff[m];
                    }
                }
                for (int l = 0; l < noOfStateVars; l++) {
                    eVector[l+(j-1)*noOfStateVars] = stateVectorDiff[l];
                }   
            }
            
            System.out.println("ei vector:");
            for (int l = 0; l < eVector.length; l++) {
              System.out.print(eVector[l] + " ");
            }
            System.out.print("\n");
            
            System.out.println("E matrix:");
            for (int j = 0; j < Ematrix.length; j++) {
               for (int l = 0; l < Ematrix[0].length; l++) {
                  System.out.print(Ematrix[j][l] + " ");
               }
            System.out.print("\n");
            }
            
            //get inverse of E matrix 
            EmatrixInv = DoubleMatrix.inverse(matrixCopy(Ematrix));
            
            System.out.println("E matrix inverse:");
            for (int j = 0; j < EmatrixInv.length; j++) {
               for (int l = 0; l < EmatrixInv[0].length; l++) {
                  System.out.print(EmatrixInv[j][l] + " ");
               }
            System.out.print("\n");
            }
            
            //mult inverse by e vector to get vector of A matrix rows
            ARowsTVector = multMatrixByVector(EmatrixInv,eVector);
            
            System.out.println("A matrix rows vector:");
            for (int l = 0; l < ARowsTVector.length; l++) {
              System.out.print(ARowsTVector[l] + " ");
            }
            System.out.print("\n");
            
            //construct A matrix
            for (int k = 0; k < noOfStateVars; k++) {
                for (int j = 0; j < noOfStateVars; j++)
                    Amatrix[k][j] = ARowsTVector[(k*noOfStateVars)+j];
            }
            
            System.out.println("A matrix:");
            for (int j = 0; j < Amatrix.length; j++) {
               for (int l = 0; l < Amatrix[0].length; l++) {
                  System.out.print(Amatrix[j][l] + " ");
               }
            System.out.print("\n");
            }
            
            //construct n x n identity matrix
            IMatrix = constructIMatrix(noOfStateVars);
            IADiff = new double[noOfStateVars][noOfStateVars];
            //subtract A matrix from identity matrix
            for (int j = 0; j < noOfStateVars; j++) {
                for (int k = 0; k < noOfStateVars; k++) {
                    IADiff[k][j] = IMatrix[k][j] - Amatrix[k][j];
                }
            }
            //invert
            IADiffInv = DoubleMatrix.inverse(matrixCopy(IADiff));
            //mult A matrix by stateVectorPrev - x(i-1)
            AxPrev = multMatrixByVector(Amatrix,stateVectorPrev);
            //subtract from x(i)
            xAxPrevDiff = subtractVectors(stateVector,AxPrev);
            //calculate steady state vector
            steadyStateVector = multMatrixByVector(IADiffInv,xAxPrevDiff);
            
            setStateVector(steadyStateVector);
            _sim.startSim();
            totalSimRuns++;
            stateVector = getStateVector();
            System.out.println("Total sim runs: " + totalSimRuns);
            
            error = compareVectorsCircuitOnly(steadyStateVector,stateVector);
            steadyStateReached = evaluateVector(error,allowedError,errorEvalOption);
            System.out.println("Error for calculated steady state vector:");
            for (int l = 0; l < error.length; l++) {
              System.out.print(error[l] + " ");
            }
            System.out.print("\n");
            System.out.println("Steady state reached: " + steadyStateReached);
            
            
            
        }
        //if not steady state reached, simulate onwards from this point
        if (!steadyStateReached) {
            _circuitModel.tEnd.setValue(origTEnd);
            setStateVector(stateVector);
            detectSteadyStateFromSimulation(-1);
        }
              
        
        
    }
    
    public void solveSteadyStateSkelboe(double dt) {
        
        if (dt < 0) {
            dt = _circuitModel.dt.getValue();
        } else {
            _circuitModel.dt.setValue(dt);
        }
        
        double time = 0;
        
        initSteadyStateMonitoring();
        constructStateVariableVector();
        
        double periodsSimulated;
        int initialIntegrationPeriods = 5; //number of periods to integrate over, starting from zero, in order to find initial point x0
        boolean searchCircularity = true; //if we wish to simulate unti circularity is found instead of using the initial periods given above
        double[] deltaXm;
        Matrix dXm;
        double[][] deltaXkMatrix;
        Matrix Bm;
        Matrix cm;
        double[] cmVector;
        double[] ckxk;
        double[] error;
        double cksum;
        Matrix rm;
        double rmNormF;
        double[] extrapolatedSteadyStateVector;
        double[] steadyStateComparisonVector1, steadyStateComparisonVector2;
        int m;
        ArrayList<double[]> listOfXks;
        ArrayList<double[]> listOfdeltaXks;
        double[] xk, xkprev;
        double[] deltaxk;
        int noOfStateVars;
        double allowedError = 0.0001; //error allowed between beginning and end of period for steady state calculation
        int errorEvalOption = 2;
        double terminationError = 1e-6; //size of euclidian norm of vector rm = Bm*cm - deltaxm i.e. acceptable solution for linear least squares problem
        double[] initialStateVector;
        SwitchStateRecord circuitStates;
        boolean isCircular;
        ArrayList<ArrayList<SwitchState>> statesDuringSimulation = null;
        double startTime;
        boolean steadyStateReached;
        int maximumPeriods;
        double fittingError;
        int iterations;
        int verificationPeriods = 2; //periods to simulate for checking whether extrapolated solution is steady state
        
        for (int i = 0; i < _periods.length; i++) {
            
           System.out.println("Checking with potential period: " + _periods[i] + " s");
            
           periodsSimulated = 0;
           iterations = 0;
           //now perform initial simulation to get initial state vector for Skelboe's algorithm
           _sim.initSim();
           if (searchCircularity) { //simulate until circular topology is found
               circuitStates = new SwitchStateRecord(_circuitModel);
               _sim.recordCircuitState(true, circuitStates);
               isCircular = false;
               startTime = 0;
               while (!isCircular && time < _circuitModel.tEnd.getValue()) {
                  time = _sim.simOneStep(time, dt);
                  statesDuringSimulation = circuitStates.getOrderedListOfCircuitStates();
                  if (Math.abs(startTime - time) >= (_periods[i])) {
                     isCircular = isCircular(statesDuringSimulation,_periods[i],dt,time);
                     startTime = time;
                  }
               }
               periodsSimulated = Math.round(time / _periods[i]);
               
               System.out.println("Simulated until time: " + time + " s");
               System.out.println("Number of circuit states recorded during pre-simulation: " + statesDuringSimulation.size());
               for (ArrayList<SwitchState> circuitState : statesDuringSimulation) {
                    System.out.println("Circuit state at t = " + circuitState.get(0).getTime());
                    for (SwitchState deviceState : circuitState) {
                        System.out.println(deviceState.toString());
                    }
               }
               System.out.println("Circular topology: " + isCircular);
           }
           else { //simulate for predefined number of initial periods
               while (time <= initialIntegrationPeriods*_periods[i]) {
                   time = _sim.simOneStep(time,dt);
               }
               periodsSimulated = initialIntegrationPeriods;
           }
                
           initialStateVector = getStateVector(); 
           initialStateVector = checkForRedundantVars(initialStateVector);
           System.out.println("Initial state vector:");
           for (int l = 0; l < initialStateVector.length; l++) {
               System.out.print(initialStateVector[l] + " ");
           }
           System.out.print("\n");
           
           noOfStateVars = _stateVars.size();
           maximumPeriods = noOfStateVars*200;
           
           steadyStateReached = false;
           while (!steadyStateReached && (time < _circuitModel.tEnd.getValue()) && (periodsSimulated <= maximumPeriods)) {
               m = 2;
               listOfXks = new ArrayList<double[]>();
               listOfdeltaXks = new ArrayList<double[]>();
               //add initial vector, x0, to list
               listOfXks.add(initialStateVector);
               //simulate to get x1, x2
               xkprev = initialStateVector;
               for (int j = 0; j < 2; j++) {
                 startTime = time;
                 while (time <= (startTime+_periods[i])) {
                   time = _sim.simOneStep(time, dt);
                 }
                 periodsSimulated++;
                 //get x1, x2 add to list
                 xk = getStateVector();
                 listOfXks.add(xk);
                 //calculate delta_xm0, delta_xm1, add to list
                 deltaxk = subtractVectors(xk,xkprev);
                 listOfdeltaXks.add(deltaxk);
                 xkprev = xk;
               }
               
               //now attempt to get fit with ck coefficients 
               fittingError = 1;
               cm = null;
               while ((fittingError > terminationError) && (m <= noOfStateVars)) {
                   //simulate to get x(m+1)
                   startTime = time;
                   while (time <= (startTime+_periods[i])) {
                        time = _sim.simOneStep(time, dt);
                   }
                   periodsSimulated++;
                   xk = getStateVector();
                   //calculate delta_xm
                   deltaXm = subtractVectors(xk,xkprev);
                   //construct deltaXm as a Matrix object
                   double[][] deltaXmMatrix = new double[deltaXm.length][1];
                   for (int j = 0; j < deltaXm.length; j++) {
                       deltaXmMatrix[j][0] = deltaXm[j];
                   }
                   dXm = new Matrix(deltaXmMatrix);
                   dXm.print();
                   
                   //construct Bm matrix
                   deltaXkMatrix = new double[noOfStateVars][m];
                   for (int k = 0; k < m; k++) {
                       deltaxk = listOfdeltaXks.get(k);  
                       for (int j = 0; j < noOfStateVars; j++) {
                           deltaXkMatrix[j][k] = deltaxk[j];
                       }
                   }
                   Bm = new Matrix(deltaXkMatrix);
                   Bm.print();
                   
                   //now solve linear least squares problem, minimization of ||Bm*cm - delta_xm||^2
                   //use QR-decomposition
                   QRDecomposition qrD = new QRDecomposition(Bm);
                   //solve for vector cm
                   cm = qrD.solve(dXm);
                   cm.print();
                   //now compute rm = Bm*cm - delta_xm
                   rm = (Bm.times(cm)).minus(dXm);
                   rm.print();
                   
                   //calculate error, ||rm||^2
                   rmNormF = rm.normF();
                   fittingError = rmNormF*rmNormF;
                   
                   System.out.println("For m = " + m + ", error = " + fittingError);

                   if (fittingError > terminationError) {
                       //if error is not satisfactory, we should simulate another period, increment m
                       listOfXks.add(xk);
                       listOfdeltaXks.add(deltaXm);
                       xkprev = xk;
                       m++;
                   }                   
               }
               //construct cm vector
               double[][] cmArray = cm.getArray();
               cmVector = new double[m+1];
               for (int j = 0; j < m; j++) {
                   cmVector[j] = cmArray[j][0];
               }
               cmVector[m] = -1.0; //from the normalization
               
               System.out.println("ck coefficients:");
               for (int l = 0; l < cmVector.length; l++) {
                System.out.print(cmVector[l] + " ");
               }
               System.out.print("\n");
               
               //now calculate the steady state vector z
               //z = Sum(k)ck*xk / Sum(k)ck for k = 0 to m
               cksum = 0;
               ckxk = new double[noOfStateVars];
               for (int j = 0; j <= m; j++) {
                   xk = listOfXks.get(m);
                   for (int k = 0; k < noOfStateVars; k++) {
                       ckxk[k] += cmVector[j]*xk[k];
                   }
                   cksum += cmVector[j];
               }
               extrapolatedSteadyStateVector = new double[noOfStateVars];
               for (int j = 0; j < noOfStateVars; j++) {
                   extrapolatedSteadyStateVector[j] = ckxk[j] / cksum;
               }
               
               System.out.println("Extrapolated steady state vector:");
               for (int l = 0; l < extrapolatedSteadyStateVector.length; l++) {
                System.out.print(extrapolatedSteadyStateVector[l] + " ");
               }
               System.out.print("\n");
               
               setStateVector(extrapolatedSteadyStateVector);
               _sim.initSim();
               
               //simulate for at least TWO cycles to see if steady state reached; disregard results of 1st cycle
               time = 0;
               startTime = 0;
               if (verificationPeriods > 1) {
                    steadyStateComparisonVector1 = null;
                    while (time <= verificationPeriods*_periods[i]) {
                        time = _sim.simOneStep(time, dt);
                        if (time - startTime >= (verificationPeriods-1)*_periods[i]) {
                            startTime = _periods[i]*(verificationPeriods+1); //to avoid overwriting steadyStateComparisonVector1
                            steadyStateComparisonVector1 = getStateVector();
                            periodsSimulated++;
                        }
                    }
               }
               else {
               //simulate for ONE cycle to see if steady state reached
                    steadyStateComparisonVector1 = extrapolatedSteadyStateVector;
                    while (time <= _periods[i]) {
                        time = _sim.simOneStep(time, dt);
                    }
               }
               periodsSimulated++;
               steadyStateComparisonVector2 = getStateVector();
               //compare beginning and end of cycle
               error = compareVectorsCircuitOnly(steadyStateComparisonVector1,steadyStateComparisonVector2);
               
               System.out.println("Error from extrapolated steady state vector:");
               for (int l = 0; l < error.length; l++) {
                System.out.print(error[l] + " ");
               }
               System.out.print("\n");
               
               //evaluate error for steady state condition
               steadyStateReached = evaluateVector(error,allowedError,errorEvalOption);
               
               if (!steadyStateReached) {
                   //if not in steady state, set new x0 = steadyStateComparisonVector2, start again
                   initialStateVector = steadyStateComparisonVector2;
               }
               iterations++;
               
               /*if (iterations == 8)
                periodsSimulated = 1000;*/

               
           }
           
           if (!steadyStateReached) {
               System.out.println("Steady state not reached after " + periodsSimulated + " simulated periods.");
           }
           else {
               System.out.println("Steady state reached after " + periodsSimulated + " simulated periods and " + iterations + " iterations.");
           }
           
        }
        
    }
    
    //constructs an nxn identity matrix
    private double[][] constructIMatrix(int n) {
        
        double[][] IMatrix = new double[n][n];
        
        for (int i = 0; i < n; i++) {
            IMatrix[i][i] = 1.0;
        }
        
        return IMatrix;
    }
    
    private double[] subtractVectors(double[] vector1, double[] vector2) {
        
        double[] result = new double[vector1.length];
        
        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] - vector2[i];
        }
        
        return result;
    }
    
    private double[] addVectors(double[] vector1, double[] vector2) {
        
        double[] result = new double[vector1.length];
        
        for (int i = 0; i < vector1.length; i++) {
            result[i] = vector1[i] + vector2[i];
        }
        
        return result;
    }
    
    private double[] multMatrixByVector(double[][] matrix, double[] vector) {
        
        double[] result = new double[vector.length];
        
        for (int i = 0; i < vector.length; i++) {
            for (int j = 0; j < vector.length; j++) {
                result[i] += matrix[i][j] * vector[j];
            }
        }
        
        return result;
    }
    
    private double normOfVector2(double[] vector) {
        
        double norm = 0;
        
        for (int i = 0; i < vector.length; i++) {
            norm += vector[i]*vector[i];
        }
        //norm = Math.sqrt(norm);
        
        return norm;
    }
    
    private double[] multVectorByScalar(double scalar, double[] vector) {
        
        double[] vectorScaled = new double[vector.length];
        
        for (int i = 0; i < vector.length; i++) {
            vectorScaled[i] = scalar*vector[i];
        }
        
        return vectorScaled;        
    }
    
    //calculate new lambda for global method using quadratic method
    private double lambdaQuad(double initSlope, double minf, double minfNext, double lambda) {
        
        double lambdaNew;
        double lowerBound = 0.1*lambda;
        double upperBound = 0.5*lambda;
        
        System.out.println("Quad. new lambda with minf = " + minf + " lambda = " + lambda + " minfNext = " + minfNext);
        
        lambdaNew = -initSlope / (2.0*(minfNext - minf - initSlope));
        System.out.println("New lambda calculated = " + lambdaNew);
        if (lambdaNew < (lowerBound)) {
            lambdaNew = lowerBound;
        }
        else if (lambdaNew > upperBound) {
            lambdaNew = upperBound;
        }
        System.out.println("New lambda final = " + lambdaNew);
        return lambdaNew;
    }
    
    //calculate new lambda for global method using cubic method
    private double lambdaCubic(double initSlope, double minf, double minfNext, double minfNextPrev, double lambda, double lambdaPrev) {
        
        double lambdaNew;
        double lowerBound = 0.1*lambda;
        double upperBound = 0.5*lambda;
        
        System.out.println("Cub. new lambda with minf = " + minf + " lambda = " + lambda + " minfNext = " + minfNext + " lambdaPrev = " + lambdaPrev + " minfNextPrev = " + minfNextPrev);
        
        double a, b;
        
        a = (1.0 / (lambda - lambdaPrev))*((1.0 / (lambda*lambda))*(minfNext - minf - lambda*initSlope) - (1.0 / (lambdaPrev*lambdaPrev))*(minfNextPrev - minf - lambdaPrev*initSlope));
        b = (1.0 / (lambda - lambdaPrev))*((-lambdaPrev / (lambda*lambda))*(minfNext - minf - lambda*initSlope) + (lambda / (lambdaPrev*lambdaPrev))*(minfNextPrev - minf - lambdaPrev*initSlope));
        
        if (a == 0) {
            lambdaNew = -initSlope / (2.0*b);
        }
        else {
            double d = b*b - 3*a*initSlope;
            lambdaNew = (-b + Math.sqrt(d)) / (3.0*a);
        }
        System.out.println("New lambda calculated = " + lambdaNew);
        if (lambdaNew < (lowerBound)) {
            lambdaNew = lowerBound;
        }
        else if (lambdaNew > upperBound) {
            lambdaNew = upperBound;
        }
        System.out.println("New lambda final = " + lambdaNew);
        return lambdaNew;
    }
    
    private boolean stepSatisfactory(double initSlope, double minf, double minfNext, double lambda, double factor) {
        
        boolean satisfactory;
        
        double criterion;
        System.out.println("Check if step OK with minf = " + minf + " lambda = " + lambda + " minfNext = " + minfNext);
        criterion = minf + factor*lambda*initSlope;
        System.out.println("criterion = " + criterion);
        if (minfNext <= criterion) {
            satisfactory = true;
        }
        else {
            satisfactory = false;
        }
        System.out.println("step OK: " + satisfactory);
        return satisfactory;
    }
    
    //check whether topology is "circular" i.e. has same circuit state at beginning and end of period AND changes in between
    public boolean isCircular(ArrayList<ArrayList<SwitchState>> circuitStatesAfterSimulation) {
        
        boolean circular;
        
        if (circuitStatesAfterSimulation.size() > 4) {
            ArrayList<SwitchState> firstState = circuitStatesAfterSimulation.get(0);
            ArrayList<SwitchState> lastState = circuitStatesAfterSimulation.get(circuitStatesAfterSimulation.size()-1);
            circular = compareStates(firstState,lastState);
        }
        else {
            circular = false;
        }
        return circular;
    }
    
    //find state which is one period +/- 2 dt away, check if equal
    public boolean isCircular(ArrayList<ArrayList<SwitchState>> circuitStatesAfterSimulation, double period, double dt) {
        
        boolean circular = false;
        double timeDiff;
        double periodDiff;
        
        if (circuitStatesAfterSimulation.size() > 4) {
            //traverse list to find same states
            ArrayList<SwitchState> lastState = circuitStatesAfterSimulation.get(circuitStatesAfterSimulation.size()-1);
            int index = circuitStatesAfterSimulation.size()-2;
            ArrayList<SwitchState> currState; 
            for (int i = index; i >= 0 && !circular; i--) {
                currState = circuitStatesAfterSimulation.get(i);
                timeDiff = lastState.get(0).getTime() - currState.get(0).getTime();
                periodDiff = Math.abs(timeDiff - period);
                //System.out.println("timeDiff: " + timeDiff + " periodDiff: " + periodDiff);
                if (periodDiff <= 2.1*dt) {
                    circular = compareStates(currState,lastState);
                }
            }
        }
        
        return circular;
    }
    
    //identify current state (last state in list)
    //find state of circuit 1 period ago from present time
    //check if states are equal - if yes, circular
    public boolean isCircular(ArrayList<ArrayList<SwitchState>> circuitStatesAfterSimulation, double period, double dt, double time) {
        
        boolean circular = false;
        double timeDiff1, timeDiff2;
        
        if (circuitStatesAfterSimulation.size() > 4) {
            //traverse list to find same states
            ArrayList<SwitchState> lastState = circuitStatesAfterSimulation.get(circuitStatesAfterSimulation.size()-1);
            int index = circuitStatesAfterSimulation.size()-2;
            ArrayList<SwitchState> currState, prevState; 
            for (int i = index; i >= 0 && !circular; i--) {
                currState = circuitStatesAfterSimulation.get(i);
                prevState = circuitStatesAfterSimulation.get(i+1);
                timeDiff1 = time - currState.get(0).getTime();
                timeDiff2 = time - prevState.get(0).getTime();
                if ((timeDiff1 > period) && (timeDiff2 <= period)) {
                    circular = compareStates(currState,lastState);
                }
            }
        }
        
        return circular;
    }
    
    public boolean compareStates(ArrayList<SwitchState> state1, ArrayList<SwitchState> state2) {
        
        boolean equal = true;
        SwitchState deviceState1, deviceState2;
        
        if (state1.size() != state2.size()) {
            return false;
        }
        
        for (int index = 0; index < state1.size() - 1; index++) {
            deviceState1 = state1.get(0);
            deviceState2 = state2.get(0);
            if (deviceState1.getState() != deviceState2.getState()) {
                equal = false;
                break;
            }
        }
        return equal;
    }
    
    private double calculateMachineEpsilonDouble() {
        double machEps = 1.0d;
 
        do {
           machEps /= 2.0d;
        }
        while ((double)(1.0 + (machEps/2.0)) != 1.0);
 
        return machEps;
    }
    
    private double[][] matrixCopy(double[][] matrix) {
        
        double[][] copy = new double[matrix.length][matrix[0].length];
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                copy[i][j] = matrix[i][j];
            }
        }
        
        return copy;
    }
           
    //want to return both means and cross-correlation values
    class CorrelationValues {
        double[] crossCorrelations;
        double[] meanRatios;
        
        CorrelationValues(double[] correlations, double[] means) {
            crossCorrelations = correlations;
            meanRatios = means;
        }
    }

}
