/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew.GeckoSCRIPT;

import gecko.GeckoCIRCUITS.allg.Typ;
import geckocircuitsnew.Model;
import geckocircuitsnew.OPTIMIZABLE;
import gecko.geckocircuitsnew.circuit.TDSimulation;
import gecko.GeckoCIRCUITS.elements.*;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControl;
import Utilities.ModelMVC.ModelMVC;
import gecko.geckocircuitsnew.circuit.TimeFunction;
import gecko.geckocircuitsnew.circuit.SwitchStateRecord;

import java.util.ArrayList;
import java.util.HashMap;

import java.lang.reflect.*;
import java.lang.annotation.Annotation;

/**
 *
 * @author anstupar
 */
public class SimulationAccess {
    private Model circuitModel;
    private ScriptWindow scriptwindow;
    private boolean simFinished;
    private TDSimulation simulation;
    private boolean sim_init = false;
    private SwitchStateRecord stateRecord;
    private boolean record_states = false;

    public SimulationAccess (Model circuit)
    {
        circuitModel = circuit;
        scriptwindow = new ScriptWindow(this);
        scriptwindow.setVisible(true);
        simFinished = false;
    }

    //constructor for creating a SimulationAccess object without opening a script window
    public SimulationAccess(Model circuit, boolean userscripting)
    {
        circuitModel = circuit;
        if (userscripting)
        {
            scriptwindow = new ScriptWindow(this);
            scriptwindow.setVisible(true);
        }
        simFinished = false;
    }

    public void startSim()
    {
         simFinished = false;
         simulation = new TDSimulation(circuitModel);
         if (record_states) {
             simulation.recordCircuitSwitchStates(stateRecord);
         }
         simulation.start();
         while (!simulation.isFinished());
         simFinished = true;
    }
    
    //for simulating step-by-step
    public void initSim()
    {
        simulation = new TDSimulation(circuitModel);
        simulation.init();
        sim_init = true;
    }
    
    /*public void initSim(double dampingFactor)
    {
        simulation = new TDSimulation(circuitModel,true,dampingFactor);
        simulation.init();
        sim_init = true;
    }*/
    
    public double simOneStep(double time, double dt)
    {
        if (!sim_init)
            initSim();

        if (dt == -1) //use step size defined in the model
            return simulation.simulateOneStep(time);
        else
            return simulation.simulateOneStep(time,dt);
    }

    public boolean isModelActive(Model activeModel)
    {
        if (activeModel == circuitModel)
            return true;
        else
            return false;
    }

    public void makeVisible()
    {
        scriptwindow.setVisible(true);
    }

    public boolean isSimFinished()
    {
        return simFinished;
    }
    
    public void recordCircuitState(boolean record, SwitchStateRecord recordMap) {
        if (record) {
            if (sim_init) {
            simulation.recordCircuitSwitchStates(recordMap);
            }
            record_states = true;
            stateRecord = recordMap;
        }
        else {
            if (sim_init) {
            simulation.stopRecordingCircuitSwitchStates();
            }
            record_states = false;
        }
    }
    

    public ArrayList<ElementIGBT> getIGBTs()
    {
        ElementIGBT igbt;
        Class elementClass;
        String className;
        ArrayList<ElementIGBT> IGBTs = new ArrayList<ElementIGBT>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementIGBT"))
             {
                igbt = (ElementIGBT) elem;
                IGBTs.add(igbt);
             }
        }
        return IGBTs;
    }

    public ArrayList<ElementDiode> getDiodes()
    {
        ElementDiode diode;
        Class elementClass;
        String className;
        ArrayList<ElementDiode> Diodes = new ArrayList<ElementDiode>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementDiode"))
             {
                diode = (ElementDiode) elem;
                Diodes.add(diode);
             }
        }
        return Diodes;
    }
    
    public ArrayList<ElementIdealSwitch> getIdealSwitches()
    {
        ElementIdealSwitch idealswitch;
        Class elementClass;
        String className;
        ArrayList<ElementIdealSwitch> Switches = new ArrayList<ElementIdealSwitch>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementIdealSwitch"))
             {
                idealswitch = (ElementIdealSwitch) elem;
                Switches.add(idealswitch);
             }
        }
        return Switches;
    }
    


    public ArrayList<ElementThyristor> getThyristors()
    {
        ElementThyristor thyr;
        Class elementClass;
        String className;
        ArrayList<ElementThyristor> Thyristors = new ArrayList<ElementThyristor>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementThyristor"))
             {
                thyr = (ElementThyristor) elem;
                Thyristors.add(thyr);
             }
        }
        return Thyristors;
    }

    public ArrayList<ElementResistor> getResistors()
    {
        ElementResistor res;
        Class elementClass;
        String className;
        ArrayList<ElementResistor> Resistors = new ArrayList<ElementResistor>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementResistor"))
             {
                res = (ElementResistor) elem;
                Resistors.add(res);
             }
        }
        return Resistors;
    }

    public ArrayList<ElementCapacitor> getCapacitors()
    {
        ElementCapacitor cap;
        Class elementClass;
        String className;
        ArrayList<ElementCapacitor> Capacitors = new ArrayList<ElementCapacitor>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementCapacitor"))
             {
                cap = (ElementCapacitor) elem;
                Capacitors.add(cap);
             }
        }
        return Capacitors;
    }

    public ArrayList<ElementInductor> getInductors()
    {
        ElementInductor ind;
        Class elementClass;
        String className;
        ArrayList<ElementInductor> Inductors = new ArrayList<ElementInductor>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
             if (className.equals("ElementInductor"))
             {
                ind = (ElementInductor) elem;
                Inductors.add(ind);
             }
        }
        return Inductors;
    }

    public ArrayList<ElementControl> getControlElements()
    {
        ElementControl control_elem;
        ArrayList<ElementControl> ControlElements = new ArrayList<ElementControl>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            if (elem instanceof ElementControl)
            {
                control_elem = (ElementControl) elem;
                ControlElements.add(control_elem);
            }
        }
        return ControlElements;
    }

    public ArrayList<ElementCircuit> getCircuitElements()
    {
        ElementCircuit circuit_elem;
        ArrayList<ElementCircuit> CircuitElements = new ArrayList<ElementCircuit>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            if (elem instanceof ElementCircuit)
            {
                circuit_elem = (ElementCircuit) elem;
                CircuitElements.add(circuit_elem);
            }
        }
        return CircuitElements;
    }

    public ArrayList<ElementInterface> getAllElements()
    {
        ArrayList<ElementInterface> AllNonWireElements = new ArrayList<ElementInterface>();
        for (ElementInterface elem : circuitModel.modelElements)
        {
            if (!(elem instanceof ElementConnection))
            {
                AllNonWireElements.add(elem);
            }
        }
        return AllNonWireElements;
    }

    public double getNodeValue(String nodeName) throws Exception
    {
        double nodevalue = 0;
        TerminalControl term;
        boolean found = false;
        for (ElementInterface elem : circuitModel.modelElements)
        {
            if (elem instanceof ElementControl)
            {
               for (Terminal node : elem._terminals)
               {
                   if (node.getLabel().equals(nodeName))
                   {
                      term = (TerminalControl) node;
                      nodevalue = term._potArea._value;
                      found = true;
                   }
              }
            }
        }
        if (found)
         return nodevalue;
        else
         throw new Exception("node not found!");
    }

    public ElementInterface getElementByName(String elemName) throws Exception
    {
        ElementInterface search = null;
        boolean found = false;
        for (ElementInterface elem : circuitModel.modelElements)
        {
            if (elem._elementName.getValue().equals(elemName))
            {
                found = true;
                search = elem;
            }
        }
        if (found)
            return search;
        else
           throw new Exception("element not not found!");
    }
    
    private ArrayList<Field> getAnnotatedFields(ElementInterface elem, String annotation)
    {
        ArrayList<Field> annotated_fields = new ArrayList<Field>();
        Class elementClass = elem.getClass();
        Field[] elementFields = elementClass.getFields();
        
        for (int i = 0; i < elementFields.length; i++)
        {
            for (Annotation an : elementFields[i].getAnnotations())
            {
                if (an.toString().endsWith(annotation+"()"))
                    annotated_fields.add(elementFields[i]);
            }
        }
        
        return annotated_fields;
        
    }

    public Field[] getModifiableFields(ElementInterface elem)
    {
        ArrayList<Field> accessible_fields = getAnnotatedFields(elem,"ACCESSIBLE");
        ArrayList<Field> optimizable_fields = getAnnotatedFields(elem,"OPTIMIZABLE");
        accessible_fields.addAll(optimizable_fields);
        Field[] modifiable_fields = (Field[]) accessible_fields.toArray(new Field[1]);
        return modifiable_fields;
    }


    public void setFieldByName(ElementInterface elem, String fieldName, double value) throws Exception
    {
        boolean found = false;
        ModelMVC<Double> fieldobject;


        //get @OPTIMIZABLE fields
        ArrayList<Field> optimizableFields = getAnnotatedFields(elem,"OPTIMIZABLE");

        for (Field field : optimizableFields)
        {
            fieldobject = (ModelMVC<Double>) field.get(elem);
            if (fieldobject.getDescription().equals(fieldName))
            {
                found = true;
                fieldobject.setValue(value);
            }
        }

        if (!found)
        {
            //get @ACCESSIBLE fields
            ArrayList<Field> accessibleFields = getAnnotatedFields(elem,"ACCESSIBLE");
            Object fieldobj; //not all @ACCESSIBLE fields are ModelMVC<Double>
            for (Field field : accessibleFields)
            {
                fieldobj = field.get(elem);
                if (fieldobj.getClass().getSimpleName().equals("ModelMVC<Double>"))
                {
                    fieldobject = (ModelMVC<Double>) fieldobj;
                    if (fieldobject.getDescription().equals(fieldName))
                    {
                        found = true;
                        fieldobject.setValue(value);
                    }
                }
            }
        }

        if (!found)
            throw new Exception("parameter by that name not found!");

    }

    public void setRon_IGBT(ElementIGBT igbt, double ron)
    {
        igbt.rON.setValue(ron);
    }

    public void setRon_diode(ElementDiode diode, double rd)
    {
        diode.onResistance.setValue(rd);
    }

    public void setRoff_IGBT(ElementIGBT igbt, double roff)
    {
        igbt.rOFF.setValue(roff);
    }

    public void setForwardVoltage_IGBT(ElementIGBT igbt, double vf)
    {
        igbt.uForward.setValue(vf);
    }

    public void setRoff_diode(ElementDiode diode, double roff)
    {
        diode.offResistance.setValue(roff);
    }

    public void setForwardVoltage_diode(ElementDiode diode, double vd)
    {
        diode.uForward.setValue(vd);
    }

    public void setReverseRecovery_thyristor(ElementThyristor thyr, double tRR)
    {
        thyr._trRR.setValue(tRR);
    }

    public void setRon_thyristor(ElementThyristor thyr, double ron)
    {
        thyr._rON.setValue(ron);
    }

    public void setRoff_thyristor (ElementThyristor thyr, double roff)
    {
        thyr._rOFF.setValue(roff);
    }

    public void setForwardVoltage_thyristor(ElementThyristor thyr, double vf)
    {
        thyr._uForward.setValue(vf);
    }

    public void setRon_idealswitch(ElementIdealSwitch sw, double ron)
    {
        sw._rON.setValue(ron);
    }

    public void setRoff_idealswitch(ElementIdealSwitch sw, double roff)
    {
        sw._rOFF.setValue(roff);
    }

    public void setResistor(ElementResistor resistor, double R)
    {
        resistor.resistance.setValue(R);
    }

    public void setInductance_inductor(ElementInductor ind, double L)
    {
        ind.inductance.setValue(L);
    }

    public void setInitialCurrent_inductor(ElementInductor ind, double I)
    {
        ind.initCurrent.setValue(I);
    }

    public void setCapacitance_capacitor(ElementCapacitor cap, double C)
    {
        cap.capacitance.setValue(C);
    }

    public void setInitialVoltage_capacitor(ElementCapacitor cap, double V)
    {
        cap.capacitance.setValue(V);
    }

    //function for advanced scripting within the GeckoSCRIPT window in GeckoCIRCUITS - manipulation of all model objects (ElementInterfaces) directly
    //return an array of array lists, with each array entry having an array list of elements of a particular type

    public ArrayList/*<ElementInterface>*/[] getAllObjects()
    {
        ArrayList<ElementInterface> allObjects = getAllElements();
        ArrayList<ArrayList<ElementInterface>> allObjectsSorted = new ArrayList<ArrayList<ElementInterface>>();
        HashMap indicesmap = new HashMap();
        int index = 0;
        int currentindex;
        ArrayList<ElementInterface> currentList;
        String objectClass;

        for (ElementInterface elem : allObjects)
        {
            if (indicesmap.isEmpty()) //we are the beginning of the allObjects list; nothing in hashmap yet
            {
                currentList = new ArrayList<ElementInterface>();
                currentList.add(elem);
                indicesmap.put(elem.getClass().getSimpleName(), new Integer(index)); //keep track of which index of allObjectsSorted contains the list for this type of object
                allObjectsSorted.add(index,currentList);
                index++;
            }
            else //at some point in the allObjects list; hashmap has some entries
            {
                objectClass = elem.getClass().getSimpleName();
                if (indicesmap.containsKey(objectClass)) //if this type of element was already encountered, find the proper array list to put it in
                {
                    currentindex = ((Integer) indicesmap.get(objectClass)).intValue();
                    currentList = allObjectsSorted.get(currentindex);
                    currentList.add(elem);
                }
                else //newly encountered element type; create new entry in allObjectsSorted list
                {
                    currentList = new ArrayList<ElementInterface>();
                    currentList.add(elem);
                    indicesmap.put(objectClass, new Integer(index));
                    allObjectsSorted.add(index,currentList);
                    index++;
                }
            }
        }
        //try {
        //Class<?> clazz;

        //clazz = Class.forName("java.util.ArrayList<gecko.GeckoCIRCUITS.elements.ElementInterface>");
        //ArrayList<ElementInterface>[] sortedElements = (ArrayList<ElementInterface>[]) allObjectsSorted.toArray((ArrayList<ElementInterface>[]) Array.newInstance(clazz, 1));
        ArrayList/*<ElementInterface>*/[] sortedElements = (ArrayList/*<ElementInterface>*/[]) allObjectsSorted.toArray( new ArrayList/*<ElementInterface>*/[1]);
        return sortedElements;//}
        //catch (java.lang.ClassNotFoundException e) {
          //  System.err.println(e);
           // return null;
        //}

        

    }


    //functions for steady state analysis
    /*public ArrayList<ElementInterface> getStateVariables()
    {
        //state variables through which steady state is ascertained - in this case capacitors and inductors (coupled and uncoupled)
        ArrayList<ElementInterface> stateVars = new ArrayList<ElementInterface>();
        Class elementClass;
        String className;
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
            if (className.equals("ElementCapacitor") || className.equals("ElementInductor"))
                stateVars.add(elem);
        }
        return stateVars;
    }*/

    //function to return all elements which produce a periodic signal of some sort - voltage/current source, signal generators
    public ArrayList<ElementInterface> getPeriodicElements()
    {
        ArrayList<ElementInterface> periodElems = new ArrayList<ElementInterface>();
        Class elementClass;
        String className;
        int sourceType;
        for (ElementInterface elem : circuitModel.modelElements)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
            if (className.equals("ElementCurrentSource"))
            {
                sourceType = ((ElementCurrentSource) elem).getSourceType();
                if ((sourceType == Typ.QUELLE_DREIECK) || (sourceType == Typ.QUELLE_RECHTECK)|| (sourceType == Typ.QUELLE_SIN))
                {
                    periodElems.add(elem);
                }
                //System.out.println(elem._elementName.getValue());
            }
            else if (className.equals("ElementVoltage"))
            {
                sourceType = ((ElementVoltage) elem).getSourceType();
                if ((sourceType == Typ.QUELLE_DREIECK) || (sourceType == Typ.QUELLE_RECHTECK)|| (sourceType == Typ.QUELLE_SIN))
                {
                    periodElems.add(elem);
                }
                //System.out.println(elem._elementName.getValue());
            }
            else if (className.equals("ElementSignalSource"))
            {
                sourceType = ((ElementSignalSource) elem).getSourceType();
                if ((sourceType == Typ.QUELLE_DREIECK) || (sourceType == Typ.QUELLE_RECHTECK)|| (sourceType == Typ.QUELLE_SIN))
                {
                    periodElems.add(elem);
                }
                //System.out.println(elem._elementName.getValue());
            }
        }
        return periodElems;
    }

    public double[] getFrequencies()
    {
        ArrayList<ElementInterface> periodElems = getPeriodicElements();
        double[] frequencies = new double[periodElems.size()];
        Class elementClass;
        String className;
        int i = 0;
        for (ElementInterface elem : periodElems)
        {
            elementClass = elem.getClass();
            className = elementClass.getSimpleName();
            if (className.equals("ElementCurrentSource"))
            {
                frequencies[i] = ((ElementCurrentSource) elem).frequency.getValue();
                //System.out.println(elem._elementName.getValue() + ": " + frequencies[i]);
            }
            else if (className.equals("ElementVoltage"))
            {
                frequencies[i] = ((ElementVoltage) elem).frequency.getValue();
                //System.out.println(elem._elementName.getValue() + ": " + frequencies[i]);
            }
            else if (className.equals("ElementSignalSource"))
            {
                frequencies[i] = ((ElementSignalSource) elem).frequency.getValue();
                //System.out.println(elem._elementName.getValue() + ": " + frequencies[i]);
            }
            i++;
        }
        return frequencies;
    }
    
    


}
