/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package geckocircuitsnew.GeckoSCRIPT;
import javax.swing.JTextArea;

/**
 * Source created on Thu Jan 27 18:47:56 CET 2011
 */

public class GeckoCustom40096 extends GeckoCustom {

 		public int x;

public GeckoCustom40096(SimulationAccess simaccess, JTextArea area) {
 		 super(simaccess,area);
 }

     public void runScript() throws Exception {
 // ****************** your code segment **********************
 		x = 0;
 		x = x + 2;

 		System.out.println("x = " + x);
 // ****************** end of code segment **********************
     }
 }
