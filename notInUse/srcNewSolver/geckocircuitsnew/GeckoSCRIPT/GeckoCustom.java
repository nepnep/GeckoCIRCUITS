/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package geckocircuitsnew.GeckoSCRIPT;
import javax.swing.JTextArea;
import java.io.PrintWriter;
import gecko.GeckoCIRCUITS.elements.*;
import java.util.HashMap;
import java.util.ArrayList;

/**
 *
 * @author anstupar
 */
public abstract class GeckoCustom {

    protected SimulationAccess circuit;
    private JTextArea outputWindow;
    private PrintWriter output;
    private HashMap elements;

    protected GeckoCustom (SimulationAccess simaccess, JTextArea outputFrame)
    {
       circuit = simaccess;
       outputWindow = outputFrame;
       output = new PrintWriter(new JTextAreaWriter(outputWindow));
       elements = new HashMap();
    }

    protected void runSimulation()
    {
        circuit.startSim();
    }

    public abstract void runScript() throws Exception;


    protected void writeOutput(String toBeWritten)
    {
        output.print(toBeWritten);
    }

    protected void writeOutputLn(String toBeWritten)
    {
        output.println(toBeWritten);
    }

    protected boolean simFinished()
    {
        return circuit.isSimFinished();
    }

    protected double getNodeValue(String name)
    {
        try {
            return circuit.getNodeValue(name);
        }
        catch (Exception e)
        {
            System.err.println(e);
            writeOutput("Node named " + name + " not found in circuit!");
        }
        return 0;
    }

    private ElementInterface getElementByName(String name)
    {
        ElementInterface elem = null;
        try {
            elem = circuit.getElementByName(name);
            elements.put(name,elem);
        }
        catch (Exception e)
        {
            System.err.println(e);
            writeOutput("Element named " + name + " not found in circuit!");
        }
        return elem;
    }

    //gives the user an array of strings - names of all control elements in circuit
    protected String[] getControlElements()
    {
        ArrayList<ElementControl> control_elems = circuit.getControlElements();
        int size = control_elems.size();
        String[] control_names = new String[size];
        int i = 0;
        for (ElementControl elem : control_elems)
        {
            control_names[i] = elem._elementName.getValue();
            if (elements.get(control_names[i]) == null)
                elements.put(control_names[i], elem);
            i++;
        }
        return control_names;
    }

    //gives the user an array of strings - names of all circuit elements in circuit
    protected String[] getCircuitElements()
    {
        ArrayList<ElementCircuit> circuit_elems = circuit.getCircuitElements();
        int size = circuit_elems.size();
        String[] circuit_names = new String[size];
        int i = 0;
        for (ElementCircuit elem : circuit_elems)
        {
            circuit_names[i] = elem._elementName.getValue();
            if (elements.get(circuit_names[i]) == null)
                elements.put(circuit_names[i], elem);
            i++;
        }
        return circuit_names;
    }

    //gives the user an array of strings - names of all non-wire (non-connector) elements in the model
    protected String[] getAllElements()
    {
        ArrayList<ElementInterface> model_elems = circuit.getAllElements();
        int size = model_elems.size();
        String[] model_names = new String[size];
        int i = 0;
        for (ElementInterface elem : model_elems)
        {
            model_names[i] = elem._elementName.getValue();
            if (elements.get(model_names[i]) == null)
                elements.put(model_names[i], elem);
            i++;
        }
        return model_names;
    }

    //gives the user an array of strings - names of all IGBTs in circuit
    protected String[] getIGBTs()
    {
        ArrayList<ElementIGBT> igbts = circuit.getIGBTs();
        int size = igbts.size();
        String[] IGBT_names = new String[size];
        int i = 0;
        for (ElementIGBT elem : igbts)
        {
            IGBT_names[i] = elem._elementName.getValue();
            if (elements.get(IGBT_names[i]) == null)
                elements.put(IGBT_names[i], elem);
            i++;
        }
        return IGBT_names;
    }

    //now the same for diodes, resistors, capacitors, thyristors, switches
    protected String[] getDiodes()
    {
        ArrayList<ElementDiode> diodes = circuit.getDiodes();
        int size = diodes.size();
        String[] diode_names = new String[size];
        int i = 0;
        for (ElementDiode elem : diodes)
        {
            diode_names[i] = elem._elementName.getValue();
            if (elements.get(diode_names[i]) == null)
                elements.put(diode_names[i], elem);
            i++;
        }
        return diode_names;
    }

    protected String[] getThyristors()
    {
        ArrayList<ElementThyristor> thyrs = circuit.getThyristors();
        int size = thyrs.size();
        String[] thyr_names = new String[size];
        int i = 0;
        for (ElementThyristor elem : thyrs)
        {
            thyr_names[i] = elem._elementName.getValue();
            if (elements.get(thyr_names[i]) == null)
                elements.put(thyr_names[i], elem);
            i++;
        }
        return thyr_names;
    }

    protected String[] getIdealSwitches()
    {
        ArrayList<ElementIdealSwitch> switches = circuit.getIdealSwitches();
        int size = switches.size();
        String[] IS_names = new String[size];
        int i = 0;
        for (ElementIdealSwitch elem : switches)
        {
            IS_names[i] = elem._elementName.getValue();
            if (elements.get(IS_names[i]) == null)
                elements.put(IS_names[i], elem);
            i++;
        }
        return IS_names;
    }

    protected String[] getResistors()
    {
        ArrayList<ElementResistor> resistors = circuit.getResistors();
        int size = resistors.size();
        String[] res_names = new String[size];
        int i = 0;
        for (ElementResistor elem : resistors)
        {
            res_names[i] = elem._elementName.getValue();
            if (elements.get(res_names[i]) == null)
                elements.put(res_names[i], elem);
            i++;
        }
        return res_names;
    }

    protected String[] getInductors()
    {
        ArrayList<ElementInductor> inductors = circuit.getInductors();
        int size = inductors.size();
        String[] ind_names = new String[size];
        int i = 0;
        for (ElementInductor elem : inductors)
        {
            ind_names[i] = elem._elementName.getValue();
            if (elements.get(ind_names[i]) == null)
                elements.put(ind_names[i], elem);
            i++;
        }
        return ind_names;
    }

    protected String[] getCapacitors()
    {
        ArrayList<ElementCapacitor> capacitors = circuit.getCapacitors();
        int size = capacitors.size();
        String[] cap_names = new String[size];
        int i = 0;
        for (ElementCapacitor elem : capacitors)
        {
            cap_names[i] = elem._elementName.getValue();
            if (elements.get(cap_names[i]) == null)
                elements.put(cap_names[i], elem);
            i++;
        }
        return cap_names;
    }

    //method which sets the value of field of an element, taking the name of that field as argument
    protected void setParameter(String elementName, String parameterName, double value)
    {
        ElementInterface elem = (ElementInterface) elements.get(elementName);
        if (elem == null)
        {
            elem = getElementByName(elementName);
        }
        if (elem != null)
        {
            try {
                circuit.setFieldByName(elem, parameterName, value);
            }
            catch (Exception e)
            {
                System.err.println(e);
                writeOutput("Element " + elementName + " does not have parameter field " + parameterName + ".");
            }
        }
    }

    //method which allows several parameters to be set at once, with names and values given as arrays
    protected void setParameters(String elementName, String[] parameterNames, double[] values)
    {
        if (parameterNames.length == values.length)
        {
            if (elements.get(elementName) != null || getElementByName(elementName) != null)
            {
                for (int i = 0; i < values.length; i++)
                {
                    setParameter(elementName,parameterNames[i],values[i]);
                }
            }
        }
        else
        {
            writeOutput("Error writing values for element " + elementName + ": array names not equal in length to array of values.");
        }
    }

    //method which sets resistance of a circuit element
    //if element is resistor, sets its resistance
    //if element is a semiconductor, sets off resistance if type = "off",
    //otherwise sets the on resistance
    protected void setResistance(String name, String type, double res)
    {
        ElementInterface elem = (ElementInterface) elements.get(name);
        if (elem == null)
        {
            elem = getElementByName(name);
        }
        if (elem != null) {
        Class elementClass = elem.getClass();
        String className = elementClass.getSimpleName();
        type.toLowerCase();

        if (className.equals("ElementResistor"))
        {
            circuit.setResistor((ElementResistor) elem, res);
        }
        else if (className.equals("ElementIGBT"))
        {
            if (type.contains("off"))
                circuit.setRoff_IGBT((ElementIGBT) elem, res);
            else
                circuit.setRon_IGBT((ElementIGBT) elem, res);
        }
        else if (className.equals("ElementDiode"))
        {
            if (type.contains("off"))
                circuit.setRoff_diode((ElementDiode) elem, res);
            else
                circuit.setRon_diode((ElementDiode) elem, res);
        }
        else if (className.equals("ElementThyristor"))
        {
            if (type.contains("off"))
                circuit.setRoff_thyristor((ElementThyristor) elem, res);
            else
                circuit.setRon_thyristor((ElementThyristor) elem, res);
        }
        else if (className.equals("ElementIdealSwitch"))
        {
            if (type.contains("off"))
                circuit.setRoff_idealswitch((ElementIdealSwitch) elem, res);
            else
                circuit.setRon_idealswitch((ElementIdealSwitch) elem, res);
        }
        else
        {
            writeOutputLn("Cannot set resistance value for this element " + name + " because it is of type " + className);
        } }
    }

    //set reverse recovery time for a thyristor
    protected void setTRR(String name, double time)
    {
        ElementThyristor thyr = (ElementThyristor) elements.get(name);
        if (thyr == null)
            thyr = (ElementThyristor) getElementByName(name);
        if (thyr != null) {
        circuit.setReverseRecovery_thyristor(thyr, time); }
    }

    protected void setFVoltage(String name, double V)
    {
        ElementInterface elem = (ElementInterface) elements.get(name);
        if (elem == null)
            elem = getElementByName(name);

        if (elem != null)
        {
            Class elemClass = elem.getClass();
            String className = elemClass.getSimpleName();

            if (className.equals("ElementIGBT"))
            {
                circuit.setForwardVoltage_IGBT((ElementIGBT) elem, V);
            }
            else if (className.equals("ElementDiode"))
            {
                circuit.setForwardVoltage_diode((ElementDiode) elem, V);
            }
            else if (className.equals("ElementThyristor"))
            {
                circuit.setForwardVoltage_thyristor((ElementThyristor) elem, V);
            }
            else
            {
                writeOutputLn("Cannot set forward voltage value for element " + name + " because it is of type " + className);
            }
        }
    }

    protected void setLC(String name, double value)
    {
        ElementInterface elem = (ElementInterface) elements.get(name);
        if (elem == null)
            elem = getElementByName(name);

        if (elem != null)
        {
            Class elemClass = elem.getClass();
            String className = elemClass.getSimpleName();

            if (className.equals("ElementCapacitor"))
            {
                circuit.setCapacitance_capacitor((ElementCapacitor) elem, value);
            }
            else if (className.equals("ElementInductor"))
            {
                circuit.setInductance_inductor((ElementInductor) elem, value);
            }
            else
            {
                writeOutputLn("Cannot set inductance or capacitance for element " + name + "because it is of type " + className);
            }
        }
    }

    protected void setInitLC(String name, double value)
    {
        ElementInterface elem = (ElementInterface) elements.get(name);
        if (elem == null)
            elem = getElementByName(name);

        if (elem != null)
        {
            Class elemClass = elem.getClass();
            String className = elemClass.getSimpleName();

            if (className.equals("ElementCapacitor"))
            {
                circuit.setInitialVoltage_capacitor((ElementCapacitor) elem, value);
            }
            else if (className.equals("ElementInductor"))
            {
                circuit.setInitialCurrent_inductor((ElementInductor) elem, value);
            }
            else
            {
                writeOutputLn("Cannot set initial voltage or current for element " + name + "because it is of type " + className);
            }
        }
    }

    
    

}
