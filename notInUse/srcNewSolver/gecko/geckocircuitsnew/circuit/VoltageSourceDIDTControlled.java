/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */

package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;


public class VoltageSourceDIDTControlled extends VoltageSourceControlled implements HistoryUpdatable, BStampable {

    public VoltageSourceDIDTControlled(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        assert _z > 0;
        super.stampMatrixA(matrix, dt);
        matrix[_z][_currentControl.getZValue()] += _gain / dt;

    }

    @Override
    public void stampVectorB(double[] b, double t, double dt) {
            double value = _gain * _currentControl.getCurrent() / dt;
            b[_z] += value;//_function.calculate(t, dt);
    }

    @Override
    public boolean isBasisStampable() {
        return false;
    }

    @Override
    public void registerBVector(BVector bvector) {
    }

}
