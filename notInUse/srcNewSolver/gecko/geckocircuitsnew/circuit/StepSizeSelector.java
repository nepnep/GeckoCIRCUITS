/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;
import gecko.geckocircuitsnew.circuit.MatrixSolver.SolverType;

/**
 *
 * @author anstupar
 */

//this class controls the simulation step width for variable step width simulations

public class StepSizeSelector {
    
    private TDSimulation _sim; //the simulation for which this StepSizeSelector control the time step width
    private StepSynchronizer _synchronizer; //the synchronizer that creates irregular step widths for maintaining proper behaviour of synchronized signals (e.g. rectangular/triangular signals generated from signal source)
    private double _minDt;
    private double _maxDt;
    private double _dt;
    private double _maxLTE;
    private boolean _varDt;
    private int _maxConsecutiveAttempts; //number of consecutive failures to increase step size until giving up on increasing step size further
    private int _solverOrder; //order of integration method used for simulation
    private double _LTEfactor;
    private boolean _revertToMinAfterSync; //option to revert to minimum step-width after a synchronization event
    private boolean _checkControlLTE; //whether to check control LTE or not when varying the step size
    
    //it is easiest to think of the step size selection as a series of states, like a state machine, i.e which point are we at?
    //normal execution (just go on do not touch step width), executing trial step, executing first comparison step, second comparison step, etc.
    //so create an enum which names these states, so that we always know exactly where we are
    private enum StepState { NORMAL_EXECUTION, TRIAL_STEP, COMPARISON_STEP1, COMPARISON_STEP2, SYNC_STEP }
    
    private StepState _state;
    private boolean _maxLTEreached;
    private double _trialDt;
    private double _stepsExecutedSinceLastAttempt;
    private final double _intervalSuccess = 4; //steps to execute after a successful step change, until we try again
    private final double _intervalFailure = 50; //steps to execute after an unsuccessful step change, until we try again
    private final double _intervalSwitching = 10; //steps to execute after reverting to dt_min after a switching event, before trying to increase step size again
    private double _interval;
    private int _consecutiveFailedAttempts;
    
    public StepSizeSelector(TDSimulation sim, boolean vardt, double dtmin, double dtmax, double LTEmax, int attempts, StepSynchronizer sync, SolverType solvtype, boolean revertAfterSync, boolean checkControlLTE) {
        _minDt = dtmin;
        _maxDt = dtmax;
        _sim = sim;
        _synchronizer = sync;
        _maxLTE = LTEmax;
        _state = StepState.NORMAL_EXECUTION;
        _dt = dtmin;
        _stepsExecutedSinceLastAttempt = 0;
        _interval = _intervalSuccess;
        _varDt = vardt;
        _maxConsecutiveAttempts = attempts;
        _consecutiveFailedAttempts = 0;
        if (solvtype == SolverType.BE) {
            _solverOrder = 1;
        }
        else { //GS and TRZ and methods of order 2
            _solverOrder = 2;
        }
        _LTEfactor = Math.pow(2.0,_solverOrder) / (Math.pow(2.0,_solverOrder) - 1.0);
        //System.out.println("LTE factor: " + _LTEfactor);
        _revertToMinAfterSync = vardt && revertAfterSync; //do not turn on this option if vardt option is not set
        _checkControlLTE = checkControlLTE;
    }
    
    public double determineStepSize() {
        
        double dt = _dt;
        //System.out.println(_state.toString());
        switch (_state) {
            case NORMAL_EXECUTION: 
                if (_varDt) { //if running a varying step size simulation
                    //switching events have priority
                    //when a switching event occurs, should set dt to dt_min in order to guarantee accuracy for transitions
                    if (AbstractSwitch.switchAction || Diode.diodeErrorOccurred) {
                        dt = handleSwitchingEvent();
                    }
                    //next priority are synchronization events
                    else if (_synchronizer.isThereSyncEvent()) {
                        dt = _synchronizer.getNextStep();
                        _state = StepState.SYNC_STEP; //next state is sync step state
                    }
                    //if no switching or synchronization event, check if enough steps have gone by since the last attempt at changing step size
                    //if yes, then also check whether we have had the specified number of consecutive failed attempts - if yes, do nothing
                    else if ((_stepsExecutedSinceLastAttempt >= _interval) && (_consecutiveFailedAttempts < _maxConsecutiveAttempts)) {
                        //we try a new step by doubling the current step
                        if ((2.0*_dt) <= _maxDt) {
                            _trialDt = 2.0*_dt;
                            _sim.setTrialStep(true);
                            _state = StepState.TRIAL_STEP;
                            dt = _trialDt;
                            //System.out.println("Go to trial step with step width: " + _trialDt);
                        }
                    }
                    //if not, just return current dt and go on
                    else {
                        dt = _dt;
                        _stepsExecutedSinceLastAttempt++;
                    }
                }
                else {
                    if (_synchronizer.isThereSyncEvent()) { //if not, step size is only varied for synchronization events
                        dt = _synchronizer.getNextStep();
                        _state = StepState.SYNC_STEP; //next state is sync step state
                    }
                    else { //otherwise just return the regular time step
                        dt = _dt;
                    }
                }
                break;
            case TRIAL_STEP:
                //if synchronization events occur during a trial step, we ignore them -> they are always something to do in the NEXT step,
                //but after trial step we step back anyway
                //so just clear the synchronizer queue and do nothing
                if (_synchronizer.isThereSyncEvent()) {
                    dt = _synchronizer.getNextStep();
                }
                //now we must first check if a switcing event occured during the trial step
                //if it did, since we do not yet know the LTE (since we have not done the two smaller steps), we do not know if the trial step is acceptable or not
                //therefore we should just step back
                //there is no point in doing a comparison, because the switching event anyways causes a default back to the minimum width - and to know whether we have a
                //valid result, we anyways have to do two smaller steps for comparison - extra work for nothing, since we will not use larger step anyway due to switching event
                if (AbstractSwitch.switchAction || Diode.diodeErrorOccurred) {
                    _sim.setTrialStep(false);
                    //now "nullify" effects of this step
                    AbstractSwitch.switchAction = false;
                    Diode.diodeErrorOccurred = false;
                    _sim.stepBack(_trialDt);
                    //return time step to the value before the trial step
                    dt = _dt;
                    //go back to normal execution
                    _state = StepState.NORMAL_EXECUTION;
                    //reset counter to avoid trying to change step immediately again and getting stuck in an infinite loop
                    _stepsExecutedSinceLastAttempt = 0;
                }
                //otherwise no switching event occurred, trial step is successful
                //this means we should get the control vector for comparison, step back, go to comparison step 1
                else {
                    _sim.storeTrialStepControlVector();
                    _sim.setTrialStep(false);
                    _sim.stepBack(_trialDt);
                    _state = StepState.COMPARISON_STEP1;
                    //step size should now go back to previous, for comparison steps
                    dt = _dt;
                }
                break;
            case COMPARISON_STEP1:
                //this is the first step out of the two used to determine if the trial step is valid
                //2 things can happen that can cause flow to return to normal execution and abandond attempt at changing step size:
                //1. switching event - just handle as during normal execution, set dt do dt_min, abandon step size increase
                //2. synchronization event - accept sync step, abandon trying to change step size - sync. event most likely causes switching event in the following step, so no point in changing step size
                if (AbstractSwitch.switchAction || Diode.diodeErrorOccurred) {
                    dt = handleSwitchingEvent();
                }
                else if (_synchronizer.isThereSyncEvent()) {
                    dt = _synchronizer.getNextStep();
                    _state = StepState.SYNC_STEP;
                    //reset counter to prevent immediate attempt to increase step in next step
                    _stepsExecutedSinceLastAttempt = 0;
                    //in the case that sync. steps always come at this stage (possible for certain step sizes at certain points in simulation)
                    _interval += 2;
                }
                else {
                    //time step for next is still the same dt as for this step
                    dt = _dt;
                    _state = StepState.COMPARISON_STEP2;
                }
                break;
            case COMPARISON_STEP2:
                //this is end of 2nd comparison step
                //again, if a switiching event has occurred, handle it, abandon switching step size since we must anyway revert to dt min
                if (AbstractSwitch.switchAction || Diode.diodeErrorOccurred) {
                    dt = handleSwitchingEvent();
                }
                //in all other cases, perform comparison with trial step
                else {
                    //get difference of the control values and the calculate p matrix (voltages) values for the two cases
                    double controlError = _sim.getControlError();
                    double potError = _sim.getPError();
                    //System.out.println("Circuit error: " + potError + " Control error: " + controlError);
                    //calculate local truncation error (LTE)
                    double controlLTE = _LTEfactor * controlError;
                    double LTE = _LTEfactor * potError;
                    //System.out.println("Circuit LTE: " + LTE + " Control LTE: " + controlLTE);
                    //if both errors are below threshold, then all is fine -> accept new step
                    if ((LTE <= _maxLTE) && ((_checkControlLTE && (controlLTE <= _maxLTE)) || !_checkControlLTE)) {
                        _sim.keepNewStep(_trialDt);
                        //reset failure counter since this was a success
                        _consecutiveFailedAttempts = 0;
                        //set dt to new (trial) dt
                        dt = _trialDt;
                        _dt = _trialDt;
                        _maxLTEreached = false;
                        _interval = _intervalSuccess;
                    }
                    //otherwise, reject new step
                    else {
                        if (_maxLTEreached) { //if this flag is high, this means that previously there was an attempt to change dt, and it failed
                            _consecutiveFailedAttempts++;
                        }
                        else {
                            _maxLTEreached = true; //first failure after some successes
                        }
                        //calculate new step using formula
                        dt = _trialDt * Math.pow(((0.99*_maxLTE) / Math.max(LTE,controlLTE)),(_solverOrder / (_solverOrder + 1.0)));
                        if (dt <= 1.01*_minDt) {
                            dt = _minDt;
                        }
                        _dt = dt;
                        _interval = _intervalFailure;
                    }
                    //reset counter
                    _stepsExecutedSinceLastAttempt = 0;
                    _state = StepState.NORMAL_EXECUTION;
                    //check for synchronization event
                    if (_synchronizer.isThereSyncEvent()) {
                        dt = _synchronizer.getNextStep();
                        _state = StepState.SYNC_STEP;
                    }
                }
                //System.out.println("dt after comparison: " + dt);
                break;
            case SYNC_STEP:
                //if we are in this step it means a synchronizing step has been performed
                //first check if there is a switching event
                if (_varDt && (AbstractSwitch.switchAction || Diode.diodeErrorOccurred)) {
                        dt = handleSwitchingEvent();
                }
                //if not, all which is left to is to return the regular step UNLESS _revertToMinAfterSync option is selected - then we go to the minimum step
                else if (_revertToMinAfterSync && (_dt > _minDt)) {
                    //actions like after switching event
                    _dt = _minDt;
                    _maxLTEreached = false;
                    _consecutiveFailedAttempts = 0;
                    _interval = _intervalSwitching;
                    _stepsExecutedSinceLastAttempt = 0;
                    //we should check if there is another following sync. event (this is possible)
                    //if yes, check the sync. dt - if it is less than the min. dt, perform the sync. event, otherwise, just switch to min. dt and go to normal exec.
                    //we should check if there is another, following sync. event (this is possible)
                    if (_synchronizer.isThereSyncEvent()) {
                        dt = _synchronizer.getNextStep();
                        if (dt <= 1.01*_minDt) {      
                            _state = StepState.SYNC_STEP; //next state is sync step state
                        }
                        else {
                            dt = _minDt;
                            _state = StepState.NORMAL_EXECUTION;
                        }
                    }
                    else {
                        dt = _minDt;
                        _state = StepState.NORMAL_EXECUTION;
                    }
                }
                else {
                    //we should check if there is another sync. event (possible)
                    if (_synchronizer.isThereSyncEvent()) {
                        dt = _synchronizer.getNextStep();
                        _state = StepState.SYNC_STEP;
                    }
                    else {
                        dt = _dt;
                        _state = StepState.NORMAL_EXECUTION;
                    }
                }
                
                
                break;
        }
        
        //System.out.println(_state.toString());
        //System.out.println("dt: " + dt);
        return dt;
    }
    
    private double handleSwitchingEvent() {
        double dt;
        //System.out.println("SWITCHING EVENT!");
        //check if there is also a synchronization event at the same time
        if (_synchronizer.isThereSyncEvent()) {
               dt = _synchronizer.getNextStep();
               //if requested sync. dt is less than the minimum dt, perform syncdt, but set dt to dt_min for subsequent steps
               if (dt <= 1.1*_minDt) {
                     _state = StepState.SYNC_STEP;
               }
               //otherwise, ignore the synchronization event, set dt to min dt, sync. event will come later anyway
               else {
                     dt = _minDt;
               }
        }
        else { //no simultaneous switching and synchronization event, just handle switching event
               dt = _minDt;
               _state = StepState.NORMAL_EXECUTION;
        }
        //check if current step was higher than min dt and whether we have reset to smallest step
        if (_dt > _minDt) {
               _maxLTEreached = false; //since we are resetting to lowest step, it means that later we will try to increase again, and in any case our LTE has gone down
               _dt = _minDt; //set dt to minimum
               _consecutiveFailedAttempts = 0;
        }
        //set interval in which we do not attempt step change after this
        _interval = _intervalSwitching;
        //reset counter so that we do not attempt any step size changes in that interval
        _stepsExecutedSinceLastAttempt = 0;
        Diode.diodeErrorOccurred = false;
        return dt;
    }
    
}
