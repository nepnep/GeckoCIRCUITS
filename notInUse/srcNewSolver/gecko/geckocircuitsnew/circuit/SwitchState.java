/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

/**
 *
 * @author anstupar
 */
//this class allows us to get information about a switching device's state - when it changed into the present state and what is it
public class SwitchState {
    
    public enum State {ON, OFF};
    
    private final State _state;
    private final ElementInterface _circuitElement;
    private final double _time;
    
    public SwitchState(ElementInterface elem, State switchState, double switchTime) {
        _state = switchState;
        _circuitElement = elem;
        _time = switchTime;
    }
    
    public State getState() {
        return _state;
    }
    
    public ElementInterface getElement() {
        return _circuitElement;
    }
    
    public String getElementName() {
        return _circuitElement._elementName.getValue();
    }
    
    public double getTime() {
        return _time;
    }
    
    public String toString() {
        return getElementName() + ": " + _state;
    }
    
    
}
