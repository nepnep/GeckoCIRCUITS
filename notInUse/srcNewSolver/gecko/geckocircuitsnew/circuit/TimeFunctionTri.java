/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.elements.ElementSignalSource;

/**
 *
 * @author anstupar
 */
public class TimeFunctionTri extends TimeFunction implements Synchronizable {

    public double amplitude;
    public double frequency;
    public double offset;
    public double phase;
    public double dutyratio;

    private double _dyUP;
    private double _dyDOWN;
    private double _triangle = 0;
    private boolean _ascending = true;
    
    private StepSynchronizer _synchronizer;
    private boolean _synchronized;
    
    private double _slope;
    private double _yIntrc;
    private double _nextExtreme;
    private double _regDt;

    public TimeFunctionTri(double ampl, double freq, double offs, double phas, double duty, int steps_saved) {
        amplitude = ampl;
        frequency = freq;
        offset = offs;
        phase = phas;
        dutyratio = duty;
        _steps_saved = steps_saved;
        var_history = new double[steps_saved][4];
    }

    @Override
    public double calculate(double t, double dt)
    {
        double signal;
        double timeToExtreme;

        if (saveHistory && !stepped_back && (t > 0))
        {
            historyForward();
            if (_synchronized) {
                var_history[0][0] = _slope;
                var_history[0][1] = _nextExtreme;
                var_history[0][2] = _yIntrc;
                var_history[0][3] = _regDt;
            }
            else {
                var_history[0][0] = _triangle;
                var_history[0][1] = (_ascending) ? 1:0;
            }
        }

        if (t == 0) {
            if (_synchronized)
                calculateInitialPhaseShiftSynchronized();
            else
                calculateInitialPhaseShift();
            _regDt = dt;
        }

        if (dutyratio <= 0)
            dutyratio = 1e-6;

        if (dutyratio >= 1)
            dutyratio = 1 - 1e-6;

        if (_synchronized) {
            signal = offset + _slope*t + _yIntrc;
            timeToExtreme = _nextExtreme - t;
            if (Math.abs(timeToExtreme) <= ElementSignalSource.ROUND_ERROR) {
                if (_slope > 0) { //means was ascending, reached peak, now start descending 
                    _slope = (-2.0*amplitude*frequency) / (1 - dutyratio);
                    _nextExtreme = t + (1 - dutyratio) * (1.0 / frequency);
                    _yIntrc = -1.0*amplitude - _slope*_nextExtreme;
                }
                else { //was descending, reached bottom, now start ascending
                    _slope = (2*amplitude*frequency) / dutyratio;                  
                    _nextExtreme = t + dutyratio*(1.0 / frequency);
                    _yIntrc = amplitude - _slope*_nextExtreme;
                }
                //it is possible that the new _nextExtreme point is coming at a time smaller than the time step (could happen in duty ratio is very large or very small!)
                //therefore check the previous normal (non-synchronizing) time step and place sync. request if necessary
                timeToExtreme = _nextExtreme - t;
                if ((timeToExtreme <= 1.01*_regDt) && (timeToExtreme != _regDt)) {
                    requestSyncStep(timeToExtreme);
                }
            }
            else {
                if ((timeToExtreme <= 1.01*dt) && (timeToExtreme != dt)) {
                    requestSyncStep(timeToExtreme);
                    _regDt = dt; //record what was the previous, non-synchronizing step
                }
            }
        }
        else {
            _dyUP = (amplitude * 2 * frequency * dt) / dutyratio;
            _dyDOWN = (amplitude * 2 * frequency * dt) / (1 - dutyratio);
            if (_ascending) {
                _triangle += _dyUP;
            }
            else {
                _triangle -= _dyDOWN;
            }
            if (amplitude != 0)
            {
                if (_triangle >= +amplitude) {
                    _triangle = +amplitude;
                    _ascending = false;
                }
                else if (_triangle <= -amplitude) {
                    _triangle = -amplitude;
                    _ascending = true;
                }
            }
            signal = _triangle + offset;
        }

        if (saveHistory) {
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }

        return signal;
    }

    private void calculateInitialPhaseShift()
    {
        if (dutyratio > 1)
            dutyratio = 1;
        if (dutyratio < 0)
            dutyratio = 0;

        double tx = 0;
        double txEnd = 1.0 / frequency;
        double dtx = txEnd / 1e3;
        double phaseX = phase;

        while (phaseX > (2 * Math.PI))
            phaseX -= (2 * Math.PI);

        while (phaseX < 0)
            phaseX += (2 * Math.PI);

        _triangle = 0;
        _ascending = true;

        while (tx < (txEnd * phaseX / (2 * Math.PI))) {
            double dyUPx = (amplitude * 2 * frequency * dtx) / dutyratio;
            double dyDOWNx = (amplitude * 2 * frequency * dtx) / (1 - dutyratio);
            if (_ascending) {
                _triangle += dyUPx;
            }
            else
            {
                _triangle -= dyDOWNx;
            }
            if (amplitude != 0) {
                if (_triangle >= +amplitude) {
                    _triangle = +amplitude;
                    _ascending = false;
                }
                else if (_triangle <= -amplitude) {
                    _triangle = -amplitude;
                    _ascending = true;
                }
            }
            tx += dtx;
       }
       _triangle = -_triangle;
    }
    
    private void calculateInitialPhaseShiftSynchronized() {
        
        double phaseX = phase;
        if (phase >= 2.0*Math.PI) {
            phaseX = phase - 2.0*Math.PI;
        }
        else if (phase <= -2.0*Math.PI) {
            phaseX = phase + 2.0*Math.PI;
        }
        //convert always to positive phase
        if (phaseX < 0) {
            phaseX = 2.0*Math.PI + phaseX;
        }
        
        if ((phaseX / (2.0*Math.PI)) <= (0.5*dutyratio)) {
            //initially ascending
            _slope = (2*amplitude*frequency) / dutyratio;
            _nextExtreme = (0.5*dutyratio + (phaseX / (2.0*Math.PI)))*(1.0 / frequency);
            _yIntrc = amplitude - _slope*_nextExtreme;
        }
        else {
            //initially descending
            _slope = (-2.0*amplitude*frequency) / (1 - dutyratio);
            _nextExtreme = ((phaseX / (2.0*Math.PI)) - 0.5*dutyratio)*(1.0 / frequency);
            _yIntrc = -1.0*amplitude - _slope*_nextExtreme;
        }
        
            
        
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < _steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            if (_synchronized) {
                _slope = var_history[0][0];
                _nextExtreme = var_history[0][1];
                _yIntrc = var_history[0][2];
                _regDt = var_history[0][3];
            }
            else {
                _triangle = var_history[0][0];
                _ascending = (((int)var_history[0][1]) == 1);
            }
            stepped_back = true;
            steps_reversed++;
        }
    }
    
    //synchronization functions
    public void addSynchronizer(StepSynchronizer syncer) {
        _synchronizer = syncer;
    }
    
    public void makeSynchronizable(boolean sync) {
        _synchronized = sync;
    }
    
    private void requestSyncStep(double dtSync) {
        _synchronizer.requestSyncStep(dtSync);
    }

}
