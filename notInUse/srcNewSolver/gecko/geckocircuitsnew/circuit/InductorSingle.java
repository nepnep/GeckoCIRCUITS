/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.geckocircuitsnew.circuit.MatrixSolver.SolverType;

/**
 *
 * @author andy
 */
public class InductorSingle extends Inductor implements AStampable, CurrentCalculatable {

    public InductorSingle(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        //double aW = dt / _inductance;  //  +dt/L
        
        double aW = 0;
        if (_element.getSolverType() == SolverType.BE)
                aW = dt / _inductance;
        else if (_element.getSolverType() == SolverType.TRZ)
                aW = 0.5 * dt / _inductance;
        else if (_element.getSolverType() == SolverType.GS)
                aW = (2.0 / 3.0) * dt / _inductance;

        matrix[matrixIndices[0]][matrixIndices[0]] += (+aW);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+aW);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-aW);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-aW);
        
    }
   
    

    @Override
    public void calculateCurrent(double[] p, double dt, double t) {
        //_current = +_oldCurrent + dt / _inductance * (p[matrixIndices[0]] - p[matrixIndices[1]]);
        if (_element.getSolverType() == SolverType.BE)
             _current = +_oldCurrent + dt / _inductance * (p[matrixIndices[0]] - p[matrixIndices[1]]);   
        else if (_element.getSolverType() == SolverType.TRZ)
             _current = +_oldCurrent + dt / (2 * _inductance) * ((p[matrixIndices[0]] - p[matrixIndices[1]]) + (_potential1 - _potential2));
        else if (_element.getSolverType() == SolverType.GS)
             _current = (2.0 / 3.0) * dt / _inductance * (p[matrixIndices[0]] - p[matrixIndices[1]]) + (4.0 / 3.0) * _oldCurrent - (1.0 / 3.0) * _oldOldCurrent; 
    }
    
    

}
