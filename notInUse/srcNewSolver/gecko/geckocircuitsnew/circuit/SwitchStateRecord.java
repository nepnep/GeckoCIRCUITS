/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;
import java.util.TreeMap;
import java.util.ArrayList;
import geckocircuitsnew.Model;
import java.util.Map.Entry;

/**
 *
 * @author anstupar
 */
//this class allows us to keep track of all switch states within a simulation
public class SwitchStateRecord {
    
    //use a Tree Map - so we can have an ordered map
    private final TreeMap<Double, ArrayList<SwitchState>> _stateMap;
    final Model _circuitModel;
    
    public SwitchStateRecord(Model circuitModel) {
        _stateMap = new TreeMap<Double, ArrayList<SwitchState>>();
        _circuitModel = circuitModel;
    }
    
    //re-initialize - clear all contents
    public void reInit() {
        _stateMap.clear();
    }
    
    public void addSwitchState(SwitchState state) {
        //get time at which change of state ocurred / this state was recorded
        double time = state.getTime();
        //get the list in the map which contains the switch states for this time
        ArrayList<SwitchState> stateListForTime = _stateMap.get(time);
        //if null, doesn't exist yet -> create
        if (stateListForTime == null) {
            stateListForTime = new ArrayList<SwitchState>();
            stateListForTime.add(state);
            _stateMap.put(time,stateListForTime);
        }
        else { //exist -> add to existing list
            stateListForTime.add(state);
        }
        
    }
    
    public ArrayList<ArrayList<SwitchState>> getOrderedListOfCircuitStates() {
        ArrayList<ArrayList<SwitchState>> listOfCircuitStates = new ArrayList<ArrayList<SwitchState>>();
        for (Entry<Double, ArrayList<SwitchState>> entry : _stateMap.entrySet()) {
            listOfCircuitStates.add(entry.getValue());
        }
        
        //int index;
        boolean differentStateFound;
        ArrayList<SwitchState> state, nextState;
        /*traverse list to remove reduntant states - can be caused by diode errors that do not ultimately change diode state*/
        for (int index = 0; index < listOfCircuitStates.size(); index++) {
            state = listOfCircuitStates.get(index);
            //index = listOfCircuitStates.indexOf(state);
            if (index+1 < listOfCircuitStates.size()) {
                differentStateFound = false;
                while (!differentStateFound && index+1 < listOfCircuitStates.size()) {
                    nextState = listOfCircuitStates.get(index+1);
                    differentStateFound = !(compareStates(state,nextState));
                    if (!differentStateFound) {
                        listOfCircuitStates.remove(index+1);
                    }
                }
            }
        }
        
        return listOfCircuitStates;
    }
    
    public boolean compareStates(ArrayList<SwitchState> state1, ArrayList<SwitchState> state2) {
        
        boolean equal = true;
        SwitchState deviceState1, deviceState2;
        
        if (state1.size() != state2.size()) {
            return false;
        }
        
        for (int index = 0; index < state1.size() - 1; index++) {
            deviceState1 = state1.get(0);
            deviceState2 = state2.get(0);
            if (deviceState1.getState() != deviceState2.getState()) {
                equal = false;
                break;
            }
        }
        return equal;
    }
    
    
}
