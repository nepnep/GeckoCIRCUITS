/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.geckocircuitsnew.animation.CurrentTopology;
import gecko.geckocircuitsnew.animation.CurrentTopologyContainer;
import gecko.geckocircuitsnew.animation.AbstractAnimate;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.util.*;

/**
 *
 * @author andy
 */
public final class MatrixSolver {

    private int _NAMatrix;
    private final List<AStampable> _AStampables = new ArrayList<AStampable>();
    private final List<CircuitComponent> _allAnimateComponents = new ArrayList<CircuitComponent>();
    private final List<BStampable> _BStampables = new ArrayList<BStampable>();
    private final List<CurrentCalculatable> _currentCalculatables = new ArrayList<CurrentCalculatable>();
    private final List<HistoryUpdatable> _historyUpdatables = new ArrayList<HistoryUpdatable>();
    private final List<DirectCurrentCalculatable> _zValuables = new ArrayList<DirectCurrentCalculatable>();
    private final List<Capacitor> _capacitors = new ArrayList<Capacitor>();
    private double[][] _aArray;
    private double[][] _LUDecomp;
    private BVector _bVector;
    private double[] _pVector;
    private int[] _pivots;
    private int _NAMatrixSub1;
    private double _dt;
    private double _prevDt;
    private double _time;
    private final int _potentialNumber;
    private final List _cComponents;
    private boolean _saveHistory = false;
    private boolean _trialStep = false;
    private double[][] _aArrayCopy;
    private double[][] _LUDecompCopy;
    private double[] _pVectorCopy;
    private int[] _pivCopy;
    private BVector _bVectorCopy;
    private int _dampingType;
    private double _dampingFactor;
    // Andy: value changed a 18. June, since the Vienna-Rectifier-Example (10 kw_thermal) made problems
    private static final double STOER_RED_FACTOR = 0.9999;
    private static final double P_VECTOR_ERROR_THRESHOLD = 100.0;
    private final CurrentTopologyContainer _TopologyContainer;

    //define types of solvers
    //BE - Backwards Euler
    //TRZ - Trapezoidal
    //GS - Gear-Schihman a.k.a Gear-2
    public enum SolverType {

        BE, TRZ, GS
    };

    public MatrixSolver(final List cComponents, final int potentials, final CurrentTopologyContainer cTPLContainer) {
        
        for (Object comp : cComponents) {
            
            if (comp instanceof AStampable) {
                _AStampables.add((AStampable) comp);
                _allAnimateComponents.add((CircuitComponent) comp);
            
            }

            if (comp instanceof CurrentCalculatable) {
                _currentCalculatables.add((CurrentCalculatable) comp);
            }

            if (comp instanceof HistoryUpdatable) {
                _historyUpdatables.add((HistoryUpdatable) comp);
            }

            if (comp instanceof BStampable) {
                _BStampables.add((BStampable) comp);
            }

            if (comp instanceof Capacitor) {
                _capacitors.add((Capacitor) comp);
            }
        }
        _cComponents = cComponents;
        _potentialNumber = potentials;


        _TopologyContainer = cTPLContainer;

    }

    public void setDampingFactor(final double factor) {
        _dampingFactor = factor;
        _LUDecomp = null;
    }

    private void sortZValuables() {

        _zValuables.clear();

        for (Object cComponent : _cComponents) {

            if (cComponent instanceof Capacitor) {
                final Capacitor cap = (Capacitor) cComponent;
                if (Capacitor.initCapacitor && cap.getInitialValue() != 0) {
                    _zValuables.add((DirectCurrentCalculatable) cap);
                }
                continue;
            }

            if (cComponent instanceof DirectCurrentCalculatable) {
                _zValuables.add((DirectCurrentCalculatable) cComponent);

            }
        }

        for (DirectCurrentCalculatable zVal : _zValuables) {
            zVal.setZValue(_potentialNumber + _zValuables.indexOf(zVal));
        }


    }

    public void stampMatrixA(final double deltaT) {
        _aArray = new double[_NAMatrix][_NAMatrix];
        for (int i = 0; i < _aArray.length; i++) {
            for (int j = 0; j < _aArray.length; j++) {
                _aArray[i][j] = 0;
            }
        }


        for (AStampable astampable : _AStampables) {
            astampable.stampMatrixA(_aArray, deltaT);
        }


//        for(int i = 0; i < aArray.length; i++)
//        {
//            for(int j = 0; j < aArray[0].length; j++) {
//                System.out.print(aArray[i][j] + " ");
//            }
//            System.out.println("");
//        }

        _LUDecomp = luDecomposition(_aArray);
    }

    public void calculateInitialState(final double deltaT) {

        Capacitor.initCapacitor = true;
        sortZValuables();

        _NAMatrix = _potentialNumber + _zValuables.size();

        _bVector = new BVector(_NAMatrix, _BStampables);
        _pVector = new double[_NAMatrix];
        _time = -1;
        _dt = deltaT;
        _prevDt = _dt;
        setupAndSolve();

        calculateComponentCurrents();

        iterateDiodeErrors();
        updatePotentials();

        Capacitor.initCapacitor = false;
        sortZValuables();
        _NAMatrix = _potentialNumber + _zValuables.size();

        _pVector = new double[_NAMatrix];
        _bVector = new BVector(_NAMatrix, _BStampables);
        initCurrentTopology();

    }

    private void saveHistories() {
        for (Object o : _cComponents) {
            if (o instanceof CircuitComponent) {
                final CircuitComponent comp = (CircuitComponent) o;
                comp.saveHistory();
            }
        }
    }

    public void calculateComponentCurrents() {

        for (CurrentCalculatable comp : _currentCalculatables) {
            comp.calculateCurrent(_pVector, _dt, _time);
        }

    }

    private void maintainHistories(final double time) {
        for (Object obj : _cComponents) {
            if (obj instanceof CircuitComponent) {
                final CircuitComponent comp = (CircuitComponent) obj;
                comp.historyMaintainance(time);
            }
        }
    }

    public void solveTimeStep(final double time, final double deltaT) {
        _time = time;
        _dt = deltaT;
        if (_trialStep) {
            makeCopies();
            //System.out.println("trial step at " + time + " dt = " + deltaT);
        }
        setupAndSolve();
        if (_saveHistory) {
            saveHistories();
            //System.out.println("save component history");
        }
        calculateComponentCurrents();

        if (Capacitor.capError) {
            //occurs after calculateComponentCurrents only in the special case where current is 
            // miscalculated to cross the axis (see Capacitor class)
            fixCapacitorError();
        }
        //System.out.println("iterate diode errors");
        iterateDiodeErrors();
        if (_saveHistory) {
            maintainHistories(time);
        }
        //System.out.println("diode errors iterated");
        updatePotentials();

        if (_TopologyContainer != null) {
            currentVisualization();
        }



        /*
         * if (_trialStep) { System.out.println("Results of trial step: "); for (int i = 0; i < _pVector.length; i++) {
         * System.out.println("pVector["+i+"] = " + _pVector[i]); } } else { System.out.println("Results of normal step: "); for
         * (int i = 0; i < _pVector.length; i++) { System.out.println("pVector["+i+"] = " + _pVector[i]); } for (int i = 0; i <
         * _bVector.b.length; i++) { System.out.println("bVector["+i+"] = " + _bVector.b[i]); } }
         */


    }

    private void fixCapacitorError() {
        _LUDecomp = null;
        setupAndSolve();
    }

    public void iterateDiodeErrors() {
        Diode.disturbanceValue = 1;

        while (Diode.diodeSwitchError) {
            CircuitComponent.disturbanceValue *= STOER_RED_FACTOR;
            Diode.diodeSwitchError = false;
            Diode.inSwitchErrorMode = true;
            _LUDecomp = null;
            setupAndSolve();
            calculateComponentCurrents();
            Diode.inSwitchErrorMode = false;
            Diode.diodeErrorOccurred = true;
            //System.out.println("diodeErrorOccurred set to true at: " + _time);
        }
    }

    public void setupAndSolve() {
        if (!Capacitor.capError) { //avoid this step if calling fixCapacitorError    
            updateNonLinearCapacitances();
        }

        if (AbstractSwitch.switchAction || (_time <= 0) || (_LUDecomp == null)
                || _trialStep || (_prevDt != _dt) || Capacitor.capError) {
            stampMatrixA(_dt);
            //System.out.println("stamp matrix A");
            if (AbstractSwitch.switchAction) {
                AbstractSwitch.switchActionOccurred = true;
            }
            AbstractSwitch.switchAction = false;
            Capacitor.capError = false;
            if (!_trialStep) { //comparison of prev_dt and dt is for step back to dt_min due to switching, 
                // does not apply for trial step            
                _prevDt = _dt;
            }
            /*
             * for (int i = 0; i < _aArray.length; i++) { for (int j = 0; j < _aArray[i].length; j++)
             * System.out.print(_aArray[i][j] + " "); System.out.print("\n"); }
             */
        }

        _bVector.stampBVector(_time, _dt);
        /*
         * System.out.println("B vector:"); for (int i = 0; i < _bVector.b.length; i++) System.out.print(_bVector.b[i] + " ");
         * System.out.print("\n");
         */

        final double[] bVectorGndRemoved = new double[_NAMatrixSub1];
        System.arraycopy(_bVector.b, 1, bVectorGndRemoved, 0, _NAMatrixSub1);

        // TODO remove later:
        //for (int i = 1; i < _NAMatrix; i++) {            
        //    bVectorGndRemoved[i - 1] = _bVector.b[i];
        //}

        _pVector = solve(bVectorGndRemoved);
        /*
         * System.out.println("P vector:"); for (int i = 0; i < _pVector.length; i++) System.out.print(_pVector[i] + " ");
         * System.out.print("\n");
         */
    }

    private void updateNonLinearCapacitances() {
        for (Capacitor cap : _capacitors) {
            if (cap.isNonLinear()) {
                cap.updateNonLinearCapacitance();
            }
        }
    }

    private double[][] luDecomposition(final double[][] aMatrix) {

        // Use a "left-looking", dot-product, Crout/Doolittle algorithm.
        assert aMatrix.length == aMatrix[0].length;
        _NAMatrixSub1 = aMatrix[0].length - 1;


        final double[][] luDecomp = new double[aMatrix.length - 1][aMatrix[0].length - 1];

        for (int i = 0; i < _NAMatrixSub1; i++) {
            for (int j = 0; j < _NAMatrixSub1; j++) {
                luDecomp[i][j] = aMatrix[i + 1][j + 1];
            }
        }

        _pivots = new int[_NAMatrixSub1];
        for (int i = 0; i < _NAMatrixSub1; i++) {
            _pivots[i] = i;
        }

        int pivsign = 1;
        double[] luRowJ;
        double[] luColJ = new double[_NAMatrixSub1];

        // Outer loop.

        for (int j = 0; j < _NAMatrixSub1; j++) {

            // Make a copy of the j-th column to localize references.

            for (int i = 0; i < _NAMatrixSub1; i++) {
                luColJ[i] = luDecomp[i][j];
            }

            // Apply previous transformations.

            for (int i = 0; i < _NAMatrixSub1; i++) {
                luRowJ = luDecomp[i];

                // Most of the time is spent in the following dot product.

                int kmax = Math.min(i, j);
                double s = 0.0;
                for (int k = 0; k < kmax; k++) {
                    s += luRowJ[k] * luColJ[k];
                }

                luRowJ[j] = luColJ[i] -= s;
            }

            // Find pivot and exchange if necessary.

            int p = j;
            for (int i = j + 1; i < _NAMatrixSub1; i++) {
                if (Math.abs(luColJ[i]) > Math.abs(luColJ[p])) {
                    p = i;
                }
            }
            if (p != j) {
                for (int k = 0; k < _NAMatrixSub1; k++) {
                    double t = luDecomp[p][k];
                    luDecomp[p][k] = luDecomp[j][k];
                    luDecomp[j][k] = t;
                }
                int k = _pivots[p];
                _pivots[p] = _pivots[j];
                _pivots[j] = k;
                pivsign = -pivsign;
            }

            // Compute multipliers.

            if (j < _NAMatrixSub1 & luDecomp[j][j] != 0.0) {
                for (int i = j + 1; i < _NAMatrixSub1; i++) {
                    luDecomp[i][j] /= luDecomp[j][j];
                }
            }
        }

        for (int j = 0; j < luDecomp.length; j++) {
            if (luDecomp[j][j] == 0) {
                throw new RuntimeException("Matrix is singular.");
            }
        }

        return luDecomp;
    }

    public double[] solve(final double[] bVector) {

        final double[] xVector = new double[_pivots.length + 1];
        try {
            for (int i = 0; i < _pivots.length; i++) {
                xVector[i + 1] = bVector[_pivots[i]];
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("Submatrix indices");
        }

        // Solve L*Y = B(piv,:)
        for (int k = 0; k < _NAMatrixSub1; k++) {
            for (int i = k + 1; i < _NAMatrixSub1; i++) {
                xVector[i + 1] -= xVector[k + 1] * _LUDecomp[i][k];
            }
        }
        // Solve U*X = Y;
        for (int k = _NAMatrixSub1 - 1; k >= 0; k--) {
            xVector[k + 1] /= _LUDecomp[k][k];
            for (int i = 0; i < k; i++) {
                xVector[i + 1] -= xVector[k + 1] * _LUDecomp[i][k];
            }
        }
        return xVector;
    }

    private void updatePotentials() {
        for (HistoryUpdatable hist : _historyUpdatables) {
            hist.updateHistory(_pVector);
        }

    }

    private void copyMatrix(final double[][] orig, final double[][] copy) {
        for (int i = 0; i < orig.length; i++) {
            System.arraycopy(orig[i], 0, copy[i], 0, orig[i].length);
        }
    }

    private void makeCopies() {
        _pVectorCopy = new double[_pVector.length];
        _pivCopy = new int[_pivots.length];
        System.arraycopy(_pVector, 0, _pVectorCopy, 0, _pVector.length);
        System.arraycopy(_pivots, 0, _pivCopy, 0, _pivots.length);

        _aArrayCopy = new double[_NAMatrix][_NAMatrix];
        _LUDecompCopy = new double[_aArray.length - 1][_aArray[0].length - 1];

        copyMatrix(_aArray, _aArrayCopy);
        copyMatrix(_LUDecomp, _LUDecompCopy);

        _bVectorCopy = _bVector.copy();

        /*
         * System.out.println("made copies of matrix solver vectors"); for (int i = 0; i < _pVectorCopy.length; i++) {
         * System.out.println("pVectorCopy["+i+"] = " + _pVectorCopy[i]); }
         */

    }

    public void stepBack() {
        //System.out.println("step back called");
        final int[] pivNewStep = _pivots;
        final double[] pNewStep = _pVector;
        final BVector bNewStep = _bVector;
        final double[][] aNewStep = _aArray;
        final double[][] luNewStep = _LUDecomp;

        _pivots = _pivCopy;
        _pVector = _pVectorCopy;
        _bVector = _bVectorCopy;
        _aArray = _aArrayCopy;
        _LUDecomp = _LUDecompCopy;
        _bVector.registerBVector();

        _pivCopy = pivNewStep;
        _pVectorCopy = pNewStep;
        _bVectorCopy = bNewStep;
        _aArrayCopy = aNewStep;
        _LUDecompCopy = luNewStep;

        //assert _p != _p_copy;
        //assert _LUDecomp != _LUDecomp_copy;

        /*
         * for (int i = 0; i < _pVector.length; i++) { System.out.println("pVector["+i+"] = " + _pVector[i]); } for (int i = 0; i
         * < _pVectorCopy.length; i++) { System.out.println("pVectorCopy["+i+"] = " + _pVectorCopy[i]); }
         *
         * for (int i = 0; i < _aArray.length; i++) { for (int j = 0; j < _aArray[i].length; j++) {
         * System.out.println("aArray["+i+"]["+j+"] = " + _aArray[i][j]); } } for (int i = 0; i < _aArrayCopy.length; i++) { for
         * (int j = 0; j < _aArrayCopy[i].length; j++) { System.out.println("aArrayCopy["+i+"]["+j+"] = " + _aArrayCopy[i][j]); }
         * }
         *
         * for (int i = 0; i < _LUDecomp.length; i++) { for (int j = 0; j < _LUDecomp[i].length; j++) {
         * System.out.println("LUDecomp["+i+"]["+j+"] = " + _LUDecomp[i][j]); } } for (int i = 0; i < _LUDecompCopy.length; i++) {
         * for (int j = 0; j < _LUDecomp[i].length; j++) { System.out.println("LUDecompCopy["+i+"]["+j+"] = " +
         * _LUDecompCopy[i][j]); } }
         *
         * for (int i = 0; i < _bVector.b.length; i++) { System.out.println("bVector["+i+"] = " + _bVector.b[i]); } for (int i =
         * 0; i < _bVectorCopy.b.length; i++) { System.out.println("bVectorCopy["+i+"] = " + _bVectorCopy.b[i]); }
         */

        for (Object o : _cComponents) {
            if (o instanceof CircuitComponent) {
                final CircuitComponent comp = (CircuitComponent) o;
                comp.stepBack();
            }
        }
    }

    public void stepBackComponents() {
        for (Object o : _cComponents) {
            if (o instanceof CircuitComponent) {
                final CircuitComponent comp = (CircuitComponent) o;
                comp.stepBack();
            }
        }
    }

    public void keepNewStep(final double deltaT) {
        //save ONLY THESE to avoid stamping the A matrix and calculating the LU decomp. for the new step
        _pivots = _pivCopy;
        _aArray = _aArrayCopy;
        _LUDecomp = _LUDecompCopy;
        _dt = deltaT;
        _prevDt = deltaT;
    }

    public double findPVectorError() {
        double error = 0;
        double currentError;
        for (int i = 0; i < _pVector.length; i++) {
            //if using % error
            //currentError = Math.abs((_pVector[i] - _pVectorCopy[i]) / _pVector[i]) * P_VECTOR_ERROR_THRESHOLD;

            //if using LTE
            //current_error = 2.0*Math.abs(_p[i] - _p_copy[i]);
            currentError = Math.abs(_pVector[i] - _pVectorCopy[i]);
            //System.out.println("current error: " + currentError);
            //System.out.println("_p["+i+"] = " + _pVector[i] + "; _p_copy["+i+"] = " + _pVectorCopy[i]);
            if (currentError > error) {
                error = currentError;
            }
        }
        //System.out.println("error: " + error);
        return error;
    }

    public void setSaveHistory(final boolean value) {
        _saveHistory = value;
    }

    public void setTrialStep(final boolean value) {
        _trialStep = value;
    }

    private void initCurrentTopology() {
        _TopologyContainer.initTopologyContainer(_allAnimateComponents);
//        _TopologyContainer.initTopologyContainer(_currentCalculatables);
//        for (CircuitComponent comp : _TopologyContainer._components) {
//            List<Terminal> terms = comp.getTerminals();
//            System.out.println("Terminals: " + comp.getOwningElementName() + ":\t X:" + terms.get(0).getRelX() + "\t Y:" + terms.get(0).getRelY());
//            System.out.println("\t\t X:" + terms.get(1).getRelX() + "\t Y:" + terms.get(1).getRelY());
//        }


//        _TopologyContainer.printPotentialAreas();
        //_TopologyContainer.printTerminals();


    }

    private void currentVisualization() {
        CurrentTopology.createIfNotPresentFabric(_TopologyContainer, _time);
    }
}
