/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;


import geckocircuitsnew.DatenSpeicher;
import java.util.StringTokenizer;
import java.io.Serializable;




// Datenbehaelter fuer eine Messkurve -->
public class SchaltverlusteMesskurve implements Serializable {

    //-----------------------------
    private String kurvenName;
    private int anzPunkte;
    private double[][] data;
    private double tj, uBlock;
    //-----------------------------


    // Datenbehaelter mit folgendem Format fuer  data[][] -->
    // I [A] - Eon [Ws] - Eoff [Ws] - Etotal
    // ..      ..         ..          ..
    // ..      ..         ..          ..
    // usw.
    // Parameter: T_junction, uBlock --> bei der Messung vorgegeben
    //
    public SchaltverlusteMesskurve (double tj, double uBlock) {
        anzPunkte=0;   data= null;  // noch keine Daten vorhanden
        this.tj=tj;    this.uBlock=uBlock;   this.kurvenName= ((int)tj)+"°C @ "+((int)uBlock)+"V";
    }

    public SchaltverlusteMesskurve copy () {
        SchaltverlusteMesskurve copy= new SchaltverlusteMesskurve(-1,-1);
        copy.kurvenName=this.kurvenName;   copy.anzPunkte=this.anzPunkte;
        copy.data= new double[this.data.length][this.data[0].length];
        for (int i1=0;  i1<this.data.length;  i1++) for (int i2=0;  i2<this.data[0].length;  i2++) copy.data[i1][i2]= this.data[i1][i2];
        copy.tj = this.tj;    copy.uBlock = this.uBlock;
        return copy;
    }


    public void addData (double[][] data) { this.data=data; }

    public double[][] getData () { return data; }
    public double getTj () { return tj; }
    public double getUBlock () { return uBlock; }
    public String getName () { return kurvenName; }





    //===============================================================================
    //===============================================================================
    // Testdaten -->

    public static SchaltverlusteMesskurve createTestKurve (int nr) {
        SchaltverlusteMesskurve k= new SchaltverlusteMesskurve(-1,-1);
        switch (nr) {
            case 1:
                k.tj= 11;   k.uBlock=100;
                k.kurvenName= ((int)k.tj)+"°C @ "+((int)k.uBlock)+"V";
                double[] iMess=    new double[]{10,20,30,40,50};
                double[] eONmess=  new double[]{0.22e-3,0.42e-3,0.65e-3,0.77e-3,0.99e-3};
                double[] eOFFmess= new double[]{0.11e-3,0.25e-3,0.33e-3,0.40e-3,0.44e-3};
                k.anzPunkte= iMess.length;
                double[] eGESmess= new double[k.anzPunkte];
                for (int i1=0;  i1<k.anzPunkte;  i1++)  eGESmess[i1]= eONmess[i1]+eOFFmess[i1];
                k.data= new double[][]{iMess,eONmess,eOFFmess,eGESmess};
                break;
            case 2:
                k.tj= 22;   k.uBlock=200;
                k.kurvenName= ((int)k.tj)+"°C @ "+((int)k.uBlock)+"V";
                iMess=    new double[]{7,9,23,45,55};
                eONmess=  new double[]{0.13e-3,0.18e-3,0.43e-3,0.81e-3,1.10e-3};
                eOFFmess= new double[]{0.09e-3,0.14e-3,0.31e-3,0.54e-3,0.67e-3};
                k.anzPunkte= iMess.length;
                eGESmess= new double[k.anzPunkte];
                for (int i1=0;  i1<k.anzPunkte;  i1++)  eGESmess[i1]= eONmess[i1]+eOFFmess[i1];
                k.data= new double[][]{iMess,eONmess,eOFFmess,eGESmess};
                break;
            case 3:
                k.tj= 33;   k.uBlock=300;
                k.kurvenName= ((int)k.tj)+"°C @ "+((int)k.uBlock)+"V";
                iMess=    new double[]{20,40,70};
                eONmess=  new double[]{0.40e-3,0.90e-3,2.00e-3};
                eOFFmess= new double[]{0.36e-3,0.70e-3,1.65e-3};
                k.anzPunkte= iMess.length;
                eGESmess= new double[k.anzPunkte];
                for (int i1=0;  i1<k.anzPunkte;  i1++)  eGESmess[i1]= eONmess[i1]+eOFFmess[i1];
                k.data= new double[][]{iMess,eONmess,eOFFmess,eGESmess};
                break;
            default: System.out.println("Ungueltige Testkurven-Nummer! (derzeit nur '1', '2', '3' implementiert)");  break;
        }
        return k;
    }


    //===============================================================================
    //===============================================================================

    // zum Speichern im ASCII-Format (anstatt als Object-Stream) -->
    //
    protected void exportASCII (StringBuffer ascii) {

    }
    public void importASCII (String asciiBlock) {
        StringTokenizer stk= null;
        String zz= "";
        String[] ascii= DatenSpeicher.makeStringArray(asciiBlock);
        //------------------
        for (int i1=0;  i1<ascii.length;  i1++) {
            if (ascii[i1].startsWith("anzPunkte "))  anzPunkte= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("tj ")) tj= DatenSpeicher.leseASCII_double(ascii[i1]);
            if (ascii[i1].startsWith("uBlock ")) uBlock= DatenSpeicher.leseASCII_double(ascii[i1]);
            if (ascii[i1].startsWith("data[][] ")) data= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
        }
        //------------------
        kurvenName= ((int)tj)+"°C @ "+((int)uBlock)+"V";
        //------------------
    }




}




