/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.geckocircuitsnew.animation.CurrentTopologyNode;
import gecko.geckocircuitsnew.animation.CurrentTopologyEdge;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Zimmi
 */
public final class CurrentPathMeter {

    private final CurrentTopologyEdge _edge;
    private final CurrentTopologyNode _node;
    private final Map<CircuitComponent, Integer> _components = new LinkedHashMap<CircuitComponent, Integer>();

    public CurrentPathMeter(final CurrentTopologyEdge curEdge, final CurrentTopologyNode node, List<CurrentTopologyEdge> dependingEdges) {
        _edge = curEdge;
        _node = node;

        if (dependingEdges == null) {
            dependingEdges = new ArrayList<CurrentTopologyEdge>();
        }

        dependingEdges.add(_edge);

        // Gather dependency information
        for (CurrentTopologyEdge edge : node.getEdges()) {
            if ((edge == null) || (curEdge.equals(edge))) {
                continue;
            }



            if (edge.getDomPos() > -1) {
                // Add dominat Component
                _components.put(edge.getDominantComponent(), calacuteDirection(edge, node));
            } else {
                // There is no dominat Component -> new "spreading" needed
                if (!(dependingEdges.contains(edge))) {
                    dependingEdges.add(edge);
                    edge.printPath();
                    
                    final CurrentPathMeter newPathMeter = new CurrentPathMeter(edge, edge.getOtherNode(node), dependingEdges);

                    for (Map.Entry<CircuitComponent, Integer> entry : newPathMeter._components.entrySet()) {
                        _components.put(entry.getKey(), entry.getValue());
                    }
                }
            }
        }
    }

    public void printPathMeter() {
        System.out.print("\tPathMeter @\t" + _edge.getNode(0).getPos() + "\t -->\t");
        System.out.print(_edge.getNode(1).getPos() + " \t mit Knotengleichung:\t");

        for (Map.Entry<CircuitComponent, Integer> entry : _components.entrySet()) {
            if (entry.getValue() == 1) {
                System.out.print(" + ");
            } else {
                System.out.print(" - ");
            }
            System.out.print(entry.getKey().getOwningElementName());
        }
        System.out.println("");
    }

    public Map<CircuitComponent, Integer> getComponents() {
        return _components;
    }

    private int calacuteDirection(final CurrentTopologyEdge edge, final CurrentTopologyNode node) {
        int direction;

        if (node.getPos().equals(edge.getNode(0).getPos())) {
            direction = -1;
        } else {
            direction = 1;
        }

        if (edge.isReversed()) {
            direction = direction * -1;
        }


        return direction;
    }

    public double calculateCurrent() {
        double total = 0;
        for (Map.Entry<CircuitComponent, Integer> entry : _components.entrySet()) {

            total = total + entry.getValue() * entry.getKey().getCurrent();
        }
        return total;
    }

    public CurrentTopologyEdge getEdge() {
        return _edge;
    }
}
