/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.allg.Typ;
import geckocircuitsnew.DatenSpeicher;
import geckocircuitsnew.Main;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

/**
 *
 * @author andy
 */
public class VerlustBerechnung {

    private static final double EPS = 1e-2;  // praktisch Null --> Schwelle zur Umgehung numerischer Minimalfehler
    private String datnamGemesseneVerluste;
    private double Cosser;
    private double uSWnorm;
    private double kOFF;
    private double kON;
    private double uf;
    private double rON;
    private int verlustTyp;
    private int anzMesskurvenPvSWITCH;
    private int anzMesskurvenPvCOND;
    private SchaltverlusteMesskurve[] messkurvePvSWITCH;
    private LeitverlusteMesskurve[] messkurvePvCOND;
    private double[] tjGrenzenCOND;
    private double[] b0COND;
    private double[] b1COND;
    private double[] c0COND;
    private double[] c1COND;
    private double[] d0COND;
    private double[] d1COND;
    private double[] tjGrenzenSW;
    private double[] b0onSW;
    private double[] b1onSW;
    private double[] c0onSW;
    private double[] c1onSW;
    private double[] d0onSW;
    private double[] d1onSW;
    private double[] b0offSW;
    private double[] b1offSW;
    private double[] c0offSW;
    private double[] c1offSW;
    private double[] d0offSW;
    private double[] d1offSW;
    private double i_alt = -1, u_alt = -1;  // Werte ein Zeitschritt vorher --> Detektion eines Schaltvorgangs fuer Schaltverluste
    public static final int VERLUST_TYP_SIMPEL = 1;
    public static final int VERLUST_TYP_DETAIL = 2;
    // What is the state of the switch? ON (=1) or OFF (=0)?
    private int state = 0, stateOld = 0, stateOldOld = 0;

    public VerlustBerechnung(final String[] ascii, final int lineCounter) {
        // alle folgenden ascii[]-Zeilen parametrisieren die VerlustBerechnungs-Parameter -->
        String asciiRB = "";
        int index = lineCounter + 1;
        while (!ascii[index].startsWith("<\\Verluste>")) {
            asciiRB += ("\n" + ascii[index]);
            index++;
        }

        importASCII(asciiRB);  // Laden der korrekten Parameter
    }

    private void importASCII(String asciiRB) {
        String[] ascii = DatenSpeicher.makeStringArray(asciiRB);


        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("verlustTyp ")) {
                verlustTyp = DatenSpeicher.leseASCII_int(ascii[i1]);
            } else if (ascii[i1].startsWith("rON ")) {
                rON = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("uf ")) {
                uf = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("kON ")) {
                kON = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("kOFF ")) {
                kOFF = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("uSWnorm ")) {
                uSWnorm = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("Cosser ")) {
                Cosser = DatenSpeicher.leseASCII_double(ascii[i1]);
            } else if (ascii[i1].startsWith("datnamGemesseneVerluste ")) {
                datnamGemesseneVerluste = ascii[i1].substring((new String("datnamGemesseneVerluste ")).length());
            }
        }
        //------------------
        try {
            // Relative Pfadangaben pruefen und gegebenfalls aktualisieren:
            String aktualisierterPfad = DatenSpeicher.lokalisiereRelativenPfad(Typ.DATNAM, datnamGemesseneVerluste);
            datnamGemesseneVerluste = aktualisierterPfad;
        } catch (Exception e) {
        }
        //
        if (!datnamGemesseneVerluste.equals(Typ.DATNAM_NOT_DEFINED)) {  // eventuelle Verlust-Detail-Datei laden und messkurvePvSWITCH[] initialisieren
            this.leseDetailVerlusteVonDatei(datnamGemesseneVerluste);
            //this.testausgabe();
        }
    }

    // wird einmal pro Zeitschritt zur Verlustberechnung aufgerufen:
    public double calcLosses_S(double i, double u, double tj, double dt, int state, double resistance, double uForward) {
        if (verlustTyp == VerlustBerechnung.VERLUST_TYP_SIMPEL) {
            //--------------
            boolean turnONcurr = (Math.abs(i_alt) < EPS) && (Math.abs(i) > EPS);  // current-based
            boolean turnOFFcurr = (Math.abs(i_alt) > EPS) && (Math.abs(i) < EPS);  // current-based
            boolean turnONstate = (stateOldOld == 0) && (stateOld == 1);  // if capacitive losses are relevant, they might occur at very low current >> current-detedction not sufficient
            boolean turnOFFstate = (stateOldOld == 1) && (stateOld == 0);  // we have to delay one time-step because the voltage change comes one step after the change of rON
            //----
            double pvSchalt = this.calcSwitchingLossSimple(turnONcurr, turnOFFcurr, i, u, dt);
       
            double pvLeit = this.calcConductionLossSimple(i, resistance, uForward);
            //double pvVirtCap= this.calcCossRelatedLoss(turnOFFstate,u,dt);
            //----
            i_alt = i;
            u_alt = u;
            stateOldOld = stateOld;
            stateOld = state;
            return (pvLeit + pvSchalt);
        } else if (verlustTyp == VerlustBerechnung.VERLUST_TYP_DETAIL) {
            throw new UnsupportedOperationException("not yet implemented!");

//            if (tj>1e4) tj= TEMP_DEFAULT;  // falls kein thermisches Modell angeschlossen, sondern vorerst nur der Pv-Innenwiderstand
//            //----
//            double pvSchalt= this.calcSwitchingLossDetailled(i, u, dt, tj);
//            double pvLeit= this.calcConductionLossDetailled(i, tj);
//            //----
//            i_alt=i;   u_alt=u;   stateOldOld=stateOld;   stateOld=state;
//            return (pvLeit +pvSchalt);
        }
        return -1;  // sollte eigentlich nie erreicht werden
    }

    public double calcConductionLossSimple(final double current, final double resistance, final double uForward) {
        return Math.abs(current * uForward) + (current * current * resistance);
    }

    private double calcSwitchingLossSimple(boolean turnONcurr, boolean turnOFFcurr, double i, double u, double dt) {
        double iSchalt = 0, pvSchalt = 0;
        
        if (turnONcurr) {  // turn-ON loss
            double uBlock = Math.abs(u_alt);
            if (uBlock < Math.abs(u)) {
                uBlock = Math.abs(u);  // aktuelle Schaltspannung
            }
            iSchalt = Math.abs(i_alt);
            if (iSchalt < Math.abs(i)) {
                iSchalt = Math.abs(i);  // aktueller Strom im Schalter
            }
            pvSchalt = (kON * iSchalt) * (uBlock / uSWnorm) / dt;  // 1* (19.39e-6) +
        } else if (turnOFFcurr) {  // turn-OFF loss
            double uBlock = Math.abs(u_alt);
            if (uBlock < Math.abs(u)) {
                uBlock = Math.abs(u);
            }
            iSchalt = Math.abs(i_alt);
            if (iSchalt < Math.abs(i)) {
                iSchalt = Math.abs(i);
            }
            pvSchalt = (kOFF * iSchalt) * (uBlock / uSWnorm) / dt;  // 1* (-127.5e-6) +
        }
        return Math.abs(pvSchalt);
    }

    public boolean leseDetailVerlusteVonDatei(String fyomu) {
        //------------------
        // Datei einlesen -->
        Vector datVec = new Vector();
        //
        // GZIP-Format (March 2009) - ganz neu! -->
        try {
            GZIPInputStream in1 = null;
            if (Typ.IS_APPLET) {
                in1 = new GZIPInputStream((new URL(Main.urlApplet, fyomu)).openStream());
            } else {
                in1 = new GZIPInputStream(new FileInputStream(fyomu));
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(in1));
            String z = null;
            while ((z = in.readLine()) != null) {
                datVec.addElement(z);
            }
            in.close();
        } catch (Exception e0) {
            // neue gezipte Version -->
            try {
                InflaterInputStream in1 = new InflaterInputStream(new FileInputStream(fyomu));
                BufferedReader in = new BufferedReader(new InputStreamReader(in1));
                String z = null;
                while ((z = in.readLine()) != null) {
                    datVec.addElement(z);
                }
                in.close();
            } catch (Exception e) {
                // Fehler, vielleicht ist es eine alte Version in reinem ASCII -->
                try {
                    BufferedReader in = new BufferedReader(new FileReader(fyomu));
                    String z = null;
                    while ((z = in.readLine()) != null) {
                        datVec.addElement(z);
                    }
                    in.close();
                } catch (Exception e2) {
                    return false;
                }
            }
        }
        //
        String[] ascii = new String[datVec.size()];
        for (int i1 = 0; i1 < datVec.size(); i1++) {
            ascii[i1] = (String) datVec.elementAt(i1);
        }
        StringTokenizer stk = null;
        //------------------
        // Parameter von 'VerlustBerechnung' initialisieren -->
        anzMesskurvenPvSWITCH = -1;  // dh. noch nicht initialisiert
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("anzMesskurvenPvSWITCH ")) {
                anzMesskurvenPvSWITCH = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
        }
        if (anzMesskurvenPvSWITCH <= 0) {
            return false;
        }
        messkurvePvSWITCH = new SchaltverlusteMesskurve[anzMesskurvenPvSWITCH];
        //
        anzMesskurvenPvCOND = -1;  // dh. noch nicht initialisiert
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("anzMesskurvenPvCOND ")) {
                anzMesskurvenPvCOND = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
        }
        if (anzMesskurvenPvCOND <= 0) {
            return false;
        }
        messkurvePvCOND = new LeitverlusteMesskurve[anzMesskurvenPvCOND];
        //------------------
        // die einzelnen 'messkurvePvSWITCH[]' initialisieren -->
        int index = 0;
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("<SchaltverlusteMesskurve>")) {
                String asciiRB = "";
                while (!ascii[i1].startsWith("<\\SchaltverlusteMesskurve>")) {
                    asciiRB += ("\n" + ascii[i1]);
                    i1++;
                }
                if (index >= anzMesskurvenPvSWITCH) {
                    return false;
                }
                messkurvePvSWITCH[index] = new SchaltverlusteMesskurve(-1, -1);
                messkurvePvSWITCH[index].importASCII(asciiRB);  // Initialisierung der entsprechenden gemessenen Kurve
                index++;
            }
        }
        //------------------
        // die einzelnen 'messkurvePvCOND[]' initialisieren -->
        index = 0;
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("<LeitverlusteMesskurve>")) {
                String asciiRB = "";
                while (!ascii[i1].startsWith("<\\LeitverlusteMesskurve>")) {
                    asciiRB += ("\n" + ascii[i1]);
                    i1++;
                }
                if (index >= anzMesskurvenPvCOND) {
                    return false;
                }
                messkurvePvCOND[index] = new LeitverlusteMesskurve(-1);
                messkurvePvCOND[index].importASCII(asciiRB);  // Initialisierung der entsprechenden gemessenen Kurve
                index++;
            }
        }
        datnamGemesseneVerluste = fyomu;
        //------------------
        // Naeherungs-Koeffizienten einlesen:
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("tjGrenzenCOND[] ")) {
                tjGrenzenCOND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b0COND[] ")) {
                b0COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b1COND[] ")) {
                b1COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c0COND[] ")) {
                c0COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c1COND[] ")) {
                c1COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d0COND[] ")) {
                d0COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d1COND[] ")) {
                d1COND = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            //
            if (ascii[i1].startsWith("tjGrenzenSW[] ")) {
                tjGrenzenSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b0onSW[] ")) {
                b0onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b1onSW[] ")) {
                b1onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c0onSW[] ")) {
                c0onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c1onSW[] ")) {
                c1onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d0onSW[] ")) {
                d0onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d1onSW[] ")) {
                d1onSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b0offSW[] ")) {
                b0offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("b1offSW[] ")) {
                b1offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c0offSW[] ")) {
                c0offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("c1offSW[] ")) {
                c1offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d0offSW[] ")) {
                d0offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("d1offSW[] ")) {
                d1offSW = DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            }
        }
        //------------------
        //this.testausgabe();
        return true;
    }
}
