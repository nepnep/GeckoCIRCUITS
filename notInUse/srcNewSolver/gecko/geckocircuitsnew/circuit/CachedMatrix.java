/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

public final class CachedMatrix {

    private double[][] _matrix;
    private double[][] _LUDecomp;
    private int _nn;
    private int[] _piv;
    private int[][] _lowerLUIndices;
    private int[][] _upperLUIndices;
    private double _latestAccessTime = -1;
    private int _accessCounter = 0;
    private int _hashCode = -1;
    private double[] _XCol;
    private static final int HASH_13 = 13;
    private static final int HASH_7 = 7;
    private static final int HASH_17 = 17;
    private static final int HASH_23 = 23;
    private static final int HASH_37 = 37;
    private static final int INT_LENGTH = 32;

    public CachedMatrix(final double[][] matrix) {
        _matrix = matrix;
    }

    public void initLUDecomp() {
        double[][] tmpMatrix = _matrix;
        _matrix = new double[_matrix.length][_matrix.length];

        // make a local copy of the original matrix:
        for (int i = 0; i < _matrix.length; i++) {
            System.arraycopy(tmpMatrix[i], 0, _matrix[i], 0, _matrix[0].length);
        }

        doLUDecomposition(_matrix);
        calculateLowerSparseLUDecompositionIndices();
        calculateUpperSparseLUDecompositionIndices();
    }

    @Override
    public int hashCode() {
        if (_hashCode == -1) {
            long newHashCode = HASH_13;
            for (int i = 0; i < _matrix.length; i++) {
                final int iValue = HASH_13 * i - HASH_7;
                for (int j = 0; j < _matrix[0].length; j++) {
                    final double matrixValue = _matrix[i][j];
                    if (matrixValue != 0) {
                        final long matrixBits = Double.doubleToLongBits(matrixValue);
                        newHashCode += -HASH_17 + HASH_37 * i - HASH_17 * j + iValue
                                * (HASH_23 * j + HASH_13) * ((int) (matrixBits ^ (matrixBits >>> INT_LENGTH)));
                    }
                }
            }
            _hashCode = (int) ((int) (newHashCode ^ (newHashCode >>> INT_LENGTH)));
        }
        return _hashCode;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CachedMatrix other = (CachedMatrix) obj;

        if (other._matrix.length != this._matrix.length) {
            return false;
        }

        for (int i = 0; i < _matrix.length; i++) {
            for (int j = 0; j < _matrix[0].length; j++) {
                if (_matrix[i][j] != other._matrix[i][j]) {
                    return false;
                }
            }
        }

        return true;
    }

    public void doLUDecomposition(final double[][] aMatrix) {


        // Use a "left-looking", dot-product, Crout/Doolittle algorithm.
        assert aMatrix.length == aMatrix[0].length;
        _nn = aMatrix[0].length - 1;


        _LUDecomp = new double[aMatrix.length - 1][aMatrix[0].length - 1];

        for (int i = 0; i < _nn; i++) {
            for (int j = 0; j < _nn; j++) {
                _LUDecomp[i][j] = aMatrix[i + 1][j + 1];
            }
        }

        _piv = new int[_nn];
        _XCol = new double[_piv.length + 1];
        for (int i = 0; i < _nn; i++) {
            _piv[i] = i;
        }
        int pivsign = 1;
        double[] LUrowi;
        double[] LUcolj = new double[_nn];

        // Outer loop.

        for (int j = 0; j < _nn; j++) {

            // Make a copy of the j-th column to localize references.

            for (int i = 0; i < _nn; i++) {
                LUcolj[i] = _LUDecomp[i][j];
            }

            // Apply previous transformations.

            for (int i = 0; i < _nn; i++) {
                LUrowi = _LUDecomp[i];

                // Most of the time is spent in the following dot product.

                int kmax = Math.min(i, j);
                double s = 0.0;
                for (int k = 0; k < kmax; k++) {
                    s += LUrowi[k] * LUcolj[k];
                }

                LUrowi[j] = LUcolj[i] -= s;
            }

            // Find pivot and exchange if necessary.

            int p = j;
            for (int i = j + 1; i < _nn; i++) {
                if (Math.abs(LUcolj[i]) > Math.abs(LUcolj[p])) {
                    p = i;
                }
            }
            if (p != j) {
                for (int k = 0; k < _nn; k++) {
                    double t = _LUDecomp[p][k];
                    _LUDecomp[p][k] = _LUDecomp[j][k];
                    _LUDecomp[j][k] = t;
                }
                int k = _piv[p];
                _piv[p] = _piv[j];
                _piv[j] = k;
                pivsign = -pivsign;
            }

            // Compute multipliers.

            if (j < _nn & _LUDecomp[j][j] != 0.0) {
                for (int i = j + 1; i < _nn; i++) {
                    _LUDecomp[i][j] /= _LUDecomp[j][j];
                }
            }
        }

        for (int j = 0; j < _LUDecomp.length; j++) {
            if (_LUDecomp[j][j] == 0) {
                throw new RuntimeException("Matrix is singular.");
            }
        }

        // shift piv + 1
        for (int i = 0; i < _piv.length; i++) {
            _piv[i]++;
        }
    }

    public double[] solveSparse(final double[] bVector) {

        for (int i = 0; i < _piv.length; i++) {
            _XCol[i + 1] = bVector[_piv[i]];
        }
        // Solve L*Y = B(piv,:)
        for (int k = 0; k < _nn; k++) {
            for (int i : _lowerLUIndices[k]) {
                _XCol[i + 1] -= _XCol[k + 1] * _LUDecomp[i][k];
            }
        }

        // Solve U*X = Y;
        for (int k = _nn - 1; k >= 0; k--) {
            _XCol[k + 1] /= _LUDecomp[k][k];
            for (int i : _upperLUIndices[k]) {
                _XCol[i + 1] -= _XCol[k + 1] * _LUDecomp[i][k];
            }
        }

        return _XCol;
    }

    private void calculateLowerSparseLUDecompositionIndices() {

        int[][] lowerLUIndices = new int[_LUDecomp.length][_LUDecomp.length];
        try {

            for (int k = 0; k < _nn; k++) {
                int counter = 0;
                for (int i = k + 1; i < _nn; i++) {
                    if (_LUDecomp[i][k] != 0) {
                        counter++;
                    }
                }

                lowerLUIndices[k] = new int[counter];
                counter = 0;

                for (int i = k + 1; i < _nn; i++) {
                    if (_LUDecomp[i][k] != 0) {
                        lowerLUIndices[k][counter] = i;
                        counter++;
                    }
                }

            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println(_LUDecomp.length + " xxx ");
            for (int i = 0; i < _LUDecomp.length; i++) {
                System.out.println("i: " + i + " " + _LUDecomp[i].length);
            }

            ex.printStackTrace();
            throw ex;

        }
        _lowerLUIndices = lowerLUIndices;
    }

    private void calculateUpperSparseLUDecompositionIndices() {
        int[][] upper = new int[_LUDecomp.length][_LUDecomp.length];

        for (int k = _nn - 1; k >= 0; k--) {
            int counter = 0;

            for (int i = 0; i < k; i++) {
                if (_LUDecomp[i][k] != 0) {
                    counter++;
                }
            }

            upper[k] = new int[counter];
            counter = 0;

            for (int i = 0; i < k; i++) {
                if (_LUDecomp[i][k] != 0) {
                    upper[k][counter] = i;
                    counter++;
                }
            }
        }
        _upperLUIndices = upper;
    }

    protected void setAccess(final double time) {
        _accessCounter++;
        _latestAccessTime = time;
    }

    protected int getAccessCounter() {
        return _accessCounter;
    }

    protected double getLatestAccessTime() {
        return _latestAccessTime;
    }
}
