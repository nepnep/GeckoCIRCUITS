/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

/**
 * Andrija: this interface is not yet used, but you should replace 
 * historyUpdatable, CurrentCalculatable, AStampable, BStampable with
 * this new interface, since it will clean things up a lot! The interfaces
 * which were holding only a single method are not very nice, so this here
 * is much better.
 * 
 * the "isSomeThingable" methods should be used to sort the components correspondingly,
 * at the initialization of the solver.
 * 
 * @author andy
 */
public interface ComponentSortable {

    //XXXXXXXXXXXXXXXXX former interface HistoryUpdatable
    public abstract void updateHistory(double[] p);

    public abstract boolean isUpdateHistorable();
    // a method void setUpdateHistory(boolean historyRequired); is not required 
    // for the history,
    // since this property should be defined fixed, e.g. only energy storages
    // require an update of the history. Then, also do only an update of the
    // history (old values), and not calculate currents or anything else in
    // the implementations

    //XXXXXXXXXXXXXXXXXX former interface AStampable
    public abstract void stampMatrixA(double[][] matrix, double dt);
    public abstract boolean isAStampable();
    // same aplies as above, isAStampable is a fixed property, therefore we don't need a setter

    //XXXXXXXXXXXXXXXXXX  former interface CurrentCalculatable();
    public abstract void calculateCurrent(final double[] p, final double dt, final double t);
    public abstract boolean isCurrentCalculatable();
    // this, we need now for currentCalculatable! Example: inductors or capacitors should always be currentCalculatable,
    // current sources should never be currentCalculatable (i.e. return always false when
    // currentSource.isCurrentCalculatable is called, since its value is known.
    // other components: when a current measurement is defined to the circuit component,
    // THEN set the component currentCalculatable before the solver initialization/sorting
    // of different components.
    public abstract boolean setCurrentCalculatable(boolean currentValueRequired);

    //XXXXXXXXXXXXXXXXXXX former interface DirectCurrentCalculatable
    public abstract void setZIndex(int index); // renamed from setZValue()
    public abstract int getZIndex(); // renamed from getZValue()
    public abstract double getZValueCurrent(); // renamed form getCurrent in DirectCurrentCalculatable
    public abstract boolean isZValuable();
    public abstract boolean setZValuable(); // this is needed, since the capacitor is with initial
    // condition a voltage source (ZValuable), after the initialisation, the capacitor should be
    // set as "not ZValuable)!
}