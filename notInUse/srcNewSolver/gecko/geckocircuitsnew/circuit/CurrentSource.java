/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.elements.ElementCurrentSource;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.util.ArrayList;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

public class CurrentSource extends CircuitComponent implements AStampable, BStampable, HistoryUpdatable {

    private TimeFunction _function;
    private ArrayList<BStampable> _stampUpdateList;

    public CurrentSource(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public final void stampMatrixA(double[][] matrix, double dt) {
    }

    @Override
    public final void stampVectorB(double[] b, double t, double dt) {
        _current = _function.calculate(t, dt);
        b[matrixIndices[0]] -= _current;
        b[matrixIndices[1]] += _current;

    }


    public void setFunction(TimeFunction function) {
        _function = function;
    }

    @Override
    public boolean isBasisStampable() {
        if(_function instanceof TimeFunctionConstant) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void registerBVector(BVector bvector) {
    }

    @Override
    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            prev_time = var_history[0][0];
            //System.out.println("before: _potential 1 = " + _potential1 + " _potential 2 = " + _potential2);
            _potential1 = var_history[0][1];
            _potential2 = var_history[0][2];
            //System.out.println("after: _potential 1 = " + _potential1 + " _potential 2 = " + _potential2);
            _current = var_history[0][3];
            _voltage = var_history[0][4];
            /*if (_needsOldPotCurrent)
            {
                _potOld1 = var_history[0][5];
                _potOld2 = var_history[0][6];
                //System.out.println("before: _oldCurrent = " + _oldCurrent);
                _oldCurrent = var_history[0][7];
                //System.out.println("after: _oldCurrent = " + _oldCurrent);
                _oldOldCurrent = var_history[0][8];
            }*/

            _function.stepBack();

            stepped_back = true;
            steps_reversed++;
        }
    }
    
    public final void updateHistory(double[] p) {
        _potential1 = p[matrixIndices[0]];
        _potential2 = p[matrixIndices[1]];
    }

}
