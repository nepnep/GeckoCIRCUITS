/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.PotentialArea;
import gecko.GeckoCIRCUITS.controlNew.BlockOrderOptimizerNew;
import gecko.GeckoCIRCUITS.controlNew.ControlPotentialValue;
import gecko.GeckoCIRCUITS.elements.ElementCircuit;
import gecko.GeckoCIRCUITS.elements.ElementConnection;
import gecko.GeckoCIRCUITS.elements.ElementConnectionCircuit;
import gecko.GeckoCIRCUITS.elements.ElementConnectionControl;
import gecko.GeckoCIRCUITS.elements.ElementControl;
import gecko.GeckoCIRCUITS.elements.ElementScope;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.elements.ElementMutualInductance;
import gecko.GeckoCIRCUITS.terminal.Terminable;
import geckocircuitsnew.Model;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import gecko.GeckoCIRCUITS.terminal.TerminalControl;
import gecko.GeckoCIRCUITS.terminal.TerminalGND;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import gecko.GeckoCIRCUITS.elements.ElementIGBT;
import gecko.GeckoCIRCUITS.elements.ElementIdealSwitch;
import gecko.GeckoCIRCUITS.elements.ElementThyristor;
import gecko.GeckoCIRCUITS.elements.ElementDiode;

import gecko.GeckoCIRCUITS.elements.ElementControlPI;
import gecko.GeckoCIRCUITS.elements.ElementControlPD;

/**
 *
 * @author andy
 */
public class TDSimulation implements Runnable {

    private double _dt;
    private double _simTime;
    private double _time;
    private double _dt_ctrl;
    private boolean _vardt;
    private double _allowed_error = 0.1;
    //private double _allowed_LTE = 0;
    private int _stepsExecuted;
    private boolean _synchronized;
    private StepSynchronizer _synchronizer;
    private StepSizeSelector _stepSizeControl;
    private List<ElementControl> _optimizedControlList;
    private List<ElementControl> _controlElementsUnsorted = new ArrayList<ElementControl>();
    private final List<ElementConnection> _lkConnections = new ArrayList<ElementConnection>();
    private final List<ElementConnection> _controlConnections = new ArrayList<ElementConnection>();
    private final List<PostProcessable> _postProcessables = new ArrayList<PostProcessable>();
    private final List _cComponents = new ArrayList();
    private final List<AbstractVoltageSource> _voltageSources = new ArrayList<AbstractVoltageSource>();
    private final List<MutualCoupling> _mutualCouplings = new ArrayList<MutualCoupling>();
    private final List<InductorCoupling> _inductors = new ArrayList<InductorCoupling>();
    private List<Terminal> controlTerminals = new ArrayList<Terminal>();
    private List<Terminal> circuitTerminals = new ArrayList<Terminal>();
    private List<Terminal> thermalTerminals = new ArrayList<Terminal>();
    private List<MatrixSolver> _solvers = new ArrayList<MatrixSolver>();
    private int _numberOfLKPotentials = -1;
    private int _numberOfThermPotentials = -1;
    private List<CircuitComponent> _thermComponents = new ArrayList<CircuitComponent>();
    private Model simModel;
    private boolean simFinished;
    private ArrayList<ControlPotentialValue> controlPotentialValues = new ArrayList<ControlPotentialValue>();
    private boolean _recordCircuitState = false;
    private SwitchStateRecord _circuitStateRecord;
    private ArrayList<ElementInterface> _switchingDevices = null;
    private boolean _doPostProcessables = true;
    private double[] _trialStepControlVector;
    



    public TDSimulation(Model model) {
        
        _vardt = model.dtvar.getValue();
        if (_vardt) {
            _dt = model.dt_min.getValue();
        }
        else {
            _dt = model.dt.getValue();
        }
        _synchronized = model.synched.getValue();
            
        simModel = model;
        _simTime = model.tEnd.getValue();
        simFinished = false;
        sortElements();
        createPotentialAreas();
        assignComponentIndices(_cComponents);
        assignComponentIndices(_thermComponents);

        BlockOrderOptimizerNew boo = new BlockOrderOptimizerNew(_controlElementsUnsorted);
        _optimizedControlList = boo.getOptimierteAbarbeitungsListe();


        if (_inductors.size() > 0) {
            CoupledInductorsGroup cIndGroup = new CoupledInductorsGroup(_inductors, _mutualCouplings,
                    _numberOfLKPotentials + _voltageSources.size());
            _cComponents.add(cIndGroup);

        }

        if (_cComponents.size() > 0) {
            _solvers.add(new MatrixSolver(_cComponents, _numberOfLKPotentials, simModel._TopologyContainer));
        }

        if (_thermComponents.size() > 0) {
            _solvers.add(new MatrixSolver(_thermComponents, _numberOfThermPotentials, null));
        }

        _stepsExecuted = 0;

    }
    



    public void start() {

        Thread simulationThread = new Thread(this);
        //initialize scope prior to every starting of a simulation
        ElementScope scope;
        CircuitComponent circuitcomp;
        for (ElementControl controlelement : _optimizedControlList)
        {
            if (controlelement instanceof ElementScope)
            {
                scope = (ElementScope) controlelement;
                //simModel.saveHistory = false; //remove later
                scope.initScope();
            }
            else {
                controlelement.init();
            }
        }
        for (Object comp : _cComponents) {
            if (comp instanceof CircuitComponent) {
                circuitcomp = (CircuitComponent) comp;
                circuitcomp.init();
            }
        }
        
        //check if it is a synchronized or variable step simulation
        if (_synchronized || _vardt) {
            //if yes we need a synchronizer and step size selector object
            _synchronizer = new StepSynchronizer();
            //pass _dt to step size selector since the minimum dt it uses varies based on whether it is a fixed step or variable step simulation
            _stepSizeControl = new StepSizeSelector(this,_vardt,_dt,simModel.dt_max.getValue(),simModel.LTEmax.getValue(),
                                simModel.attempts.getValue(),_synchronizer,simModel.solverType.getValue(),simModel.minAfterSync.getValue(),simModel.checkControlLTE.getValue());
        }
        //must make synchronizable element synchronized/unsynchronized (according to selected option)
        for (ElementControl controlelement : _optimizedControlList) {
            if (controlelement instanceof Synchronizable) {
                ((Synchronizable)controlelement).makeSynchronizable(_synchronized||_vardt); //if using variable time step, MUST be synchronized
                if (_synchronized  || _vardt) {
                    ((Synchronizable)controlelement).addSynchronizer(_synchronizer);
                }
            }
        }
        
        simulationThread.start();

    }

    public void run() {
        
        System.out.println("record circuit state: " + _recordCircuitState);
        
        _time = 0;
        //simModel.saveHistory = true; //remove later
        for (MatrixSolver solver : _solvers) {
            solver.calculateInitialState(1e-9);
            solver.setSaveHistory(simModel.saveHistory);
        }
        //boolean doubled = false;
        //boolean doubled2 = false;
        //boolean synced = false;
        boolean initState = true;
        
        //double dt_old = _dt;
        
        
        //_dt = _dt_min;
        System.out.println("sim. time: " + _simTime + " (starting) dt: " + _dt);
        long startTime = (new java.util.Date()).getTime();
        

        

        while (_time <= _simTime) {

            
            //System.out.println("start executing step: " + (_stepsExecuted+1) + " at time: " + _time);
            
            
            /*if (_synchronized) {
                if (synced) {
                    _dt = dt_old;
                    synced = false;
                }
                
                if (_synchronizer.isThereSyncEvent()) {
                    dt_old = _dt;
                    _dt = _synchronizer.getNextStep();
                    _time = _time - dt_old;
                    _time = _time + _dt;
                    System.out.println("Sync event registered, sync dt is: " + _dt);
                    synced = true;
                }
            }*/
            
            //execute one simulation step
            //System.out.println("circuit solver");
            for (MatrixSolver solver : _solvers) {
                solver.solveTimeStep(_time, _dt);
            }
            
            if (_doPostProcessables) {
                //System.out.println("post processables");
                for (PostProcessable post : _postProcessables) {
                    post.doPostProcess(_dt, _time);
                } 
            }
            //System.out.println("control blocks");
            for (ElementControl regler : _optimizedControlList) {
                regler.calculateOutput(_time, _dt);              
            }
                

            /*(if (_vardt)
                simModel.saveHistory = true;*/
            
            if (_recordCircuitState && (((AbstractSwitch.switchActionOccurred || Diode.diodeErrorOccurred) && (_time > 3*_dt)) || (_time > 3*_dt && initState)) && _doPostProcessables) { //postprocessables flag is there to ensure no states get recorded during trial step
                if (initState) {
                    initState = false;
                }
                for (ElementInterface elem : _switchingDevices) {
                    if (elem instanceof ElementDiode) {
                        ElementDiode diode = (ElementDiode) elem;
                        _circuitStateRecord.addSwitchState(diode.getState(_time));
                    }
                    else if (elem instanceof ElementThyristor) {
                        ElementThyristor thyr = (ElementThyristor) elem;
                        _circuitStateRecord.addSwitchState(thyr.getState(_time));
                    }
                    else if (elem instanceof ElementIGBT) {
                        ElementIGBT igbt = (ElementIGBT) elem;
                        _circuitStateRecord.addSwitchState(igbt.getState(_time));
                    }
                    else if (elem instanceof ElementIdealSwitch) {
                        ElementIdealSwitch sw = (ElementIdealSwitch) elem;
                        _circuitStateRecord.addSwitchState(sw.getState(_time));
                    }
                }
            }
            
           if (Diode.diodeErrorOccurred) {
               //System.out.println("diode ERROR occurred!");
           } 
                
                 
           
           
           if (_synchronized || _vardt) {
               _dt = _stepSizeControl.determineStepSize();
           } 
           
           _time += _dt;
           _stepsExecuted++;
           
           if (_recordCircuitState && Diode.diodeErrorOccurred) {
                Diode.diodeErrorOccurred = false;
           }
           if (_recordCircuitState && AbstractSwitch.switchActionOccurred)
                AbstractSwitch.switchActionOccurred = false;
           //System.out.println("executed step: " + _stepsExecuted); 
            
            
        }

        long endTime = (new java.util.Date()).getTime();
        long simLength = endTime - startTime;
        System.out.println("calculation finished");
        //simFinished = true;
        System.out.println(_time + " " + _simTime + " " + simModel.tEnd.getValue());
        System.out.println("steps executed: " + _stepsExecuted);
        System.out.println("simulation duration: " + simLength);
        
        //update all scopes at end of a simulation
        ElementScope scope;
        for (ElementControl controlelement : _optimizedControlList)
        {
            if (controlelement instanceof ElementScope)
            {
                scope = (ElementScope) controlelement;
                System.out.println("updating scope " + scope._elementName.getValue() + " at end of simulation");
                //simModel.saveHistory = false; //remove later
                scope.scope.updateScope(0, simModel.tEnd.getValue());
            }
        }
        simFinished = true;
        //System.exit(3);
    }
    
    void setTrialStep(boolean isTrialStep) {
        simModel.testStep = isTrialStep;
        for (MatrixSolver solver : _solvers) {
                 solver.setTrialStep(isTrialStep);
        }
        _doPostProcessables = !isTrialStep;
    }
    
    void stepBack(double dt) {
        for (MatrixSolver solver : _solvers) {
              solver.stepBack();
         }
        
        for (ElementControl regler : _optimizedControlList) {
              regler.stepBack();
        }
        
        _time -= dt;
    }
    
    void storeTrialStepControlVector() {
        _trialStepControlVector = createControlValuesVector(controlPotentialValues);
    }
    
    void keepNewStep(double newDt) {
        for (MatrixSolver solver : _solvers) {
              solver.keepNewStep(newDt); 
        }
    }
    
    double getControlError() {
        double error;
        
        double[] controlVector = createControlValuesVector(controlPotentialValues);
        error = findControlError(_trialStepControlVector,controlVector);
        
        return error;
    }
    
    double getPError() {
        double error = 0;
        double current_error;
        for (MatrixSolver solver : _solvers) {
             current_error = solver.findPVectorError();
             if (current_error > error)
                    error = current_error;
        }
        return error;    
    }

    private double findControlError(double [] test_control_vector, double [] control_vector)
    {
        double error = 0;
        double current_error;
        for (int i = 0; i < control_vector.length; i++)
        {
            //current_error = Math.abs((control_vector[i] - test_control_vector[i])/control_vector[i]) * 100.0;
            //System.out.println("control_vector["+i+"] = " + control_vector[i] + "; test_control_vector["+i+"] = " +test_control_vector[i]);
            current_error = Math.abs(control_vector[i] - test_control_vector[i]);
            if (current_error > error)
                error = current_error;
        }
        return error;
    }
    
    

    private void createPotentialAreas() {
        _numberOfThermPotentials = createPotentialAreasCircuit(thermalTerminals).size();
        _numberOfLKPotentials = createPotentialAreasCircuit(circuitTerminals).size();
        

        simModel._TopologyContainer.setPotentialAreas(createPotentialAreasCircuit(circuitTerminals));
        
        final ArrayList<PotentialArea> controlPotentials = createPotentialAreasControl(controlTerminals);
    }

    private ArrayList<PotentialArea> createPotentialAreasControl(final List<Terminal> terminals) {
        // create a new potential area for each connector:
        ArrayList<PotentialArea> circuitPotentials = new ArrayList<PotentialArea>();

        for (Terminal term : terminals) {
            PotentialArea pot = new PotentialArea(term);
            circuitPotentials.add(pot);
        }

        PotentialArea.mergeConnectors(circuitPotentials);
        removeNotConnectedPotentials(circuitPotentials);

        Map<Integer, ControlPotentialValue> potMap = new HashMap<Integer, ControlPotentialValue>();
        for (int i = 0; i < circuitPotentials.size(); i++) {
            potMap.put(i, new ControlPotentialValue(i));
        }
        ControlPotentialValue cntpot;
        for (PotentialArea pot : circuitPotentials) {
            for (Terminal term : pot._allTerminals) {
                int index = circuitPotentials.indexOf(pot);
                term.setIndex(index);

                if (term instanceof TerminalControl) {
                    cntpot = potMap.get(term.getIndex());
                    //System.out.println("index: " + term.getIndex());
                    ((TerminalControl) term).setPotentialValue(cntpot);
                    if (!(controlPotentialValues.contains(cntpot)))
                        controlPotentialValues.add(cntpot);
                }

            }
        }

        return circuitPotentials;
    }

    private double[] createControlValuesVector(ArrayList<ControlPotentialValue> controlpotentials)
    {
        double[] control_vector = new double[controlpotentials.size()];
        for (int i = 0; i < control_vector.length; i++)
        {
            control_vector[i] = controlpotentials.get(i)._value;
        }
        return control_vector;
    }


    private ArrayList<PotentialArea> createPotentialAreasCircuit(final List<Terminal> terminals) {
        // create a new potential area for each connector:
        ArrayList<PotentialArea> circuitPotentials = new ArrayList<PotentialArea>();

        for (Terminal term : terminals) {
            PotentialArea pot = new PotentialArea(term);
            circuitPotentials.add(pot);
        }

        PotentialArea.mergeConnectors(circuitPotentials);
        
        removeNotConnectedPotentials(circuitPotentials);
        connectIndependentCircuits(circuitPotentials);

        PotentialArea gndPot = null;
        for (PotentialArea pot : circuitPotentials) {
            for (Terminal term : pot._allTerminals) {
                if (term instanceof TerminalGND) {
                    gndPot = pot;
                }
            }
        }

        if (gndPot != null) {
            System.out.println(circuitPotentials.indexOf(gndPot));
            circuitPotentials.remove(gndPot);
            circuitPotentials.add(0, gndPot);
        }

        for (PotentialArea pot : circuitPotentials) {
            for (Terminal term : pot._allTerminals) {
                int index = circuitPotentials.indexOf(pot);
                term.setIndex(index);
            }
        }

        return circuitPotentials;
    }

    /**
     * insert the different types of ElementInterface object in the specific
     * containers. Additionally, sort the control elements into an optimized
     * ordered list
     *
     */
    private void sortElements() {

        //for (ElementInterface element : ElementInterface.allElements) {
        for (ElementInterface element : simModel.modelElements) {
            if (element.enabled.getValue()) {

                if (element instanceof ElementCircuit) {
                    ElementCircuit elCir = (ElementCircuit) element;
                    switch (elCir.getConnectionType()) {
                        case POWERCIRCUIT:
                        case RELUCTANCE:
                            _cComponents.addAll(elCir.circuitComponents);
                            break;
                        case THERMALCIRCUIT:
                            _thermComponents.addAll(elCir.circuitComponents);
                            break;
                        default:
                            assert false : elCir;
                    }

                }

                if (element instanceof ElementMutualInductance) {
                    //MutualCoupling cop = ((ElementMutualInductance) element).getMutualCoupling(ElementInterface.allElements);
                    MutualCoupling cop = ((ElementMutualInductance) element).getMutualCoupling(simModel.modelElements);
                    _mutualCouplings.add(cop);
                }

                if (element instanceof ElementConnectionCircuit) {
                    ElementConnection con = (ElementConnection) element;
                    _lkConnections.add((ElementConnection) element);
                }

                if (element instanceof Terminable) {
                    for (Terminal term : ((Terminable) element).getTerminals()) {

                        switch (term.getConnectionType()) {
                            case CONTROL:
                                controlTerminals.add(term);
                                break;
                            case POWERCIRCUIT:
                            case RELUCTANCE:
                                circuitTerminals.add(term);
                                break;
                            case THERMALCIRCUIT:
                                thermalTerminals.add(term);
                                break;
                            default:
                                assert false;
                        }
                    }
                }

            }

            sortControlElements(element);
        }


        sortCircuitComponents();
    }

    private void sortControlElements(ElementInterface element) {

        if (element.enabled.getValue()) {
            if (element instanceof ElementControl) {
                _controlElementsUnsorted.add((ElementControl) element);
            }
        }

        if (element instanceof ElementConnectionControl) {
            _controlConnections.add((ElementConnection) element);
        }
    }

    private void sortCircuitComponents() {

        for (Object cComponent : _cComponents) {

            if (cComponent instanceof AbstractVoltageSource) {
                _voltageSources.add((AbstractVoltageSource) cComponent);
            }

            if (cComponent instanceof InductorCoupling) {
                _inductors.add((InductorCoupling) cComponent);
            }


            if (cComponent instanceof PostProcessable) {
                _postProcessables.add((PostProcessable) cComponent);
            }
        }
    }

    private void assignComponentIndices(final List components) {

        for (Object cComponent : components) {
            if (cComponent instanceof CircuitComponent) {
                ((CircuitComponent) cComponent).assignTerminalIndices();
                ((CircuitComponent) cComponent).setComponentNumber(_cComponents.indexOf(cComponent));
            }

        }
    }

    private void removeNotConnectedPotentials(ArrayList<PotentialArea> circuitPotentials) {
        // remove potentials that have no circuit element connected to it:
        ArrayList<PotentialArea> deleteEmtyPotentials = new ArrayList<PotentialArea>();

        for (PotentialArea pot : circuitPotentials) {
            boolean deleteFlag = true;


            for (Terminal term : pot._allTerminals) {
                if (!(term instanceof TerminalConnector)) {
                    deleteFlag = false;
                }
            }

            if (deleteFlag) {
                deleteEmtyPotentials.add(pot);
            }
        }

        circuitPotentials.removeAll(deleteEmtyPotentials);
    }

    /*
     * Independent circuits result in a nearly singular solution matrix.
     * This function detects independent circuits and merges them at an
     * arbitrary potential. This function is still ugly, maybe renice later...
     */
    private void connectIndependentCircuits(ArrayList<PotentialArea> circuitPotentials) {

        ArrayList<PotentialArea> removeList = new ArrayList<PotentialArea>();

        for (PotentialArea potArea : circuitPotentials) {
            removeList.add(new PotentialArea(potArea._allTerminals));
        }

        int oldSize = removeList.size();
        do {
            ArrayList<PotentialArea> toRemove = new ArrayList<PotentialArea>();
            for (int i = 0; i < removeList.size(); i++) {
                for (int j = i + 1; j < removeList.size(); j++) {
                    PotentialArea potArea1 = removeList.get(i);
                    PotentialArea potArea2 = removeList.get(j);

                    if (PotentialArea.hasComponentConnection(potArea1, potArea2)) {
                        potArea1._allTerminals.addAll(potArea2._allTerminals);
                        potArea2._allTerminals.clear();
                        toRemove.add(potArea2);
                    }
                }
            }
            oldSize = removeList.size();
            removeList.removeAll(toRemove);
        } while (oldSize != removeList.size());

        // connect the potentials at one arbitrary terminal
        PotentialArea potMain = null;
        for (PotentialArea tmp : circuitPotentials) {
            if (tmp._allTerminals.contains(removeList.get(0)._allTerminals.get(0))) {
                potMain = tmp;
            }
        }

        ArrayList<PotentialArea> finalRemove = new ArrayList<PotentialArea>();
        for (int i = 1; i < removeList.size(); i++) {
            PotentialArea toRemove = removeList.get(i);
            Terminal term = toRemove._allTerminals.get(0);
            for (PotentialArea potOrig : circuitPotentials) {
                if (potOrig._allTerminals.contains(term)) {
                    assert potOrig != potMain;
                    potMain._allTerminals.addAll(potOrig._allTerminals);
                    potOrig._allTerminals.clear();
                    finalRemove.add(potOrig);
                }
            }
        }

        circuitPotentials.removeAll(finalRemove);

    }

    public boolean isFinished()
    {
        return simFinished;
    }


    //functions for running simulation step by step
    public void init()
    {
        //initialize scope prior to every starting of a simulation
        ElementScope scope;
        CircuitComponent circuitcomp;
        for (ElementControl controlelement : _optimizedControlList)
        {
            if (controlelement instanceof ElementScope)
            {
                scope = (ElementScope) controlelement;
                //simModel.saveHistory = false; //remove later
                scope.initScope();
            }
            else {
                controlelement.init();
            }
        }
        for (Object comp : _cComponents) {
            circuitcomp = (CircuitComponent) comp;
            circuitcomp.init();
        }

        //calculate initial state
        for (MatrixSolver solver : _solvers) {
            solver.calculateInitialState(1e-9);
            solver.setSaveHistory(simModel.saveHistory);
        }

        _stepsExecuted = 0;
    }

    //to use timestep defined in the model, set dt = -1
    public double simulateOneStep(double time, double dt)
    {
        if (dt == -1)
            dt = _dt;

        for (MatrixSolver solver : _solvers) {
                solver.solveTimeStep(time, dt);
        }

        for (PostProcessable post : _postProcessables) {
                post.doPostProcess(dt, time);
        }

        
        
        if (_recordCircuitState && (((AbstractSwitch.switchActionOccurred || Diode.diodeErrorOccurred) && (time > 3*_dt)))) {
                for (ElementInterface elem : _switchingDevices) {
                    if (elem instanceof ElementDiode) {
                        ElementDiode diode = (ElementDiode) elem;
                        _circuitStateRecord.addSwitchState(diode.getState(time));
                    }
                    else if (elem instanceof ElementThyristor) {
                        ElementThyristor thyr = (ElementThyristor) elem;
                        _circuitStateRecord.addSwitchState(thyr.getState(time));
                    }
                    else if (elem instanceof ElementIGBT) {
                        ElementIGBT igbt = (ElementIGBT) elem;
                        _circuitStateRecord.addSwitchState(igbt.getState(time));
                    }
                    else if (elem instanceof ElementIdealSwitch) {
                        ElementIdealSwitch sw = (ElementIdealSwitch) elem;
                        _circuitStateRecord.addSwitchState(sw.getState(time));
                    }
                }
            }
            //for situations where a gate pulse last for only one step, to aviod recording FUTURE state (in next step) rather than present state
            for (ElementControl regler : _optimizedControlList) {
                regler.calculateOutput(time, dt);
            }

        _stepsExecuted++;
        if (Diode.diodeErrorOccurred)
            Diode.diodeErrorOccurred = false;
        if (AbstractSwitch.switchActionOccurred)
            AbstractSwitch.switchActionOccurred = false;
        return time += dt;     
    }

    public double simulateOneStep(double time)
    {
        return simulateOneStep(time, -1);
    }
    
    public void recordCircuitSwitchStates(SwitchStateRecord record) {
        _circuitStateRecord = record;
        if (_switchingDevices == null) {
            _switchingDevices = new ArrayList<ElementInterface>();
            for (ElementInterface elem : simModel.modelElements) {
                if ((elem instanceof ElementIdealSwitch) || (elem instanceof ElementIGBT) || (elem instanceof ElementDiode) || (elem instanceof ElementThyristor)) {
                    _switchingDevices.add(elem);
                }
            }
        }
        _recordCircuitState = true;
    }
    
    public void stopRecordingCircuitSwitchStates() {
        _recordCircuitState = false;
    }
    
    
}
