/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;

import geckocircuitsnew.DatenSpeicher;
import java.io.Serializable;




// Datenbehaelter fuer eine Messkurve -->
public class LeitverlusteMesskurve implements Serializable {

    //-----------------------------
    private String kurvenName;
    private int anzPunkte;
    private double[][] data;
    private double tj;
    //-----------------------------


    // Datenbehaelter mit folgendem Format fuer  data[][] -->
    // U [V] - I [A]
    // ..      ..
    // ..      ..
    // usw.
    // Parameter: T_junction --> bei der Messung vorgegeben
    //
    public LeitverlusteMesskurve (double tj) {
        anzPunkte=0;   data= null;  // noch keine Daten vorhanden
        this.tj=tj;    this.kurvenName= ((int)tj)+"°C";
    }

    public LeitverlusteMesskurve copy () {
        LeitverlusteMesskurve copy= new LeitverlusteMesskurve(-1);
        copy.kurvenName=this.kurvenName;   copy.anzPunkte=this.anzPunkte;
        copy.data= new double[this.data.length][this.data[0].length];
        for (int i1=0;  i1<this.data.length;  i1++) for (int i2=0;  i2<this.data[0].length;  i2++) copy.data[i1][i2]= this.data[i1][i2];
        copy.tj = this.tj;
        return copy;
    }


    public void addData (double[][] data) { this.data=data; }

    public double[][] getData () { return data; }
    public double getTj () { return tj; }
    public String getName () { return kurvenName; }





    //===============================================================================
    //===============================================================================
    // Testdaten -->

    public static LeitverlusteMesskurve createTestKurve (int nr) {
        LeitverlusteMesskurve k= new LeitverlusteMesskurve(-1);
        switch (nr) {
            case 1:
                k.tj= 11;
                k.kurvenName= ((int)k.tj)+"°C";
                double[] uMess=  new double[]{0.2, 0.4, 0.6, 0.8, 1.0};
                double[] iMess=  new double[]{0.1, 0.2, 1.0, 6.0, 15.0};
                k.anzPunkte= uMess.length;
                k.data= new double[][]{uMess,iMess};
                break;
            case 2:
                k.tj= 22;
                k.kurvenName= ((int)k.tj)+"°C";
                uMess=  new double[]{0.0, 0.6, 2.0};
                iMess=  new double[]{0.0, 0.5, 10.0};
                k.anzPunkte= uMess.length;
                k.data= new double[][]{uMess,iMess};
                break;
            case 3:
                k.tj= 33;
                k.kurvenName= ((int)k.tj)+"°C";
                uMess=  new double[]{0.1, 0.4, 0.6, 0.9, 1.5};
                iMess=  new double[]{0.0, 0.2, 2.0, 8.0, 17.0};
                k.anzPunkte= uMess.length;
                k.data= new double[][]{uMess,iMess};
                break;
            default: System.out.println("Ungueltige Testkurven-Nummer! (derzeit nur '1', '2', '3' implementiert)");  break;
        }
        return k;
    }


    //===============================================================================
    //===============================================================================

    // zum Speichern im ASCII-Format (anstatt als Object-Stream) -->
    //
    protected void exportASCII (StringBuffer ascii) {

    }
    public void importASCII (String asciiBlock) {
        String[] ascii= DatenSpeicher.makeStringArray(asciiBlock);
        //------------------
        for (int i1=0;  i1<ascii.length;  i1++) {
            if (ascii[i1].startsWith("anzPunkte "))  anzPunkte= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("tj ")) tj= DatenSpeicher.leseASCII_double(ascii[i1]);
            if (ascii[i1].startsWith("data[][] ")) data= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
        }
        //------------------
        kurvenName= ((int)tj)+"°C";
        //------------------
    }




}




