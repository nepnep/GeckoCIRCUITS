/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.elements.ElementSignalSource;

/**
 *
 * @author anstupar
 */
public class TimeFunctionRect extends TimeFunction implements Synchronizable {

    public double amplitude;
    public double frequency;
    public double offset;
    public double phase;
    public double dutyratio;

    private double _dyUP;
    private double _dyDOWN;
    private double _triangle = 0;
    private boolean _ascending = true;
    
    private StepSynchronizer _synchronizer;
    private boolean _synchronized;
    
    private double _nextEdge; //time at which next egde should occur
    private boolean _nextEdgePositive; //true if the next edge is positive (rising), false if negative (falling)
    private double _regDt;

    public TimeFunctionRect(double ampl, double freq, double offs, double phas, double duty, int steps_saved) {
        amplitude = ampl;
        frequency = freq;
        offset = offs;
        phase = phas;
        dutyratio = duty;
        _steps_saved = steps_saved;
        var_history = new double[steps_saved][3];
    }
    
    
    @Override
    public double calculate(double t, double dt)
    {
        double signal;
        double timeToEdge;

        if (saveHistory && !stepped_back && (t > 0))
        {
            historyForward();
            if (_synchronized) {
                var_history[0][0] = _nextEdge;
                var_history[0][1] = (_nextEdgePositive) ? 1:0;
                var_history[0][2] = _regDt;
            }
            else {
                var_history[0][0] = _triangle;
                var_history[0][1] = (_ascending) ? 1:0;
            }
        }

        if (t == 0) {
            if (_synchronized)
                signal = calculateInitialPhaseShiftSynchronized();
            else
                calculateInitialPhaseShift();
            _regDt = dt;
        }

        if (dutyratio <= 0)
            dutyratio = 1e-6;

        if (dutyratio >= 1)
            dutyratio = 1 - 1e-6;


        if (_synchronized) {
            timeToEdge = _nextEdge - t;
            if (Math.abs(timeToEdge) <= ElementSignalSource.ROUND_ERROR) {
                if (_nextEdgePositive) {
                    signal = offset + amplitude;
                    _nextEdge = t + dutyratio * (1.0 / frequency);
                    _nextEdgePositive = false;
                }
                else {
                    signal = offset;
                    _nextEdge = t + (1.0 - dutyratio) * (1.0 / frequency);
                    _nextEdgePositive = true;
                }
                //it is possible that the new _nextExtreme point is coming at a time smaller than the time step (could happen in duty ratio is very large or very small!)
                //therefore check the previous normal (non-synchronizing) time step and place sync. request if necessary
                timeToEdge = _nextEdge - t;
                if ((timeToEdge <= 1.01*_regDt) && (timeToEdge != _regDt)) {
                    requestSyncStep(timeToEdge);
                }
            }
            else {
                if (_nextEdgePositive) {
                    signal = offset;
                }
                else {
                    signal = amplitude + offset;
                }
                //synchronize edge time exactly
                if ((timeToEdge <= 1.01*dt) && (timeToEdge != dt)) {
                    requestSyncStep(timeToEdge);
                    //record previous non-sync dt
                    _regDt = dt;
                }
            }
        }
        else {
            _dyUP = 4 * frequency * dt;
            _dyDOWN = 4 * frequency * dt;
            if (_ascending) {
                 _triangle += _dyUP;
            }
            else {
                 _triangle -= _dyDOWN;
            }
            if (_triangle >= (+1)) {
                 _triangle = (+1);
                 _ascending = false;
            }
            else if (_triangle <= (-1)) {
                 _triangle = (-1);
                 _ascending = true;
            }
            if (_triangle > 1 - 2 * dutyratio) {
                signal = amplitude + offset;
            }
            else {
                signal = offset;
            }
        }

        //System.out.println("" + t + " " + dt + " " + _triangle + " " + signal + " " + _dyUP + " " + saveHistory + " " + stepped_back);
        
        if (saveHistory) {
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }

        

        return signal;
    }

    private void calculateInitialPhaseShift()
    {
        if (dutyratio > 1)
            dutyratio = 1;
        if (dutyratio < 0)
            dutyratio = 0;

        double tx = 0;
        double txEnd = 1.0 / frequency;
        double dtx = txEnd / 1e3;
        double phaseX = phase;

        while (phaseX > (2 * Math.PI))
            phaseX -= (2 * Math.PI);

        while (phaseX < 0)
            phaseX += (2 * Math.PI);

        _triangle = 0;
        _ascending = true;

        double txE = txEnd * phaseX / (2 * Math.PI) - (1 - 2 * dutyratio) / (4 * frequency);
        if (txE < 0)
            txE += txEnd;

        while (tx < txE)
        {
            double dyUPx = (2 * frequency * dtx) / 0.5;
            double dyDOWNx = (2 * frequency * dtx) / (1 - 0.5);
            if (_ascending) {
                    _triangle += dyUPx;
                } else {
                    _triangle -= dyDOWNx;
                }

                if (_triangle >= (+1)) {
                    _triangle = (+1);
                    _ascending = false;
                } else if (_triangle <= (-1)) {
                    _triangle = (-1);
                    _ascending = true;
                }
                tx += dtx;
            }
            _triangle = -_triangle;
    }
    
    private double calculateInitialPhaseShiftSynchronized() {
        
        //should determine whether signal is initially high or low (based on phase shift)
        double initSignal;
        
        double phaseX = phase;
        if (phase >= 2.0*Math.PI) {
            phaseX = phase - 2.0*Math.PI;
        }
        else if (phase <= -2.0*Math.PI) {
            phaseX = phase + 2.0*Math.PI;
        }
        
        if (phaseX == 0.0) { //if phase is zero, initially signal is high, next edge is determined by duty ratio
            _nextEdge = dutyratio * (1.0 / frequency);
            _nextEdgePositive = false;
            initSignal = amplitude + offset;
        }
        else {
            //if phase non-zero, determine whether signal is high or low
            double dutyPhaseRel = dutyratio + (phaseX / (2.0*Math.PI));
            
            
            if ((phase < 0.0) && (dutyPhaseRel <= 0.0)) {
                //if phase is negative, but duty ratio + phase / 360deg is <= 0, this is equivalent to a positive phase shift in which signal is initially low
                phaseX = 2.0*Math.PI + phase;
            }
            else if ((phase > 0.0) && (dutyPhaseRel > 1.0)) {
                //if phase is positive, but duty ration + phase / 360deg is > 1, this is equivalent to a negative phase shift in which signal is initially high
                phaseX = phase - 2.0*Math.PI;
            }
            
            //now phase is recalculated such that for phase > 0, signal is initially low, for phase < 0, signal is initially high
            
            if (phaseX > 0.0) {
                _nextEdge = (phaseX / (2.0*Math.PI)) * (1.0 / frequency);
                _nextEdgePositive = true;
                initSignal = offset;
            }
            else {
                _nextEdge = (dutyratio + (phaseX / (2.0*Math.PI))) * (1.0 / frequency);
                _nextEdgePositive = false;
                initSignal = amplitude + offset;
            }
            
        }
        
        return initSignal;
        
        
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < _steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            if (_synchronized) {
                _nextEdge = var_history[0][0];
                _nextEdgePositive = (((int)var_history[0][1]) == 1);
                _regDt = var_history[0][2];
            }
            else {
                _triangle = var_history[0][0];
                _ascending = (((int)var_history[0][1]) == 1);
            }
            stepped_back = true;
            steps_reversed++;
        }
    }
    
    //synchronization functions
    public void addSynchronizer(StepSynchronizer syncer) {
        _synchronizer = syncer;
    }
    
    public void makeSynchronizable(boolean sync) {
        _synchronized = sync;
    }
    
    private void requestSyncStep(double dtSync) {
        _synchronizer.requestSyncStep(dtSync);
    }

}
