/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

public class Thyristor extends AbstractSwitch implements CurrentCalculatable {

    private double _tReverse;
    private double _lastSwitchEvent;
    private static final double REVERSE_FACTOR = 3.0;
    
    public Thyristor(final Terminal term1, final Terminal term2, final ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public final void stampMatrixA(final double[][] matrix, final double deltaT) {
        final double aValue = 1.0 / _rDt;  //  +1/r
        matrix[matrixIndices[0]][matrixIndices[0]] += (+aValue);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+aValue);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-aValue);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-aValue);
    }

    @Override
    public final void stampVectorB(final double[] bVector, final double time, final double deltaT) {
        final double bValue = _uForward / _rDt;
        bVector[matrixIndices[0]] += (+bValue);
        bVector[matrixIndices[1]] += (-bValue);
    }

    @Override
    public final void calculateCurrent(final double[] pVector, final double deltaT, final double time) {

        _voltage = pVector[matrixIndices[0]] - pVector[matrixIndices[1]];
        _current = (_voltage - _uForward) / _rDt;

        if ((_voltage + _current * _rDt < (disturbanceValue * _uForward)) && (_rDt < _rOFF)) {  // (uD < uf) und Thyristor "ON"

            if (time - _lastSwitchEvent > REVERSE_FACTOR * _tReverse) {
                _lastSwitchEvent = time;
            }

            if (time - _lastSwitchEvent >= _tReverse) {
                _rDt = _rOFF;
                Diode.diodeSwitchError = true;
                if (_bVector != null) {
                    _bVector.setUpdateAllFlag();
                }
                _rDt = Math.max(_rDt, NEARLY_ZERO_R);
                switchAction = true;

            }
        }

        if (_gateValue && (((_voltage) > (disturbanceValue * _uForward)) && (_rDt > _rON))) {  
            // gate==1  und  (uD > uf) und Thyristor "OFF"
            _rDt = _rON;
            if (_bVector != null) {
                _bVector.setUpdateAllFlag();
            }
            Diode.diodeSwitchError = true;
            _rDt = Math.max(_rDt, NEARLY_ZERO_R);
            switchAction = true;
        }

    }

    /**
     * function is overwritten, since the thyristor behaves different:
     * - switch off only after _tReverse
     * - switch off only when current is < 0
     * @param value
     */
    @Override
    public final void setGateSignal(final boolean value) {
        _gateValue = value;
    }

    public final void setTRR(final double value) {
        _tReverse = value;
    }
}