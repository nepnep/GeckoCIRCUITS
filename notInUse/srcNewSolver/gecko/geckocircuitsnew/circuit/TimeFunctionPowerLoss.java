/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.elements.ElementCircuit;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

/**
 *
 * @author andy
 */
public class TimeFunctionPowerLoss extends TimeFunction {

    private String _lossElementName = "";
    private LossCalculatable _lossElement = null;



    public void setLossElementName(String name) {
        _lossElementName = name;
    }

    @Override
    public double calculate(double t, double dt) {
        if (_lossElement == null) {
            for (ElementInterface elem : ElementInterface.allElements) {
                if (elem._elementName.getValue().equals(_lossElementName)) {
                    CircuitComponent comp = ((ElementCircuit) elem).circuitComponents.get(0);
                    if (comp instanceof LossCalculatable) {
                        _lossElement = (LossCalculatable) comp;
                    }
                }
            }

        }

        assert _lossElement != null;
        double loss = _lossElement.calculateLoss(t, dt);
        return loss;
    }

    public void stepBack() { }
}
