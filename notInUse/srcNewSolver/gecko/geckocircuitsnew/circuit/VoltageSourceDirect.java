/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.elements.ElementCircuit;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.elements.ElementVoltage;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

public class VoltageSourceDirect extends VoltageSource {

    private double _gain = 1000;
    private int _directPotentialIndex1 = -1;
    private int _directPotentialIndex2 = -1;
    private Terminal _direct1;
    private Terminal _direct2;
    private String directString = "";

    public VoltageSourceDirect(TimeFunction timeFunction, Terminal term1, Terminal term2, Terminal direct1, Terminal direct2, double gain, ElementInterface element) {
        super(timeFunction, term1, term2, element);
        _direct1 = direct1;
        _direct2 = direct2;
        _gain = gain;
    }

    public VoltageSourceDirect(TimeFunction timeFunction, Terminal term1, Terminal term2, String string, double gain, ElementInterface element) {
        super(timeFunction, term1, term2, element);
        _gain = gain;
        directString = string;
    }

    @Override
    public void assignTerminalIndices() {
        super.assignTerminalIndices();

        if (!directString.isEmpty()) {
            for (ElementInterface elem : ElementInterface.allElements) {
                if (elem._elementName.getValue().equals(directString)) {
                    _direct1 = ((ElementCircuit) elem).circuitComponents.get(0)._term1;
                    _direct2 = ((ElementCircuit) elem).circuitComponents.get(0)._term2;
                }
            }
        }


        _directPotentialIndex1 = _direct1.getIndex();
        _directPotentialIndex2 = _direct2.getIndex();

    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        // voltage is function of node-potentials and, therefore, calculated here in matrix A
        matrix[matrixIndices[0]][_z] += (+1.0);
        matrix[matrixIndices[1]][_z] += (-1.0);
        matrix[_z][matrixIndices[0]] += (+1.0);
        matrix[_z][matrixIndices[1]] += (-1.0);
        matrix[_z][_directPotentialIndex1] += (-_gain);
        matrix[_z][_directPotentialIndex2] += (+_gain);
    }

    @Override
    public void stampVectorB(double[] b, double t, double dt) {
        // does not add a voltage --> voltage is function of node-potentials and, therefore, calculated in matrix A
    }

    @Override
    public boolean isBasisStampable() {
        return true;
    }

    public void setGain(double value) {
        _gain = value;
    }
}
