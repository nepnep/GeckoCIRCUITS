/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.util.ArrayList;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

/**
 *
 * @author andy
 */
public class IdealSwitch extends AbstractSwitch implements HistoryUpdatable {

    public IdealSwitch(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        double aW = 1.0 / _rDt;  //  +1/R        
        assert aW < 1E60;
        matrix[matrixIndices[0]][matrixIndices[0]] += (+aW);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+aW);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-aW);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-aW);
    }

    @Override
    public final void stampVectorB(double[] b, double t, double dt) {
    }


    public boolean isBasisStampable() {
        return true;
    }

    @Override
    public void updateHistory(double[] p) {

        _potential1 = p[matrixIndices[0]];
        _potential2 = p[matrixIndices[1]];

        // old current is not needed for Resistor, but the current has not yet been
        // calculated:
        _current = (p[matrixIndices[0]] - p[matrixIndices[1]]) / _rDt;
        //_oldCurrent = _current;
    }
}
