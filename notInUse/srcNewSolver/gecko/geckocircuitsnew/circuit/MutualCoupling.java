/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andy
 */
public class MutualCoupling {
    private double _M;
    private double _k;
    private InductorCoupling _l1;
    private InductorCoupling _l2;

    
    public MutualCoupling(InductorCoupling l1, InductorCoupling l2, double k) {
        _k = k;

        _l1 = l1;
        _l2 = l2;

    }

    public MutualCoupling() {
    }

    public InductorCoupling getL1() {
        return _l1;
    }

    public InductorCoupling getL2() {
        return _l2;
    }

    private double getMutualInductance() {
        return _k * Math.sqrt(_l1.getInductance() * _l2.getInductance() );
    }


    void stampInductanceMatrix(double[][] inductanceMatrix, final List<InductorCoupling> allInductors) {
        int index1 = allInductors.indexOf(_l1);
        int index2 = allInductors.indexOf(_l2);
        inductanceMatrix[index1][index2] = getMutualInductance();
        inductanceMatrix[index2][index1] = getMutualInductance();
    }


}
