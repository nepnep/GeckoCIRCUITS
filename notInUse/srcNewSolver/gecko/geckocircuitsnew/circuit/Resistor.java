/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

/**
 *
 * @author andy
 */
public class Resistor extends CircuitComponent implements AStampable, HistoryUpdatable, LossCalculatable {

    private static final double FAST_NULL_R = 1e-9;

    private double _resistance = 100;

    public Resistor(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    public void setResistance(double resistance) {
        if(_resistance < FAST_NULL_R) {
            _resistance = FAST_NULL_R;
        } else {
            _resistance = resistance;
        }
    }

    @Override
    public void assignTerminalIndices() {
        super.assignTerminalIndices();
    }


    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        double aW = 0;
        aW = 1.0 / _resistance;  //  +1/R        
        matrix[matrixIndices[0]][matrixIndices[0]] += (+aW);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+aW);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-aW);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-aW);
    }


    @Override
    public String toString() {
        return super.toString() + " " + _resistance + " " + matrixIndices[0] + " " + matrixIndices[1];
    }

    @Override
    public final void updateHistory(double[] p) {
        _potential1 = p[matrixIndices[0]];
        _potential2 = p[matrixIndices[1]];
        _voltage = _potential1 - _potential2;
        // old current is not needed for Resistor, but the current has not yet been
        // calculated:
        _current = (_potential1 - _potential2) / _resistance;
        //_oldCurrent = _current;
    }

    public double calculateLoss(double time, double dt) {
        return Math.abs(_resistance * _current * _current);
    }

    public void setLosscalculation(VerlustBerechnung verlustBerechnung) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
