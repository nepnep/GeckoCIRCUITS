/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.circuit;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author anstupar
 */

//this class is for allowing control blocks (eg. signal sources) to synchronize events (eg. edges of square wave) by requesting "irregular" step sizes
public class StepSynchronizer {
    
    private ArrayList<Double> _requestedSyncSteps;
    private boolean _syncEvent;
    
    public StepSynchronizer() {
        
        _requestedSyncSteps = new ArrayList<Double>();
        _syncEvent = false;
        
    }
    
    public void requestSyncStep(double syncDt) {
        
        _requestedSyncSteps.add(new Double(syncDt));
        _syncEvent = true;
    }
    
    public boolean isThereSyncEvent() {
        return _syncEvent;
    }
    
    public double getNextStep() {
        
        double dt;
        
        //sort the list to get smallest step
        Collections.sort(_requestedSyncSteps);
        dt = _requestedSyncSteps.get(0);
        //clear all other requests, since once this step is executed, they will change
        _requestedSyncSteps.clear();
        _syncEvent = false;
        return dt;
        
    }
    
    
    
}
