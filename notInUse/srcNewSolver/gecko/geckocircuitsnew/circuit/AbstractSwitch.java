/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

/**
 *
 * @author andy
 */
public abstract class AbstractSwitch extends CircuitComponent implements AStampable, BStampable, LossCalculatable {

    
    protected static final double NEARLY_ZERO_R = 1e-9;
    private static final double DEFAULT_TEMP = 25;
    
    protected double _rON = DEFAULT_R_ON;
    protected double _rOFF = DEFAULT_R_OFF;
    // variable resistance
    protected double _rDt = DEFAULT_R_OFF;
    protected double _uForward = DEFAULT_U_FORWARD;

    public static boolean switchAction = false;
    public static boolean switchActionOccurred = false;
    protected BVector _bVector;


    protected boolean _gateValue = false;

    public AbstractSwitch(final Terminal term1, final Terminal term2, final ElementInterface element) {
        super(term1, term2, element);
    }


    /*
     * maybe unclean coding: Thyristor overrides this method with a different implementation,
     * all other switches use this piece of code.
     */
    public void setGateSignal(final boolean value) {
        
        _gateValue = value;
        switchAction = true;
        
        if (_gateValue) {
            _rDt = _rON;
        } else {
            _rDt = _rOFF;
        }

        _rDt = Math.max(_rDt, NEARLY_ZERO_R);
        if(_bVector != null) {
            _bVector.setUpdateAllFlag();
        }
        
        //System.out.println("Gate signal set to " + _gateValue);
        
    }

    public final boolean isGateSignalOn() {
        return _gateValue;
    }

    public boolean isBasisStampable() {
        return true;
    }

    @Override
    public final void registerBVector(final BVector bvector) {
       _bVector = bvector;
    }

    public final void setROn(final double value) {
        _rON = value;
    }

    public final void setROff(final double value) {
        _rOFF = value;
    }

    public final void setUForward(final double value) {
        _uForward = value;
    }

    @Override
    public final double calculateLoss(final double time, final double deltaT) {

        int state = 1;
        if(_rDt > _rON) {
            state = 0;
        }

        return _lossCalculation.calcLosses_S(_current, _voltage, DEFAULT_TEMP, deltaT, state , _rDt, _uForward);        
    }
    
    public SwitchState getState(double time) {
        
        SwitchState state;
        SwitchState.State componentState;
        
        if (_rDt > _rON) {
            componentState = SwitchState.State.OFF;
        }
        else {
            componentState = SwitchState.State.ON;
        }
        
        state = new SwitchState(_element,componentState,time);
        
        return state;
        
    }

}
