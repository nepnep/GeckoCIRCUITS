/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.elements.ElementInterface;

public abstract class AbstractVoltageSource extends CircuitComponent implements AStampable,
        DirectCurrentCalculatable, HistoryUpdatable {

    protected int _z = -1;


    public AbstractVoltageSource(Terminal term1, Terminal term2, ElementInterface element) {
        super(term1, term2, element);
    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        assert _z > 0;
        matrix[matrixIndices[0]][_z] += (+1.0);
        matrix[matrixIndices[1]][_z] += (-1.0);
        matrix[_z][matrixIndices[0]] += (+1.0);
        matrix[_z][matrixIndices[1]] += (-1.0);

    }

    @Override
    public int getZValue() {
        return _z;
    }

    @Override
    public void setZValue(int value) {
        _z = value;
    }

    public void updateHistory(double[] p) {
        // System.out.println("function: " + _function + " " + _z);
        _current = p[_z];  // SpgQuellen-Stroeme stehen als Unbekannte ausnahmsweise auch im Knotenpotetial-Vektor
        _potential1 = p[matrixIndices[0]];
        _potential2 = p[matrixIndices[1]];
    }
}
