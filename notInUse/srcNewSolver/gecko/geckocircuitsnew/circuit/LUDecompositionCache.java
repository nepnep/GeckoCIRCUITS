/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.circuit;

import java.util.HashMap;
import java.util.Map;

/**
 * Main-Function: getCachedLUDecomposition. For switched converters, one and the same LU decomposition is typically
 * re-computed very often. Therefore, this class provides a cache, which speeds up the calculation for larger
 * matrices.
 * @author andy
 */
public class LUDecompositionCache {

    private static final int MAX_CACHE_SIZE = 100;
    private final Map<Integer, CachedMatrix> _cachedMatrices = new HashMap<Integer, CachedMatrix>();
    private int _cacheHitCounter = 0;
    private int _cacheMissCounter = 0;
    private static final boolean USE_CACHE = true;

    /**
     * 
     * @param matrix the matrix to search in the LU-Cache
     * @param time actual simulationtime, needed for the cache overflow removal algorithm
     * @return the matrix in the cache, including the LU-Decomposition
     */
    public CachedMatrix getCachedLUDecomposition(final double[][] matrix, final double time) {

        final CachedMatrix newMatrix = new CachedMatrix(matrix);
        final CachedMatrix fromCache = _cachedMatrices.get(newMatrix.hashCode());
        if (fromCache == null) {
            //_cacheMissCounter++;
            newMatrix.setAccess(time);
            testForCacheShrink(time);
            newMatrix.initLUDecomp();
            if (USE_CACHE) {
                _cachedMatrices.put(newMatrix.hashCode(), newMatrix);
            }
            return newMatrix;

        } else {
            //assert fromCache.equals(newMatrix);
            fromCache.setAccess(time);
            // _cacheHitCounter++;
            //printDebugMessages(time);
            return fromCache;
        }
    }

    private void printDebugMessages(final double time) {
        _cacheHitCounter++;
        System.out.println("cache size: " + _cachedMatrices.size());
        for (int key : _cachedMatrices.keySet()) {
            final CachedMatrix mat = _cachedMatrices.get(key);
            System.out.println("cache: " + mat + " " + (time - mat.getLatestAccessTime()) + " " + mat.getAccessCounter());
        }
        System.out.println("cache hits: " + _cacheHitCounter + " " + _cacheMissCounter + " " + (100.0 * _cacheHitCounter / (_cacheHitCounter + _cacheMissCounter)) + "%");

    }

    /*
     * if cache is larger than maximum cache size, then remove
     * the oldest and the two least accessed matrix entry.
     * careful with the least accessed: put a threshold, so that 
     * a quite new matrix is not removed immediately!
     */
    private void testForCacheShrink(double time) {
        if (_cachedMatrices.size() > MAX_CACHE_SIZE) {
            int oldestKey = -1;
            double oldestTime = 1e99;

            // test for oldest key:
            for (int key : _cachedMatrices.keySet()) {
                CachedMatrix tmp = _cachedMatrices.get(key);
                if (tmp.getLatestAccessTime() < oldestTime) {
                    oldestKey = key;
                    oldestTime = tmp.getLatestAccessTime();
                }
            }

            _cachedMatrices.remove(oldestKey);

            double accessMinimumAge = (oldestTime + time) / 2;
            removeLeastAccessedMatrix(accessMinimumAge);
            removeLeastAccessedMatrix(accessMinimumAge);
        }
    }

    private void removeLeastAccessedMatrix(double accessMinimumAge) {

        int leastAccessKey = -1;
        int leastAccessCounter = 1000000;

        for (int key : _cachedMatrices.keySet()) {
            CachedMatrix tmp = _cachedMatrices.get(key);
            if (tmp.getLatestAccessTime() < accessMinimumAge) {
                if (tmp.getAccessCounter() < leastAccessCounter) {
                    leastAccessCounter = tmp.getAccessCounter();
                    leastAccessKey = key;
                }
            }
        }

        if (leastAccessKey != -1) {
            _cachedMatrices.remove(leastAccessKey);
        }
    }
}
