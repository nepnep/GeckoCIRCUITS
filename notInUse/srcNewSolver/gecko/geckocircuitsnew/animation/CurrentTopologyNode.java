/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zimmi
 */
public final class CurrentTopologyNode {

    private final Point _position;
    private int _potentialAreaID;
    List<CurrentTopologyEdge> _edges = new ArrayList<CurrentTopologyEdge>(); // Order: N, W, S, E
    private static final int DIRECTIONS = 4;

    CurrentTopologyNode(final Point position) {
        _position = position;
        for (int i = 0; i < DIRECTIONS; i++) {
            _edges.add(null);
        }
    }

    public void addEdge(final CurrentTopologyEdge edge, final int position) {
        _edges.set(position, edge);
    }

    public void addPotentialAreaID(final int potentialAreaID) {
        _potentialAreaID = potentialAreaID;
    }

    public int getDirections() {
        return DIRECTIONS;
    }

    public int getID() {
        return _potentialAreaID;
    }

    public boolean fullyDefinedCurrents(final CurrentTopologyEdge curEdge) {
        for (CurrentTopologyEdge edge : _edges) {
            if ((edge == null) || (curEdge.equals(edge))) {
                continue;
            }
            if (edge.getDomPos() == -1) {
                return false;
            }
        }
        return true;
    }

    public Iterable<CurrentTopologyEdge> getEdges() {
        return _edges;
    }

    public Point getPos() {
        return _position;
    }
}
