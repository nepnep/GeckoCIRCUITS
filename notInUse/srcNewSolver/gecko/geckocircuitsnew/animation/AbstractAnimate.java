/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.GeckoCIRCUITS.allg.Typ;
import geckocircuitsnew.Orientation;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JComponent;

public abstract class AbstractAnimate {

    private static final int DELAY = 100;
    protected static List<JComponent> _animateParents = new ArrayList<JComponent>();
    protected static int counter = 0;
    public static final Color ANIMATIONCOLOR = Color.MAGENTA;
    final Color[] _colors = {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.MAGENTA};
    protected int _startX;
    protected int _startY;
    protected static int dpix;
    protected int _stopX;
    protected int _stopY;
    protected Point _start;
    protected Point _end;
    protected CurrentState _state = CurrentState.ZERO;
    private static Timer timer;
    protected static final int POLYSIZE = 3;
    public final Orientation _direction;
    protected static final int ARROWSIZE = 2;

    public AbstractAnimate(final JComponent parent, final int[] relCoords) {
        _startX = relCoords[0];
        _startY = relCoords[1];

        _stopX = relCoords[2];
        _stopY = relCoords[3];
        _animateParents.add(parent);

        _start = new Point(_startX, _startY);
        _end = new Point(_stopX, _stopY);

        _direction = Orientation.getDirection(_start, _end);

    }

    public abstract void paint(final Graphics graphic, final int scaling);

    public final void setAnimationState(final CurrentState state) {
        assert state != null;
        _state = state;
    }

    public static void startAnimation() {
        final TimerTask task = new TimerTask() {

            @Override
            public void run() {
                for (JComponent comp : _animateParents) {
                    comp.repaint();
                }

                counter++;

                if (counter > dpix - 1) {
                    counter = 0;
                }


            }
        };
        timer = new Timer();
        timer.schedule(task, 0, DELAY);
    }

    public static void stop() {
        if (timer != null) {
            timer.cancel();
        }
    }

    final int[] shiftPositiveDirection(final int pos, final int coord) {
        final int[] returnValues = new int[POLYSIZE];
        returnValues[0] = pos - (1 * ARROWSIZE) + dpix * coord + counter;
        returnValues[1] = pos - (1 * ARROWSIZE) + dpix * coord + counter;
        returnValues[2] = pos + (2 * ARROWSIZE) + dpix * coord + counter;
        return returnValues;
    }

    final int[] shiftNegativeDirection(final int pos, final int coord) {
        final int[] returnValues = new int[POLYSIZE];
        returnValues[0] = pos + (1 * ARROWSIZE) + dpix * coord - counter;
        returnValues[1] = pos + (1 * ARROWSIZE) + dpix * coord - counter;
        returnValues[2] = pos - (2 * ARROWSIZE) + dpix * coord - counter;
        return returnValues;
    }
}