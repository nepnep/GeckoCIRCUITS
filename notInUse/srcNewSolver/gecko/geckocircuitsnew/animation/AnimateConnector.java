/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.animation;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;

/**
 *
 * @author zimmi
 */
public final class AnimateConnector extends AbstractAnimate {

    private List<Point> _animationPath = new ArrayList<Point>();
    private boolean _reversedAnimation = false;

    public AnimateConnector(final JComponent parent, final int[] relCoords) {
        super(parent, relCoords);
    }

    public boolean containsPoint(final Point testPoint) {
        return _animationPath.contains(testPoint);
    }

    public void setAnimationLocation(final Point start, final Point end) {
        final ArrayList<Point> path = new ArrayList<Point>();
        path.add(start);
        path.add(end);

        _animationPath = Collections.unmodifiableList(path);
    }

    @Override
    public void paint(final Graphics graphic, final int scaling) {
        final Graphics2D g2d = (Graphics2D) graphic;
        // Calculate each time in case of resize
        dpix = scaling;

        final int steps = _colors.length;
        synchronized (_colors) {
            for (int i = 0; i < steps; i++) {

                graphic.setColor(AbstractAnimate.ANIMATIONCOLOR);
                final AlphaComposite aComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
                g2d.setComposite(aComp);

                // Animation Path correction
                CurrentState correctedState;

                if (isReversionNeeded()) {
                    correctedState = CurrentState.getOppositeState(_state);
                } else {
                    correctedState = _state;
                }

                final int xPos = dpix * Math.min(_startX, _stopX);
                final int yPos = dpix * Math.min(_startY, _stopY);


                switch (correctedState) {
                    case NEGATIVE:
                        for (int n = 1; n <= Math.abs(_stopX - _startX); n++) {
                            final int[] yValues = new int[]{yPos - (2 * ARROWSIZE), yPos + (2 * ARROWSIZE), yPos};
                            final int[] xValues = shiftNegativeDirection(xPos, n);


                            graphic.fillPolygon(xValues, yValues, POLYSIZE);
                        }
                        for (int n = 1; n <= Math.abs(_stopY - _startY); n++) {
                            final int[] xValues = new int[]{xPos - (2 * ARROWSIZE), xPos + (2 * ARROWSIZE), xPos};
                            final int[] yValues = shiftNegativeDirection(yPos, n);

                            graphic.fillPolygon(xValues, yValues, POLYSIZE);
                        }
                        break;
                    case POSITIVE:
                        for (int n = 0; n < Math.abs(_stopX - _startX); n++) {
                            final int[] yValues = new int[]{yPos - (2 * ARROWSIZE), yPos + (2 * ARROWSIZE), yPos};
                            final int[] xValues = shiftPositiveDirection(xPos, n);

                            graphic.fillPolygon(xValues, yValues, POLYSIZE);

                        }
                        for (int n = 0; n < Math.abs(_stopY - _startY); n++) {
                            final int[] xValues = new int[]{xPos - (2 * ARROWSIZE), xPos + (2 * ARROWSIZE), xPos};
                            final int[] yValues = shiftPositiveDirection(yPos, n);

                            graphic.fillPolygon(xValues, yValues, POLYSIZE);

                        }
                        break;
                    case ZERO:
                        break;
                    default:
                        assert false;

                }
            }
        }
        final AlphaComposite aComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);

        g2d.setComposite(aComp);
    }

    public float calculateHalfConnectorLength(final Point curPoint) {
        final Point relevantPoint = getOtherTerminal(curPoint);

        if (curPoint.x == relevantPoint.x) {
            return (((float) relevantPoint.y) - ((float) curPoint.y)) / 2;
        } else {
            return (((float) relevantPoint.x) - ((float) curPoint.x)) / 2;
        }
    }

    public Point getOtherTerminal(final Point curPoint) {
        if (curPoint.equals(_animationPath.get(0))) {
            return _animationPath.get(1);
        } else {
            return _animationPath.get(0);
        }
    }

    private boolean isReversionNeeded() {
        boolean returnValue = false;
        if ((_startX > _stopX) || (_startY > _stopY)) { // irregular case: Animation not follows trimmedCoordinates
            //System.out.println("Reverse case: " + _animationPath +", "+ _startX +", "+ _startY +", "+ _stopX +", "+ _stopY);
            returnValue = true;
        }
        if (_reversedAnimation) {
            return !returnValue;
        } else {
            return returnValue;
        }
    }

    public void isSameDirection(final CurrentTopologyEdge curEdge) {
        final List<Point> path = curEdge.getPath();

        int index = 0;
        if (curEdge.isReversed()) {
            index = 1;
        }

        if (path.indexOf(_animationPath.get(index)) < path.indexOf(_animationPath.get(((index + 1) % 2)))) {
            _reversedAnimation = false;
        } else {
            _reversedAnimation = true;
        }
    }
}
