/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.CurrentPathMeter;
import gecko.GeckoCIRCUITS.terminal.PotentialArea;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import geckocircuitsnew.Orientation;
import java.awt.Point;
import java.util.*;

/**
 *
 * @author Zimmi
 */
public final class CurrentTopologyGraph {

    private final List<CurrentTopologyNode> _nodes = new ArrayList<CurrentTopologyNode>();
    private final List<CurrentTopologyEdge> _edges = new ArrayList<CurrentTopologyEdge>();
    private final List<Point> _nodePositions = new ArrayList<Point>();
    private final Map<CurrentTopologyEdge, CircuitComponent> _domComponents = 
            new HashMap<CurrentTopologyEdge, CircuitComponent>();
    private final Map<CurrentTopologyEdge, CurrentPathMeter> _pathMeters = new HashMap<CurrentTopologyEdge, CurrentPathMeter>();
    private final List<PotentialArea> _potentialAreas;
    private final List<CircuitComponent> _components;
    private static final int MAXDIRECTION = 4;

    public CurrentTopologyGraph(final List<PotentialArea> potentialAreas, final List<CircuitComponent> components) {
        _potentialAreas = potentialAreas;
        _components = components;
    }

    public void addNode(final CurrentTopologyNode node) {
        for (CurrentTopologyNode curNode : _nodes) {
            if (curNode.getPos().equals(node.getPos())) {
                return;
            }
        }
        _nodes.add(node);
        _nodePositions.add(node.getPos());
    }

    public void addEdge(final CurrentTopologyEdge edge) {
        if (_edges.contains(edge)) {
            return;
        }
        _edges.add(edge);
    }

    void print() {
        System.out.println("---------------------");
        System.out.println("Graph Representation:");
        System.out.println("---------------------");
        System.out.println("Der Graph besitzt " + _nodes.size() + " Knoten.");
        for (CurrentTopologyNode node : _nodes) {
            System.out.println("\tNode @ " + node.getPos());
        }
        System.out.println("Der Graph besitzt " + _edges.size() + " Kanten.");
        for (CurrentTopologyEdge edge : _edges) {
            edge.printPath();
        }

        System.out.println("Der Graph besitzt " + _pathMeters.size() + " PathMeters.");
        for (CurrentPathMeter pathMeter : _pathMeters.values()) {
            pathMeter.printPathMeter();
        }


    }

    public void generateNodes() {
        for (PotentialArea pArea : _potentialAreas) {
            final List<TerminalComponent> termComps = pArea.getTermComponents();
            final List<TerminalConnector> termConns = pArea.getTermConnectors();

            createNodesFromIntersection(termConns, pArea);

            createNodesFromMultipleTerminals(termComps, termConns, pArea);
        }
    }

    public void buildGraph() {
        generateNodes();

        for (CurrentTopologyNode node : _nodes) {
            initEdges(node);
        }

        fixSingleLoops();

        initPathMeters();

        print();
    }

    private void initEdges(final CurrentTopologyNode node) {

        for (Orientation searchDirection : Orientation.values()) {

            if (node._edges.get(searchDirection.ordinal()) != null) {
                continue;
            }

            final LoopParameters params = new LoopParameters(node, searchDirection);

            checkForAnimateConnectors(params);

            if (node._edges.get(searchDirection.ordinal()) == null) {
                checkForComponents(params);
            }

            // Bereits nach einem Stück den nächsten Node erreicht

            if (_nodePositions.contains(params._curPoint) && !(params._curPoint.equals(node.getPos()))) {
                final CurrentTopologyNode secondNode = _nodes.get(_nodePositions.indexOf(params._curPoint));

                node._edges.get(searchDirection.ordinal()).addNode(secondNode);
                secondNode.addEdge(node._edges.get(searchDirection.ordinal()), (searchDirection.ordinal() + 2) % MAXDIRECTION);

            }

            if (node._edges.get(searchDirection.ordinal()) == null) {
                continue;
            }


            buildEdge(params);
        }
    }

    private List<AnimateConnector> getAnimations(final PotentialArea pArea) {
        final ArrayList<AnimateConnector> animations = new ArrayList<AnimateConnector>();

        for (TerminalConnector termConn : pArea.getTermConnectors()) {
            for (AnimateConnector aniConn : termConn.getConnector()._animation) {
                animations.add(aniConn);
            }
        }
        return animations;
    }

    private AnimateConnector getNextAnimation(final LoopParameters params) {
        AnimateConnector curAnimation = null;
        if (params._curEdge.getAnimations().size() > 0) {
            curAnimation = params._curEdge.getAnimation(params._curEdge.getAnimations().size() - 1);
        }

        if (curAnimation == null) {
            for (AnimateConnector aniConn : params._animations) {
                if (aniConn.containsPoint(params._curPoint)) {
                    return aniConn;
                }
            }
        }

        for (AnimateConnector aniConn : params._animations) {
            if ((aniConn.containsPoint(params._curPoint)) && !(curAnimation.equals(aniConn))) {
                return aniConn;
            }
        }
        return null;
    }

    private Orientation calculateComponentDirection(final TerminalComponent termComp) {
        final Point start = termComp.getSheetPosition();
        final Point end = getOtherComponentTerminal(termComp).getSheetPosition();

        if (start.x == end.x) {
            if (start.y > end.y) {
                return Orientation.NORTH;
            } else {
                return Orientation.SOUTH;
            }
        } else {
            if (start.x < end.x) {
                return Orientation.EAST;
            } else {
                return Orientation.WEST;
            }
        }
    }

    private Terminal getOtherComponentTerminal(final TerminalComponent termComp) {
        final List<Terminal> terminals = termComp.getCircuitComponent().getTerminals();
        if (terminals.get(0).getSheetPosition().equals(termComp.getSheetPosition())) {
            return terminals.get(1);
        } else {
            return terminals.get(0);
        }
    }

    private Terminal getOtherComponentTerminal(final Point curPoint, final CircuitComponent comp) {
        final List<Terminal> terms = comp.getTerminals();
        if (terms.get(0).getSheetPosition().equals(curPoint)) {
            return terms.get(1);
        } else {
            return terms.get(0);
        }
    }

    private boolean rightObservation(final Orientation curDirection,
            final Orientation direction, final float mean) {
        if (curDirection.isHorizontal() == direction.isHorizontal()) {
            if ((curDirection == Orientation.NORTH) || (curDirection == Orientation.WEST)) {
                if (mean < 0) {
                    return true;
                }
            } else {
                if (mean > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    private void sortEdge(final CurrentTopologyEdge curEdge) {

        curEdge.checkReversed();

        final List<AnimateConnector> animations = curEdge.getAnimations();

        for (AnimateConnector aniConn : animations) {
            aniConn.isSameDirection(curEdge);
        }

    }

    private boolean existingMeter(final CurrentPathMeter meter) {
        for (CurrentPathMeter curMeter : _pathMeters.values()) {
            if (curMeter.getEdge().equals(meter.getEdge())) {
                return true;
            }
        }
        return false;
    }

    public List<CurrentPathMeter> getPathMeters() {
        final List<CurrentPathMeter> pathMeters = new ArrayList<CurrentPathMeter>();
        for (CurrentPathMeter pathMeter : _pathMeters.values()) {
            pathMeters.add(pathMeter);
        }
        return pathMeters;
    }

    private void buildEdge(final LoopParameters loopParameters) {
        LoopParameters params = loopParameters;
        
        final int dir = params._searchDirection.ordinal();
        params._curEdge = params._node._edges.get(dir);

        while (!(_nodePositions.contains(params._curPoint))) {
            params._securityBreak = true;

            if (params._lastAdded == addedType.CONNECTOR) {
                final AnimateConnector nextAnimation = getNextAnimation(params);
                if (nextAnimation == null) {
                    params = addNextComponent(params);
                } else {
                    params = addNextAnimateConnector(params, nextAnimation);
                }
            } else {
                params._animations = getAnimations(_potentialAreas.get(params._potID)); // update Animation Parts
                if (params._animations.isEmpty()) {
                    params = addNextComponent(params);
                } else {
                    final AnimateConnector nextAnimation = getNextAnimation(params);
                    if (nextAnimation != null) {
                        params = addNextAnimateConnector(params, nextAnimation);
                    }
                }
            }

            if (params._securityBreak) {
                params._curEdge = null;
                params._node._edges.set(dir, null);
                return;

            }
        }

        updateEndNode(params);

        if (params._curEdge.getDomPos() != -1) {
            _domComponents.put(params._curEdge, params._curEdge.getDominantComponent());
        }


        sortEdge(params._curEdge);

    }

    List<CurrentTopologyEdge> getEdges() {
        return _edges;
    }

    private float calculateComponentMean(final Point curPoint, final TerminalComponent termComp) {
        final Point relevantPoint = getOtherComponentTerminal(termComp).getSheetPosition();

        if (curPoint.x == relevantPoint.x) {
            return (((float) relevantPoint.y) - ((float) curPoint.y)) / 2;
        } else {
            return (((float) relevantPoint.x) - ((float) curPoint.x)) / 2;
        }
    }

    private void fixSingleLoops() {
        //Spezialfall: Single Loop
        final List<CircuitComponent> remainingComponents = new ArrayList<CircuitComponent>();
        remainingComponents.addAll(_components);


        for (CircuitComponent comp : _components) {
            boolean breaker = false;
            for (CurrentTopologyEdge edge : _edges) {
                if (edge.getComponents().contains(comp)) {
                    remainingComponents.remove(comp);
                    breaker = true;
                    break;
                }
            }
            if (breaker) {
                continue;
            }
        }

        if (!remainingComponents.isEmpty()) {
            final CircuitComponent comp = remainingComponents.get(0);
            final Terminal term = comp.getTerminals().get(0);
            final CurrentTopologyNode node = new CurrentTopologyNode(term.getSheetPosition());
            node.addPotentialAreaID(term.getIndex());
            addNode(node);

            initEdges(node);

            fixSingleLoops();


        }
    }

    private void createNodesFromIntersection(final List<TerminalConnector> termConns, final PotentialArea pArea) {
        for (TerminalConnector connector : termConns) {
            if (!(connector.getConnector()._subPaths.isEmpty())) {
                for (Point subPoint : connector.getConnector()._subPaths) {
                    final CurrentTopologyNode node = new CurrentTopologyNode(subPoint);
                    node.addPotentialAreaID(_potentialAreas.indexOf(pArea));
                    addNode(node);

                }
            }
        }
    }

    private void createNodesFromMultipleTerminals(final List<TerminalComponent> termComps,
            final List<TerminalConnector> termConns, final PotentialArea pArea) {
        final HashMap<Point, Integer> pointMap = new LinkedHashMap<Point, Integer>();

        for (TerminalComponent termComp : termComps) {
            if (pointMap.containsKey(termComp.getSheetPosition())) {
                pointMap.put(termComp.getSheetPosition(), pointMap.get(termComp.getSheetPosition()) + 1);
            } else {
                pointMap.put(termComp.getSheetPosition(), 1);
            }

        }

        for (TerminalConnector termConn : termConns) {
            for (Point termPoint : termConn.getConnector().getTrimmedCoords()) {
                if (pointMap.containsKey(termPoint)) {
                    pointMap.put(termPoint, pointMap.get(termPoint) + 1);
                } else {
                    pointMap.put(termPoint, 1);
                }
            }
        }

        for (Map.Entry<Point, Integer> entry : pointMap.entrySet()) {
            if (entry.getValue() > 2) {
                final CurrentTopologyNode node = new CurrentTopologyNode(entry.getKey());
                node.addPotentialAreaID(_potentialAreas.indexOf(pArea));
                addNode(node);
            }
        }
    }

    private void initPathMeters() {
        for (CurrentTopologyNode node : _nodes) {
            for (CurrentTopologyEdge edge : node._edges) {
                if ((edge == null) || (edge.getDomPos() != -1)) {
                    continue;
                } else {
                    final CurrentPathMeter meter = new CurrentPathMeter(edge, node, null);

                    if (!(existingMeter(meter))) {
                        edge.addMeter(meter);
                        _pathMeters.put(edge, meter);
                    }
                }
            }
        }
    }

    private void checkForAnimateConnectors(final LoopParameters params) {
        for (AnimateConnector aniConn : params._animations) {
            final float length = aniConn.calculateHalfConnectorLength(params._curPoint);
            if ((aniConn.containsPoint(params._curPoint))
                    && rightObservation(params._searchDirection, aniConn._direction, length)) {
                final CurrentTopologyEdge edge = new CurrentTopologyEdge(params._node);
                params._node.addEdge(edge, params._searchDirection.ordinal());
                addEdge(edge);

                edge.addAnimation(aniConn);
                edge.addPathPoint(params._curPoint);

                params._curPoint = aniConn.getOtherTerminal(params._curPoint);

                edge.addPathPoint(params._curPoint);

                params._lastAdded = addedType.CONNECTOR;
                edge.addOrder(params._lastAdded);
                break;
            }
        }
    }

    private void checkForComponents(final LoopParameters params) {
        for (TerminalComponent termComp : _potentialAreas.get(params._potID).getTermComponents()) {
            if ((termComp.getSheetPosition().equals(params._node.getPos()))
                    && (rightObservation(params._searchDirection, calculateComponentDirection(termComp)
                    , calculateComponentMean(params._node.getPos(), termComp)))) {

                final CurrentTopologyEdge edge = new CurrentTopologyEdge(params._node);
                params._node.addEdge(edge, params._searchDirection.ordinal());
                addEdge(edge);
                final CircuitComponent element = termComp.getCircuitComponent();
                edge.addComponent(element);

                edge.addPathPoint(params._curPoint);

                params._curPoint = getOtherComponentTerminal(termComp).getSheetPosition();

                edge.addPathPoint(params._curPoint);

                params._lastAdded = addedType.COMPONENT;
                edge.addOrder(params._lastAdded);

                params._potID = getOtherComponentTerminal(termComp).getIndex(); // new PotentialArea

                break;
            }
        }
    }

    private LoopParameters addNextComponent(final LoopParameters params) {
        for (TerminalComponent termComp : _potentialAreas.get(params._potID).getTermComponents()) {
            if (termComp.getSheetPosition().equals(params._curPoint)) {
                final CircuitComponent element = termComp.getCircuitComponent();
                params._curEdge.addComponent(element);

                params._curPoint = getOtherComponentTerminal(termComp).getSheetPosition();
                params._lastAdded = addedType.COMPONENT;
                params._curEdge.addOrder(params._lastAdded);
                params._curEdge.addPathPoint(params._curPoint);

                params._potID = getOtherComponentTerminal(termComp).getIndex(); // new PotentialArea
//                                animations = getAnimations(_potentialAreas.get(potID)); // update Animation Parts

                params._securityBreak = false;
                break;
            }
        }

        return params;
    }

    private LoopParameters addNextAnimateConnector(final LoopParameters params, final AnimateConnector nextAnimation) {
        params._curEdge.addAnimation(nextAnimation);

        params._curPoint = nextAnimation.getOtherTerminal(params._curPoint);
        params._lastAdded = addedType.CONNECTOR;
        params._curEdge.addOrder(params._lastAdded);
        params._curEdge.addPathPoint(params._curPoint);
        params._securityBreak = false;

        return params;
    }

    private void updateEndNode(final LoopParameters params) {
        if (_nodePositions.contains(params._curPoint)) { // && !(curPoint.equals(node.getPos()))
            final CurrentTopologyNode secondNode = _nodes.get(_nodePositions.indexOf(params._curPoint));

            params._curEdge.addNode(secondNode);
            Point relevantPoint;
            // Evaluate where to add
            if (params._lastAdded == addedType.CONNECTOR) {
                final AnimateConnector aniConn = params._curEdge.getAnimation(params._curEdge.getAnimations().size() - 1);

                relevantPoint = aniConn.getOtherTerminal(params._curPoint);
            } else {
                final CircuitComponent comp = params._curEdge.getComponent(params._curEdge.getComponents().size() - 1);
                final Terminal termComp = getOtherComponentTerminal(params._curPoint, comp);

                relevantPoint = termComp.getSheetPosition();
            }
            secondNode.addEdge(params._curEdge, Orientation.getDirection(secondNode.getPos(), relevantPoint).ordinal());

        }
    }

    public enum addedType {

        NONE,
        COMPONENT,
        CONNECTOR;
    }

    private final class LoopParameters {

        private Point _curPoint;
        private int _potID;
        private addedType _lastAdded = addedType.NONE;
        private List<AnimateConnector> _animations;
        private CurrentTopologyNode _node;
        private CurrentTopologyEdge _curEdge;
        private Orientation _searchDirection;
        private boolean _securityBreak = true;

        private LoopParameters(final CurrentTopologyNode node, final Orientation searchDirection) {
            _curPoint = node.getPos();
            _node = node;
            _potID = node.getID();
            _animations = getAnimations(_potentialAreas.get(_potID));
            _searchDirection = searchDirection;
        }
    }
}
