/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.geckocircuitsnew.animation.CurrentTopologyGraph.addedType;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.CurrentPathMeter;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zimmi
 */
public final class CurrentTopologyEdge {

    private final List<CircuitComponent> _components = new ArrayList<CircuitComponent>();
    private final List<AnimateConnector> _animation = new ArrayList<AnimateConnector>();
    private final List<addedType> _addingOrder = new ArrayList<addedType>(); 
    private CircuitComponent _domComp;
    private final List<CurrentTopologyNode> _nodes = new ArrayList<CurrentTopologyNode>(2);
    private int _domPos = -1;
    private final List<Point> _path = new ArrayList<Point>();
    private boolean _reversed = false;
    private CurrentPathMeter _pathMeter = null;

    public CurrentTopologyEdge(final CurrentTopologyNode node) {
        _nodes.add(node);
    }

    public void addAnimation(final AnimateConnector aniConn) {
        _animation.add(aniConn);
    }

    public List<AnimateConnector> getAnimations() {
        return _animation;
    }

    public AnimateConnector getAnimation(final int aniPos) {
        return _animation.get(aniPos);
    }

    void addComponent(final CircuitComponent element) {
        _components.add(element);
    }

    public void addNode(final CurrentTopologyNode node) {
        _nodes.add(node);
    }

    public List<CircuitComponent> getComponents() {
        return _components;
    }

    public CircuitComponent getComponent(final int index) {
        return _components.get(index);
    }

    void addOrder(final addedType lastAdded) {
        if (lastAdded == addedType.COMPONENT && !(_addingOrder.contains(lastAdded))) {
            _domComp = _components.get(0);

            _domPos = _addingOrder.size();

        }

        _addingOrder.add(lastAdded);


    }

    public CircuitComponent getDominantComponent() {
        return _domComp;
    }

    public Object getMainComponent() {
        if (getPathMeter() == null) {
            return getDominantComponent();
        } else {
            return getPathMeter();
        }
    }

    public void setAnimationState(final CurrentState state) {

        for (AnimateConnector aniConn : _animation) {
            aniConn.setAnimationState(state);
        }
    }

    public void addPathPoint(final Point curPoint) {
        _path.add(curPoint);
    }

    public void printPath() {
        if (_domComp == null) {
            System.out.print("\t" + _domComp + "\t" + _reversed + "\tEdgePath: ");
        } else {
            System.out.print("\t" + _domComp.getOwningElementName() + "\t" + _reversed + "\tEdgePath: ");
        }
        for (Point curPos : _path) {
            System.out.print("[" + curPos.x + ", " + curPos.y + "] --> ");
        }
        System.out.println("PathEnd");
    }

    public CurrentTopologyNode getNode(final int index) {
        return _nodes.get(index);
    }

    public CurrentTopologyNode getOtherNode(final CurrentTopologyNode node) {
        int index = _nodes.indexOf(node);
        if (index == 0) {
            index = 1;
        } else {
            index = 0;
        }
        return _nodes.get(index);
    }

    public void addMeter(final CurrentPathMeter meter) {
        _pathMeter = meter;
    }

    public void checkReversed() {
        if (_domPos != -1) {
            final List<Terminal> terms = _domComp.getTerminals();
            if (_path.indexOf(terms.get(0).getSheetPosition()) < _path.indexOf(terms.get(1).getSheetPosition())) {
                setReversed(false);
            } else {
                setReversed(true);
            }
        }

    }

    public void setReversed(final boolean state) {
        _reversed = state;
    }

    public int getDomPos() {
        return _domPos;
    }

    public List<Point> getPath() {
        return _path;
    }

    public CurrentPathMeter getPathMeter() {
        return _pathMeter;
    }

    public boolean isReversed() {
        return _reversed;
    }
}
