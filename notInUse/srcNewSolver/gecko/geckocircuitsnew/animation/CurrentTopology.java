/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.CurrentPathMeter;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zimmi
 */
public final class CurrentTopology {

    private final int _topologyHashCode;
    private final List<Object> _orderedComponents;
    private final List<CurrentState> _orderedCurrentStates = new ArrayList<CurrentState>();
    private final CurrentTopologyGraph _topologyGraph;

    public static void createIfNotPresentFabric(final CurrentTopologyContainer container, final double time) {
        int key = 0;

        for (CircuitComponent comp : container.getComponents()) {

            final double current = comp.getCurrent();
            final CurrentState state = CurrentState.getState(current);

            key = key + comp.hashCode() * state.hashCode();
        }

        if (!container.isTopologyChanged(key)) {
            return;
        }


        final CurrentTopology searchedTop = container._TPLObjects.get(key);
        if (searchedTop == null) {
            final CurrentTopology newTopology = new CurrentTopology(container, key);
            container.newTopology(newTopology);
            container.insertNewTopologyTime(time, newTopology);
            return;
        }

        container.insertNewTopologyTime(time, searchedTop);

    }

    private CurrentTopology(final CurrentTopologyContainer container, final int hashCode) {

        _topologyGraph = container.getGraph();
        _orderedComponents = container.getOrderedComponents();
        _topologyHashCode = hashCode;

        for (CircuitComponent comp : container.getComponents()) {
            final CurrentState state = CurrentState.getState(comp.getCurrent());
            _orderedCurrentStates.add(state);
        }

        for (CurrentPathMeter pathMeter : _topologyGraph.getPathMeters()) {
            _orderedCurrentStates.add(CurrentState.getState(pathMeter.calculateCurrent()));
        }
    }

    public int getTopologyHashCode() {
        return _topologyHashCode;
    }

    public void updateTopologyElements() {
        updateTopologyComponents();
        updateTopologyConnectors();

        CircuitSheet.globalSheet.repaint();
    }

    public void updateTopologyComponents() {
        for (int i = 0; i < _orderedComponents.size(); i++) {
            final Object obj = _orderedComponents.get(i);
            if (obj instanceof CircuitComponent) {
                ((CircuitComponent) obj).getElement().setAnimationState(_orderedCurrentStates.get(i));
            }
        }
    }

    private void updateTopologyConnectors() {
//       this.printTopology();
        for (CurrentTopologyEdge curEdge : _topologyGraph.getEdges()) {
            final int indexOfMain = _orderedComponents.indexOf(curEdge.getMainComponent());
            if (indexOfMain >= 0) {
                curEdge.setAnimationState(_orderedCurrentStates.get(indexOfMain));
            }

        }
    }
}
