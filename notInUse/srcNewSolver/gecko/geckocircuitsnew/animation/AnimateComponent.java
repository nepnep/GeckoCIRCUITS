/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuitsnew.animation;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 *
 * @author zimmi
 */
public final class AnimateComponent extends AbstractAnimate {

    public AnimateComponent(final JComponent parent, final int[] relCoords) {
        super(parent, relCoords);
    }

    @Override
    public void paint(final Graphics graphic, final int scaling) {
        final Graphics2D g2d = (Graphics2D) graphic;
        // Calculate each time in case of resize
        dpix = scaling;


        final int steps = _colors.length;
        synchronized (_colors) {
            for (int i = 0; i < steps; i++) {

                graphic.setColor(AbstractAnimate.ANIMATIONCOLOR);
                final AlphaComposite aComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
                g2d.setComposite(aComp);

                final int xPos = dpix * _startX;
                final int yPos = dpix * _startY;
                switch(_state) {
                    case NEGATIVE:
                        for (int n = 1; n <= _stopY - _startY; n++) {
                        final int[] xValues = new int[]{xPos - (2 * ARROWSIZE), xPos + (2 * ARROWSIZE), xPos};
                        final int[] yValues = shiftNegativeDirection(yPos, n);


                        graphic.fillPolygon(xValues, yValues, POLYSIZE);
                    }
                        break;
                    case POSITIVE:
                        for (int n = 0; n < Math.abs(_stopY - _startY); n++) {
                        final int[] xValues = new int[]{xPos - (2 * ARROWSIZE), xPos + (2 * ARROWSIZE), xPos};
                        final int[] yValues = shiftPositiveDirection(yPos, n);

                        graphic.fillPolygon(xValues, yValues, POLYSIZE);

                    }
                        break;
                    case ZERO:
                        // nothing do draw
                        break;
                    default:
                        assert false;
                }
                
            }
        }
        final AlphaComposite aComp = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);

        g2d.setComposite(aComp);
    }
}
