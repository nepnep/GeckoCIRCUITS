/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.CurrentPathMeter;
import gecko.GeckoCIRCUITS.scope.TimeSeriesVariableArray;
import gecko.GeckoCIRCUITS.terminal.PotentialArea;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import java.util.*;

/**
 *
 * @author zimmi
 */
public final class CurrentTopologyContainer {

    private final List<CircuitComponent> _components = new ArrayList<CircuitComponent>();
    private List<CurrentPathMeter> _pathMeters = new ArrayList<CurrentPathMeter>();
    private final TimeSeriesVariableArray _TPLSwitchTimeOrdered = new TimeSeriesVariableArray();
    private final List<CurrentTopology> _TPLOrdered = new ArrayList<CurrentTopology>();
    final Map<Integer, CurrentTopology> _TPLObjects = new LinkedHashMap<Integer, CurrentTopology>();
    private List<PotentialArea> _potentialAreas;
    private static final int COMPONENTWIDTH = 2;
    private CurrentTopologyGraph _topologyGraph;
    private List _orderedComponents;

    public void initTopologyContainer(final List<CircuitComponent> circuitComponents) {
        resetTopologyContainer();
        for (CircuitComponent comp : circuitComponents) {

            _components.add(comp);

            comp.getElement().initAnimation(new int[]{0, -COMPONENTWIDTH, 0, COMPONENTWIDTH});


        }

        initTopologyConnectors();
        initTopologyGraph();
    }

    public void newTopology(final CurrentTopology ctopo) {
        _TPLObjects.put(ctopo.getTopologyHashCode(), ctopo);
    }

    public boolean isTopologyChanged(final int hashcode) {
        if (_TPLOrdered.isEmpty()) {
            return true;
        }

        return _TPLOrdered.get(_TPLOrdered.size() - 1).getTopologyHashCode() != hashcode;
    }

    public void insertNewTopologyTime(final double time, final CurrentTopology tpl) {
        _TPLOrdered.add(tpl);
        _TPLSwitchTimeOrdered.add(time);
    }

    public int getIndexByTime(final double time) {
        return _TPLSwitchTimeOrdered.findTimeIndex(time, _TPLSwitchTimeOrdered.getMaximumIndex());

    }

    public CurrentTopology getTopoByIndex(final int index) {
        return _TPLOrdered.get(index);
    }

    public double getTimeByIndex(final int index) {
        return _TPLSwitchTimeOrdered.getValue(index);
    }

    public CurrentTopology getTopoByTime(final double time) {
        return _TPLOrdered.get(getIndexByTime(time));
    }

    public int getMaxIndex() {
        return _TPLOrdered.size();
    }

    public void setPotentialAreas(final List<PotentialArea> pac) {
        _potentialAreas = pac;
    }

    void initTopologyConnectors() {
        int index = 0;
        for (PotentialArea parea : _potentialAreas) {
            ConnectorPath.createConnectorPaths(parea, index++);
        }
        for (PotentialArea parea : _potentialAreas) {
            for (TerminalConnector term : parea.getTermConnectors()) {
                term.getConnector().initAnimationParts();
            }
        }
    }

    private void initTopologyGraph() {
        _topologyGraph = new CurrentTopologyGraph(_potentialAreas, _components);
        _topologyGraph.buildGraph();

        _pathMeters = _topologyGraph.getPathMeters();
    }

    public CurrentTopologyGraph getGraph() {
        return _topologyGraph;
    }

    List<CurrentPathMeter> getPathMeters() {
        return _pathMeters;
    }

    private void resetTopologyContainer() {
        _TPLObjects.clear();
        _TPLOrdered.clear();
        _TPLSwitchTimeOrdered.clear();
    }

    public List<CircuitComponent> getComponents() {
        return _components;
    }

    public List<Object> getOrderedComponents() {
        if (_orderedComponents == null) {
            final List tmp = new ArrayList<Object>();
            tmp.addAll(_components);
            tmp.addAll(getGraph().getPathMeters());
            _orderedComponents = Collections.unmodifiableList(tmp);
        }
        return _orderedComponents;
    }
}
