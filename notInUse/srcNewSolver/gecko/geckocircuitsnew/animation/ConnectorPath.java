/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

import gecko.GeckoCIRCUITS.terminal.PotentialArea;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zimmi
 */
public final class ConnectorPath {

    private ConnectorPath() {
        // unused constructor -> utility class
    }

    static void createConnectorPaths(final PotentialArea parea, final int potentialID) {
        createSubPath(parea, potentialID);
    }

    private static void createSubPath(final PotentialArea pArea, final int potIndex) {

        // Split up Terminals into TerminalComponents and TerminalConnectors
        splitTerminals(pArea, potIndex);

        final List<TerminalConnector> connectors = pArea.getTermConnectors();
        final List<TerminalComponent> components = pArea.getTermComponents();


        // Create SubPaths for each connector in this PotentialArea
        for (TerminalConnector curConnector : connectors) {
            List<Point> subCompPaths = new ArrayList<Point>();
            List<Point> subConnPaths = new ArrayList<Point>();

            final ArrayList<Point> pathpoints = curConnector.getConnector().getTrimmedCoords();

            createCompIntersectionPoints(components, pathpoints, subCompPaths);

            createConnectorIntersectionPoints(curConnector, connectors, pathpoints, subConnPaths);

            subCompPaths = sortPath(curConnector.getConnector().getTrimmedCoords().get(0), subCompPaths);
            subConnPaths = sortPath(curConnector.getConnector().getTrimmedCoords().get(0), subConnPaths);

            curConnector.getConnector()._subPaths = mergeIntersectionPoints(curConnector.getConnector().getTrimmedCoords(),
                    subCompPaths, subConnPaths);

        }
    }

    private static void splitTerminals(final PotentialArea pArea, final int potIndex) {

        final ArrayList<TerminalConnector> connectors = new ArrayList<TerminalConnector>();
        final ArrayList<TerminalComponent> components = new ArrayList<TerminalComponent>();
        int skip = 0; // Used for skipping the second TerminalConnector
        for (Terminal terms : pArea._allTerminals) {
            if ((terms instanceof TerminalConnector) && (skip % 2 == 0) && (!(connectors.contains((TerminalConnector) terms)))) {
                connectors.add((TerminalConnector) terms);
                pArea.addTermConnector((TerminalConnector) terms);
            }
            skip++;

            if ((terms instanceof TerminalComponent) && (terms.getIndex() == potIndex)) {
                if (!(components.contains((TerminalComponent) terms))) {
                    components.add((TerminalComponent) terms);
                    pArea.addTermComponent((TerminalComponent) terms);
                }
                skip--;
            }
        }
    }

    private static boolean isBetweenConnTerms(final Point componentPosition, final List<Point> pathCoords) {

        if (pathCoords.get(0).equals(componentPosition) || pathCoords.get(pathCoords.size() - 1).equals(componentPosition)) {
            return false;
        }


        for (int i = 0; i < pathCoords.size() - 1; i++) {
            if (((pathCoords.get(i).x - pathCoords.get(i + 1).x) == 0)
                    && (checkYDirection(i, pathCoords, componentPosition))) { // path points in y direction

                return true;
            }
            if (((pathCoords.get(i).y - pathCoords.get(i + 1).y) == 0)
                    && (checkXDirection(i, pathCoords, componentPosition))) {

                return true;
            }
        }

        return false;
    }

    private static boolean checkYDirection(final int index, final List<Point> pathCoords, final Point componentPosition) {
        final int ymin = Math.min(pathCoords.get(index).y, pathCoords.get(index + 1).y);
        final int ymax = Math.max(pathCoords.get(index).y, pathCoords.get(index + 1).y);

        for (int y = ymin; y <= ymax; y++) {
            if ((y == componentPosition.y) && (pathCoords.get(index).x == componentPosition.x)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkXDirection(final int index, final List<Point> pathCoords, final Point componentPosition) {
        final int xmin = Math.min(pathCoords.get(index).x, pathCoords.get(index + 1).x);
        final int xmax = Math.max(pathCoords.get(index).x, pathCoords.get(index + 1).x);

        for (int x = xmin; x <= xmax; x++) {
            if ((x == componentPosition.x) && (pathCoords.get(index).y == componentPosition.y)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isExistingPathPoint(final List<Point> subPathPoints, final Point check) {
        if (subPathPoints.isEmpty()) {
            return false;
        }

        for (Point coord : subPathPoints) {
            if (coord.equals(check)) {
                return true;
            }
        }

        return false;
    }

    /**
     * This Function is used to merge the individual intersection points on a
     * Connector. The Intersection points are splited up into Components and
     * Connector intersection points
     *
     * @param trimmedCoords Path Coordinate on the CircuitSheet of the Connector
     * @param subCompPaths Component intersection points
     * @param subConnPaths Connector intersection points
     * @return merged and sorted intersection points of Connectors and
     * Components
     */
    private static List<Point> mergeIntersectionPoints(final List<Point> trimmedCoords, final List<Point> subCompPaths,
            final List<Point> subConnPaths) {

        List<Point> subPath = new ArrayList<Point>();
        final Point start = trimmedCoords.get(0);

        if (subCompPaths.isEmpty()) {
            if (subConnPaths.isEmpty()) {
                return subPath;
            } else {
                subPath = subConnPaths;
            }
        } else {
            if (subConnPaths.isEmpty()) {
                subPath = subCompPaths;
            } else {
                final List<Point> allIntersectionPoints = new ArrayList();
                allIntersectionPoints.addAll(subCompPaths);
                allIntersectionPoints.addAll(subCompPaths);

                subPath = sortPath(start, allIntersectionPoints);

            }
        }

        return subPath;
    }

    private static List<Point> sortPath(final Point start, final List<Point> subPoints) {
        List<Point> subPointsSorted = new ArrayList<Point>();
        final int maxIndex = subPoints.size();

        if (subPoints.isEmpty()) {
            subPointsSorted = subPoints;
        } else {
            for (int i = 0; i < maxIndex; i++) {
                int pos = 0;
                for (Point curPoint : subPoints) {
                    if (start.distance(curPoint) < start.distance(subPoints.get(pos))) {
                        pos = subPoints.indexOf(curPoint);
                    }
                }
                if (!subPointsSorted.contains(subPoints.get(pos))) {
                    subPointsSorted.add(subPoints.get(pos));
                }
                subPoints.remove(pos);
            }
        }

        return subPointsSorted;
    }

    private static void createCompIntersectionPoints(final List<TerminalComponent> components,
            final List<Point> pathpoints, final List<Point> subCompPaths) {
        for (TerminalComponent comp : components) {
            final Point compPos = comp.getSheetPosition();
            if ((isBetweenConnTerms(compPos, pathpoints)) && (!(isExistingPathPoint(subCompPaths, compPos)))) {
                subCompPaths.add(compPos);
            }
        }
    }

    private static void createConnectorIntersectionPoints(final TerminalConnector curConnector,
            final List<TerminalConnector> allConnectors, final List<Point> pathpoints, final List<Point> subConnPaths) {
        for (TerminalConnector otherConnectors : allConnectors) {
            if (!otherConnectors.equals(curConnector)) {
                for (Terminal termConnector : otherConnectors.getConnector().getTerminals()) {
                    final Point termPos = termConnector.getSheetPosition();
                    if ((isBetweenConnTerms(termPos, pathpoints)) && (!(isExistingPathPoint(subConnPaths, termPos)))) {
                        subConnPaths.add(termPos);

                    }
                }
            }
        }
    }
}
