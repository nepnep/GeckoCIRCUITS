/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuitsnew.animation;

/**
 *
 * @author zimmi
 *
 * Possible states: 0 zero or minimal current 1 negative current 2 positive
 * current
 */
public enum CurrentState {

    POSITIVE,
    NEGATIVE,
    ZERO;
    
    private static final double DEFAULT_THRES = 1e-2;
    private static double threshold = DEFAULT_THRES;

    public static void setThreshold(final double value) {
        threshold = Math.abs(value);
    }

    public static CurrentState getState(final double current) {
        if (Math.abs(current) <= threshold) {
            return ZERO;
        }

        if (current <= threshold) {
            return NEGATIVE;
        } else {
            return POSITIVE;
        }
    }

    public static CurrentState getOppositeState(final CurrentState originalState) {
        switch (originalState) {
            case POSITIVE:
                return NEGATIVE;
            case NEGATIVE:
                return POSITIVE;
            case ZERO:
                return ZERO;
            default:
                assert false;
                return ZERO;
        }
    }
}
