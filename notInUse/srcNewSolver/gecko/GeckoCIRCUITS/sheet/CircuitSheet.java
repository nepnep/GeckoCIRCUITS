/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.sheet;

import Utilities.ModelMVC.ModelMVC;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Scrollable;
import geckocircuitsnew.Model;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author andy
 */
public class CircuitSheet extends JPanel implements MouseListener, Scrollable {

    public static CircuitSheet globalSheet;
    
    public static final ModelMVC<Integer> _dpix = new ModelMVC<Integer>(12);
    public static CircuitSheet superSheet = null;
    public final ModelMVC<Integer> _sheetSizeX = new ModelMVC<Integer>(100);
    public final ModelMVC<Integer> _sheetSizeY = new ModelMVC<Integer>(100);
    public int paintExtensionX = 0;
    public int paintExtensionY = 0;
    public Model sheetModel; //Model that sheet is displaying (Andrija)
    // the sheet that is actually displayed
    public static CircuitSheet activeSheet;
    private static JScrollPane _scrollPane;

    public CircuitSheet(int sizeX, int sizeY) {
        this.setSize(sizeX, sizeY);

        ActionListener repaintListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        };



        _sheetSizeX.addModelListener(repaintListener);
        _sheetSizeY.addModelListener(repaintListener);
        _dpix.addModelListener(repaintListener);
        this.setLayout(null);
        //ElementLabel testLabel = new ElementLabel();
        //testLabel.addMouseListener(this);
        //this.add(testLabel);

        this.setOpaque(false);

        sheetModel = null;


    }

    //circuit sheet constructor which associates model with it (Andrija)
    public CircuitSheet(int sizeX, int sizeY, Model model) {
        this.setSize(sizeX, sizeY);
        
        globalSheet = this;

        ActionListener repaintListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        };


        _sheetSizeX.addModelListener(repaintListener);
        _sheetSizeY.addModelListener(repaintListener);
        _dpix.addModelListener(repaintListener);
        this.setLayout(null);
        //ElementLabel testLabel = new ElementLabel();
        //testLabel.addMouseListener(this);
        //this.add(testLabel);

        this.setOpaque(false);

        sheetModel = model;


    }

    public static void setActiveSheet(CircuitSheet sub) {
        activeSheet = sub;
        superSheet = (CircuitSheet) _scrollPane.getViewport().getView();
        _scrollPane.getViewport().setView(sub);

        CircuitSheet.activeSheet.repaint();
    }

    public void paintComponent(Graphics2D g2d) {
        int dpix = _dpix.getValue();

        g2d.setColor(Color.decode("0xaaaaaa"));  // zwischen GRAY (808080) und LIGHTGREY (d3d3d3)
        for (int i = 0; i < _sheetSizeX.getValue(); i++) {
            for (int j = 0; j < _sheetSizeY.getValue(); j++) {
                g2d.drawLine(dpix * i, dpix * j, dpix * i, dpix * j);
            }
        }

    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        if (activeSheet == null) {
            activeSheet = this;
        }

        activeSheet.paintComponent(g2d);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(_sheetSizeX.getValue() * _dpix.getValue() + paintExtensionX,
                _sheetSizeY.getValue() * _dpix.getValue() + paintExtensionY);
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return super.getPreferredSize();
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 40;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 40;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void setInsertViewPort(JScrollPane jScrollPaneCircuit) {
        _scrollPane = jScrollPaneCircuit;
        jScrollPaneCircuit.getViewport().setView(this);
    }

}
