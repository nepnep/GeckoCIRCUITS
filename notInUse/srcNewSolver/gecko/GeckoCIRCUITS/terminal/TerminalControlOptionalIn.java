/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.elements.ElementControl;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class TerminalControlOptionalIn extends TerminalControlIn {

    public final ModelMVC<Boolean> visible = new ModelMVC<Boolean>(false);

    public TerminalControlOptionalIn(int relX, int relY, ElementControl element) {
        super(relX, relY, element);
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        if (visible.getValue()) {
            super.paint(g, dpix);
        }
    }

}
