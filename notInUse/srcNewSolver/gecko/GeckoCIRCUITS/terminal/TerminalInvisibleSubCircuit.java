/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * this terminal represents an "unvisible" terminal, which cannot be merged
 with other potential areas.
 */

package gecko.GeckoCIRCUITS.terminal;

import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author andy
 */
public class TerminalInvisibleSubCircuit extends TerminalComponent {
    private int _potentialIndex;

    public TerminalInvisibleSubCircuit(ElementInterface element) {
        super(element, 0, 0);
    }

    public Point getSheetPosition() {
        return new Point(-1999, -1099);
    }

    public boolean isConnected(Terminal term) {
        return false;
    }

    public void paint(Graphics2D g, int dpix) {
    }
    
    

    
}
