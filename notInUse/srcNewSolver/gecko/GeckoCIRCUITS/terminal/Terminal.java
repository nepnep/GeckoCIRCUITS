/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Graphics2D;
import java.awt.Point;

public abstract class Terminal {

    private CircuitSheet _circuitSheet;

    public Terminal(CircuitSheet sheet) {
        _circuitSheet = sheet;
        assert _circuitSheet != null;
    }

    public final int PAINT_SIZE = 4;
    
    abstract public Point getSheetPosition();
    abstract public void paint(Graphics2D g, int dpix);
    abstract public String getLabel();
    abstract public void setLabel(String newLabel);

    public abstract void setIndex(int potentialIndex);
    public abstract int getIndex();

    public abstract int getRelX();
    public abstract int getRelY();

    public abstract ConnectionType getConnectionType();

    public CircuitSheet getSheet() {
        return _circuitSheet;
    }

    public boolean isTermConnected(Terminal term2) {
//        if(term2.getConnectionType() != this.getConnectionType()) {
//            return false;
//        }
        
        if(getSheet() != term2.getSheet()) {
            return false;
        }

        if ((!getLabel().isEmpty()) && getLabel().equals(term2.getLabel())) {
            return true;
        }

        if (getSheetPosition().equals(term2.getSheetPosition())) {
            return true;
        }

        return false;
    }
}