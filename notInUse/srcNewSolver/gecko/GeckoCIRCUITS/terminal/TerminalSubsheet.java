/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.terminal;

import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.elements.ElementSubSheet;
import gecko.GeckoCIRCUITS.elements.ElementTerminalCircuit;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class TerminalSubsheet extends Terminal {
    private int _relX;
    private int _relY;
    private int _potentialIndex;
    private ElementInterface _parentElement;
    public ElementTerminalCircuit _termCircuit;


    public TerminalSubsheet(ElementInterface parent, ElementTerminalCircuit termCircuit, int relX, int relY) {
        super(parent.getSheet());
        _parentElement = parent;
        _termCircuit = termCircuit;
        _relX = relX;
        _relY = relY;
    }


    public Point getSheetPosition() {
        Orientation orientierung = _parentElement.getOrientation();

        int x1 = 0;
        int y1 = 0;

        switch (orientierung) {
            case SOUTH:
                x1 = _parentElement.x.getValue() + _relX;
                y1 = _parentElement.y.getValue() + _relY;
                break;
            case NORTH:
                x1 = _parentElement.x.getValue() - _relX;
                y1 = _parentElement.y.getValue() - _relY;
                break;
            case EAST:
                x1 = _parentElement.x.getValue() + _relY;
                y1 = _parentElement.y.getValue() - _relX;
                break;
            case WEST:
                x1 = _parentElement.x.getValue() - _relY;
                y1 = _parentElement.y.getValue() + _relX;
                break;
            default:
                assert false;
        }

        return new Point(x1, y1);

    }


    public void paint(Graphics2D g, int dpix) {
        //super.paint(g, dpix);
        g.fillOval(_relX * dpix - PAINT_SIZE / 2, _relY * dpix - PAINT_SIZE / 2, PAINT_SIZE, PAINT_SIZE);
    }

    public String getLabel() {
        return "";
    }

    public void setLabel(String newLabel) {
        
    }

    public void setIndex(int potentialIndex) {
        _potentialIndex = potentialIndex;
    }

    public int getIndex() {
        return _potentialIndex;
    }

    public int getRelX() {
        return _relX;
    }

    public int getRelY() {
        return _relY;
    }

    @Override
    public String toString() {
        return super.toString() + " " + _termCircuit.term1.getSheetPosition() 
                + " " + _termCircuit.term2.getSheetPosition()
                + " " + _parentElement.x.getValue()
                + " " + _parentElement.y.getValue();
    }


    @Override
    public boolean isTermConnected(Terminal term2) {
        if (term2 instanceof TerminalSubsheet) {
            TerminalSubsheet tss2 = (TerminalSubsheet) term2;
            if (_termCircuit == tss2._termCircuit) {
                return true;
            }
        }
        return super.isTermConnected(term2);
    }

    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.POWERCIRCUIT;
    }

}
