/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.terminal;

import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class PotentialArea {

    public final ArrayList<Terminal> _allTerminals = new ArrayList<Terminal>();
    String _potentialLabel = "";
    private ArrayList<TerminalComponent> _terminalComponents = new ArrayList<TerminalComponent>();
    private ArrayList<TerminalConnector> _terminalConnectors = new ArrayList<TerminalConnector>();

    
    /*
     * returns true if some of the terminals in the potential areas have a connection via
     * a component
     */
    public static boolean hasComponentConnection(PotentialArea potArea1, PotentialArea potArea2) {
        for (Terminal term1 : potArea1._allTerminals) {
            if (term1 instanceof TerminalComponent) {
                for (Terminal term2 : potArea2._allTerminals) {
                    if (term2 instanceof TerminalComponent) {
                        TerminalComponent comTerm1 = (TerminalComponent) term1;
                        TerminalComponent comTerm2 = (TerminalComponent) term2;
                        if (comTerm1.getCircuitComponent() == comTerm2.getCircuitComponent()) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
    
//    public ArrayList<Boolean> hasComponentConnection(ArrayList<PotentialArea> potAreas) {
//        ArrayList<Boolean> resultConnections = new ArrayList<Boolean>();
//        
//        System.out.print("PotentialAreaConnectionVector: [");
//        for(PotentialArea potArea : potAreas) {
//            resultConnections.add(hasComponentConnection(this, potArea));
//            System.out.print(hasComponentConnection(this, potArea) + " , ");
//        }
//        System.out.println("]");
//        
//        return resultConnections;
//    }
    
    public PotentialArea(Terminal term) {
        _allTerminals.add(term);
    }

    public PotentialArea(ArrayList<Terminal> terms) {
        _allTerminals.addAll(terms);
    }

    // merge connectors that are on the same potential area
    public static void mergeConnectors(ArrayList<PotentialArea> allPotentials) {

        int oldSize = allPotentials.size();

        do {
            ArrayList<PotentialArea> toDelete = new ArrayList<PotentialArea>();

            for (int i = 0; i < allPotentials.size(); i++) {
                for (int j = i + 1; j < allPotentials.size(); j++) {
                    PotentialArea pot1 = allPotentials.get(i);
                    PotentialArea pot2 = allPotentials.get(j);

                    if (pot1.hasConnection(pot2)) {
                        pot1.swallow(pot2);
                        toDelete.add(pot2);
                    }
                }
            }

            oldSize = allPotentials.size();
            allPotentials.removeAll(toDelete);


        } while (oldSize != allPotentials.size());

//        for (PotentialArea pot : allPotentials) {
//            System.out.println("----------------");
//            for (ElementConnection con : pot._allConnectors) {
//                System.out.println("con: " + con.getAnfangsKnoten()[0] + " " + con.getAnfangsKnoten()[1] + " "
//                        + con.getEndKnoten()[0] + " " + con.getEndKnoten()[1]);
//            }
//            for (Terminal term : pot._allTerminals) {
//                System.out.println("term: " + term);
//            }
//            System.out.println("++++++++++++++++");
//        }
//        System.out.println(allPotentials.size());

    }

    private boolean hasConnection(PotentialArea pot2) {
        assert pot2 != this;

        for (Terminal term : this._allTerminals) {
            for (Terminal term2 : pot2._allTerminals) {
                if (term.isTermConnected(term2)) {
                    if(term2.isTermConnected(term))
                        return true;
                }
            }
        }

        return false;
    }

    private void swallow(PotentialArea pot2) {

        _allTerminals.addAll(pot2._allTerminals);
        for (Terminal term : _allTerminals) {
            if (!term.getLabel().isEmpty()) {
                setAllPotentialLabels(term.getLabel());
            }
        }
        pot2._allTerminals.clear();


    }

    private void setAllPotentialLabels(String label) {
        for (Terminal term : _allTerminals) {
            term.setLabel(label);
        }

        _potentialLabel = label;
    }

    public void addTermConnector(TerminalConnector terminalConnector) {
        _terminalConnectors.add(terminalConnector);
    }

    public void addTermComponent(TerminalComponent terminalComponent) {
        _terminalComponents.add(terminalComponent);
    }

    public ArrayList<TerminalComponent> getTermComponents() {
        return _terminalComponents;
    }

    public ArrayList<TerminalConnector> getTermConnectors() {
        return _terminalConnectors;
    }
    



}
