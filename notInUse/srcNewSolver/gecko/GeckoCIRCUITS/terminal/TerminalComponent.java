/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.elements.ElementVoltage;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
public class TerminalComponent extends Terminal {


    public final ModelMVC<String> terminalLabel = new ModelMVC<String>("");

    private int _relX = -2;
    private int _relY = 0;    
    private ElementInterface _element;

    private int _potentialIndex = -1;

    private CircuitComponent _circuitComponent = null;

    public TerminalComponent(ElementInterface element, int relX, int relY) {
        super(element.getSheet());
        _relX = relX;
        _relY = relY;
        _element = element;        
    }

    public void setCircuitComponent(CircuitComponent comp) {
        _circuitComponent = comp;
    }

    public CircuitComponent getCircuitComponent() {
        assert _circuitComponent != null;
        return _circuitComponent;
    }

    public Point getScreenPosition(int dpix) {        
        return new Point((_relX + _element.x.getValue()) * dpix, (_relY + _element.y.getValue()) * dpix);
    }

    @Override
    public String toString() {
        return  super.toString()  + " " + getIndex();
    }

    public Point getSheetPosition() {
        Orientation orientierung = _element.getOrientation();

        int x1 = 0;
        int y1 = 0;
               
        switch (orientierung) {
            case SOUTH:
                x1 = _element.x.getValue() + _relX;
                y1 = _element.y.getValue() + _relY;
                break;
            case NORTH:
                x1 = _element.x.getValue() - _relX;
                y1 = _element.y.getValue() - _relY;
                break;
            case EAST:
                x1 = _element.x.getValue() + _relY;
                y1 = _element.y.getValue() - _relX;
                break;
            case WEST:
                x1 = _element.x.getValue() - _relY;
                y1 = _element.y.getValue() + _relX;
                break;
            default:
                assert false;
        }
        
        return new Point(x1, y1);

    }


    public void paint(Graphics2D g, int dpix) {
        g.setColor(Typ.colorForeGroundElement);
        g.fillOval(_relX * dpix - PAINT_SIZE / 2, _relY * dpix - PAINT_SIZE / 2, PAINT_SIZE, PAINT_SIZE);
        //g.drawString( " " + getIndex(), _relX * dpix - PAINT_SIZE / 2 +5, _relY * dpix - PAINT_SIZE / 2 +5);
        
    }

    public String getLabel() {
        return terminalLabel.getValue();
    }

    public void setLabel(String newLabel) {
        terminalLabel.setValue(new String(newLabel));
    }

    public void setIndex(int potentialIndex) {
        _potentialIndex = potentialIndex;
    }

    public int getIndex() {
        return _potentialIndex;
    }

    public int getRelX() {
        return _relX;
    }

    public int getRelY() {
        return _relY;
    }

    @Override
    public ConnectionType getConnectionType() {
        if(_element instanceof Terminable) {
            return ((Terminable) _element).getConnectionType();
        } else {
            assert false;
        }
        return null;
    }

}
