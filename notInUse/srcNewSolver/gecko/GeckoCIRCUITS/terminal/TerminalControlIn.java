/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.controlNew.ControlPotentialValue;
import gecko.GeckoCIRCUITS.elements.ElementControl;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class TerminalControlIn extends TerminalControl {


    public final ModelMVC<String> terminalLabel = new ModelMVC<String>("");

    private int _relX, _relY;
    private final ElementControl _controlElement;

    private int _terminalIndex = -1;
    
    public TerminalControlIn(int relX, int relY, ElementControl controlElement) {
        super(controlElement.getSheet());
        _relX = relX;
        _relY = relY;
        _controlElement = controlElement;
    }

    @Override
    public String toString() {
        return "TerminalControlIn";
    }
    
    public Point getSheetPosition() {
        return new Point(_controlElement.x.getValue() + _relX, _controlElement.y.getValue() + _relY);
    }

    public void paint(Graphics2D g, int dpix) {

        g.setColor(Color.green);
        g.fillOval(_relX * dpix - PAINT_SIZE / 2, _relY * dpix - PAINT_SIZE / 2, PAINT_SIZE, PAINT_SIZE);
        g.drawLine(_relX*dpix, _relY * dpix, _relX*dpix+ dpix * (Math.abs(_relX))  , _relY * dpix);
    }


    public String getLabel() {
        return terminalLabel.getValue();
    }

    public void setLabel(String newLabel) {
        terminalLabel.setValue(new String(newLabel));
    }

    public void setIndex(int terminalIndex) {
        _terminalIndex = terminalIndex;
    }

    public int getIndex() {
        return _terminalIndex;
    }

    public final double getValue() {
        return _potArea._value;
    }

    public int getRelX() {
        return _relX;
    }

    public int getRelY() {
        return _relY;
    }
}
