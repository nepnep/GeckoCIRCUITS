/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.controlNew.ControlPotentialValue;
import gecko.GeckoCIRCUITS.elements.ElementControl;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class TerminalControlOut extends TerminalControl {

    public final ModelMVC<String> terminalLabel = new ModelMVC<String>("");
    
    private int _relX, _relY;
    private final ElementControl _controlElement;
    private int _terminalIndex;
    
    public TerminalControlOut(int relX, int relY, ElementControl controlElement) {
        super(controlElement.getSheet());
        _relX = relX;
        _relY = relY;
        _controlElement = controlElement;
    }

    @Override
    public String toString() {
        return "TerminalControlOut";
    }



    public final void writeValue(double value, double time) {
        _potArea.setValue(value, time);
    }


    public Point getSheetPosition() {
        return new Point(_controlElement.x.getValue() + _relX, _controlElement.y.getValue() + _relY);
    }

    public void paint(Graphics2D g, int dpix) {
        int[] xValues = new int[]{
            _relX * dpix,
            _relX * dpix - dpix / 2,
            _relX * dpix - dpix / 2,};

        int[] yValues = new int[]{
            _relY * dpix,
            _relY * dpix - dpix / 4,
            _relY * dpix + dpix / 4,};


        g.drawPolygon(xValues, yValues, 3);
    }

    public String getLabel() {
        return terminalLabel.getValue();
    }

    public void setLabel(String newLabel) {
        terminalLabel.setValue(new String(newLabel));
    }

    @Override
    public void setIndex(int terminalIndex) {
        _terminalIndex = terminalIndex;
    }

    @Override
    public int getIndex() {
        return _terminalIndex;
    }

    public int getRelX() {
        return _relX;
    }

    public int getRelY() {
        return _relY;
    }

    public void setRelX(int value) {
        _relX = value;
    }

}
