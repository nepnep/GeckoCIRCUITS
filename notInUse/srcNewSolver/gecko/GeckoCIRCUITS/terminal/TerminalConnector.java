/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.terminal;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.elements.ElementConnection;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class TerminalConnector extends Terminal {

    public final ModelMVC<String> terminalLabel = new ModelMVC<String>("");

    protected final ElementConnection _connector;
    private boolean _startNode;
    private int _terminalIndex;

    public TerminalConnector(ElementConnection verb, boolean startNode) {
        super(verb.getSheet());
        _connector = verb;
        _startNode = startNode;
    }


    public Point getSheetPosition() {

        int x = _connector.getAnfangsKnoten()[0];
        int y = _connector.getAnfangsKnoten()[1];

        if (!_startNode) {
            x = _connector.getEndKnoten()[0];
            y = _connector.getEndKnoten()[1];
        }
        return new Point(x, y);
    }

    public void setIndex(int potentialIndex) {
        _terminalIndex = potentialIndex;
    }

    public int getIndex() {
        return _terminalIndex;
    }

    @Override
    public String toString() {
        return super.toString() + " " + getSheetPosition();
    }

    public void paint(Graphics2D g, int dpix) {
        ArrayList<Integer> xPix = _connector.getXPix();
        ArrayList<Integer> yPix = _connector.getYPix();

        int posX = (xPix.get(0) - _connector.x.getValue()) * dpix;
        int posY = (yPix.get(0) - _connector.y.getValue()) * dpix;


        if (_startNode) {
            posX = (xPix.get(xPix.size() - 1) - _connector.x.getValue()) * dpix;
            posY = (yPix.get(yPix.size() - 1) - _connector.y.getValue()) * dpix;
        }

        g.fillOval(posX - PAINT_SIZE / 2, posY - PAINT_SIZE / 2, PAINT_SIZE, PAINT_SIZE);

    }



    @Override
    public int getRelX() {
        throw new UnsupportedOperationException();
    }

    public int getRelY() {
        throw new UnsupportedOperationException();
    }

    public String getLabel() {
        return terminalLabel.getValue();
    }

    public void setLabel(String newLabel) {
        terminalLabel.setValue(new String(newLabel));
        _connector._label.setValue(newLabel);
    }

    @Override
    public boolean isTermConnected(Terminal term2) {
    
        if(super.isTermConnected(term2)) {
            return true;
        }

        if(isTerminalOnVerbindung(term2)) {
            return true;
        }

        return false;
    }

    private boolean isTerminalOnVerbindung(Terminal term) {

        if(this.getConnectionType() != term.getConnectionType()) {
            return false;
        }

        if (term.getSheet() != _connector.getSheet()) {
            return false;
        }


        if ((!term.getLabel().isEmpty()) && term.getLabel().equals(_connector._label.getValue())) {
            return true;
        }

        Point terminalPoint = term.getSheetPosition();

        if(this.getConnectionType() == ConnectionType.THERMALCIRCUIT && term.getConnectionType() == ConnectionType.THERMALCIRCUIT) {
            return true;
        }

        for (Point pt : _connector.getAllPoints()) {
            if (pt.equals(terminalPoint)) {
                return true;
            }
        }

        return false;


    }

    @Override
    public ConnectionType getConnectionType() {
        return _connector.getConnectionType();
    }
    
    public final ElementConnection getConnector() {
        return _connector;
    }

}
