/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.terminal;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import geckocircuitsnew.Orientation;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 *
 * @author Zimmi
 */
public class TerminalComponentDummy extends TerminalComponent {

    public TerminalComponentDummy(ElementInterface parentelement) {
        super(parentelement, 0, 0);
    }

    @Override
    public Point getScreenPosition(int dpix) {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public String toString() {
        return super.toString() + " Dummy terminal";
    }

    @Override
    public Point getSheetPosition() {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public String getLabel() {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public void setLabel(String newLabel) {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public void setIndex(int potentialIndex) {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public int getIndex() {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public int getRelX() {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public int getRelY() {
        throw new AssertionError("this is a dummy terminal!");
    }

    @Override
    public ConnectionType getConnectionType() {
        throw new AssertionError("this is a dummy terminal!");
    }
}
