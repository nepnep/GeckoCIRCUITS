/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.DatenSpeicher;
// gecko.GeckoCIRCUITS.circuit.Verbindung;


import java.awt.Color;
import java.io.Serializable;
import java.util.StringTokenizer;


public class ScopeSettings implements Serializable {


    //---------------------------------
    public boolean usesExternalData;  // kommen die darzustellenden Daten von einer ZV-Simulation oder von einer externen (statischen) Datei? - default: ZV-Sim.
    //==========================================
    public int anzGrfVisible;  // Anzahl der aktuell sichtbaren Graphen im Scope
    public int anzDiagram;  // Anzahl der Diagramme
    public String[] nameDiagram;  // Bezeichnungen der Diagramme
    public double[] ySpacingDiagram;  // wieviel 'y-Anteil' hat das jeweilige Diagramm
    private int[] notwendigeHoehePixGRF;  // bei SIGNAL wird die Graph-Hoehe in Pix vorgegeben, eventuell wird die Scope-Groesse angepasst (bei ZV --> '-1')
    public int[] diagramTyp;  // ist das jeweilige Diagramm ein ZV-Typ oder ein Signal-Typ?
    public boolean[] jcbShowLegende;  // sollen die Kurvennamen in Form einer Legende am linken Graph-Rand angezeigt werden?
    public boolean[] showAxisX,showAxisY;  // Anzeigen oder Ausblenden der Achsen
    //
    public double[] minX, maxX, minY, maxY;  // Achsen-Begrenzungen
    public boolean[] autoScaleX, autoScaleY;  // sollen die Achsenbegrenzungen automatisch an die Worksheetdaten angepasst werden?
    public int[] xAchsenTyp, yAchsenTyp;  // Linear oder logarithmisch?
    public int[] xAchseFarbe, yAchseFarbe;
    public int[] xAchseStil, yAchseStil;
    public String[] xAchseBeschriftung, yAchseBeschriftung;
    //
    public int[] gridNormalX_zugeordneteXAchse, gridNormalX_zugeordneteYAchse;
    public int[] gridNormalY_zugeordneteXAchse, gridNormalY_zugeordneteYAchse;
    public int[] farbeGridNormalX, farbeGridNormalXminor, farbeGridNormalY, farbeGridNormalYminor;
    public int[] linStilGridNormalX, linStilGridNormalXminor, linStilGridNormalY, linStilGridNormalYminor;
    public boolean[] xShowGridMaj, xShowGridMin, yShowGridMaj, yShowGridMin;
    //
    public boolean[] xTickAutoSpacing, yTickAutoSpacing;
    public double[] xTickSpacing, yTickSpacing;
    public int[] xAnzTicksMinor, yAnzTicksMinor;
    public int[] xTickLaenge, xTickLaengeMinor, yTickLaenge, yTickLaengeMinor;
    //
    public boolean[] zeigeLabelsXmaj, zeigeLabelsXmin, zeigeLabelsYmaj, zeigeLabelsYmin;
    //-------------------------
    // Graph-Properties --> speziell fuer SIGNAL
    private int[] positionSIGNAL;  // fuer SIGNAL --> enthaelt die y-Position (Reihenfolge) der Signal-Kurve innerhalb des einzelnen Graphen
    private int[] positionSIGNAL_ALT;
    public int[] sgnHeight, sgnDistance;
    public double[] sgnSchwelle;
    //
    //-------------------------
    // Graph-Properties zum Merken, wenn man Achsen ein- und ausblendet -->
    // ... wird jeweils direkt aus 'DialogGraphProperties' gelesen und neugesetzt
    public boolean[] ORIGjcbXShowGridMaj, ORIGjcbXShowGridMin;
    public boolean[] ORIGjcbYShowGridMaj, ORIGjcbYShowGridMin;
    public int[] ORIGjcmXlinCol, ORIGjcmYlinCol;
    public int[] ORIGjcmXlinStyl, ORIGjcmYlinStyl;
    public int[] ORIGjtfXtickLengthMaj, ORIGjtfXtickLengthMin;
    public int[] ORIGjtfYtickLengthMaj, ORIGjtfYtickLengthMin;
    public boolean[] ORIGjcbXShowLabelMaj, ORIGjcbXShowLabelMin;
    public boolean[] ORIGjcbYShowLabelMaj, ORIGjcbYShowLabelMin;
    //=========================
    // Signal-Namen bzw. Worksheet-Headers:
    public int anzSignalePlusZeit;  // soviel verschiedene Kolonnen hat das Worksheet
    public String[] signalNamen;
    // Zuordnungen Kurven - Diagramme:
    public int[][] matrixZuordnungKurveDiagram;
    //
    //public int[] zugehoerigkeitX, zugehoerigkeitY;  // Zuordnung der Kurve zur x- und zur y-Achse
    public int[][] indexWsXY;  // Zuordnung Worksheetdaten - Kurven
    //=========================
    //
    // Kurven-Properties
    public int kurvenanzahl;  // soviel verschiedene Kurven werden aktuell im SCOPE dargestellt --> entspricht einer 'Kurven-ID'
    // zur Zuordnung der Kurvenindizes zur Zuordnungsmatrix:
    public int[] indexDerKurveInDerMatrix;  // Abspeichern in folgendem Format: --> 1000*i1 +i2 wobei (i1..Graphenanzahl / i2..Kurvenanzahl)
    public int[] indexDerKurveInDerMatrixALT;  // Speicherung notwendig um Darstellung der SIGNAL-Ordnung korrekt durchfuehren zu koennen
    //
    // jedem Eintrag in der Verknuepfungsmatrix entspricht eine potentielle Kurve -->
    private int[][] crvAchsenTyp;  // wird ueber SET-Methode aktualisiert, damit die Matrix 'matrixZuordnungKurveDiagram' nicht vergessen wird!
    public int[][] crvLineStyle, crvLineColor;
    public boolean[][] crvSymbShow;
    public int[][] crvSymbFrequ;
    public int[][] crvSymbShape, crvSymbColor;
    public int[][] crvClipXmin, crvClipXmax, crvClipYmin, crvClipYmax;
    public double[][] crvClipValXmin, crvClipValXmax, crvClipValYmin, crvClipValYmax;
    public boolean[][] crvFillDigitalCurves;
    public int[][] crvFillingDigitalColor;
    private double[][] crvTransparency;

    public int[] powerAnalysisCurrentIndex = {-1, -1, -1};
    public int[] powerAnalysisVoltageIndex = {-1, -1, -1};

    //---------------------------------



    public ScopeSettings () {
        this.setDefault_Graphs_No_Signal_INIT(GraferImplementation.ANZ_DIAGRAM_MAX);
    }



    public void saveSettings (GraferImplementation impl) {
        usesExternalData=impl.usesExternalData;
        //-------------------------
        anzGrfVisible=impl.getAnzahlSichtbarerDiagramme();
        anzDiagram=impl.getAnzahlDiagramme();
        nameDiagram=impl.nameDiagram;
        ySpacingDiagram=impl.ySpacingDiagram;
        notwendigeHoehePixGRF=impl.notwendigeHoehePixGRF;
        diagramTyp=impl.diagramTyp;
        jcbShowLegende=impl.jcbShowLegende;
        showAxisX=impl.showAxisX;   showAxisY=impl.showAxisY;
        //
        minX=impl.minX;   maxX=impl.maxX;   minY=impl.minY;   maxY=impl.maxY;
        autoScaleX=impl.autoScaleX;   autoScaleY=impl.autoScaleY;
        xAchsenTyp=impl.xAchsenTyp;   yAchsenTyp=impl.yAchsenTyp;
        xAchseFarbe=impl.xAchseFarbe;   yAchseFarbe=impl.yAchseFarbe;
        xAchseStil=impl.xAchseStil;   yAchseStil=impl.yAchseStil;
        xAchseBeschriftung=impl.xAchseBeschriftung;   yAchseBeschriftung=impl.yAchseBeschriftung;
        //
        gridNormalX_zugeordneteXAchse=impl.gridNormalX_zugeordneteXAchse;   gridNormalX_zugeordneteYAchse=impl.gridNormalX_zugeordneteYAchse;
        gridNormalY_zugeordneteXAchse=impl.gridNormalY_zugeordneteXAchse;   gridNormalY_zugeordneteYAchse=impl.gridNormalY_zugeordneteYAchse;
        farbeGridNormalX=impl.farbeGridNormalX;     farbeGridNormalXminor=impl.farbeGridNormalXminor;
        farbeGridNormalY=impl.farbeGridNormalY;     farbeGridNormalYminor=impl.farbeGridNormalYminor;
        linStilGridNormalX=impl.linStilGridNormalX;     linStilGridNormalXminor=impl.linStilGridNormalXminor;
        linStilGridNormalY=impl.linStilGridNormalY;     linStilGridNormalYminor=impl.linStilGridNormalYminor;
        xShowGridMaj=impl.xShowGridMaj;     xShowGridMin=impl.xShowGridMin;
        yShowGridMaj=impl.yShowGridMaj;     yShowGridMin=impl.yShowGridMin;
        //
        xTickAutoSpacing=impl.xTickAutoSpacing;   yTickAutoSpacing=impl.yTickAutoSpacing;
        xTickSpacing=impl.xTickSpacing;           yTickSpacing=impl.yTickSpacing;
        xAnzTicksMinor=impl.xAnzTicksMinor;       yAnzTicksMinor=impl.yAnzTicksMinor;
        xTickLaenge=impl.xTickLaenge;     xTickLaengeMinor=impl.xTickLaengeMinor;
        yTickLaenge=impl.yTickLaenge;     yTickLaengeMinor=impl.yTickLaengeMinor;
        //
        zeigeLabelsXmaj=impl.zeigeLabelsXmaj;     zeigeLabelsXmin=impl.zeigeLabelsXmin;
        zeigeLabelsYmaj=impl.zeigeLabelsYmaj;     zeigeLabelsYmin=impl.zeigeLabelsYmin;
        //-------------------------
        positionSIGNAL=impl.positionSIGNAL;
        positionSIGNAL_ALT=impl.positionSIGNAL_ALT;
        sgnHeight=impl.sgnHeight;   sgnDistance=impl.sgnDistance;
        sgnSchwelle=impl.sgnSchwelle;
        //-------------------------
        ORIGjcbXShowGridMaj=impl.ORIGjcbXShowGridMaj;     ORIGjcbXShowGridMin=impl.ORIGjcbXShowGridMin;
        ORIGjcbYShowGridMaj=impl.ORIGjcbYShowGridMaj;     ORIGjcbYShowGridMin=impl.ORIGjcbYShowGridMin;
        ORIGjcmXlinCol=impl.ORIGjcmXlinCol;     ORIGjcmYlinCol=impl.ORIGjcmYlinCol;
        ORIGjcmXlinStyl=impl.ORIGjcmXlinStyl;   ORIGjcmYlinStyl=impl.ORIGjcmYlinStyl;
        ORIGjtfXtickLengthMaj=impl.ORIGjtfXtickLengthMaj;     ORIGjtfXtickLengthMin=impl.ORIGjtfXtickLengthMin;
        ORIGjtfYtickLengthMaj=impl.ORIGjtfYtickLengthMaj;     ORIGjtfYtickLengthMin=impl.ORIGjtfYtickLengthMin;
        ORIGjcbXShowLabelMaj=impl.ORIGjcbXShowLabelMaj;   ORIGjcbXShowLabelMin=impl.ORIGjcbXShowLabelMin;
        ORIGjcbYShowLabelMaj=impl.ORIGjcbYShowLabelMaj;   ORIGjcbYShowLabelMin=impl.ORIGjcbYShowLabelMin;
        //=========================
        anzSignalePlusZeit=impl.anzSignalePlusZeit;
        signalNamen=impl.signalNamen;        
        matrixZuordnungKurveDiagram=impl.matrixZuordnungKurveDiagram;
        //
        indexWsXY=impl.indexWsXY;
        //=========================
        kurvenanzahl=impl.kurvenanzahl;
        indexDerKurveInDerMatrix=impl.indexDerKurveInDerMatrix;
        indexDerKurveInDerMatrixALT=impl.indexDerKurveInDerMatrixALT;
        //
        crvAchsenTyp=impl.crvAchsenTyp;
        crvLineStyle=impl.crvLineStyle;   crvLineColor=impl.crvLineColor;
        crvTransparency = impl.crvLineTransparency;
        crvSymbShow=impl.crvSymbShow;
        crvSymbFrequ=impl.crvSymbFrequ;
        crvSymbShape=impl.crvSymbShape;   crvSymbColor=impl.crvSymbColor;
        crvClipXmin=impl.crvClipXmin;   crvClipXmax=impl.crvClipXmax;   crvClipYmin=impl.crvClipYmin;   crvClipYmax=impl.crvClipYmax;
        crvClipValXmin=impl.crvClipValXmin;   crvClipValXmax=impl.crvClipValXmax;   crvClipValYmin=impl.crvClipValYmin;   crvClipValYmax=impl.crvClipValYmax;
        crvFillDigitalCurves=impl.crvFillDigitalCurves;
        crvFillingDigitalColor=impl.crvFillingDigitalColor;
        //---------------------------------
    }




    public void loadSettings (GraferImplementation impl) {
        impl.usesExternalData=usesExternalData;
        //-------------------------
        impl.setAnzahlSichtbarerDiagramme(anzGrfVisible);
        impl.setAnzahlDiagramme(anzDiagram);
        //
        impl.nameDiagram=nameDiagram;
        impl.ySpacingDiagram=ySpacingDiagram;
        impl.notwendigeHoehePixGRF=notwendigeHoehePixGRF;
        impl.diagramTyp=diagramTyp;
        impl.jcbShowLegende=jcbShowLegende;
        impl.showAxisX=showAxisX;   impl.showAxisY=showAxisY;
        //
        impl.minX=minX;   impl.maxX=maxX;   impl.minY=minY;   impl.maxY=maxY;
        impl.autoScaleX=autoScaleX;   impl.autoScaleY=autoScaleY;
        impl.xAchsenTyp=xAchsenTyp;   impl.yAchsenTyp=yAchsenTyp;
        impl.xAchseFarbe=xAchseFarbe;   impl.yAchseFarbe=yAchseFarbe;
        impl.xAchseStil=xAchseStil;   impl.yAchseStil=yAchseStil;
        impl.xAchseBeschriftung=xAchseBeschriftung;   impl.yAchseBeschriftung=yAchseBeschriftung;
        //
        impl.gridNormalX_zugeordneteXAchse=gridNormalX_zugeordneteXAchse;   impl.gridNormalX_zugeordneteYAchse=gridNormalX_zugeordneteYAchse;
        impl.gridNormalY_zugeordneteXAchse=gridNormalY_zugeordneteXAchse;   impl.gridNormalY_zugeordneteYAchse=gridNormalY_zugeordneteYAchse;
        impl.farbeGridNormalX=farbeGridNormalX;     impl.farbeGridNormalXminor=farbeGridNormalXminor;
        impl.farbeGridNormalY=farbeGridNormalY;     impl.farbeGridNormalYminor=farbeGridNormalYminor;
        impl.linStilGridNormalX=linStilGridNormalX;     impl.linStilGridNormalXminor=linStilGridNormalXminor;
        impl.linStilGridNormalY=linStilGridNormalY;     impl.linStilGridNormalYminor=linStilGridNormalYminor;
        impl.xShowGridMaj=xShowGridMaj;     impl.xShowGridMin=xShowGridMin;
        impl.yShowGridMaj=yShowGridMaj;     impl.yShowGridMin=yShowGridMin;
        //
        impl.xTickAutoSpacing=xTickAutoSpacing;   impl.yTickAutoSpacing=yTickAutoSpacing;
        impl.xTickSpacing=xTickSpacing;           impl.yTickSpacing=yTickSpacing;
        impl.xAnzTicksMinor=xAnzTicksMinor;       impl.yAnzTicksMinor=yAnzTicksMinor;
        impl.xTickLaenge=xTickLaenge;     impl.xTickLaengeMinor=xTickLaengeMinor;
        impl.yTickLaenge=yTickLaenge;     impl.yTickLaengeMinor=yTickLaengeMinor;
        //
        impl.zeigeLabelsXmaj=zeigeLabelsXmaj;     impl.zeigeLabelsXmin=zeigeLabelsXmin;
        impl.zeigeLabelsYmaj=zeigeLabelsYmaj;     impl.zeigeLabelsYmin=zeigeLabelsYmin;
        //-------------------------
        impl.positionSIGNAL=positionSIGNAL;
        impl.positionSIGNAL_ALT=positionSIGNAL_ALT;
        impl.sgnHeight=sgnHeight;   impl.sgnDistance=sgnDistance;
        impl.sgnSchwelle=sgnSchwelle;
        //-------------------------
        impl.ORIGjcbXShowGridMaj=ORIGjcbXShowGridMaj;     impl.ORIGjcbXShowGridMin=ORIGjcbXShowGridMin;
        impl.ORIGjcbYShowGridMaj=ORIGjcbYShowGridMaj;     impl.ORIGjcbYShowGridMin=ORIGjcbYShowGridMin;
        impl.ORIGjcmXlinCol=ORIGjcmXlinCol;     impl.ORIGjcmYlinCol=ORIGjcmYlinCol;
        impl.ORIGjcmXlinStyl=ORIGjcmXlinStyl;   impl.ORIGjcmYlinStyl=ORIGjcmYlinStyl;
        impl.ORIGjtfXtickLengthMaj=ORIGjtfXtickLengthMaj;     impl.ORIGjtfXtickLengthMin=ORIGjtfXtickLengthMin;
        impl.ORIGjtfYtickLengthMaj=ORIGjtfYtickLengthMaj;     impl.ORIGjtfYtickLengthMin=ORIGjtfYtickLengthMin;
        impl.ORIGjcbXShowLabelMaj=ORIGjcbXShowLabelMaj;    impl.ORIGjcbXShowLabelMin=ORIGjcbXShowLabelMin;
        impl.ORIGjcbYShowLabelMaj=ORIGjcbYShowLabelMaj;    impl.ORIGjcbYShowLabelMin=ORIGjcbYShowLabelMin;
        //=========================
        impl.anzSignalePlusZeit=anzSignalePlusZeit;
        impl.signalNamen=signalNamen;        
        impl.matrixZuordnungKurveDiagram=matrixZuordnungKurveDiagram;
        //
        impl.indexWsXY=indexWsXY;
        //=========================
        impl.kurvenanzahl=kurvenanzahl;
        impl.indexDerKurveInDerMatrix=indexDerKurveInDerMatrix;
        impl.indexDerKurveInDerMatrixALT=indexDerKurveInDerMatrixALT;
        //
        impl.crvAchsenTyp=crvAchsenTyp;
        impl.crvLineStyle=crvLineStyle;   impl.crvLineColor=crvLineColor;
        impl.crvLineTransparency = crvTransparency;
        impl.crvSymbShow=crvSymbShow;
        impl.crvSymbFrequ=crvSymbFrequ;
        impl.crvSymbShape=crvSymbShape;   impl.crvSymbColor=crvSymbColor;
        impl.crvClipXmin=crvClipXmin;   impl.crvClipXmax=crvClipXmax;   impl.crvClipYmin=crvClipYmin;   impl.crvClipYmax=crvClipYmax;
        impl.crvClipValXmin=crvClipValXmin;   impl.crvClipValXmax=crvClipValXmax;   impl.crvClipValYmin=crvClipValYmin;   impl.crvClipValYmax=crvClipValYmax;
        impl.crvFillDigitalCurves=crvFillDigitalCurves;
        impl.crvFillingDigitalColor=crvFillingDigitalColor;
        //---------------------------------
    }





    //***************************************************************************************************************************************+
    //***************************************************************************************************************************************+
    // DEFAULT - Settings -->
    //***************************************************************************************************************************************+
    //***************************************************************************************************************************************+



    public void setDefault_Graphs_No_Signal (int index, boolean setDefaultKnopfGedrueckt) {
        int i1= index;
        //----------------------------------------------
        nameDiagram[i1]="GRF "+i1;
        if (!setDefaultKnopfGedrueckt) ySpacingDiagram[i1]=1.0;
        diagramTyp[i1]= GraferImplementation.DIAGRAM_TYP_ZV;
        notwendigeHoehePixGRF[i1]=-1;  // default --> kein SIGNAL-Graph sondern ZV-Graph
        jcbShowLegende[i1]= true;
        showAxisX[i1]=true;   showAxisY[i1]=true;
        minX[i1]=0;    maxX[i1]=10;    autoScaleX[i1]= true;
        minY[i1]=0;    maxY[i1]=10;    autoScaleY[i1]= true;
        xAchsenTyp[i1]=GraferV3.ACHSE_LIN;    yAchsenTyp[i1]=GraferV3.ACHSE_LIN;
        xAchseFarbe[i1]=GraferV3.BLACK;       yAchseFarbe[i1]=GraferV3.BLACK;
        xAchseStil[i1]=GraferV3.SOLID_PLAIN;  yAchseStil[i1]=GraferV3.SOLID_PLAIN;
        xAchseBeschriftung[i1]="";            yAchseBeschriftung[i1]="";
        farbeGridNormalX[i1]=GraferV3.LIGTHGRAY;        farbeGridNormalXminor[i1]=GraferV3.LIGTHGRAY;
        farbeGridNormalY[i1]=GraferV3.LIGTHGRAY;        farbeGridNormalYminor[i1]=GraferV3.LIGTHGRAY;
        linStilGridNormalX[i1]=GraferV3.SOLID_PLAIN;    linStilGridNormalXminor[i1]=GraferV3.SOLID_PLAIN;
        linStilGridNormalY[i1]=GraferV3.SOLID_PLAIN;    linStilGridNormalYminor[i1]=GraferV3.SOLID_PLAIN;
        xShowGridMaj[i1]=true;                          xShowGridMin[i1]=true;
        yShowGridMaj[i1]=true;                          yShowGridMin[i1]=true;
        xTickAutoSpacing[i1]=true;       yTickAutoSpacing[i1]=true;
        xAnzTicksMinor[i1]=2;            yAnzTicksMinor[i1]=2;
        xTickLaenge[i1]=10;              yTickLaenge[i1]=10;
        xTickLaengeMinor[i1]=5;          yTickLaengeMinor[i1]=5;
        xTickSpacing[i1]=  100;  //this.getAutoTickSpacingX(i1);
        yTickSpacing[i1]=  100;  //this.getAutoTickSpacingY(i1);
        zeigeLabelsXmaj[i1]=true;     zeigeLabelsXmin[i1]=false;
        zeigeLabelsYmaj[i1]=true;     zeigeLabelsYmin[i1]=false;
        //
        sgnHeight[i1]= 10;
        sgnDistance[i1]= 4;
        sgnSchwelle[i1]= 0.5;
        //----------------------------------------------
    }





    public void setDefault_Graphs_No_Signal_INIT (int anz) {
        //----------------------------------------------
        anzGrfVisible= 1;  // default-maessig sichtbar
        anzDiagram= anzGrfVisible;
        //
        nameDiagram= new String[anz];
        jcbShowLegende= new boolean[anz];
        showAxisX= new boolean[anz];   showAxisY= new boolean[anz];
        ySpacingDiagram= new double[anz];
        notwendigeHoehePixGRF = new int[anz];
        diagramTyp= new int[anz];
        //----------------------------------------------
        minX= new double[anz];                maxX= new double[anz];                autoScaleX= new boolean[anz];
        minY= new double[anz];                maxY= new double[anz];                autoScaleY= new boolean[anz];
        xAchsenTyp= new int[anz];             yAchsenTyp= new int[anz];
        xAchseFarbe= new int[anz];            yAchseFarbe= new int[anz];
        xAchseStil= new int[anz];             yAchseStil= new int[anz];
        xAchseBeschriftung= new String[anz];  yAchseBeschriftung= new String[anz];
        //
        farbeGridNormalX= new int[anz];      farbeGridNormalXminor= new int[anz];
        farbeGridNormalY= new int[anz];      farbeGridNormalYminor= new int[anz];
        linStilGridNormalX= new int[anz];    linStilGridNormalXminor= new int[anz];
        linStilGridNormalY= new int[anz];    linStilGridNormalYminor= new int[anz];
        xShowGridMaj= new boolean[anz];      xShowGridMin= new boolean[anz];
        yShowGridMaj= new boolean[anz];      yShowGridMin= new boolean[anz];
        //
        xTickAutoSpacing= new boolean[anz];  yTickAutoSpacing= new boolean[anz];
        xTickSpacing= new double[anz];       yTickSpacing= new double[anz];
        xAnzTicksMinor= new int[anz];        yAnzTicksMinor= new int[anz];
        xTickLaenge= new int[anz];           xTickLaengeMinor= new int[anz];
        yTickLaenge= new int[anz];           yTickLaengeMinor= new int[anz];
        //
        zeigeLabelsXmaj= new boolean[anz];    zeigeLabelsXmin= new boolean[anz];
        zeigeLabelsYmaj= new boolean[anz];    zeigeLabelsYmin= new boolean[anz];
        //----------------------------------------------
        ORIGjcbXShowGridMaj= new boolean[anz];    ORIGjcbXShowGridMin= new boolean[anz];
        ORIGjcbYShowGridMaj= new boolean[anz];    ORIGjcbYShowGridMin= new boolean[anz];
        ORIGjcmXlinCol= new int[anz];    ORIGjcmYlinCol= new int[anz];
        ORIGjcmXlinStyl= new int[anz];   ORIGjcmYlinStyl= new int[anz];
        ORIGjtfXtickLengthMaj= new int[anz];    ORIGjtfXtickLengthMin= new int[anz];
        ORIGjtfYtickLengthMaj= new int[anz];    ORIGjtfYtickLengthMin= new int[anz];
        ORIGjcbXShowLabelMaj= new boolean[anz];    ORIGjcbXShowLabelMin= new boolean[anz];
        ORIGjcbYShowLabelMaj= new boolean[anz];    ORIGjcbYShowLabelMin= new boolean[anz];
        //----------------------------------------------
        // speziell fuer SIGNAL -->
        sgnHeight= new int[anz];
        sgnDistance= new int[anz];
        sgnSchwelle= new double[anz];

        crvTransparency = new double[50][50];
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                crvTransparency[i][j] = 0.9;
            }
        }
        //----------------------------------------------
        //
        for (int i1=0;  i1<anz;  i1++)
            this.setDefault_Graphs_No_Signal(i1,false);
        //
        //----------------------------------------------
    }




    // wird verwendet, wenn sich die Anzahl der SCOPE-Signale fuer die ZV-Simulation aendert -->
    //
    public void update_ZVs (int anzSignalePlusZeitNEU, String[] signalNamenNEU) {
        //------------------------
        // Initialisieren der Kurve-Properties: xxxTEMP wird temporaer initialisiert, wenn die Zahl der anzuzeigenden
        // ZV-Signale steigt, dann werden alle alten Property-Werte beibehalten (dh. aus den Original-Property-Werten
        // in xxxTEMP hineinkopiert), aber die neu dazugekommenen werden default-maessig initialisiert
        // Abschliessend werden alle xxxTEMP in auf die alten Werte kopiert
        //
        int[][] matrixZuordnungKurveDiagramTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        //
        int[][] crvAchsenTypTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvLineStyleTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvLineColorTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        double[][] crvTransparencyTEMP = new double[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        //
        boolean[][] crvSymbShowTEMP= new boolean[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvSymbFrequTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvSymbShapeTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvSymbColorTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        //
        int[][] crvClipXminTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvClipXmaxTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvClipYminTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvClipYmaxTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        double[][] crvClipValXminTEMP= new double[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        double[][] crvClipValXmaxTEMP= new double[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        double[][] crvClipValYminTEMP= new double[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        double[][] crvClipValYmaxTEMP= new double[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        //
        boolean[][] crvFillDigitalCurvesTEMP= new boolean[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        int[][] crvFillingDigitalColorTEMP= new int[GraferImplementation.ANZ_DIAGRAM_MAX][anzSignalePlusZeitNEU];
        //------------------------
        // Wenn die Signal-Anzahl steigt, dann werden die Felder vergroessert und die neuen Felder default-maessig initialisiert
        // Wenn die Signal-Anzahl sinkt, dann wird die Feldgroesse reduziert und bei erneuter Feldvergroesserung sind die ehemaligen Properties nicht reproduzierbar
        //
        if (signalNamen==null) {
            //==========================
            // erstmalige Initialisierung --> alle Kurve-Properties werden default-maessig gesetzt
            for (int i2=0;  i2<signalNamenNEU.length;  i2++)
                this.initKurveDefault (
                    i2,
                    matrixZuordnungKurveDiagramTEMP, crvAchsenTypTEMP, crvLineStyleTEMP, crvLineColorTEMP,
                    crvSymbShowTEMP, crvSymbFrequTEMP, crvSymbShapeTEMP, crvSymbColorTEMP,
                    crvClipXminTEMP, crvClipXmaxTEMP, crvClipYminTEMP, crvClipYmaxTEMP, 
                    crvFillDigitalCurvesTEMP, crvFillingDigitalColorTEMP, crvTransparencyTEMP
                );
            //---------------
        } else if (signalNamenNEU.length > signalNamen.length) {
            //==========================
            // es sind Signale dazugekommen --> die zuseatzlichen Signale werden mit default-Properties gesetzt
            for (int i2=0;  i2<signalNamenNEU.length;  i2++)
                if (i2<signalNamen.length)
                    this.uebernehmeAltePropertiesUnveraendert (
                        i2,
                        matrixZuordnungKurveDiagramTEMP, crvAchsenTypTEMP, crvLineStyleTEMP, crvLineColorTEMP,
                        crvSymbShowTEMP, crvSymbFrequTEMP, crvSymbShapeTEMP, crvSymbColorTEMP,
                        crvClipXminTEMP, crvClipXmaxTEMP, crvClipYminTEMP, crvClipYmaxTEMP,
                        crvClipValXminTEMP, crvClipValXmaxTEMP, crvClipValYminTEMP, crvClipValYmaxTEMP, 
                        crvFillDigitalCurvesTEMP, crvFillingDigitalColorTEMP, crvTransparencyTEMP
                    );
                else
                    this.initKurveDefault (
                        i2,
                        matrixZuordnungKurveDiagramTEMP, crvAchsenTypTEMP, crvLineStyleTEMP, crvLineColorTEMP,
                        crvSymbShowTEMP, crvSymbFrequTEMP, crvSymbShapeTEMP, crvSymbColorTEMP,
                        crvClipXminTEMP, crvClipXmaxTEMP, crvClipYminTEMP, crvClipYmaxTEMP, 
                        crvFillDigitalCurvesTEMP, crvFillingDigitalColorTEMP, crvTransparencyTEMP
                    );
            //---------------
        } else if (signalNamenNEU.length <= signalNamen.length) {
            //==========================
            // (a) die Anzahl der Signale hat sich verringert --> die alten Signal-Property-Arrays werden einfach entsprechend abgeschnitten
            // (b) die Signal-Anzahl bleibt unveraendert --> die alten Properties werden unveraendert uebernommen
            for (int i2=0;  i2<signalNamenNEU.length;  i2++)
                this.uebernehmeAltePropertiesUnveraendert (
                    i2,
                    matrixZuordnungKurveDiagramTEMP, crvAchsenTypTEMP, crvLineStyleTEMP, crvLineColorTEMP,
                    crvSymbShowTEMP, crvSymbFrequTEMP, crvSymbShapeTEMP, crvSymbColorTEMP,
                    crvClipXminTEMP, crvClipXmaxTEMP, crvClipYminTEMP, crvClipYmaxTEMP,
                    crvClipValXminTEMP, crvClipValXmaxTEMP, crvClipValYminTEMP, crvClipValYmaxTEMP, 
                    crvFillDigitalCurvesTEMP, crvFillingDigitalColorTEMP, crvTransparencyTEMP
                );
            //---------------
        }
        //------------------------
        // abschliessende Aktualisierung der Kurven-Properties:        
        matrixZuordnungKurveDiagram= matrixZuordnungKurveDiagramTEMP;
        //
        crvAchsenTyp= crvAchsenTypTEMP;   crvLineStyle= crvLineStyleTEMP;   crvLineColor= crvLineColorTEMP;
        crvSymbShow= crvSymbShowTEMP;     crvSymbFrequ= crvSymbFrequTEMP;   crvSymbShape= crvSymbShapeTEMP;   crvSymbColor= crvSymbColorTEMP;
        crvClipXmin= crvClipXminTEMP;     crvClipXmax= crvClipXmaxTEMP;     crvClipYmin= crvClipYminTEMP;     crvClipYmax= crvClipYmaxTEMP;
        crvClipValXmin= crvClipValXminTEMP;    crvClipValXmax= crvClipValXmaxTEMP;    crvClipValYmin= crvClipValYminTEMP;    crvClipValYmax= crvClipValYmaxTEMP;
        crvFillDigitalCurves=crvFillDigitalCurvesTEMP;   crvFillingDigitalColor=crvFillingDigitalColorTEMP;
        //
        this.anzSignalePlusZeit= anzSignalePlusZeitNEU;
        this.signalNamen= signalNamenNEU;
        //
        //==============================================
        /*
        ... muss in 'initClipping()' initialisiert werden, ansonsten NullPointerException -->
        //
        for (int i1=0;  i1<matrixZuordnungKurveDiagram.length;  i1++)
            for (int i2=0;  i2<matrixZuordnungKurveDiagram[0].length;  i2++) {
                crvClipValXmin[i1][i2]= this.getXClip_ACHSE(i1,i2)[0];
                crvClipValXmax[i1][i2]= this.getXClip_ACHSE(i1,i2)[1];
                crvClipValYmin[i1][i2]= this.getYClip_ACHSE(i1,i2)[0];
                crvClipValYmax[i1][i2]= this.getYClip_ACHSE(i1,i2)[1];
            }
        */
        //==============================================
    }



    // Default-Settings fuer ein einzelne neue Signal-Kurve in allen Diagrammen -->
    //
    private void initKurveDefault (
        int indexSignal,
        int[][] matrixZuordnungKurveDiagramTEMP, int[][] crvAchsenTypTEMP, int[][] crvLineStyleTEMP, int[][] crvLineColorTEMP,
        boolean[][] crvSymbShowTEMP, int[][] crvSymbFrequTEMP, int[][] crvSymbShapeTEMP, int[][] crvSymbColorTEMP,
        int[][] crvClipXminTEMP, int[][] crvClipXmaxTEMP, int[][] crvClipYminTEMP, int[][] crvClipYmaxTEMP, 
        boolean[][] crvFillDigitalCurvesTEMP, int[][] crvFillingDigitalColorTEMP, double[][] crvTransparencyTEMP
        ) {
        //-----------------------------
        for (int i1=0;  i1<GraferImplementation.ANZ_DIAGRAM_MAX;  i1++) {
            int i2= indexSignal;
            if (i2==0) {
                matrixZuordnungKurveDiagramTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_X;
            } else if (i1==0) {  // erste Reihe --> per default alle Kurven gesetzt
                matrixZuordnungKurveDiagramTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_Y;
            } else if (i2==1) {  // in allen weiteren Diagrammen nur die jeweils erste Kurve gesetzt
                matrixZuordnungKurveDiagramTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_Y;
            } else {
                matrixZuordnungKurveDiagramTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_NIX;
            }
            switch (matrixZuordnungKurveDiagramTEMP[i1][i2]) {
                case GraferImplementation.ZUORDNUNG_X:    crvAchsenTypTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_X;    break;
                case GraferImplementation.ZUORDNUNG_Y:    crvAchsenTypTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_Y;    break;
                case GraferImplementation.ZUORDNUNG_NIX:  crvAchsenTypTEMP[i1][i2]= GraferImplementation.ZUORDNUNG_NIX;  break;
                case GraferImplementation.ZUORDNUNG_SIGNAL:  break;
                default: System.out.println("Fehler: no3g5rn903g"); 
            }
            crvLineStyleTEMP[i1][i2]= GraferV3.SOLID_PLAIN;
            crvLineColorTEMP[i1][i2]= getInitCureLineColor(i1, i2);
            crvSymbShowTEMP[i1][i2]=  false;
            crvSymbFrequTEMP[i1][i2]= 1;
            crvSymbShapeTEMP[i1][i2]= GraferV3.SYBM_CIRCLE;
            crvSymbColorTEMP[i1][i2]= GraferV3.DARKGRAY;
            crvClipXminTEMP[i1][i2]=  GraferV3.CLIP_ACHSE;
            crvClipXmaxTEMP[i1][i2]=  GraferV3.CLIP_ACHSE;
            crvClipYminTEMP[i1][i2]=  GraferV3.CLIP_NO;
            crvClipYmaxTEMP[i1][i2]=  GraferV3.CLIP_NO;
            crvFillDigitalCurvesTEMP[i1][i2]=   true;
            crvFillingDigitalColorTEMP[i1][i2]= GraferV3.LIGTHGRAY;
            crvTransparencyTEMP[i1][i2] = 1.0;
        }
        //-----------------------------
    }

    private int getInitCureLineColor(int i1, int i2) {
        int color = (i1 + i2)%12;

        switch(color) {
            case  0:  return GraferV3.BLACK;
            case  1:  return GraferV3.RED;
            case  2:  return GraferV3.GREEN;
            case  3:  return GraferV3.BLUE;
            case  4:  return GraferV3.DARKGRAY;
            case  5:  return GraferV3.GRAY;
            case  6:  return GraferV3.RED;
            case  7:  return GraferV3.BLACK;
            case  8:  return GraferV3.MAGENTA;
            case  9:  return GraferV3.CYAN;
            case 10:  return GraferV3.ORANGE;
            case 11:  return GraferV3.BLUE;
            case 12:  return GraferV3.DARKGREEN;
        }
        return GraferV3.BLACK;

    }

    // die alten Properties werden unveraendert uebernommen, und zwar fuer ein einzelne neue Signal-Kurve in allen Diagrammen -->
    //
    private void uebernehmeAltePropertiesUnveraendert (            
        int indexSignal,
        int[][] matrixZuordnungKurveDiagramTEMP, int[][] crvAchsenTypTEMP, int[][] crvLineStyleTEMP, int[][] crvLineColorTEMP,
        boolean[][] crvSymbShowTEMP, int[][] crvSymbFrequTEMP, int[][] crvSymbShapeTEMP, int[][] crvSymbColorTEMP,
        int[][] crvClipXminTEMP, int[][] crvClipXmaxTEMP, int[][] crvClipYminTEMP, int[][] crvClipYmaxTEMP,
        double[][] crvClipValXminTEMP, double[][] crvClipValXmaxTEMP, double[][] crvClipValYminTEMP, double[][] crvClipValYmaxTEMP, 
        boolean[][] crvFillDigitalCurvesTEMP, int[][] crvFillingDigitalColorTEMP , double[][] crvTransparencyTEMP
        ) {
        //-----------------------------
        try {
        for (int i1=0;  i1<GraferImplementation.ANZ_DIAGRAM_MAX;  i1++) {
            int i2= indexSignal;
            //
            matrixZuordnungKurveDiagramTEMP[i1][i2]= matrixZuordnungKurveDiagram[i1][i2];
            crvAchsenTypTEMP[i1][i2]= crvAchsenTyp[i1][i2];   crvLineStyleTEMP[i1][i2]= crvLineStyle[i1][i2];   crvLineColorTEMP[i1][i2]= crvLineColor[i1][i2];
            crvSymbShowTEMP[i1][i2]= crvSymbShow[i1][i2];     crvSymbFrequTEMP[i1][i2]= crvSymbFrequ[i1][i2];   crvSymbShapeTEMP[i1][i2]= crvSymbShape[i1][i2];   crvSymbColorTEMP[i1][i2]= crvSymbColor[i1][i2];
            crvClipXminTEMP[i1][i2]= crvClipXmin[i1][i2];     crvClipXmaxTEMP[i1][i2]= crvClipXmax[i1][i2];     crvClipYminTEMP[i1][i2]= crvClipYmin[i1][i2];     crvClipYmaxTEMP[i1][i2]= crvClipYmax[i1][i2];
            crvClipValXminTEMP[i1][i2]= crvClipValXmin[i1][i2];    crvClipValXmaxTEMP[i1][i2]= crvClipValXmax[i1][i2];
            crvClipValYminTEMP[i1][i2]= crvClipValYmin[i1][i2];    crvClipValYmaxTEMP[i1][i2]= crvClipValYmax[i1][i2];
            crvFillDigitalCurvesTEMP[i1][i2]=   crvFillDigitalCurves[i1][i2];
            crvFillingDigitalColorTEMP[i1][i2]= crvFillingDigitalColor[i1][i2];
        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //-----------------------------
    }




    // zum Speichern im ASCII-Format (anstatt als Object-Stream) -->
    //
    public void exportASCII (StringBuffer ascii) {
        /*ascii.append("\n<scopeSettings>");
        //------------------
        DatenSpeicher.appendAsString(ascii.append("\nusesExternalData"), usesExternalData);
        DatenSpeicher.appendAsString(ascii.append("\nanzGrfVisible"), anzGrfVisible);
        DatenSpeicher.appendAsString(ascii.append("\nanzDiagram"), anzDiagram);
        DatenSpeicher.appendAsString(ascii.append("\njcbShowLegende"), jcbShowLegende);
        DatenSpeicher.appendAsString(ascii.append("\nshowAxisX"), showAxisX);
        DatenSpeicher.appendAsString(ascii.append("\nshowAxisY"), showAxisY);
        DatenSpeicher.appendAsString(ascii.append("\npowerAnalysisCurrents"), powerAnalysisCurrentIndex);
        DatenSpeicher.appendAsString(ascii.append("\npowerAnalysisVoltages"), powerAnalysisVoltageIndex);
        DatenSpeicher.appendAsString(ascii.append("\nautoScaleX"), autoScaleX);
        DatenSpeicher.appendAsString(ascii.append("\nautoScaleY"), autoScaleY);
        DatenSpeicher.appendAsString(ascii.append("\nxShowGridMaj"), xShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nxShowGridMin"), xShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nyShowGridMaj"), yShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nyShowGridMin"), yShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nxTickAutoSpacing"), xTickAutoSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nyTickAutoSpacing"), yTickAutoSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsXmaj"), zeigeLabelsXmaj);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsXmin"), zeigeLabelsXmin);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsYmaj"), zeigeLabelsYmaj);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsYmin"), zeigeLabelsYmin);
        DatenSpeicher.appendAsString(ascii.append("\ndiagramTyp"), diagramTyp);
        DatenSpeicher.appendAsString(ascii.append("\nnotwendigeHoehePixGRF"), notwendigeHoehePixGRF);
        DatenSpeicher.appendAsString(ascii.append("\nxAchsenTyp"), xAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\nyAchsenTyp"), yAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseFarbe"), xAchseFarbe);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseFarbe"), yAchseFarbe);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseStil"), xAchseStil);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseStil"), yAchseStil);
        DatenSpeicher.appendAsString(ascii.append("\nnotwendigeHoehePixGRF"), notwendigeHoehePixGRF);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalX_zugeordneteXAchse"), gridNormalX_zugeordneteXAchse);  // ??
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalX_zugeordneteYAchse"), gridNormalX_zugeordneteYAchse);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalY_zugeordneteXAchse"), gridNormalY_zugeordneteXAchse);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalY_zugeordneteYAchse"), gridNormalY_zugeordneteYAchse);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalX"), farbeGridNormalX);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalXminor"), farbeGridNormalXminor);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalY"), farbeGridNormalY);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalYminor"), farbeGridNormalYminor);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalX"), linStilGridNormalX);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalXminor"), linStilGridNormalXminor);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalY"), linStilGridNormalY);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalYminor"), linStilGridNormalYminor);
        DatenSpeicher.appendAsString(ascii.append("\nxAnzTicksMinor"), xAnzTicksMinor);
        DatenSpeicher.appendAsString(ascii.append("\nyAnzTicksMinor"), yAnzTicksMinor);
        DatenSpeicher.appendAsString(ascii.append("\nxTickLaenge"), xTickLaenge);
        DatenSpeicher.appendAsString(ascii.append("\nxTickLaengeMinor"), xTickLaengeMinor);
        DatenSpeicher.appendAsString(ascii.append("\nyTickLaenge"), yTickLaenge);
        DatenSpeicher.appendAsString(ascii.append("\nyTickLaengeMinor"), yTickLaengeMinor);
        DatenSpeicher.appendAsString(ascii.append("\nySpacingDiagram"), ySpacingDiagram);
        DatenSpeicher.appendAsString(ascii.append("\nminX"), minX);
        DatenSpeicher.appendAsString(ascii.append("\nmaxX"), maxX);
        DatenSpeicher.appendAsString(ascii.append("\nminY"), minY);
        DatenSpeicher.appendAsString(ascii.append("\nmaxY"), maxY);
        DatenSpeicher.appendAsString(ascii.append("\nxTickSpacing"), xTickSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nyTickSpacing"), yTickSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nnameDiagram"), nameDiagram);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseBeschriftung"), xAchseBeschriftung);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseBeschriftung"), yAchseBeschriftung);
        //-------------------------
        DatenSpeicher.appendAsString(ascii.append("\npositionSIGNAL"), positionSIGNAL);
        DatenSpeicher.appendAsString(ascii.append("\npositionSIGNAL_ALT"), positionSIGNAL_ALT);
        DatenSpeicher.appendAsString(ascii.append("\nsgnHeight"), sgnHeight);
        DatenSpeicher.appendAsString(ascii.append("\nsgnDistance"), sgnDistance);
        DatenSpeicher.appendAsString(ascii.append("\nsgnSchwelle"), sgnSchwelle);
        //-------------------------
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowGridMaj"), ORIGjcbXShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowGridMin"), ORIGjcbXShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowGridMaj"), ORIGjcbYShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowGridMin"), ORIGjcbYShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmXlinCol"), ORIGjcmXlinCol);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmYlinCol"), ORIGjcmYlinCol);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmXlinStyl"), ORIGjcmXlinStyl);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmYlinStyl"), ORIGjcmYlinStyl);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfXtickLengthMaj"), ORIGjtfXtickLengthMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfXtickLengthMin"), ORIGjtfXtickLengthMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfYtickLengthMaj"), ORIGjtfYtickLengthMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfYtickLengthMin"), ORIGjtfYtickLengthMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowLabelMaj"), ORIGjcbXShowLabelMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowLabelMin"), ORIGjcbXShowLabelMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowLabelMaj"), ORIGjcbYShowLabelMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowLabelMin"), ORIGjcbYShowLabelMin);
        //=========================
        DatenSpeicher.appendAsString(ascii.append("\nanzSignalePlusZeit"), anzSignalePlusZeit);
        DatenSpeicher.appendAsString(ascii.append("\nsignalNamen"), signalNamen);
        if(matrixZuordnungKurveDiagram != null)
        DatenSpeicher.appendAsString(ascii.append("\nmatrixZuordnungKurveDiagram"), matrixZuordnungKurveDiagram);
        if (indexWsXY==null) indexWsXY=new int[][]{{}};  // um eventuelle NullPointerException beim Speichern zu vermeiden
        DatenSpeicher.appendAsString(ascii.append("\nindexWsXY"), indexWsXY);
        //=========================
        DatenSpeicher.appendAsString(ascii.append("\nkurvenanzahl"), kurvenanzahl);
        DatenSpeicher.appendAsString(ascii.append("\nindexDerKurveInDerMatrix"), indexDerKurveInDerMatrix);
        DatenSpeicher.appendAsString(ascii.append("\nindexDerKurveInDerMatrixALT"), indexDerKurveInDerMatrixALT);
        if(crvSymbShow != null) {
            DatenSpeicher.appendAsString(ascii.append("\ncrvSymbShow"), crvSymbShow);        
        DatenSpeicher.appendAsString(ascii.append("\ncrvAchsenTyp"), crvAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\ncrvLineStyle"), crvLineStyle);
        DatenSpeicher.appendAsString(ascii.append("\ncrvLineColor"), crvLineColor);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbFrequ"), crvSymbFrequ);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbShape"), crvSymbShape);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbColor"), crvSymbColor);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipXmin"), crvClipXmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipXmax"), crvClipXmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipYmin"), crvClipYmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipYmax"), crvClipYmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValXmin"), crvClipValXmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValXmax"), crvClipValXmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValYmin"), crvClipValYmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValYmax"), crvClipValYmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvTransparency"), crvTransparency);
        DatenSpeicher.appendAsString(ascii.append("\ncrvFillDigitalCurves"), crvFillDigitalCurves);
        DatenSpeicher.appendAsString(ascii.append("\ncrvFillingDigitalColor"), crvFillingDigitalColor);

        }
        //------------------
        ascii.append("\n<\\scopeSettings>");
        //------------------*/
    }


    // zum Speichern im ASCII-Format (anstatt als Object-Stream) -->
    //
    public void exportASCII2 (StringBuffer ascii) {
        /*ascii.append("\n<scopeSettings2>");
        //------------------
        DatenSpeicher.appendAsString(ascii.append("\nusesExternalData"), usesExternalData);
        DatenSpeicher.appendAsString(ascii.append("\nanzGrfVisible"), anzGrfVisible);
        DatenSpeicher.appendAsString(ascii.append("\nanzDiagram"), anzDiagram);
        DatenSpeicher.appendAsString(ascii.append("\njcbShowLegende"), jcbShowLegende);
        DatenSpeicher.appendAsString(ascii.append("\nshowAxisX"), showAxisX);
        DatenSpeicher.appendAsString(ascii.append("\nshowAxisY"), showAxisY);
        DatenSpeicher.appendAsString(ascii.append("\nautoScaleX"), autoScaleX);
        DatenSpeicher.appendAsString(ascii.append("\nautoScaleY"), autoScaleY);
        DatenSpeicher.appendAsString(ascii.append("\npowerAnalysisCurrents"), powerAnalysisCurrentIndex);
        DatenSpeicher.appendAsString(ascii.append("\npowerAnalysisCurrents"), powerAnalysisVoltageIndex);
        DatenSpeicher.appendAsString(ascii.append("\nxShowGridMaj"), xShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nxShowGridMin"), xShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nyShowGridMaj"), yShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nyShowGridMin"), yShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nxTickAutoSpacing"), xTickAutoSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nyTickAutoSpacing"), yTickAutoSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsXmaj"), zeigeLabelsXmaj);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsXmin"), zeigeLabelsXmin);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsYmaj"), zeigeLabelsYmaj);
        DatenSpeicher.appendAsString(ascii.append("\nzeigeLabelsYmin"), zeigeLabelsYmin);
        DatenSpeicher.appendAsString(ascii.append("\ndiagramTyp"), diagramTyp);
        DatenSpeicher.appendAsString(ascii.append("\nnotwendigeHoehePixGRF"), notwendigeHoehePixGRF);
        DatenSpeicher.appendAsString(ascii.append("\nxAchsenTyp"), xAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\nyAchsenTyp"), yAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseFarbe"), xAchseFarbe);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseFarbe"), yAchseFarbe);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseStil"), xAchseStil);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseStil"), yAchseStil);
        DatenSpeicher.appendAsString(ascii.append("\nnotwendigeHoehePixGRF"), notwendigeHoehePixGRF);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalX_zugeordneteXAchse"), gridNormalX_zugeordneteXAchse);  // ??
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalX_zugeordneteYAchse"), gridNormalX_zugeordneteYAchse);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalY_zugeordneteXAchse"), gridNormalY_zugeordneteXAchse);
        DatenSpeicher.appendAsString(ascii.append("\ngridNormalY_zugeordneteYAchse"), gridNormalY_zugeordneteYAchse);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalX"), farbeGridNormalX);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalXminor"), farbeGridNormalXminor);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalY"), farbeGridNormalY);
        DatenSpeicher.appendAsString(ascii.append("\nfarbeGridNormalYminor"), farbeGridNormalYminor);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalX"), linStilGridNormalX);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalXminor"), linStilGridNormalXminor);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalY"), linStilGridNormalY);
        DatenSpeicher.appendAsString(ascii.append("\nlinStilGridNormalYminor"), linStilGridNormalYminor);
        DatenSpeicher.appendAsString(ascii.append("\nxAnzTicksMinor"), xAnzTicksMinor);
        DatenSpeicher.appendAsString(ascii.append("\nyAnzTicksMinor"), yAnzTicksMinor);
        DatenSpeicher.appendAsString(ascii.append("\nxTickLaenge"), xTickLaenge);
        DatenSpeicher.appendAsString(ascii.append("\nxTickLaengeMinor"), xTickLaengeMinor);
        DatenSpeicher.appendAsString(ascii.append("\nyTickLaenge"), yTickLaenge);
        DatenSpeicher.appendAsString(ascii.append("\nyTickLaengeMinor"), yTickLaengeMinor);
        DatenSpeicher.appendAsString(ascii.append("\nySpacingDiagram"), ySpacingDiagram);
        DatenSpeicher.appendAsString(ascii.append("\nminX"), minX);
        DatenSpeicher.appendAsString(ascii.append("\nmaxX"), maxX);
        DatenSpeicher.appendAsString(ascii.append("\nminY"), minY);
        DatenSpeicher.appendAsString(ascii.append("\nmaxY"), maxY);
        DatenSpeicher.appendAsString(ascii.append("\nxTickSpacing"), xTickSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nyTickSpacing"), yTickSpacing);
        DatenSpeicher.appendAsString(ascii.append("\nnameDiagram"), nameDiagram);
        DatenSpeicher.appendAsString(ascii.append("\nxAchseBeschriftung"), xAchseBeschriftung);
        DatenSpeicher.appendAsString(ascii.append("\nyAchseBeschriftung"), yAchseBeschriftung);
        //-------------------------
        DatenSpeicher.appendAsString(ascii.append("\npositionSIGNAL"), positionSIGNAL);
        DatenSpeicher.appendAsString(ascii.append("\npositionSIGNAL_ALT"), positionSIGNAL_ALT);
        DatenSpeicher.appendAsString(ascii.append("\nsgnHeight"), sgnHeight);
        DatenSpeicher.appendAsString(ascii.append("\nsgnDistance"), sgnDistance);
        DatenSpeicher.appendAsString(ascii.append("\nsgnSchwelle"), sgnSchwelle);
        //-------------------------
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowGridMaj"), ORIGjcbXShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowGridMin"), ORIGjcbXShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowGridMaj"), ORIGjcbYShowGridMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowGridMin"), ORIGjcbYShowGridMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmXlinCol"), ORIGjcmXlinCol);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmYlinCol"), ORIGjcmYlinCol);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmXlinStyl"), ORIGjcmXlinStyl);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcmYlinStyl"), ORIGjcmYlinStyl);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfXtickLengthMaj"), ORIGjtfXtickLengthMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfXtickLengthMin"), ORIGjtfXtickLengthMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfYtickLengthMaj"), ORIGjtfYtickLengthMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjtfYtickLengthMin"), ORIGjtfYtickLengthMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowLabelMaj"), ORIGjcbXShowLabelMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbXShowLabelMin"), ORIGjcbXShowLabelMin);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowLabelMaj"), ORIGjcbYShowLabelMaj);
        DatenSpeicher.appendAsString(ascii.append("\nORIGjcbYShowLabelMin"), ORIGjcbYShowLabelMin);
        //=========================
        DatenSpeicher.appendAsString(ascii.append("\nanzSignalePlusZeit"), anzSignalePlusZeit);
        DatenSpeicher.appendAsString(ascii.append("\nsignalNamen"), signalNamen);
        if(matrixZuordnungKurveDiagram != null)
        DatenSpeicher.appendAsString(ascii.append("\nmatrixZuordnungKurveDiagram"), matrixZuordnungKurveDiagram);
        if (indexWsXY==null) indexWsXY=new int[][]{{}};  // um eventuelle NullPointerException beim Speichern zu vermeiden
        DatenSpeicher.appendAsString(ascii.append("\nindexWsXY"), indexWsXY);
        //=========================
        DatenSpeicher.appendAsString(ascii.append("\nkurvenanzahl"), kurvenanzahl);
        DatenSpeicher.appendAsString(ascii.append("\nindexDerKurveInDerMatrix"), indexDerKurveInDerMatrix);
        DatenSpeicher.appendAsString(ascii.append("\nindexDerKurveInDerMatrixALT"), indexDerKurveInDerMatrixALT);
        if(crvSymbShow != null) {
            DatenSpeicher.appendAsString(ascii.append("\ncrvSymbShow"), crvSymbShow);
        DatenSpeicher.appendAsString(ascii.append("\ncrvAchsenTyp"), crvAchsenTyp);
        DatenSpeicher.appendAsString(ascii.append("\ncrvLineStyle"), crvLineStyle);
        DatenSpeicher.appendAsString(ascii.append("\ncrvLineColor"), crvLineColor);
        DatenSpeicher.appendAsString(ascii.append("\ncrvTransparency"), crvTransparency);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbFrequ"), crvSymbFrequ);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbShape"), crvSymbShape);
        DatenSpeicher.appendAsString(ascii.append("\ncrvSymbColor"), crvSymbColor);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipXmin"), crvClipXmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipXmax"), crvClipXmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipYmin"), crvClipYmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipYmax"), crvClipYmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValXmin"), crvClipValXmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValXmax"), crvClipValXmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValYmin"), crvClipValYmin);
        DatenSpeicher.appendAsString(ascii.append("\ncrvClipValYmax"), crvClipValYmax);
        DatenSpeicher.appendAsString(ascii.append("\ncrvFillDigitalCurves"), crvFillDigitalCurves);
        DatenSpeicher.appendAsString(ascii.append("\ncrvFillingDigitalColor"), crvFillingDigitalColor);
        }
        //------------------
        ascii.append("\n<\\scopeSettings2>");*/
        //------------------
    }




    public boolean importASCII (String asciiBlock) {
        StringTokenizer stk= null;
        String zz= "";
        String[] ascii= DatenSpeicher.makeStringArray(asciiBlock);
        //System.out.println(asciiBlock+"\n********************");
        //------------------
        for (int i1=0;  i1<ascii.length;  i1++) {
            //-------------------------
            if (ascii[i1].startsWith("usesExternalData ")) usesExternalData= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("anzGrfVisible ")) anzGrfVisible= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("anzDiagram ")) anzDiagram= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("jcbShowLegende[] ")) jcbShowLegende= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("showAxisX[] ")) showAxisX= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("showAxisY[] ")) showAxisY= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
//            if (ascii[i1].startsWith("powerAnalysisCurrents[] ")) {
//                powerAnalysisCurrentIndex = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
//            }
//            if (ascii[i1].startsWith("powerAnalysisVoltages[] ")) {
//                powerAnalysisVoltageIndex = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
//            }
            if (ascii[i1].startsWith("autoScaleX[] ")) autoScaleX= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("autoScaleY[] ")) autoScaleY= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("xShowGridMaj[] ")) xShowGridMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("xShowGridMin[] ")) xShowGridMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("yShowGridMaj[] ")) yShowGridMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("yShowGridMin[] ")) yShowGridMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("xTickAutoSpacing[] ")) xTickAutoSpacing= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("yTickAutoSpacing[] ")) yTickAutoSpacing= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("yTickAutoSpacing[] ")) yTickAutoSpacing= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("zeigeLabelsXmaj[] ")) zeigeLabelsXmaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            
            if (ascii[i1].startsWith("zeigeLabelsXmin[] ")) zeigeLabelsXmin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("zeigeLabelsYmaj[] ")) zeigeLabelsYmaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("zeigeLabelsYmin[] ")) zeigeLabelsYmin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("diagramTyp[] ")) diagramTyp= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("notwendigeHoehePixGRF[] ")) notwendigeHoehePixGRF= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xAchsenTyp[] ")) xAchsenTyp= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yAchsenTyp[] ")) yAchsenTyp= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xAchseFarbe[] ")) xAchseFarbe= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yAchseFarbe[] ")) yAchseFarbe= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xAchseStil[] ")) xAchseStil= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yAchseStil[] ")) yAchseStil= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("gridNormalX_zugeordneteXAchse[] ")) gridNormalX_zugeordneteXAchse= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("gridNormalX_zugeordneteYAchse[] ")) gridNormalX_zugeordneteYAchse= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("gridNormalY_zugeordneteXAchse[] ")) gridNormalY_zugeordneteXAchse= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("gridNormalY_zugeordneteYAchse[] ")) gridNormalY_zugeordneteYAchse= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("farbeGridNormalX[] ")) farbeGridNormalX= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("farbeGridNormalXminor[] ")) farbeGridNormalXminor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("farbeGridNormalY[] ")) farbeGridNormalY= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("farbeGridNormalYminor[] ")) farbeGridNormalYminor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("linStilGridNormalX[] ")) linStilGridNormalX= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("linStilGridNormalXminor[] ")) linStilGridNormalXminor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("linStilGridNormalY[] ")) linStilGridNormalY= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("linStilGridNormalYminor[] ")) linStilGridNormalYminor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xAnzTicksMinor[] ")) xAnzTicksMinor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yAnzTicksMinor[] ")) yAnzTicksMinor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xTickLaenge[] ")) xTickLaenge= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("xTickLaengeMinor[] ")) xTickLaengeMinor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yTickLaenge[] ")) yTickLaenge= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("yTickLaengeMinor[] ")) yTickLaengeMinor= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ySpacingDiagram[] ")) ySpacingDiagram= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("minX[] ")) minX= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("maxX[] ")) maxX= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("minY[] ")) minY= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("maxY[] ")) maxY= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("xTickSpacing[] ")) xTickSpacing= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("yTickSpacing[] ")) yTickSpacing= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            if (ascii[i1].startsWith("nameDiagram[] ")) nameDiagram= DatenSpeicher.leseASCII_StringArray1(ascii[i1]);
            if (ascii[i1].startsWith("xAchseBeschriftung[] ")) xAchseBeschriftung= DatenSpeicher.leseASCII_StringArray1(ascii[i1]);
            if (ascii[i1].startsWith("yAchseBeschriftung[] ")) yAchseBeschriftung= DatenSpeicher.leseASCII_StringArray1(ascii[i1]);
            //-------------------------
            if (ascii[i1].startsWith("positionSIGNAL[] ")) positionSIGNAL= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("positionSIGNAL_ALT[] ")) positionSIGNAL_ALT= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("sgnHeight[] ")) sgnHeight= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("sgnDistance[] ")) sgnDistance= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("sgnSchwelle[] ")) sgnSchwelle= DatenSpeicher.leseASCII_doubleArray1(ascii[i1]);
            //-------------------------
            if (ascii[i1].startsWith("ORIGjcbXShowGridMaj[] ")) ORIGjcbXShowGridMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbXShowGridMin[] ")) ORIGjcbXShowGridMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbYShowGridMaj[] ")) ORIGjcbYShowGridMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbYShowGridMin[] ")) ORIGjcbYShowGridMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcmXlinCol[] ")) ORIGjcmXlinCol= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcmYlinCol[] ")) ORIGjcmYlinCol= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcmXlinStyl[] ")) ORIGjcmXlinStyl= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcmYlinStyl[] ")) ORIGjcmYlinStyl= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjtfXtickLengthMaj[] ")) ORIGjtfXtickLengthMaj= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjtfXtickLengthMin[] ")) ORIGjtfXtickLengthMin= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjtfYtickLengthMaj[] ")) ORIGjtfYtickLengthMaj= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjtfYtickLengthMin[] ")) ORIGjtfYtickLengthMin= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbXShowLabelMaj[] ")) ORIGjcbXShowLabelMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbXShowLabelMin[] ")) ORIGjcbXShowLabelMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbYShowLabelMaj[] ")) ORIGjcbYShowLabelMaj= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            if (ascii[i1].startsWith("ORIGjcbYShowLabelMin[] ")) ORIGjcbYShowLabelMin= DatenSpeicher.leseASCII_booleanArray1(ascii[i1]);
            //=========================
            if (ascii[i1].startsWith("anzSignalePlusZeit ")) anzSignalePlusZeit= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("signalNamen[] ")) signalNamen= DatenSpeicher.leseASCII_StringArray1(ascii[i1]);
            if (ascii[i1].startsWith("matrixZuordnungKurveDiagram[][] ")) matrixZuordnungKurveDiagram= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("indexWsXY[][] ")) indexWsXY= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            //=========================
            if (ascii[i1].startsWith("kurvenanzahl ")) kurvenanzahl= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("indexDerKurveInDerMatrix[] ")) indexDerKurveInDerMatrix= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("indexDerKurveInDerMatrixALT[] ")) indexDerKurveInDerMatrixALT= DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            if (ascii[i1].startsWith("crvAchsenTyp[][] ")) crvAchsenTyp= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvLineStyle[][] ")) crvLineStyle= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvLineColor[][] ")) crvLineColor= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvTransparency[][] ")) crvTransparency= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvSymbFrequ[][] ")) crvSymbFrequ= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvSymbShape[][] ")) crvSymbShape= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvSymbColor[][] ")) crvSymbColor= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipXmin[][] ")) crvClipXmin= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipXmax[][] ")) crvClipXmax= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipYmin[][] ")) crvClipYmin= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipYmax[][] ")) crvClipYmax= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipValXmin[][] ")) crvClipValXmin= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipValXmax[][] ")) crvClipValXmax= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipValYmin[][] ")) crvClipValYmin= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvClipValYmax[][] ")) crvClipValYmax= DatenSpeicher.leseASCII_doubleArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvSymbShow[][] ")) crvSymbShow= DatenSpeicher.leseASCII_booleanArray2(ascii[i1]);
            //=============
            // Achtung: in alten Versionen sind diese beiden Parameter nicht gesetzt: 
            if (ascii[i1].startsWith("crvFillDigitalCurves[][] ")) crvFillDigitalCurves= DatenSpeicher.leseASCII_booleanArray2(ascii[i1]);
            if (ascii[i1].startsWith("crvFillingDigitalColor[][] ")) crvFillingDigitalColor= DatenSpeicher.leseASCII_intArray2(ascii[i1]);
            if (ascii[i1].startsWith("powerAnalysisCurrents[] ")) {
                powerAnalysisCurrentIndex = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            }
            
            if (ascii[i1].startsWith("powerAnalysisVoltages[] ")) {
                powerAnalysisVoltageIndex = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            }
            
            
            //-------------------------
        }
        //=================
        // Achtung: in alten Versionen sind diese beiden Parameter nicht gesetzt, daher hier default-Initialisierung: 
        if (crvTransparency==null) {
            crvTransparency = new double[crvLineColor.length][crvLineColor[0].length];
            for(int i = 0; i < crvTransparency.length; i++) {
                for(int j = 0; j < crvTransparency[0].length; j++) {
                    crvTransparency[i][j] = 1.0;
                }
            }
        }
        if (crvFillDigitalCurves==null) {
            if(crvLineColor != null) {
            crvFillDigitalCurves= new boolean[crvLineColor.length][crvLineColor[0].length]; 
            crvFillingDigitalColor= new int[crvLineColor.length][crvLineColor[0].length]; 
            for (int i1=0;  i1<crvLineColor.length;  i1++)
                for (int i2=0;  i2<crvLineColor[0].length;  i2++) {
                    crvFillDigitalCurves[i1][i2]= true; 
                    crvFillingDigitalColor[i1][i2]= GraferV3.LIGTHGRAY; 
                }
            }
        } 
        //------------------------------------------------------------------------------------------
        // hier erfolgt vorerst keine weitere Pruefung, ob die SCOPE-Version fehlerhaft oder veraltet ist -->
        return true;
    }





}


