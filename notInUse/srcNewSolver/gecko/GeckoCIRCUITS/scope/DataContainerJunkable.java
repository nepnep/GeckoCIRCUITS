/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import java.util.ArrayList;

/**
 *
 * @author andy
 */
public abstract class DataContainerJunkable implements DataContainer {

    protected ArrayList<DataJunk> _data = new ArrayList<DataJunk>();
    protected int JUNK_SIZE = 2048;
    protected TimeSerie _timeSerie = new TimeSeriesDynamic();
    ;
    protected final int _rows;
    protected int _totalDataSize = 0;
    double tStart;
    double tEnd;

    public DataContainerJunkable(int numberOfRows) {
        _rows = numberOfRows;
    }

    public double getTimeValue(int column) {
        return _timeSerie.getValue(column);
    }

    public void setTimeSeries(TimeSerie timeSerie) {
        _timeSerie = timeSerie;
    }

    public int getMaximumTimeIndex() {
        return _timeSerie.getMaximumIndex();
    }

    public HiLoData getHiLoValue(int row, int column, int columnOld) {

        try {
            int minIndex = Math.min(column, columnOld);
            int maxIndex = Math.max(column, columnOld);

            int junkIndex = minIndex / JUNK_SIZE;
            DataJunk junk = _data.get(junkIndex);
            HiLoData hiLoData = junk.getHiLoValue(row - 1, minIndex, maxIndex);
            try {
                while (junkIndex < maxIndex / JUNK_SIZE) {
                    junkIndex++;
                    junk = _data.get(junkIndex);
                    hiLoData.insertCompare(junk.getHiLoValue(row - 1, minIndex, maxIndex));
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.printStackTrace();
            }

            return hiLoData;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected abstract void checkAndCorrectContainerSize(int index);

    public int getRowLength() {
        return _rows;
    }

    public int getColumnLength() {
        return getMaximumTimeIndex();
    }

    public void setColumn(double[] data, int index) {
        assert false;
        System.err.println("set rows from compressable not yet implemented");
    }

    public double[] getColumn(int index) {
        System.err.println("get rows from compressable not yet implemented");
        return null;
    }
}
