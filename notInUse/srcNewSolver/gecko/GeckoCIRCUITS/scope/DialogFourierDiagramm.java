/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;
import gecko.GeckoCIRCUITS.allg.TechFormat;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.Event;
import javax.swing.KeyStroke;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.JDialog;
import java.net.URL;
import java.util.StringTokenizer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;

public class DialogFourierDiagramm extends JDialog implements WindowListener, ComponentListener {

    //-------------------
    private JTabbedPane tabber;
    private FourierDiagramm[] diag;
    private DisplayFourierWorksheet[] sheet;
    private FourierKurvenRekonstruktion[] rekonstruktion;
    //
    private DataContainer worksheet;
    private double[][] cn, jn, an, bn;
    private double f1;
    private int nMin;
    private double rng1, rng2;
    private String[] header;
    private boolean[] signalFourierAnalysiert;
    //
    private int mausModus = GraferImplementation.MAUSMODUS_NIX;
    private int iconONnummerALT = 0;
    //-------------------
    private ImageIcon[] iconOFF, iconON;
    private JButton[] jbMaus;
    private JToolBar jtb1;
    private JMenuItem mItemF3, mItemF5;
    //-----------------------

    public DialogFourierDiagramm(
            double[][][] erg, String[] header1, boolean[] sngFourierAnalysiert, int nMin, double f1, DataContainer worksheet,
            double rng1, double rng2, int xEltern, int yEltern) {
        super.setModal(true);
        this.addComponentListener(this);
        try {
            this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL, "gecko.gif"))).getImage());
        } catch (Exception e) {
        }
        //--------------------------
        this.an = erg[0];
        this.bn = erg[1];
        this.cn = erg[2];
        this.jn = erg[3];
        this.header = header1;
        this.signalFourierAnalysiert = sngFourierAnalysiert;
        this.nMin = nMin;
        this.f1 = f1;
        this.rng1 = rng1;
        this.rng2 = rng2;
        this.worksheet = worksheet;
        this.addWindowListener(this);
        //--------------------------
        this.setTitle(Typ.spTitle + "Diagram Fourier-Transform");
        this.getContentPane().setLayout(new BorderLayout());
        this.baueGUItoolbar();
        this.baueGUI();
        this.pack();
        if (Typ.mode_location == Typ.LOCATION_BY_PROGRAM_A) {
            this.setLocation(xEltern - this.getWidth(), yEltern);  // Positionierung relativ zum 'Eltern'-Fenster
        } else if (Typ.mode_location == Typ.LOCATION_BY_PLATFORM) {
            this.setLocationByPlatform(true);
        }
        //------------------------
    }

    private void baueGUI() {
        //------------------------
        tabber = new JTabbedPane();
        diag = new FourierDiagramm[header.length];
        sheet = new DisplayFourierWorksheet[header.length];
        rekonstruktion = new FourierKurvenRekonstruktion[header.length];
        //
        for (int i1 = 1; i1 < header.length; i1++) {
            if (signalFourierAnalysiert[i1]) {
                JPanel jpSg = new JPanel();
                jpSg.setLayout(new BorderLayout());
                JTabbedPane tabberLok = new JTabbedPane();
                tabberLok.setFont(TxtI.ti_Font_A);
                //---------
                diag[i1 - 1] = new FourierDiagramm(cn[i1 - 1], nMin, f1); 
                tabberLok.addTab(TxtI.ti_fa_DialogFourierDiagramm, diag[i1 - 1]); 
                sheet[i1 - 1] = new DisplayFourierWorksheet(cn[i1 - 1], jn[i1 - 1], nMin);
                tabberLok.addTab(TxtI.ti_wd_DialogFourierDiagramm, sheet[i1 - 1]);
                rekonstruktion[i1 - 1] = new FourierKurvenRekonstruktion(an[i1 - 1], bn[i1 - 1], nMin, f1, worksheet, i1, rng1, rng2);
                tabberLok.addTab(TxtI.ti_rec_DialogFourierDiagramm, rekonstruktion[i1 - 1]);
                //---------
                jpSg.add(tabberLok, BorderLayout.CENTER);
                tabber.addTab(header[i1] + " ", jpSg);
            }
        }
        //------------------------
        JPanel jpIN = new JPanel();  // User-Input:  n-Bereich-Einschraenkung, OK usw ...
        //------------------------
        Container con = this.getContentPane();
        con.setLayout(new BorderLayout());
        con.add(tabber, BorderLayout.CENTER);
        con.add(jpIN, BorderLayout.SOUTH);
        con.add(jtb1, BorderLayout.WEST);
        //=======================================
        final JDialog ich = this;  // Selbstreferenz
        final FileFilter filter = new FileFilter() {

            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                if (f.getName().endsWith(".dat")) {
                    return true;
                } else {
                    return false;
                }
            }

            public String getDescription() {
                return new String("Data File, Space-Spr. (*.dat)");
            }
        };
        //-----------
        JMenu dataMenu = new JMenu(TxtI.ti_File);
        dataMenu.setFont(TxtI.ti_Font_A);
        mItemF3 = new JMenuItem(TxtI.ti_jmiWri_ScopeX);
        mItemF3.setFont(TxtI.ti_Font_A);
        mItemF3.setMnemonic(KeyEvent.VK_S);
        mItemF3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                //------------------------------
                JFileChooser dialog = new JFileChooser();
                dialog.addChoosableFileFilter(filter);
                try {
                    String datnam = Typ.DATNAM;
                    if (datnam.equals(Typ.DATNAM_NOT_DEFINED)) {
                        datnam = Typ.PFAD_JAR_HOME;
                    }
                    dialog.setCurrentDirectory(new File(datnam));
                } catch (Exception e) {
                    dialog.setCurrentDirectory(new File(Typ.PFAD_JAR_HOME));
                }
                int result = dialog.showSaveDialog(ich);
                if (result == JFileChooser.CANCEL_OPTION) {
                    return;
                }
                String datnam = dialog.getSelectedFile().getAbsolutePath();
                if (!datnam.endsWith(".dat")) {
                    datnam += (".dat");
                }
                //------------------
                // zuerst die Signal-Namen in der obersten Zeile -->
                StringBuffer sb = new StringBuffer();
                sb.append("N ");
                for (int i1 = 1; i1 < header.length; i1++) {
                    if (signalFourierAnalysiert[i1]) {
                        sb.append(header[i1] + "_cN " + header[i1] + "_phiN[rad] ");
                    }
                }
                sb.append("\n");
                // jetzt die zugehoerigen Daten in allen folgenden Zeilen -->
                for (int i1 = 0; i1 < cn[0].length; i1++) {
                    sb.append(i1 + " ");
                    for (int i2 = 1; i2 < header.length; i2++) {
                        if (signalFourierAnalysiert[i2]) {
                            sb.append(cn[i2 - 1][i1] + " " + jn[i2 - 1][i1] + " ");
                        }

                    }
                    sb.append("\n");
                }
                // ... und jetzt abspeichern -->
                try {
                    BufferedWriter fkaku = new BufferedWriter(new FileWriter(new File(datnam)));
                    fkaku.write(sb.toString());
                    fkaku.flush();
                    fkaku.close();
                } catch (Exception e) {
                    System.out.println(e + "   qe90r8gn03g8q");
                }
                //------------------------------
            }
        });
        JMenuItem mItemF5 = new JMenuItem(TxtI.ti_Exit);
        mItemF5.setFont(TxtI.ti_Font_A);
        mItemF5.setMnemonic(KeyEvent.VK_X);
        mItemF5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                schliesseFenster();
            }
        });
        dataMenu.add(mItemF3);
        mItemF3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
        dataMenu.add(mItemF5);
        mItemF5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK));
        //
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(dataMenu);
        this.setJMenuBar(menuBar);
        //=======================================
    }

    private void baueGUItoolbar() {
        //--------------------
        try {
            iconON = new ImageIcon[]{
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_off.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_zoomFit2.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_zoomFenster.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_getXYschieber.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_log.png")), /*, /*
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_zoomDX.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_getXYkreuz.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_drawLine.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_drawText.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_measureDX.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_measureDY.png")),
                     */};
            iconOFF = new ImageIcon[]{
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_off.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_zoomFit2.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_zoomFenster.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_getXYschieber.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_log.png")), /*
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_zoomDX.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_getXYkreuz.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_drawLine.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_drawText.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_measureDX.png")),
                    new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_measureDY.png")),
                     */};
        } catch (Exception e) {
            System.out.println(e);
        }
        //
        jbMaus = new JButton[iconOFF.length];
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jbMaus[i1] = new JButton();
            jbMaus[i1].setFont(TxtI.ti_Font_A);
            jbMaus[i1].setIcon(iconOFF[i1]);
            jbMaus[i1].setActionCommand("mausModus " + i1);
            jbMaus[i1].addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae) {
                    aktualisiereMausModus(ae);
                }
            });

            //--------------------
            switch (i1) {
                case 0:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt1_ScopeX);
                    break;
                case 1:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt2_ScopeX);
                    break;
                case 2:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt3_ScopeX);
                    break;
                case 3:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt4_ScopeX);
                    break;
                case 4:
                    jbMaus[i1].setToolTipText("set logarithmic y-axis");
                    break;
                /*
                case  4:  jbMaus[i1].setToolTipText("Define X-range");  break;
                case  5:  jbMaus[i1].setToolTipText("Set marker for single X/Y-values");  break;
                case  6:  jbMaus[i1].setToolTipText("Draw line");  break;
                case  7:  jbMaus[i1].setToolTipText("Draw text");  break;
                case  8:  jbMaus[i1].setToolTipText("Measure dX-length");  break;
                case  9:  jbMaus[i1].setToolTipText("Measure dY-length");  break;
                 */
                default:
                    System.out.println("Fehler: 49ugnw3grjgtfzj " + i1);
                    break;
            }
            //--------------------
        }
        jbMaus[0].setIcon(iconON[0]);  // default: Maus deaktiviert
        
        //--------------------
        jtb1 = new JToolBar(TxtI.ti_jtb_ScopeX);
        jtb1.setFont(TxtI.ti_Font_B);
        jtb1.setOrientation(JToolBar.VERTICAL);
        jtb1.setFloatable(false);
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jtb1.add(jbMaus[i1]);
        }
        //--------------------
    }

    private int yAchseTyp = GraferV3.ACHSE_LIN;

    private void aktualisiereMausModus(ActionEvent ae) {
        //--------------------
        // alten Zustand speichern: 
        int mausModusALT = -1;
        //--------------------
        StringTokenizer stk = new StringTokenizer(ae.getActionCommand(), " ");
        stk.nextToken();
        int indexKnopfGedrueckt = new Integer(stk.nextToken()).intValue();
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jbMaus[i1].setIcon(iconOFF[i1]);
        }
        jbMaus[indexKnopfGedrueckt].setIcon(iconON[indexKnopfGedrueckt]);
        //--------------------
        switch (indexKnopfGedrueckt) {
            case 0:
                mausModus = GraferImplementation.MAUSMODUS_NIX;
                break;
            case 1:
                mausModusALT = mausModus;
                mausModus = GraferImplementation.MAUSMODUS_ZOOM_AUTOFIT;
                break;
            case 2:
                mausModus = GraferImplementation.MAUSMODUS_ZOOM_FENSTER;
                break;
            case 3:
                mausModus = GraferImplementation.MAUSMODUS_WERTANZEIGE_SCHIEBER;
                break;
            case 4:
                if(yAchseTyp == GraferV3.ACHSE_LIN) {
                   yAchseTyp = GraferV3.ACHSE_LOG;
                   jbMaus[4].setIcon(iconON[4]);
                } else {
                    yAchseTyp = GraferV3.ACHSE_LIN;
                    jbMaus[4].setIcon(iconOFF[4]);
                }
                for (int i = 0; i < diag.length; i++) {
                    if (diag[i] != null) {
                        diag[i].setzeAchsenTyp(new int[]{GraferV3.ACHSE_LIN}, new int[]{yAchseTyp});
                    }
                }

                break;
            /*
            case  4:  grafer.setMausModus(GraferImplementation.MAUSMODUS_MARKIERE_X_BEREICH);  break;
            case  5:  grafer.setMausModus(GraferImplementation.MAUSMODUS_WERTANZEIGE_FADENKREUZ);  break;
            case  6:  grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_LINIE);  break;
            case  7:  grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_TEXT);  break;
            case  8:  grafer.setMausModus(GraferImplementation.MAUSMODUS_DISTANZMESSUNG_X);  break;
            case  9:  grafer.setMausModus(GraferImplementation.MAUSMODUS_DISTANZMESSUNG_Y);  break;
            case 10:  grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_FIBONACCI_LIN);  break;
            case 11:  grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_FIBONACCI_LOG);  break;
             */
            default:
                System.out.println("Fehler: 98n3gweggtq5t");
                break;
        }
        for (int i1 = 1; i1 < header.length; i1++) {
            if (signalFourierAnalysiert[i1]) {
                diag[i1 - 1].setMausModus(mausModus);
                rekonstruktion[i1 - 1].setMausModus(mausModus);
            }
        }
        if (mausModus == GraferImplementation.MAUSMODUS_ZOOM_AUTOFIT) {
            mausModus = mausModusALT;
            jbMaus[1].setIcon(iconOFF[1]);
            jbMaus[iconONnummerALT].setIcon(iconON[iconONnummerALT]);
            for (int i1 = 1; i1 < header.length; i1++) {
                if (signalFourierAnalysiert[i1]) {
                    diag[i1 - 1].setMausModus(mausModus);
                    rekonstruktion[i1 - 1].setMausModus(mausModus);
                }
            }
        } else {
            iconONnummerALT = indexKnopfGedrueckt;
        }
        //--------------------
    }

    //------------------------------------------------
    public void componentResized(ComponentEvent ce) {
        for (int i1 = 1; i1 < header.length; i1++) {
            if (signalFourierAnalysiert[i1]) {
                diag[i1 - 1].resize();
                diag[i1 - 1].repaint();
                rekonstruktion[i1 - 1].resize();
                rekonstruktion[i1 - 1].repaint();
            }
        }
    }

    public void componentMoved(ComponentEvent ce) {
    }

    public void componentShown(ComponentEvent ce) {
    }

    public void componentHidden(ComponentEvent ce) {
    }
    //------------------------------------------------

    //------------------------------------------------
    public void windowDeactivated(WindowEvent we) {
    }

    public void windowActivated(WindowEvent we) {
    }

    public void windowDeiconified(WindowEvent we) {
    }

    public void windowIconified(WindowEvent we) {
    }

    public void windowClosed(WindowEvent we) {
    }

    public void windowClosing(WindowEvent we) {
        this.schliesseFenster();
    }

    public void windowOpened(WindowEvent we) {
    }
    //------------------------------------------------

    public void schliesseFenster() {
        this.dispose();
    }
}



//****************************************************************************
//****************************************************************************
//****************************************************************************
class DisplayFourierWorksheet extends JPanel {

    //----------------------------
    private JTable table;
    private JScrollPane jsp;
    private Object[][] wsObj;
    private TechFormat cf = new TechFormat();
    //----------------------------
    private double[] cnSG, jnSG;
    private int nMin;
    //----------------------------

    public DisplayFourierWorksheet(double[] cnSG, double[] jnSG, int nMin) {
        this.cnSG = cnSG;
        this.jnSG = jnSG;
        this.nMin = nMin;
        this.schreibeData();
        this.baueGUI();
    }

    private void baueGUI() {
        this.removeAll();
        this.setLayout(new BorderLayout());
        jsp = new JScrollPane(table);
        this.add(jsp, BorderLayout.CENTER);
    }

    private void schreibeData() {
        String[] header = new String[]{"n", "c_n", "phi_n [rad]"};
        Object[][] wsObj = new Object[cnSG.length][header.length];
        for (int i1 = 0; i1 < wsObj.length; i1++) {
            for (int i2 = 0; i2 < wsObj[0].length; i2++) {
                if (i2 == 0) {
                    wsObj[i1][i2] = new String(cf.formatT(i1 + nMin, TechFormat.FORMAT_AUTO));
                } else if (i2 == 1) {
                    wsObj[i1][i2] = new String(cf.formatT(cnSG[i1], TechFormat.FORMAT_AUTO));
                } else if (i2 == 2) {
                    wsObj[i1][i2] = new String(cf.formatT(jnSG[i1], TechFormat.FORMAT_AUTO));
                }
            }
        }
        table = new JTable(wsObj, header) {
            // ueberschreiben, damit man nicht in den Daten herumeditieren kann -->

            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
    }
}

