/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.TxtI;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

/**
 *
 * @author andy
 */
public class JPanelDialogRange extends JPanel {

    public JRadioButton jrb1, jrb2, jrb3;
    private FormatJTextField rngSc1, rngSc2, rngDf1, rngDf2, rngSl1, rngSl2;  // Angaben Zeitbereiche
    public double xScope1, xScope2;  // durch den Scope-Anzeigenbereich definierter Rechenbereich
    private double xSlider1, xSlider2;  // vom Slider-Paar definierter Rechenbereich
    private double xDef1, xDef2;  // von Hand definierter Rechenbereich

    public JPanelDialogRange(DataContainer worksheet, double slider1, double slider2) {
        setLayout(new GridLayout(6, 1));
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogAvgRms, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));

        //--------------------------
        // Default-Bereichsgrenzen:
        xScope1 = worksheet.getValue(0, 1);  // weil bei 'Continue' oft der erste Worksheet-Wert Null ist
        int i2 = worksheet.getColumnLength() - 1;
        xScope2 = worksheet.getValue(0, i2);
        while ((xScope2 == 0) && (i2 > 0)) {
            xScope2 = worksheet.getValue(0, i2);
            i2--;
        }
        if (xScope2 == xScope1) {
            xScope2 = xScope1 + 1;  // damit keine Division durch Null
        }        //System.out.println("range --> "+xScope1+"   "+xScope2);
        // die anderen beiden Einstellungen werden vorerst genauso gesetzt:
        xDef1 = xScope1;
        xDef2 = xScope2;
        xSlider1 = xScope1;
        xSlider2 = xScope2;


        ButtonGroup group = new ButtonGroup();
        jrb1 = new JRadioButton(TxtI.ti_scRa_DialogFourier);
        jrb1.setFont(TxtI.ti_Font_A);
        jrb1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(true);
                rngSc2.setEnabled(true);
                rngDf1.setEnabled(false);
                rngDf2.setEnabled(false);
                rngSl1.setEnabled(false);
                rngSl2.setEnabled(false);
                rngDf1.setEditable(false);
                rngDf2.setEditable(false);
            }
        });
        jrb2 = new JRadioButton(TxtI.ti_dfRa_DialogFourier);
        jrb2.setFont(TxtI.ti_Font_A);
        jrb2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(false);
                rngSc2.setEnabled(false);
                rngDf1.setEnabled(true);
                rngDf2.setEnabled(true);
                rngSl1.setEnabled(false);
                rngSl2.setEnabled(false);
                rngDf1.setEditable(true);
                rngDf2.setEditable(true);
            }
        });
        jrb3 = new JRadioButton(TxtI.ti_slRa_DialogFourier);
        jrb3.setFont(TxtI.ti_Font_A);
        jrb3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(false);
                rngSc2.setEnabled(false);
                rngDf1.setEnabled(false);
                rngDf2.setEnabled(false);
                rngSl1.setEnabled(true);
                rngSl2.setEnabled(true);
                rngDf1.setEditable(false);
                rngDf2.setEditable(false);
            }
        });
        group.add(jrb1);
        group.add(jrb2);
        group.add(jrb3);
        //
        this.add(jrb1);
        JPanel jpR1 = new JPanel();
        int maximumFractionDigits = 9;
        rngSc1 = new FormatJTextField(xScope1, maximumFractionDigits);
        int cols = 9;
        rngSc1.setColumns(cols);
        rngSc2 = new FormatJTextField(xScope2, maximumFractionDigits);
        rngSc2.setColumns(cols);
        jpR1.add(rngSc1);
        jpR1.add(rngSc2);
        this.add(jpR1);
        //
        this.add(jrb2);
        
        //
        JPanel jpR2 = new JPanel();
        rngDf1 = new FormatJTextField(xDef1);
        rngDf1.setColumns(cols);
        rngDf1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                xDef1 = rngDf1.getNumberFromField();
            }
        });
        rngDf2 = new FormatJTextField(xDef2);
        rngDf2.setColumns(cols);
        rngDf2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                xDef2 = rngDf2.getNumberFromField();
            }
        });
        jpR2.add(rngDf1);
        jpR2.add(rngDf2);
        this.add(jpR2);
        
        setSliderValues(slider1, slider2);
        
        this.add(jrb3);
        JPanel jpR3 = new JPanel();
        rngSl1 = new FormatJTextField(xSlider1);
        rngSl1.setColumns(cols);
        rngSl2 = new FormatJTextField(xSlider2);
        rngSl2.setColumns(cols);
        jpR3.add(rngSl1);
        jpR3.add(rngSl2);
        this.add(jpR3);
        //
        //-------------------
        // Initialisieren -->
        jrb1.setSelected(true);
        rngSc1.setEditable(false);
        rngSc2.setEditable(false);
        rngDf1.setEditable(false);
        rngDf2.setEditable(false);
        rngSl1.setEditable(false);
        rngSl2.setEditable(false);
        //
        rngSc1.setEnabled(true);
        rngSc2.setEnabled(true);
        rngDf1.setEnabled(false);
        rngDf2.setEnabled(false);
        rngSl1.setEnabled(false);
        rngSl2.setEnabled(false);

        
    }

    double getStopTimeValue() {
        if (jrb1.isSelected()) {
            return xScope2;
        } else if (jrb2.isSelected()) {
            return xDef2;
        } else {
            return xSlider2;
        }
    }

    double getStartTimeValue() {
        if (jrb1.isSelected()) {
            return xScope1;
        } else if (jrb2.isSelected()) {
            return xDef1;
        } else {
            return xSlider1;
        }
    }
    
    public void setSliderValues(double value1, double value2) {
        xSlider1 = Math.min(value2, value1);
        xSlider2 = Math.max(value2, value1);
    }
}
