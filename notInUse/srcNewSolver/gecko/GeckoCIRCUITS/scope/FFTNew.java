/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andy
 */
public class FFTNew {

    public float[] zvResampled;
    public float[] timeValuesResampled;
    public float[] fftSpectrumReal;
    public float[] fftSpectrumImag;
    public float[] frequencies;
    public float maxResolutionFrequency;
    public int maximumNN;
    private int NN = 0;
    public float baseFrequency = 0;
    public int reductor;

    public FFTNew(DataContainer zv, int reductor) {
        float[] real = null;

        doResampling(zv, 0, 1, reductor);

        real = new float[NN + 1];

        for (int i = 0; i < NN; i++) {
            real[i + 1] = (float) zvResampled[i];
        }

        blackman(real);
        realft(real, 1);
        this.reductor = reductor;
        

        baseFrequency = 1.0f / (timeValuesResampled[NN - 1] - timeValuesResampled[0]);
        float frequency = baseFrequency;

        fftSpectrumImag = new float[NN / 2];
        fftSpectrumReal = new float[NN / 2];
        frequencies = new float[NN / 2];

        for (int i = 0; i < NN / 2; i++) {
            fftSpectrumReal[i] = real[2 * i] / (NN / 2);
            fftSpectrumImag[i] = real[2 * i + 1] / (NN / 2);

            frequency += baseFrequency;
            frequencies[i] = (float) frequency;
        }

        maxResolutionFrequency = frequency / 2;
    }

    public void doResampling(DataContainer zv, int startOffset, int endOffset, int reductor) {
        NN = 1;
        while (NN < zv.getColumnLength() / 2) {
            NN *= 2;
        }
        maximumNN = NN;
        
        int reductorN = 1;
        for (int i = 0; i < reductor; i++) {
            NN /= 2;
            reductorN *= 2;
        }

        zvResampled = new float[NN];
        timeValuesResampled = new float[NN];
        int endShift = 0;
        while (zv.getValue(0, zv.getColumnLength() - 1 - endShift) == 0) {
            endShift += 1;
        }

        for (int i = 1; i <= NN; i++) {
            int index = zv.getColumnLength() - i * reductorN - endShift;
            for(int j = 0; j < reductorN; j++) {
                zvResampled[NN - i] += (float) zv.getValue(1, index - j);
                timeValuesResampled[NN - i] += (float) zv.getValue(0, index - j);
            }
            zvResampled[NN - i] /= reductorN;
            timeValuesResampled[NN - i] /= reductorN;
        }


    }

    public static void blackman(float[] data) {
        for (int ii = 0; ii < data.length - 1; ii++) {
            int N = data.length - 1;
            data[ii + 1] *= 0.42 - 0.5 * Math.cos(2 * ii * Math.PI / N) + 0.08 * Math.cos(4 * ii * Math.PI / N);
        }
    }

    public static void inverseBlackman(float[] data) {
        int N = data.length - 1;
        double const1 = 2 * Math.PI / N;
        double const2 = 4 * Math.PI / N;
        for (int ii = 0; ii < data.length - 1; ii++) {
            float factor = (float) (0.42 - 0.5 * Math.cos(ii * const1) + 0.08 * Math.cos(ii * const2));
//            if (factor < 0.001) {
//                factor = 1;
//            }
            data[ii + 1] /= factor;
        }
    }

    
    /**
     * 
     * @param data input-output array of length N + 1 (!)
     * @param n
     * @param isign 1 if from Timedomain to frequency domain, FD -> TD sign = -1;
     */
    public static void realft(float[] data, int isign) {
        assert isign == 1 || isign == -1;
        int n = data.length-1;
        assert n % 2 == 0 : "realft only possible with n = 2^x  data number";
        
        int i, i1, i2, i3, i4, np3;
        double c1 = 0.5, c2, h1r, h1i, h2r, h2i;
        double wr, wi, wpr, wpi, wtemp, theta;

        theta = Math.PI / (float) (n >> 1);
        if (isign == 1) {
            c2 = -0.5;
            ffour1(data, n >> 1, 1);
        } else {
            c2 = 0.5;
            theta = -theta;
        }
        wtemp = Math.sin(0.5 * theta);
        wpr = -2.0 * wtemp * wtemp;
        wpi = Math.sin(theta);
        wr = 1.0 + wpr;
        wi = wpi;
        np3 = n + 3;
        for (i = 2; i <= (n >> 2); i++) {
            i4 = 1 + (i3 = np3 - (i2 = 1 + (i1 = i + i - 1)));
            h1r = c1 * (data[i1] + data[i3]);
            h1i = c1 * (data[i2] - data[i4]);
            h2r = -c2 * (data[i2] + data[i4]);
            h2i = c2 * (data[i1] - data[i3]);
            data[i1] = (float) (h1r + wr * h2r - wi * h2i);
            data[i2] = (float) (h1i + wr * h2i + wi * h2r);
            data[i3] = (float) (h1r - wr * h2r + wi * h2i);
            data[i4] = (float) (-h1i + wr * h2i + wi * h2r);
            wr = (wtemp = wr) * wpr - wi * wpi + wr;
            wi = wi * wpr + wtemp * wpi + wi;
        }
        if (isign == 1) {
            data[1] = (float) (h1r = data[2]) + data[2];
            data[2] = (float) h1r - data[2];
        } else {
            data[1] = (float) (c1 * ((h1r = data[1] + data[2])));
            data[2] = (float) (c1 * (h1r - data[2]));
            ffour1(data, n >> 1, -1);
        }
    }

    // float-Version von four1()
    //
    public static void ffour1(float data[], int nn, int isign) {
        int n, mmax, m, j, istep, i;
        double wtemp, wr, wpr, wpi, wi, theta;  // float precision for the trigonometric recurrences.
        float tempr, tempi;
        n = nn << 1;
        j = 1;

        for (i = 1; i < n; i += 2) {  // This is the bit-reversal section of the routine.
            if (j > i) {  // Exchange the two complex numbers.
                float temp = data[j];
                data[j] = data[i];
                data[i] = temp;
                temp = data[j + 1];
                data[j + 1] = data[i + 1];
                data[i + 1] = temp;
            }
            //m = nn;
            m = n >> 1;
            while ((m >= 2) && (j > m)) {
                j -= m;
                m >>= 1;
            }
            j += m;
        }

        // Here begins the Danielson-Lanczos section of the routine.
        mmax = 2;
        while (n > mmax) {  // Outer loop executed log2 nn times.
            istep = mmax << 1;
            theta = isign * (6.28318530717959 / mmax);  // Initialize the trigonometric recurrence.
            wtemp = Math.sin(0.5 * theta);
            wpr = -2.0 * wtemp * wtemp;
            wpi = Math.sin(theta);
            wr = 1.0;
            wi = 0.0;
            for (m = 1; m < mmax; m += 2) {  // Here are the two nested inner loops.
                for (i = m; i <= n; i += istep) {
                    j = i + mmax;  // This is the Danielson-Lanczos for mula:
                    tempr = (float) (wr * data[j] - wi * data[j + 1]);
                    tempi = (float) (wr * data[j + 1] + wi * data[j]);
                    data[j] = data[i] - tempr;
                    data[j + 1] = data[i + 1] - tempi;
                    data[i] += tempr;
                    data[i + 1] += tempi;
                }
                wr = (wtemp = wr) * wpr - wi * wpi + wr;  // Trigonometric recurrence.
                wi = wi * wpr + wtemp * wpi + wi;
            }
            mmax = istep;
        }
    }
}
