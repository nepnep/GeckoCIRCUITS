/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;
import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.TechFormat;

import java.awt.Color;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import java.net.URL;

public class DialogScopeCharacteristics extends JDialog {

    private GridBagConstraints _gridBagConstraints = new GridBagConstraints();
    //private boolean focusIstFreigegeben=false;
    //-------------
    private TechFormat _cf = new TechFormat();
    private FormatJTextField _jTextFieldStatus;  // Staus-Anzeige der Berechnung
    private JPanelDialogRange _panelDialogRange;
    private PanelCharacteristicsResult _characteristicsErgPanel;
    private PowerAnalysisPanel _powerAnalysisPanel;
    private JPanel _jPanelCharacteristicsInformation;
    private JTabbedPane _characteristicsTab;
    private JPanel _jPanelPowerAnalysisInformation;
    private JPanel _panelPowerAnalysis;
    private JTabbedPane _powerAnalysisTabbedPane;
    private JPanel _jpCalc;
    private final String[] _header;
    private final DataContainer _worksheet;
    private JButton _jbCALC;

    public DialogScopeCharacteristics(final DataContainer worksheet, final String[] header, ScopeSettings scopeSettings, double slider1, double slider2) {
        _header = header;
        _worksheet = worksheet;
        setWindowProperties();
        _panelDialogRange = new JPanelDialogRange(worksheet, slider1, slider2);
        _characteristicsErgPanel = new PanelCharacteristicsResult(header);
        setPanelCharacteristicsInformation();
        setCharactericsticsTabber();
        setPanelPowerAnalysis(scopeSettings);
        
        setPowerAnalysisInformation();
        setPowerAnalysisTabber();
        setPanelCalcCancel();
        setPanelAll();
    }

    private void setWindowProperties() {
        super.setModal(true);
        try {
            this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL, "gecko.gif"))).getImage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTitle(Typ.spTitle + "Characteristics & Power Analysis");
        getContentPane().setLayout(new BorderLayout());
        _gridBagConstraints = new GridBagConstraints();
        _gridBagConstraints.fill = GridBagConstraints.BOTH;
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
   }

    private void setPanelCharacteristicsInformation() {
        _jPanelCharacteristicsInformation = new JPanel();
        
        JComponent c = new JComponent() {

            public void paint(Graphics g) {
                try {
                    g.setColor(Color.white);
                    g.fillRect(0, 0, 999, 999);  // weisser Hintergrund
                    Image equ1 = new ImageIcon(new URL(Typ.PFAD_PICS_URL, "equ1.png")).getImage();
                    g.drawImage(equ1, 0, 0, null);
                } catch (Exception e) {
                }
            }
        };
        c.setPreferredSize(new Dimension(688, 173));
        _jPanelCharacteristicsInformation.add(c);
    }

    private void setCharactericsticsTabber() {
        _characteristicsTab = new JTabbedPane();
        _characteristicsTab.setFont(TxtI.ti_Font_A);
        _characteristicsTab.addTab(TxtI.ti_jmiChar_ScopeX, _characteristicsErgPanel);
        _characteristicsTab.addTab(TxtI.ti_def_DialogAvgRms, _jPanelCharacteristicsInformation);
    }

    private void setPowerAnalysisInformation() {
        _jPanelPowerAnalysisInformation = new JPanel();
        JComponent c2 = new JComponent() {

            public void paint(Graphics g) {
                try {
                    g.setColor(Color.white);
                    g.fillRect(0, 0, 999, 999);  // weisser Hintergrund
                    Image equ1 = new ImageIcon(new URL(Typ.PFAD_PICS_URL, "equ2b.png")).getImage();
                    g.drawImage(equ1, 0, 0, null);
                } catch (Exception e) {
                }
            }
        };
        c2.setPreferredSize(new Dimension(379, 224));
        _jPanelPowerAnalysisInformation.add(c2);
        
    }

    private void setPowerAnalysisTabber() {
        _powerAnalysisTabbedPane = new JTabbedPane();
        _powerAnalysisTabbedPane.setFont(TxtI.ti_Font_A);
        _powerAnalysisTabbedPane.addTab(TxtI.ti_pa_DialogAvgRms, _panelPowerAnalysis);
        _powerAnalysisTabbedPane.addTab(TxtI.ti_def_DialogAvgRms, _jPanelPowerAnalysisInformation);
    }

    private void setPanelPowerAnalysis(ScopeSettings scopeSettings) {
        _panelPowerAnalysis = new JPanel();
        _panelPowerAnalysis.setLayout(new BorderLayout());
        _powerAnalysisPanel = new PowerAnalysisPanel(_header, scopeSettings);
        _panelPowerAnalysis.add(_powerAnalysisPanel, BorderLayout.NORTH);
        
    }

    private void setPanelCalcCancel() {
        JPanel pOK = new JPanel();
        _jbCALC = new JButton(TxtI.ti_calc_DialogFourier);
        _jbCALC.setFont(TxtI.ti_Font_A);
        _jbCALC.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                //----------------------------------
                double xDef1 = _panelDialogRange.getStartTimeValue();
                double xDef2 = _panelDialogRange.getStopTimeValue();
                if (xDef1 >= xDef2) {
                    _jTextFieldStatus.setForeground(Color.red);
                    _jTextFieldStatus.setText(TxtI.ti_e1_DialogFourier);
                    return;
                } else if ((xDef1 < _panelDialogRange.xScope1) || (xDef2 > _panelDialogRange.xScope2)) {
                    _jTextFieldStatus.setForeground(Color.red);
                    _jTextFieldStatus.setText(TxtI.ti_e2_DialogFourier);
                    return;
                }
                Thread rechner = new CalculationThread();
                //----------------------------------
                rechner.setPriority(Thread.MIN_PRIORITY);
                rechner.start();
            }
        });
        JButton jbCancel = new JButton(TxtI.ti_Cancel);
        jbCancel.setFont(TxtI.ti_Font_A);
        jbCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        pOK.add(_jbCALC);
        pOK.add(jbCancel);
        _jpCalc = new JPanel();
        _jpCalc.setLayout(new BorderLayout());
        _jpCalc.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_stat_DialogConnectSignalsGraphs, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        _jTextFieldStatus = new FormatJTextField();
        _jTextFieldStatus.setFont(TxtI.ti_Font_B);
        _jTextFieldStatus.setColumns(16);
        _jTextFieldStatus.setEditable(false);
        _jpCalc.add(pOK, BorderLayout.SOUTH);
        _jpCalc.add(_jTextFieldStatus, BorderLayout.NORTH);
    }

    private void setPanelAll() {
        JPanel pALL = new JPanel();
        pALL.setLayout(new GridBagLayout());
        pALL.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "", TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        _gridBagConstraints.fill = _gridBagConstraints.BOTH;
        _gridBagConstraints.gridx = 0;
        _gridBagConstraints.gridy = 0;
        _gridBagConstraints.gridwidth = 2;
        _gridBagConstraints.gridheight = 1;
        pALL.add(_characteristicsTab, _gridBagConstraints);
        _gridBagConstraints.gridx = 0;
        _gridBagConstraints.gridy = 1;
        _gridBagConstraints.gridwidth = 1;
        _gridBagConstraints.gridheight = 1;
        pALL.add(_panelDialogRange, _gridBagConstraints);
        _gridBagConstraints.gridx = 0;
        _gridBagConstraints.gridy = 2;
        _gridBagConstraints.gridwidth = 1;
        _gridBagConstraints.gridheight = 1;
        pALL.add(_jpCalc, _gridBagConstraints);
        _gridBagConstraints.gridx = 1;
        _gridBagConstraints.gridy = 1;
        _gridBagConstraints.gridwidth = 1;
        _gridBagConstraints.gridheight = 2;
        pALL.add(_powerAnalysisTabbedPane, _gridBagConstraints);
        //
        getContentPane().add(pALL);

        this.pack();
        this.setResizable(false);
        
    }
    
    class CalculationThread extends Thread {

                    public void run() {
                        try {
                            _jbCALC.setEnabled(false);
                            _jTextFieldStatus.setForeground(Color.blue);
                            _jTextFieldStatus.setText(TxtI.ti_calcrun_DialogAvgRms + " ... ");
                            CharacteristicsCalculator characteristics = CharacteristicsCalculator.calculateFabric(_worksheet, _panelDialogRange.getStartTimeValue(), _panelDialogRange.getStopTimeValue());;

                            for (int i1 = 0; i1 < _header.length - 1; i1++) {
                                _characteristicsErgPanel.ftfAVG[i1].setText(_cf.formatT(characteristics.avg[i1], TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfRMS[i1].setText(_cf.formatT(characteristics.rms2[i1], TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfMIN[i1].setText(_cf.formatT((characteristics.min[i1] == +1e99 ? 0 : characteristics.min[i1]), TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfMAX[i1].setText(_cf.formatT((characteristics.max[i1] == -1e99 ? 0 : characteristics.max[i1]), TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfCREST[i1].setText(_cf.formatT(characteristics.klirr[i1], TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfSHAPE[i1].setText(_cf.formatT(characteristics.shape[i1], TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfTHD[i1].setText(_cf.formatT(characteristics.thd[i1], TechFormat.FORMAT_AUTO));
                                _characteristicsErgPanel.ftfRIPPLE[i1].setText(_cf.formatT(characteristics.ripple[i1], TechFormat.FORMAT_AUTO));
                            }
                            
                            _powerAnalysisPanel.calculate(_worksheet, _panelDialogRange.getStartTimeValue(), _panelDialogRange.getStopTimeValue());
                            _jTextFieldStatus.setForeground(Color.black);
                            _jTextFieldStatus.setText(TxtI.ti_calcok_DialogAvgRms);
                           
                            

                        } catch (Exception e) {
                            _jTextFieldStatus.setForeground(Color.red);
                            _jTextFieldStatus.setText(TxtI.ti_datErr_DialogFourier);
                        } finally {
                             _jbCALC.setEnabled(true);
                        }
                    }
                };
    
}
