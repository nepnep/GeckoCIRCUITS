/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;
import geckocircuitsnew.MainFrame;
//import gecko.GeckoCIRCUITS.allg.DialogInfoFestplattenSpeicherungAktivieren;
//import gecko.GeckoCIRCUITS.allg.SaveView;
import gecko.GeckoCIRCUITS.elements.ElementScope;
//import gecko.GeckoCIRCUITS.control.ReglerOSZI;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.Container;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.Event;
import javax.swing.KeyStroke;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.util.Vector;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.net.URL;

public class Scope extends JFrame implements ActionListener, ComponentListener {

    //------------------------
    private static MainFrame win;  // gibt es nur einmalig, daher static
    //
    protected ElementScope regelBlockOSZI;  // damit man vom SCOPE-Menu aus die Datenspeicherung triggern kann
    protected DataContainer worksheet;  // ZV-Daten fuer das ZV-Diagramm
    protected String datnam;
    protected Container c;
    protected JToolBar jtb1;
    //
    public static int BREITE = 500, HOEHE = 450;
    public int X_SCOPE, Y_SCOPE;  // absolute Scope-Position in Pixel auf dem Screen --> individuelle SCOPE-Positionen moeglich wenn mehrere SCOPE vorhanden
    protected int X_CONN_DIAG, Y_CONN_DIAG;  // absolute Koord. des DialogConnectSignalsGraph-Fensters
    protected int DX_DIAG_CALC, DY_DIAG_CALC;  // relative Koordinaten der Dialogfenster fuer Auswertungen zB. AVG, RMS, ...
    //---------------------------------
    protected JTabbedPane tabber;
    protected DisplayWorksheet sheet;  // Worksheet-Daten in Spreadsheet-Darstellung
    protected String[] header;  // Header der Worksheet-Daten
    protected GraferImplementation grafer;  // Kurven-Grafiken
    protected ScopeSettings scopeSettings;
    protected DialogConnectSignalsGraphs diagCON;  // Dialog fuer Zuordnungen SignalZV - Graph
    //
    protected JCheckBoxMenuItem mItemF1;
    protected JMenu dataMenu, diagMenu, anlyMenu, simMenu;
    protected JMenuItem mItemF3;
    protected JMenuItem mItemSaveImage;
    protected JMenuItem mItemStop, mItemContinue;
    //---------------------------------
    protected ImageIcon[] iconOFF, iconON;
    protected JButton[] jbMaus;
    //---------------------------------
    protected int[] zvCounterRef;  // zeigt auf den letzten gueltigen Wert in Worksheet waehrend der ZV-Simulation, Uebergabe in zvCounterRef[0] als Referenz
    //---------------------------------
    protected DialogScopeCharacteristics diagAvgRms;
    protected DialogFourier diagFourier;
    //---------------------------------

    public int getX_PositionAbsolut() {
        return this.X_SCOPE;
    }

    public int getY_PositionAbsolut() {
        return this.Y_SCOPE;
    }

    public void setXY_PositionAbs_DialogConnect(int x, int y) {
        this.X_CONN_DIAG = x;
        this.Y_CONN_DIAG = y;
    }

    public int getX_PositionAbsolut_DialogConnect() {
        return this.X_CONN_DIAG;
    }

    public int getY_PositionAbsolut_DialogConnect() {
        return this.Y_CONN_DIAG;
    }

    // fuer Zugriff auf ZV-Daten im RAM seitens 'grafer' --> 
    public DataContainer getZVDatenImRAM() {
        return regelBlockOSZI.getZVDatenImRAM();
    }


    /*
    public static void main (String[] args) { new Scope(); }
     */
    public Scope() {
        try {
            this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL, "gecko.gif"))).getImage());
        } catch (Exception e) {
        }
        this.init();
    }

    public Scope(ScopeSettings scopeSettings) {
        try {
            this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL, "gecko.gif"))).getImage());
        } catch (Exception e) {
        }
        this.scopeSettings = scopeSettings;
        this.init();
    }

    public static void setReferenzAufFenster(MainFrame win) {
        Scope.win = win;
    }

    // wenn die Simulation gerade laeuft, werden alle Menus deaktiviert, damit man dort nicht herumpfuschen kann --> 
    public void setScopeMenueEnabled(boolean simulationGestartet) {
        if (simulationGestartet) {
            grafer.setSimulationLaeuftGerade(true);
            mItemContinue.setEnabled(false);
            dataMenu.setEnabled(false);
            diagMenu.setEnabled(false);
            anlyMenu.setEnabled(false);
            // Worksheet-Daten waehrend der Simulation nicht zugaenglich: 
            tabber.setSelectedIndex(0);
            tabber.setEnabledAt(1, false);
        } else {
            grafer.setSimulationLaeuftGerade(false);
            mItemContinue.setEnabled(true);
            dataMenu.setEnabled(true);
            diagMenu.setEnabled(true);
            anlyMenu.setEnabled(true);
            // Worksheet-Daten werden aktualisiert und sind jetzt wieder optional zugreifbar: 
            sheet.ladeWorksheet(header, worksheet, zvCounterRef);
            tabber.setEnabledAt(1, true);
        }
    }

    protected void init() {
        //--------------------
        this.addComponentListener(this);
        this.setTitle(Typ.spTitle + "SCOPE");
        //
        this.X_SCOPE = Typ.X_SCREEN + Typ.DX_SCOPE;
        this.Y_SCOPE = Typ.Y_SCREEN + Typ.DY_SCOPE;
        if (Typ.mode_location == Typ.LOCATION_BY_PROGRAM_A) {
            this.setLocation(this.X_SCOPE, this.Y_SCOPE);
        } else if (Typ.mode_location == Typ.LOCATION_BY_PLATFORM) {
            try {
                win.setLocationByPlatform(true);
            } catch (Exception e) {
            }
        }
        this.X_CONN_DIAG = this.X_SCOPE + Typ.DX_SCOPE_DIALOG;
        this.Y_CONN_DIAG = this.Y_SCOPE + Typ.DY_SCOPE_DIALOG;
        this.DX_DIAG_CALC = 50;
        this.DY_DIAG_CALC = 250;
        //--------------------
        grafer = new GraferImplementation(this);
        grafer._antialiasing = true;  //win.aliasingCONTROL.isSelected();
        sheet = new DisplayWorksheet();
        this.baueGUIMenu();
        this.baueGUItoolbar();
        this.baueGUI();
        
    }

    protected void baueGUI() {
        //--------------------
        c = this.getContentPane();
        c.setLayout(new BorderLayout());
        //--------------------
        tabber = new JTabbedPane();
        tabber.setFont(TxtI.ti_Font_A);
        tabber.addTab(TxtI.ti_grfTab_ScopeX, grafer);
        tabber.addTab(TxtI.ti_datTab_ScopeX, sheet);
        c.add(jtb1, BorderLayout.WEST);
        c.add(tabber, BorderLayout.CENTER);
        this.setSize(this.BREITE, this.HOEHE);
        //--------------------
    }

    protected void baueGUItoolbar() {
        //--------------------        
        try {
            iconON = new ImageIcon[]{
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_off.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_zoomFit2.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_zoomFenster.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconON_getXYschieber.png")), //                new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_drawLine.png")),
                    //                new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconON_drawText.png")),
                    };
            iconOFF = new ImageIcon[]{
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_off.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_zoomFit2.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_zoomFenster.png")),
                        new ImageIcon(new URL(Typ.PFAD_PICS_URL, "iconOFF_getXYschieber.png")), //                new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_drawLine.png")),
                    //                new ImageIcon(new URL(Typ.PFAD_PICS_URL,"iconOFF_drawText.png")),
                    };
        } catch (Exception e) {
            e.printStackTrace();
        }
        //
        jbMaus = new JButton[iconOFF.length];
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jbMaus[i1] = new JButton();
            jbMaus[i1].setFont(TxtI.ti_Font_A);
            jbMaus[i1].setIcon(iconOFF[i1]);
            jbMaus[i1].setActionCommand("mausModus " + i1);
            jbMaus[i1].addActionListener(this);
            //--------------------
            switch (i1) {
                case 0:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt1_ScopeX);
                    break;
                case 1:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt2_ScopeX);
                    break;
                case 2:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt3_ScopeX);
                    break;
                case 3:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt4_ScopeX);
                    break;
                case 4:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt5_ScopeX);
                    break;
                case 5:
                    jbMaus[i1].setToolTipText(TxtI.ti_tt6_ScopeX);
                    break;
                default:
                    System.out.println("Fehler: 98n3gweq5t");
                    break;
            }
            //--------------------
        }
        jbMaus[0].setIcon(iconON[0]);  // default: Maus deaktiviert
        grafer.setMausModus(GraferImplementation.MAUSMODUS_NIX);
        //--------------------
        jtb1 = new JToolBar(TxtI.ti_jtb_ScopeX);
        jtb1.setFont(TxtI.ti_Font_A);
        jtb1.setOrientation(JToolBar.VERTICAL);
        //jtb1.setFloatable(false);
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jtb1.add(jbMaus[i1]);
        }
        //--------------------
    }

    // der User drueck einen Knopf auf dem GUI --> 
    protected void aktualisiereMausModus(ActionEvent ae) {
        //--------------------
        StringTokenizer stk = new StringTokenizer(ae.getActionCommand(), " ");
        stk.nextToken();
        int indexKnopfGedrueckt = new Integer(stk.nextToken()).intValue();
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jbMaus[i1].setIcon(iconOFF[i1]);
        }
        jbMaus[indexKnopfGedrueckt].setIcon(iconON[indexKnopfGedrueckt]);
        //--------------------
        switch (indexKnopfGedrueckt) {
            case 0:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_NIX);
                break;
            case 1:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_ZOOM_AUTOFIT);
                break;
            case 2:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_ZOOM_FENSTER);
                break;
            case 3:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_WERTANZEIGE_SCHIEBER);
                break;
            case 4:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_LINIE);
                break;
            case 5:
                grafer.setMausModus(GraferImplementation.MAUSMODUS_ZEICHNE_TEXT);
                break;
            default:
                System.out.println("Fehler: 98n3gweq5t");
                break;
        }
        //--------------------
    }

    // Rueckmeldung von 'GraferImplementation' aendert bedarfsweise automatisch den Zustand --> 
    public void aktualisiereMausModus(int mausModus) {
        for (int i1 = 0; i1 < jbMaus.length; i1++) {
            jbMaus[i1].setIcon(iconOFF[i1]);
        }
        switch (mausModus) {
            case GraferImplementation.MAUSMODUS_NIX:
                jbMaus[0].setIcon(iconON[0]);
                break;
            case GraferImplementation.MAUSMODUS_ZOOM_AUTOFIT:
                jbMaus[1].setIcon(iconON[1]);
                break;
            case GraferImplementation.MAUSMODUS_ZOOM_FENSTER:
                jbMaus[2].setIcon(iconON[2]);
                break;
            case GraferImplementation.MAUSMODUS_WERTANZEIGE_SCHIEBER:
                jbMaus[3].setIcon(iconON[3]);
                break;
            case GraferImplementation.MAUSMODUS_ZEICHNE_LINIE:
                jbMaus[4].setIcon(iconON[4]);
                break;
            case GraferImplementation.MAUSMODUS_ZEICHNE_TEXT:
                jbMaus[5].setIcon(iconON[5]);
                break;
            default:
                System.out.println("Fehler: eug039wf");
                break;
        }
    }

    protected void baueGUIMenu() {
        //=======================================
        dataMenu = new JMenu(TxtI.ti_datTab_ScopeX);
        dataMenu.setFont(TxtI.ti_Font_A);
        mItemF1 = new JCheckBoxMenuItem(TxtI.ti_jcbSav_ScopeX);
        mItemF1.addActionListener(this);
        mItemF1.setActionCommand("activateDataSaving");
        mItemF1.setSelected(false);
        mItemF3 = new JMenuItem(TxtI.ti_jmiWri_ScopeX);
        mItemF3.addActionListener(this);
        mItemF3.setActionCommand("Write Data To File");
        mItemF3.setMnemonic(KeyEvent.VK_S);
        mItemF3.setEnabled(false);
        JMenuItem mItemF5 = new JMenuItem(TxtI.ti_Exit);
        mItemF5.addActionListener(this);
        mItemF5.setActionCommand("Exit");
        mItemF5.setMnemonic(KeyEvent.VK_X);
        mItemSaveImage = new JMenuItem("Save View as Image");
        mItemSaveImage.addActionListener(this);
        mItemSaveImage.setActionCommand("Save View as Image");
        mItemSaveImage.setMnemonic(KeyEvent.VK_I);
        mItemSaveImage.setEnabled(true);
        mItemSaveImage.setFont(TxtI.ti_Font_A);
        if (Typ.IS_APPLET && !Typ.IS_BRANDED) {
            mItemSaveImage.setEnabled(false);
        }
        mItemF1.setFont(TxtI.ti_Font_A);
        mItemF3.setFont(TxtI.ti_Font_A);
        mItemF5.setFont(TxtI.ti_Font_A);
        dataMenu.add(mItemF1);
        dataMenu.add(mItemF3);
        mItemF3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
//        dataMenu.add(mItemSaveImage);    mItemSaveImage.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, Event.CTRL_MASK));
        dataMenu.add(mItemF5);
        mItemF5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK));
        if (Typ.IS_APPLET) {
            mItemF1.setEnabled(false);
        }
        dataMenu.add(mItemSaveImage);

        //=======================================
        diagMenu = new JMenu(TxtI.ti_jmGrf_ScopeX);
        diagMenu.setFont(TxtI.ti_Font_A);
        JMenuItem mItemD3 = new JMenuItem(TxtI.ti_jmiMtx_ScopeX);
        mItemD3.addActionListener(this);
        mItemD3.setActionCommand("Signal - Graph");
        mItemD3.setMnemonic(KeyEvent.VK_D);
        mItemD3.setFont(TxtI.ti_Font_A);
        diagMenu.add(mItemD3);
        mItemD3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, Event.CTRL_MASK));
        //=======================================
        simMenu = new JMenu(TxtI.ti_Simulation);
        simMenu.setFont(TxtI.ti_Font_A);
        mItemStop = new JMenuItem(TxtI.ti_Pause);
        mItemStop.addActionListener(this);
        mItemStop.setActionCommand("Pause");
        mItemStop.setMnemonic(KeyEvent.VK_F3);
        mItemContinue = new JMenuItem(TxtI.ti_Continue);
        mItemContinue.addActionListener(this);
        mItemContinue.setActionCommand("Continue");
        mItemContinue.setMnemonic(KeyEvent.VK_F4);
        mItemStop.setFont(TxtI.ti_Font_A);
        mItemContinue.setFont(TxtI.ti_Font_A);
        simMenu.add(mItemStop);
        mItemStop.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
        simMenu.add(mItemContinue);
        mItemContinue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
        if (Typ.operatingmode == Typ.OPERATINGMODE_SIMULINK) {
            simMenu.setEnabled(false);
        }
        //=======================================
        anlyMenu = new JMenu(TxtI.ti_jmAna_ScopeX);
        anlyMenu.setFont(TxtI.ti_Font_A);
        JMenuItem mItemA1 = new JMenuItem(TxtI.ti_jmiChar_ScopeX);
        mItemA1.addActionListener(this);
        mItemA1.setActionCommand("Characteristics");
        JMenuItem mItemA3 = new JMenuItem(TxtI.ti_jmiFour_ScopeX);
        mItemA3.addActionListener(this);
        mItemA3.setActionCommand("Fourier");
        mItemA1.setFont(TxtI.ti_Font_A);
        mItemA3.setFont(TxtI.ti_Font_A);
        anlyMenu.add(mItemA1);
        anlyMenu.add(mItemA3);
        //=======================================
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(dataMenu);
        menuBar.add(diagMenu);
        menuBar.add(anlyMenu);
        menuBar.add(simMenu);
        //
        this.setJMenuBar(menuBar);
        //=======================================
    }

    public void actionPerformed(ActionEvent ae) {
        //=========================
        // FileFilter zum Datenladen ins Scope -->
        FileFilter filter = new FileFilter() {

            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                if ((f.getName().endsWith(".dat")) || (f.getName().endsWith(".txt"))) {
                    return true;
                } else {
                    return false;
                }
            }

            public String getDescription() {
                return new String("Data File, Space-Spr. (*.dat, *.txt)");
            }
        };
        //=========================
        if (ae.getActionCommand().startsWith("mausModus")) {
            this.aktualisiereMausModus(ae);
        } else if (ae.getActionCommand().equals("Signals")) {
            System.out.println(ae.getActionCommand());
        } else if (ae.getActionCommand().equals("Load External Data")) {
            //------------------------------
            grafer.usesExternalData = true;
            //-----------
            JFileChooser dialog = new JFileChooser();
            dialog.setMultiSelectionEnabled(false);
            dialog.addChoosableFileFilter(filter);
            try {
                if (datnam.equals(Typ.DATNAM_NOT_DEFINED)) {
                    datnam = Typ.PFAD_JAR_HOME;
                }
                dialog.setCurrentDirectory(new File(datnam));
            } catch (Exception e) {
                dialog.setCurrentDirectory(new File(Typ.PFAD_JAR_HOME));
            }
            int result = dialog.showOpenDialog(this);
            if (result == JFileChooser.CANCEL_OPTION) {
                return;
            }
            datnam = dialog.getSelectedFile().getAbsolutePath();
            worksheet = schreibeWorksheet(datnam);
            grafer.setzeKurvenUndWorksheetDaten(header, worksheet);
            sheet.ladeWorksheet(header, worksheet);
            //------------------------------
        } else if (ae.getActionCommand().equals("activateDataSaving")) {
            //------------------------------
            if (mItemF1.isSelected()) {
                mItemF3.setEnabled(true);
                this.activateDataSaving();
            } else {
                mItemF3.setEnabled(false);
                this.deactivateDataSaving();
            }
            //------------------------------
        } else if (ae.getActionCommand().equals("Save View as Image")) { // Andy.
            //SaveView.saveViewAsImage(grafer);
        } else if (ae.getActionCommand().equals("Write Data To File")) {
            saveDataToDisk(filter);
            //------------------------------
            /*
            JFileChooser dialog= new JFileChooser();
            dialog.addChoosableFileFilter(filter);
            try {
            if (datnam.equals(Typ.DATNAM_NOT_DEFINED)) datnam= Typ.PFAD_JAR_HOME;
            dialog.setCurrentDirectory(new File(datnam));
            } catch (Exception e) {
            dialog.setCurrentDirectory(new File(Typ.PFAD_JAR_HOME));
            }
            int result= dialog.showSaveDialog(this);
            if (result==JFileChooser.CANCEL_OPTION) { return; }
            datnam= dialog.getSelectedFile().getAbsolutePath();
            if (!datnam.endsWith(".dat"))  datnam += (".dat");
            //
            regelBlockOSZI.speichereZVDaten(datnam);
             */
            //------------------------------
        } else if (ae.getActionCommand().equals("Save Diagram Settings")) {
            System.out.println(ae.getActionCommand());
        } else if (ae.getActionCommand().equals("Print Picture")) {
            System.out.println(ae.getActionCommand());
        } else if (ae.getActionCommand().equals("Exit")) {
            this.dispose();
        } else if (ae.getActionCommand().equals("Signal - Graph")) {
            if (diagCON != null) {
                diagCON.dispose();
            }
            diagCON = new DialogConnectSignalsGraphs(this, grafer, true);
        } else if (ae.getActionCommand().equals("Characteristics")) {
            DataContainer xx = regelBlockOSZI.getZVDatenImRAM();
            if (xx == null) {
                return;
            }
            
            diagAvgRms = new DialogScopeCharacteristics(xx, header, scopeSettings, grafer.getSlider1Value(), grafer.getSlider2Value());
            if (Typ.mode_location == Typ.LOCATION_BY_PROGRAM_A) {
                diagAvgRms.setLocation(this.X_SCOPE + this.DX_DIAG_CALC, this.Y_SCOPE + this.DY_DIAG_CALC);
            } else if (Typ.mode_location == Typ.LOCATION_BY_PLATFORM) {
                diagAvgRms.setLocationByPlatform(true);
            }
            diagAvgRms.setVisible(true);
        } else if (ae.getActionCommand().equals("Fourier")) {
            // Fourier erhaelt prinzipiell die hochaufloesenden Daten aus dem RAM-Speicher --> 
            DataContainer xx = regelBlockOSZI.getZVDatenImRAM();
            if (xx == null) {
                return;
            }
            diagFourier = new DialogFourier(xx, header);
            if (Typ.mode_location == Typ.LOCATION_BY_PROGRAM_A) {
                diagFourier.setLocation(this.X_SCOPE + this.DX_DIAG_CALC, this.Y_SCOPE + this.DY_DIAG_CALC);
            } else if (Typ.mode_location == Typ.LOCATION_BY_PLATFORM) {
                diagFourier.setLocationByPlatform(true);
            }
            diagFourier.setVisible(true);
            //------------------------------
        } else if (ae.getActionCommand().equals("Pause")) {
            win.stopCalculation();
        } else if (ae.getActionCommand().equals("Continue")) {
            try {
                win.continueCalculation();
            } catch (RuntimeException re) {
                System.out.println("we0if00ww");
            }
            //------------------------------
        }
    }

    public void activateDataSaving() {
        regelBlockOSZI.aktiviereZVDatenSpeicherung(true);
        //DialogInfoFestplattenSpeicherungAktivieren info = new DialogInfoFestplattenSpeicherungAktivieren();
    }

    public void deactivateDataSaving() {
        regelBlockOSZI.aktiviereZVDatenSpeicherung(false);
    }

    public boolean saveZVData(String fileName) {
        return regelBlockOSZI.speichereZVDaten(fileName);
    }

    public void saveDataToDisk(FileFilter filter) {
        //------------------------------
        JFileChooser dialog = new JFileChooser();
        dialog.addChoosableFileFilter(filter);
        try {
            if (datnam.equals(Typ.DATNAM_NOT_DEFINED)) {
                datnam = Typ.PFAD_JAR_HOME;
            }
            dialog.setCurrentDirectory(new File(datnam));
        } catch (Exception e) {
            dialog.setCurrentDirectory(new File(Typ.PFAD_JAR_HOME));
        }
        int result = dialog.showSaveDialog(this);
        if (result == JFileChooser.CANCEL_OPTION) {
            return;
        }
        datnam = dialog.getSelectedFile().getAbsolutePath();
        if (!datnam.endsWith(".dat")) {
            datnam += (".dat");
        }
        //
        regelBlockOSZI.speichereZVDaten(datnam);
        //------------------------------
    }

    // ================================================================================================
    // ===============   fuer die Anbindung / Integration mit dem Schaltungssimulator =================
    // ================================================================================================
    // Daten vom Simulator werden direkt weitergegeben:
    public void setzeSignalDaten(DataContainer signal, String[] signalNamen, int[] zvCounterRef) {
        //--------------------
        grafer.usesExternalData = false;
        worksheet = signal;
        header = signalNamen;
        this.zvCounterRef = zvCounterRef;
        grafer.setzeKurvenUndWorksheetDaten(header, worksheet);
        
        sheet.ladeWorksheet(header, worksheet);
        
        //--------------------
    }

    public void setReferenzAufRegelBlock(ElementScope regelBlockOSZI) {
        this.regelBlockOSZI = regelBlockOSZI;
    }

    public void setZVCounter(int zvCounter) {
        grafer.setZVCounter(zvCounter);
    }  // gibt an, wieviel der Punkte im Worksheet als Kkurve dargestellt werden sollen

    public ScopeSettings getScopeSettings() {
        return scopeSettings;
    }

    public void saveScopeSettings() {
        scopeSettings.saveSettings(grafer);
    }

    // das Bild wird nicht bei jedem Zeitschritt neu gezeichnet, sondern in definierten Zeitabstaenden --> viel effizienter
    public void updateScope(double t1SCOPE, double t2SCOPE) {
        //System.out.println("update scope called");
        if (grafer.usesExternalData) {
            return;  // wird nur verwendet bei ZV-Simulation
        }        //--------------------
        // (0) zvCounter zeigt an, wieviele ZV-Daten in Worksheet gueltig sind / Uebergabe als Array zwecks Referenzierung -->
        grafer.setZVCounter(zvCounterRef[0]);
        //--------------------
        // (1) Kurven muessen aktualisiert werden -->
        grafer.akualisiereKurvenUndWorksheetDaten(t1SCOPE, t2SCOPE);
    }

    public void updateScopeHeader() {
        sheet.aktualisiereWorksheetHeader(header);
    }

    // durch Aufrufen vom Konstruktor in 'DialogConnectSignalsGraphs' werden die noetigen Parameter gesetzt
    // (konkret in 'bestaetigeMatrix'), sodass ein initialer Graph gaenzlich ohne ZV-Simulationsdaten gezeichnet werden kann
    // (eigentlich ein bischen Pfusch ...)
    public void initBildVomGraphOhneZVDaten() {
        if (diagCON != null) {
            diagCON.dispose();
        }
        diagCON = new DialogConnectSignalsGraphs(this, grafer, false);
        diagCON.dispose();
    }

    // wenn die Terminalzahl beim Scope veraendert wird, also die Signal-Anzahl fuer die Scope-Anzeige sich aendert,
    // dann muss das entsprechend in der ConnectionMatrix beruecksichtigt werden -->
    public void set_crvAchsenTyp_ConnectionMatrix(int im1, int im2, int typ) {
        grafer.set_crvAchsenTyp(im1, im2, typ);
        grafer.berechneNotwendigeHoeheSIGNALGraph();
        grafer.setzeAchsen();
        grafer.setzeKurven();
    }

    // ================================================================================================
    // ================================================================================================
    // ================================================================================================
    // GeckoOPTIMIZER --> 
    // 
    public DataContainer getZVData() {
        return worksheet;
    }

    protected DataContainer schreibeWorksheet(String datnam) {
        //--------------------
        try {
            Vector dat = new Vector();
            BufferedReader in = new BufferedReader(new FileReader(new File(datnam)));
            StringTokenizer stk = new StringTokenizer(in.readLine(), " ");  // Header mit Space-Trennzeichen
            header = new String[stk.countTokens()];
            for (int i1 = 0; i1 < header.length; i1++) {
                header[i1] = stk.nextToken();
            }
            //
            String zeile = "";  // alle weiteren Zeilen enthalten double-Werte
            while ((zeile = in.readLine()) != null) {
                if (zeile.startsWith("1") || zeile.startsWith("2") || zeile.startsWith("3") || zeile.startsWith("4")
                        || zeile.startsWith("5") || zeile.startsWith("6") || zeile.startsWith("7") || zeile.startsWith("8") || zeile.startsWith("9")) {
                    dat.addElement(zeile);
                }
            }
            int lg = dat.size();
            stk = new StringTokenizer(dat.elementAt(0).toString(), " ");
            int anzKolonnen = stk.countTokens();
            DataContainer daten = new DataContainerSimple(anzKolonnen, lg);
            for (int i1 = 0; i1 < lg; i1++) {
                zeile = dat.elementAt(i1).toString();
                stk = new StringTokenizer(zeile, " ");
                for (int i2 = 0; i2 < anzKolonnen; i2++) {
                    try {
                        daten.setValue((new Double(stk.nextToken())).doubleValue(), i2, i1); } catch (Exception e) {
                        daten.setValue(-1, i2, i1);
                    }
                }
            }
            in.close();
            //--------------------
            return daten;
        } catch (Exception e) {
            System.out.println(e + "   ____");
        }
        return null;
    }

    //------------------------------------------------
    public void componentResized(ComponentEvent ce) {
        BREITE = ce.getComponent().getWidth();
        HOEHE = ce.getComponent().getHeight();
        try {
            grafer.aktualisiereAchsenNachResizing();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        grafer.repaint();
    }

    public void componentMoved(ComponentEvent ce) {
        // Aktualisierung der SchematicEntry-Koord. am Screen (fuer Dialog-Positionierung)
        this.X_SCOPE = ce.getComponent().getX();
        this.Y_SCOPE = ce.getComponent().getY();
    }

    public void componentShown(ComponentEvent ce) {
    }

    public void componentHidden(ComponentEvent ce) {
    }
}
