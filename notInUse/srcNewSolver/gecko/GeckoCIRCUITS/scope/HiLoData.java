/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.scope;

/**
 *
 * @author andy
 */
public class HiLoData {
    public float yLo = 1E30f;
    public float yHi = -1E30f;

    void insertCompare(float y) {
        yLo = Math.min(yLo, y);
        yHi = Math.max(yHi, y);

    }

    void insertCompare(HiLoData data) {
        if(data.yLo < 1E30f)
            yLo = Math.min(yLo, data.yLo);
        if(data.yHi > -1E30)
            yHi = Math.max(yHi, data.yHi);

        assert Math.abs(yHi) < 1e20f : data.yHi + " " + yHi;
        assert Math.abs(yLo) < 1e20f  : data.yLo + " " + yLo;

        
    }
    
    
}
