/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.TechFormat;
import gecko.GeckoCIRCUITS.allg.TxtI;
import gecko.GeckoCIRCUITS.allg.Typ;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author andy
 */
public class PowerAnalysisPanel extends JPanel {

    private JComboBox[] comboU = new JComboBox[3];
    private JComboBox[] comboI = new JComboBox[3];
    private FormatJTextField[][] pqTextFields;  // Textfelder fuer Leistungswerte A und B
    private final TechFormat cf = new TechFormat();
    private GridBagConstraints gbc = new GridBagConstraints();

    public PowerAnalysisPanel(String[] header, final ScopeSettings _scopeSettings) {

        //===========================================================
        JPanel[] pUIa = new JPanel[3];
        
        for(int i = 0; i < pUIa.length; i++) {
            pUIa[i] = new JPanel();
            pUIa[i].setLayout(new GridBagLayout());
            pUIa[i].setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x2_DialogAvgRms, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        }
        
        //
        String[] signalListe = new String[header.length];
        signalListe[0] = TxtI.ti_deact_DialogAvgRms;
        for (int i1 = 1; i1 < header.length; i1++) {
            signalListe[i1] = header[i1];
        }

        
        for (int i = 0; i < comboU.length; i++) {
            final int index = i;
            comboU[i] = new JComboBox(signalListe);
            comboU[i].setFont(TxtI.ti_Font_B);

            if (_scopeSettings.powerAnalysisVoltageIndex[i] >= 0) {
                comboU[i].setSelectedIndex(_scopeSettings.powerAnalysisVoltageIndex[i]);
            }

            comboU[i].addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae) {
                    _scopeSettings.powerAnalysisVoltageIndex[index] = comboU[index].getSelectedIndex();
                }
            });

            comboI[i] = new JComboBox(signalListe);
            comboI[i].setFont(TxtI.ti_Font_B);

            if (_scopeSettings.powerAnalysisCurrentIndex[i] >= 0) {
                comboI[i].setSelectedIndex(_scopeSettings.powerAnalysisCurrentIndex[i]);
            }

            comboI[i].addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae) {
                    _scopeSettings.powerAnalysisCurrentIndex[index] = comboI[index].getSelectedIndex();
                }
            });

        }

        setPQTextFields();


        gbc.gridx = 0;
        gbc.gridy = 0;
        JLabel[][] powerAnalysisLabels = new JLabel[8][3];
        for (int i = 0; i < powerAnalysisLabels[0].length; i++) {
            powerAnalysisLabels[0][i] = new JLabel("u(t) = ");
            powerAnalysisLabels[1][i] = new JLabel("   P = ");
            powerAnalysisLabels[2][i] = new JLabel("   D = ");
            powerAnalysisLabels[3][i] = new JLabel("   lambda = ");
            powerAnalysisLabels[4][i] = new JLabel("i(t) = ");
            powerAnalysisLabels[5][i] = new JLabel("   Q = ");
            powerAnalysisLabels[6][i] = new JLabel("   S = ");
            powerAnalysisLabels[7][i] = new JLabel("   cos(p1) = ");

            for (int j = 0; j < powerAnalysisLabels.length; j++) {
                JLabel tmp = powerAnalysisLabels[j][i];
                tmp.setFont(Typ.LAB_FONT_DIALOG_1);
                tmp.setForeground(Typ.LAB_COLOR_DIALOG_1);

            }

        }

        /*
         * todo: refactor this mess with some more intelligent indexing!
         */
        for (int ii = 0; ii < 3; ii++) {
            gbc.gridx = 0;
            gbc.gridy = 0;
            pUIa[ii].add(powerAnalysisLabels[0][ii], gbc);
            gbc.gridx = 1;
            gbc.gridy = 0;
            pUIa[ii].add(comboU[ii], gbc);
            gbc.gridx = 2;
            gbc.gridy = 0;
            pUIa[ii].add(powerAnalysisLabels[1][ii], gbc);
            gbc.gridx = 3;
            gbc.gridy = 0;
            pUIa[ii].add(pqTextFields[ii][0], gbc);
            gbc.gridx = 4;
            gbc.gridy = 0;
            pUIa[ii].add(powerAnalysisLabels[2][ii], gbc);
            gbc.gridx = 5;
            gbc.gridy = 0;
            pUIa[ii].add(pqTextFields[ii][2], gbc);
            gbc.gridx = 6;
            gbc.gridy = 0;
            pUIa[ii].add(powerAnalysisLabels[3][ii], gbc);
            gbc.gridx = 7;
            gbc.gridy = 0;
            pUIa[ii].add(pqTextFields[ii][4], gbc);
            gbc.gridx = 0;
            gbc.gridy = 1;
            pUIa[ii].add(powerAnalysisLabels[4][ii], gbc);
            gbc.gridx = 1;
            gbc.gridy = 1;
            pUIa[ii].add(comboI[ii], gbc);
            gbc.gridx = 2;
            gbc.gridy = 1;
            pUIa[ii].add(powerAnalysisLabels[5][ii], gbc);
            gbc.gridx = 3;
            gbc.gridy = 1;
            pUIa[ii].add(pqTextFields[ii][1], gbc);
            gbc.gridx = 4;
            gbc.gridy = 1;
            pUIa[ii].add(powerAnalysisLabels[6][ii], gbc);
            gbc.gridx = 5;
            gbc.gridy = 1;
            pUIa[ii].add(pqTextFields[ii][3], gbc);
            gbc.gridx = 6;
            gbc.gridy = 1;
            pUIa[ii].add(powerAnalysisLabels[7][ii], gbc);
            gbc.gridx = 7;
            gbc.gridy = 1;
            pUIa[ii].add(pqTextFields[ii][5], gbc);
        }


        setLayout(new GridLayout(3, 1));
        for(int i = 0; i < 3; i++) {
            add(pUIa[i]);
        }

    }

    void calculate(DataContainer _worksheet, double startTimeValue, double stopTimeValue) {
        PowerCalculatorSelectionIndex pcsi = new PowerCalculatorSelectionIndex(startTimeValue, stopTimeValue);
        pcsi.calculate(_worksheet);
    }

    private void setPQTextFields() {
        pqTextFields = new FormatJTextField[3][6];
        for (int i = 0; i < pqTextFields.length; i++) {
            for (int j = 0; j < pqTextFields[0].length; j++) {
                FormatJTextField tf = new FormatJTextField();
                pqTextFields[i][j] = tf;
                tf.setColumns(8);
                tf.setEditable(false);
                tf.setFont(new java.awt.Font("Arial", 0, 11));
            }
        }
    }

    public class PowerCalculatorSelectionIndex {

        /**
         * points to the right analysis selection (A, B, C, ...)
         */
        final ArrayList<Integer> selectedPowerAnalysis = new ArrayList<Integer>();
        final ArrayList<Integer> selectedCurrentIndices = new ArrayList<Integer>();
        final ArrayList<Integer> selectedVoltageIndices = new ArrayList<Integer>();
        public final double startTime;
        public final double stopTime;

        public PowerCalculatorSelectionIndex(double rng1, double rng2) {

            for (int i = 0; i < comboU.length; i++) {
                if (comboU[i].getSelectedIndex() > 0 && comboI[i].getSelectedIndex() > 0) {
                    selectedPowerAnalysis.add(i);
                    selectedCurrentIndices.add(comboI[i].getSelectedIndex());
                    selectedVoltageIndices.add(comboU[i].getSelectedIndex());
                }
            }

            startTime = rng1;
            stopTime = rng2;
        }

        public void calculate(DataContainer worksheet) {
            try {
                PowerCalculator ergP = PowerCalculator.CalculatorFabric(worksheet, this);
                if (ergP._numberOfPowerAnalyses != 0) {  // dh. keine Leistungsberechnung durchzufuehren weil deaktiviert

                    for (int i = 0; i < pqTextFields.length; i++) {
                        for (int j = 0; j < pqTextFields[0].length; j++) {
                            pqTextFields[i][j].setText("    -");
                        }
                    }

                    int counter = 0;
                    for (int i : selectedPowerAnalysis) {
                        pqTextFields[i][0].setText(cf.formatT(ergP.power_P[counter], TechFormat.FORMAT_AUTO));
                        pqTextFields[i][1].setText(cf.formatT(ergP.power_Q[counter], TechFormat.FORMAT_AUTO));
                        pqTextFields[i][2].setText(cf.formatT(ergP.power_D[counter], TechFormat.FORMAT_AUTO));
                        pqTextFields[i][3].setText(cf.formatT(ergP.power_S[counter], TechFormat.FORMAT_AUTO));
                        pqTextFields[i][4].setText(cf.formatT(ergP.lambda[counter], TechFormat.FORMAT_AUTO));
                        pqTextFields[i][5].setText(cf.formatT(ergP.cosPhi[counter], TechFormat.FORMAT_AUTO));
                        counter++;
                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
