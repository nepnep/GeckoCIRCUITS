/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.scope;

/**
 *
 * @author andy
 */
public class DataContainerSimple implements DataContainer {
    private double[][] _data;
    private int _maximumTimeIndex;
    private double maximumResolution = -1e20;
    private double minimumResolution = 1e20;

    public DataContainerSimple(int rows, int columns) {
        try {
        _data = new double[rows][columns];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public double getValue(int row, int column) {
        return _data[row][column];
    }

    public void setValue(double value, int row, int column) {
        assert row <_data.length : "size: " + _data.length + " " + row;
        //assert column < _data[row].length : " size: " + _data[row].length + " " + column;
        
        if(column >= _data[row].length || column < 0) {
            return;
        }
        
        if(row == 0) {
            _maximumTimeIndex = Math.max(_maximumTimeIndex, column);
            if(column > 1) {
                maximumResolution = Math.max(maximumResolution, value - _data[0][column-1]);
                minimumResolution = Math.min(minimumResolution, value - _data[0][column-1]);
            }
        }

        if(column < _data[row].length)
        _data[row][column] = value;
    }

    public int getRowLength() {
        return _data.length;
    }

    public int getColumnLength() {
        return _data[0].length;
    }

    public void setColumn(double[] data, int index) {
        _data[index] = data;
    }

    public double[] getColumn(int index) {
        return _data[index];
    }

    public double getTimeIntervalResolution() {
        return maximumResolution;
    }

    public HiLoData getHiLoValue(int row, int columnStart, int columnStop) {
        assert columnStart < columnStop;  
        HiLoData hiLoData = new HiLoData();
        
        
        for(int index = columnStart; index <= columnStop; index++) {
            hiLoData.insertCompare( (float) _data[row][index]);
        }
                
        return hiLoData;
    }

    public double getTimeValue(int column) {
        return _data[0][column];
    }
    
    public int getMaximumTimeIndex() {
        return _maximumTimeIndex;
    }

}
