/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

/**
 * Calculates the characteristic values of a datacontainer, e.g. average, rms, klirr, ...
 * Implemented as a static fabric, so that values can only be obtained via the calculateFabric
 * @author andy
 */
public class CharacteristicsCalculator {

    final double[] avg;
    final double[] rms2;
    final double[] min;
    final double[] max;
    final double[] shape;
    final double[] thd;
    final double[] klirr;
    final double[] ripple;
    final double[] gleichrichtwert;
    final double[] gsA1;  // Grundschwingungsanteil A1
    final double[] gsB1;  // Grundschwingungsanteil B1

    private CharacteristicsCalculator(DataContainer worksheet, double rng1, double rng2) {
        final int numberOfDataRows = worksheet.getRowLength()-1;
        
        avg = new double[numberOfDataRows];
        rms2 = new double[numberOfDataRows];
        min = new double[numberOfDataRows];
        max = new double[numberOfDataRows];
        shape = new double[numberOfDataRows];
        thd = new double[numberOfDataRows];
        klirr = new double[numberOfDataRows];
        ripple = new double[numberOfDataRows];
        gleichrichtwert = new double[numberOfDataRows];
        gsA1 = new double[numberOfDataRows];  // Grundschwingungsanteil A1
        gsB1 = new double[numberOfDataRows];  // Grundschwingungsanteil B1

        for(int i = 0; i < max.length; i++) {
            max[i] = -Double.MAX_VALUE;
            min[i] = Double.MAX_VALUE;
        }
        
        calculate(worksheet, rng1, rng2);
        
    }

    private void calculate(DataContainer worksheet, double rng1, double rng2) {
        
        assert avg != null;
        
        final int dataRowLength = worksheet.getRowLength()-1;
        final double dT = rng2 - rng1;
        //-------------------
        // Startpunkt finden:
        int startIndex = 0;
        while (worksheet.getValue(0, startIndex) <= rng1) {
            startIndex++;
        }
        //-------------------
        for(int i1 = startIndex; i1 < worksheet.getColumnLength()
                && worksheet.getValue(0, i1 + 1) > worksheet.getValue(0, i1)
                && worksheet.getValue(0, i1) <= rng2; i1++) {
            double dt = worksheet.getValue(0, i1 + 1) - worksheet.getValue(0, i1);
            double tAktuell = worksheet.getValue(0, i1);
            double omega_T = 2 * Math.PI / dT * tAktuell;
            double sinOmegaT = Math.sin(omega_T);
            double cosOmegaT = Math.cos(omega_T);
            for (int i2 = 0; i2 < dataRowLength; i2++) {
                double wert = worksheet.getValue(i2 + 1, i1);
                avg[i2] += (wert * dt);
                rms2[i2] += (wert * wert * dt);
                min[i2] = Math.min(min[i2], wert);
                max[i2] = Math.max(max[i2], wert);
                gleichrichtwert[i2] += Math.abs(wert);
                gsA1[i2] += wert * cosOmegaT * dt;
                gsB1[i2] += wert * sinOmegaT * dt;
            }
        }
        //-------------------
        // Auswertung:
        for (int i2 = 0; i2 < dataRowLength; i2++) {
            avg[i2] /= dT;
            rms2[i2] /= dT;
            rms2[i2] = Math.sqrt(rms2[i2]);  // ab hier ist rms2 nicht mehr der quadratische Wert!!
            gleichrichtwert[i2] /= dT;
            shape[i2] = rms2[i2] / gleichrichtwert[i2];
            gsA1[i2] /= (0.5 * dT);
            gsB1[i2] /= (0.5 * dT);
            double sqrtHigherHarmonics = Math.sqrt(rms2[i2] * rms2[i2]
                    - 0.5 * (gsA1[i2] * gsA1[i2] + gsB1[i2] * gsB1[i2]) - (avg[i2] * avg[i2]));
            thd[i2] =  sqrtHigherHarmonics / Math.sqrt(0.5 * (gsA1[i2] * gsA1[i2] + gsB1[i2] * gsB1[i2]));
            klirr[i2] = sqrtHigherHarmonics / rms2[i2];
            ripple[i2] = Math.sqrt(rms2[i2] * rms2[i2] - avg[i2] * avg[i2]) / avg[i2];
        }        
    }
    
    /**
     * 
     * @param worksheet Container which holds the data. Careful, the first row of the container must contain the time values!
     * @param rng1 time value to start the calculation
     * @param rng2 time value where to stop the calculation
     * @return data structure which contains the results
     * @throws Exception 
     */
    public static CharacteristicsCalculator calculateFabric(DataContainer worksheet, double rng1, double rng2) throws Exception {
        CharacteristicsCalculator res = new CharacteristicsCalculator(worksheet, rng1, rng2);        
        return res;
    }
}
