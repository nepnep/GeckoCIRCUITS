/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JDialog; 
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Container;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import java.net.URL;




public class DialogDigitalCurveProperties extends JDialog implements WindowListener {


    //----------------------------
    private DialogConnectSignalsGraphs connectionMatrixFenster;
    private GraferImplementation grafer;  // callback fuer Properties-Zugriff
    private Container c;
    private JButton jbOK, jbCa, jbAp;
    private GridBagConstraints gbc= new GridBagConstraints();
    //
    private int im1, im2;  // Indizes (im1,im2) der Zuordnungsmatrix
    //----------------------------
    // Lokal zu speichernde Groessen --> Weiterleitung duch 'Apply'-Button:
    private String signalNamen;
    //----------------------------
    private FormatJTextField jtfNameKURV, jtfNameGRF;  // wird zugewiesen --> nicht editierbar!
    private JComboBox jcbAchsenTyp;  // X oder Y oder Y2 ??
    private JComboBox jcbLineStyle, jcbLineColor;
    //----------------------------
    // lokal gespeicherte Parameter dieses Dialog-Fensters, die in einem Stueck bei 'Apply' oder 'OK' uebergeben werden -->
    private String _jtfNameKURV, _jtfNameGRF;
    private int _jcbAchsenTyp;
    private int _jcbLineStyle, _jcbLineColor;
    //----------------------------
    // Digital-Kurven koennen optional gefuellt werden --> 
    private boolean _jcbFillDigitalCurves; 
    private JCheckBox jcbFillDigitalCurves;
    private int _jcbFillingDigitalColor; 
    private JComboBox jcbFillingDigitalColor;
    //----------------------------



    public DialogDigitalCurveProperties (DialogConnectSignalsGraphs conF, GraferImplementation grafer, int im1, int im2) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        this.connectionMatrixFenster= conF;
        this.grafer= grafer;
        this.im1= im1-1;
        this.im2= im2-1;
        //--------------------
        // this.setLocation(230,230);
        this.setTitle(Typ.spTitle+"Digital Curve Properties");
        this.addWindowListener(this);
        c= this.getContentPane();
        c.setLayout(new BorderLayout());
        //--------------------
        c.removeAll();
        this.initPropertiesZVmodus();
        this.baueGUI();
        this.pack();
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(grafer.getScope().getX_PositionAbsolut_DialogConnect()+Typ.DX_SCOPE_DLG2, grafer.getScope().getY_PositionAbsolut_DialogConnect()+Typ.DY_SCOPE_DLG2);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setResizable(false);
        this.setVisible(true);
        //--------------------
    }



    private void initPropertiesZVmodus () {
        //----------------------------
        // Initialisierung derlokal gespeicherte Parameter dieses Dialog-Fensters
        //
        _jtfNameGRF= grafer.nameDiagram[im1];
        _jtfNameKURV= grafer.signalNamen[im2];
        //
        _jcbAchsenTyp= grafer.get_crvAchsenTyp(im1,im2);
//        _jcbAchsenTyp= grafer.crvAchsenTyp[im1][im2];
        _jcbLineStyle= grafer.crvLineStyle[im1][im2];
        _jcbLineColor= grafer.crvLineColor[im1][im2];
        //
        _jcbFillDigitalCurves= grafer.crvFillDigitalCurves[im1][im2];
        _jcbFillingDigitalColor= grafer.crvFillingDigitalColor[im1][im2]; 
        //----------------------------
    }



    private void baueGUI () {
        //================================================
        c.add(this.getKurvenTab(), BorderLayout.CENTER);
        //
        JPanel jpOK= new JPanel();
        jbOK= new JButton(TxtI.ti_OK);
        jbOK.setFont(TxtI.ti_Font_A); 
        jbCa= new JButton(TxtI.ti_Cancel);
        jbCa.setFont(TxtI.ti_Font_A); 
        jbAp= new JButton(TxtI.ti_apply_DialogCheckSimulation);
        jbAp.setFont(TxtI.ti_Font_A); 
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { bestaetige();  dispose(); }
        });
        jbCa.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { dispose(); }
        });
        jbAp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { bestaetige(); }
        });
        jpOK.add(jbOK);
        jpOK.add(jbCa);
        jpOK.add(jbAp);
        c.add(jpOK, BorderLayout.SOUTH);
        //================================================
    }



    

    private JPanel getKurvenTab () {
        //==========================================================================================
        // (0) Allgemeines --> Achsentyp (X/Y/Y2) ? -->
        JPanel jpALLG= new JPanel();
        jpALLG.setLayout(new GridBagLayout());
        jpALLG.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=0;   gbc.gridy=0;   gbc.fill=gbc.BOTH;
        JLabel jlu0= new JLabel(TxtI.ti_grfTab_ScopeX+" :  "); 
        jlu0.setFont(TxtI.ti_Font_B); 
        jpALLG.add(jlu0, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jtfNameGRF= new FormatJTextField();
        jtfNameGRF.setText(_jtfNameGRF);
        jtfNameGRF.setEditable(false);
        jtfNameGRF.setBackground(Color.white);
        jtfNameGRF.setForeground(Color.darkGray);
        jpALLG.add(jtfNameGRF, gbc);
        //
        gbc.gridx=0;   gbc.gridy=1;
        JLabel jlu1= new JLabel(TxtI.ti_cur_DialogCurveProperties+" :  "); 
        jlu1.setFont(TxtI.ti_Font_B); 
        jpALLG.add(jlu1, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfNameKURV= new FormatJTextField();
        jtfNameKURV.setText(_jtfNameKURV);
        jtfNameKURV.setEditable(false);
        jtfNameKURV.setBackground(Color.white);
        jtfNameKURV.setForeground(Color.darkGray);
        jpALLG.add(jtfNameKURV, gbc);
        //
        gbc.gridx=0;   gbc.gridy=2;
        JLabel jlu2= new JLabel(TxtI.ti_ax_DialogCurveProperties+" :  "); 
        jlu2.setFont(TxtI.ti_Font_B); 
        jpALLG.add(jlu2, gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcbAchsenTyp= new JComboBox(new String[]{"X", "SIGNAL", "INACTIVE"});
        jcbAchsenTyp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                //------------
                boolean aktiviere= true;
                switch (jcbAchsenTyp.getSelectedIndex()) {
                    case 0:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_X;    aktiviere= false;  break;  // alten X-Wert aus der Matrix loeschen (es kann nur genau 1 X pro Zeile geben) -->
                    case 1:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_SIGNAL;    aktiviere= true;   break;
                    case 2:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_NIX;  aktiviere= false;  break;
                    default: System.out.println("Fehler: je5zj7z"); 
                }
                //------------
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(aktiviere);    jcbLineColor.setEnabled(aktiviere);
                // Verknuepfungsmartix aktualisieren -->
                connectionMatrixFenster.setzeMatrixEintrag(im1,im2,_jcbAchsenTyp);
                //------------
                bestaetige();
                //------------
            }
        });
        jpALLG.add(jcbAchsenTyp, gbc);
        //==========================================================================================
        // (1) Linien-Properties:
        JPanel jpLINE= new JPanel();
        jpLINE.setLayout(new GridBagLayout());
        jpLINE.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x2_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=0;   gbc.gridy=0;   gbc.fill=gbc.BOTH;
        JLabel jlu3= new JLabel(TxtI.ti_sty_DialogCurveProperties+" :  "); 
        jlu3.setFont(TxtI.ti_Font_B); 
        jpLINE.add(jlu3, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jcbLineStyle= new JComboBox(GraferV3.LINIEN_STIL);
        jcbLineStyle.setSelectedIndex(this.setzeLinienstilListe(_jcbLineStyle));
        jcbLineStyle.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbLineStyle= setzeLinienstilSelektiert(jcbLineStyle.getSelectedIndex());
                bestaetige();
            }
        });
        jpLINE.add(jcbLineStyle, gbc);
        //
        gbc.gridx=0;   gbc.gridy=1;
        JLabel jlu4= new JLabel(TxtI.ti_col_DialogCurveProperties+" :  "); 
        jlu4.setFont(TxtI.ti_Font_B); 
        jpLINE.add(jlu4, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcbLineColor= new JComboBox(GraferV3.FARBEN);
        jcbLineColor.setSelectedIndex(this.setzeFarbListe(_jcbLineColor));
        jcbLineColor.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbLineColor= setzeFarbeSelektiert(jcbLineColor.getSelectedIndex());
                bestaetige();
            }
        });
        jpLINE.add(jcbLineColor, gbc);
        //
        //==========================================================================================
        // (2) DigitalCurveFilling-Properties:
        JPanel jpFILL= new JPanel();
        jpFILL.setLayout(new GridBagLayout());
        jpFILL.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x_DialogDigitalCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=1;   gbc.gridy=0;
        JLabel jlu9= new JLabel(TxtI.ti_fill_DialogDigitalCurveProperties+"a  "); 
        jlu9.setFont(TxtI.ti_Font_B); 
        jpFILL.add(jlu9, gbc);
        gbc.gridx=0;   gbc.gridy=0;
        jcbFillDigitalCurves= new JCheckBox();
        jcbFillDigitalCurves.setSelected(_jcbFillDigitalCurves);
        jcbFillDigitalCurves.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbFillDigitalCurves= jcbFillDigitalCurves.isSelected();
                if (_jcbFillDigitalCurves) jcbFillingDigitalColor.setEnabled(true);
                else jcbFillingDigitalColor.setEnabled(false); 
                bestaetige();
            }
        });
        jpFILL.add(jcbFillDigitalCurves, gbc);
        gbc.fill=gbc.BOTH;
        //
        gbc.gridx=0;   gbc.gridy=1;
        JLabel jlu8= new JLabel(TxtI.ti_col_DialogCurveProperties+" :  "); 
        jlu8.setFont(TxtI.ti_Font_B); 
        jpFILL.add(jlu8, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jcbFillingDigitalColor= new JComboBox(GraferV3.FARBEN);
        jcbFillingDigitalColor.setSelectedIndex(this.setzeFarbListe(_jcbFillingDigitalColor));
        jcbFillingDigitalColor.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbFillingDigitalColor= setzeFarbeSelektiert(jcbFillingDigitalColor.getSelectedIndex());
                bestaetige();
            }
        });
        jpFILL.add(jcbFillingDigitalColor, gbc);
        //
        //==========================================================================================
        //
        // anschliessende Initialisierung:
        //
        switch (_jcbAchsenTyp) {
            case GraferImplementation.ZUORDNUNG_X:
                jcbAchsenTyp.setSelectedIndex(0);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(false);    jcbLineColor.setEnabled(false);
                break;
            case GraferImplementation.ZUORDNUNG_SIGNAL:
                jcbAchsenTyp.setSelectedIndex(1);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(true);    jcbLineColor.setEnabled(true);
                break;
            case GraferImplementation.ZUORDNUNG_NIX:
                jcbAchsenTyp.setSelectedIndex(2);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(false);    jcbLineColor.setEnabled(false);
                break;
            case GraferImplementation.ZUORDNUNG_Y:   break;
            default: System.out.println("Fehler: ouerg7u7u6549"); 
        }
        //==========================================================================================
        // Gesamt-Panel -->
        JPanel pGes= new JPanel();
        pGes.setLayout(new GridBagLayout());
        //
        gbc.gridx=0;   gbc.gridy=0;
        pGes.add(jpLINE, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        pGes.add(jpFILL, gbc);
        gbc.gridx=0;   gbc.gridy=1;
        //pGes.add(jpALLG, gbc);
        //
        return pGes;
        //--------------------
    }




    private int setzeFarbListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.BLACK:     return  0;
            case GraferV3.RED:       return  1;
            case GraferV3.GREEN:     return  2;
            case GraferV3.BLUE:      return  3;
            case GraferV3.DARKGRAY:  return  4;
            case GraferV3.GRAY:      return  5;
            case GraferV3.LIGTHGRAY: return  6;
            case GraferV3.WHITE:     return  7;
            case GraferV3.MAGENTA:   return  8;
            case GraferV3.CYAN:      return  9;
            case GraferV3.ORANGE:    return 10;
            case GraferV3.YELLOW:    return 11;
            case GraferV3.DARKGREEN: return 12;
            default:
                System.out.println("Fehler: rthjrzj5"); 
                return -1;
        }
        //--------------------
    }


    private int setzeFarbeSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.BLACK;
            case  1:  return GraferV3.RED;
            case  2:  return GraferV3.GREEN;
            case  3:  return GraferV3.BLUE;
            case  4:  return GraferV3.DARKGRAY;
            case  5:  return GraferV3.GRAY;
            case  6:  return GraferV3.LIGTHGRAY;
            case  7:  return GraferV3.WHITE;
            case  8:  return GraferV3.MAGENTA;
            case  9:  return GraferV3.CYAN;
            case 10:  return GraferV3.ORANGE;
            case 11:  return GraferV3.YELLOW;
            case 12:  return GraferV3.DARKGREEN;
            default:
                System.out.println("Fehler: drdzmndzn"); 
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.SOLID_PLAIN:  return  0;
            case GraferV3.INVISIBLE:    return  1;
            case GraferV3.SOLID_FAT_1:  return  2;
            case GraferV3.SOLID_FAT_2:  return  3;
            case GraferV3.DOTTED_PLAIN: return  4;
            case GraferV3.DOTTED_FAT:   return  5;
            default:
                System.out.println("Fehler: 67j65h"); 
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.SOLID_PLAIN;
            case  1:  return GraferV3.INVISIBLE;
            case  2:  return GraferV3.SOLID_FAT_1;
            case  3:  return GraferV3.SOLID_FAT_2;
            case  4:  return GraferV3.DOTTED_PLAIN;
            case  5:  return GraferV3.DOTTED_FAT;
            default:
                System.out.println("Fehler: 34t6ujsd");
                return -1;
        }
        //--------------------
    }


    private int setzeClippingListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.CLIP_ACHSE:  return  0;
            case GraferV3.CLIP_NO:     return  1;
            case GraferV3.CLIP_VALUE:  return  2;
            default:
                System.out.println("Fehler: dgkdkjdjkdd"); 
                return -1;
        }
        //--------------------
    }


    private int setzeClippingSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.CLIP_ACHSE;
            case  1:  return GraferV3.CLIP_NO;
            case  2:  return GraferV3.CLIP_VALUE;
            default:
                System.out.println("Fehler: rh5wjj37u"); 
                return -1;
        }
        //--------------------
    }



    private void bestaetige () {
        //----------------------------
        // Zahlenwerte werden von JTextField() eingelesen, falls nicht "Return" gedrueckt wurde -->
        // lokal gespeicherte Parameter werden an das Diagramm zurueckgemeldet -->
        grafer.crvLineStyle[im1][im2]= _jcbLineStyle;
        grafer.crvLineColor[im1][im2]= _jcbLineColor;
        grafer.crvFillDigitalCurves[im1][im2]=   _jcbFillDigitalCurves; 
        grafer.crvFillingDigitalColor[im1][im2]= _jcbFillingDigitalColor; 
        //----------------------------
        grafer.berechneNotwendigeHoeheSIGNALGraph();
        grafer.setzeKurven();
        grafer.repaint();
    }




    //-----------------------------
    public void windowClosing (WindowEvent we) { this.dispose(); }
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowOpened (WindowEvent we) {}
    //-----------------------------

}


