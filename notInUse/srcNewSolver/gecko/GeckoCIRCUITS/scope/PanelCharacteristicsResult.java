/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.TxtI;
import gecko.GeckoCIRCUITS.allg.Typ;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author andy
 */
public class PanelCharacteristicsResult extends JPanel {
    private JPanel _pERGx = new JPanel();
        
    
    FormatJTextField[] ftfAVG;  // Textfelder fuer AVG-Werte
    FormatJTextField[] ftfRMS;  // Textfelder fuer RMS-Werte
    FormatJTextField[] ftfMIN, ftfMAX;  // Textfelder fuer minimale und maximale Werte
    FormatJTextField[] ftfCREST, ftfSHAPE, ftfTHD, ftfRIPPLE;  // Textfelder fuer weitere Kennwerte 
    private final JPanel _pClcq;
    private final JPanel _pClc;
    
    
    public PanelCharacteristicsResult(String[] header) {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_jmiChar_ScopeX, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        _pClcq = new JPanel();
        _pClcq.setLayout(new GridLayout(header.length, 1));
        //
        JLabel jlH1 = new JLabel("");
        jlH1.setFont(Typ.LAB_FONT_DIALOG_1);
        jlH1.setForeground(Typ.LAB_COLOR_DIALOG_1);
        _pClcq.add(jlH1);
        for (int i1 = 1; i1 < header.length; i1++) {
            JLabel jlH = new JLabel(header[i1] + "   ");
            jlH.setFont(Typ.LAB_FONT_DIALOG_1);
            jlH.setForeground(Typ.LAB_COLOR_DIALOG_1);
            _pClcq.add(jlH);
        }

        _pClc = new JPanel();
        _pClc.setLayout(new GridLayout(header.length, 8));
        
        setCharTitleLabels();
        
        final int columnLength = header.length - 1;
        ArrayList<FormatJTextField[]> allFormatTextFields = new ArrayList<FormatJTextField[]>();

        ftfAVG = new FormatJTextField[columnLength];
        ftfRMS = new FormatJTextField[columnLength];
        ftfTHD = new FormatJTextField[columnLength];
        ftfMIN = new FormatJTextField[columnLength];
        ftfMAX = new FormatJTextField[columnLength];
        ftfRIPPLE = new FormatJTextField[columnLength];
        ftfCREST = new FormatJTextField[columnLength];
        ftfSHAPE = new FormatJTextField[columnLength];


        allFormatTextFields.add(ftfAVG);
        allFormatTextFields.add(ftfRMS);
        allFormatTextFields.add(ftfTHD);
        allFormatTextFields.add(ftfMIN);
        allFormatTextFields.add(ftfMAX);
        allFormatTextFields.add(ftfRIPPLE);
        allFormatTextFields.add(ftfCREST);
        allFormatTextFields.add(ftfSHAPE);

        int cols = 8;
        for (int i1 = 0; i1 < columnLength; i1++) {
            for (FormatJTextField[] tfarray : allFormatTextFields) {
                tfarray[i1] = new FormatJTextField();
                tfarray[i1].setColumns(cols);
                tfarray[i1].setText("-");
                tfarray[i1].setEditable(false);
                tfarray[i1].setFont(new java.awt.Font("Arial", 0, 11));
                _pClc.add(tfarray[i1]);
            }
        }

        _pERGx.setLayout(new BorderLayout());
        _pERGx.add(_pClcq, BorderLayout.WEST);
        _pERGx.add(_pClc, BorderLayout.CENTER);
        this.add(_pERGx, BorderLayout.NORTH);
    }

    private void setCharTitleLabels() {
        ArrayList<JLabel> titleLabels = new ArrayList<JLabel>();
        titleLabels.add(new JLabel("avg"));
        titleLabels.add(new JLabel("rms"));
        titleLabels.add(new JLabel("THD"));
        titleLabels.add(new JLabel("min"));
        titleLabels.add(new JLabel("max"));
        titleLabels.add(new JLabel("ripple"));
        titleLabels.add(new JLabel("klirr"));
        titleLabels.add(new JLabel("shape"));

        for(JLabel tmp : titleLabels) {
            tmp.setFont(Typ.LAB_FONT_DIALOG_1);
            tmp.setForeground(Typ.LAB_COLOR_DIALOG_1);
            _pClc.add(tmp);
        }
        
    }
}
