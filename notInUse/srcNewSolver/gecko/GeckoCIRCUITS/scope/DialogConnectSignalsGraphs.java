/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JDialog; 
import javax.swing.JCheckBox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.util.StringTokenizer;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.net.URL;



public class DialogConnectSignalsGraphs extends JDialog implements WindowListener, ActionListener, ComponentListener {

    //----------------------------
    private GraferImplementation grafer;
    private Scope callback;
    private Container c;
    private FormatJTextField[][] jbM;
    private JButton[] jlGRF;  // draufklicken --> Fenster zum Graph-Editieren
    private FormatJTextField[] jlST;  // Status pro Matrix-Zeile
    private JCheckBox[] jcbVIS;  // ist Graph 'Digital'?
    private FormatJTextField[] jtfWEIG;  // relative Graphen-Gewichtung (y-Achse) mi SCOPE
    private JLabel jlStatus;  // Status-Gesamt
    private Color defaultColor= new JLabel().getForeground();
    private JButton jbOK, jbCa, jbAp;
    private String[] zeilenStatus;
    private JButton jbAdd,jbDel;  // Add/Delete Graph
    //----------------------------
    public static final int MATRIXZELLE_BREITE=30, MATRIXZELLE_HOEHE=28;
    //----------------------------
    private int[][] matrixZuordnungKurveDiagramLok;
    private int[] diagramTypLok;
    private double[] ySpacingDiagramLok;
    //----------------------------



    public DialogConnectSignalsGraphs (Scope callback, GraferImplementation grafer, boolean visible) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        //--------------------
        this.callback=callback;
        this.grafer=grafer;
        //--------------------
        // Lokale Parameter, die erste beim Druecken von 'OK' uebernommen werden -->
        //
        matrixZuordnungKurveDiagramLok= new int[grafer.matrixZuordnungKurveDiagram.length][grafer.matrixZuordnungKurveDiagram[0].length];
        for (int i1=0;  i1<grafer.matrixZuordnungKurveDiagram.length;  i1++) {
            for (int i2=0;  i2<grafer.matrixZuordnungKurveDiagram[0].length;  i2++) {
                matrixZuordnungKurveDiagramLok[i1][i2]= grafer.matrixZuordnungKurveDiagram[i1][i2];
            }
        }
        diagramTypLok= new int[grafer.diagramTyp.length];
        for (int i1=0;  i1<diagramTypLok.length;  i1++) {
            diagramTypLok[i1]= grafer.diagramTyp[i1];
        }
        ySpacingDiagramLok= new double[grafer.ySpacingDiagram.length];
        for (int i1=0;  i1<ySpacingDiagramLok.length;  i1++) {
            ySpacingDiagramLok[i1]= grafer.ySpacingDiagram[i1];
        }
        //--------------------
        this.setTitle(Typ.spTitle+"ConnectionMatrix  Signal - Graph");
        this.addWindowListener(this);
        this.addComponentListener(this);
        c= this.getContentPane();
        c.setLayout(new BorderLayout());
        //--------------------
        c.removeAll();
        this.baueGUI();
        this.pack();
        this.setResizable(false);
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(callback.getX_PositionAbsolut()+Typ.DX_SCOPE_DIALOG, callback.getY_PositionAbsolut()+Typ.DY_SCOPE_DIALOG);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setVisible(visible);
        //--------------------
        aktualisiereGewichtungDerGraphen(false);
        //--------------------
    }




    public void setzeMatrixEintrag (int im1, int im2, int eintrag) {
        //--------------------
        matrixZuordnungKurveDiagramLok[im1][im2]= eintrag;
        if (diagramTypLok[im1]==grafer.DIAGRAM_TYP_SGN) {  // Digital
            if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_X)           jbM[im1][im2].setText("X");
            else if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_SIGNAL) jbM[im1][im2].setText("sg");
            else if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_NIX)    jbM[im1][im2].setText("-");
            else System.out.println("Fehler: jrhzw5jwj6");
        } else {  // Analog
            if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_X)           jbM[im1][im2].setText("X");
            else if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_Y)      jbM[im1][im2].setText("Y");
            else if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_NIX)    jbM[im1][im2].setText("-");
            else System.out.println("Fehler: 58on7wbq5");
        }
        //--------------------
        // pro Zeile darf in der Zuordnungsmatrix nur ein X-Eintrag stehen --> der alte Eintrag muss geloescht werden -->
        if (matrixZuordnungKurveDiagramLok[im1][im2]==GraferImplementation.ZUORDNUNG_X)
            for (int i2=0;  i2<matrixZuordnungKurveDiagramLok[im1].length;  i2++)
                if ((i2!=im2) && (matrixZuordnungKurveDiagramLok[im1][i2]==GraferImplementation.ZUORDNUNG_X)) {
                    matrixZuordnungKurveDiagramLok[im1][i2]= GraferImplementation.ZUORDNUNG_NIX;
                    jbM[im1][i2].setText("-");
                }
        //--------------------
        this.checkStatus();
    }





    public void baueGUI () {
        //===========================================================================================================
        JPanel jpMatrix= new JPanel();
        jpMatrix.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogConnectSignalsGraphs, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        jpMatrix.setLayout(new GridBagLayout());
        GridBagConstraints gbc= new GridBagConstraints();
        JLabelRot[] jlSGN= new JLabelRot[grafer.anzSignalePlusZeit];
        jlGRF= new JButton[grafer.getAnzahlDiagramme()];
        jlST= new FormatJTextField[grafer.getAnzahlDiagramme()];
        jcbVIS= new JCheckBox[grafer.getAnzahlDiagramme()];
        jtfWEIG= new FormatJTextField[grafer.getAnzahlDiagramme()];
        jbM= new FormatJTextField[grafer.getAnzahlDiagramme()][grafer.anzSignalePlusZeit];
        //--------------------
        gbc.fill=gbc.BOTH;
        gbc.gridx=0;   gbc.gridy=0;
        JLabel jlX= new JLabel("");
        jpMatrix.add(jlX, gbc);  // MATRIX: Ecke links oben --> unbeschriftet
        for (int i1=0;  i1<grafer.getAnzahlDiagramme()+1;  i1++) {
            if (i1>0)  {
                jlGRF[i1-1]= new JButton(grafer.nameDiagram[i1-1]+"   ");
                jlGRF[i1-1].setActionCommand(""+i1);
                jlGRF[i1-1].addActionListener(new ActionListener () {
                    public void actionPerformed (ActionEvent ae) {
                        int grfIndex= (new Integer(ae.getActionCommand())).intValue()-1;
                        if (diagramTypLok[grfIndex]==GraferImplementation.DIAGRAM_TYP_ZV) new DialogGraphProperties(grafer, grfIndex);
                        else if (diagramTypLok[grfIndex]==GraferImplementation.DIAGRAM_TYP_SGN) new DialogDigitalGraphProperties(grafer, grfIndex);
                    }
                });
                gbc.gridx=0;   gbc.gridy=i1;
                jpMatrix.add(jlGRF[i1-1], gbc);  // MATRIX: Beschriftung Graph-Namen
            }
            for (int i2=0;  i2<grafer.anzSignalePlusZeit+4;  i2++) {
                if ((i1==0)&&(i2>0)&&(i2<grafer.anzSignalePlusZeit+1)) {
                    jlSGN[i2-1]= new JLabelRot(grafer.signalNamen[i2-1], -90.0, MATRIXZELLE_BREITE, 80);
                    gbc.gridx=i2;   gbc.gridy=0;
                    jpMatrix.add(jlSGN[i2-1], gbc);  // MATRIX: Beschriftung Signal-Namen
                }
                if ((i1>0)&&(i2>0)&&(i2<grafer.anzSignalePlusZeit+1)) {
                    jbM[i1-1][i2-1]= new FormatJTextField();
                    jbM[i1-1][i2-1].setEditable(false);
                    jbM[i1-1][i2-1].setBackground(Color.white);
                    if (i2-1 != 0) jbM[i1-1][i2-1].setForeground(Color.red); else jbM[i1-1][i2-1].setForeground(Color.black);  // x-Achse (Zeit) nicht anklickbar 
                    jbM[i1-1][i2-1].setHorizontalAlignment(JTextField.CENTER);
                    jbM[i1-1][i2-1].setToolTipText(TxtI.ti_ttip_DialogConnectSignalsGraphs);
                    if (diagramTypLok[i1-1]==grafer.DIAGRAM_TYP_SGN) {  // Digital
                        if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_X)           jbM[i1-1][i2-1].setText("X");
                        else if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_SIGNAL) jbM[i1-1][i2-1].setText("sg");
                        else if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_NIX)    jbM[i1-1][i2-1].setText("-");
                        else System.out.println("Fehler: 90u3ng9038");
                    } else {  // Analog
                        if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_X)           jbM[i1-1][i2-1].setText("X");
                        else if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_Y)      jbM[i1-1][i2-1].setText("Y");
                        else if (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_NIX)    jbM[i1-1][i2-1].setText("-");
                        else System.out.println("Fehler: fwhggfuf0");
                    }
                    gbc.gridx=i2;   gbc.gridy=i1;
                    jpMatrix.add(jbM[i1-1][i2-1], gbc);  // MATRIX: x/y/y2/sg - Knoepfe als zentrales Element
                    //----------------
                    jbM[i1-1][i2-1].setActionCommand(i1+" "+i2);
                    final DialogConnectSignalsGraphs ref= this;  // zur Referenzierbarkeit aus dem Listener heraus
                    if (i2-1 != 0) jbM[i1-1][i2-1].addMouseListener(new MouseAdapter () {  // x-Achse (Zeit) nicht anklick- und veraenderbar 
                        public void mousePressed (MouseEvent me) {
                            //------------------------
                            // Position in der Matrix bestimmen:
                            int q1= me.toString().indexOf("command=")+8;
                            String com= me.toString().substring(q1);
                            int q2= com.indexOf(",");
                            String com2= com.substring(0,q2);
                            StringTokenizer stk= new StringTokenizer(com2, " ");
                            int i1= (new Integer(stk.nextToken())).intValue();
                            int i2= (new Integer(stk.nextToken())).intValue();
                            //------------------------
                            // rechte Maus --> 'Flippen' der ZUORDNUNG ohne Dialogfenster-Eingabe
                            if (me.getModifiers()==me.BUTTON3_MASK) {
                                int _jcbAchsenTyp= grafer.get_crvAchsenTyp(i1-1,i2-1);
                                if (diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_ZV) {
                                    if (_jcbAchsenTyp!=GraferImplementation.ZUORDNUNG_NIX) setzeMatrixEintrag(i1-1,i2-1,GraferImplementation.ZUORDNUNG_NIX);
                                    else setzeMatrixEintrag(i1-1,i2-1,GraferImplementation.ZUORDNUNG_Y);
                                } else if (diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_SGN) {
                                    if (_jcbAchsenTyp!=GraferImplementation.ZUORDNUNG_NIX) setzeMatrixEintrag(i1-1,i2-1,GraferImplementation.ZUORDNUNG_NIX);
                                    else setzeMatrixEintrag(i1-1,i2-1,GraferImplementation.ZUORDNUNG_SIGNAL);
                                }
                                grafer.berechneNotwendigeHoeheSIGNALGraph();
                                checkStatus();
                                return;
                            }
                            //------------------------
                            // linke Maus --> Dialogfenster
                            if ((diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_ZV)
                                    && (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_Y)) 
                                new DialogCurveProperties(ref,grafer,i1,i2);
                            else if ((diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_SGN) 
                                    && (matrixZuordnungKurveDiagramLok[i1-1][i2-1]==GraferImplementation.ZUORDNUNG_SIGNAL)) 
                                new DialogDigitalCurveProperties(ref,grafer,i1,i2);
                            //-----------
                            // Umrechnen der SIGNAL-Graphen auf fixe Pix-Werte:
                            for (int i6=0;  i6<diagramTypLok.length;  i6++)  grafer.diagramTyp[i6]= diagramTypLok[i6];
                            grafer.berechneNotwendigeHoeheSIGNALGraph();
                            //-----------
                            checkStatus();
                        }
                    });
                    //----------------
                }
            }
            if (i1==0) {
                gbc.gridx=grafer.anzSignalePlusZeit+1;   gbc.gridy=i1;
                JLabelRot jlrSTAT= new JLabelRot(TxtI.ti_stat_DialogConnectSignalsGraphs, -90.0, MATRIXZELLE_BREITE, 70);
                jlrSTAT.setFont(TxtI.ti_Font_B); 
                jpMatrix.add(jlrSTAT, gbc);  // MATRIX: Beschriftung "Status"
                //
                gbc.gridx=grafer.anzSignalePlusZeit+2;   gbc.gridy=i1;
                JLabelRot jlrWEIG= new JLabelRot(TxtI.ti_weig_DialogConnectSignalsGraphs, -90.0, MATRIXZELLE_BREITE+10, 70);
                jlrWEIG.setFont(TxtI.ti_Font_B); 
                jpMatrix.add(jlrWEIG, gbc);
                //
                gbc.gridx=grafer.anzSignalePlusZeit+3;   gbc.gridy=i1;
                // <html><b><font color=#0064000)>Digital</font></html>
                JLabelRot jlrVIS= new JLabelRot("Digital", -90.0, MATRIXZELLE_BREITE+10, 70);
                jlrVIS.setFont(TxtI.ti_Font_B); 
                jpMatrix.add(jlrVIS, gbc);  // MATRIX: Beschriftung "Digital" (statt "Analog")
            } else if (i1>0) {
                //----------------
                jlST[i1-1]= new FormatJTextField();
                jlST[i1-1].setEditable(false);
                jlST[i1-1].setBackground(Color.white);
                jlST[i1-1].setForeground(Color.red);
                jlST[i1-1].setHorizontalAlignment(JTextField.CENTER);
                jlST[i1-1].setPreferredSize(new Dimension(MATRIXZELLE_BREITE, MATRIXZELLE_HOEHE));
                jlST[i1-1].setActionCommand(""+(i1-1));
                jlST[i1-1].addMouseListener(new MouseAdapter () {
                    public void mousePressed (MouseEvent me) {
                        int q1= me.toString().indexOf("command=")+8;
                        String com= me.toString().substring(q1);
                        int q2= com.indexOf(",");
                        String com2= com.substring(0,q2);
                        //-----------
                        int index= (new Integer(com2)).intValue();
                        jlStatus.setText(zeilenStatus[index]);
                    }
                });
                gbc.gridx=grafer.anzSignalePlusZeit+1;   gbc.gridy=i1;
                jpMatrix.add(jlST[i1-1], gbc);  // MATRIX: Status-Knoepfe --> OK/Info
                //----------------
                jtfWEIG[i1-1]= new FormatJTextField();
                jtfWEIG[i1-1].setActionCommand(""+i1);
                jtfWEIG[i1-1].setPreferredSize(new Dimension(MATRIXZELLE_BREITE, MATRIXZELLE_HOEHE));
                if (diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_ZV) {
                    jtfWEIG[i1-1].setEnabled(true);
                    jtfWEIG[i1-1].setNumberToField(Math.round(100*ySpacingDiagramLok[i1-1]));
                } else {
                    jtfWEIG[i1-1].setText("-");
                    jtfWEIG[i1-1].setEnabled(false);
                }
                //
                jtfWEIG[i1-1].addActionListener(new ActionListener () {
                    public void actionPerformed (ActionEvent ae) {
                        aktualisiereGrafer();
                    }
                });
                //
                gbc.gridx=grafer.anzSignalePlusZeit+2;   gbc.gridy=i1;   gbc.fill=gbc.NONE;
                jpMatrix.add(jtfWEIG[i1-1], gbc);
                gbc.fill=gbc.BOTH;
                //----------------
                jcbVIS[i1-1]= new JCheckBox();
                jcbVIS[i1-1].setActionCommand(""+i1);
                jcbVIS[i1-1].setPreferredSize(new Dimension(MATRIXZELLE_BREITE/2, MATRIXZELLE_HOEHE));
                if (diagramTypLok[i1-1]==GraferImplementation.DIAGRAM_TYP_SGN) jcbVIS[i1-1].setSelected(true);
                else jcbVIS[i1-1].setSelected(false);
                //
                jcbVIS[i1-1].addActionListener(new ActionListener () {
                    public void actionPerformed (ActionEvent ae) {
                        int index= (new Integer(ae.getActionCommand())).intValue();
                        if (jcbVIS[index-1].isSelected()) {
                            // -------------------
                            // Umschaltung von ZV-analog auf DIGITAL -->
                            diagramTypLok[index-1]= GraferImplementation.DIAGRAM_TYP_SGN;
                            for (int i3=1;  i3<grafer.anzSignalePlusZeit+1;  i3++)
                                if ((jbM[index-1][i3-1].getText().equals("Y"))) {
                                    jbM[index-1][i3-1].setText("sg");
                                    matrixZuordnungKurveDiagramLok[index-1][i3-1]= GraferImplementation.ZUORDNUNG_SIGNAL;
                                    grafer.set_crvAchsenTyp (index-1, i3-1, matrixZuordnungKurveDiagramLok[index-1][i3-1]);
                                    // y-Achse default-maessig ausblenden: 
                                    grafer.yShowGridMaj[index-1]=false;   grafer.yShowGridMin[index-1]=false; 
                                }
                            // -------------------
                        } else {
                            // -------------------
                            // Umschaltung von DIGITAL auf ZV-analog -->
                            diagramTypLok[index-1]= GraferImplementation.DIAGRAM_TYP_ZV;
                            for (int i3=1;  i3<grafer.anzSignalePlusZeit+1;  i3++)
                                if (jbM[index-1][i3-1].getText().equals("sg")) {
                                    jbM[index-1][i3-1].setText("Y");
                                    matrixZuordnungKurveDiagramLok[index-1][i3-1]= GraferImplementation.ZUORDNUNG_Y;
                                    grafer.set_crvAchsenTyp (index-1, i3-1, matrixZuordnungKurveDiagramLok[index-1][i3-1]);
                                    // y-Achse default-maessig einblenden: 
                                    grafer.yShowGridMaj[index-1]=true;   grafer.yShowGridMin[index-1]=true; 
                                }
                            grafer.definiereAchsenbegrenzungenImAutoZoom();
                            berechneGewichtungDerGraphen();  // alle Graphen gleich hoch, nachdem ein SIGNAL auf ZV zurueckgeschaltet wurde
                            // -------------------
                        }
                        //----------------------
                        // Umrechnen der SIGNAL-Graphen auf fixe Pix-Werte:
                        for (int i1=0;  i1<diagramTypLok.length;  i1++)  grafer.diagramTyp[i1]= diagramTypLok[i1];
                        grafer.berechneNotwendigeHoeheSIGNALGraph();
                        //----------------------
                        if (diagramTypLok[index-1]==GraferImplementation.DIAGRAM_TYP_ZV) {
                            jtfWEIG[index-1].setEnabled(true);
                            jtfWEIG[index-1].setNumberToField(Math.round(100*ySpacingDiagramLok[index-1]));
                        } else {
                            jtfWEIG[index-1].setText("-");
                            jtfWEIG[index-1].setEnabled(false);
                        }
                        //----------------------
                        bestaetigeMatrix();
                    }
                });
                //
                gbc.gridx=grafer.anzSignalePlusZeit+3;   gbc.gridy=i1;   gbc.fill=gbc.NONE;
                jpMatrix.add(jcbVIS[i1-1], gbc);  // MATRIX: Check-Boxen Digital JA/NEIN ?
                gbc.fill=gbc.BOTH;  // fuer alle anderen Elemente
                //----------------
            }
        }
        gbc.gridx=0;   gbc.gridy=grafer.getAnzahlDiagramme()+2;
        jpMatrix.add(new JLabel(" "), gbc);  // MATRIX: Vertikaler Abstandshalter nach unten
        //===========================================================================================================
        //
        jbAdd= new JButton(TxtI.ti_add_DialogConnectSignalsGraphs);
        jbAdd.setFont(TxtI.ti_Font_A); 
        jbDel= new JButton (TxtI.ti_rmv_DialogConnectSignalsGraphs);
        jbDel.setFont(TxtI.ti_Font_A); 
        jbAdd.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (grafer.getAnzahlDiagramme()>=grafer.ANZ_DIAGRAM_MAX-1) return;
                grafer.setAnzahlDiagramme(grafer.getAnzahlDiagramme()+1);  //  grafer.anzDiagram++;
                setResizable(true);
                c.removeAll();
                baueGUI();
                pack();
                setResizable(false);
                //-----------------------
                // die x-Achse wird nur beim untesten Diagramm angezeigt --> Aktualisierung
                wechsleDarstellungDerXAchse(true);
                // Graph-Gewichtung des neuen Graphen muss angepasst werden:
                berechneGewichtungDerGraphen();
                aktualisiereGrafer();
                //-----------------------
            }
        });
        jbDel.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (grafer.getAnzahlDiagramme()==1) return;  // mindestens 1 Graph muss angezeigt werden
                grafer.setAnzahlDiagramme(grafer.getAnzahlDiagramme()-1);  // grafer.anzDiagram--;
                setResizable(true);
                c.removeAll();
                baueGUI();
                pack();
                setResizable(false);
                //-----------------------
                // die x-Achse wird nur beim untesten Diagramm angezeigt --> Aktualisierung
                wechsleDarstellungDerXAchse(false);
                // Graph-Gewichtung der verringerten Graphen muss angepasst werden:
                berechneGewichtungDerGraphen();
                aktualisiereGrafer();
                //-----------------------
            }
        });
        JPanel p3= new JPanel();
        p3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x2_DialogConnectSignalsGraphs, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        p3.setLayout(new GridLayout(2,1));
        p3.add(jbAdd);
        p3.add(jbDel);
        //===========================================================================================================
        JPanel pSTAT= new JPanel();
        pSTAT.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x3_DialogConnectSignalsGraphs, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        pSTAT.setLayout(new BorderLayout());
        jlStatus= new JLabel(" ");
        jlStatus.setFont(TxtI.ti_Font_B); 
        int mindestLaenge= jpMatrix.getPreferredSize().width;
        if (mindestLaenge<300) mindestLaenge=300;
        jlStatus.setPreferredSize(new Dimension(mindestLaenge, (int)jlStatus.getPreferredSize().getHeight()));
        pSTAT.add(jlStatus, BorderLayout.CENTER);
        this.checkStatus();
        //===========================================================================================================
        JPanel jpALLG= new JPanel();
        jpALLG.setLayout(new GridBagLayout());
        gbc.gridx=0;   gbc.gridy=0;   gbc.gridheight=2;   gbc.gridwidth=1;
        jpALLG.add(p3,gbc);
        gbc.gridx=1;   gbc.gridy=0;   gbc.gridheight=1;   gbc.gridwidth=2;
        jpALLG.add(pSTAT,gbc);
        //
        //--------------------------------------
        JPanel pG= new JPanel();
        pG.setLayout(new BorderLayout());
        pG.add(jpMatrix,BorderLayout.CENTER);
        pG.add(jpALLG,BorderLayout.NORTH);
        //===========================================================================================================
        //===========================================================================================================
        JPanel jpOK= new JPanel();
        jbOK= new JButton(TxtI.ti_OK);
        jbOK.setFont(TxtI.ti_Font_A); 
        jbCa= new JButton(TxtI.ti_Cancel);
        jbCa.setFont(TxtI.ti_Font_A); 
        jbAp= new JButton(TxtI.ti_apply_DialogCheckSimulation);
        jbAp.setFont(TxtI.ti_Font_A); 
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                bestaetigeMatrix();
                dispose();
            }
        });
        jbCa.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { dispose(); }
        });
        jbAp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                bestaetigeMatrix();
            }
        });
        jpOK.add(jbOK);
        jpOK.add(jbCa);
        jpOK.add(jbAp);
        //===========================================================================================================
        //===========================================================================================================
        c.add(pG, BorderLayout.CENTER);
        c.add(jpOK, BorderLayout.SOUTH);
        //
        //===========================================================================================================
        //===========================================================================================================
    }







    private void bestaetigeMatrix () {
        //-------------------------------
        for (int i1=0;  i1<grafer.matrixZuordnungKurveDiagram.length;  i1++)
            for (int i2=0;  i2<grafer.matrixZuordnungKurveDiagram[0].length;  i2++) {
                grafer.set_crvAchsenTyp (i1, i2, matrixZuordnungKurveDiagramLok[i1][i2]);
            }
        for (int i1=0;  i1<diagramTypLok.length;  i1++) {
            grafer.diagramTyp[i1]= diagramTypLok[i1];
        }
        for (int i1=0;  i1<ySpacingDiagramLok.length;  i1++) {
            grafer.ySpacingDiagram[i1]= ySpacingDiagramLok[i1];
        }
        //-------------------------------
        aktualisiereGrafer();
        callback.saveScopeSettings();
        //-------------------------------
    }





    private void aktualisiereGewichtungDerGraphen (boolean aktualisierteDatenBestaetigen) {
        //-------------------------------
        int ySpGes=0;  // // fuer Normierung auf 100% insgesamt
        for (int i1=0;  i1<jtfWEIG.length;  i1++) {
            int ySp=0;
            try { ySp= (int)jtfWEIG[i1].getNumberFromField(); } catch (Exception e) {}
            if (diagramTypLok[i1]==GraferImplementation.DIAGRAM_TYP_ZV)
                ySpGes += ySp;
            ySpacingDiagramLok[i1]= ySp;
        }
        for (int i1=0;  i1<jtfWEIG.length;  i1++) {
            if (diagramTypLok[i1]==GraferImplementation.DIAGRAM_TYP_ZV) {
                jtfWEIG[i1].setEnabled(true);
                jtfWEIG[i1].setNumberToField(Math.round(100*ySpacingDiagramLok[i1]/ySpGes));
            } else {
                jtfWEIG[i1].setText("-");
                jtfWEIG[i1].setEnabled(false);
            }
            //jtfWEIG[i1].setText(""+Math.round(100.0*ySpacingDiagramLok[i1]/ySpGes));
            if (ySpGes==0) ySpGes=1;  // ist dann relevant, wenn nur DIGITAL-Signale
            ySpacingDiagramLok[i1] /= (1.0*ySpGes);
            if (aktualisierteDatenBestaetigen)
                grafer.ySpacingDiagram[i1]= ySpacingDiagramLok[i1];
        }
        //-------------------------------
    }



    // wenn ein neuer Graph dazu- oder weggeschaltet wird -->
    private void berechneGewichtungDerGraphen () {
        //-----------------------
        // Graph-Gewichtung der verringerten Graphen muss angepasst werden:
        int zvs=0;
        for (int i1=0;  i1<grafer.getAnzahlDiagramme();  i1++)
            if (diagramTypLok[i1]==GraferImplementation.DIAGRAM_TYP_ZV) zvs++;
        for (int i1=0;  i1<grafer.getAnzahlDiagramme();  i1++) {
            if (zvs!=0)
                ySpacingDiagramLok[i1]= 1.0/zvs;  // default-Gewichtung
            else
                ySpacingDiagramLok[i1]= 1.0;
            if (diagramTypLok[i1]==GraferImplementation.DIAGRAM_TYP_ZV) {
                jtfWEIG[i1].setEnabled(true);
                jtfWEIG[i1].setNumberToField(Math.round(100*ySpacingDiagramLok[i1]));
            } else {
                jtfWEIG[i1].setText("-");
                jtfWEIG[i1].setEnabled(false);
            }
            //jtfWEIG[i1].setText(""+Math.round(100.0*ySpacingDiagramLok[i1]));
        }
        //-----------------------
    }





    private void checkStatus () {
        //-------------------------------
        // (1) nicht erlaubt: kein 'X' oder kein 'Y' bzw. 'Y2' in einer Zeile
        // (2) nicht erlaubt: mehr als genau 1 'X' in einer Zeile
        // (3) nicht erlaubt: Mischung 'sg' mit 'Y' bzw 'Y2' in einer Zeile
        //-------------------------------
        boolean errorTotal=false;
        zeilenStatus= new String[grafer.getAnzahlDiagramme()];
        for (int i1=0;  i1<grafer.getAnzahlDiagramme();  i1++) {
            boolean error=true;
            boolean keinX=true, keinY=true, keinSG=true;
            int anzX=0;
            for (int i2=0;  i2<grafer.anzSignalePlusZeit;  i2++) {
                if (matrixZuordnungKurveDiagramLok[i1][i2]==GraferImplementation.ZUORDNUNG_X) anzX++;
                if (matrixZuordnungKurveDiagramLok[i1][i2]==GraferImplementation.ZUORDNUNG_Y)
                    keinY= false;
                if (matrixZuordnungKurveDiagramLok[i1][i2]==GraferImplementation.ZUORDNUNG_SIGNAL)
                    keinSG=false;
            }
            if (anzX==0) zeilenStatus[i1]= TxtI.ti_se1_DialogConnectSignalsGraphs;
            else if (anzX>1) zeilenStatus[i1]= TxtI.ti_se2_DialogConnectSignalsGraphs;
            else if ((keinY)&&(keinSG)) zeilenStatus[i1]= TxtI.ti_se3_DialogConnectSignalsGraphs;
            else if ((!keinY)&&(!keinSG)) zeilenStatus[i1]= TxtI.ti_se4_DialogConnectSignalsGraphs;
            else { zeilenStatus[i1]= TxtI.ti_inok_DialogConnectSignalsGraphs;  error=false; }
            //
            if (error) {
                errorTotal=true;
                jlST[i1].setForeground(Color.red);
                jlST[i1].setText("?");
                jlST[i1].setEnabled(true);
            } else {
                jlST[i1].setForeground(defaultColor);
                jlST[i1].setText("ok");
                jlST[i1].setEnabled(false);
            }
        }
        //-------------------------------
        if (errorTotal) {
            jlStatus.setForeground(Color.red);
            jlStatus.setText(TxtI.ti_err_DialogConnectSignalsGraphs);
            try {
                jbOK.setEnabled(false);
                jbAp.setEnabled(false);
            } catch (Exception e) {}
        } else {
            jlStatus.setForeground(Color.blue);
            jlStatus.setText(TxtI.ti_inok_DialogConnectSignalsGraphs);
            try {
                jbOK.setEnabled(true);
                jbAp.setEnabled(true);
            } catch (Exception e) {}
            this.bestaetigeMatrix();
        }
        //-------------------------------
    }




    public void actionPerformed (ActionEvent ae) {
    }




    private void aktualisiereGrafer () {
        aktualisiereGewichtungDerGraphen(true);
        grafer.setzeAchsen();
        grafer.setzeKurven();
        grafer.blendeEventuellGridLinienAus(); 
        grafer.repaint();
    }




    private void wechsleDarstellungDerXAchse (boolean graphAdded) {
        //----------------------------
        // wenn die Graphen-Anzahl durch ADD oder DELETE GRAPH geaendert wird, wird die Sichtbarkeit der x-Achsen aktualisiert -->
        // 'grafer.anzGrfVisible' wurde bereits aktualisiert
        //
        int indexUntesterGraphALT= grafer.getAnzahlSichtbarerDiagramme() -2;
        int indexUntesterGraphNEU= grafer.getAnzahlSichtbarerDiagramme() -1;
        if (!graphAdded) {
            indexUntesterGraphALT= grafer.getAnzahlSichtbarerDiagramme() -0;
            indexUntesterGraphNEU= grafer.getAnzahlSichtbarerDiagramme() -1;
        }
        //--------------------
        // nur das NEUE unteste sichtbare Diagramm hat eine x-Achse -->
        grafer.showAxisX[indexUntesterGraphNEU]= grafer.showAxisX[indexUntesterGraphALT];
        //grafer.minX[indexUntesterGraphNEU]= grafer.minX[indexUntesterGraphALT];
        //grafer.maxX[indexUntesterGraphNEU]= grafer.maxX[indexUntesterGraphALT];
        //grafer.autoScaleX[indexUntesterGraphNEU]= grafer.autoScaleX[indexUntesterGraphALT];
        grafer.xAchseStil[indexUntesterGraphNEU]= grafer.xAchseStil[indexUntesterGraphALT];
        //grafer.xAchseFarbe[indexUntesterGraphNEU]= grafer.xAchseFarbe[indexUntesterGraphALT];
        //grafer.xAchsenTyp[indexUntesterGraphNEU]= grafer.xAchsenTyp[indexUntesterGraphALT];
        //grafer.xTickAutoSpacing[indexUntesterGraphNEU]= grafer.xTickAutoSpacing[indexUntesterGraphALT];
        //grafer.xTickSpacing[indexUntesterGraphNEU]= grafer.xTickSpacing[indexUntesterGraphALT];
        //grafer.xAnzTicksMinor[indexUntesterGraphNEU]= grafer.xAnzTicksMinor[indexUntesterGraphALT];
        grafer.xTickLaenge[indexUntesterGraphNEU]= grafer.xTickLaenge[indexUntesterGraphALT];
        grafer.xTickLaengeMinor[indexUntesterGraphNEU]= grafer.xTickLaengeMinor[indexUntesterGraphALT];
        grafer.zeigeLabelsXmaj[indexUntesterGraphNEU]= grafer.zeigeLabelsXmaj[indexUntesterGraphALT];
        grafer.zeigeLabelsXmin[indexUntesterGraphNEU]= grafer.zeigeLabelsXmin[indexUntesterGraphALT];
        //grafer.xShowGridMaj[indexUntesterGraphNEU]= grafer.xShowGridMaj[indexUntesterGraphALT];
        //grafer.xShowGridMin[indexUntesterGraphNEU]= grafer.xShowGridMin[indexUntesterGraphALT];
        //grafer.linStilGridNormalX[indexUntesterGraphNEU]= grafer.linStilGridNormalX[indexUntesterGraphALT];
        //grafer.linStilGridNormalXminor[indexUntesterGraphNEU]= grafer.linStilGridNormalXminor[indexUntesterGraphALT];
        //grafer.farbeGridNormalX[indexUntesterGraphNEU]= grafer.farbeGridNormalX[indexUntesterGraphALT];
        //grafer.farbeGridNormalXminor[indexUntesterGraphNEU]= grafer.farbeGridNormalXminor[indexUntesterGraphALT];
        /*
        grafer.ORIGjcmXlinCol[indexUntesterGraphNEU]= grafer.ORIGjcmXlinCol[indexUntesterGraphALT];
        grafer.ORIGjcmXlinStyl[indexUntesterGraphNEU]= grafer.ORIGjcmXlinStyl[indexUntesterGraphALT];
        grafer.ORIGjtfXtickLengthMaj[indexUntesterGraphNEU]= grafer.ORIGjtfXtickLengthMaj[indexUntesterGraphALT];
        grafer.ORIGjtfXtickLengthMin[indexUntesterGraphNEU]= grafer.ORIGjtfXtickLengthMin[indexUntesterGraphALT];
        grafer.ORIGjcbXShowLabelMaj[indexUntesterGraphNEU]= grafer.ORIGjcbXShowLabelMaj[indexUntesterGraphALT];
        grafer.ORIGjcbXShowLabelMin[indexUntesterGraphNEU]= grafer.ORIGjcbXShowLabelMin[indexUntesterGraphALT];
        grafer.ORIGjcbXShowGridMaj[indexUntesterGraphNEU]= grafer.ORIGjcbXShowGridMaj[indexUntesterGraphALT];
        grafer.ORIGjcbXShowGridMin[indexUntesterGraphNEU]= grafer.ORIGjcbXShowGridMin[indexUntesterGraphALT];
        */
        //
        //--------------------
        // das vorherige untesten Diagramm wird nun mit unsichtbarer x-Achse dargestelllt -->
        grafer.showAxisX[indexUntesterGraphALT]= false;
        //grafer.minX[indexUntesterGraphALT]= grafer.minX[indexUntesterGraphALT];
        //grafer.maxX[indexUntesterGraphALT]= grafer.maxX[indexUntesterGraphALT];
        //grafer.autoScaleX[indexUntesterGraphALT]= grafer.autoScaleX[indexUntesterGraphALT];
        grafer.xAchseStil[indexUntesterGraphALT]= GraferV3.INVISIBLE;
        //grafer.xAchseFarbe[indexUntesterGraphALT]= grafer.xAchseFarbe[indexUntesterGraphALT];
        //grafer.xAchsenTyp[indexUntesterGraphALT]= grafer.xAchsenTyp[indexUntesterGraphALT];
        //grafer.xTickAutoSpacing[indexUntesterGraphALT]= grafer.xTickAutoSpacing[indexUntesterGraphALT];
        //grafer.xTickSpacing[indexUntesterGraphALT]= grafer.xTickSpacing[indexUntesterGraphALT];
        //grafer.xAnzTicksMinor[indexUntesterGraphALT]= grafer.xAnzTicksMinor[indexUntesterGraphALT];
        grafer.xTickLaenge[indexUntesterGraphALT]= 0;
        grafer.xTickLaengeMinor[indexUntesterGraphALT]= 0;
        grafer.zeigeLabelsXmaj[indexUntesterGraphALT]= false;
        grafer.zeigeLabelsXmin[indexUntesterGraphALT]= false;
        //grafer.xShowGridMaj[indexUntesterGraphALT]= grafer.xShowGridMaj[indexUntesterGraphALT];
        //grafer.xShowGridMin[indexUntesterGraphALT]= grafer.xShowGridMin[indexUntesterGraphALT];
        //grafer.linStilGridNormalX[indexUntesterGraphALT]= grafer.linStilGridNormalX[indexUntesterGraphALT];
        //grafer.linStilGridNormalXminor[indexUntesterGraphALT]= grafer.linStilGridNormalXminor[indexUntesterGraphALT];
        //grafer.farbeGridNormalX[indexUntesterGraphALT]= grafer.farbeGridNormalX[indexUntesterGraphALT];
        //grafer.farbeGridNormalXminor[indexUntesterGraphALT]= grafer.farbeGridNormalXminor[indexUntesterGraphALT];
        /*
        grafer.ORIGjcmXlinCol[indexUntesterGraphALT]= grafer.ORIGjcmXlinCol[indexUntesterGraphALT];
        grafer.ORIGjcmXlinStyl[indexUntesterGraphALT]= grafer.ORIGjcmXlinStyl[indexUntesterGraphALT];
        grafer.ORIGjtfXtickLengthMaj[indexUntesterGraphALT]= grafer.ORIGjtfXtickLengthMaj[indexUntesterGraphALT];
        grafer.ORIGjtfXtickLengthMin[indexUntesterGraphALT]= grafer.ORIGjtfXtickLengthMin[indexUntesterGraphALT];
        grafer.ORIGjcbXShowLabelMaj[indexUntesterGraphALT]= grafer.ORIGjcbXShowLabelMaj[indexUntesterGraphALT];
        grafer.ORIGjcbXShowLabelMin[indexUntesterGraphALT]= grafer.ORIGjcbXShowLabelMin[indexUntesterGraphALT];
        grafer.ORIGjcbXShowGridMaj[indexUntesterGraphALT]= grafer.ORIGjcbXShowGridMaj[indexUntesterGraphALT];
        grafer.ORIGjcbXShowGridMin[indexUntesterGraphALT]= grafer.ORIGjcbXShowGridMin[indexUntesterGraphALT];
        */
        //--------------------
    }



    //-----------------------------
    public void windowClosing (WindowEvent we) { this.dispose(); }
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowOpened (WindowEvent we) {}
    //------------------------------------------------
    public void componentResized (ComponentEvent ce) {}
    public void componentMoved (ComponentEvent ce) {
        // Aktualisierung der SchematicEntry-Koord. am Screen (fuer Dialog-Positionierung)
        int x= ce.getComponent().getX();
        int y= ce.getComponent().getY();
        callback.setXY_PositionAbs_DialogConnect(x,y);
    }
    public void componentShown (ComponentEvent ce) {}
    public void componentHidden (ComponentEvent ce) {}
    //------------------------------------------------


}











