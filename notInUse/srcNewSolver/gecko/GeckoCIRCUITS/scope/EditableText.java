/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.DatenSpeicher; 

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics; 
import java.awt.Graphics2D; 
import java.util.StringTokenizer; 
import java.util.Vector; 
import java.util.StringTokenizer; 
import java.awt.FontMetrics; 
import java.awt.geom.Rectangle2D; 
import java.awt.font.FontRenderContext; 
import java.awt.font.TextLayout; 




public class EditableText {

    //-------------
    private int xA, yA;  // Text-Position (Punkt links oben)
    private int xE,yE;  // Punkt rechts unten des Rechtecks (klickbar), des den Textblock umschiesst 
    private int indexMaxZeilenLaenge; 
    private Font font; 
    private String[] txtLines; 
    private String txt; 
    private String txtColorFore, txtColorBack; 
    private String txtFontName, txtFontSize, txtFontStil; 
    //-------------
    private Vector txtEintraege; 
    private int zeiger=-1;  // zeigt auf den Texteintrag im Vector, der gerade in Bearbeitung ist 
    private int status; 
    public static final int STAUS_NEU=1; 
    public static final int STAUS_IN_BEARBEITUNG=2; 
    //-------------



    public EditableText (Vector txtEintraege, int mx, int my) {
        //--------------------------
        this.txtEintraege= txtEintraege; 
        // in 'txtEintraege' stehen alle Texteintraege vom Scope >> hier wird geprueft, ob wir bearbeiten oder neu anlegen ... 
        // wenn in ein bestehendes Text-Rechteck hineingeklickt wurde, wird dieses bearbeitet, ansonsten wird ein neuer Texteintrag angelegt --> 
        EditableText et=null; 
        int[] aussenRechteck= null; 
        status=STAUS_NEU;  // falls der Vector anfangs noch leer ist 
        xA=mx;   yA=my;
        //
        for (int i1=0;  i1<txtEintraege.size();  i1++) {
            et= (EditableText)txtEintraege.elementAt(i1); 
            aussenRechteck= et.getUmgrenzendesRechteck(); 
            if ((aussenRechteck[0]<=mx)&&(mx<=aussenRechteck[2])&&(aussenRechteck[1]<=my)&&(my<=aussenRechteck[3])) {
                status=STAUS_IN_BEARBEITUNG;
                zeiger= i1; 
                //-------------
                // Object aus Vector verwenden --> 
                this.txt=et.txt; 
                this.xA=et.xA;   this.yA=et.yA;   this.xE=et.xE;   this.yE=et.yE; 
  //              this.txtColor=et.txtColor;   this.txtFont=et.txtFont; 
                //-------------
                break; 
            } 
        } 
System.out.println("status= "+status); 
        DialogEditText dialog= new DialogEditText(this); 
        //--------------------------
    }

    
    
    
    public void setFontName (String x) { this.txtFontName=x; }
    public void setFontSize (String x) { this.txtFontSize=x; }
    public void setFontStil (String x) { this.txtFontStil=x; }
    public void setColorFore (String x) { this.txtColorFore=x; }
    public void setColorBack (String x) { this.txtColorBack=x; }

    public String getPlainText () { return txt; }
    public String getFontName () { return txtFontName; }
    public String getFontSize () { return txtFontSize; }
    public String getFontStil () { return txtFontStil; }
    public String getColorFore () { return txtColorFore; }
    public String getColorBack () { return txtColorBack; }
    
    public int getStatus () { return status; }
    public Vector getVectorAlleTextEintraege () { return txtEintraege; }
    public int getZeigerImVector () { return zeiger; }
    public int getPositionX () { return xA; }
    public int getPositionY () { return yA; }
    public int[] getUmgrenzendesRechteck () { return new int[]{xA,yA,xE,yE}; }
    

    
    public void setPlainText (String x) { 
        this.txt= x; 
        StringTokenizer stk= new StringTokenizer(x,"\n"); 
        txtLines= new String[stk.countTokens()]; 
        int i1=0; 
        int maxZeilenLaenge=0; 
        indexMaxZeilenLaenge=-1; 
        while (stk.hasMoreElements()) {
            txtLines[i1]= stk.nextToken();
            if (txtLines[i1].length()>maxZeilenLaenge) { maxZeilenLaenge=txtLines[i1].length();  indexMaxZeilenLaenge=i1; }
            i1++; 
        } 
    }

    

    public void zeichne (Graphics g) {
        Graphics2D g2= (Graphics2D)g; 
        //---------------------------
        // einmalige Initialisierung 
        //if (!rechteckBerechnet) {
            int f2= -1; 
            if (txtFontStil.equals(DialogEditText.FONT_STIL[0])) f2=Font.PLAIN; 
            else if (txtFontStil.equals(DialogEditText.FONT_STIL[1])) f2=Font.BOLD; 
            else if (txtFontStil.equals(DialogEditText.FONT_STIL[2])) f2=Font.ITALIC; 
            int f3= (new Integer(txtFontSize)).intValue(); 
            font= new Font(txtFontName,f2,f3); 
            //-----------
            int zeilenHoehe= g2.getFontMetrics().getHeight();
            int zeilenAbstand= 1+(int)(0.3*zeilenHoehe); 
            TextLayout tl= new TextLayout(txtLines[indexMaxZeilenLaenge], font, g2.getFontRenderContext()); 
            int laengeMax= (int)(tl.getBounds().getWidth());
            xE= xA +laengeMax; 
            yE= yA +txtLines.length*(zeilenHoehe+zeilenAbstand); 
        //}
        //---------------------------
        //g2.setColor(GraferV3.getSelectedColor(txtColorFore)); 
        //g2.drawRect(xA,yA, xE-xA,yE-yA); 
        g2.setColor(GraferV3.getSelectedColor(txtColorBack)); 
        g2.fillRect(xA,yA, xE-xA,yE-yA+zeilenAbstand); 
        g2.setColor(GraferV3.getSelectedColor(txtColorFore)); 
        g2.setFont(font); 
        for (int i1=0;  i1<txtLines.length;  i1++)
            g2.drawString(txtLines[i1], xA, yA+(i1+1)*(zeilenHoehe+zeilenAbstand)); 
        //---------------------------
    }
    
    
    
    public StringBuffer createASCII () {
        StringBuffer sb= new StringBuffer(); 
        String separator="\n"; 
        //-------------------
        sb.append("txt");  DatenSpeicher.appendAsString(sb, txt);  sb.append(separator); 
//        sb.append("txtColor");  DatenSpeicher.appendAsString(sb, txtColor.toString());  sb.append(separator); 
//        sb.append("txtFont");  DatenSpeicher.appendAsString(sb, txtFont.toString());  sb.append(separator); 
        //-------------------
        return sb; 
    }
    
    public void reconstructObjectFromASCII (String st) {
        String separator=";"; 
        StringTokenizer stk= new StringTokenizer(st, separator); 
        //-------------------
        txt= DatenSpeicher.leseASCII_String(stk.nextToken()); 
        //-------------------
    }

    
}



