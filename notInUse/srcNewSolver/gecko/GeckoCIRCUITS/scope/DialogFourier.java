/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;
import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.TechFormat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import java.net.URL;
import javax.swing.JOptionPane;

public class DialogFourier extends JDialog {

    //-------------
    private DataContainer worksheet;
    private String[] header;
    private boolean[] signalFourierAnalysiert;  // gibt zu jedem Header (= Signalnamen) an, ob Fourier-Analyse durchgefuehrt wurde
    private GridBagConstraints gbc = new GridBagConstraints();
    //-------------
    private TechFormat cf = new TechFormat();
    private JRadioButton jrb1, jrb2, jrb3;
    private FormatJTextField rngSc1, rngSc2, rngDf1, rngDf2, rngSl1, rngSl2;  // Angaben Zeitbereiche
    private double xScope1, xScope2;  // durch den Scope-Anzeigenbereich definierter Rechenbereich
    private double xSlider1, xSlider2;  // vom Slider-Paar definierter Rechenbereich
    private double xDef1, xDef2;  // von Hand definierter Rechenbereich
    private FormatJTextField ftfnMin, ftfnMax, ftff1;  // Textfelder fuer Fourier-Daten
    private double f1;  // Grundfrequenz fuer Fourieranalyse
    private int nMin, nMax;  // Grundfrequenz-Vielfache fuer Fourieranalyse
    private JCheckBox[] jcbZV;   // Auswahl der ZV-Kurven, die Fourier-analysiert werden soll
    private JButton jbCALC;  // Berechnung starten
    //-------------
    //-------------
    private double rng1 = 0, rng2 = 1;  // Bereichsgrenze fuer Berechnung 
    //-------------
    public int NN;

    public DialogFourier(DataContainer worksheet, String[] header) {
        super.setModal(true);
        try {
            this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL, "gecko.gif"))).getImage());
        } catch (Exception e) {
        }
        //--------------------------
        this.worksheet = worksheet;
        this.header = header;
        signalFourierAnalysiert = new boolean[header.length];
        //--------------------------
        // Default-Bereichsgrenzen:
        xScope1 = worksheet.getValue(0, 1);
        int i2 = worksheet.getColumnLength() - 1;
        xScope2 = worksheet.getValue(0, i2);
        while ((xScope2 == 0) && (i2 > 0)) {
            xScope2 = worksheet.getValue(0, i2);
            i2--;
        }
        if (xScope2 == xScope1) {
            xScope2 = xScope1 + 1;  // damit keine Division durch Null
        }        // die anderen beiden Einstellungen werden vorerst genauso gesetzt:
        xDef1 = xScope1;
        xDef2 = xScope2;
        xSlider1 = xScope1;
        xSlider2 = xScope2;
        //--------------------------
        nMin = 0;
        nMax = 100;  // default
        //--------------------------
        this.setTitle(Typ.spTitle + "Fourier-Transform");
        this.getContentPane().setLayout(new BorderLayout());
        this.baueGUI();
        this.pack();
        this.setResizable(false);
        //------------------------
    }

    // Setzen der Slider-Bereichsgrenzen von Extern -->
    public void setSliderValues(double xSlider1, double xSlider2) {
        this.xSlider1 = xSlider1;
        this.xSlider2 = xSlider2;
    }

    private void baueGUI() {
        //------------------
        Container con = this.getContentPane();
        con.setLayout(new BorderLayout());
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        int cols = 7;
        //
        //===========================================================
        //===========================================================
        JPanel pRNG = new JPanel();
        pRNG.setLayout(new GridLayout(4, 1));
        pRNG.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogAvgRms, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        ButtonGroup group = new ButtonGroup();
        jrb1 = new JRadioButton(TxtI.ti_scRa_DialogFourier);
        jrb1.setFont(TxtI.ti_Font_A);
        jrb1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(true);
                rngSc2.setEnabled(true);
                rngDf1.setEnabled(false);
                rngDf2.setEnabled(false);
                rngSl1.setEnabled(false);
                rngSl2.setEnabled(false);
                rngDf1.setEditable(false);
                rngDf2.setEditable(false);
                f1 = 1.0 / (xScope2 - xScope1);
                ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
            }
        });
        jrb2 = new JRadioButton(TxtI.ti_dfRa_DialogFourier);
        jrb2.setFont(TxtI.ti_Font_A);
        jrb2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(false);
                rngSc2.setEnabled(false);
                rngDf1.setEnabled(true);
                rngDf2.setEnabled(true);
                rngSl1.setEnabled(false);
                rngSl2.setEnabled(false);
                rngDf1.setEditable(true);
                rngDf2.setEditable(true);
                f1 = 1.0 / (xDef2 - xDef1);
                ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
            }
        });
        jrb3 = new JRadioButton(TxtI.ti_slRa_DialogFourier);
        jrb3.setFont(TxtI.ti_Font_A);
        jrb3.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                rngSc1.setEnabled(false);
                rngSc2.setEnabled(false);
                rngDf1.setEnabled(false);
                rngDf2.setEnabled(false);
                rngSl1.setEnabled(true);
                rngSl2.setEnabled(true);
                rngDf1.setEditable(false);
                rngDf2.setEditable(false);
            }
        });
        group.add(jrb1);
        group.add(jrb2);
        group.add(jrb3);
        //
        pRNG.add(jrb1);
        JPanel jpR1 = new JPanel();
        int maximumFractionDigits = 9;
        rngSc1 = new FormatJTextField(xScope1, maximumFractionDigits);
        rngSc1.setColumns(cols);
        rngSc2 = new FormatJTextField(xScope2, maximumFractionDigits);
        rngSc2.setColumns(cols);
        jpR1.add(rngSc1);
        jpR1.add(rngSc2);
        pRNG.add(jpR1);
        //
        pRNG.add(jrb2);
        JPanel jpR2 = new JPanel();
        rngDf1 = new FormatJTextField(xDef1);
        rngDf1.setColumns(cols);
        rngDf1.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                xDef1 = rngDf1.getNumberFromField();
                f1 = 1.0 / (xDef2 - xDef1);
                ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
            }
        });
        rngDf2 = new FormatJTextField(xDef2);
        rngDf2.setColumns(cols);
        rngDf2.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                xDef2 = rngDf2.getNumberFromField();
                f1 = 1.0 / (xDef2 - xDef1);
                ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
            }
        });
        jpR2.add(rngDf1);
        jpR2.add(rngDf2);
        pRNG.add(jpR2);
        //
        //pRNG.add(jrb3);
        JPanel jpR3 = new JPanel();
        rngSl1 = new FormatJTextField(xSlider1);
        rngSl1.setColumns(cols);
        rngSl2 = new FormatJTextField(xSlider2);
        rngSl2.setColumns(cols);
        jpR3.add(rngSl1);
        jpR3.add(rngSl2);
        //pRNG.add(jpR3);
        //
        //-------------------
        // Initialisieren -->
        jrb1.setSelected(true);
        rngSc1.setEditable(false);
        rngSc2.setEditable(false);
        rngDf1.setEditable(false);
        rngDf2.setEditable(false);
        rngSl1.setEditable(false);
        rngSl2.setEditable(false);
        //
        rngSc1.setEnabled(true);
        rngSc2.setEnabled(true);
        rngDf1.setEnabled(false);
        rngDf2.setEnabled(false);
        rngSl1.setEnabled(false);
        rngSl2.setEnabled(false);
        //===========================================================
        //===========================================================
        JPanel pHAR = new JPanel();
        pHAR.setLayout(new GridLayout(2, 2));
        pHAR.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogFourier, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        JLabel jlH2 = new JLabel("f_1 [Hz] =  ");
        jlH2.setFont(Typ.LAB_FONT_DIALOG_1);
        jlH2.setForeground(Typ.LAB_COLOR_DIALOG_1);
        pHAR.add(jlH2);
        //
        ftff1 = new FormatJTextField();
        ftff1.setText(cf.formatT((1.0 / (xScope2 - xScope1)), TechFormat.FORMAT_AUTO));
        ftff1.setEditable(false);
        pHAR.add(ftff1);
        //
        JLabel jlH3 = new JLabel("n_min =  ");
        jlH3.setFont(Typ.LAB_FONT_DIALOG_1);
        jlH3.setForeground(Typ.LAB_COLOR_DIALOG_1);
//        pHAR.add(jlH3);
        //
        ftfnMin = new FormatJTextField();
        ftfnMin.setText(nMin + "");
//        pHAR.add(ftfnMin);
        //
        JLabel jlH4 = new JLabel("n_max =  ");
        jlH4.setFont(Typ.LAB_FONT_DIALOG_1);
        jlH4.setForeground(Typ.LAB_COLOR_DIALOG_1);
        pHAR.add(jlH4);
        //
        ftfnMax = new FormatJTextField();
        ftfnMax.setText(nMax + "");
        pHAR.add(ftfnMax);
        //===========================================================
        //===========================================================
        JPanel pSEL = new JPanel();
        pSEL.setLayout(new GridBagLayout());
        pSEL.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x2_DialogFourier, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        jcbZV = new JCheckBox[header.length - 1];
        gbc.fill = gbc.BOTH;
        for (int i1 = 1; i1 < header.length; i1++) {
            gbc.gridx = 0;
            gbc.gridy = i1 - 1;
            jcbZV[i1 - 1] = new JCheckBox();
            if (i1 == 1) {
                jcbZV[i1 - 1].setSelected(true);  // default-init
            }
            jcbZV[i1 - 1].addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae) {
                }
            });
            pSEL.add(jcbZV[i1 - 1], gbc);
            //
            gbc.gridx = 1;
            gbc.gridy = i1 - 1;
            JLabel jlZV = new JLabel(header[i1]);
            jlZV.setFont(Typ.LAB_FONT_DIALOG_1);
            jlZV.setForeground(Typ.LAB_COLOR_DIALOG_1);
            pSEL.add(jlZV, gbc);
        }
        //===========================================================
        //===========================================================
        final JDialog ich = this;  // fuer Referenz in innerer Klasse
        //
        JPanel pOK = new JPanel();
        jbCALC = new JButton(TxtI.ti_calc_DialogFourier);
        jbCALC.setFont(TxtI.ti_Font_A);
        jbCALC.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                //----------------------------------
                if (jrb1.isSelected()) {
                    f1 = 1.0 / (xScope2 - xScope1);
                    ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
                    rng1 = xScope1;
                    rng2 = xScope2;
                } else if (jrb2.isSelected()) {
                    xDef1 = rngDf1.getNumberFromField();
                    xDef2 = rngDf2.getNumberFromField();
                    if (xDef1 >= xDef2) {
                        return;
                    } else if ((xDef1 < xScope1) || (xDef2 > xScope2)) {
                        return;
                    }
                    f1 = 1.0 / (xDef2 - xDef1);
                    ftff1.setText(cf.formatT(f1, TechFormat.FORMAT_AUTO));
                    rng1 = xDef1;
                    rng2 = xDef2;
                }
                nMin = 0;  // (int)ftfnMin.getNumberFromField();
                nMax = (int) ftfnMax.getNumberFromField();
                if ((nMin < 0) || (nMax < 0) || (nMin > nMax)) {
                    return;
                }
                if (f1 <= 0) {
                    return;
                }
                //----------------------------------
                Thread rechner = new Thread() {

                    private double[][][] erg;

                    public void run() {
                        jbCALC.setEnabled(false);  // damit man nicht mehrere Berechnungen durch versehentliches Druecken startet
                        try {
                            erg = calculate();
                            //-----------------
                            jbCALC.setEnabled(true);
                            //-----------------
                            // fertige Grafik nach Rechenende hochfahren ..
                            DialogFourierDiagramm diagramm = new DialogFourierDiagramm(
                                    erg, header, signalFourierAnalysiert, nMin, f1, worksheet, rng1, rng2,
                                    (int) ich.getLocation().getX(), (int) ich.getLocation().getY());
                            diagramm.setVisible(true);
                            //-----------------
                        } catch (java.lang.OutOfMemoryError er) {
                            JOptionPane.showMessageDialog(null,
                                    "Could not allocate enough memory for Fourier transformation!",
                                    "Memory error!",
                                    JOptionPane.ERROR_MESSAGE);
                        } catch (Error e0) {
                            e0.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                            jbCALC.setEnabled(true);
                        }
                    }
                };
                //----------------------------------
                rechner.setPriority(Thread.MIN_PRIORITY);
                rechner.start();
                //
            }
        });
        JButton jbCancel = new JButton(TxtI.ti_Cancel);
        jbCancel.setFont(TxtI.ti_Font_B);
        jbCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        pOK.add(jbCALC);
        pOK.add(jbCancel);
        JPanel jpCalc = new JPanel();
        jpCalc.setLayout(new BorderLayout());
        jpCalc.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_calc_DialogFourier, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        jpCalc.add(pOK, BorderLayout.SOUTH);
        //
        //===========================================================
        //===========================================================
        JPanel jpERGx = new JPanel();
        jpERGx.setLayout(new BorderLayout());
        jpERGx.add(pRNG, BorderLayout.NORTH);
        jpERGx.add(pHAR, BorderLayout.CENTER);
        //
        JPanel jpERGx2 = new JPanel();
        jpERGx2.setLayout(new BorderLayout());
        jpERGx2.add(pSEL, BorderLayout.NORTH);
        jpERGx2.add(jpCalc, BorderLayout.SOUTH);
        //
        JPanel pALL = new JPanel();
        pALL.setLayout(new BorderLayout());
        pALL.add(jpERGx, BorderLayout.WEST);
        pALL.add(jpERGx2, BorderLayout.EAST);
        con.add(pALL);
        //===========================================================
        //===========================================================
    }

    private double[][][] calculate() throws Exception, Error {  // eventuell OutOfMemoryError bei zuvielen Oberschwingungen
        //-------------------
        double[][] an = new double[header.length - 1][nMax - nMin + 1];
        double[][] bn = new double[header.length - 1][nMax - nMin + 1];
        double[][] cn = new double[header.length - 1][nMax - nMin + 1];  // Amplitude
        double[][] jn = new double[header.length - 1][nMax - nMin + 1];  // Winkel [rad]



        for (int i1 = 0; i1 < an.length; i1++) {
            for (int i2 = 0; i2 < an[0].length; i2++) {
                an[i1][i2] = 0;
                bn[i1][i2] = 0;
                cn[i1][i2] = 0;
                jn[i1][i2] = 0;
            }
        }
        // nur die selektierten Kurven werden auch wirklich Fourier-berechnet, die nicht-selektierten bleiben Null gesetzt
        //-------------------
        // Bereichsgrenze fuer Berechnung festlegen:
        rng1 = 0;
        rng2 = 1;
        if (jrb1.isSelected()) {
            rng1 = xScope1;
            rng2 = xScope2;
        } else if (jrb2.isSelected()) {
            rng1 = xDef1;
            rng2 = xDef2;
        } else if (jrb3.isSelected()) {
            rng1 = xSlider1;
            rng2 = xSlider2;
        }
        //-------------------
        // Startpunkt finden:
        int i1 = 0;
        while (worksheet.getValue(0, i1) <= rng1) {
            i1++;
        }
        int startIndex = i1;

        int stopIndex = 0;
        while ((i1 < worksheet.getColumnLength() - 1) && (worksheet.getValue(0, i1 + 1) > worksheet.getValue(0, i1))
                && (rng1 <= worksheet.getValue(0, i1)) && (worksheet.getValue(0, i1) <= rng2)) {  // Schleife Zeitbereich [t1...t2]
            stopIndex = i1;
            i1++;
        }

        int numberOfSamples = stopIndex - startIndex;
        NN = 1;
        while (NN < numberOfSamples) {
            NN *= 2;
        }

        if (NN > numberOfSamples) {
            NN /= 2;
        }

        float stopTime = (float) worksheet.getValue(0, stopIndex);
        float startTime = (float) worksheet.getValue(0, startIndex);

        double timeSpan = stopTime - startTime;


        i1 = startIndex;
        double dT = rng2 - rng1;
        double dt = worksheet.getValue(0, i1 + 1) - worksheet.getValue(0, i1);
        //-------------------
        // Rechnen bis zum Endpunkt:
        double q = 2 * Math.PI * f1;  // Hilfskonstante
        while ((i1 < worksheet.getColumnLength() - 1) && (worksheet.getValue(0, i1 + 1) > worksheet.getValue(0, i1))
                && (rng1 <= worksheet.getValue(0, i1)) && (worksheet.getValue(0, i1) <= rng2)) {  // Schleife Zeitbereich [t1...t2]
            try {
                dt = worksheet.getValue(0, i1 + 1) - worksheet.getValue(0, i1);
            } catch (Exception e) {
            }  // wenn wir ganz am Ende sind --> Exception --> altes 'dt' wird verwendet
            for (int i2 = 1; i2 < header.length; i2++) {  // Schleife ueber alle Fourier-zu-zerlegenden Kurven
                if (jcbZV[i2 - 1].isSelected()) {
                    signalFourierAnalysiert[i2] = true;
                    double wert = worksheet.getValue(i2, i1);
                    double arg = q * worksheet.getValue(0, i1);
//                    for (int n = nMin; n <= nMax; n++) {  // Schleife ueber alle Grundfrequenz-Vielfachen [nMin...nMax]
//                        an[i2 - 1][n - nMin] += (wert * Math.cos(arg * n) * dt) / (0.5 * dT);
//                        bn[i2 - 1][n - nMin] += (wert * Math.sin(arg * n) * dt) / (0.5 * dT);
//                    }
                } else {
                    signalFourierAnalysiert[i2] = false;
                }
            }
            i1++;
        }

        for (int i2 = 1; i2 < header.length; i2++) {  // Schleife ueber alle Fourier-zu-zerlegenden Kurven
            if (jcbZV[i2 - 1].isSelected()) {
                float[] data = new float[NN + 1];
                int j = startIndex;
                for (int i = 0; i < NN; i++) {
                    while (worksheet.getValue(0, j) < startTime + i * timeSpan / NN) {
                        j++;
                    }
                    data[i + 1] = (float) worksheet.getValue(i2, j);
                }

                FFTNew.realft(data, 1);
                for (int n = nMin; n <= nMax; n++) {
                    an[i2 - 1][n - nMin] = data[2 * n + 1] / (NN / 2);
                    bn[i2 - 1][n - nMin] = data[2 * n + 2] / (NN / 2);
                }
            }
        }
        
        //-------------------
        // Auswertung:

        for (int i2 = 1; i2 < header.length; i2++) {
            for (int n = nMin; n <= nMax; n++) {

                if (n == 0) {  // DC-Gleichanteil
                    cn[i2 - 1][n - nMin] = 0.5 * an[i2 - 1][n - nMin];
                    jn[i2 - 1][n - nMin] = 0;
                } else {
                    cn[i2 - 1][n - nMin] = Math.sqrt(an[i2 - 1][n - nMin] * an[i2 - 1][n - nMin] + bn[i2 - 1][n - nMin] * bn[i2 - 1][n - nMin]);
                    jn[i2 - 1][n - nMin] = Math.atan2(an[i2 - 1][n - nMin], bn[i2 - 1][n - nMin]);
                }
            }
        }


        double[][][] erg = new double[4][][];
        erg[0] = an;
        erg[1] = bn;
        erg[2] = cn;
        erg[3] = jn;
        return erg;
    }
}
