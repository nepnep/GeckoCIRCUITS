/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JDialog; 
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import java.net.URL;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;




public class DialogCurveProperties extends JDialog implements WindowListener {


    //----------------------------
    private DialogConnectSignalsGraphs connectionMatrixFenster;
    private GraferImplementation grafer;  // callback fuer Properties-Zugriff
    private Container c;
    private JButton jbOK, jbCa, jbAp;
    private GridBagConstraints gbc= new GridBagConstraints();
    //
    private int im1, im2;  // Indizes (im1,im2) der Zuordnungsmatrix
    //----------------------------
    // Lokal zu speichernde Groessen --> Weiterleitung duch 'Apply'-Button:
    private String signalNamen;
    //----------------------------
    private FormatJTextField jtfNameKURV;  // wird zugewiesen --> nicht editierbar!
    private JComboBox jcbAchsenTyp;  // X oder Y oder Y2 ??
    private JComboBox jcbLineStyle, jcbLineColor;
    private FormatJTextField jtfSymbFrequ;
    private JCheckBox jcbSymbShow;
    private JComboBox jcmSymbShape, jcbSymbColor;
    private JComboBox jcmClipXmin, jcmClipXmax, jcmClipYmin, jcmClipYmax;
    private FormatJTextField jcmClipValXmin, jcmClipValXmax, jcmClipValYmin, jcmClipValYmax;
    //----------------------------
    // lokal gespeicherte Parameter dieses Dialog-Fensters, die in einem Stueck bei 'Apply' oder 'OK' uebergeben werden -->
    private String _jtfNameKURV;
    private int _jcbAchsenTyp;
    private int _jcbLineStyle, _jcbLineColor;
    private double _curveTransparency;
    private boolean _jcbSymbShow;
    private int _jtfSymbFrequ, _jcmSymbShape, _jcbSymbColor;
    private int _jcmClipXmin, _jcmClipXmax, _jcmClipYmin, _jcmClipYmax;
    private double _jcmClipValXmin, _jcmClipValXmax, _jcmClipValYmin, _jcmClipValYmax;
    //----------------------------



    public DialogCurveProperties (DialogConnectSignalsGraphs conF, GraferImplementation grafer, int im1, int im2) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        this.connectionMatrixFenster= conF;
        this.grafer= grafer;
        this.im1= im1-1;
        this.im2= im2-1;
        //--------------------
        // this.setLocation(230,230);
        this.setTitle(Typ.spTitle+"Curve Properties");
        this.addWindowListener(this);
        c= this.getContentPane();
        c.setLayout(new BorderLayout());
        //--------------------
        c.removeAll();
        this.initPropertiesZVmodus();
        this.baueGUI();
        this.pack();
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(grafer.getScope().getX_PositionAbsolut_DialogConnect()+Typ.DX_SCOPE_DLG2, grafer.getScope().getY_PositionAbsolut_DialogConnect()+Typ.DY_SCOPE_DLG2);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setResizable(false);
        this.setVisible(true);
        //--------------------
    }



    private void initPropertiesZVmodus () {
        //----------------------------
        // Initialisierung der lokal gespeicherte Parameter dieses Dialog-Fensters
        //
        _jtfNameKURV= grafer.signalNamen[im2];
        //
        _jcbAchsenTyp= grafer.get_crvAchsenTyp(im1,im2);
//        _jcbAchsenTyp= grafer.crvAchsenTyp[im1][im2];
        _jcbLineStyle= grafer.crvLineStyle[im1][im2];
        _jcbLineColor= grafer.crvLineColor[im1][im2];
        _curveTransparency = grafer.crvLineTransparency[im1][im2];
        //
        _jcbSymbShow= grafer.crvSymbShow[im1][im2];
        _jtfSymbFrequ= grafer.crvSymbFrequ[im1][im2];
        _jcmSymbShape= grafer.crvSymbShape[im1][im2];
        _jcbSymbColor= grafer.crvSymbColor[im1][im2];
        //
        _jcmClipXmin= grafer.crvClipXmin[im1][im2];
        _jcmClipXmax= grafer.crvClipXmax[im1][im2];
        _jcmClipYmin= grafer.crvClipYmin[im1][im2];
        _jcmClipYmax= grafer.crvClipYmax[im1][im2];
        //
        //----------------------------
        switch (_jcmClipXmin) {
            case GraferV3.CLIP_NO:     _jcmClipValXmin= grafer.getXClip_NO(im1,im2)[0];  break;
            case GraferV3.CLIP_ACHSE:  _jcmClipValXmin= grafer.getXClip_ACHSE(im1,im2)[0];  break;
            case GraferV3.CLIP_VALUE:  _jcmClipValXmin= grafer.crvClipValXmin[im1][im2];  break;
            default: break;
        }
        switch (_jcmClipXmax) {
            case GraferV3.CLIP_NO:     _jcmClipValXmax= grafer.getXClip_NO(im1,im2)[1];  break;
            case GraferV3.CLIP_ACHSE:  _jcmClipValXmax= grafer.getXClip_ACHSE(im1,im2)[1];  break;
            case GraferV3.CLIP_VALUE:  _jcmClipValXmax= grafer.crvClipValXmax[im1][im2];  break;
            default: break;
        }
        switch (_jcmClipYmin) {
            case GraferV3.CLIP_NO:     _jcmClipValYmin= grafer.getYClip_NO(im1,im2)[0];  break;
            case GraferV3.CLIP_ACHSE:  _jcmClipValYmin= grafer.getYClip_ACHSE(im1,im2)[0];  break;
            case GraferV3.CLIP_VALUE:  _jcmClipValYmin= grafer.crvClipValYmin[im1][im2];  break;
            default: break;
        }
        switch (_jcmClipYmax) {
            case GraferV3.CLIP_NO:     _jcmClipValYmax= grafer.getYClip_NO(im1,im2)[1];  break;
            case GraferV3.CLIP_ACHSE:  _jcmClipValYmax= grafer.getYClip_ACHSE(im1,im2)[1];  break;
            case GraferV3.CLIP_VALUE:  _jcmClipValYmax= grafer.crvClipValYmax[im1][im2];  break;
            default: break;
        }
        //----------------------------
    }



    private void baueGUI () {
        //================================================
        c.add(this.getKurvenTab(), BorderLayout.CENTER);
        //
        JPanel jpOK= new JPanel();
        jbOK= new JButton(TxtI.ti_OK);
        jbOK.setFont(TxtI.ti_Font_A); 
        jbCa= new JButton(TxtI.ti_Cancel);
        jbCa.setFont(TxtI.ti_Font_A); 
        jbAp= new JButton(TxtI.ti_apply_DialogCheckSimulation);
        jbAp.setFont(TxtI.ti_Font_A); 
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { bestaetige();  dispose(); }
        });
        jbCa.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { dispose(); }
        });
        jbAp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) { bestaetige(); }
        });
        jpOK.add(jbOK);
        jpOK.add(jbCa);
        jpOK.add(jbAp);
        c.add(jpOK, BorderLayout.SOUTH);
        //================================================
    }




    private JPanel getKurvenTab () {
        String plenk= "  ";  // dx-Abstand
        int colsNumber=6;
        //==========================================================================================
        // (0) Allgemeines --> Achsentyp (X/Y/Y2) ? -->
        JPanel jpALLG= new JPanel();
        jpALLG.setLayout(new GridBagLayout());
        jpALLG.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x1_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=0;   gbc.gridy=1;   gbc.fill=gbc.BOTH;
        JLabel jlu1= new JLabel(TxtI.ti_cur_DialogCurveProperties+" :  "); 
        jlu1.setFont(TxtI.ti_Font_B); 
        jpALLG.add(jlu1, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfNameKURV= new FormatJTextField();
        jtfNameKURV.setText(_jtfNameKURV);
        jtfNameKURV.setEditable(false);
        jtfNameKURV.setBackground(Color.white);
        jtfNameKURV.setForeground(Color.darkGray);
        jpALLG.add(jtfNameKURV, gbc);
        //
        gbc.gridx=0;   gbc.gridy=2;
        JLabel jlu2= new JLabel(TxtI.ti_ax_DialogCurveProperties+" :  "); 
        jlu2.setFont(TxtI.ti_Font_B); 
        jpALLG.add(jlu2, gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcbAchsenTyp= new JComboBox(new String[]{"X", "Y", "INACTIVE"});
        jcbAchsenTyp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                //------------
                boolean aktiviere= true;
                switch (jcbAchsenTyp.getSelectedIndex()) {
                    case 0:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_X;    aktiviere= false;  break;  // alten X-Wert aus der Matrix loeschen (es kann nur genau 1 X pro Zeile geben) -->
                    case 1:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_Y;    aktiviere= true;   break;
                    case 2:  _jcbAchsenTyp= GraferImplementation.ZUORDNUNG_NIX;  aktiviere= false;  break;
                    case 3:  // GraferImplementation.ZUORDNUNG_SIGNAL  break;
                    default: System.out.println("Fehler: ouert45gn8349"); 
                }
                //------------
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(aktiviere);    jcbLineColor.setEnabled(aktiviere);
                jcbSymbShow.setEnabled(aktiviere);     jtfSymbFrequ.setEnabled(aktiviere);    jcmSymbShape.setEnabled(aktiviere);    jcbSymbColor.setEnabled(aktiviere);
                jcmClipXmin.setEnabled(aktiviere);     jcmClipXmax.setEnabled(aktiviere);     jcmClipYmin.setEnabled(aktiviere);     jcmClipYmax.setEnabled(aktiviere);
                if (_jcmClipXmin!=GraferV3.CLIP_VALUE) jcmClipValXmin.setEnabled(false); else jcmClipValXmin.setEnabled(aktiviere);
                if (_jcmClipXmax!=GraferV3.CLIP_VALUE) jcmClipValXmax.setEnabled(false); else jcmClipValXmax.setEnabled(aktiviere);
                if (_jcmClipYmin!=GraferV3.CLIP_VALUE) jcmClipValYmin.setEnabled(false); else jcmClipValYmin.setEnabled(aktiviere);
                if (_jcmClipYmax!=GraferV3.CLIP_VALUE) jcmClipValYmax.setEnabled(false); else jcmClipValYmax.setEnabled(aktiviere);
                // Verknuepfungsmartix aktualisieren -->
                connectionMatrixFenster.setzeMatrixEintrag(im1,im2,_jcbAchsenTyp);
                //------------
                bestaetige();
                //------------
            }
        });
        jpALLG.add(jcbAchsenTyp, gbc);
        //==========================================================================================
        // (1) Linien-Properties:
        JPanel jpLINE= new JPanel();
        jpLINE.setLayout(new GridBagLayout());
        jpLINE.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x2_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=0;   gbc.gridy=0;   gbc.fill=gbc.BOTH;
        JLabel jlu3= new JLabel(TxtI.ti_sty_DialogCurveProperties+" : ");
        jlu3.setFont(TxtI.ti_Font_B); 
        jpLINE.add(jlu3, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jcbLineStyle= new JComboBox(GraferV3.LINIEN_STIL);
        jcbLineStyle.setSelectedIndex(this.setzeLinienstilListe(_jcbLineStyle));
        jcbLineStyle.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbLineStyle= setzeLinienstilSelektiert(jcbLineStyle.getSelectedIndex());
                bestaetige();
            }
        });
        jpLINE.add(jcbLineStyle, gbc);
        //
        gbc.gridx=0;   gbc.gridy=1;
        JLabel jlu4= new JLabel(TxtI.ti_col_DialogCurveProperties+" :  "); 
        jlu4.setFont(TxtI.ti_Font_B); 
        jpLINE.add(jlu4, gbc);

        gbc.gridx=0;   gbc.gridy=2;
        JLabel jluTrans= new JLabel("Curve transparency :  ");
        jluTrans.setFont(TxtI.ti_Font_B);
        jpLINE.add(jluTrans, gbc);

        if(_curveTransparency < 0.1) {
            _curveTransparency = 0.5;
        }

        if(_curveTransparency > 1) {
            _curveTransparency = 1;
        }

        final SpinnerModel model = new SpinnerNumberModel(_curveTransparency, //initial value
                               0.1, //min value 0.1, otherwise a curve may be "disappeared"!
                               1, //max
                               0.1);                //step

        model.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                _curveTransparency = (Double) model.getValue();
            }
        });

        JSpinner transSpinner = new JSpinner(model);

        gbc.gridx=1;   gbc.gridy=2;
        jpLINE.add(transSpinner, gbc);

        gbc.gridx=1;   gbc.gridy=1;
        jcbLineColor= new JComboBox(GraferV3.FARBEN);
        jcbLineColor.setSelectedIndex(this.setzeFarbListe(_jcbLineColor));
        jcbLineColor.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbLineColor= setzeFarbeSelektiert(jcbLineColor.getSelectedIndex());
                bestaetige();
            }
        });
        jpLINE.add(jcbLineColor, gbc);
        //
        //==========================================================================================
        // (2) Symbol-Properties:
        JPanel jpSYMB= new JPanel();
        jpSYMB.setLayout(new GridBagLayout());
        jpSYMB.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x3_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=1;   gbc.gridy=0;
        JLabel jlu5= new JLabel(TxtI.ti_shsy_DialogCurveProperties+"  "); 
        jlu5.setFont(TxtI.ti_Font_B); 
        jpSYMB.add(jlu5, gbc);
        gbc.gridx=0;   gbc.gridy=0;
        jcbSymbShow= new JCheckBox();
        jcbSymbShow.setSelected(_jcbSymbShow);
        jcbSymbShow.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbSymbShow= jcbSymbShow.isSelected();
                if (_jcbSymbShow) {
                    jtfSymbFrequ.setEnabled(true);   jcmSymbShape.setEnabled(true);   jcbSymbColor.setEnabled(true);
                } else {
                    jtfSymbFrequ.setEnabled(false);   jcmSymbShape.setEnabled(false);   jcbSymbColor.setEnabled(false);
                }
                bestaetige();
            }
        });
        jpSYMB.add(jcbSymbShow, gbc);
        gbc.fill=gbc.BOTH;
        //
        gbc.gridx=0;   gbc.gridy=1;
        JLabel jlu6= new JLabel(TxtI.ti_frq_DialogCurveProperties+" :  "); 
        jlu6.setFont(TxtI.ti_Font_B); 
        jpSYMB.add(jlu6, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfSymbFrequ= new FormatJTextField();
        jtfSymbFrequ.setColumns(5);
        //if (_jcbXautoScale) jtfSymbFrequ.setEnabled(false);
        jtfSymbFrequ.setNumberToField(_jtfSymbFrequ);
        jtfSymbFrequ.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfSymbFrequ= (int)jtfSymbFrequ.getNumberFromField();
                    if (_jtfSymbFrequ<0) _jtfSymbFrequ=0;  // keine negative Symbol-Frequenz
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpSYMB.add(jtfSymbFrequ, gbc);
        //
        gbc.gridx=0;   gbc.gridy=2;
        JLabel jlu7= new JLabel(TxtI.ti_shp_DialogCurveProperties+" :  "); 
        jlu7.setFont(TxtI.ti_Font_B); 
        jpSYMB.add(jlu7, gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmSymbShape= new JComboBox();
        jcmSymbShape= new JComboBox(GraferV3.SYMBOLSHAPE);
        switch (_jcmSymbShape) {
            case GraferV3.SYBM_CIRCLE:         jcmSymbShape.setSelectedIndex(0);   break;
            case GraferV3.SYBM_CIRCLE_FILLED:  jcmSymbShape.setSelectedIndex(1);   break;
            case GraferV3.SYBM_CROSS:          jcmSymbShape.setSelectedIndex(2);   break;
            case GraferV3.SYBM_RECT:           jcmSymbShape.setSelectedIndex(3);   break;
            case GraferV3.SYBM_RECT_FILLED:    jcmSymbShape.setSelectedIndex(4);   break;
            case GraferV3.SYBM_TRIANG:         jcmSymbShape.setSelectedIndex(5);   break;
            case GraferV3.SYBM_TRIANG_FILLED:  jcmSymbShape.setSelectedIndex(6);   break;
            default:  System.out.println("Fehler: hrhr454544"); 
        }
        jcmSymbShape.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                switch (jcmSymbShape.getSelectedIndex()) {
                    case  0:  _jcmSymbShape= GraferV3.SYBM_CIRCLE;   break;
                    case  1:  _jcmSymbShape= GraferV3.SYBM_CIRCLE_FILLED;   break;
                    case  2:  _jcmSymbShape= GraferV3.SYBM_CROSS;   break;
                    case  3:  _jcmSymbShape= GraferV3.SYBM_RECT;   break;
                    case  4:  _jcmSymbShape= GraferV3.SYBM_RECT_FILLED;   break;
                    case  5:  _jcmSymbShape= GraferV3.SYBM_TRIANG;   break;
                    case  6:  _jcmSymbShape= GraferV3.SYBM_TRIANG_FILLED;   break;
                    default:  System.out.println("Fehler: mkefi93"); 
                }
                bestaetige();
            }
        });
        jpSYMB.add(jcmSymbShape, gbc);
        //
        gbc.gridx=0;   gbc.gridy=3;
        JLabel jlu8= new JLabel(TxtI.ti_col_DialogCurveProperties+" :  "); 
        jlu8.setFont(TxtI.ti_Font_B); 
        jpSYMB.add(jlu8, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jcbSymbColor= new JComboBox(GraferV3.FARBEN);
        jcbSymbColor.setSelectedIndex(this.setzeFarbListe(_jcbSymbColor));
        jcbSymbColor.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcbSymbColor= setzeFarbeSelektiert(jcbSymbColor.getSelectedIndex());
                bestaetige();
            }
        });
        jpSYMB.add(jcbSymbColor, gbc);
        //
        //==========================================================================================
        // (3) Clipping-Properties:
        JPanel jpCLIP= new JPanel();
        jpCLIP.setLayout(new GridBagLayout());
        jpCLIP.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x4_DialogCurveProperties, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        //
        gbc.gridx=0;   gbc.gridy=0;
        jpCLIP.add(new JLabel(" xMIN :  "), gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jcmClipXmin= new JComboBox(GraferV3.CLIPPING);
        jcmClipXmin.setSelectedIndex(this.setzeClippingListe(_jcmClipXmin));
        jcmClipXmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmClipXmin= setzeClippingSelektiert(jcmClipXmin.getSelectedIndex());
                switch (_jcmClipXmin) {
                    case GraferV3.CLIP_NO:
                        _jcmClipValXmin= grafer.getXClip_NO(im1,im2)[0];
                        jcmClipValXmin.setNumberToField(_jcmClipValXmin);
                        jcmClipValXmin.setEnabled(false);
                        break;
                    case GraferV3.CLIP_ACHSE:
                        _jcmClipValXmin= grafer.getXClip_ACHSE(im1,im2)[0];
                        jcmClipValXmin.setNumberToField(_jcmClipValXmin);
                        jcmClipValXmin.setEnabled(false);
                        break;
                    case GraferV3.CLIP_VALUE:
                        jcmClipValXmin.setEnabled(true);
                        break;
                    default: System.out.println("Fehler: 9nuwf92");  break;
                }
                bestaetige();
            }
        });
        jpCLIP.add(jcmClipXmin, gbc);
        gbc.gridx=2;   gbc.gridy=0;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=0;
        jcmClipValXmin= new FormatJTextField();
        jcmClipValXmin.setColumns(colsNumber);
        //if (_jcbXautoScale) jcmClipValXmin.setEnabled(false);
        jcmClipValXmin.setNumberToField(_jcmClipValXmin);
        jcmClipValXmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jcmClipValXmin= jcmClipValXmin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpCLIP.add(jcmClipValXmin, gbc);
        gbc.gridx=4;   gbc.gridy=0;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        //
        gbc.gridx=0;   gbc.gridy=1;
        jpCLIP.add(new JLabel(" xMAX :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcmClipXmax= new JComboBox(GraferV3.CLIPPING);
        jcmClipXmax.setSelectedIndex(this.setzeClippingListe(_jcmClipXmax));
        jcmClipXmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmClipXmax= setzeClippingSelektiert(jcmClipXmax.getSelectedIndex());
                switch (_jcmClipXmax) {
                    case GraferV3.CLIP_NO:
                        _jcmClipValXmax= grafer.getXClip_NO(im1,im2)[1];
                        jcmClipValXmax.setNumberToField(_jcmClipValXmax);
                        jcmClipValXmax.setEnabled(false);
                        break;
                    case GraferV3.CLIP_ACHSE:
                        _jcmClipValXmax= grafer.getXClip_ACHSE(im1,im2)[1];
                        jcmClipValXmax.setNumberToField(_jcmClipValXmax);
                        jcmClipValXmax.setEnabled(false);
                        break;
                    case GraferV3.CLIP_VALUE:
                        jcmClipValXmax.setEnabled(true);
                        break;
                    default: System.out.println("Fehler: 9j3493g");  break;
                }
                bestaetige();
            }
        });
        jpCLIP.add(jcmClipXmax, gbc);
        gbc.gridx=2;   gbc.gridy=1;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=1;
        jcmClipValXmax= new FormatJTextField();
        jcmClipValXmax.setColumns(colsNumber);
        //if (_jcbXautoScale) jcmClipValXmax.setEnabled(false);
        jcmClipValXmax.setNumberToField(_jcmClipValXmax);
        jcmClipValXmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jcmClipValXmax= jcmClipValXmax.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpCLIP.add(jcmClipValXmax, gbc);
        gbc.gridx=4;   gbc.gridy=1;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        //
        gbc.gridx=0;   gbc.gridy=2;
        jpCLIP.add(new JLabel(" yMIN :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmClipYmin= new JComboBox(GraferV3.CLIPPING);
        jcmClipYmin.setSelectedIndex(this.setzeClippingListe(_jcmClipYmin));
        jcmClipYmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmClipYmin= setzeClippingSelektiert(jcmClipYmin.getSelectedIndex());
                switch (_jcmClipYmin) {
                    case GraferV3.CLIP_NO:
                        _jcmClipValYmin= grafer.getYClip_NO(im1,im2)[0];
                        jcmClipValYmin.setNumberToField(_jcmClipValYmin);
                        jcmClipValYmin.setEnabled(false);
                        break;
                    case GraferV3.CLIP_ACHSE:
                        _jcmClipValYmin= grafer.getYClip_ACHSE(im1,im2)[0];
                        jcmClipValYmin.setNumberToField(_jcmClipValYmin);
                        jcmClipValYmin.setEnabled(false);
                        break;
                    case GraferV3.CLIP_VALUE:
                        jcmClipValYmin.setEnabled(true);
                        break;
                    default: System.out.println("Fehler: j9uwfe9j0wg");  break;
                }
                bestaetige();
            }
        });
        jpCLIP.add(jcmClipYmin, gbc);
        gbc.gridx=2;   gbc.gridy=2;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=2;
        jcmClipValYmin= new FormatJTextField();
        jcmClipValYmin.setColumns(colsNumber);
        //if (_jcbXautoScale) jcmClipValYmin.setEnabled(false);
        jcmClipValYmin.setNumberToField(_jcmClipValYmin);
        jcmClipValYmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jcmClipValYmin= jcmClipValYmin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpCLIP.add(jcmClipValYmin, gbc);
        gbc.gridx=4;   gbc.gridy=2;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        //
        gbc.gridx=0;   gbc.gridy=3;
        jpCLIP.add(new JLabel(" yMAX :  "), gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jcmClipYmax= new JComboBox(GraferV3.CLIPPING);
        jcmClipYmax.setSelectedIndex(this.setzeClippingListe(_jcmClipYmax));
        jcmClipYmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmClipYmax= setzeClippingSelektiert(jcmClipYmax.getSelectedIndex());
                switch (_jcmClipYmax) {
                    case GraferV3.CLIP_NO:
                        _jcmClipValYmax= grafer.getYClip_NO(im1,im2)[1];
                        jcmClipValYmax.setNumberToField(_jcmClipValYmax);
                        jcmClipValYmax.setEnabled(false);
                        break;
                    case GraferV3.CLIP_ACHSE:
                        _jcmClipValYmax= grafer.getYClip_ACHSE(im1,im2)[1];
                        jcmClipValYmax.setNumberToField(_jcmClipValYmax);
                        jcmClipValYmax.setEnabled(false);
                        break;
                    case GraferV3.CLIP_VALUE:
                        jcmClipValYmax.setEnabled(true);
                        break;
                    default: System.out.println("Fehler: tjkej55");  break;
                }
                bestaetige();
            }
        });
        jpCLIP.add(jcmClipYmax, gbc);
        gbc.gridx=2;   gbc.gridy=3;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=3;
        jcmClipValYmax= new FormatJTextField();
        jcmClipValYmax.setColumns(colsNumber);
        //if (_jcbXautoScale) jcmClipValYmax.setEnabled(false);
        jcmClipValYmax.setNumberToField(_jcmClipValYmax);
        jcmClipValYmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jcmClipValYmax= jcmClipValYmax.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpCLIP.add(jcmClipValYmax, gbc);
        gbc.gridx=4;   gbc.gridy=3;
        jpCLIP.add(new JLabel(plenk), gbc);  // dx-Abstand
        //
        //-------------------------------
        //-------------------------------
        //
        // anschliessende Initialisierung:
        //
        switch (_jcbAchsenTyp) {
            case GraferImplementation.ZUORDNUNG_X:
                jcbAchsenTyp.setSelectedIndex(0);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(false);    jcbLineColor.setEnabled(false);
                jcbSymbShow.setEnabled(false);     jtfSymbFrequ.setEnabled(false);    jcmSymbShape.setEnabled(false);    jcbSymbColor.setEnabled(false);
                jcmClipXmin.setEnabled(false);     jcmClipXmax.setEnabled(false);     jcmClipYmin.setEnabled(false);     jcmClipYmax.setEnabled(false);
                jcmClipValXmin.setEnabled(false);  jcmClipValXmax.setEnabled(false);  jcmClipValYmin.setEnabled(false);  jcmClipValYmax.setEnabled(false);
                break;
            case GraferImplementation.ZUORDNUNG_Y:
                jcbAchsenTyp.setSelectedIndex(1);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(true);    jcbLineColor.setEnabled(true);
                jcbSymbShow.setEnabled(true);     jtfSymbFrequ.setEnabled(true);    jcmSymbShape.setEnabled(true);    jcbSymbColor.setEnabled(true);
                jcmClipXmin.setEnabled(true);     jcmClipXmax.setEnabled(true);     jcmClipYmin.setEnabled(true);     jcmClipYmax.setEnabled(true);
                jcmClipValXmin.setEnabled(true);  jcmClipValXmax.setEnabled(true);  jcmClipValYmin.setEnabled(true);  jcmClipValYmax.setEnabled(true);
                break;
            case GraferImplementation.ZUORDNUNG_NIX:
                jcbAchsenTyp.setSelectedIndex(2);
                // alles in diesem Fenster AKTIV oder INAKTIV schalten -->
                jcbLineStyle.setEnabled(false);    jcbLineColor.setEnabled(false);
                jcbSymbShow.setEnabled(false);     jtfSymbFrequ.setEnabled(false);    jcmSymbShape.setEnabled(false);    jcbSymbColor.setEnabled(false);
                jcmClipXmin.setEnabled(false);     jcmClipXmax.setEnabled(false);     jcmClipYmin.setEnabled(false);     jcmClipYmax.setEnabled(false);
                jcmClipValXmin.setEnabled(false);  jcmClipValXmax.setEnabled(false);  jcmClipValYmin.setEnabled(false);  jcmClipValYmax.setEnabled(false);
                break;
            case GraferImplementation.ZUORDNUNG_SIGNAL:  break;
            default: System.out.println("Fehler: ouerkki8349"); 
        }
        if (!_jcbSymbShow) {
            jtfSymbFrequ.setEnabled(false);   jcmSymbShape.setEnabled(false);   jcbSymbColor.setEnabled(false);
        } else {
            jtfSymbFrequ.setEnabled(true);    jcmSymbShape.setEnabled(true);    jcbSymbColor.setEnabled(true);
        }
        switch (_jcmClipXmin) {
            case GraferV3.CLIP_NO:
                jcmClipValXmin.setEnabled(false); //  jcmClipValXmin.setText("0");
                break;
            case GraferV3.CLIP_ACHSE:
                jcmClipValXmin.setEnabled(false);
                break;
            case GraferV3.CLIP_VALUE:
                jcmClipValXmin.setEnabled(true);
                break;
            default: System.out.println("Fehler: rthjj");  break;
        }
        switch (_jcmClipXmax) {
            case GraferV3.CLIP_NO:
                jcmClipValXmax.setEnabled(false);  // jcmClipValXmax.setText("0");
                break;
            case GraferV3.CLIP_ACHSE:
                jcmClipValXmax.setEnabled(false);
                break;
            case GraferV3.CLIP_VALUE:
                jcmClipValXmax.setEnabled(true);
                break;
            default: System.out.println("Fehler: jrzuu45u");  break;
        }
        switch (_jcmClipYmin) {
            case GraferV3.CLIP_NO:
                jcmClipValYmin.setEnabled(false);  // jcmClipValYmin.setText("0");
                break;
            case GraferV3.CLIP_ACHSE:
                jcmClipValYmin.setEnabled(false);
                break;
            case GraferV3.CLIP_VALUE:
                jcmClipValYmin.setEnabled(true);
                break;
            default: System.out.println("Fehler: jtj546uu4");  break;
        }
        switch (_jcmClipYmax) {
            case GraferV3.CLIP_NO:
                jcmClipValYmax.setEnabled(false);  // jcmClipValYmax.setText("0");
                break;
            case GraferV3.CLIP_ACHSE:
                jcmClipValYmax.setEnabled(false);
                break;
            case GraferV3.CLIP_VALUE:
                jcmClipValYmax.setEnabled(true);
                break;
            default: System.out.println("Fehler: jwuj7i568");  break;
        }
        //
        //==========================================================================================
        // Gesamt-Panel -->
        JPanel pGes= new JPanel();
        pGes.setLayout(new GridBagLayout());
        //
        // alle drei JPanels sind gleich hoch (angepasst an 'jpCLIP'), aber flexibel in der Breite -->
        double h= jpCLIP.getPreferredSize().getHeight();
        jpLINE.setPreferredSize(new Dimension((int)jpLINE.getPreferredSize().getWidth(), (int)h));
        jpSYMB.setPreferredSize(new Dimension((int)jpSYMB.getPreferredSize().getWidth(), (int)h));
        //
        gbc.gridx=0;   gbc.gridy=0; 
        pGes.add(jpLINE, gbc);
        gbc.gridx=1;   gbc.gridy=0; 
        pGes.add(jpSYMB, gbc);
        gbc.gridx=0;   gbc.gridy=1; 
        //pGes.add(jpALLG, gbc);
        gbc.gridx=1;   gbc.gridy=1; 
        //pGes.add(jpCLIP, gbc);
        //
        return pGes;
        //--------------------
    }




    private int setzeFarbListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.BLACK:     return  0;
            case GraferV3.RED:       return  1;
            case GraferV3.GREEN:     return  2;
            case GraferV3.BLUE:      return  3;
            case GraferV3.DARKGRAY:  return  4;
            case GraferV3.GRAY:      return  5;
            case GraferV3.LIGTHGRAY: return  6;
            case GraferV3.WHITE:     return  7;
            case GraferV3.MAGENTA:   return  8;
            case GraferV3.CYAN:      return  9;
            case GraferV3.ORANGE:    return 10;
            case GraferV3.YELLOW:    return 11;
            case GraferV3.DARKGREEN: return 12;
            default:
                System.out.println("Fehler: rthjrzj5"); 
                return -1;
        }
        //--------------------
    }


    private int setzeFarbeSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.BLACK;
            case  1:  return GraferV3.RED;
            case  2:  return GraferV3.GREEN;
            case  3:  return GraferV3.BLUE;
            case  4:  return GraferV3.DARKGRAY;
            case  5:  return GraferV3.GRAY;
            case  6:  return GraferV3.LIGTHGRAY;
            case  7:  return GraferV3.WHITE;
            case  8:  return GraferV3.MAGENTA;
            case  9:  return GraferV3.CYAN;
            case 10:  return GraferV3.ORANGE;
            case 11:  return GraferV3.YELLOW;
            case 12:  return GraferV3.DARKGREEN;
            default:
                System.out.println("Fehler: drdzmndzn"); 
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.SOLID_PLAIN:  return  0;
            case GraferV3.INVISIBLE:    return  1;
            case GraferV3.SOLID_FAT_1:  return  2;
            case GraferV3.SOLID_FAT_2:  return  3;
            case GraferV3.DOTTED_PLAIN: return  4;
            case GraferV3.DOTTED_FAT:   return  5;
            default:
                System.out.println("Fehler: 67j65h");
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.SOLID_PLAIN;
            case  1:  return GraferV3.INVISIBLE;
            case  2:  return GraferV3.SOLID_FAT_1;
            case  3:  return GraferV3.SOLID_FAT_2;
            case  4:  return GraferV3.DOTTED_PLAIN;
            case  5:  return GraferV3.DOTTED_FAT;
            default:
                System.out.println("Fehler: 34t6ujsd"); 
                return -1;
        }
        //--------------------
    }


    private int setzeClippingListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.CLIP_ACHSE:  return  0;
            case GraferV3.CLIP_NO:     return  1;
            case GraferV3.CLIP_VALUE:  return  2;
            default:
                System.out.println("Fehler: dgkdkjdjkdd"); 
                return -1;
        }
        //--------------------
    }


    private int setzeClippingSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.CLIP_ACHSE;
            case  1:  return GraferV3.CLIP_NO;
            case  2:  return GraferV3.CLIP_VALUE;
            default:
                System.out.println("Fehler: rh5wjj37u"); 
                return -1;
        }
        //--------------------
    }



    private void bestaetige () {
        //----------------------------
        // Zahlenwerte werden von JTextField() eingelesen, falls nicht "Return" gedrueckt wurde -->
        //
        try {
            _jtfSymbFrequ= (int)jtfSymbFrequ.getNumberFromField();
            if (_jtfSymbFrequ<0) _jtfSymbFrequ=0;  // keine negative Symbol-Frequenz
        } catch (Exception e) {}
        try {
            _jcmClipValXmin= jcmClipValXmin.getNumberFromField();
            _jcmClipValXmax= jcmClipValXmax.getNumberFromField();
            _jcmClipValYmin= jcmClipValYmin.getNumberFromField();
            _jcmClipValYmax= jcmClipValYmax.getNumberFromField();
        } catch (Exception e) {}
        //----------------------------
        // lokal gespeicherte Parameter werden an das Diagramm zurueckgemeldet -->
        //
//        grafer.crvAchsenTyp[im1][im2]= _jcbAchsenTyp;
        grafer.crvLineStyle[im1][im2]= _jcbLineStyle;
        grafer.crvLineColor[im1][im2]= _jcbLineColor;
        grafer.crvLineTransparency[im1][im2] = _curveTransparency;
        //
        grafer.crvSymbShow[im1][im2]=  _jcbSymbShow;
        grafer.crvSymbFrequ[im1][im2]= _jtfSymbFrequ;
        grafer.crvSymbShape[im1][im2]= _jcmSymbShape;
        grafer.crvSymbColor[im1][im2]= _jcbSymbColor;
        /*
        grafer.crvClipXmin[im1][im2]= _jcmClipXmin;
        grafer.crvClipXmax[im1][im2]= _jcmClipXmax;
        grafer.crvClipYmin[im1][im2]= _jcmClipYmin;
        grafer.crvClipYmax[im1][im2]= _jcmClipYmax;
        grafer.crvClipValXmin[im1][im2]= _jcmClipValXmin;
        grafer.crvClipValXmax[im1][im2]= _jcmClipValXmax;
        grafer.crvClipValYmin[im1][im2]= _jcmClipValYmin;
        grafer.crvClipValYmax[im1][im2]= _jcmClipValYmax;
        */
        //----------------------------
        grafer.setzeKurven();
        grafer.repaint();
    }




    //-----------------------------
    public void windowClosing (WindowEvent we) { this.dispose(); }
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowOpened (WindowEvent we) {}
    //-----------------------------

}


