/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import javax.swing.JLabel;
import javax.swing.Icon;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.Dimension;


public class JLabelRot extends JLabel {

    //-------------
    private double phiGradImUhrzeiger=0;
    private String txt;
    private int dx=5;
    //-------------


    //------------------------------------------------------
    public JLabelRot (String text, Icon icon, int horizontalAlignment) { super(text,icon,horizontalAlignment); }
    public JLabelRot (String text, int horizontalAlignment) { super(text,horizontalAlignment); }
    public JLabelRot (String text) { super(text); }
    public JLabelRot (Icon image, int horizontalAlignment) { super(image,horizontalAlignment); }
    public JLabelRot (Icon image) { super(image); }
    public JLabelRot () { super(); }
    //------------------------------------------------------

    public JLabelRot (String txt, double phiGradImUhrzeiger, int breite, int hoehe) {
        this(txt);
        this.txt=txt;   this.phiGradImUhrzeiger=phiGradImUhrzeiger;
        this.setPreferredSize(new Dimension(breite,hoehe));
    }

    public void paint (Graphics g) {
        Graphics2D g2= (Graphics2D)g;
        g2.transform(AffineTransform.getRotateInstance(phiGradImUhrzeiger*Math.PI/180.0));
        g2.drawString(txt, (-this.getHeight()+dx), (this.getWidth()-(this.getWidth()-g2.getFont().getSize())/2) );
    }

}


