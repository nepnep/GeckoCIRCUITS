/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author andy Compresses the data employing a zip-compression algorithm.
 * In order to achive a higher compression ratio, the difference compression
 * algorithm of order 2 (MDIFFORDER) is used, see therefore the paper:
 * "Lossless Compression of High-volume Numerical Data from Simulations,
 * Engelson, Fritzson, Fritzson...
 * For conveniece, the float data is directly stored inside a int array,
 * therefore use the Float.floatBitsToInt and Float.intBitsToFloat to
 * access the data values.
 * Furhermore, the double values are rounded to less bits than float.
 */
public class DataJunkCompressable implements DataJunk {

    private int[][] _data;
    private byte[] compressedData;
    private SoftReference<int[][]> _dataSoftRef;
    private int _startIndex;
    // divide this data junk in HI_LOW_N pieces, and pre-calculate the
    // high/low data values
    private int HI_LOW_N = 8;
    private ArrayList<ArrayList<HiLoData>> hiLoData;
    private final int _columns;
    public static int compressionCounter = 1;
    public static double compressionSum = 0;
    private static int MORDER_DIFF = 2;

    public DataJunkCompressable(int startIndex, int rows, int columns) {
        _data = new int[rows][columns];
        _columns = columns;
        _dataSoftRef = new SoftReference<int[][]>(_data);
        _startIndex = startIndex;
    }

    public double getValue(int row, int index) {
        //assert index - _startIndex < _data[0].length : _startIndex + " " + index;
        //assert index - _startIndex >= 0;

        if (_data == null) {
            int[][] hardSoftLink = _dataSoftRef.get();
            if (hardSoftLink == null) {
                hardSoftLink = deCompress();
            } else {
            }
            _dataSoftRef = new SoftReference<int[][]>(hardSoftLink);
            return Float.intBitsToFloat(hardSoftLink[row][index - _startIndex]);
        } else {
            return Float.intBitsToFloat(_data[row][index - _startIndex]);
        }
    }

    public void setValue(double value, int row, int column) {
        int columnIndex = column - _startIndex;
        _data[row][columnIndex] = Float.floatToIntBits(roundFloat((float) value));
        
    }

    public void doCompression() {
        Thread thread = new Thread(new compressThread());
        thread.start();
    }

    public int[][] deCompress() {
        int[][] returnValue = null;

        ObjectInputStream objIn = null;
        try {
            ByteArrayInputStream bin = new ByteArrayInputStream(compressedData);


            Deflater d = new Deflater(Deflater.FILTERED);
            //DeflaterInputStream deCompressedStream = new DeflaterInputStream(bin, d);
            GZIPInputStream deCompressedStream = new GZIPInputStream(bin);

            objIn = new ObjectInputStream(deCompressedStream);


            returnValue = (int[][]) objIn.readObject();
            objIn.close();

            calculateDifferenceDecompression(returnValue);


        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataJunkCompressable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataJunkCompressable.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                objIn.close();
            } catch (IOException ex) {
                Logger.getLogger(DataJunkCompressable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return returnValue;
    }

    private float roundFloat(float value) {
        int floatIntBits = Float.floatToIntBits(value);

        // this bit-field has 1111's at the beginning, whereas the last
        // x bits are zero. And-Connection with floatIntBits will round
        // the float value.
        int bitField = 2 * Integer.MAX_VALUE + 1 - (1 + 2 + 4 + 8 + 16 + 32 + 64 + 128
                + 256
                //+ 512 //+ 1024 + 2048 + 4096 + 8192 + 16384
                );
        int output = floatIntBits & bitField;
        //System.out.println(Integer.toBinaryString(bitField));
        return Float.intBitsToFloat(output);
    }
    

    public HiLoData getHiLoValue(int row, int columnStart, int columnStop) {
        assert columnStart <= columnStop;   

        if( (columnStop - columnStart) < _columns / HI_LOW_N) {
            return calculateRealHiLowData(row, columnStart, columnStop);
        }        
        
        if (hiLoData == null) {
            calculateHiLoData();
        }

        int hiLoStart = 0;
        int hiLoStop = HI_LOW_N;

        
        if (columnStart > _startIndex) {
            hiLoStart = ((columnStart - _startIndex) * HI_LOW_N) / _columns;
        }
        
        if(hiLoStart > HI_LOW_N - 1) { // if out of bounds, correct to maximal index
            hiLoStart = HI_LOW_N -1;
        }

        if (columnStop < _startIndex + _columns) {
            hiLoStop = 1 + ((columnStop - _startIndex) *HI_LOW_N)/ _columns;
        }

        if (hiLoStop > HI_LOW_N) {
            hiLoStop = HI_LOW_N;
        }

        if (hiLoStart < 0) {
            hiLoStart = 0;
        }

        HiLoData returnValue = new HiLoData();
        
        for (int j = hiLoStart; j < hiLoStop; j++) {
            HiLoData tmp = hiLoData.get(row).get(j);
            returnValue.insertCompare(tmp);
        }
        
        return returnValue;
    }

    public void calculateHiLoData() {

        hiLoData = new ArrayList<ArrayList<HiLoData>>();
        int intervalLength = _data[0].length / HI_LOW_N;
        for (int row = 0; row < _data.length; row++) {
            ArrayList<HiLoData> currentData = new ArrayList<HiLoData>();
            hiLoData.add(currentData);
            for (int i = 0; i < HI_LOW_N; i++) {
                HiLoData hiLow = new HiLoData();
                currentData.add(hiLow);
                for (int j = i * intervalLength; j < (i + 1) * intervalLength; j++) {
                    float x = Float.intBitsToFloat(_data[row][j]);
                    hiLow.insertCompare(x);
                }
            }
        }


    }

    public int getStartIndex() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getStopIndex() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void calculateDifferenceDecompression(int[][] returnValue) {
        for (int i = 0; i < returnValue.length; i++) {
            for (int k = 0; k < MORDER_DIFF; k++) {
                for (int j = MORDER_DIFF; j < returnValue[0].length; j++) {
                    returnValue[i][j] = returnValue[i][j] + returnValue[i][j - 1];
                }
            }
        }
    }

    private HiLoData calculateRealHiLowData(int row, int columnStart, int columnStop) {
        HiLoData hld = new HiLoData();
        
        if(columnStop - _startIndex > _columns) {
            columnStop = _startIndex + _columns;
        }
        if(columnStart - _startIndex < 0) {
            columnStart = _startIndex;
        }
        
        for(int i = columnStart; i < columnStop; i++) {
            hld.insertCompare((float) getValue(row, i));
        }
        return hld;
    }

    class compressThread implements Runnable {

        public void run() {
            calculateHiLoData();
            ObjectOutputStream objOut = null;
            try {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();

                //Deflater d = new Deflater(Deflater.FILTERED);
                //DeflaterOutputStream compression = new DeflaterOutputStream(bout, d);
                GZIPOutputStream compression = new GZIPOutputStream(bout);
                objOut = new ObjectOutputStream(compression);

                int[][] compressData = calculateDifferenceCompression(_data);

                objOut.writeObject(compressData);
                objOut.flush();
                objOut.close();

                int byteLength = bout.size();
                compressedData = bout.toByteArray();
                int originalLength = _data[0].length * _data.length * 4;
                //System.out.println("compressed length: " + byteLength + " " + originalLength + " " + 1.0 * byteLength / originalLength);

//                compressionCounter++;
//                compressionSum += 1.0 * byteLength / originalLength;
//                System.out.println("compression ratio: " + compressionSum / compressionCounter);

            } catch (IOException ex) {
                Logger.getLogger(DataJunkCompressable.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    objOut.close();
                } catch (IOException ex) {
                    Logger.getLogger(DataJunkCompressable.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            _data = null;
        }

        private int[][] calculateDifferenceCompression(int[][] _data) {
            int[][] compressData = new int[_data.length][_data[0].length];

            for (int i = 0; i < _data.length; i++) {
                for (int j = 0; j < _data[0].length; j++) {
                    compressData[i][j] = _data[i][j];
                }
            }

            for (int i = 0; i < _data.length; i++) {
                for (int k = 0; k < MORDER_DIFF; k++) {
                    for (int j = _data[0].length - 1; j >= MORDER_DIFF; j--) {
                        compressData[i][j] = compressData[i][j] - compressData[i][j - 1];
                    }
                }
            }
            return compressData;
        }
    }
}
