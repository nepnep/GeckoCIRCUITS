/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.TechFormat;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;


public class DisplayWorksheet extends JPanel {

    //----------------------------
    private JTable table;
    private JScrollPane jsp;
    private Object[][] wsObj;
    private TechFormat tcf= new TechFormat();
    //----------------------------


    public DisplayWorksheet () {
        //this.test();
        this.baueGUI();
    }


    private void baueGUI () {
        this.removeAll();
        this.setLayout(new BorderLayout());
        jsp= new JScrollPane(table);
        this.add(jsp, BorderLayout.CENTER);
    }
    
    

    public void ladeWorksheet (String[] header, DataContainer ws, int[] zvCounterRef) {
        wsObj= new Object[zvCounterRef[0]+1][ws.getRowLength()];
        for (int i1=0;  i1<wsObj.length;  i1++)
            for (int i2=0;  i2<ws.getRowLength();  i2++)
                // Formate muessen 'geflippt' werden, Table speichert String-Objekte --> 
                try { wsObj[i1][i2]= tcf.formatENG(ws.getValue(i2, i1),6); } catch (Exception e) {}
        this.aktualisiereWorksheetHeader(header);
    }


    public void ladeWorksheet (String[] header, DataContainer ws) {
        wsObj= new Object[ws.getColumnLength()][ws.getRowLength()];
        for (int i1=0;  i1<ws.getColumnLength();  i1++)
            for (int i2=0;  i2<ws.getRowLength();  i2++)
                wsObj[i1][i2]= tcf.formatENG(ws.getValue(i2, i1),6);  // Formate muessen 'geflippt' werden, Table speichert String-Objekte
        this.aktualisiereWorksheetHeader(header);
    }


    // wird von ausserhalb aufgerufen, wenn Knoten im SchematicEntry umbenannt werden
    public void aktualisiereWorksheetHeader (String[] header) {
        table= new JTable(wsObj, header) {
            // ueberschreiben, damit man nicht in den Daten herumeditieren kann -->
            public boolean isCellEditable (int row, int col) { return false; }
        };
        this.baueGUI();
    }




    private void test () {
        String[] header= new String[]{"a","b","c","d","a2","b2","c2","d2"};
        Object[][] wsObj= new Object[32][header.length];
        for (int i1=0;  i1<wsObj.length;  i1++)
            for (int i2=0;  i2<wsObj[0].length;  i2++)
                if (i2==0) wsObj[i1][i2]= new Double(i1);
                else  wsObj[i1][i2]= new Double(Math.sin(i1*i2));
        table= new JTable(wsObj,header);
    }


}



