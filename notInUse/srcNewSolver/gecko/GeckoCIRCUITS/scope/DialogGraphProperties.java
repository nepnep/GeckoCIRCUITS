/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.FormatJTextField;
import gecko.GeckoCIRCUITS.allg.Typ;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JDialog; 
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.JTabbedPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Container;
import java.util.StringTokenizer;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import java.net.URL;




public class DialogGraphProperties extends JDialog implements WindowListener {


    //----------------------------
    private GraferImplementation grafer;  // callback fuer Properties-Zugriff
    private int grfIndex;  // Nummer des aktuell zu bearbeitenden Graphen
    private Container c;
    private JButton jbOK, jbCa, jbAp;
    private GridBagConstraints gbc= new GridBagConstraints();
    //----------------------------
    private FormatJTextField jtfXmin, jtfXmax;
    private JCheckBox jcbXautoScale;
    private JComboBox jcmXlinStyl, jcmXlinCol, jcmXLinLog;
    private JCheckBox jcbXAutoTickMaj;
    private FormatJTextField jtfXtickSpaceMaj, jtfXtickSpaceMin, jtfXtickLengthMaj, jtfXtickLengthMin;
    private JCheckBox jcbXShowLabelMaj, jcbXShowLabelMin;
    private JCheckBox jcbXShowGridMaj, jcbXShowGridMin;
    private JComboBox jcmXgridMajStyl, jcmXgridMinStyl, jcmXgridMajCol, jcmXgridMinCol;
    //
    private FormatJTextField jtfYmin, jtfYmax;
    private JCheckBox jcbYautoScale;
    private JComboBox jcmYlinStyl, jcmYlinCol, jcmYLinLog;
    private JCheckBox jcbYAutoTickMaj;
    private FormatJTextField jtfYtickSpaceMaj, jtfYtickSpaceMin, jtfYtickLengthMaj, jtfYtickLengthMin;
    private JCheckBox jcbYShowLabelMaj, jcbYShowLabelMin;
    private JCheckBox jcbYShowGridMaj, jcbYShowGridMin;
    private JComboBox jcmYgridMajStyl, jcmYgridMinStyl, jcmYgridMajCol, jcmYgridMinCol;
    //
    //----------------------------
    // lokal gespeicherte Parameter dieses Dialog-Fensters, die in einem Stueck bei 'Apply' oder 'OK' uebergeben werden -->
    //
    private boolean _jcbShowLegende;
    private boolean _jcbShowAxisX,_jcbShowAxisY;  // Anzeigen oder Ausblenden der Achsen
    //
    private double _jtfXmin, _jtfXmax;
    private boolean _jcbXautoScale;
    private int _jcmXlinStyl, _jcmXlinCol, _jcmXLinLog;
    private boolean _jcbXAutoTickMaj;
    private double _jtfXtickSpaceMaj;
    private int _jtfXtickSpaceMin, _jtfXtickLengthMaj, _jtfXtickLengthMin;
    private boolean _jcbXShowLabelMaj, _jcbXShowLabelMin;
    private boolean _jcbXShowGridMaj, _jcbXShowGridMin;
    private int _jcmXgridMajStyl, _jcmXgridMinStyl, _jcmXgridMajCol, _jcmXgridMinCol;
    //
    private double _jtfYmin, _jtfYmax;
    private boolean _jcbYautoScale;
    private int _jcmYlinStyl, _jcmYlinCol, _jcmYLinLog;
    private boolean _jcbYAutoTickMaj;
    private double _jtfYtickSpaceMaj;
    private int _jtfYtickSpaceMin, _jtfYtickLengthMaj, _jtfYtickLengthMin;
    private boolean _jcbYShowLabelMaj, _jcbYShowLabelMin;
    private boolean _jcbYShowGridMaj, _jcbYShowGridMin;
    private int _jcmYgridMajStyl, _jcmYgridMinStyl, _jcmYgridMajCol, _jcmYgridMinCol;
    //
    //----------------------------
    // Wenn 'Show X-Axis' usw. ein- und wieder ausgeblendet wird, dann sollen die Original-Einstellungen gespeichert bleiben -->
    //
    private boolean _ORIGjcbXShowGridMaj, _ORIGjcbXShowGridMin;
    private boolean _ORIGjcbYShowGridMaj, _ORIGjcbYShowGridMin;
    private int _ORIGjcmXlinCol, _ORIGjcmYlinCol;
    private int _ORIGjcmXlinStyl, _ORIGjcmYlinStyl;
    private int _ORIGjtfXtickLengthMaj, _ORIGjtfXtickLengthMin;
    private int _ORIGjtfYtickLengthMaj, _ORIGjtfYtickLengthMin;
    private boolean _ORIGjcbXShowLabelMaj, _ORIGjcbXShowLabelMin;
    private boolean _ORIGjcbYShowLabelMaj, _ORIGjcbYShowLabelMin;
    //----------------------------




    public DialogGraphProperties (GraferImplementation grafer, int grfIndex) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        this.grafer= grafer;
        this.grfIndex= grfIndex;
        //--------------------
        this.setTitle(Typ.spTitle+"Graph Properties");
        this.addWindowListener(this);
        c= this.getContentPane();
        c.setLayout(new BorderLayout());
        //--------------------
        c.removeAll();
        this.initPropertiesZVmodus();  // Initialisierung und anschliessendes Einlesen der Parameter direkt von 'grafer'
        this.baueGUI();  // Initialisierung und Plazierung aller Swing-Eingabe-Elemente
        this.setParameterInSwingElements();  // die Parameter werden den Swing-Feldern zugewiesen
        this.pack();
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(grafer.getScope().getX_PositionAbsolut_DialogConnect()+Typ.DX_SCOPE_DLG2, grafer.getScope().getY_PositionAbsolut_DialogConnect()+Typ.DY_SCOPE_DLG2);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setResizable(false);
        this.setVisible(true);
        //--------------------
    }



    private void initPropertiesZVmodus () {
        //----------------------------
        int indexGraphUnten= grafer.getAnzahlSichtbarerDiagramme()-1;
        _jcbShowAxisX= grafer.showAxisX[grfIndex];
        _jtfXmin= grafer.minX[indexGraphUnten];
        _jtfXmax= grafer.maxX[indexGraphUnten];
        _jcbXautoScale= grafer.autoScaleX[indexGraphUnten];
        _jcmXlinStyl= grafer.xAchseStil[indexGraphUnten];     _ORIGjcmXlinStyl= grafer.ORIGjcmXlinStyl[indexGraphUnten];
        _jcmXlinCol= grafer.xAchseFarbe[indexGraphUnten];     _ORIGjcmXlinCol= grafer.ORIGjcmXlinCol[indexGraphUnten];
        _jcmXLinLog= grafer.xAchsenTyp[indexGraphUnten];
        _jcbXAutoTickMaj= grafer.xTickAutoSpacing[indexGraphUnten];
        _jtfXtickSpaceMaj= grafer.xTickSpacing[indexGraphUnten];
        _jtfXtickSpaceMin= grafer.xAnzTicksMinor[indexGraphUnten];
        _jtfXtickLengthMaj= grafer.xTickLaenge[indexGraphUnten];       _ORIGjtfXtickLengthMaj= grafer.ORIGjtfXtickLengthMaj[indexGraphUnten];
        _jtfXtickLengthMin= grafer.xTickLaengeMinor[indexGraphUnten];  _ORIGjtfXtickLengthMin= grafer.ORIGjtfXtickLengthMin[indexGraphUnten];
        _jcbXShowLabelMaj= grafer.zeigeLabelsXmaj[indexGraphUnten];    _ORIGjcbXShowLabelMaj= grafer.ORIGjcbXShowLabelMaj[indexGraphUnten];
        _jcbXShowLabelMin= grafer.zeigeLabelsXmin[indexGraphUnten];    _ORIGjcbXShowLabelMin= grafer.ORIGjcbXShowLabelMin[indexGraphUnten];
        _jcbXShowGridMaj= grafer.xShowGridMaj[indexGraphUnten];        _ORIGjcbXShowGridMaj= grafer.ORIGjcbXShowGridMaj[indexGraphUnten];
        _jcbXShowGridMin= grafer.xShowGridMin[indexGraphUnten];        _ORIGjcbXShowGridMin= grafer.ORIGjcbXShowGridMin[indexGraphUnten];
        _jcmXgridMajStyl= grafer.linStilGridNormalX[indexGraphUnten];
        _jcmXgridMinStyl= grafer.linStilGridNormalXminor[indexGraphUnten];
        _jcmXgridMajCol= grafer.farbeGridNormalX[indexGraphUnten];
        _jcmXgridMinCol= grafer.farbeGridNormalXminor[indexGraphUnten];
        //
        _jcbShowLegende= grafer.jcbShowLegende[grfIndex];
        _jcbShowAxisY= grafer.showAxisY[grfIndex];
        _jtfYmin= grafer.minY[grfIndex];
        _jtfYmax= grafer.maxY[grfIndex];
        _jcbYautoScale= grafer.autoScaleY[grfIndex];
        _jcmYlinStyl= grafer.yAchseStil[grfIndex];   _ORIGjcmYlinStyl= grafer.ORIGjcmYlinStyl[grfIndex];
        _jcmYlinCol= grafer.yAchseFarbe[grfIndex];   _ORIGjcmYlinCol= grafer.ORIGjcmYlinCol[grfIndex];
        _jcmYLinLog= grafer.yAchsenTyp[grfIndex];
        _jcbYAutoTickMaj= grafer.yTickAutoSpacing[grfIndex];
        _jtfYtickSpaceMaj= grafer.yTickSpacing[grfIndex];
        _jtfYtickSpaceMin= grafer.yAnzTicksMinor[grfIndex];
        _jtfYtickLengthMaj= grafer.yTickLaenge[grfIndex];       _ORIGjtfYtickLengthMaj= grafer.ORIGjtfYtickLengthMaj[grfIndex];
        _jtfYtickLengthMin= grafer.yTickLaengeMinor[grfIndex];  _ORIGjtfYtickLengthMin= grafer.ORIGjtfYtickLengthMin[grfIndex];
        _jcbYShowLabelMaj= grafer.zeigeLabelsYmaj[grfIndex];    _ORIGjcbYShowLabelMaj= grafer.ORIGjcbYShowLabelMaj[grfIndex];
        _jcbYShowLabelMin= grafer.zeigeLabelsYmin[grfIndex];    _ORIGjcbYShowLabelMin= grafer.ORIGjcbYShowLabelMin[grfIndex];
        _jcbYShowGridMaj= grafer.yShowGridMaj[grfIndex];        _ORIGjcbYShowGridMaj= grafer.ORIGjcbYShowGridMaj[grfIndex];
        _jcbYShowGridMin= grafer.yShowGridMin[grfIndex];        _ORIGjcbYShowGridMin= grafer.ORIGjcbYShowGridMin[grfIndex];
        _jcmYgridMajStyl= grafer.linStilGridNormalY[grfIndex];
        _jcmYgridMinStyl= grafer.linStilGridNormalYminor[grfIndex];
        _jcmYgridMajCol= grafer.farbeGridNormalY[grfIndex];
        _jcmYgridMinCol= grafer.farbeGridNormalYminor[grfIndex];
        //
        //----------------------------
    }



    private void baueGUI () {
        //================================================
        c.add(this.getGraferTab(grfIndex), BorderLayout.CENTER);
        //
        JPanel jpOK= new JPanel();
        jbOK= new JButton("OK");
        jbCa= new JButton("Cancel");
        jbAp= new JButton("Apply");
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                werteVonFeldernEinlesen();
                bestaetige();
                dispose();
            }
        });
        jbCa.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                dispose();
            }
        });
        jbAp.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                werteVonFeldernEinlesen();
                bestaetige();
            }
        });
        jpOK.add(jbOK);
        jpOK.add(jbCa);
        jpOK.add(jbAp);
        c.add(jpOK, BorderLayout.SOUTH);
        //================================================
    }




    private void setParameterInSwingElements () {
        //-------------------------------
        // Einlesen der Parameter-Werte in die Swing-Eingabefelder -->
        //
        // ALLGEMEIN -->
        //
        //===============================
        // (1) X-Achse -->
        //
        jcbXautoScale.setSelected(_jcbXautoScale);
        jtfXmax.setNumberToField(_jtfXmax);
        jtfXmin.setNumberToField(_jtfXmin);
        if (_jcbXautoScale) { jtfXmax.setEnabled(false);   jtfXmin.setEnabled(false); }
        //
        jcmXlinStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmXlinStyl));
        jcmXlinCol.setSelectedIndex(this.setzeFarbListe(_jcmXlinCol));
        jcbXAutoTickMaj.setSelected(_jcbXAutoTickMaj);
        jtfXtickSpaceMaj.setNumberToField(_jtfXtickSpaceMaj);
        jtfXtickLengthMaj.setNumberToField(_jtfXtickLengthMaj);
        jcbXShowLabelMaj.setSelected(_jcbXShowLabelMaj);
        jtfXtickSpaceMin.setNumberToField(_jtfXtickSpaceMin);
        jtfXtickLengthMin.setNumberToField(_jtfXtickLengthMin);
        jcbXShowLabelMin.setSelected(_jcbXShowLabelMin);
        //
        if (jcbXAutoTickMaj.isSelected()) {
            jtfXtickSpaceMaj.setEnabled(false);   jtfXtickSpaceMin.setEnabled(false);
        } else {
            jtfXtickSpaceMaj.setEnabled(true);    jtfXtickSpaceMin.setEnabled(true);
        }
        jcmXgridMajStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmXgridMajStyl));
        jcmXgridMajCol.setSelectedIndex(this.setzeFarbListe(_jcmXgridMajCol));
        if (_jcbXShowGridMaj) {
            jcbXShowGridMaj.setSelected(true);
            jcmXgridMajStyl.setEnabled(true);
            jcmXgridMajCol.setEnabled(true);
        } else {
            jcbXShowGridMaj.setSelected(false);
            jcmXgridMajStyl.setEnabled(false);
            jcmXgridMajCol.setEnabled(false);
        }
        jcmXgridMinStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmXgridMinStyl));
        jcmXgridMinCol.setSelectedIndex(this.setzeFarbListe(_jcmXgridMinCol));
        if (_jcbXShowGridMin) {
            jcbXShowGridMin.setSelected(true);
            jcmXgridMinStyl.setEnabled(true);
            jcmXgridMinCol.setEnabled(true);
        } else {
            jcbXShowGridMin.setSelected(false);
            jcmXgridMinStyl.setEnabled(false);
            jcmXgridMinCol.setEnabled(false);
        }
        switch (_jcmXLinLog) {
            case GraferV3.ACHSE_LIN:
                jcmXLinLog.setSelectedIndex(0);
                jcbXAutoTickMaj.setEnabled(true);
                if (!_jcbXAutoTickMaj) { jtfXtickSpaceMaj.setEnabled(true);   jtfXtickSpaceMin.setEnabled(true); }
                else { jtfXtickSpaceMaj.setEnabled(false);   jtfXtickSpaceMin.setEnabled(false); }
                break;
            case GraferV3.ACHSE_LOG:
                jcmXLinLog.setSelectedIndex(1);
                jcbXAutoTickMaj.setEnabled(false);
                jtfXtickSpaceMaj.setEnabled(false);
                jtfXtickSpaceMin.setEnabled(false);
                break;
            default: System.out.println("Fehler: 77uinaeg98");  break;
        }
        //===============================
        // (2) Y-Achse -->
        //
        jcbYautoScale.setSelected(_jcbYautoScale);
        jtfYmax.setNumberToField(_jtfYmax);
        jtfYmin.setNumberToField(_jtfYmin);
        if (_jcbYautoScale) { jtfYmax.setEnabled(false);   jtfYmin.setEnabled(false); }
        //
        jcmYlinStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmYlinStyl));
        jcmYlinCol.setSelectedIndex(this.setzeFarbListe(_jcmYlinCol));
        jcbYAutoTickMaj.setSelected(_jcbYAutoTickMaj);
        jtfYtickSpaceMaj.setNumberToField(_jtfYtickSpaceMaj);
        jtfYtickLengthMaj.setNumberToField(_jtfYtickLengthMaj);
        jcbYShowLabelMaj.setSelected(_jcbYShowLabelMaj);
        jtfYtickSpaceMin.setNumberToField(_jtfYtickSpaceMin);
        jtfYtickLengthMin.setNumberToField(_jtfYtickLengthMin);
        jcbYShowLabelMin.setSelected(_jcbYShowLabelMin);
        //
        if (jcbYAutoTickMaj.isSelected()) {
            jtfYtickSpaceMaj.setEnabled(false);   jtfYtickSpaceMin.setEnabled(false);
        } else {
            jtfYtickSpaceMaj.setEnabled(true);    jtfYtickSpaceMin.setEnabled(true);
        }
        jcmYgridMajStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmYgridMajStyl));
        jcmYgridMajCol.setSelectedIndex(this.setzeFarbListe(_jcmYgridMajCol));
        if (_jcbYShowGridMaj) {
            jcbYShowGridMaj.setSelected(true);
            jcmYgridMajStyl.setEnabled(true);
            jcmYgridMajCol.setEnabled(true);
        } else {
            jcbYShowGridMaj.setSelected(false);
            jcmYgridMajStyl.setEnabled(false);
            jcmYgridMajCol.setEnabled(false);
        }
        jcmYgridMinStyl.setSelectedIndex(this.setzeLinienstilListe(_jcmYgridMinStyl));
        jcmYgridMinCol.setSelectedIndex(this.setzeFarbListe(_jcmYgridMinCol));
        if (_jcbYShowGridMin) {
            jcbYShowGridMin.setSelected(true);
            jcmYgridMinStyl.setEnabled(true);
            jcmYgridMinCol.setEnabled(true);
        } else {
            jcbYShowGridMin.setSelected(false);
            jcmYgridMinStyl.setEnabled(false);
            jcmYgridMinCol.setEnabled(false);
        }
        switch (_jcmYLinLog) {
            case GraferV3.ACHSE_LIN:
                jcmYLinLog.setSelectedIndex(0);
                jcbYAutoTickMaj.setEnabled(true);
                if (!_jcbYAutoTickMaj) { jtfYtickSpaceMaj.setEnabled(true);   jtfYtickSpaceMin.setEnabled(true); }
                else { jtfYtickSpaceMaj.setEnabled(false);   jtfYtickSpaceMin.setEnabled(false); }
                break;
            case GraferV3.ACHSE_LOG:
                jcmYLinLog.setSelectedIndex(1);
                jcbYAutoTickMaj.setEnabled(false);
                jtfYtickSpaceMaj.setEnabled(false);
                jtfYtickSpaceMin.setEnabled(false);
                break;
            default: System.out.println("Fehler: yyyth6hj6");  break;
        }
        //===============================
        //===============================
        bestaetige();
    }





    private JPanel getGraferTab (int indexLok) {
        final int index= indexLok;  // fuer Zugriff aus innerer Klasse
        final JTabbedPane jtbACH= new JTabbedPane();  // X, Y
        String plenk= "             ";  // dx-Abstand
        int colsNumber=5, colsTxt=10;
        gbc.fill=gbc.BOTH;
        //==========================================================================================
        //==========================================================================================
        // Achsen-Beschreibungen: X / Y / Signal -->
        //
        JPanel pACH= new JPanel();
        pACH.setLayout(new BorderLayout());
        pACH.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Axis", TitledBorder.LEFT, TitledBorder.TOP));
        pACH.add(jtbACH, BorderLayout.CENTER);
        //
        //==========================================================================================
        //==========================================================================================
        // (1) X-Achse:
        //
        //------------
        JPanel jpXscale= new JPanel();
        jpXscale.setLayout(new GridBagLayout());
        jpXscale.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Scale", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jpXscale.add(new JLabel("Scale :  "), gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jcmXLinLog= new JComboBox(new String[]{"LINEAR", "LOG"});
        jcmXLinLog.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                switch (jcmXLinLog.getSelectedIndex()) {
                    case 0:
                        _jcmXLinLog= GraferV3.ACHSE_LIN;
                        jcbXAutoTickMaj.setEnabled(true);
                        jtfXtickSpaceMaj.setEnabled(true);
                        jtfXtickSpaceMin.setEnabled(true);
                        break;
                    case 1:
                        _jcmXLinLog= GraferV3.ACHSE_LOG;
                        jcbXAutoTickMaj.setEnabled(false);
                        jtfXtickSpaceMaj.setEnabled(false);
                        jtfXtickSpaceMin.setEnabled(false);
                        break;
                    default: System.out.println("Fehler: eghq3434");  break;
                }
                bestaetige();
            }
        });
        jpXscale.add(jcmXLinLog, gbc);
        gbc.gridx=2;   gbc.gridy=0;
        jpXscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=0;   gbc.gridy=1;
        jcbXautoScale= new JCheckBox();
        jcbXautoScale.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXautoScale.isSelected()) {
                    _jcbXautoScale= true;
                    // AUTO-Berechnung starten -->
                    grafer.definiereAchsenbegrenzungenImAutoZoom();
                    _jtfXmin= grafer.minX[grfIndex];   jtfXmin.setNumberToField(_jtfXmin);
                    _jtfXmax= grafer.maxX[grfIndex];   jtfXmax.setNumberToField(_jtfXmax);
                    jtfXmin.setEnabled(false);         jtfXmax.setEnabled(false);  // ist zwar deaktiviert, zeigt aber die richtigen Werte an
                } else {
                    _jcbXautoScale= false;
                    jtfXmin.setEnabled(true);   jtfXmax.setEnabled(true);
                }
                bestaetige();
            }
        });
        jpXscale.add(jcbXautoScale, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jpXscale.add(new JLabel("Auto-Scaling"), gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpXscale.add(new JLabel(" x-MIN :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfXmin= new FormatJTextField();
        jtfXmin.setColumns(colsNumber);
        jtfXmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXmin= jtfXmin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXscale.add(jtfXmin, gbc);
        gbc.gridx=2;   gbc.gridy=2;
        jpXscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=0;   gbc.gridy=3;
        jpXscale.add(new JLabel(" x-MAX :  "), gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jtfXmax= new FormatJTextField();
        jtfXmax.setColumns(colsNumber);
        jtfXmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXmax= jtfXmax.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXscale.add(jtfXmax, gbc);
        gbc.gridx=2;   gbc.gridy=3;
        jpXscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=2;   gbc.gridy=3;
        jpXscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=0;
        jpXscale.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=4;   gbc.gridy=0;
        jcmXlinStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmXlinStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXlinStyl= setzeLinienstilSelektiert(jcmXlinStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpXscale.add(jcmXlinStyl, gbc);
        gbc.gridx=3;   gbc.gridy=1;
        jpXscale.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=4;   gbc.gridy=1;
        jcmXlinCol= new JComboBox(GraferV3.FARBEN);
        jcmXlinCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXlinCol= setzeFarbeSelektiert(jcmXlinCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpXscale.add(jcmXlinCol, gbc);
        //
        //------------
        JPanel jpXticks= new JPanel();
        jpXticks.setLayout(new GridLayout(1,2));
        jpXticks.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Define Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        JPanel jpXTickMaj= new JPanel();
        jpXTickMaj.setLayout(new GridBagLayout());
        jpXTickMaj.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Major-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        JPanel jpXTickMin= new JPanel();
        jpXTickMin.setLayout(new GridBagLayout());
        jpXTickMin.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Minor-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        jpXticks.add(jpXTickMaj);
        jpXticks.add(jpXTickMin);
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbXAutoTickMaj= new JCheckBox();
        jcbXAutoTickMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXAutoTickMaj.isSelected()) {
                    _jtfXtickSpaceMaj= grafer.getAutoTickSpacingX(index);
                    jtfXtickSpaceMaj.setNumberToField(_jtfXtickSpaceMaj);
                    _jcbXAutoTickMaj= true;   jtfXtickSpaceMaj.setEnabled(false);   jtfXtickSpaceMin.setEnabled(false);
                } else {
                    _jcbXAutoTickMaj= false;   jtfXtickSpaceMaj.setEnabled(true);   jtfXtickSpaceMin.setEnabled(true);
                }
                bestaetige();
            }
        });
        jpXTickMaj.add(jcbXAutoTickMaj, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpXTickMaj.add(new JLabel("Auto-Spacing"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpXTickMaj.add(new JLabel("Tick-Space :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfXtickSpaceMaj= new FormatJTextField();
        jtfXtickSpaceMaj.setColumns(colsNumber);
        jtfXtickSpaceMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXtickSpaceMaj= jtfXtickSpaceMaj.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXTickMaj.add(jtfXtickSpaceMaj, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpXTickMaj.add(new JLabel("Tick-Length :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfXtickLengthMaj= new FormatJTextField();
        jtfXtickLengthMaj.setColumns(colsNumber);
        jtfXtickLengthMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXtickLengthMaj= (int)jtfXtickLengthMaj.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXTickMaj.add(jtfXtickLengthMaj, gbc);
        gbc.gridx=0;   gbc.gridy=3;
        jcbXShowLabelMaj= new JCheckBox();
        jcbXShowLabelMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXShowLabelMaj.isSelected()) {
                    _jcbXShowLabelMaj= true;
                } else {
                    _jcbXShowLabelMaj=false;
                }
                bestaetige();
            }
        });
        jpXTickMaj.add(jcbXShowLabelMaj, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jpXTickMaj.add(new JLabel("Show Label at Tick"), gbc);
        //
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        gbc.gridx=1;   gbc.gridy=0;
        jpXTickMin.add(new JLabel(" "), gbc);  // "Plenk"
        gbc.gridx=0;   gbc.gridy=1;
        jpXTickMin.add(new JLabel("Number of Ticks :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfXtickSpaceMin= new FormatJTextField();
        jtfXtickSpaceMin.setColumns(colsNumber);
        jtfXtickSpaceMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXtickSpaceMin= (int)jtfXtickSpaceMin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXTickMin.add(jtfXtickSpaceMin, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpXTickMin.add(new JLabel("Tick-Length :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfXtickLengthMin= new FormatJTextField();
        jtfXtickLengthMin.setColumns(colsNumber);
        jtfXtickLengthMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfXtickLengthMin= (int)jtfXtickLengthMin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpXTickMin.add(jtfXtickLengthMin, gbc);
        gbc.gridx=0;   gbc.gridy=3;
        jcbXShowLabelMin= new JCheckBox();
        jcbXShowLabelMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXShowLabelMin.isSelected()) {
                    _jcbXShowLabelMin= true;
                } else {
                    _jcbXShowLabelMin= false;
                }
                bestaetige();
            }
        });
        jpXTickMin.add(jcbXShowLabelMin, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jpXTickMin.add(new JLabel("Show Label at Tick"), gbc);
        //
        //------------
        JPanel jpXgrid= new JPanel();
        jpXgrid.setLayout(new GridLayout(1,2));
        jpXgrid.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Grid Normal to X-Axis", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        JPanel jpXGridMaj= new JPanel();
        jpXGridMaj.setLayout(new GridBagLayout());
        jpXGridMaj.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "At Major-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        JPanel jpXGridMin= new JPanel();
        jpXGridMin.setLayout(new GridBagLayout());
        jpXGridMin.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "At Minor-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        jpXgrid.add(jpXGridMaj);
        jpXgrid.add(jpXGridMin);
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbXShowGridMaj= new JCheckBox();
        jcbXShowGridMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXShowGridMaj.isSelected()) {
                    jcmXgridMajStyl.setEnabled(true);  jcmXgridMajCol.setEnabled(true);   _jcbXShowGridMaj=true;
                } else {
                    jcmXgridMajStyl.setEnabled(false);  jcmXgridMajCol.setEnabled(false);   _jcbXShowGridMaj=false;
                }
                bestaetige();
            }
        });
        jpXGridMaj.add(jcbXShowGridMaj, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpXGridMaj.add(new JLabel("Show Grid Line"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpXGridMaj.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcmXgridMajStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmXgridMajStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXgridMajStyl= setzeLinienstilSelektiert(jcmXgridMajStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpXGridMaj.add(jcmXgridMajStyl, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpXGridMaj.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmXgridMajCol= new JComboBox(GraferV3.FARBEN);
        jcmXgridMajCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXgridMajCol= setzeFarbeSelektiert(jcmXgridMajCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpXGridMaj.add(jcmXgridMajCol, gbc);
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbXShowGridMin= new JCheckBox();
        jcbXShowGridMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbXShowGridMin.isSelected()) {
                    jcmXgridMinStyl.setEnabled(true);  jcmXgridMinCol.setEnabled(true);   _jcbXShowGridMin=true;
                } else {
                    jcmXgridMinStyl.setEnabled(false);  jcmXgridMinCol.setEnabled(false);   _jcbXShowGridMin=false;
                }
                bestaetige();
            }
        });
        jpXGridMin.add(jcbXShowGridMin, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpXGridMin.add(new JLabel("Show Grid Line"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpXGridMin.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcmXgridMinStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmXgridMinStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXgridMinStyl= setzeLinienstilSelektiert(jcmXgridMinStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpXGridMin.add(jcmXgridMinStyl, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpXGridMin.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmXgridMinCol= new JComboBox(GraferV3.FARBEN);
        jcmXgridMinCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmXgridMinCol= setzeFarbeSelektiert(jcmXgridMinCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpXGridMin.add(jcmXgridMinCol, gbc);
        //
        //------------
        JPanel jpX= new JPanel();
        jpX.setLayout(new BorderLayout());
        jpX.add(jpXscale, BorderLayout.NORTH);
        jpX.add(jpXticks, BorderLayout.CENTER);
        jpX.add(jpXgrid, BorderLayout.SOUTH);
        jtbACH.addTab("X-Axis", jpX);
        //
        //==========================================================================================
        //==========================================================================================
        // (2) Y-Achse:
        //
        //------------
        JPanel jpYscale= new JPanel();
        jpYscale.setLayout(new GridBagLayout());
        jpYscale.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Scale", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jpYscale.add(new JLabel("Scale :  "), gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jcmYLinLog= new JComboBox(new String[]{"LINEAR", "LOG"});
        jcmYLinLog.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                switch (jcmYLinLog.getSelectedIndex()) {
                    case 0:
                        _jcmYLinLog= GraferV3.ACHSE_LIN;
                        jcbYAutoTickMaj.setEnabled(true);
                        jtfYtickSpaceMaj.setEnabled(true);
                        jtfYtickSpaceMin.setEnabled(true);
                        break;
                    case 1:
                        _jcmYLinLog= GraferV3.ACHSE_LOG;
                        jcbYAutoTickMaj.setEnabled(false);
                        jtfYtickSpaceMaj.setEnabled(false);
                        jtfYtickSpaceMin.setEnabled(false);
                        break;
                    default: System.out.println("Fehler: rtn5j6");  break;
                }
                bestaetige();
            }
        });
        jpYscale.add(jcmYLinLog, gbc);
        gbc.gridx=2;   gbc.gridy=0;
        jpYscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=0;   gbc.gridy=1;
        jcbYautoScale= new JCheckBox();
        jcbYautoScale.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYautoScale.isSelected()) {
                    _jcbYautoScale= true;
                    // AUTO-Berechnung starten -->
                    grafer.definiereAchsenbegrenzungenImAutoZoom();
                    _jtfYmin= grafer.minY[grfIndex];   jtfYmin.setNumberToField(_jtfYmin);
                    _jtfYmax= grafer.maxY[grfIndex];   jtfYmax.setNumberToField(_jtfYmax);
                    jtfYmin.setEnabled(false);         jtfYmax.setEnabled(false);  // ist zwar deaktiviert, zeigt aber die richtigen Werte an
                } else {
                    _jcbYautoScale= false;
                    jtfYmin.setEnabled(true);   jtfYmax.setEnabled(true);
                }
                bestaetige();
            }
        });
        jpYscale.add(jcbYautoScale, gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jpYscale.add(new JLabel("Auto-Scaling"), gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpYscale.add(new JLabel(" y-MIN :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfYmin= new FormatJTextField();
        jtfYmin.setColumns(colsNumber);
        jtfYmin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYmin= jtfYmin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYscale.add(jtfYmin, gbc);
        gbc.gridx=2;   gbc.gridy=2;
        jpYscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=0;   gbc.gridy=3;
        jpYscale.add(new JLabel(" y-MAX :  "), gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jtfYmax= new FormatJTextField();
        jtfYmax.setColumns(colsNumber);
        jtfYmax.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYmax= jtfYmax.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYscale.add(jtfYmax, gbc);
        gbc.gridx=2;   gbc.gridy=3;
        jpYscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=2;   gbc.gridy=3;
        jpYscale.add(new JLabel(plenk), gbc);  // dx-Abstand
        gbc.gridx=3;   gbc.gridy=0;
        jpYscale.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=4;   gbc.gridy=0;
        jcmYlinStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmYlinStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYlinStyl= setzeLinienstilSelektiert(jcmYlinStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpYscale.add(jcmYlinStyl, gbc);
        gbc.gridx=3;   gbc.gridy=1;
        jpYscale.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=4;   gbc.gridy=1;
        jcmYlinCol= new JComboBox(GraferV3.FARBEN);
        jcmYlinCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYlinCol= setzeFarbeSelektiert(jcmYlinCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpYscale.add(jcmYlinCol, gbc);
        //
        //------------
        JPanel jpYticks= new JPanel();
        jpYticks.setLayout(new GridLayout(1,2));
        jpYticks.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Define Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        JPanel jpYTickMaj= new JPanel();
        jpYTickMaj.setLayout(new GridBagLayout());
        jpYTickMaj.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Major-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        JPanel jpYTickMin= new JPanel();
        jpYTickMin.setLayout(new GridBagLayout());
        jpYTickMin.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Minor-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        jpYticks.add(jpYTickMaj);
        jpYticks.add(jpYTickMin);
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbYAutoTickMaj= new JCheckBox();
        jcbYAutoTickMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYAutoTickMaj.isSelected()) {
                    _jtfYtickSpaceMaj= grafer.getAutoTickSpacingY(index);
                    jtfYtickSpaceMaj.setNumberToField(_jtfYtickSpaceMaj);
                    _jcbYAutoTickMaj= true;   jtfYtickSpaceMaj.setEnabled(false);   jtfYtickSpaceMin.setEnabled(false);
                } else {
                    _jcbYAutoTickMaj= false;   jtfYtickSpaceMaj.setEnabled(true);   jtfYtickSpaceMin.setEnabled(true);
                }
                bestaetige();
            }
        });
        jpYTickMaj.add(jcbYAutoTickMaj, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpYTickMaj.add(new JLabel("Auto-Spacing"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpYTickMaj.add(new JLabel("Tick-Space :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfYtickSpaceMaj= new FormatJTextField();
        jtfYtickSpaceMaj.setColumns(colsNumber);
        jtfYtickSpaceMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYtickSpaceMaj= jtfYtickSpaceMaj.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYTickMaj.add(jtfYtickSpaceMaj, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpYTickMaj.add(new JLabel("Tick-Length :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfYtickLengthMaj= new FormatJTextField();
        jtfYtickLengthMaj.setColumns(colsNumber);
        jtfYtickLengthMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYtickLengthMaj= (int)jtfYtickLengthMaj.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYTickMaj.add(jtfYtickLengthMaj, gbc);
        gbc.gridx=0;   gbc.gridy=3;
        jcbYShowLabelMaj= new JCheckBox();
        jcbYShowLabelMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYShowLabelMaj.isSelected()) {
                    _jcbYShowLabelMaj= true;
                } else {
                    _jcbYShowLabelMaj= false;
                }
                bestaetige();
            }
        });
        jpYTickMaj.add(jcbYShowLabelMaj, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jpYTickMaj.add(new JLabel("Show Label at Tick"), gbc);
        //
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        gbc.gridx=1;   gbc.gridy=0;
        jpYTickMin.add(new JLabel(" "), gbc);  // "Plenk"
        gbc.gridx=0;   gbc.gridy=1;
        jpYTickMin.add(new JLabel("Number of Ticks :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jtfYtickSpaceMin= new FormatJTextField();
        jtfYtickSpaceMin.setColumns(colsNumber);
        jtfYtickSpaceMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYtickSpaceMin= (int)jtfYtickSpaceMin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYTickMin.add(jtfYtickSpaceMin, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpYTickMin.add(new JLabel("Tick-Length :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jtfYtickLengthMin= new FormatJTextField();
        jtfYtickLengthMin.setColumns(colsNumber);
        jtfYtickLengthMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                try {
                    _jtfYtickLengthMin= (int)jtfYtickLengthMin.getNumberFromField();
                    bestaetige();
                } catch (Exception e) {}
            }
        });
        jpYTickMin.add(jtfYtickLengthMin, gbc);
        gbc.gridx=0;   gbc.gridy=3;
        jcbYShowLabelMin= new JCheckBox();
        jcbYShowLabelMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYShowLabelMin.isSelected()) {
                    _jcbYShowLabelMin= true;
                } else {
                    _jcbYShowLabelMin= false;
                }
                bestaetige();
            }
        });
        jpYTickMin.add(jcbYShowLabelMin, gbc);
        gbc.gridx=1;   gbc.gridy=3;
        jpYTickMin.add(new JLabel("Show Label at Tick"), gbc);
        //
        //------------
        JPanel jpYgrid= new JPanel();
        jpYgrid.setLayout(new GridLayout(1,2));
        jpYgrid.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Grid Normal to Y-Axis", TitledBorder.LEFT, TitledBorder.TOP));
        //------------
        JPanel jpYGridMaj= new JPanel();
        jpYGridMaj.setLayout(new GridBagLayout());
        jpYGridMaj.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "At Major-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        JPanel jpYGridMin= new JPanel();
        jpYGridMin.setLayout(new GridBagLayout());
        jpYGridMin.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "At Minor-Ticks", TitledBorder.LEFT, TitledBorder.TOP));
        jpYgrid.add(jpYGridMaj);
        jpYgrid.add(jpYGridMin);
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbYShowGridMaj= new JCheckBox();
        jcbYShowGridMaj.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYShowGridMaj.isSelected()) {
                    jcmYgridMajStyl.setEnabled(true);  jcmYgridMajCol.setEnabled(true);   _jcbYShowGridMaj=true;
                } else {
                    jcmYgridMajStyl.setEnabled(false);  jcmYgridMajCol.setEnabled(false);   _jcbYShowGridMaj=false;
                }
                bestaetige();
            }
        });
        jpYGridMaj.add(jcbYShowGridMaj, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpYGridMaj.add(new JLabel("Show Grid Line"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpYGridMaj.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcmYgridMajStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmYgridMajStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYgridMajStyl= setzeLinienstilSelektiert(jcmYgridMajStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpYGridMaj.add(jcmYgridMajStyl, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpYGridMaj.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmYgridMajCol= new JComboBox(GraferV3.FARBEN);
        jcmYgridMajCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYgridMajCol= setzeFarbeSelektiert(jcmYgridMajCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpYGridMaj.add(jcmYgridMajCol, gbc);
        //
        //------------
        gbc.gridx=0;   gbc.gridy=0;
        jcbYShowGridMin= new JCheckBox();
        jcbYShowGridMin.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                if (jcbYShowGridMin.isSelected()) {
                    jcmYgridMinStyl.setEnabled(true);  jcmYgridMinCol.setEnabled(true);   _jcbYShowGridMin=true;
                } else {
                    jcmYgridMinStyl.setEnabled(false);  jcmYgridMinCol.setEnabled(false);   _jcbYShowGridMin=false;
                }
                bestaetige();
            }
        });
        jpYGridMin.add(jcbYShowGridMin, gbc);
        gbc.gridx=1;   gbc.gridy=0;
        jpYGridMin.add(new JLabel("Show Grid Line"), gbc);
        gbc.gridx=0;   gbc.gridy=1;
        jpYGridMin.add(new JLabel("Line Style :  "), gbc);
        gbc.gridx=1;   gbc.gridy=1;
        jcmYgridMinStyl= new JComboBox(GraferV3.LINIEN_STIL);
        jcmYgridMinStyl.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYgridMinStyl= setzeLinienstilSelektiert(jcmYgridMinStyl.getSelectedIndex());
                bestaetige();
            }
        });
        jpYGridMin.add(jcmYgridMinStyl, gbc);
        gbc.gridx=0;   gbc.gridy=2;
        jpYGridMin.add(new JLabel("Line Color :  "), gbc);
        gbc.gridx=1;   gbc.gridy=2;
        jcmYgridMinCol= new JComboBox(GraferV3.FARBEN);
        jcmYgridMinCol.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                _jcmYgridMinCol= setzeFarbeSelektiert(jcmYgridMinCol.getSelectedIndex());
                bestaetige();
            }
        });
        jpYGridMin.add(jcmYgridMinCol, gbc);
        //
        //------------
        JPanel jpY= new JPanel();
        jpY.setLayout(new BorderLayout());
        jpY.add(jpYscale, BorderLayout.NORTH);
        jpY.add(jpYticks, BorderLayout.CENTER);
        jpY.add(jpYgrid, BorderLayout.SOUTH);
        jtbACH.addTab("Y-Axis", jpY);
        //
        //==========================================================================================
        //==========================================================================================
        // Gesamt-Panel -->
        //
        JPanel pGes= new JPanel();
        pGes.setLayout(new BorderLayout());
        pGes.add(pACH, BorderLayout.CENTER);
        //
        return pGes;
        //--------------------
    }




    private int setzeFarbListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.BLACK:     return  0;
            case GraferV3.RED:       return  1;
            case GraferV3.GREEN:     return  2;
            case GraferV3.BLUE:      return  3;
            case GraferV3.DARKGRAY:  return  4;
            case GraferV3.GRAY:      return  5;
            case GraferV3.LIGTHGRAY: return  6;
            case GraferV3.WHITE:     return  7;
            case GraferV3.MAGENTA:   return  8;
            case GraferV3.CYAN:      return  9;
            case GraferV3.ORANGE:    return 10;
            case GraferV3.YELLOW:    return 11;
            case GraferV3.DARKGREEN: return 12;
            default:
                System.out.println("Fehler: 49785zognn");
                return -1;
        }
        //--------------------
    }


    private int setzeFarbeSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.BLACK;
            case  1:  return GraferV3.RED;
            case  2:  return GraferV3.GREEN;
            case  3:  return GraferV3.BLUE;
            case  4:  return GraferV3.DARKGRAY;
            case  5:  return GraferV3.GRAY;
            case  6:  return GraferV3.LIGTHGRAY;
            case  7:  return GraferV3.WHITE;
            case  8:  return GraferV3.MAGENTA;
            case  9:  return GraferV3.CYAN;
            case 10:  return GraferV3.ORANGE;
            case 11:  return GraferV3.YELLOW;
            case 12:  return GraferV3.DARKGREEN;
            default:
                System.out.println("Fehler: drdzmndzn"); 
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilListe (int ii) {
        //--------------------
        switch (ii) {
            case GraferV3.SOLID_PLAIN:  return  0;
            case GraferV3.INVISIBLE:    return  1;
            case GraferV3.SOLID_FAT_1:  return  2;
            case GraferV3.SOLID_FAT_2:  return  3;
            case GraferV3.DOTTED_PLAIN: return  4;
            case GraferV3.DOTTED_FAT:   return  5;
            default:
                System.out.println("Fehler: j9g3rr98g3mm");
                return -1;
        }
        //--------------------
    }


    private int setzeLinienstilSelektiert (int ii) {
        //--------------------
        switch (ii) {
            case  0:  return GraferV3.SOLID_PLAIN;
            case  1:  return GraferV3.INVISIBLE;
            case  2:  return GraferV3.SOLID_FAT_1;
            case  3:  return GraferV3.SOLID_FAT_2;
            case  4:  return GraferV3.DOTTED_PLAIN;
            case  5:  return GraferV3.DOTTED_FAT;
            default:
                System.out.println("Fehler: 873bgf"); 
                return -1;
        }
        //--------------------
    }



    private void werteVonFeldernEinlesen () {
        //----------------------------
        // wenn nicht 'Return' gedrueckt wurde, muessen die Feld-Werte an dieser Stelle explizit eingelesen werden,
        // bevor das Fenster verlassen wird -->
        //
        try { _jtfXmin= jtfXmin.getNumberFromField(); } catch (Exception e) {}
        try { _jtfXmax= jtfXmax.getNumberFromField(); } catch (Exception e) {}
        try { _jtfXtickSpaceMaj=  jtfXtickSpaceMaj.getNumberFromField(); } catch (Exception e) {}
        try { _jtfXtickSpaceMin=  (int)jtfXtickSpaceMin.getNumberFromField();  } catch (Exception e) {}
        try { _jtfXtickLengthMaj= (int)jtfXtickLengthMaj.getNumberFromField(); } catch (Exception e) {}
        try { _jtfXtickLengthMin= (int)jtfXtickLengthMin.getNumberFromField(); } catch (Exception e) {}
        //
        try { _jtfYmin= jtfYmin.getNumberFromField(); } catch (Exception e) {}
        try { _jtfYmax= jtfYmax.getNumberFromField(); } catch (Exception e) {}
        try { _jtfYtickSpaceMaj=  jtfYtickSpaceMaj.getNumberFromField(); } catch (Exception e) {}
        try { _jtfYtickSpaceMin=  (int)jtfYtickSpaceMin.getNumberFromField();  } catch (Exception e) {}
        try { _jtfYtickLengthMaj= (int)jtfYtickLengthMaj.getNumberFromField(); } catch (Exception e) {}
        try { _jtfYtickLengthMin= (int)jtfYtickLengthMin.getNumberFromField(); } catch (Exception e) {}
        //----------------------------
    }




    private void bestaetige () {
        //----------------------------
        // lokal gespeicherte Parameter werden an das Diagramm zurueckgemeldet -->
        //
        for (int i1=0;  i1<GraferImplementation.ANZ_DIAGRAM_MAX;  i1++) {
            if (i1==grafer.getAnzahlSichtbarerDiagramme()-1) {
                // nur das unteste sichtbare Diagramm hat eine x-Achse -->
                grafer.showAxisX[i1]= true;  // _jcbShowAxisX;
                grafer.minX[i1]= _jtfXmin;
                grafer.maxX[i1]= _jtfXmax;
                grafer.autoScaleX[i1]= _jcbXautoScale;
                grafer.xAchseStil[i1]= _jcmXlinStyl;
                grafer.xAchseFarbe[i1]= _jcmXlinCol;
                grafer.xAchsenTyp[i1]= _jcmXLinLog;
                grafer.xTickAutoSpacing[i1]= _jcbXAutoTickMaj;
                grafer.xTickSpacing[i1]= _jtfXtickSpaceMaj;
                grafer.xAnzTicksMinor[i1]= _jtfXtickSpaceMin;
                grafer.xTickLaenge[i1]= _jtfXtickLengthMaj;
                grafer.xTickLaengeMinor[i1]= _jtfXtickLengthMin;
                grafer.zeigeLabelsXmaj[i1]= _jcbXShowLabelMaj;
                grafer.zeigeLabelsXmin[i1]= _jcbXShowLabelMin;
                grafer.xShowGridMaj[i1]= _jcbXShowGridMaj;
                grafer.xShowGridMin[i1]= _jcbXShowGridMin;
                grafer.linStilGridNormalX[i1]= _jcmXgridMajStyl;
                grafer.linStilGridNormalXminor[i1]= _jcmXgridMinStyl;
                grafer.farbeGridNormalX[i1]= _jcmXgridMajCol;
                grafer.farbeGridNormalXminor[i1]= _jcmXgridMinCol;
                grafer.ORIGjcmXlinCol[i1]= _ORIGjcmXlinCol;
                grafer.ORIGjcmXlinStyl[i1]= _ORIGjcmXlinStyl;
                grafer.ORIGjtfXtickLengthMaj[i1]= _ORIGjtfXtickLengthMaj;
                grafer.ORIGjtfXtickLengthMin[i1]= _ORIGjtfXtickLengthMin;
                grafer.ORIGjcbXShowLabelMaj[i1]= _ORIGjcbXShowLabelMaj;
                grafer.ORIGjcbXShowLabelMin[i1]= _ORIGjcbXShowLabelMin;
                grafer.ORIGjcbXShowGridMaj[i1]= _ORIGjcbXShowGridMaj;
                grafer.ORIGjcbXShowGridMin[i1]= _ORIGjcbXShowGridMin;
            } else {
                // alle sichtbaren Diagramme ausser dem untesten Diagramm werden mit unsichtbarer x-Achse dargestelllt -->
                grafer.showAxisX[i1]= false;
                grafer.minX[i1]= _jtfXmin;
                grafer.maxX[i1]= _jtfXmax;
                grafer.autoScaleX[i1]= _jcbXautoScale;
                grafer.xAchseStil[i1]= GraferV3.INVISIBLE;  // _jcmXlinStyl;
                grafer.xAchseFarbe[i1]= _jcmXlinCol;
                grafer.xAchsenTyp[i1]= _jcmXLinLog;
                grafer.xTickAutoSpacing[i1]= _jcbXAutoTickMaj;
                grafer.xTickSpacing[i1]= _jtfXtickSpaceMaj;
                grafer.xAnzTicksMinor[i1]= _jtfXtickSpaceMin;
                grafer.xTickLaenge[i1]= 0;
                grafer.xTickLaengeMinor[i1]= 0;
                grafer.zeigeLabelsXmaj[i1]= false;
                grafer.zeigeLabelsXmin[i1]= false;
                grafer.xShowGridMaj[i1]= _jcbXShowGridMaj;
                grafer.xShowGridMin[i1]= _jcbXShowGridMin;
                grafer.linStilGridNormalX[i1]= _jcmXgridMajStyl;
                grafer.linStilGridNormalXminor[i1]= _jcmXgridMinStyl;
                grafer.farbeGridNormalX[i1]= _jcmXgridMajCol;
                grafer.farbeGridNormalXminor[i1]= _jcmXgridMinCol;
                // --> ?
                grafer.ORIGjcmXlinCol[i1]= _ORIGjcmXlinCol;
                grafer.ORIGjcmXlinStyl[i1]= _ORIGjcmXlinStyl;
                grafer.ORIGjtfXtickLengthMaj[i1]= _ORIGjtfXtickLengthMaj;
                grafer.ORIGjtfXtickLengthMin[i1]= _ORIGjtfXtickLengthMin;
                grafer.ORIGjcbXShowLabelMaj[i1]= _ORIGjcbXShowLabelMaj;
                grafer.ORIGjcbXShowLabelMin[i1]= _ORIGjcbXShowLabelMin;
                grafer.ORIGjcbXShowGridMaj[i1]= _ORIGjcbXShowGridMaj;
                grafer.ORIGjcbXShowGridMin[i1]= _ORIGjcbXShowGridMin;
            }
        }
        //------------------------
        // y-Achsen sind individuell -->
        grafer.jcbShowLegende[grfIndex]= _jcbShowLegende;
        grafer.showAxisY[grfIndex]= _jcbShowAxisY;
        grafer.minY[grfIndex]= _jtfYmin;
        grafer.maxY[grfIndex]= _jtfYmax;
        grafer.autoScaleY[grfIndex]= _jcbYautoScale;
        grafer.yAchseStil[grfIndex]= _jcmYlinStyl;
        grafer.yAchseFarbe[grfIndex]= _jcmYlinCol;
        grafer.yAchsenTyp[grfIndex]= _jcmYLinLog;
        grafer.yTickAutoSpacing[grfIndex]= _jcbYAutoTickMaj;
        grafer.yTickSpacing[grfIndex]= _jtfYtickSpaceMaj;
        grafer.yAnzTicksMinor[grfIndex]= _jtfYtickSpaceMin;
        grafer.yTickLaenge[grfIndex]= _jtfYtickLengthMaj;
        grafer.yTickLaengeMinor[grfIndex]= _jtfYtickLengthMin;
        grafer.zeigeLabelsYmaj[grfIndex]= _jcbYShowLabelMaj;
        grafer.zeigeLabelsYmin[grfIndex]= _jcbYShowLabelMin;
        grafer.yShowGridMaj[grfIndex]= _jcbYShowGridMaj;
        grafer.yShowGridMin[grfIndex]= _jcbYShowGridMin;
        grafer.linStilGridNormalY[grfIndex]= _jcmYgridMajStyl;
        grafer.linStilGridNormalYminor[grfIndex]= _jcmYgridMinStyl;
        grafer.farbeGridNormalY[grfIndex]= _jcmYgridMajCol;
        grafer.farbeGridNormalYminor[grfIndex]= _jcmYgridMinCol;
        grafer.ORIGjcmYlinCol[grfIndex]= _ORIGjcmYlinCol;
        grafer.ORIGjcmYlinStyl[grfIndex]= _ORIGjcmYlinStyl;
        grafer.ORIGjtfYtickLengthMaj[grfIndex]= _ORIGjtfYtickLengthMaj;
        grafer.ORIGjtfYtickLengthMin[grfIndex]= _ORIGjtfYtickLengthMin;
        grafer.ORIGjcbYShowLabelMaj[grfIndex]= _ORIGjcbYShowLabelMaj;
        grafer.ORIGjcbYShowLabelMin[grfIndex]= _ORIGjcbYShowLabelMin;
        grafer.ORIGjcbYShowGridMaj[grfIndex]= _ORIGjcbYShowGridMaj;
        grafer.ORIGjcbYShowGridMin[grfIndex]= _ORIGjcbYShowGridMin;
        //
        //----------------------------
        grafer.setzeAchsen();
        grafer.repaint();
    }




    //-----------------------------
    public void windowClosing (WindowEvent we) { this.dispose(); }
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowOpened (WindowEvent we) {}
    //-----------------------------

}


