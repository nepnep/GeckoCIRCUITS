/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI; 
import gecko.GeckoCIRCUITS.allg.TechFormat;

import java.awt.Font;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.ImageIcon;
import javax.swing.JDialog; 
import javax.swing.JMenuBar; 
import javax.swing.JComboBox; 
import javax.swing.JTextArea; 
import javax.swing.JButton; 
import java.net.URL;




public class DialogEditText extends JDialog implements WindowListener {

    //-------------
    public static final String[] FONT_NAMEN= new String[]{"Arial", "Times New Roman", "Courier", "Symbol"}; 
    public static final String[] FONT_GROESSE= new String[]{"6","8","10","12","14","16","18","20","24","28","32","36","40"}; 
    public static final String[] FONT_STIL= new String[]{"Plain", "Bold", "Italic"}; 
    //-------------
    private EditableText callback; 
    private GridBagConstraints gbc= new GridBagConstraints();
    private TechFormat cf= new TechFormat();
    private int DX_TXT=60, DY_TXT=80; 
    private JTextArea ta; 
    //-------------
    // Temporaere Aenderung - muss erst durch 'OK' bestaetigt werden: 
    private String txtTEMP; 
    private String txtColorForeTEMP, txtColorBackTEMP; 
    private String txtFontNameTEMP, txtFontSizeTEMP, txtFontStilTEMP; 
    //-------------


    public static int getIndexForFontNameSelector (String fx) {
        for (int i1=0;  i1<DialogEditText.FONT_NAMEN.length;  i1++) 
            if (DialogEditText.FONT_NAMEN[i1].equals(fx)) return i1; 
        return -1; 
    }
    public static int getIndexForFontSizeSelector (String fx) {
        for (int i1=0;  i1<DialogEditText.FONT_GROESSE.length;  i1++) 
            if (DialogEditText.FONT_GROESSE[i1].equals(fx)) return i1; 
        return -1; 
    }
    public static int getIndexForFontStyleSelector (String fx) {
        for (int i1=0;  i1<DialogEditText.FONT_STIL.length;  i1++) 
            if (DialogEditText.FONT_STIL[i1].equals(fx)) return i1; 
        return -1; 
    }

    
    
    public DialogEditText (EditableText callback) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        this.callback=callback; 
        //--------------------------
        /*
        Font[] f= GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts(); 
        for (int i1=0;  i1<f.length;  i1++) 
            System.out.println(f[i1].getFontName()+"\t"+f[i1].getFamily()+"\t"+f[i1].getName()); 
        */
        //--------------------------
        switch (callback.getStatus()) {
            case EditableText.STAUS_NEU:
                // default-Werte: 
                txtTEMP=""; 
                txtColorForeTEMP= GraferV3.FARBEN[0];  // black 
                txtColorBackTEMP= GraferV3.FARBEN[7];  // white
                txtFontNameTEMP= DialogEditText.FONT_NAMEN[0];    // Arial
                txtFontSizeTEMP= DialogEditText.FONT_GROESSE[3];  // 12pkt
                txtFontStilTEMP= DialogEditText.FONT_STIL[0];     // Plain
                break;
            case EditableText.STAUS_IN_BEARBEITUNG:
                // die originalen Werte als Kopien uebernehmen: 
                txtTEMP= callback.getPlainText(); 
                txtColorForeTEMP= callback.getColorFore(); 
                txtColorBackTEMP= callback.getColorBack(); 
                txtFontNameTEMP= callback.getFontName(); 
                txtFontSizeTEMP= callback.getFontSize(); 
                txtFontStilTEMP= callback.getFontStil(); 
                break;
            default: break; 
        }
        //--------------------------
        this.setTitle(Typ.spTitle+"Edit Text");
        this.baueGUI();
        this.pack();
        this.setResizable(true);
        int x0= (Typ.X_SCREEN+Typ.DX_SCOPE) +(callback.getPositionX()+DX_TXT); 
        int y0= (Typ.Y_SCREEN+Typ.DY_SCOPE) +(callback.getPositionY()+DY_TXT); 
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(x0,y0);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setVisible(true);
        //------------------------
    }    



    private void baueGUI () {
        //------------------
        Container con= this.getContentPane();
        con.setLayout(new BorderLayout());
        //=======================================
        final JComboBox jcbFont= new JComboBox(DialogEditText.FONT_NAMEN); 
        jcbFont.setSelectedIndex(DialogEditText.getIndexForFontNameSelector(txtFontNameTEMP)); 
        jcbFont.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                txtFontNameTEMP= jcbFont.getSelectedItem().toString(); 
                updateFont(); 
            } 
        }); 
        final JComboBox jcbSize= new JComboBox(DialogEditText.FONT_GROESSE); 
        jcbSize.setSelectedIndex(DialogEditText.getIndexForFontSizeSelector(txtFontSizeTEMP)); 
        jcbSize.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                txtFontSizeTEMP= jcbSize.getSelectedItem().toString(); 
                updateFont(); 
            } 
        }); 
        final JComboBox jcbStyle= new JComboBox(DialogEditText.FONT_STIL); 
        jcbStyle.setSelectedIndex(DialogEditText.getIndexForFontStyleSelector(txtFontStilTEMP)); 
        jcbStyle.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                txtFontStilTEMP= jcbStyle.getSelectedItem().toString(); 
                updateFont(); 
            } 
        }); 
        final JComboBox jcbColFront= new JComboBox(GraferV3.FARBEN); 
        jcbColFront.setSelectedIndex(GraferV3.getIndexForColorSelector(txtColorForeTEMP)); 
        jcbColFront.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                txtColorForeTEMP= jcbColFront.getSelectedItem().toString(); 
                ta.setForeground(GraferV3.getSelectedColor(txtColorForeTEMP)); 
            } 
        }); 
        final JComboBox jcbColBack= new JComboBox(GraferV3.FARBEN); 
        jcbColBack.setSelectedIndex(GraferV3.getIndexForColorSelector(txtColorBackTEMP)); 
        jcbColBack.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                txtColorBackTEMP= jcbColBack.getSelectedItem().toString(); 
                ta.setBackground(GraferV3.getSelectedColor(txtColorBackTEMP)); 
            } 
        }); 
        JMenuBar menuBar= new JMenuBar();
        menuBar.add(new JLabel("Properties: "));
        menuBar.add(jcbFont);
        menuBar.add(jcbSize);
        menuBar.add(jcbStyle);
        menuBar.add(jcbColFront);
        menuBar.add(jcbColBack);
        this.setJMenuBar(menuBar);
        //=======================================
        JPanel pP1= new JPanel();
        pP1.setLayout(new BorderLayout());
        pP1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x_DialogEditText, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        ta= new JTextArea(); 
        ta.setLineWrap(false);  // weil man sonst Umbrueche im Dialog hat, nicht aber im SCOPE 
        ta.setRows(3); 
        this.updateFont();
        pP1.add(ta, BorderLayout.CENTER);
        //=======================================
        JPanel pP2= new JPanel(); 
        pP2.setLayout(new GridBagLayout()); 
        JButton jbDelete= new JButton("Delete Text"); 
        jbDelete.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                switch (callback.getStatus()) {
                    case EditableText.STAUS_NEU:
                        // falls neuer Texteintrag: Nicht zum Vector hinzufuegen & Fenster zumachen --> 
                        schliesseFenster(); 
                        break;
                    case EditableText.STAUS_IN_BEARBEITUNG:
                        // falls Modifizierung: Aus den Vector entfernen & Fenster zumachen --> 
                        callback.getVectorAlleTextEintraege().removeElementAt(callback.getZeigerImVector()); 
                        schliesseFenster(); 
                        break;
                    default: break; 
                }
                
            } 
        }); 
        JButton jbOK= new JButton("OK"); 
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                // zuerst in jedem Fall die neuen Werte bestaetigen --> 
                callback.setFontName(txtFontNameTEMP); 
                callback.setFontSize(txtFontSizeTEMP); 
                callback.setFontStil(txtFontStilTEMP); 
                callback.setColorFore(txtColorForeTEMP); 
                callback.setColorBack(txtColorBackTEMP); 
                txtTEMP= ta.getText(); 
                callback.setPlainText(txtTEMP); 
                //--------------
                switch (callback.getStatus()) {
                    case EditableText.STAUS_NEU:
                        // falls neuer Texteintrag: als Neueintrag zum Vector hinzufuegen & Fenster zumachen --> 
                        callback.getVectorAlleTextEintraege().addElement(callback); 
                        schliesseFenster(); 
                        break;
                    case EditableText.STAUS_IN_BEARBEITUNG:
                        // falls Modifizierung: gegen den alten Texteintrag im Vector austauschen & Fenster zumachen --> 
                        callback.getVectorAlleTextEintraege().setElementAt(callback, callback.getZeigerImVector()); 
                        schliesseFenster(); 
                        break;
                    default: break; 
                }
                
            } 
        }); 
        JButton jbCancel= new JButton("Cancel"); 
        jbCancel.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                switch (callback.getStatus()) {
                    case EditableText.STAUS_NEU:
                        // falls neuer Texteintrag: Nicht zum Vector hinzufuegen & Fenster zumachen --> 
                        schliesseFenster(); 
                        break;
                    case EditableText.STAUS_IN_BEARBEITUNG:
                        // falls Modifizierung: unveraendert im Vector drinnenlassen & Fenster zumachen --> 
                        schliesseFenster(); 
                        break;
                    default: break; 
                }
                
            } 
        }); 
        gbc= new GridBagConstraints(); 
        gbc.gridx=0;   gbc.gridy=0; 
        pP2.add(jbDelete, gbc); 
        gbc.gridx=1;   gbc.gridy=0; 
        pP2.add(jbOK, gbc); 
        gbc.gridx=2;   gbc.gridy=0; 
        pP2.add(jbCancel, gbc); 
        //=======================================
        con.add(pP1, BorderLayout.CENTER);
        con.add(pP2, BorderLayout.SOUTH);
        //=======================================
    }



    private void updateFont () {
        String f1= txtFontNameTEMP; 
        int f2= -1; 
        if (txtFontStilTEMP.equals(DialogEditText.FONT_STIL[0])) f2=Font.PLAIN; 
        else if (txtFontStilTEMP.equals(DialogEditText.FONT_STIL[1])) f2=Font.BOLD; 
        else if (txtFontStilTEMP.equals(DialogEditText.FONT_STIL[2])) f2=Font.ITALIC; 
        int f3= (new Integer(txtFontSizeTEMP)).intValue(); 
        ta.setFont(new Font(f1,f2,f3));
    }


    //------------------------------------------------
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowClosing (WindowEvent we) { this.schliesseFenster(); }
    public void windowOpened (WindowEvent we) {}
    //------------------------------------------------

    public void schliesseFenster () {
        this.dispose();
    }




}

