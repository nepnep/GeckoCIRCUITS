/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.scope;

/**
 *
 * @author andy
 */
public class DataJunkSimple implements DataJunk {

    private double[][] _data;
    private int _startIndex;

    public DataJunkSimple(int startIndex, int rows, int columns) {
        _data = new double[rows][columns];
        _startIndex = startIndex;
    }

    public double getValue(int row, int index) {
        assert index - _startIndex < _data[0].length : _startIndex + " " + index;
        assert index - _startIndex >= 0;

        return _data[row][index - _startIndex];
    }

    public void setValue(double value, int row, int column) {
        _data[row][column - _startIndex] = value;
    }

    

    public HiLoData getHiLoValue(int row, int columnStart, int columnStop) {
        assert columnStart <= columnStop;   

        HiLoData returnValue = new HiLoData();
        int startIndex = columnStart - _startIndex;
        int maxIndex = columnStop - _startIndex;

        startIndex = Math.max(0, startIndex);
        maxIndex = Math.min(maxIndex, _data[0].length);
        
        if(startIndex >= maxIndex) {
            throw new ArrayIndexOutOfBoundsException();
        }
        
        for(int i = startIndex; i < maxIndex; i++) {
            returnValue.insertCompare((float) _data[row][i]);
        }
        
        return returnValue;
    }

    public int getStartIndex() {
        return _startIndex;
    }

    public int getStopIndex() {
        return _startIndex + _data[0].length;
    }


}
