/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.scope;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.allg.TxtI;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JDialog; 
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Container;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import java.net.URL;




public class DialogOrdnungSIGNAL extends JDialog implements WindowListener, MouseListener, MouseMotionListener {

    //----------------------------
    private GraferImplementation grafer;
    private JTable table;
    private Container c;
    //
    private String[] signalNamen, namenDerSignaleDesSelektiertenGraphen;
    private int grfIndex;
    private int[] indexDerKurveInDerMatrix;
    // zur Bearbeitung von JTable:
    private int rowSelektiert=-1, rowZiel=-1;
    private String gewaehltesSignal= "";
    private int xGewSGN=-1, yGewSGN=-1;  // Position der Markierung des ausgewaehlten Signals
    //
    private int[] posName;
    //----------------------------



    public DialogOrdnungSIGNAL (int grfIndex, GraferImplementation grafer) {
        super.setModal(true);
        try { this.setIconImage((new ImageIcon(new URL(Typ.PFAD_PICS_URL,"gecko.gif"))).getImage()); } catch (Exception e) {}
        this.grfIndex= grfIndex;
        this.grafer= grafer;
        this.signalNamen= grafer.signalNamen;
        this.indexDerKurveInDerMatrix= grafer.indexDerKurveInDerMatrix;
        //
        this.ermittleNamenDerSIGNALeDesGraphen();
        //----------------
        this.baueGUI();
        this.pack();
        if (Typ.mode_location==Typ.LOCATION_BY_PROGRAM_A)
            this.setLocation(grafer.getScope().getX_PositionAbsolut_DialogConnect()+Typ.DX_SCOPE_DLG2, grafer.getScope().getY_PositionAbsolut_DialogConnect()+Typ.DY_SCOPE_DLG2);
        else if (Typ.mode_location==Typ.LOCATION_BY_PLATFORM)
            this.setLocationByPlatform(true);
        this.setResizable(false);
        this.setVisible(true);
        //----------------
    }



    private void ermittleNamenDerSIGNALeDesGraphen () {
        int[] posOrig= grafer.getPositionSIGNAL();
        String[] name= new String[indexDerKurveInDerMatrix.length];
        posName= new int[indexDerKurveInDerMatrix.length];
        int z=0;
        for (int i1=0;  i1<indexDerKurveInDerMatrix.length;  i1++) {
            int grf= indexDerKurveInDerMatrix[i1]/1000;
            int yNr= indexDerKurveInDerMatrix[i1]%1000;
            if (grf==grfIndex) {
                name[z]= signalNamen[yNr];
                posName[z]= posOrig[i1];
                z++;
            }
        }
        namenDerSignaleDesSelektiertenGraphen= new String[z];
        for (int i1=0;  i1<z;  i1++)
            namenDerSignaleDesSelektiertenGraphen[i1]= name[posName[i1]];
        //------------------------
         for (int i1=0;  i1<namenDerSignaleDesSelektiertenGraphen.length;  i1++) System.out.println(i1+"   "+namenDerSignaleDesSelektiertenGraphen[i1]);  System.out.println("...");
    }



    private void baueGUI () {
        //================
        Object[][] wsObj= new Object[namenDerSignaleDesSelektiertenGraphen.length][1];
        for (int i1=0;  i1<wsObj.length;  i1++) {
            wsObj[i1][0]= namenDerSignaleDesSelektiertenGraphen[i1];
        }
        //
        table= new JTable(wsObj, new String[]{"Signals"}) {
            public boolean isCellEditable (int row, int col) { return false; }
            public void paint (Graphics g) {
                super.paint(g);
                //------------
                Color f= g.getColor();  // urspruengliche Farbe speichern
                g.setColor(Color.green);
                g.drawString(gewaehltesSignal, xGewSGN, yGewSGN);  // Drag&Drop-Einfuege-Markierung
                g.setColor(f);  // urspruengliche Farbe wiederherstellen
            }
        };
        //
        table.setSelectionBackground(Color.white);
        table.addMouseListener(this);
        table.addMouseMotionListener(this);
        //------------------------
        JPanel jpTable= new JPanel();
        jpTable.setLayout(new BorderLayout());
        jpTable.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x_DialogOrdnungSIGNAL, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        jpTable.add(new JScrollPane(table), BorderLayout.CENTER);
        jpTable.setPreferredSize(new Dimension(80, 70+table.getRowCount()*table.getRowHeight()));
        //================
        JPanel jpTxt= new JPanel();
        jpTxt.setLayout(new BorderLayout());
        jpTxt.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), TxtI.ti_x13_DialogElementCONTROL, TitledBorder.LEFT, TitledBorder.TOP, TxtI.ti_Font_border));
        JTextArea jtx= new JTextArea();
        jtx.setFont(TxtI.ti_Font_C); 
        jtx.setText(TxtI.ti_txt_DialogOrdnungSIGNAL);
        jtx.setLineWrap(true);
        jtx.setWrapStyleWord(true);
        jtx.setBackground(this.getBackground());
        jtx.setEditable(false);
        jtx.setRows(5);
        jpTxt.add(jtx, BorderLayout.CENTER);
        //================
        JPanel jpOK= new JPanel();
        jpOK.setLayout(new BorderLayout());
        JButton jbOK= new JButton(TxtI.ti_OK);
        jbOK.setFont(TxtI.ti_Font_A); 
        jbOK.addActionListener(new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                dispose();
            }
        });
        jpOK.add(jbOK, BorderLayout.CENTER);
        //--------------------
        c= this.getContentPane();
        c.removeAll();
        c.setLayout(new BorderLayout());
        c.add(jpTxt, BorderLayout.NORTH);
        c.add(jpTable, BorderLayout.CENTER);
        c.add(jpOK, BorderLayout.SOUTH);
        //--------------------
    }



    //-----------------------------
    public void mousePressed (MouseEvent me) {
        int mx=me.getX(), my=me.getY();
        double cellHoehe= table.getCellRect(0,0,true).getHeight();
        rowSelektiert= (int)(my/cellHoehe);
        gewaehltesSignal= "<< " +(String)(table.getModel().getValueAt(rowSelektiert,0));
        xGewSGN=mx;   yGewSGN=my;
        table.repaint();
    }
    public void mouseReleased (MouseEvent me) {
        if (rowSelektiert==-1) return;
        int mx=me.getX(), my=me.getY();
        double cellHoehe= table.getCellRect(0,0,true).getHeight();
        rowZiel= (int)(my/cellHoehe);
        //------------------
        this.verschiebeReihenEntsprechendSelektion();
        rowSelektiert=-1;
        rowZiel=-1;
        gewaehltesSignal= "";
        xGewSGN=-1;   yGewSGN=-1;
        //------------------
        table.repaint();
    }
    public void mouseDragged (MouseEvent me) {
        if (rowSelektiert==-1) return;
        int mx=me.getX(), my=me.getY();
        int grenzeX=15;  // weiter nach links kann der Text nicht geschoben werden
        if (mx<=grenzeX) mx=grenzeX;
        xGewSGN=mx;   yGewSGN=my;
        table.repaint();
    }

    public void mouseEntered (MouseEvent me) {}
    public void mouseExited (MouseEvent me) {}
    public void mouseClicked (MouseEvent me) {}
    public void mouseMoved (MouseEvent me) {}
    //-----------------------------



    private void verschiebeReihenEntsprechendSelektion () {
        //-----------------------------
        if (rowSelektiert==rowZiel) return;
        try {
            if (rowSelektiert>rowZiel) {  // selektierte Zelle wird nach oben geschoben
                Object zuVerschieben= table.getModel().getValueAt(rowSelektiert,0);
                for (int i1=rowSelektiert;  i1>rowZiel;  i1--) {
                    Object temp= table.getModel().getValueAt(i1-1,0);
                    table.getModel().setValueAt(temp,i1,0);
                }
                table.getModel().setValueAt(zuVerschieben,rowZiel,0);
            } else {  // selektierte Zelle wird nach unten geschoben
                Object zuVerschieben= table.getModel().getValueAt(rowSelektiert,0);
                for (int i1=rowSelektiert;  i1<rowZiel;  i1++) {
                    Object temp= table.getModel().getValueAt(i1+1,0);
                    table.getModel().setValueAt(temp,i1,0);
                }
                table.getModel().setValueAt(zuVerschieben,rowZiel,0);
            }
            this.ermittleOrdnung();
        } catch (ArrayIndexOutOfBoundsException e) {}
        //-----------------------------
    }



    private void ermittleOrdnung () {
        //-----------------------------
        String[] listeNamen= new String[namenDerSignaleDesSelektiertenGraphen.length];  // aktuelle Ordnung der Tabelle
        for (int i1=0;  i1<table.getRowCount();  i1++) {
            listeNamen[i1]= (String)(table.getValueAt(i1,0));
            System.out.println(i1+"   "+listeNamen[i1]+"   original --> "+namenDerSignaleDesSelektiertenGraphen[i1]);
        }
        System.out.println("----");
        //-----------------------------
        int[] posOrig= grafer.getPositionSIGNAL();  // derzeitige Position ALLER Kurven
        /*
        int[] posAktuellGRF= new int[namenDerSignaleDesSelektiertenGraphen.length];
        int z=0;
        // der zum Graph 'grfIndex' gehoerige Abschnitt muss herausgenommen werden:
        for (int i1=0;  i1<pos.length;  i1++) {
            if (indexDerKurveInDerMatrix[i1]/1000==grfIndex) {
                posAktuellGRF[z]= pos[i1];
                z++;
            }
        }
        posAktuellGRF= posName;
        */
        // dieser Abschnitt muss entsprechend der Umordnung der Signal-Namen umgeordnet werden:
        int[] posNeuGRF= new int[namenDerSignaleDesSelektiertenGraphen.length];
        for (int i1=0;  i1<namenDerSignaleDesSelektiertenGraphen.length;  i1++)
            for (int i2=0;  i2<listeNamen.length;  i2++)
                if (namenDerSignaleDesSelektiertenGraphen[i1].equals(listeNamen[i2]))
                    posNeuGRF[i1]= i2;
        // abschliessend wird der umgeordnete Abschnitt wieder in das pos-Array eingefuegt:
        int z=0;
        for (int i1=0;  i1<posOrig.length;  i1++) {
            if (indexDerKurveInDerMatrix[i1]/1000==grfIndex) {
                posOrig[i1]= posNeuGRF[posName[z]];
                z++;
            }
        }
        // Aktualisierung der Daten in 'grafer' und Neuzeichnen der Umgeordneten Kurven:
        grafer.setPositionSIGNAL(posOrig);
        grafer.repaint();
        //-----------------------------
    }



    //-----------------------------
    public void windowClosing (WindowEvent we) { this.dispose(); }
    public void windowDeactivated (WindowEvent we) {}
    public void windowActivated (WindowEvent we) {}
    public void windowDeiconified (WindowEvent we) {}
    public void windowIconified (WindowEvent we) {}
    public void windowClosed (WindowEvent we) {}
    public void windowOpened (WindowEvent we) {}
    //-----------------------------

}




