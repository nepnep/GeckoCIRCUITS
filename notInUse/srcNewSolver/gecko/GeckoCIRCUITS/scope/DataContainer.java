/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.scope;

/**
 *
 * @author andy
 */
public interface DataContainer {

    public double getValue(int row, int column);
    public HiLoData getHiLoValue(int row, int column, int columnOld);


    public abstract void setValue(double value, int row, int column);

    public abstract  int getRowLength();

    public abstract  int getColumnLength();

    public abstract  void setColumn(double[] data, int index);

    public abstract  double[] getColumn(int index);


    /**
     * returns the time value (in seconds) for a given index
     * @param i2
     * @return 
     */
    public abstract  double getTimeValue(int i2);

    public  abstract int getMaximumTimeIndex();
}
