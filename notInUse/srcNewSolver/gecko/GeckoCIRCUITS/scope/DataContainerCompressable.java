/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

import java.util.ArrayList;

/**
 *
 * @author andy
 */
public class DataContainerCompressable extends DataContainerJunkable implements DataContainer, TimeSeriable {

    private int columnOld = -1;
    

    public DataContainerCompressable(int rows) {
        super(rows);
    }

    
    public double getValue(int row, int column) {
        try {
            if (row == 0) {
                return _timeSerie.getValue(column);
            }

            checkAndCorrectContainerSize(column);
            DataJunk junk = _data.get(column / JUNK_SIZE);
            return junk.getValue(row - 1, column);
        } catch (Exception ex) {
//            ex.printStackTrace();
        }
        return 0;
    }
    

    public void setValue(double value, int row, int column) {
        checkAndCorrectContainerSize(column);
        assert column >= columnOld : column + " " + columnOld;
        columnOld = column;
        try {
            DataJunk junk = _data.get(column / JUNK_SIZE);

            //assert column / JUNK_SIZE == _data.size() - 1 : column + " " + column / JUNK_SIZE + " " + _data.size();
            if (row > 0) {
                junk.setValue(value, row - 1, column);
            } else {
                _timeSerie.setValue(column, value);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    protected void checkAndCorrectContainerSize(int column) {
        if (column >= _totalDataSize) {
            if (_data.size() > 0) {
                if (_data.get(0) instanceof DataJunkCompressable) {
                    ((DataJunkCompressable) (_data.get(_data.size() - 1))).doCompression();
                }
            }
            _data.add(new DataJunkCompressable(_totalDataSize, _rows - 1, JUNK_SIZE));
            _totalDataSize = _data.size() * JUNK_SIZE;

        }

    }

}
