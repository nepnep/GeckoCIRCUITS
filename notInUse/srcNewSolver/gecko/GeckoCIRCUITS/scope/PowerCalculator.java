/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.scope;

/**
 *
 * @author andy
 */
public class PowerCalculator {

    final double[] power_P;
    final double[] power_Q;
    final double[] power_D;
    final double[] power_S;
    final double[] lambda;
    final double[] cosPhi;
    // just needed internally as helper, therefore private
    private final double[] rms2U;
    private final double[] rms2I;
    public final int _numberOfPowerAnalyses;

    private PowerCalculator(DataContainer worksheet,PowerAnalysisPanel.PowerCalculatorSelectionIndex selectedIndices) {
        
        _numberOfPowerAnalyses = selectedIndices.selectedCurrentIndices.size();
        power_P = new double[_numberOfPowerAnalyses];
        power_Q = new double[_numberOfPowerAnalyses];
        power_D = new double[_numberOfPowerAnalyses];
        power_S = new double[_numberOfPowerAnalyses];
        lambda = new double[_numberOfPowerAnalyses];
        cosPhi = new double[_numberOfPowerAnalyses];
        rms2U = new double[_numberOfPowerAnalyses];
        rms2I = new double[_numberOfPowerAnalyses];

        
        int[] voltageIndices = new int[_numberOfPowerAnalyses];
        int[] currentIndices = new int[_numberOfPowerAnalyses];
        
        for(int i = 0; i < _numberOfPowerAnalyses; i++) {
            voltageIndices[i] = selectedIndices.selectedVoltageIndices.get(i);
            currentIndices[i] = selectedIndices.selectedCurrentIndices.get(i);
        }
        
        calculate(worksheet, voltageIndices, currentIndices, selectedIndices.startTime, selectedIndices.stopTime);
    }

    /**
     * TODO: replace strange fourier series with FFT, to speed this up
     */
    private void calculate(DataContainer worksheet, int[] voltageIndices, int[] currentIndices, double rng1, double rng2) {

        // Startpunkt finden:
        int startIndex = 0;
        while (worksheet.getValue(0, startIndex) <= rng1) {
            startIndex++;
        }
        
        
        double dt = worksheet.getValue(0, startIndex + 1) - worksheet.getValue(0, startIndex);
        final double dT = rng2 - rng1;
        //-------------------

        int nmax = 50;
        
        for (int row = 0; row < _numberOfPowerAnalyses; row++) {
            double[] anU = new double[nmax], anI = new double[nmax];
            double[] bnU = new double[nmax], bnI = new double[nmax];
            double[] cnU = new double[nmax], cnI = new double[nmax], dphiUI = new double[nmax];
            
            // Rechnen bis zum Endpunkt:
            for (int i1 = startIndex; (i1 < worksheet.getColumnLength()) && (worksheet.getValue(0, i1 + 1) > worksheet.getValue(0, i1))
                    && (worksheet.getValue(0, i1) <= rng2); i1++) {
                dt = worksheet.getValue(0, i1 + 1) - worksheet.getValue(0, i1);
                double t = worksheet.getValue(0, i1);
                double u = worksheet.getValue(voltageIndices[row], i1);
                double i = worksheet.getValue(currentIndices[row], i1);
                for (int n = 0; n < nmax; n++) {
                    double arg = 2 * Math.PI / dT * t * n;
                    double cos_dt = Math.cos(arg) * dt;
                    double sin_dt = Math.sin(arg) * dt;
                    anU[n] += u * cos_dt;
                    bnU[n] += u * sin_dt;
                    anI[n] += i * cos_dt;
                    bnI[n] += i * sin_dt;
                }
                
                power_P[row] += (u * i * dt);
                rms2U[row] += (u * u * dt);
                rms2I[row] += (i * i * dt);
            }
            //--------
            for (int n = 0; n < nmax; n++) {
                anU[n] /= (0.5 * dT);
                bnU[n] /= (0.5 * dT);
                anI[n] /= (0.5 * dT);
                bnI[n] /= (0.5 * dT);
                cnU[n] = Math.sqrt(anU[n] * anU[n] + bnU[n] * bnU[n]);
                cnI[n] = Math.sqrt(anI[n] * anI[n] + bnI[n] * bnI[n]);
                dphiUI[n] = Math.atan2(bnU[n], anU[n]) - Math.atan2(bnI[n], anI[n]);
                power_Q[row] += (0.5 * cnU[n] * cnI[n] * Math.sin(dphiUI[n]));
            }
            power_P[row] /= dT;
            rms2U[row] /= dT;
            rms2I[row] /= dT;
            power_S[row] = Math.sqrt(rms2U[row] * rms2I[row]);
            power_D[row] = Math.sqrt(power_S[row] * power_S[row] - (power_P[row] * power_P[row] + power_Q[row] * power_Q[row]));
            lambda[row] = power_P[row] / power_S[row];
            cosPhi[row] = Math.cos(dphiUI[1]);
        }
    }

    static PowerCalculator CalculatorFabric(DataContainer worksheet, PowerAnalysisPanel.PowerCalculatorSelectionIndex selectedIndices) {
        PowerCalculator result = new PowerCalculator(worksheet, selectedIndices);
        return result;
    }
}
