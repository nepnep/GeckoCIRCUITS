/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.allg;

import java.awt.Font;
import java.net.URL;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.StringTokenizer;




 public class TxtI {


    //=========================
    public static final Font FONT_GERMAN=   new Font("Arial", Font.PLAIN, 12);
    public static final Font FONT_JAPANESE= new Font("Arial Unicode MS", Font.PLAIN, 14);
    //
    public static final Font FONT_MENU_DEFAULT= new Font("Arial", Font.BOLD, 12);
    public static final Font FONT_TEXT_DEFAULT= new Font("Arial", Font.PLAIN, 12);
    //-------------------------
    public static String[] LANGUAGES;
    public static int NR_LANGUAGES=1;  // default: zumindest Englisch vorhanden
    //
    private String[] langCode;
    //=========================
    public static final int MARGIN_TOP= 6;
    public static final int MARGIN_LEFT= 10;
    public static final int MARGIN_BOTTOM= 6;
    public static final int MARGIN_RIGHT= 10;
    public static final int TEXT_WIDTH= 270;
    public static final int TEXT_HEIGHT= 300;
    //=========================





    public TxtI () {
        TxtI.LANGUAGES= new String[99];  // 99 Sprachen, sollte reichen
        langCode= new String[TxtI.LANGUAGES.length];
        this.parseFile2("GeckoCIRCUITS_lang.txt");
    }




    public boolean parseFile2 (String datnamLangTxt) {
        try {
            //---------
            InputStream is= TxtI.class.getResourceAsStream(datnamLangTxt);  // funktioniert, weil die Text-Datei und TxTI-Klasse beide im gleichen Package sitzen
            BufferedReader in= new BufferedReader(new InputStreamReader(is));
            String line= "";
            int j=0;  // Zahl der Zeilen
            while ((line=in.readLine()) != null) j++;
            String[] txt= new String[j];
            j= 0;  // reset
            is= TxtI.class.getResourceAsStream(datnamLangTxt);  // funktioniert, weil die Text-Datei und TxTI-Klasse beide im gleichen Package sitzen
            in= new BufferedReader(new InputStreamReader(is));
            while ((line=in.readLine()) != null) { txt[j]=line.trim();  j++; }
            int langCounter= 0;
            //================================
            String langCodeAktuell= "en";
            boolean unicode= false;
            //
            if (Typ.LANGUAGE.equals("English"))       { langCodeAktuell= "en";  unicode=false;  this.setFontForLanguage(langCodeAktuell); }
            else if (Typ.LANGUAGE.equals("German"))   { langCodeAktuell= "at";  unicode=false;  this.setFontForLanguage(langCodeAktuell); }
            else if (Typ.LANGUAGE.equals("Japanese")) { langCodeAktuell= "jp";  unicode=true;   this.setFontForLanguage(langCodeAktuell); }
            //================================
            for (int i1=0;  i1<txt.length;  i1++) {
                if (txt[i1].startsWith("<lang>")) {
                    i1++;
                    while (! txt[i1].startsWith("</lang>")) {
                        langCode[langCounter]= txt[i1].substring(1,3);
                        TxtI.LANGUAGES[langCounter]= txt[i1].substring(4, txt[i1].lastIndexOf("<"));
                        i1++;
                        langCounter++;
                    }
                    TxtI.NR_LANGUAGES= langCounter;
                }
                if (txt[i1].startsWith("<Class val=")) {
                    String val= txt[i1].substring(txt[i1].indexOf("\"")+1, txt[i1].lastIndexOf("\""));
                    int index= i1;
                    if (val.equals("Fenster")) index= this.parse_Fenster(txt,index,langCodeAktuell,unicode);
                    else if (val.equals("Elemente"))  index= this.parse_Elemente(txt,index,langCodeAktuell,unicode);
                    else if (val.equals("DialogeAllg"))  index= this.parse_DialogeAllg(txt,index,langCodeAktuell,unicode);
                    else if (val.equals("SchematischeEingabeAuswahl2"))  index= this.parse_SchematischeEingabeAuswahl2(txt,index,langCodeAktuell,unicode);
                    else if (val.equals("ScopeX"))  index= this.parse_ScopeX(txt,index,langCodeAktuell,unicode);
                    else if (val.equals("TitledBorderX"))  index= this.parse_TitledBorderX(txt,index,langCodeAktuell,unicode);
                    i1= index;
                }
            }
            //for (int i2=0;  i2<langCounter;  i2++) System.out.println(langCode[i2]+"   "+languages[i2]);
            //System.out.println("ti_S1txt_DialogElementLK= "+ti_S1txt_DialogElementLK);
            //---------
            return true;  // Datei OK;
        } catch (Exception e) {
            if (Typ.LANGUAGE.equals("English")) System.out.println("Language: English .... ");
            else System.out.println(e+"  Language-Error ... odifnfff");
        }
        return false;  // Fehler in der Datei
    }




    private int parse_TitledBorderX (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_TitledBorderX() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith(";"))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        //System.out.println(v3+"\t\t\t"+v2);
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_x_DialogCheckSimulation")) TxtI.ti_x_DialogCheckSimulation= v3;
                    if (v2.equals("ti_x_DialogLanguageSettings")) TxtI.ti_x_DialogLanguageSettings= v3;
                    if (v2.equals("ti_x1_DialogSimParameter")) TxtI.ti_x1_DialogSimParameter= v3;
                    if (v2.equals("ti_x2_DialogSimParameter")) TxtI.ti_x2_DialogSimParameter= v3;
                    if (v2.equals("ti_x1_DialogElementLK")) TxtI.ti_x1_DialogElementLK= v3;
                    if (v2.equals("ti_x2_DialogElementLK")) TxtI.ti_x2_DialogElementLK= v3;
                    if (v2.equals("ti_x3_DialogElementLK")) TxtI.ti_x3_DialogElementLK= v3;
                    if (v2.equals("ti_x4_DialogElementLK")) TxtI.ti_x4_DialogElementLK= v3;
                    if (v2.equals("ti_x5_DialogElementLK")) TxtI.ti_x5_DialogElementLK= v3;
                    if (v2.equals("ti_x6_DialogElementLK")) TxtI.ti_x6_DialogElementLK= v3;
                    if (v2.equals("ti_x7_DialogElementLK")) TxtI.ti_x7_DialogElementLK= v3;
                    if (v2.equals("ti_x1_DialogVerlusteDetail")) TxtI.ti_x1_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_x2_DialogVerlusteDetail")) TxtI.ti_x2_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_x3_DialogVerlusteDetail")) TxtI.ti_x3_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_x4_DialogVerlusteDetail")) TxtI.ti_x4_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_x5_DialogVerlusteDetail")) TxtI.ti_x5_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_x1_DialogElementCONTROL")) TxtI.ti_x1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x2_DialogElementCONTROL")) TxtI.ti_x2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x3_DialogElementCONTROL")) TxtI.ti_x3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x4_DialogElementCONTROL")) TxtI.ti_x4_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x5_DialogElementCONTROL")) TxtI.ti_x5_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x6_DialogElementCONTROL")) TxtI.ti_x6_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x7_DialogElementCONTROL")) TxtI.ti_x7_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x8_DialogElementCONTROL")) TxtI.ti_x8_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x9_DialogElementCONTROL")) TxtI.ti_x9_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x11_DialogElementCONTROL")) TxtI.ti_x11_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x12_DialogElementCONTROL")) TxtI.ti_x12_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x13_DialogElementCONTROL")) TxtI.ti_x13_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x14_DialogElementCONTROL")) TxtI.ti_x14_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x1_RthCthESBueberblick")) TxtI.ti_x1_RthCthESBueberblick= v3;
                    if (v2.equals("ti_x2_RthCthESBueberblick")) TxtI.ti_x2_RthCthESBueberblick= v3;
                    if (v2.equals("ti_x3_RthCthESBueberblick")) TxtI.ti_x3_RthCthESBueberblick= v3;
                    if (v2.equals("ti_x1_DialogAvgRms")) TxtI.ti_x1_DialogAvgRms= v3;
                    if (v2.equals("ti_x2_DialogAvgRms")) TxtI.ti_x2_DialogAvgRms= v3;
                    if (v2.equals("ti_x3_DialogAvgRms")) TxtI.ti_x3_DialogAvgRms= v3;
                    if (v2.equals("ti_x1_DialogConnectSignalsGraphs")) TxtI.ti_x1_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_x2_DialogConnectSignalsGraphs")) TxtI.ti_x2_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_x3_DialogConnectSignalsGraphs")) TxtI.ti_x3_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_x1_DialogCurveProperties")) TxtI.ti_x1_DialogCurveProperties= v3;
                    if (v2.equals("ti_x2_DialogCurveProperties")) TxtI.ti_x2_DialogCurveProperties= v3;
                    if (v2.equals("ti_x3_DialogCurveProperties")) TxtI.ti_x3_DialogCurveProperties= v3;
                    if (v2.equals("ti_x4_DialogCurveProperties")) TxtI.ti_x4_DialogCurveProperties= v3;
                    if (v2.equals("ti_x_DialogDigitalCurveProperties")) TxtI.ti_x_DialogDigitalCurveProperties= v3;
                    if (v2.equals("ti_x_DialogEditText")) TxtI.ti_x_DialogEditText= v3;
                    if (v2.equals("ti_x1_DialogFourier")) TxtI.ti_x1_DialogFourier= v3;
                    if (v2.equals("ti_x2_DialogFourier")) TxtI.ti_x2_DialogFourier= v3;
                    if (v2.equals("ti_x_DialogOrdnungSIGNAL")) TxtI.ti_x_DialogOrdnungSIGNAL= v3;
                    if (v2.equals("ti_x1_TestReceiverCISPR16")) TxtI.ti_x1_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_x2_TestReceiverCISPR16")) TxtI.ti_x2_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_x1_DialogElementTHERM")) TxtI.ti_x1_DialogElementTHERM= v3;
                    if (v2.equals("ti_x2_DialogElementTHERM")) TxtI.ti_x2_DialogElementTHERM= v3;
                    if (v2.equals("ti_x3_DialogElementTHERM")) TxtI.ti_x3_DialogElementTHERM= v3;
                    if (v2.equals("ti_x4_DialogElementTHERM")) TxtI.ti_x4_DialogElementTHERM= v3;
                    if (v2.equals("ti_x5_DialogElementTHERM")) TxtI.ti_x5_DialogElementTHERM= v3;
                    if (v2.equals("ti_xx1_DialogElementCONTROL")) TxtI.ti_xx1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_x2_DialogSimulinkZeitschrittProblem")) TxtI.ti_x2_DialogSimulinkZeitschrittProblem= v3;
                    if (v2.equals("ti_txt01_DialogSimulinkZeitschrittProblem")) TxtI.ti_txt01_DialogSimulinkZeitschrittProblem= v3;
                    if (v2.equals("ti_x2_DialogInfoFestplattenSpeicherungAktivieren")) TxtI.ti_x2_DialogInfoFestplattenSpeicherungAktivieren= v3;
                    if (v2.equals("ti_txt01_DialogInfoFestplattenSpeicherungAktivieren")) TxtI.ti_txt01_DialogInfoFestplattenSpeicherungAktivieren= v3;
                }
            }
            //---------
            index++;
        }
        return index;
    }

    private int parse_Elemente (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_DialogeAllg() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith(";"))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        //System.out.println(v3+"\t\t\t"+v2);
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_U1_LKreisX")) TxtI.ti_U1_LKreisX= v3;
                    if (v2.equals("ti_MOTOR_LKreisX")) TxtI.ti_MOTOR_LKreisX= v3;
                    if (v2.equals("ti_M1_LKreisX")) TxtI.ti_M1_LKreisX= v3;
                    if (v2.equals("ti_D1_LKreisX")) TxtI.ti_D1_LKreisX= v3;
                    if (v2.equals("ti_S1_LKreisX")) TxtI.ti_S1_LKreisX= v3;
                    if (v2.equals("ti_S2_LKreisX")) TxtI.ti_S2_LKreisX= v3;
                    if (v2.equals("ti_Name_DialogElementLK")) TxtI.ti_Name_DialogElementLK= v3;
                    if (v2.equals("ti_UI1_DialogElementLK")) TxtI.ti_UI1_DialogElementLK= v3;
                    if (v2.equals("ti_UI2_DialogElementLK")) TxtI.ti_UI2_DialogElementLK= v3;
                    if (v2.equals("ti_UI3_DialogElementLK")) TxtI.ti_UI3_DialogElementLK= v3;
                    if (v2.equals("ti_UI4_DialogElementLK")) TxtI.ti_UI4_DialogElementLK= v3;
                    if (v2.equals("ti_UI5_DialogElementLK")) TxtI.ti_UI5_DialogElementLK= v3;
                    if (v2.equals("ti_UI6_DialogElementLK")) TxtI.ti_UI6_DialogElementLK= v3;
                    if (v2.equals("ti_UIdc1_DialogElementLK")) TxtI.ti_UIdc1_DialogElementLK= v3;
                    if (v2.equals("ti_UIdc2_DialogElementLK")) TxtI.ti_UIdc2_DialogElementLK= v3;
                    if (v2.equals("ti_UIdc3_DialogElementLK")) TxtI.ti_UIdc3_DialogElementLK= v3;
                    if (v2.equals("ti_UIs1_DialogElementLK")) TxtI.ti_UIs1_DialogElementLK= v3;
                    if (v2.equals("ti_UIs2_DialogElementLK")) TxtI.ti_UIs2_DialogElementLK= v3;
                    if (v2.equals("ti_UIs3_DialogElementLK")) TxtI.ti_UIs3_DialogElementLK= v3;
                    if (v2.equals("ti_Ds1tab_DialogElementLK")) TxtI.ti_Ds1tab_DialogElementLK= v3;
                    if (v2.equals("ti_Ds2tab_DialogElementLK")) TxtI.ti_Ds2tab_DialogElementLK= v3;
                    if (v2.equals("ti_Ds3tab_DialogElementLK")) TxtI.ti_Ds3tab_DialogElementLK= v3;
                    if (v2.equals("ti_Ds4tab_DialogElementLK")) TxtI.ti_Ds4tab_DialogElementLK= v3;
                    if (v2.equals("ti_M1_DialogElementLK")) TxtI.ti_M1_DialogElementLK= v3;
                    if (v2.equals("ti_M2_DialogElementLK")) TxtI.ti_M2_DialogElementLK= v3;
                    if (v2.equals("ti_M3_DialogElementLK")) TxtI.ti_M3_DialogElementLK= v3;
                    if (v2.equals("ti_M4_DialogElementLK")) TxtI.ti_M4_DialogElementLK= v3;
                    if (v2.equals("ti_lisn1_DialogElementLK")) TxtI.ti_lisn1_DialogElementLK= v3;
                    if (v2.equals("ti_Loss1_DialogElementLK")) TxtI.ti_Loss1_DialogElementLK= v3;
                    if (v2.equals("ti_Loss2_DialogElementLK")) TxtI.ti_Loss2_DialogElementLK= v3;
                    if (v2.equals("ti_Loss3_DialogElementLK")) TxtI.ti_Loss3_DialogElementLK= v3;
                    if (v2.equals("ti_Loss4_DialogElementLK")) TxtI.ti_Loss4_DialogElementLK= v3;
                    if (v2.equals("ti_Loss5_DialogElementLK")) TxtI.ti_Loss5_DialogElementLK= v3;
                    if (v2.equals("ti_conLoss_DialogVerlusteDetail")) TxtI.ti_conLoss_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_swLoss_DialogVerlusteDetail")) TxtI.ti_swLoss_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_lossSC_DialogVerlusteDetail")) TxtI.ti_lossSC_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_lossSN_DialogVerlusteDetail")) TxtI.ti_lossSN_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_loss1_DialogVerlusteDetail")) TxtI.ti_loss1_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_loss2_DialogVerlusteDetail")) TxtI.ti_loss2_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_lossAdd_DialogVerlusteDetail")) TxtI.ti_lossAdd_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_lossDel_DialogVerlusteDetail")) TxtI.ti_lossDel_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_lossCon_DialogVerlusteDetail")) TxtI.ti_lossCon_DialogVerlusteDetail= v3;
                    if (v2.equals("ti_ctrVolt_DialogElementCONTROL")) TxtI.ti_ctrVolt_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrAmp_DialogElementCONTROL")) TxtI.ti_ctrAmp_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGv1_DialogElementCONTROL")) TxtI.ti_ctrSIGv1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGv2_DialogElementCONTROL")) TxtI.ti_ctrSIGv2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGv3_DialogElementCONTROL")) TxtI.ti_ctrSIGv3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGd3_DialogElementCONTROL")) TxtI.ti_ctrSIGd3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGd4_DialogElementCONTROL")) TxtI.ti_ctrSIGd4_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGe1_DialogElementCONTROL")) TxtI.ti_ctrSIGe1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGe2_DialogElementCONTROL")) TxtI.ti_ctrSIGe2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGa_DialogElementCONTROL")) TxtI.ti_ctrSIGa_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGb_DialogElementCONTROL")) TxtI.ti_ctrSIGb_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGc_DialogElementCONTROL")) TxtI.ti_ctrSIGc_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGd_DialogElementCONTROL")) TxtI.ti_ctrSIGd_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSIGe_DialogElementCONTROL")) TxtI.ti_ctrSIGe_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSW_DialogElementCONTROL")) TxtI.ti_ctrSW_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrTEMP_DialogElementCONTROL")) TxtI.ti_ctrTEMP_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrFLOW_DialogElementCONTROL")) TxtI.ti_ctrFLOW_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrEXT_DialogElementCONTROL")) TxtI.ti_ctrEXT_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrDIGS1_DialogElementCONTROL")) TxtI.ti_ctrDIGS1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrLGC1_DialogElementCONTROL")) TxtI.ti_ctrLGC1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrLGC2_DialogElementCONTROL")) TxtI.ti_ctrLGC2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrLGC3_DialogElementCONTROL")) TxtI.ti_ctrLGC3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrCNT_DialogElementCONTROL")) TxtI.ti_ctrCNT_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSTA1_DialogElementCONTROL")) TxtI.ti_ctrSTA1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSTA2_DialogElementCONTROL")) TxtI.ti_ctrSTA2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSTA3_DialogElementCONTROL")) TxtI.ti_ctrSTA3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSTA4_DialogElementCONTROL")) TxtI.ti_ctrSTA4_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrNotDef_DialogElementCONTROL")) TxtI.ti_ctrNotDef_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW1_DialogElementCONTROL")) TxtI.ti_ctrSgW1_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW2_DialogElementCONTROL")) TxtI.ti_ctrSgW2_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW3_DialogElementCONTROL")) TxtI.ti_ctrSgW3_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW4_DialogElementCONTROL")) TxtI.ti_ctrSgW4_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW5_DialogElementCONTROL")) TxtI.ti_ctrSgW5_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrSgW6_DialogElementCONTROL")) TxtI.ti_ctrSgW6_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrJAVAtab1_ReglerJavaFunction")) TxtI.ti_ctrJAVAtab1_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAtab2_ReglerJavaFunction")) TxtI.ti_ctrJAVAtab2_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAtab3_ReglerJavaFunction")) TxtI.ti_ctrJAVAtab3_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAbsp1_ReglerJavaFunction")) TxtI.ti_ctrJAVAbsp1_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAbsp2_ReglerJavaFunction")) TxtI.ti_ctrJAVAbsp2_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAjb1_ReglerJavaFunction")) TxtI.ti_ctrJAVAjb1_ReglerJavaFunction= v3;
                    if (v2.equals("ti_ctrJAVAjb2_ReglerJavaFunction")) TxtI.ti_ctrJAVAjb2_ReglerJavaFunction= v3;
                    if (v2.equals("ti_sm_DialogElementTHERM")) TxtI.ti_sm_DialogElementTHERM= v3;
                    if (v2.equals("ti_temp_DialogElementTHERM")) TxtI.ti_temp_DialogElementTHERM= v3;
                    if (v2.equals("ti_heat_DialogElementTHERM")) TxtI.ti_heat_DialogElementTHERM= v3;
                    if (v2.equals("ti_mod1_DialogElementTHERM")) TxtI.ti_mod1_DialogElementTHERM= v3;
                    if (v2.equals("ti_mod2_DialogElementTHERM")) TxtI.ti_mod2_DialogElementTHERM= v3;
                    if (v2.equals("ti_mod3_DialogElementTHERM")) TxtI.ti_mod3_DialogElementTHERM= v3;
                    if (v2.equals("ti_mod4_DialogElementTHERM")) TxtI.ti_mod4_DialogElementTHERM= v3;
                    if (v2.equals("ti_c_THERMKreisX")) TxtI.ti_c_THERMKreisX= v3;
                    if (v2.equals("ti_loss_THERMKreisX")) TxtI.ti_loss_THERMKreisX= v3;
                    if (v2.equals("ti_rc_DialogViewPowerModule")) TxtI.ti_rc_DialogViewPowerModule= v3;
                    if (v2.equals("ti_3d_DialogViewPowerModule")) TxtI.ti_3d_DialogViewPowerModule= v3;
                    if (v2.equals("ti_noMod_DialogViewPowerModule")) TxtI.ti_noMod_DialogViewPowerModule= v3;
                    if (v2.equals("ti_ferr_DialogViewPowerModule")) TxtI.ti_ferr_DialogViewPowerModule= v3;
                    if (v2.equals("ti_ctrMot_DialogElementCONTROL")) TxtI.ti_ctrMot_DialogElementCONTROL= v3;
                    if (v2.equals("ti_SVDis_1")) TxtI.ti_SVDis_1= v3;
                    if (v2.equals("ti_SVDis_2")) TxtI.ti_SVDis_2= v3;
                    if (v2.equals("ti_SVDis_3")) TxtI.ti_SVDis_3= v3;
                    if (v2.equals("ti_SVDis_b1")) TxtI.ti_SVDis_b1= v3;
                    if (v2.equals("ti_SVDis_b2")) TxtI.ti_SVDis_b2= v3;
                    if (v2.equals("ti_SVDis_clear")) TxtI.ti_SVDis_clear= v3;
                }
            }
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith("="))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        index++;
                        while (! txt[index].startsWith("</"+langCodeAktuell+">")) {
                            v3 += txt[index];
                            index++;
                        }
                        //System.out.println(v3);
                        if (unicode) v3= this.convertUnicode(v3);
                        v3= this.correctLineBreak(v3);
                        //System.out.println(v3);
                        break;
                    }
                    index++;
                }
                index--;
                //System.out.println("---------");
                if (!v3.equals("")) {
                    if (v2.equals("ti_Ds1txt_DialogElementLK")) TxtI.ti_Ds1txt_DialogElementLK= v3;
                    if (v2.equals("ti_Ds2txt_DialogElementLK")) TxtI.ti_Ds2txt_DialogElementLK= v3;
                    if (v2.equals("ti_S1txt_DialogElementLK")) TxtI.ti_S1txt_DialogElementLK= v3;
                    if (v2.equals("ti_Losstxt_DialogElementLK")) TxtI.ti_Losstxt_DialogElementLK= v3;
                    if (v2.equals("ti_ctrSIGtxt_DialogElementCONTROL")) TxtI.ti_ctrSIGtxt_DialogElementCONTROL= v3;
                    if (v2.equals("ti_ctrJAVAinfo_ReglerJavaFunction")) TxtI.ti_ctrJAVAinfo_ReglerJavaFunction= v3;
                }
            }
            //---------
            index++;
        }
        return index;
    }

    private int parse_ScopeX (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_ScopeX() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith(";"))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        //System.out.println(v3+"\t\t\t"+v2);
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_grfTab_ScopeX")) TxtI.ti_grfTab_ScopeX= v3;
                    if (v2.equals("ti_datTab_ScopeX")) TxtI.ti_datTab_ScopeX= v3;
                    if (v2.equals("ti_tt1_ScopeX")) TxtI.ti_tt1_ScopeX= v3;
                    if (v2.equals("ti_tt2_ScopeX")) TxtI.ti_tt2_ScopeX= v3;
                    if (v2.equals("ti_tt3_ScopeX")) TxtI.ti_tt3_ScopeX= v3;
                    if (v2.equals("ti_tt4_ScopeX")) TxtI.ti_tt4_ScopeX= v3;
                    if (v2.equals("ti_tt5_ScopeX")) TxtI.ti_tt5_ScopeX= v3;
                    if (v2.equals("ti_tt6_ScopeX")) TxtI.ti_tt6_ScopeX= v3;
                    if (v2.equals("ti_jtb_ScopeX")) TxtI.ti_jtb_ScopeX= v3;
                    if (v2.equals("ti_jcbSav_ScopeX")) TxtI.ti_jcbSav_ScopeX= v3;
                    if (v2.equals("ti_jmiWri_ScopeX")) TxtI.ti_jmiWri_ScopeX= v3;
                    if (v2.equals("ti_jmGrf_ScopeX")) TxtI.ti_jmGrf_ScopeX= v3;
                    if (v2.equals("ti_jmiMtx_ScopeX")) TxtI.ti_jmiMtx_ScopeX= v3;
                    if (v2.equals("ti_jmAna_ScopeX")) TxtI.ti_jmAna_ScopeX= v3;
                    if (v2.equals("ti_jmiChar_ScopeX")) TxtI.ti_jmiChar_ScopeX= v3;
                    if (v2.equals("ti_jmiFour_ScopeX")) TxtI.ti_jmiFour_ScopeX= v3;
                    if (v2.equals("ti_scRa_DialogFourier")) TxtI.ti_scRa_DialogFourier= v3;
                    if (v2.equals("ti_dfRa_DialogFourier")) TxtI.ti_dfRa_DialogFourier= v3;
                    if (v2.equals("ti_slRa_DialogFourier")) TxtI.ti_slRa_DialogFourier= v3;
                    if (v2.equals("ti_calc_DialogFourier")) TxtI.ti_calc_DialogFourier= v3;
                    if (v2.equals("ti_e1_DialogFourier")) TxtI.ti_e1_DialogFourier= v3;
                    if (v2.equals("ti_e2_DialogFourier")) TxtI.ti_e2_DialogFourier= v3;
                    if (v2.equals("ti_e3_DialogFourier")) TxtI.ti_e3_DialogFourier= v3;
                    if (v2.equals("ti_e4_DialogFourier")) TxtI.ti_e4_DialogFourier= v3;
                    if (v2.equals("ti_calcFin_DialogFourier")) TxtI.ti_calcFin_DialogFourier= v3;
                    if (v2.equals("ti_datErr_DialogFourier")) TxtI.ti_datErr_DialogFourier= v3;
                    if (v2.equals("ti_clcx_DialogFourier")) TxtI.ti_clcx_DialogFourier= v3;
                    if (v2.equals("ti_def_DialogAvgRms")) TxtI.ti_def_DialogAvgRms= v3;
                    if (v2.equals("ti_deact_DialogAvgRms")) TxtI.ti_deact_DialogAvgRms= v3;
                    if (v2.equals("ti_pa_DialogAvgRms")) TxtI.ti_pa_DialogAvgRms= v3;
                    if (v2.equals("ti_calcrun_DialogAvgRms")) TxtI.ti_calcrun_DialogAvgRms= v3;
                    if (v2.equals("ti_calcok_DialogAvgRms")) TxtI.ti_calcok_DialogAvgRms= v3;
                    if (v2.equals("ti_fa_DialogFourierDiagramm")) TxtI.ti_fa_DialogFourierDiagramm= v3;
                    if (v2.equals("ti_wd_DialogFourierDiagramm")) TxtI.ti_wd_DialogFourierDiagramm= v3;
                    if (v2.equals("ti_rec_DialogFourierDiagramm")) TxtI.ti_rec_DialogFourierDiagramm= v3;
                    if (v2.equals("ti_cur_DialogCurveProperties")) TxtI.ti_cur_DialogCurveProperties= v3;
                    if (v2.equals("ti_ax_DialogCurveProperties")) TxtI.ti_ax_DialogCurveProperties= v3;
                    if (v2.equals("ti_sty_DialogCurveProperties")) TxtI.ti_sty_DialogCurveProperties= v3;
                    if (v2.equals("ti_col_DialogCurveProperties")) TxtI.ti_col_DialogCurveProperties= v3;
                    if (v2.equals("ti_shsy_DialogCurveProperties")) TxtI.ti_shsy_DialogCurveProperties= v3;
                    if (v2.equals("ti_frq_DialogCurveProperties")) TxtI.ti_frq_DialogCurveProperties= v3;
                    if (v2.equals("ti_shp_DialogCurveProperties")) TxtI.ti_shp_DialogCurveProperties= v3;
                    if (v2.equals("ti_fill_DialogDigitalCurveProperties")) TxtI.ti_fill_DialogDigitalCurveProperties= v3;
                    if (v2.equals("ti_txt_DialogOrdnungSIGNAL")) TxtI.ti_txt_DialogOrdnungSIGNAL= v3;
                    if (v2.equals("ti_ttip_DialogConnectSignalsGraphs")) TxtI.ti_ttip_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_stat_DialogConnectSignalsGraphs")) TxtI.ti_stat_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_weig_DialogConnectSignalsGraphs")) TxtI.ti_weig_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_add_DialogConnectSignalsGraphs")) TxtI.ti_add_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_rmv_DialogConnectSignalsGraphs")) TxtI.ti_rmv_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_se1_DialogConnectSignalsGraphs")) TxtI.ti_se1_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_se2_DialogConnectSignalsGraphs")) TxtI.ti_se2_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_se3_DialogConnectSignalsGraphs")) TxtI.ti_se3_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_se4_DialogConnectSignalsGraphs")) TxtI.ti_se4_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_err_DialogConnectSignalsGraphs")) TxtI.ti_err_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_inok_DialogConnectSignalsGraphs")) TxtI.ti_inok_DialogConnectSignalsGraphs= v3;
                    if (v2.equals("ti_if1_TestReceiverCISPR16")) TxtI.ti_if1_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_if2_TestReceiverCISPR16")) TxtI.ti_if2_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_if3_TestReceiverCISPR16")) TxtI.ti_if3_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q1_TestReceiverCISPR16")) TxtI.ti_q1_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q2_TestReceiverCISPR16")) TxtI.ti_q2_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q3_TestReceiverCISPR16")) TxtI.ti_q3_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q4_TestReceiverCISPR16")) TxtI.ti_q4_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q5_TestReceiverCISPR16")) TxtI.ti_q5_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q6_TestReceiverCISPR16")) TxtI.ti_q6_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q7_TestReceiverCISPR16")) TxtI.ti_q7_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_q8_TestReceiverCISPR16")) TxtI.ti_q8_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r1_TestReceiverCISPR16")) TxtI.ti_r1_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r2_TestReceiverCISPR16")) TxtI.ti_r2_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r4_TestReceiverCISPR16")) TxtI.ti_r4_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r5_TestReceiverCISPR16")) TxtI.ti_r5_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r6_TestReceiverCISPR16")) TxtI.ti_r6_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_r7_TestReceiverCISPR16")) TxtI.ti_r7_TestReceiverCISPR16= v3;
                    if (v2.equals("ti_warn1_TestReceiverCISPR16Calculator")) TxtI.ti_warn1_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_warn2_TestReceiverCISPR16Calculator")) TxtI.ti_warn2_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_warn3_TestReceiverCISPR16Calculator")) TxtI.ti_warn3_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_warn4_TestReceiverCISPR16Calculator")) TxtI.ti_warn4_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_info1_TestReceiverCISPR16Calculator")) TxtI.ti_info1_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_info2_TestReceiverCISPR16Calculator")) TxtI.ti_info2_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_info3_TestReceiverCISPR16Calculator")) TxtI.ti_info3_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_info4_TestReceiverCISPR16Calculator")) TxtI.ti_info4_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_err_TestReceiverCISPR16Calculator")) TxtI.ti_err_TestReceiverCISPR16Calculator= v3;
                    if (v2.equals("ti_mem_TestReceiverCISPR16Calculator")) TxtI.ti_mem_TestReceiverCISPR16Calculator= v3;
                }
            }
            //---------
            if (txt[index].startsWith("public static String[]")) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                String[] v4= null;  // mehr als 40 wird es schon wegen der Uebersicht nicht in einem Tab geben
                StringTokenizer stk= null;
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        index++;
                        while (! txt[index].startsWith("</"+langCodeAktuell+">")) {
                            v3 += txt[index];
                            index++;
                        }
                        stk= new StringTokenizer(v3, ",");
                        v4= new String[stk.countTokens()];
                        for (int i1=0;  i1<v4.length;  i1++) {
                            v4[i1]= stk.nextToken();
                            v4[i1]= v4[i1].substring(v4[i1].indexOf("\"")+1, v4[i1].lastIndexOf("\""));
                            //System.out.println(v4[i1]);
                            if (unicode) v4[i1]= this.convertUnicode(v4[i1]);
                        }
                        break;
                    }
                    index++;
                }
                //System.out.println("---------");
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_linStil_GraferV3")) TxtI.ti_linStil_GraferV3= v4;
                    if (v2.equals("ti_farbe_GraferV3")) TxtI.ti_farbe_GraferV3= v4;
                    if (v2.equals("ti_formSymb_GraferV3")) TxtI.ti_formSymb_GraferV3= v4;
                }
            }
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith("="))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        index++;
                        while (! txt[index].startsWith("</"+langCodeAktuell+">")) {
                            v3 += txt[index];
                            index++;
                        }
                        //System.out.println(v3);
                        if (unicode) v3= this.convertUnicode(v3);
                        v3= this.correctLineBreak(v3);
                        break;
                    }
                    index++;
                }
                index--;
                //System.out.println("---------");
                if (!v3.equals("")) {
                    if (v2.equals("ti_Ds1txt_DialogElementLK")) TxtI.ti_Ds1txt_DialogElementLK= v3;
                }
            }
            //---------
            index++;
        }
        return index;
    }

    private int parse_Fenster (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_Fenster() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if (txt[index].startsWith("public static String")) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        //System.out.println(v3);
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_File")) TxtI.ti_File= v3;
                    if (v2.equals("ti_New")) TxtI.ti_New= v3;
                    if (v2.equals("ti_Open")) TxtI.ti_Open= v3;
                    if (v2.equals("ti_Save")) TxtI.ti_Save= v3;
                    if (v2.equals("ti_SaveAs")) TxtI.ti_SaveAs= v3;
                    if (v2.equals("ti_Exit")) TxtI.ti_Exit= v3;
                    if (v2.equals("ti_Prop")) TxtI.ti_Prop= v3;
                    if (v2.equals("ti_Edit")) TxtI.ti_Edit= v3;
                    if (v2.equals("ti_Undo")) TxtI.ti_Undo= v3;
                    if (v2.equals("ti_Redo")) TxtI.ti_Redo= v3;
                    if (v2.equals("ti_CreateSubcircuit")) TxtI.ti_CreateSubcircuit= v3;
                    if (v2.equals("ti_CopyElements")) TxtI.ti_CopyElements= v3;
                    if (v2.equals("ti_MoveElements")) TxtI.ti_MoveElements= v3;
                    if (v2.equals("ti_DeleteElements")) TxtI.ti_DeleteElements= v3;
                    if (v2.equals("ti_Deselect")) TxtI.ti_Deselect= v3;
                    if (v2.equals("ti_Import")) TxtI.ti_Import= v3;
                    if (v2.equals("ti_Export")) TxtI.ti_Export= v3;
                    if (v2.equals("ti_Simulation")) TxtI.ti_Simulation= v3;
                    if (v2.equals("ti_Parameter")) TxtI.ti_Parameter= v3;
                    if (v2.equals("ti_InitStart")) TxtI.ti_InitStart= v3;
                    if (v2.equals("ti_Pause")) TxtI.ti_Pause= v3;
                    if (v2.equals("ti_Continue")) TxtI.ti_Continue= v3;
                    if (v2.equals("ti_Name")) TxtI.ti_Name= v3;
                    if (v2.equals("ti_ShowParameter")) TxtI.ti_ShowParameter= v3;
                    if (v2.equals("ti_ShowTextLine")) TxtI.ti_ShowTextLine= v3;
                    if (v2.equals("ti_FlowDirection")) TxtI.ti_FlowDirection= v3;
                    if (v2.equals("ti_WorksheetSize")) TxtI.ti_WorksheetSize= v3;
                    if (v2.equals("ti_Scaling")) TxtI.ti_Scaling= v3;
                    if (v2.equals("ti_FontSize")) TxtI.ti_FontSize= v3;
                    if (v2.equals("ti_View")) TxtI.ti_View= v3;
                    if (v2.equals("ti_Tools")) TxtI.ti_Tools= v3;
                    if (v2.equals("ti_conTest")) TxtI.ti_conTest= v3;
                    if (v2.equals("ti_conTest")) TxtI.ti_optimizerSimple= v3;
                    if (v2.equals("ti_checkModel")) TxtI.ti_checkModel= v3;
                    if (v2.equals("ti_Electromagnetic3DModel")) TxtI.ti_Electromagnetic3DModel= v3;
                    if (v2.equals("ti_FilterOptimization")) TxtI.ti_FilterOptimization= v3;
                    if (v2.equals("ti_TestRthCthExtractor")) TxtI.ti_TestRthCthExtractor= v3;
                    if (v2.equals("ti_Help")) TxtI.ti_Help= v3;
                    if (v2.equals("ti_About")) TxtI.ti_About= v3;
                    if (v2.equals("ti_ReadySim")) TxtI.ti_ReadySim= v3;
                    if (v2.equals("ti_StartingSim")) TxtI.ti_StartingSim= v3;
                    if (v2.equals("ti_StoppedSim")) TxtI.ti_StoppedSim= v3;
                    if (v2.equals("ti_RunningSim")) TxtI.ti_RunningSim= v3;
                    if (v2.equals("ti_OK")) TxtI.ti_OK= v3;
                    if (v2.equals("ti_Cancel")) TxtI.ti_Cancel= v3;
                }
            }
            //---------
            index++;
        }
        return index;
    }

    private int parse_SchematischeEingabeAuswahl2 (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_SchematischeEingabeAuswahl2() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith(";"))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_Circuit")) TxtI.ti_Circuit= v3;
                    if (v2.equals("ti_MotorEMI")) TxtI.ti_MotorEMI= v3;
                    if (v2.equals("ti_Thermal")) TxtI.ti_Thermal= v3;
                    if (v2.equals("ti_Control")) TxtI.ti_Control= v3;
                    if (v2.equals("ti_Measure")) TxtI.ti_Measure= v3;
                    if (v2.equals("ti_Digital")) TxtI.ti_Digital= v3;
                    if (v2.equals("ti_Math")) TxtI.ti_Math= v3;
                    if (v2.equals("ti_SourceSink")) TxtI.ti_SourceSink= v3;
                    if (v2.equals("ti_Special")) TxtI.ti_Special= v3;
                }
            }
            //---------
            if (txt[index].startsWith("public static String[]")) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                String[] v4= null;  // mehr als 40 wird es schon wegen der Uebersicht nicht in einem Tab geben
                StringTokenizer stk= null;
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        index++;
                        while (! txt[index].startsWith("</"+langCodeAktuell+">")) {
                            v3 += txt[index];
                            index++;
                        }
                        stk= new StringTokenizer(v3, ",");
                        v4= new String[stk.countTokens()];
                        for (int i1=0;  i1<v4.length;  i1++) {
                            v4[i1]= stk.nextToken();
                            v4[i1]= v4[i1].substring(v4[i1].indexOf("\"")+1, v4[i1].lastIndexOf("\""));
                            //System.out.println(v4[i1]);
                            if (unicode) v4[i1]= this.convertUnicode(v4[i1]);
                        }
                        break;
                    }
                    index++;
                }
                //System.out.println("---------");
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_elementNameLK")) TxtI.ti_elementNameLK= v4;
                    if (v2.equals("ti_elementNameLKmotor")) TxtI.ti_elementNameLKmotor= v4;
                    if (v2.equals("ti_elementNameCONTROL")) TxtI.ti_elementNameCONTROL= v4;
                    if (v2.equals("ti_elementNameCONTROL2")) TxtI.ti_elementNameCONTROL2= v4;
                    if (v2.equals("ti_elementNameCONTROL3")) TxtI.ti_elementNameCONTROL3= v4;
                    if (v2.equals("ti_elementNameCONTROL4")) TxtI.ti_elementNameCONTROL4= v4;
                    if (v2.equals("ti_elementNameCONTROL5")) TxtI.ti_elementNameCONTROL5= v4;
                    if (v2.equals("ti_elementNameCONTROL6")) TxtI.ti_elementNameCONTROL6= v4;
                    if (v2.equals("ti_elementNameTHERM")) TxtI.ti_elementNameTHERM= v4;
                }
            }
            //---------
            index++;
        }
        return index;
    }

    private int parse_DialogeAllg (String[] txt, int index, String langCodeAktuell, boolean unicode) {
        //System.out.println("parse_DialogeAllg() --> "+txt[index]);
        while (! txt[index].startsWith("</Class>")) {
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith(";"))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        v3= txt[index].substring(txt[index].indexOf(">")+1, txt[index].lastIndexOf("<")).trim();
                        if (unicode) v3= this.convertUnicode(v3);
                        break;
                    }
                    index++;
                }
                index--;
                if (!v3.equals("")) {
                    if (v2.equals("ti_SimulationParameters")) TxtI.ti_SimulationParameters= v3;
                    if (v2.equals("ti_tab1_DialogErrorJava3D")) TxtI.ti_tab1_DialogErrorJava3D= v3;
                    if (v2.equals("ti_tab2_DialogErrorJava3D")) TxtI.ti_tab2_DialogErrorJava3D= v3;
                    if (v2.equals("ti_tab3_DialogErrorJava3D")) TxtI.ti_tab3_DialogErrorJava3D= v3;
                    if (v2.equals("ti_tab4_DialogErrorJava3D")) TxtI.ti_tab4_DialogErrorJava3D= v3;
                    if (v2.equals("ti_lab1_DialogCheckSimulation")) TxtI.ti_lab1_DialogCheckSimulation= v3;
                    if (v2.equals("ti_lab2_DialogCheckSimulation")) TxtI.ti_lab2_DialogCheckSimulation= v3;
                    if (v2.equals("ti_lab3_DialogCheckSimulation")) TxtI.ti_lab3_DialogCheckSimulation= v3;
                    if (v2.equals("ti_ok_DialogCheckSimulation")) TxtI.ti_ok_DialogCheckSimulation= v3;
                    if (v2.equals("ti_apply_DialogCheckSimulation")) TxtI.ti_apply_DialogCheckSimulation= v3;
                    if (v2.equals("ti_redDisk_DialogCheckSimulation")) TxtI.ti_redDisk_DialogCheckSimulation= v3;
                    if (v2.equals("ti_ok_DialogErrorDtChange")) TxtI.ti_ok_DialogErrorDtChange= v3;
                    if (v2.equals("ti_weiter_DialogErrorDtChange")) TxtI.ti_weiter_DialogErrorDtChange= v3;
                    if (v2.equals("ti_jbYes_DialogQuitWithoutSaving")) TxtI.ti_jbYes_DialogQuitWithoutSaving= v3;
                    if (v2.equals("ti_jbNo_DialogQuitWithoutSaving")) TxtI.ti_jbNo_DialogQuitWithoutSaving= v3;
                    if (v2.equals("ti_x1_DialogAbout")) TxtI.ti_x1_DialogAbout= v3;
                    if (v2.equals("ti_x2_DialogAbout")) TxtI.ti_x2_DialogAbout= v3;
                    if (v2.equals("ti_x3_DialogAbout")) TxtI.ti_x3_DialogAbout= v3;
                    if (v2.equals("ti_x4_DialogAbout")) TxtI.ti_x4_DialogAbout= v3;
                    if (v2.equals("ti_x1_VerlustBerechnung")) TxtI.ti_x1_VerlustBerechnung= v3;
                    if (v2.equals("ti_x2_VerlustBerechnung")) TxtI.ti_x2_VerlustBerechnung= v3;
                }
            }
            //---------
            if ((txt[index].startsWith("public static String")) && (txt[index].endsWith("="))) {
                String v2= txt[index].substring(txt[index].indexOf("ti_"), txt[index].lastIndexOf("=")).trim();
                //System.out.println(v2);
                String v3= "";
                index++;
                while (! txt[index].startsWith("public static String")) {
                    if (txt[index].startsWith("<"+langCodeAktuell+">")) {
                        index++;
                        while (! txt[index].startsWith("</"+langCodeAktuell+">")) {
                            v3 += txt[index];
                            index++;
                        }
                        //System.out.println(v3);
                        if (unicode) v3= this.convertUnicode(v3);
                        v3= this.correctLineBreak(v3);
                        break;
                    }
                    index++;
                }
                index--;
                //System.out.println("---------");
                if (!v3.equals("")) {
                    if (v2.equals("ti_txt01_DialogSimParameter")) TxtI.ti_txt01_DialogSimParameter= v3;
                    if (v2.equals("ti_txt01_DialogDiodenError")) TxtI.ti_txt01_DialogDiodenError= v3;
                    if (v2.equals("ti_txt01_DialogErrorJava3D")) TxtI.ti_txt01_DialogErrorJava3D= v3;
                    if (v2.equals("ti_txt01_DialogErrorComplierTools")) TxtI.ti_txt01_DialogErrorComplierTools= v3;
                    if (v2.equals("ti_txt01_DialogJavaVersionError")) TxtI.ti_txt01_DialogJavaVersionError= v3;
                    if (v2.equals("ti_txt02_DialogJavaVersionError")) TxtI.ti_txt02_DialogJavaVersionError= v3;
                    if (v2.equals("ti_txt03_DialogJavaVersionError")) TxtI.ti_txt03_DialogJavaVersionError= v3;
                    if (v2.equals("ti_txt01_DialogLizenz")) TxtI.ti_txt01_DialogLizenz= v3;
                    if (v2.equals("ti_txt01_DialogCheckSimulation")) TxtI.ti_txt01_DialogCheckSimulation= v3;
                    if (v2.equals("ti_warn01_DialogCheckSimulation")) TxtI.ti_warn01_DialogCheckSimulation= v3;
                    if (v2.equals("ti_warn02a_DialogCheckSimulation")) TxtI.ti_warn02a_DialogCheckSimulation= v3;
                    if (v2.equals("ti_warn02b_DialogCheckSimulation")) TxtI.ti_warn02b_DialogCheckSimulation= v3;
                    if (v2.equals("ti_warn03a_DialogCheckSimulation")) TxtI.ti_warn03a_DialogCheckSimulation= v3;
                    if (v2.equals("ti_warn03b_DialogCheckSimulation")) TxtI.ti_warn03b_DialogCheckSimulation= v3;
                    if (v2.equals("ti_txt01_DialogMatrixSingular")) TxtI.ti_txt01_DialogMatrixSingular= v3;
                    if (v2.equals("ti_txt01_DialogErrorDtChange")) TxtI.ti_txt01_DialogErrorDtChange= v3;
                    if (v2.equals("ti_txt01_DialogWarningNodeNumber")) TxtI.ti_txt01_DialogWarningNodeNumber= v3;
                    if (v2.equals("ti_txt01_DialogQuitWithoutSaving")) TxtI.ti_txt01_DialogQuitWithoutSaving= v3;
                    if (v2.equals("ti_tx1_DialogLanguageSettings")) TxtI.ti_tx1_DialogLanguageSettings= v3;
                }
            }
            //---------
            index++;
        }
        return index;
    }




    private String convertUnicode (String txt) {
      //-----------
        StringBuffer txtU= new StringBuffer();
        String kanji, q1, q2;
        Character c= null;
        StringTokenizer stk= new StringTokenizer(txt, ";");
        //-----------
        while (stk.hasMoreTokens()) {
            kanji= stk.nextToken();
            //System.out.println("kanji= "+kanji);
            if (kanji.startsWith("&#")) {
                try {
                    kanji= kanji.substring(kanji.indexOf("&#")+2);
                    int id= (new Integer(kanji)).intValue();
                    c= new Character((char)id);
                    txtU.append(c.toString());
                } catch (Exception e) {System.out.println(e+"   0psergh94");}
            } else if (kanji.indexOf("&#")!=-1) {
                try {
                    int index= kanji.indexOf("&#");
                    q1= kanji.substring(0,index);
                    q2= kanji.substring(index+2);
                    //System.out.println("--> "+q1+"   "+q2);
                    txtU.append(q1);
                    int id= (new Integer(q2)).intValue();
                    c= new Character((char)id);
                    txtU.append(c.toString());
                } catch (Exception e) {System.out.println(e+"   seig340");}
            } else {
                txtU.append(kanji);
            }
        }
        //-----------
        return txtU.toString();
    }





    private String correctLineBreak (String v3) {
        StringTokenizer stk= new StringTokenizer(v3, "\\");
        String erg= stk.nextToken();
        if (erg.startsWith("n")) erg=erg.substring(1);  // falls Zeilenumbruch '\n' ganz am Anfang
        erg += "\n";
        while (stk.hasMoreTokens()) erg += (stk.nextToken().substring(1)+"\n");
        return erg;
    }




    private void setFontForLanguage (String langCodeAktuell) {
        if (langCodeAktuell.equals("en")) {
            TxtI.ti_Font_A= new Font("Arial", Font.BOLD, 12);  // Menu ... FONT_MENU_DEFAULT
            TxtI.ti_Font_B= new Font("Arial", Font.PLAIN, 12);  // Labels ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_C= new Font("Arial", Font.PLAIN, 12);  // Textbloecke ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_border= new Font("Arial", Font.BOLD, 12);  // --> Header in createTitledBorder  ... FONT_MENU_DEFAULT
        } else if (langCodeAktuell.equals("at")) {
            TxtI.ti_Font_A= new Font("Arial", Font.BOLD, 12);  // Menu ... FONT_MENU_DEFAULT
            TxtI.ti_Font_B= new Font("Arial", Font.PLAIN, 12);  // Labels ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_C= new Font("Arial", Font.PLAIN, 12);  // Textbloecke ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_border= new Font("Arial", Font.BOLD, 12);  // --> Header in createTitledBorder  ... FONT_MENU_DEFAULT
        } else if (langCodeAktuell.equals("jp")) {
            TxtI.ti_Font_A= new Font("Arial Unicode MS", Font.PLAIN, 14);  // Menu ... FONT_MENU_DEFAULT
            TxtI.ti_Font_B= new Font("Arial Unicode MS", Font.PLAIN, 14);  // Labels ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_C= new Font("Arial Unicode MS", Font.PLAIN, 14);  // Textbloecke ... FONT_TEXT_DEFAULT
            TxtI.ti_Font_border= new Font("Arial Unicode MS", Font.BOLD, 12);  // --> Header in createTitledBorder  ... FONT_MENU_DEFAULT
        }
    }




    //============================================================================================================
    //============================================================================================================


    // default -->
    public static Font ti_Font_A= TxtI.FONT_MENU_DEFAULT;
    public static Font ti_Font_B= TxtI.FONT_TEXT_DEFAULT;
    public static Font ti_Font_C= TxtI.FONT_TEXT_DEFAULT;
    public static Font ti_Font_border= TxtI.FONT_MENU_DEFAULT;




    //-----------------------------
    // Fenster.java -->
    //-----------------------------
    public static String ti_File= "File";
    public static String ti_New= "New";
    public static String ti_Open= "Open";
    public static String ti_Save= "Save";
    public static String ti_SaveAs= "Save As";
    public static String ti_Exit= "Exit";
    public static String ti_Prop= "Language...";
    public static String ti_Edit= "Edit";
    public static String ti_Undo= "Undo";
    public static String ti_Redo= "Redo";
    public static String ti_CreateSubcircuit= "Create Subcircuit";
    public static String ti_CopyElements= "Copy Elements";
    public static String ti_MoveElements= "Move Elements";
    public static String ti_DeleteElements= "Delete Elements";
    public static String ti_Deselect= "Deselect";
    public static String ti_Import= "Import";
    public static String ti_Export= "Export";
    public static String ti_Simulation= "Simulation";
    public static String ti_Parameter= "Parameter";
    public static String ti_InitStart= "Init & Start";
    public static String ti_Pause= "Pause";
    public static String ti_Continue= "Continue";
    public static String ti_Name= "Name";
    public static String ti_ShowParameter= "Show Parameter";
    public static String ti_ShowTextLine= "Show Text-Line";
    public static String ti_FlowDirection= "Flow Direction";
    public static String ti_WorksheetSize= "Worksheet Size";
    public static String ti_Scaling= "Scaling";
    public static String ti_FontSize= "Font Size";
    public static String ti_View= "View";
    public static String ti_Tools= "Tools";
    public static String ti_conTest= "Check Connections";
    public static String ti_optimizerSimple= "Parameter Setting";
    public static String ti_checkModel= "Check Model";
    public static String ti_Electromagnetic3DModel= "Electromagnetic 3D-Model";
    public static String ti_FilterOptimization= "Filter Optimization";
    public static String ti_TestRthCthExtractor= "Test Rth/Cth-Extractor";
    public static String ti_Help= "Help";
    public static String ti_About= "About";
    public static String ti_ReadySim= "Ready ...";
    public static String ti_StartingSim= "Starting Simulation ... ";
    public static String ti_StoppedSim= "Stopped after  ";
    public static String ti_RunningSim= "Running ...  ";
    public static String ti_OK= "OK";
    public static String ti_Cancel= "Cancel";




    //-----------------------------
    // TitledBorderDiv.java -->
    //-----------------------------
    public static String ti_x_DialogCheckSimulation= "Memory / Data Management";
    public static String ti_x_DialogLanguageSettings= "Select Language";
    public static String ti_x1_DialogSimParameter= "Parameters";
    public static String ti_x2_DialogSimParameter= "t_BR";
    public static String ti_x1_DialogElementLK= "AC Sinusoidal";
    public static String ti_x2_DialogElementLK= "DC Constant";
    public static String ti_x3_DialogElementLK= "Signal-Controlled";
    public static String ti_x4_DialogElementLK= "Circuit Model";
    public static String ti_x5_DialogElementLK= "Losses Based on Circuit Model";
    public static String ti_x6_DialogElementLK= "Magnetically Coupled";
    public static String ti_x7_DialogElementLK= "Detailed Loss Model";
    public static String ti_x1_DialogVerlusteDetail= "Data Test";
    public static String ti_x2_DialogVerlusteDetail= "Edit Curves";
    public static String ti_x3_DialogVerlusteDetail= "Data of Selected Curve";
    public static String ti_x4_DialogVerlusteDetail= "Curve Parameter";
    public static String ti_x5_DialogVerlusteDetail= "Curves";
    public static String ti_x1_DialogElementCONTROL= "Define Nodes for Measurement";
    public static String ti_x2_DialogElementCONTROL= "Select Circuit Element";
    public static String ti_x3_DialogElementCONTROL= "Sinusoidal";
    public static String ti_x4_DialogElementCONTROL= "Rectangular";
    public static String ti_x5_DialogElementCONTROL= "Triangular";
    public static String ti_x6_DialogElementCONTROL= "Random Walk";
    public static String ti_x7_DialogElementCONTROL= "Import ASCII File";
    public static String ti_x8_DialogElementCONTROL= "Source Type";
    public static String ti_x9_DialogElementCONTROL= "Semiconductor to be Controlled";
    public static String ti_x11_DialogElementCONTROL= "Select Thermal Element";
    public static String ti_x12_DialogElementCONTROL= "Function";
    public static String ti_x13_DialogElementCONTROL= "Info";
    public static String ti_x14_DialogElementCONTROL= "StateVariable to be Set";
    public static String ti_x1_RthCthESBueberblick= "Thermal Impedance Matrix";
    public static String ti_x2_RthCthESBueberblick= "Thermal Step Response - Diagram";
    public static String ti_x3_RthCthESBueberblick= "Thermal Equivalent Circuit";
    public static String ti_x1_DialogAvgRms= "Set Range";
    public static String ti_x2_DialogAvgRms= "A";
    public static String ti_x3_DialogAvgRms= "B";
    public static String ti_x1_DialogConnectSignalsGraphs= "Matrix Signal - Graph";
    public static String ti_x2_DialogConnectSignalsGraphs= "Edit Graph Number";
    public static String ti_x3_DialogConnectSignalsGraphs= "Status-Message";
    public static String ti_x1_DialogCurveProperties= "General";
    public static String ti_x2_DialogCurveProperties= "Line Properties";
    public static String ti_x3_DialogCurveProperties= "Symbol on Data-Points";
    public static String ti_x4_DialogCurveProperties= "Clipping";
    public static String ti_x_DialogDigitalCurveProperties= "Filling";
    public static String ti_x_DialogEditText= "Text Entry";
    public static String ti_x1_DialogFourier= "Harmonics";
    public static String ti_x2_DialogFourier= "Select Curve(s)";
    public static String ti_x_DialogOrdnungSIGNAL= "Signal Order";
    public static String ti_x1_TestReceiverCISPR16= "Algorithm";
    public static String ti_x2_TestReceiverCISPR16= "Calculation";
    public static String ti_x1_DialogElementTHERM= "Measure Power Loss of Component";
    public static String ti_x2_DialogElementTHERM= "Heat Sink Geometry";
    public static String ti_x3_DialogElementTHERM= "Fan Characteristic";
    public static String ti_x4_DialogElementTHERM= "Constant";
    public static String ti_x5_DialogElementTHERM= "Power Module Description";
    public static String ti_xx1_DialogElementCONTROL= "Select Machine";
    public static String ti_x2_DialogSimulinkZeitschrittProblem= "Problem With External Time-Step";
    public static String ti_txt01_DialogSimulinkZeitschrittProblem= "WARNING: Time-step as set in GeckoCIRCUITS is different from external time-step. Therefore, the simulation results might be wrong. ";
    public static String ti_x2_DialogInfoFestplattenSpeicherungAktivieren= "Data on Harddisk";
    public static String ti_txt01_DialogInfoFestplattenSpeicherungAktivieren= "INFORMATION: Don't forget to set 'Activate Data Saving' before running the simulation. After ending the simulation, you can write all SCOPE-data to the harddisk by 'Write Data to File'.";




    //-----------------------------
    // Scope.java -->
    //-----------------------------
    public static String ti_grfTab_ScopeX= "Graph";
    public static String ti_datTab_ScopeX= "Data";
    public static String ti_tt1_ScopeX= "Deactivate mouse";
    public static String ti_tt2_ScopeX= "Autoscale";
    public static String ti_tt3_ScopeX= "Zoom rectangle";
    public static String ti_tt4_ScopeX= "Set slider for X/Y-values";
    public static String ti_tt5_ScopeX= "Draw line";
    public static String ti_tt6_ScopeX= "Draw text";
    public static String ti_jtb_ScopeX= "Mouse Options";
    public static String ti_jcbSav_ScopeX= "Activate Data Saving";
    public static String ti_jmiWri_ScopeX= "Write Data To File";
    public static String ti_jmGrf_ScopeX= "Graphs";
    public static String ti_jmiMtx_ScopeX= "Signal - Graph";
    public static String ti_jmAna_ScopeX= "Analysis";
    public static String ti_jmiChar_ScopeX= "Characteristics";
    public static String ti_jmiFour_ScopeX= "Fourier";
    //-----------------------------
    // GraferV3.java -->
    //-----------------------------
    public static String[] ti_linStil_GraferV3= new String[]{
        "SOLID_PLAIN", "INVISIBLE", "SOLID_FAT_1", "SOLID_FAT_2", "DOTTED_PLAIN", "DOTTED_FAT"
    };
    public static String[] ti_farbe_GraferV3= new String[]{
        "black", "red", "green", "blue", "darkgray", "gray", "ligthgray", "white", "magenta", "cyan", "orange", "yellow", "darkgreen"
    };
    public static String[] ti_formSymb_GraferV3= new String[]{
        "CIRCLE", "CIRCLE_FILLED", "CROSS", "RECT", "RECT_FILLED", "TRIANG", "TRIANG_FILLED"
    };
    //-----------------------------
    // DialogFourier.java -->
    //-----------------------------
    public static String ti_scRa_DialogFourier= "Scope-Range";
    public static String ti_dfRa_DialogFourier= "Define Range";
    public static String ti_slRa_DialogFourier= "Slider-Range";
    public static String ti_calc_DialogFourier= "Calculate";
    public static String ti_e1_DialogFourier= "Error: Negative range";
    public static String ti_e2_DialogFourier= "Error: Range exceeds data";
    public static String ti_e3_DialogFourier= "Error: Bad range [nMin..nMax]";
    public static String ti_e4_DialogFourier= "Error: Bad range [t1..t2]";
    public static String ti_calcFin_DialogFourier= "Calculation finished.";
    public static String ti_datErr_DialogFourier= "Data-Error";
    public static String ti_clcx_DialogFourier= "calculating";
    //-----------------------------
    // DialogAvgRms.java -->
    //-----------------------------
    public static String ti_def_DialogAvgRms= "Definitions";
    public static String ti_deact_DialogAvgRms= "deactivated";
    public static String ti_pa_DialogAvgRms= "Power Analysis";
    public static String ti_calcrun_DialogAvgRms= "Calculation running";
    public static String ti_calcok_DialogAvgRms= "Calculation OK";
    //-----------------------------
    // DialogFourierDiagramm.java -->
    //-----------------------------
    public static String ti_fa_DialogFourierDiagramm= "Fourier Analysis";
    public static String ti_wd_DialogFourierDiagramm= "Worksheet Data";
    public static String ti_rec_DialogFourierDiagramm= "Reconstruction";
    //-----------------------------
    // DialogCurveProperties.java -->
    //-----------------------------
    public static String ti_cur_DialogCurveProperties= "Curve";
    public static String ti_ax_DialogCurveProperties= "Axis";
    public static String ti_sty_DialogCurveProperties= "Style";
    public static String ti_col_DialogCurveProperties= "Color";
    public static String ti_shsy_DialogCurveProperties= "Show Symbol";
    public static String ti_frq_DialogCurveProperties= "Freq.";
    public static String ti_shp_DialogCurveProperties= "Shape";
    //-----------------------------
    // DialogDigitalCurveProperties.java -->
    //-----------------------------
    public static String ti_fill_DialogDigitalCurveProperties= "Fill Digital Curves";
    //-----------------------------
    // DialogOrdnungSIGNAL.java -->
    //-----------------------------
    public static String ti_txt_DialogOrdnungSIGNAL= "Drag Signal-Name in Table with the Mouse to define Signal-Order in Graph.";
    //-----------------------------
    // DialogConnectSignalsGraphs.java -->
    //-----------------------------
    public static String ti_ttip_DialogConnectSignalsGraphs= "Click Left or Right Mouse Button to Edit Curve";
    public static String ti_stat_DialogConnectSignalsGraphs= "Status";
    public static String ti_weig_DialogConnectSignalsGraphs= "Y-Weight [%]";
    public static String ti_add_DialogConnectSignalsGraphs= "Add Graph";
    public static String ti_rmv_DialogConnectSignalsGraphs= "Delete Graph";
    public static String ti_se1_DialogConnectSignalsGraphs= "Error: No X-axis defined";
    public static String ti_se2_DialogConnectSignalsGraphs= "Error: More than one X-axis defined";
    public static String ti_se3_DialogConnectSignalsGraphs= "Error: No Y/SG-axis defined";
    public static String ti_se4_DialogConnectSignalsGraphs= "Error: Don't mix Y-axis AND Signal-Mode";
    public static String ti_err_DialogConnectSignalsGraphs= "Error";
    public static String ti_inok_DialogConnectSignalsGraphs= "Input OK";
    //-----------------------------
    // TestReceiverCISPR16.java -->
    //-----------------------------
    public static String ti_if1_TestReceiverCISPR16= "Waiting for data. Time-domain simulation not finished.";
    public static String ti_if2_TestReceiverCISPR16= "Press 'Calculate EMI' to start calculation.";
    public static String ti_if3_TestReceiverCISPR16= "Calc. Parameter";
    public static String ti_q1_TestReceiverCISPR16= "f_low [Hz]";
    public static String ti_q2_TestReceiverCISPR16= "f_high [Hz]";
    public static String ti_q3_TestReceiverCISPR16= "nr of samples";
    public static String ti_q4_TestReceiverCISPR16= "Linear distribution";
    public static String ti_q5_TestReceiverCISPR16= "Logarithmic distrubution";
    public static String ti_q6_TestReceiverCISPR16= "PEAK-Detection";
    public static String ti_q7_TestReceiverCISPR16= "QP-Detector";
    public static String ti_q8_TestReceiverCISPR16= "Algo. 3";
    public static String ti_r1_TestReceiverCISPR16= "Status [%]";
    public static String ti_r2_TestReceiverCISPR16= "Time";
    public static String ti_r4_TestReceiverCISPR16= "Calculate EMI";
    public static String ti_r5_TestReceiverCISPR16= "Abort";
    public static String ti_r6_TestReceiverCISPR16= "FFT ...";
    public static String ti_r7_TestReceiverCISPR16= "Done.";
    //-----------------------------
    // TestReceiverCISPR16Calculator.java -->
    //-----------------------------
    public static String ti_warn1_TestReceiverCISPR16Calculator= "WARNING:\nStep-width in the proceeding transient simulation:  dt =";
    public static String ti_warn2_TestReceiverCISPR16Calculator= "With this step-width the maximum frequency cannot be calculated: f_High =";
    public static String ti_warn3_TestReceiverCISPR16Calculator= "Set dt < ";
    public static String ti_warn4_TestReceiverCISPR16Calculator= "With this new setting the following frequency range is covered:  f = [";
    public static String ti_info1_TestReceiverCISPR16Calculator= "INFORMATION:\nStep-width in the proceeding transient simulation:  dt =";
    public static String ti_info2_TestReceiverCISPR16Calculator= "With this step-width the maximum frequency can be calculated: f_High =";
    public static String ti_info3_TestReceiverCISPR16Calculator= "You could reduce the step-width to dt =";
    public static String ti_info4_TestReceiverCISPR16Calculator= "With this new setting the frequency range would be still covered:  f = [";
    public static String ti_err_TestReceiverCISPR16Calculator= "Frequency-domain simulation has been aborted at f_max =";
    public static String ti_mem_TestReceiverCISPR16Calculator= "ERROR: Java is out of memory.\nTo increase memory (e.g. 512MB) >>\n\n> java -Xmx512m -jar GeckoCIRCUITS.jar";




    //-----------------------------
    // LKreisX.java -->
    //-----------------------------
    public static String ti_U1_LKreisX= "no control-sgn";
    public static String ti_MOTOR_LKreisX= "no torque defined";
    public static String ti_M1_LKreisX= "not defined";
    public static String ti_D1_LKreisX= "loss-file not found";
    public static String ti_S1_LKreisX= "no gate-signal";
    public static String ti_S2_LKreisX= "more than one gate-signal";
    //-----------------------------
    // DialogElementLK.java -->
    //-----------------------------
    public static String ti_Name_DialogElementLK= "Name";
    public static String ti_UI1_DialogElementLK= "uMAX";
    public static String ti_UI2_DialogElementLK= "iMAX";
    public static String ti_UI3_DialogElementLK= "f [Hz]";
    public static String ti_UI4_DialogElementLK= "offset";
    public static String ti_UI5_DialogElementLK= "phase [°]";
    public static String ti_UI6_DialogElementLK= "AC";
    public static String ti_UIdc1_DialogElementLK= "uDC";
    public static String ti_UIdc2_DialogElementLK= "iDC";
    public static String ti_UIdc3_DialogElementLK= "DC";
    public static String ti_UIs1_DialogElementLK= "No signal nodes defined.";
    public static String ti_UIs2_DialogElementLK= "Sign";
    public static String ti_UIs3_DialogElementLK= "Display Details";
    public static String ti_Ds1txt_DialogElementLK=
            "Switching losses are neglected. ";
    public static String ti_Ds2txt_DialogElementLK=
            "Calculate conduction losses based on the simplified characteristic (Uf, rON) as defined in the 'Characteristic'-Tab. ";
    public static String ti_Ds1tab_DialogElementLK= "Losses";
    public static String ti_Ds2tab_DialogElementLK= "Info";
    public static String ti_Ds3tab_DialogElementLK= "Characteristic";
    public static String ti_Ds4tab_DialogElementLK= "Thermal Losses";
    public static String ti_S1txt_DialogElementLK=
            "Calculate conduction losses based on the simplified characteristic (Uf, rON) as defined in the 'Characteristic'-Tab. "
            + "\n\nSimplified switching losses: "
            + "\n(1) Temperature-independent"
            + "\n(2) Linear current-dependency via kON & kOFF "
            + "\n(3) Linear dependency on blocking voltage uK"
            + "\n\nVirtual Capacity Coss,er:"
            +"\nConstant energy-related internal capacitance of the power switch. This capacity is not included in the electrical simulation, but used to estimate losses.";
    public static String ti_M1_DialogElementLK= "Show";
    public static String ti_M2_DialogElementLK= "Define two Lc in power circuit.";
    public static String ti_M3_DialogElementLK= "Parameter";
    public static String ti_M4_DialogElementLK= "Equations";
    public static String ti_lisn1_DialogElementLK= "Definition";
    public static String ti_Losstxt_DialogElementLK=
            "Calculate switching and conduction losses  based on the characteristics specified in the file. All losses are temperature-dependent."
            + "Loss calculations are performed in parallel to the circuit simulation. Circuit Model for the circuit simulation still performed as "
            + "specified in the 'Characteristic'-Tab. ";
    public static String ti_Loss1_DialogElementLK= "Load Semiconductor";
    public static String ti_Loss2_DialogElementLK= "Edit Semiconductor";
    public static String ti_Loss3_DialogElementLK= "Create New";
    public static String ti_Loss4_DialogElementLK= "File";
    public static String ti_Loss5_DialogElementLK= "Info";
    //-----------------------------
    // DialogVerlusteDetail.java -->
    //-----------------------------
    public static String ti_conLoss_DialogVerlusteDetail= "Conduction Losses";
    public static String ti_swLoss_DialogVerlusteDetail= "Switching Losses";
    public static String ti_lossSC_DialogVerlusteDetail= "Overwrite & Save Changes";
    public static String ti_lossSN_DialogVerlusteDetail= "Save As New";
    public static String ti_loss1_DialogVerlusteDetail= "Check curves at";
    public static String ti_loss2_DialogVerlusteDetail= "Show";
    public static String ti_lossAdd_DialogVerlusteDetail= "Add New";
    public static String ti_lossDel_DialogVerlusteDetail= "Delete";
    public static String ti_lossCon_DialogVerlusteDetail= "Confirm Changes";
    //-----------------------------
    // DialogElementCONTROL.java -->
    //-----------------------------
    public static String ti_ctrVolt_DialogElementCONTROL= "Define two node labels in power circuit.";
    public static String ti_ctrAmp_DialogElementCONTROL= "No elements in power circuit defined.";
    public static String ti_ctrSIGv1_DialogElementCONTROL= "Show";
    public static String ti_ctrSIGv2_DialogElementCONTROL= "Close";
    public static String ti_ctrSIGv3_DialogElementCONTROL= "View Curve  >>";
    public static String ti_ctrSIGd3_DialogElementCONTROL= "Import Data";
    public static String ti_ctrSIGd4_DialogElementCONTROL= "No external data file specified";
    public static String ti_ctrSIGe1_DialogElementCONTROL= "Use external parameters";
    public static String ti_ctrSIGe2_DialogElementCONTROL= "Display Details";
    public static String ti_ctrSIGtxt_DialogElementCONTROL=
            "Data Format (Space-Separator)"
            + "[ time  -  value ]";
    public static String ti_ctrSIGa_DialogElementCONTROL= "SIN";
    public static String ti_ctrSIGb_DialogElementCONTROL= "REC";
    public static String ti_ctrSIGc_DialogElementCONTROL= "TRI";
    public static String ti_ctrSIGd_DialogElementCONTROL= "RND";
    public static String ti_ctrSIGe_DialogElementCONTROL= "DAT";
    public static String ti_ctrSW_DialogElementCONTROL= "No switch existing in power circuit.";
    public static String ti_ctrTEMP_DialogElementCONTROL= "Define two node labels in thermal circuit.";
    public static String ti_ctrFLOW_DialogElementCONTROL= "No elements in thermal circuit defined.";
    public static String ti_ctrEXT_DialogElementCONTROL= "Terminals";
    public static String ti_ctrDIGS1_DialogElementCONTROL= "Resolution [us]";
    public static String ti_ctrLGC1_DialogElementCONTROL= "Input x1 is assumed to be in [rad]. ";
    public static String ti_ctrLGC2_DialogElementCONTROL= "Output y1 is in [rad].";
    public static String ti_ctrLGC3_DialogElementCONTROL= "Input x1 is automatically restricted to [-1..+1].";
    public static String ti_ctrCNT_DialogElementCONTROL= "c ... internal count";
    public static String ti_ctrSTA1_DialogElementCONTROL= "Use StateVariable as 'global' variable.";
    public static String ti_ctrSTA2_DialogElementCONTROL= "Change value employing 'Set STATE-Variable'.";
    public static String ti_ctrSTA3_DialogElementCONTROL= "unchanged";
    public static String ti_ctrSTA4_DialogElementCONTROL= "No StateVariables defined.";
    public static String ti_ctrNotDef_DialogElementCONTROL= "not defined";
    public static String ti_ctrSgW1_DialogElementCONTROL= "DC-Type";
    public static String ti_ctrSgW2_DialogElementCONTROL= "Random";
    public static String ti_ctrSgW3_DialogElementCONTROL= "Import-Data";
    public static String ti_ctrSgW4_DialogElementCONTROL= "Sin.-Type";
    public static String ti_ctrSgW5_DialogElementCONTROL= "Rect.-Type";
    public static String ti_ctrSgW6_DialogElementCONTROL= "Tri.-Type";
    public static String ti_ctrMot_DialogElementCONTROL= "No machine in power circuit defined.";
    //-----------------------------
    // ReglerJavaFunction.java -->
    //-----------------------------
    public static String ti_ctrJAVAinfo_ReglerJavaFunction=
            "The 'Java Code' control block contains a freely programmable interface which allows an easy and"
            + "efficient implementation of complex control structures. Furthermore, the complete Java API is"
            + "available and makes GeckoCIRCUITS arbitrarily extendable."
            + "\n\n-----Technical details -----"
            + "\n\nThe interface function:"
            + "\n\npublic static double[] calculateYOUT(final double[] xIN, final double time) { "
            + "\n       // your custom code"
            + "\n       //return yOUT;"
            + "\n}"
            + "\n\nis called at every simulation timestep. Input arguments are the block input ports (as a double "
            + "array xIN) and the current simulation time. The return value is a predefined double array yOUT."
            + "\n\nThe function call itself is 'static', which requires all invoked functions and variables to be "
            + "static, too. It is recommended not to use the 'new' operator within this function, since it "
            + "would allocate memory at every simulation step. Therefore, (static) variables should be defined "
            + "in the 'static variables' field, and initialized immediately, or within the 'static initializer' field."
            + "\n\nA code example is supplied (button 'Load example code') which shows a simple assignment of "
            + "output values, and it displays the actual simulation time inside a newly generated window. "
            + "\n\nAfter a successful code compilation, the Java Code block appears in green color, otherwise an error is "
            + "\n\nFor further documentation, refer to the various Java Tutorials or books.";
    public static String ti_ctrJAVAtab1_ReglerJavaFunction= "Code";
    public static String ti_ctrJAVAtab2_ReglerJavaFunction= "Compiler Messages";
    public static String ti_ctrJAVAtab3_ReglerJavaFunction= "Info";
    public static String ti_ctrJAVAbsp1_ReglerJavaFunction= "Example 1";
    public static String ti_ctrJAVAbsp2_ReglerJavaFunction= "Example 2";
    public static String ti_ctrJAVAjb1_ReglerJavaFunction=  "Compile Code";
    public static String ti_ctrJAVAjb2_ReglerJavaFunction=  "Close Window";
    //-----------------------------
    // DialogElementTHERM.java -->
    //-----------------------------
    public static String ti_sm_DialogElementTHERM= "No switch defined in power circuit.";
    public static String ti_temp_DialogElementTHERM= "Temperature";
    public static String ti_heat_DialogElementTHERM= "Heat Flow";
    public static String ti_mod1_DialogElementTHERM= "View RthCth-Network Model";
    public static String ti_mod2_DialogElementTHERM= "Load Power Module";
    public static String ti_mod3_DialogElementTHERM= "Create New >> Datasheet";
    public static String ti_mod4_DialogElementTHERM= "Create New >> 3D-FEM";
    //-----------------------------
    // THERMKreisX.java -->
    //-----------------------------
    public static String ti_c_THERMKreisX= "const";
    public static String ti_loss_THERMKreisX= "Loss";
    //-----------------------------
    // DialogViewPowerModule.java -->
    //-----------------------------
    public static String ti_rc_DialogViewPowerModule= "RthCth-Network Model";
    public static String ti_3d_DialogViewPowerModule= "3D Structure";
    public static String ti_noMod_DialogViewPowerModule= "No power module defined";
    public static String ti_ferr_DialogViewPowerModule= "File Error";
    //-----------------------------
    // SpaceVectorDisplay.java -->
    //-----------------------------
    public static String ti_SVDis_1= "Length Scale";
    public static String ti_SVDis_2= "Average Time [us]";
    public static String ti_SVDis_3= "Pause Time [us]";
    public static String ti_SVDis_b1= "Line";
    public static String ti_SVDis_b2= "Point";
    public static String ti_SVDis_clear= "Clear";





    //-----------------------------
    // DialogDiodenError.java -->
    //-----------------------------
    public static String ti_txt01_DialogDiodenError=
            "Error:\nNumerical instability of switch.\nSimulation aborted at ";
    //-----------------------------
    // DialogSimParameter.java -->
    //-----------------------------
    public static String ti_SimulationParameters= "Simulation Parameters";
    public static String ti_txt01_DialogSimParameter=
            "Set 't_BR' to stop the simulation at a defined time before 't_END'. Set 't_BR= -1' to disable this feature.";
    //-----------------------------
    // DialogErrorJava3D.java -->
    //-----------------------------
    public static String ti_txt01_DialogErrorJava3D=
            "\n\n"
            + "For running the Thermal 3D-Solver, Java3D has to be installed. It is not part of the standard Java Runtime Environment (JRE). Java3D can be downloaded for free at\n\n"
            + "http://java.sun.com/products/java-media/3D/download.html"
            + "\n\n\n(1) Download Java3D and inspect the files."
            + "\n\n(2) There might be more than one JREs be installed on your computer. Locate the active JRE that is used by the operating system. The Java Control Panel (access e.g. via Control Panel) gives detailled information."
            + "\n\n(3) Three DLL-files of the downloaded Java3D have to be put into the BIN-folder as marked in the example."
            + "\n\n(4) Four JAR-files of the downloaded Java3D have to be put into the LIB/EXT-folder as marked in the example.";
    public static String ti_tab1_DialogErrorJava3D= "(1) Download Java3D";
    public static String ti_tab2_DialogErrorJava3D= "(2) Locate active JRE";
    public static String ti_tab3_DialogErrorJava3D= "(3) bin   >>   *.DLL ";
    public static String ti_tab4_DialogErrorJava3D= "(4) ext   >>   *.JAR ";
    //-----------------------------
    // DialogErrorJava3D.java -->
    //-----------------------------
    public static String ti_txt01_DialogErrorComplierTools=
            "To use Gecko-CIRCUITS-Block 'JAVA-Function', the file 'tools.jar' (from Java JDK folder lib/) has to be put into Java JRE library-path as shown in the screenshot. \n";
    //-----------------------------
    // DialogJavaVersionError.java -->
    //-----------------------------
    public static String ti_txt01_DialogJavaVersionError=
            " needs Java 1.6 (or higher) to be installed on your computer.\n\n";
    public static String ti_txt02_DialogJavaVersionError=
            "Currently you employ Java ";
    public static String ti_txt03_DialogJavaVersionError=
            ". Please update accordingly (free download at http://www.java.sun.com)\n\n";
    //-----------------------------
    // DialogLizenz.java -->
    //-----------------------------
    public static String ti_txt01_DialogLizenz=
            "\nThe licence of this version is expired.\n\nRequest an updated version from ";
    //-----------------------------
    // DialogCheckSimulation.java -->
    //-----------------------------
    public static String ti_txt01_DialogCheckSimulation=
            "INFORMATION:\n\nData-writing onto the harddisk will significantly slow down the simulation. "
            + "Data-writing is deactivated as default, but can be activated via checkbox in "
            + "the Scope-Window-Menu at Data_>>_ActivateDataSaving.\n";
    public static String ti_lab1_DialogCheckSimulation= "Data to be stored [MB]:   ";
    public static String ti_lab2_DialogCheckSimulation= "Available memory [MB]:   ";
    public static String ti_lab3_DialogCheckSimulation= "Estimated disk space [MB]:   ";
    public static String ti_ok_DialogCheckSimulation= "OK - Start Simulation";
    public static String ti_apply_DialogCheckSimulation= "Apply";
    public static String ti_redDisk_DialogCheckSimulation= "Harddisk storage reduction factor:   ";
    public static String ti_warn01_DialogCheckSimulation=
            "\nWARNING:\n\nAvailable memory too small for temporary data storage needed for Zooming, Fourier and Analysis. " +
            "Temporary data will be reduced by this factor:";
    public static String ti_warn02a_DialogCheckSimulation=
            "\nWARNING:\n\nTotal data [GB] written to harddisk will be:";
    public static String ti_warn02b_DialogCheckSimulation=
            "This can be reduced by a user-defined factor:\n";
    public static String ti_warn03a_DialogCheckSimulation=
            "\nWARNING:\n\n(-) Available memory too small for temporary data storage needed for Zooming, Fourier and Analysis. "
            + "Temporary data will be reduced by this factor:";
    public static String ti_warn03b_DialogCheckSimulation=
            "\n\n(-) Data written to harddisk will be";
    public static String ti_warn03c_DialogCheckSimulation= ti_warn02b_DialogCheckSimulation;
    //-----------------------------
    // DialogMatrixSingular.java -->
    //-----------------------------
    public static String ti_txt01_DialogMatrixSingular=
            "\n*** MODEL ERROR ***\n\nThe simulation cannot be performed.\n\nPlease check the circuit model for"
            + "\n\n- Subcircuits without ground connection\n- Parallel voltage / temperature sources"
            + "\n- Parallel capacitors with initial conditions set\n- Missing connections between elements\n\n";
    //-----------------------------
    // DialogErrorDtChange.java -->
    //-----------------------------
    public static String ti_txt01_DialogErrorDtChange=
            "Error:\n\nSimulation parameters 'dt' and/or 't_SIM' cannot be changed before t_END."
            + "\n\nAbort current simulation to restart with the new parameters or keep old parameters to continue simulation. ";
    public static String ti_ok_DialogErrorDtChange= "Stop simulation";
    public static String ti_weiter_DialogErrorDtChange= "Continue with old parameters";
    //-----------------------------
    // DialogWarningNodeNumber.java -->
    //-----------------------------
    public static String ti_txt01_DialogWarningNodeNumber=
            "\n*** WARNING ***\n\nThe node number of the model has been changed."
            + "Proceeding with the simulation might give incorrect results.\n\n";
    //-----------------------------
    // DialogQuitWithoutSaving.java -->
    //-----------------------------
    public static String ti_txt01_DialogQuitWithoutSaving=
            "\n*** WARNING ***\n\nThe content of the file has changed.\nDo you want to save the changes?\n";
    public static String ti_jbYes_DialogQuitWithoutSaving= "Yes";
    public static String ti_jbNo_DialogQuitWithoutSaving= "No";
    //-----------------------------
    // DialogAbout.java -->
    //-----------------------------
    public static String ti_x1_DialogAbout= "Licenced to";
    public static String ti_x2_DialogAbout= "relased";
    public static String ti_x3_DialogAbout= "written by";
    public static String ti_x4_DialogAbout= "expires";
    //-----------------------------
    // VerlustBerechnung.java -->
    //-----------------------------
    public static String ti_x1_VerlustBerechnung= "No semiconductor defined";
    public static String ti_x2_VerlustBerechnung= "not found";
    //-----------------------------
    // DialogLanguageSettings.java -->
    //-----------------------------
    public static String ti_tx1_DialogLanguageSettings=
            "Select a language. Your selection is stored in the properties-file. Default-language is English."
            + "\n\nYour selected language will used after restart of GeckoCIRCUITS.\n";




    //-----------------------------
    // SchematischeEingabeAuswahl2.java -->
    //-----------------------------
    public static String[] ti_elementNameLK= new String[]{
        "Voltage Source  U [V]", "Current Source  I [A]", "Resistor  R [ohm]", "Capacitor  C [F]", "Inductor  L [H]", "Inductor Coupling Lc [H]", "Magnetic Coupling  k",
        "Ideal Switch S", "IGBT", "Diode  D", "Thyristor  THYR", "OP-AMP" // "v.2 Inductor Coupling Lc [H]"
    };
    public static String[] ti_elementNameLKmotor= new String[]{
        "DC Machine", "Permanent Magnet Synchronous Machine", "Salient Pole Synchronous Machine",
        "Round Rotor Synchronous Machine", "Squirrel Cage Induction Machine",
        "Induction Machine", "Saturable Induction Machine", /*"Induction Machine with Open Stator Windings",*/ "LISN",
    };
    public static String[] ti_elementNameCONTROL= new String[]{
        "GAIN", "PT1-Control", /*"PT2-Control",*/ "Integrator", "PI-Control", "PD-Control", "Hysteresis", "LIMIT", "ADD", "SUB", "MUL", "DIV",
        "Minimum", "Maximum", "SIGNUM",
    };
    public static String[] ti_elementNameCONTROL2= new String[]{
        "SCOPE", "Voltage [V]", "Current [A]", "Temperature [K]", "Heatflow [W]", "Machine - internal", "EMI Test Receiver", "Space Vector Plotter"//, "Scope2"
    };
    public static String[] ti_elementNameCONTROL3= new String[]{
        "NOT", "AND", "OR", "XOR",
        "Digital Source", "Delay", "Sample-Hold", "Counter",
        "GreaterEqual (>=)", "Greater (>)", "Equal (==)", "NotEqual (!=)",
    };
    public static String[] ti_elementNameCONTROL4= new String[]{
        "ABS", "ROUND", "SQR", "SQRT", "POWER", "EXP", "LN",
        "SIN", "COS", "TAN", "ASIN", "ACOS", "ATAN",
    };
    public static String[] ti_elementNameCONTROL5= new String[]{
        "Signal Source", "CONST", /*"PWM Source",*/ "Gate Control",
        "Interface to EXTERNAL", "Signal from EXTERNAL"
    };
    public static String[] ti_elementNameCONTROL6= new String[]{
        "JAVA-Function", "abc >> dq", "dq >> abc",
        /*"IF-THEN", "Define STATE-Variable", "Set STATE-Variable", "Logik-Block",*/ "time", "Sparse-Matrix Control", "Thyristor Control", "Text Field"
    };
    public static String[] ti_elementNameTHERM= new String[]{
        "Defined Temperature [+C]", "Heat Source [W]", "Thermal Loss [W]",
        "Resistance Rth [K/W]", "Capacitance Cth [Ws/K]",
        "Power Module", "Reference Temp.", //"Heat Sink", , "Inductor & Trafo Losses"
    };
    public static String ti_Circuit= "Circuit";
    public static String ti_MotorEMI= "Motor & EMI";
    public static String ti_Thermal= "Thermal";
    public static String ti_Control= "Control";
    public static String ti_Measure= "Measure";
    public static String ti_Digital= "Digital";
    public static String ti_Math= "Math";
    public static String ti_SourceSink= "Source/Sink";
    public static String ti_Special= "Special";


}



