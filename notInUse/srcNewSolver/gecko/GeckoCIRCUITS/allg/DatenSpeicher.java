/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.allg;

//import de.matrixlock.LicenseManager;
import java.io.Serializable;
import java.io.File;
import java.util.StringTokenizer;
import java.util.Date; 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException; 

//import gecko.GeckoCIRCUITS.circuit.Verbindung;
//import gecko.GeckoCIRCUITS.circuit.ElementLK;
//import gecko.GeckoCIRCUITS.circuit.SchematischeEingabe2;
//import gecko.GeckoCIRCUITS.control.ElementCONTROL;
//import gecko.GeckoCIRCUITS.therm.ElementTHERM;
import java.util.Random;
//import javax.swing.JOptionPane;



// Hilfsklasse: Format fuer die Daten-Speicherung
public class DatenSpeicher implements Serializable {

    //------------------
//    public Verbindung[] v, vCONTROL, vTHERM;
    public int verbindungANZAHL, verbindungControlANZAHL, verbindungThermANZAHL;
    //------------------
   // public ElementLK[] e;
    public int elementANZAHL;
    public int[] staticZaehlerLKelementeINIT;
    //------------------
 //   public ElementCONTROL[] c;
    public int controlANZAHL;
    public int[] staticZaehlerReglerBlockINIT;
    //------------------
  //  public ElementTHERM[] eTH;
    public int thermANZAHL;
    public int[] staticZaehlerThermBlockINIT;
    //------------------
  //  public OptimizerParameterData optimizerParameterData;
  //  private GeckoOPTIMIZER geckoOpt;
    public String geckoOpt_code_ascii; 
    //------------------
    // Simulationsparameter
    public double dt, tDURATION;
    public static long exp;
    public double tPAUSE=-1, Tss, maxErrorNewton;
    public int maxIterationsNewton;
    //------------------
    // Ansicht im SchematicEntry
    public int dpix = 16;
    public int fontSize;
    public String fontTyp;
    public int fensterWidth=-1, fensterHeight=-1;  // Speicherung der individuellen Fenstergroesse 
    public String worksheetSize;  // zB. 600x600
    //------------------
    private DateFormat dFormat= new SimpleDateFormat("yyyy-MM-dd");
    private boolean zeitlizenzOK= true;
    double _dt_pre;
    double _T_pre;
    public int _uniqueFileId;
    //------------------

    public DatenSpeicher (
            //ElementLK[] e, int elementANZAHL, int[] staticZaehlerLKelementeINIT,
            //Verbindung[] v, int verbindungANZAHL,
           // ElementCONTROL[] c, int controlANZAHL, int[] staticZaehlerReglerBlockINIT,
           // Verbindung[] vCONTROL, int verbindungControlANZAHL,
           // ElementTHERM[] eTH, int thermANZAHL, int[] staticZaehlerThermBlockINIT,
           // Verbindung[] vTHERM, int verbindungThermANZAHL,
            double dt, double tDURATION, double T_pre, double dt_pre, int dpix, int fontSize, String fontTyp,
            int fensterWidth, int fensterHeight, String worksheetSize, 
            //OptimizerParameterData optimizerParameterData, GeckoOPTIMIZER geckoOpt,
            double tPAUSE, double Tss, int maxIterationsNewton, double maxErrorNewton, int uniqueFileId
        ) {
        //---------------
        //this.v= v;
        this.verbindungANZAHL= verbindungANZAHL;
        //this.e= e;
        this.elementANZAHL= elementANZAHL;
        this.staticZaehlerLKelementeINIT= staticZaehlerLKelementeINIT;
        //---------------
       // this.c= c;
        this.controlANZAHL= controlANZAHL;
       // this.vCONTROL= vCONTROL;
        this.verbindungControlANZAHL= verbindungControlANZAHL;
        this.staticZaehlerReglerBlockINIT= staticZaehlerReglerBlockINIT;
        //---------------
       // this.eTH= eTH;
        this.thermANZAHL= thermANZAHL;
       // this.vTHERM= vTHERM;
        this.verbindungThermANZAHL= verbindungThermANZAHL;
        this.staticZaehlerThermBlockINIT= staticZaehlerThermBlockINIT;
        //---------------
        this.dt= dt;
        this.tDURATION= tDURATION;
        this._T_pre = T_pre;
        this._dt_pre = dt_pre;

        this.dpix= dpix;
        this.fontSize= fontSize;
        this.fontTyp= fontTyp;
        this.fensterWidth=fensterWidth;   this.fensterHeight=fensterHeight; 
        this.worksheetSize= worksheetSize; 
        //---------------
        //this.optimizerParameterData= optimizerParameterData;
       // this.geckoOpt= geckoOpt;
        //---------------
        this.tPAUSE= tPAUSE;
        this.Tss= Tss;
        this.maxIterationsNewton= maxIterationsNewton;
        this.maxErrorNewton= maxErrorNewton;
        this._uniqueFileId = uniqueFileId;
        //---------------
    }


    public DatenSpeicher (String[] ascii) {
       // optimizerParameterData= new OptimizerParameterData();
        _uniqueFileId = this.importASCII(ascii);

    }

    
    
    public boolean istZeitlizenzOK () { return zeitlizenzOK; }
    
    

    public String exportASCII () {
        //------------------
        // Verbindung -->
        StringBuffer asc= new StringBuffer();
        asc.append("\nverbindungLeistungskreisANZAHL ").append(verbindungANZAHL);
        for (int i1=0;  i1<verbindungANZAHL;  i1++) {
            asc.append("\nverbindungLK ("+i1+")\n");
        //    v[i1].exportASCII(asc);
        }
        asc.append("\nverbindungControlANZAHL ").append(verbindungControlANZAHL);
        for (int i1=0;  i1<verbindungControlANZAHL;  i1++) {
            asc.append("\nverbindungCONTROL ("+i1+")\n");
       //     vCONTROL[i1].exportASCII(asc);
        }
        asc.append("\nverbindungThermANZAHL ").append(verbindungThermANZAHL);

        for (int i1=0;  i1<verbindungThermANZAHL;  i1++) {
            asc.append("\nverbindungTHERM ("+i1+")\n");
        //    vTHERM[i1].exportASCII(asc);
        }
        //------------------
        // ElementLK -->
        asc.append("\nelementANZAHL ").append(elementANZAHL);
        asc.append("\nstaticZaehlerLKelementeINIT[]");
        for (int i1=0;  i1<staticZaehlerLKelementeINIT.length;  i1++) asc.append(" "+staticZaehlerLKelementeINIT[i1]);
        asc.append("\n");
        for (int i1=0;  i1<elementANZAHL;  i1++) {
            asc.append("\ne ("+i1+")\n");
         //   e[i1].exportASCII(asc);
        }
        //------------------
        // ElementCONTROL -->
        asc.append("\ncontrolANZAHL ").append(controlANZAHL);
        asc.append("\nstaticZaehlerReglerBlockINIT[]");
        for (int i1=0;  i1<staticZaehlerReglerBlockINIT.length;  i1++) asc.append(" "+staticZaehlerReglerBlockINIT[i1]);
        asc.append("\n");
        for (int i1=0;  i1<controlANZAHL;  i1++) {
            asc.append("\nc ("+i1+")\n");
         //   c[i1].exportASCII(asc);
        }
        //------------------
        // ElementTHERM-->
        asc.append("\nthermANZAHL ").append(thermANZAHL);
        asc.append("\nstaticZaehlerThermBlockINIT[]");
        for (int i1=0;  i1<staticZaehlerThermBlockINIT.length;  i1++) asc.append(" "+staticZaehlerThermBlockINIT[i1]);
        asc.append("\n");
        for (int i1=0;  i1<thermANZAHL;  i1++) {
            asc.append("\neTH ("+i1+")\n");
        //    eTH[i1].exportASCII(asc);
        }
        //------------------
       // DatenSpeicher.appendAsString(asc.append("\noptimizerName"), optimizerParameterData.getNameOpt());
       // DatenSpeicher.appendAsString(asc.append("\noptimizerValue"), optimizerParameterData.getValueOpt());
//        geckoOpt.exportASCII(asc); 
        asc.append("\n\n"); 
        //------------------
        // aktuelles Datum: 
        asc.append("\nDtStor ").append(dFormat.format(new Date()));
        //------------------
        // Simulations-Einstellungen:
        asc.append("\ntDURATION ").append(tDURATION);     
      //  asc.append("\nbl ").append(LicenseManager.getInstance().getDateOfExpiration().getTime());
        asc.append("\ndt ").append(dt);
        asc.append("\ntPAUSE ").append(tPAUSE);
        asc.append("\nTss ").append(Tss);
        asc.append("\nT_pre ").append(_T_pre);
        asc.append("\ndt_pre ").append(_dt_pre);

        asc.append("\nmaxIterationsNewton ").append(maxIterationsNewton);
        asc.append("\nmaxErrorNewton ").append(maxErrorNewton);
        asc.append("\npath ").append(Typ.DATNAM);
        //
        asc.append("\n\ndpix ").append(dpix);
        asc.append("\nfontSize ").append(fontSize);
        asc.append("\nfontTyp ").append(fontTyp);
        asc.append("\nfensterWidth ").append(fensterWidth);
        asc.append("\nfensterHeight ").append(fensterHeight);
        asc.append("\nworksheetSize ").append(worksheetSize);
        // 
     /*   asc.append("\nANSICHT_SHOW_LK_NAME ").append(SchematischeEingabe2.ANSICHT_SHOW_LK_NAME);
        asc.append("\nANSICHT_SHOW_LK_PARAMETER ").append(SchematischeEingabe2.ANSICHT_SHOW_LK_PARAMETER);
        asc.append("\nANSICHT_SHOW_LK_FLOWDIR ").append(SchematischeEingabe2.ANSICHT_SHOW_LK_FLOWDIR);
        asc.append("\nANSICHT_SHOW_LK_TEXTLINIE ").append(SchematischeEingabe2.ANSICHT_SHOW_LK_TEXTLINIE);
        asc.append("\nANSICHT_SHOW_THERM_NAME ").append(SchematischeEingabe2.ANSICHT_SHOW_THERM_NAME);
        asc.append("\nANSICHT_SHOW_THERM_PARAMETER ").append(SchematischeEingabe2.ANSICHT_SHOW_THERM_PARAMETER);
        asc.append("\nANSICHT_SHOW_THERM_FLOWDIR ").append(SchematischeEingabe2.ANSICHT_SHOW_THERM_FLOWDIR);
        asc.append("\nANSICHT_SHOW_THERM_TEXTLINIE ").append(SchematischeEingabe2.ANSICHT_SHOW_THERM_TEXTLINIE);
        asc.append("\nANSICHT_SHOW_CONTROL_NAME ").append(SchematischeEingabe2.ANSICHT_SHOW_CONTROL_NAME);
        asc.append("\nANSICHT_SHOW_CONTROL_PARAMETER ").append(SchematischeEingabe2.ANSICHT_SHOW_CONTROL_PARAMETER);
        asc.append("\nANSICHT_SHOW_CONTROL_TEXTLINIE ").append(SchematischeEingabe2.ANSICHT_SHOW_CONTROL_TEXTLINIE);
        asc.append("\nFileVersion "  + Typ.RELEASENUMBER);*/
        asc.append("\nUniqueFileId "  + _uniqueFileId);
        //
        asc.append("\n=======================\n ");
        //------------------
        return asc.toString();
    }




    private int importASCII (String[] ascii) {

        Random generator = new Random(System.currentTimeMillis());
        int uniqueFileId = generator.nextInt();

        // pro Textzeile in der ASCII-Datei gibt es einen String
        //------------------
        String zz= "";
        StringTokenizer stk= null;
        for (int i1=0;  i1<ascii.length;  i1++) {
            //System.out.println(i1+"   "+ascii[i1]); 
            if (ascii[i1].startsWith("verbindungLeistungskreisANZAHL "))
                verbindungANZAHL= (new Integer(ascii[i1].substring("verbindungLeistungskreisANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("verbindungControlANZAHL "))
                verbindungControlANZAHL= (new Integer(ascii[i1].substring("verbindungControlANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("verbindungThermANZAHL "))
                verbindungThermANZAHL= (new Integer(ascii[i1].substring("verbindungThermANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("elementANZAHL "))
                elementANZAHL= (new Integer(ascii[i1].substring("elementANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("staticZaehlerLKelementeINIT[] ")) {
                zz= ascii[i1].substring("staticZaehlerLKelementeINIT[]".length()).trim();
                stk= new StringTokenizer(zz, " ");
                staticZaehlerLKelementeINIT= new int[stk.countTokens()];
                for (int i2=0;  i2<staticZaehlerLKelementeINIT.length;  i2++)  staticZaehlerLKelementeINIT[i2]= (new Integer(stk.nextToken())).intValue();
            }
            if (ascii[i1].startsWith("controlANZAHL "))
                controlANZAHL= (new Integer(ascii[i1].substring("controlANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("staticZaehlerReglerBlockINIT[] ")) {
                zz= ascii[i1].substring("staticZaehlerReglerBlockINIT[]".length()).trim();
                stk= new StringTokenizer(zz, " ");
                staticZaehlerReglerBlockINIT= new int[stk.countTokens()];
                for (int i2=0;  i2<staticZaehlerReglerBlockINIT.length;  i2++)  staticZaehlerReglerBlockINIT[i2]= (new Integer(stk.nextToken())).intValue();
            }
            if (ascii[i1].startsWith("thermANZAHL "))
                thermANZAHL= (new Integer(ascii[i1].substring("thermANZAHL".length()).trim())).intValue();
            if (ascii[i1].startsWith("staticZaehlerThermBlockINIT[] ")) {
                zz= ascii[i1].substring("staticZaehlerThermBlockINIT[]".length()).trim();
                stk= new StringTokenizer(zz, " ");
                staticZaehlerThermBlockINIT= new int[stk.countTokens()];
                for (int i2=0;  i2<staticZaehlerThermBlockINIT.length;  i2++)  staticZaehlerThermBlockINIT[i2]= (new Integer(stk.nextToken())).intValue();
            }

//        DatenSpeicher.appendAsString(asc.append("\noptimizerName"), optimizerParameterData.getNameOpt());
//        DatenSpeicher.appendAsString(asc.append("\noptimizerValue"), optimizerParameterData.getValueOpt());
//            if (ascii[i1].startsWith("optimizerName[] ")) { optimizerParameterData.setNameOpt(DatenSpeicher.leseASCII_StringArray1(ascii[i1])); }
//            if (ascii[i1].startsWith("optimizerValue[] ")) { optimizerParameterData.setValueOpt(DatenSpeicher.leseASCII_doubleArray1(ascii[i1])); }
            //
            if (ascii[i1].startsWith("DtStor ")) {
                String datGesp= DatenSpeicher.leseASCII_String(ascii[i1]); 
                Date datumGespeichert= null; 
                try { 
                    datumGespeichert= dFormat.parse(datGesp); 
                    long zeitZweiTageVorher= datumGespeichert.getTime() -(3600*1000*24*2); 
                    Date datumZweiTageVorher= new Date(zeitZweiTageVorher); 
                    Date jetzt= new Date(); 
                    if (jetzt.before(datumZweiTageVorher)) zeitlizenzOK= false;  // Fehler bei der Lizenz! 
                    //System.out.println("datumGespeichert= "+datumGespeichert+"\tdatumZweiTageVorher= "+datumZweiTageVorher+"\tjetzt= "+jetzt); 
                } catch (ParseException pe) {} 
            }

            if(ascii[i1].startsWith("bl")) {
                exp = DatenSpeicher.leseASCII_long(ascii[i1]);
            }
            if (ascii[i1].startsWith("tDURATION "))
                tDURATION= DatenSpeicher.leseASCII_double(ascii[i1]);
            if (ascii[i1].startsWith("dt ")) {                
                dt= DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("dt_pre ")) {
                _dt_pre = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("T_pre ")) {
                _T_pre = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            
            if (ascii[i1].startsWith("tPAUSE ")) {
                tPAUSE= DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("Tss ")) {
                Tss= DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("maxIterationsNewton ")) {
                maxIterationsNewton= DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("maxErrorNewton ")) {
                maxErrorNewton= DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("path ")) {
                Typ.datnamAbsLoadIPES= ascii[i1].substring((new String("path ").length()));  // wichtig, weil Pfadname Leerzeichen enthalten kann
            }
            //
            if (ascii[i1].startsWith("dpix "))
                dpix= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("fontSize "))
                fontSize= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("fontTyp ")) {
                fontTyp= ascii[i1].substring((new String("fontTyp ")).length());  // wichtig, falls FontName Leerzeichen enthaelt!
            }
            if (ascii[i1].startsWith("fensterWidth "))
                fensterWidth= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("fensterHeight "))
                fensterHeight= DatenSpeicher.leseASCII_int(ascii[i1]);
            if (ascii[i1].startsWith("worksheetSize "))
                worksheetSize= DatenSpeicher.leseASCII_String(ascii[i1]);
            // 
/*            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_NAME "))
                SchematischeEingabe2.ANSICHT_SHOW_LK_NAME= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_PARAMETER "))
                SchematischeEingabe2.ANSICHT_SHOW_LK_PARAMETER= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_FLOWDIR "))
                SchematischeEingabe2.ANSICHT_SHOW_LK_FLOWDIR= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_LK_TEXTLINIE "))
                SchematischeEingabe2.ANSICHT_SHOW_LK_TEXTLINIE= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_NAME "))
                SchematischeEingabe2.ANSICHT_SHOW_THERM_NAME= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_PARAMETER "))
                SchematischeEingabe2.ANSICHT_SHOW_THERM_PARAMETER= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_FLOWDIR "))
                SchematischeEingabe2.ANSICHT_SHOW_THERM_FLOWDIR= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_THERM_TEXTLINIE "))
                SchematischeEingabe2.ANSICHT_SHOW_THERM_TEXTLINIE= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_NAME "))
                SchematischeEingabe2.ANSICHT_SHOW_CONTROL_NAME= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_PARAMETER "))
                SchematischeEingabe2.ANSICHT_SHOW_CONTROL_PARAMETER= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if (ascii[i1].startsWith("ANSICHT_SHOW_CONTROL_TEXTLINIE "))
                SchematischeEingabe2.ANSICHT_SHOW_CONTROL_TEXTLINIE= DatenSpeicher.leseASCII_boolean(ascii[i1]);
            if(ascii[i1].startsWith("FileVersion")) {
                int readFileVersion = DatenSpeicher.leseASCII_int(ascii[i1]);
                //System.out.println("read file version: " + readFileVersion);
                if(readFileVersion > Typ.RELEASENUMBER) {
                    JOptionPane.showMessageDialog(null,
                    "This Model file was created with a newer version of GeckoCIRCUITS.\n"
                    + "Please consider to update your Software to the newest version.\n"
                            + "You can find update information in the menu Help -> Updates.",
                    "Info",
                    JOptionPane.WARNING_MESSAGE);
                }
            }*/

            if(ascii[i1].startsWith("UniqueFileId")) {
                uniqueFileId = DatenSpeicher.leseASCII_int(ascii[i1]);
            }

        }

        if(_dt_pre <= 0) {
            _dt_pre = dt;
        }
        //------------------
        //v= new Verbindung[Typ.MAX_ANZAHL_VERBINDUNGEN];
        //vCONTROL= new Verbindung[Typ.MAX_ANZAHL_VERBINDUNGEN];
        //vTHERM= new Verbindung[Typ.MAX_ANZAHL_VERBINDUNGEN];
        //------------------
        //e= new ElementLK[Typ.MAX_ANZAHL_ELEMENTE];
        //c= new ElementCONTROL[Typ.MAX_ANZAHL_ELEMENTE];
        //eTH= new ElementTHERM[Typ.MAX_ANZAHL_ELEMENTE];
        //------------------
        //
        int zeilenZeiger=0, vZaehler=0, vCONTROLZaehler=0, vTHERMZaehler=0, eZaehler=0, cZaehler=0, eTHZaehler=0;
        String txt= "";
        //
        while (zeilenZeiger<ascii.length) {
            //------------------------
            if (ascii[zeilenZeiger].startsWith("verbindungLK ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\Verbindung>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
               // v[vZaehler]= new Verbindung(txt);
                vZaehler++;
            }
            if (ascii[zeilenZeiger].startsWith("verbindungCONTROL ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\Verbindung>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
               // vCONTROL[vCONTROLZaehler]= new Verbindung(txt);
                vCONTROLZaehler++;
            }
            if (ascii[zeilenZeiger].startsWith("verbindungTHERM ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\Verbindung>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
              //  vTHERM[vTHERMZaehler]= new Verbindung(txt);
                vTHERMZaehler++;
            }
            //------------------------
            if (ascii[zeilenZeiger].startsWith("e ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\ElementLK>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
             //   e[eZaehler]= new ElementLK(txt);
                eZaehler++;
            }
            //------------------------
            if (ascii[zeilenZeiger].startsWith("c ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\ElementCONTROL>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
              //  c[cZaehler]= new ElementCONTROL(txt);
                cZaehler++;
            }
            //------------------------
            if (ascii[zeilenZeiger].startsWith("eTH ")) {
                zeilenZeiger += 2;
                txt= "";
                while (! ascii[zeilenZeiger].startsWith("<\\ElementTHERM>")) {
                    txt += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
             //   eTH[eTHZaehler]= new ElementTHERM(txt);
                eTHZaehler++;
            }
            //------------------------
            /*
            if (ascii[zeilenZeiger].startsWith("<GeckoOPTIMIZER>")) { 
                zeilenZeiger += 1;
                geckoOpt_code_ascii= "";
                while (! ascii[zeilenZeiger].startsWith("<\\GeckoOPTIMIZER>")) {
                    geckoOpt_code_ascii += "\n"+ascii[zeilenZeiger];
                    zeilenZeiger++;
                }
            }
            */
            //------------------------
            zeilenZeiger++;
        }
        //------------------
        return uniqueFileId;
}




    // ************************************************************************************
    // Hilfsfunktionen:

    // Daten werden in einen ASCII-String umgeschrieben -->


    public static void appendAsString (StringBuffer ascii, int wert) { ascii.append(" "+wert); }
    public static void appendAsString (StringBuffer ascii, double wert) { ascii.append(" "+wert); }
    public static void appendAsString (StringBuffer ascii, boolean wert) { ascii.append(" "+wert); }
    public static void appendAsString (StringBuffer ascii, String wert) {
        if (wert.equals("")) ascii.append(" "+Typ.NIX); else ascii.append(" "+wert);
    }
    public static void appendAsString (StringBuffer ascii, int[] wert) {
        ascii.append("[] ");
        if (wert==null) ascii.append("null"); else for (int i1=0;  i1<wert.length;  i1++) ascii.append(wert[i1]+" ");
    }
    public static void appendAsString (StringBuffer ascii, double[] wert) {
        ascii.append("[] ");
        if (wert==null) ascii.append("null"); else for (int i1=0;  i1<wert.length;  i1++) ascii.append(wert[i1]+" ");
    }
    public static void appendAsString (StringBuffer ascii, boolean[] wert) {
        ascii.append("[] ");
        if (wert==null) ascii.append("null"); else for (int i1=0;  i1<wert.length;  i1++) ascii.append(wert[i1]+" ");
    }
    public static void appendAsString (StringBuffer ascii, String[] wert) {
        ascii.append("[]");
        if (wert==null) ascii.append(" null");
        else {
            ascii.append(" ");
            for (int i1=0;  i1<wert.length;  i1++) ascii.append(Typ.SEPARATOR_ASCII_STRINGARRAY +(wert[i1].trim().equals("")? Typ.NIX: wert[i1]));
        }
    }
    public static void appendAsString (StringBuffer ascii, int[][] wert) {
        ascii.append("[][] "+wert.length+" "+wert[0].length);
        if (wert==null) ascii.append(" null"); else for (int i1=0;  i1<wert.length;  i1++) for (int i2=0;  i2<wert[0].length;  i2++) ascii.append(" "+wert[i1][i2]);
    }
    public static void appendAsString (StringBuffer ascii, double[][] wert) {
        ascii.append("[][] "+wert.length+" "+wert[0].length);
        if (wert==null) ascii.append(" null"); else for (int i1=0;  i1<wert.length;  i1++) for (int i2=0;  i2<wert[0].length;  i2++) ascii.append(" "+wert[i1][i2]);
    }
    public static void appendAsString (StringBuffer ascii, boolean[][] wert) {
        ascii.append("[][] "+wert.length+" "+wert[0].length);
        if (wert==null) ascii.append(" null"); else for (int i1=0;  i1<wert.length;  i1++) for (int i2=0;  i2<wert[0].length;  i2++) ascii.append(" "+wert[i1][i2]);
    }

    public static void appendAsTextBlock (StringBuffer ascii, final String wert) {
        if (wert.equals("")) {
            ascii.append(" " + Typ.NIX);
            return;
        }
        else {
            String singleLine = wert.replaceAll("\n","\\\\n");
            ascii.append(" " + singleLine);
        }
    }

    /**
     * reads a textblock, including spaces. In contradiction, LeseAsciiString would
     * return only the first token! If a \n character appears, a newline is done.
     * @param ascii
     * @return
     */
    public static String leseASCII_TextBlock(String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        String identifier = stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        String wert= ascii;
        if(stk.hasMoreElements()) {
            // remove first token, the rest is the String to read in.
            wert = ascii.substring(identifier.length()+1);
            wert = wert.replaceAll("\\\\n", "\n");
        }
        if (wert.equals(Typ.NIX)) wert="";
        return wert;
    }

    // aus dem ASCII-String werden die Daten zurueckgelesen -->

    public static boolean leseASCII_boolean (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Boolean(stk.nextToken()).booleanValue());
    }
    public static int leseASCII_int (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Integer(stk.nextToken()).intValue());
    }

    public static long leseASCII_long (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        return (new Long(stk.nextToken()));
    }

    public static double leseASCII_double (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen        
        return (new Double(stk.nextToken()).doubleValue());
    }
    public static String leseASCII_String (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        String wert= stk.nextToken();
        if (wert.equals(Typ.NIX)) wert="";
        return wert;
    }

    public static boolean[] leseASCII_booleanArray1 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        boolean[] wert= new boolean[stk.countTokens()-1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1=0;  i1<wert.length;  i1++)  {
            String zz= stk.nextToken();
            if ((i1==0)&&(zz.equals("null"))) return null;
            wert[i1]= (new Boolean(zz)).booleanValue();
        }
        return wert;
    }
    public static int[] leseASCII_intArray1 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        int[] wert= new int[stk.countTokens()-1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1=0;  i1<wert.length;  i1++)  {
            String zz= stk.nextToken();
            if ((i1==0)&&(zz.equals("null"))) return null;
            wert[i1]= (new Integer(zz)).intValue();
        }
        return wert;
    }
    public static double[] leseASCII_doubleArray1 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        double[] wert= new double[stk.countTokens()-1];
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        for (int i1=0;  i1<wert.length;  i1++)  {
            String zz= stk.nextToken();
            if ((i1==0)&&(zz.equals("null"))) return null;
            wert[i1]= (new Double(zz)).doubleValue();
        }
        return wert;
    }
    public static String[] leseASCII_StringArray1 (String ascii) {
        int erstesPlenk= ascii.indexOf(" ");
        String asciiDaten= ascii.substring(ascii.indexOf(" "));
        StringTokenizer stk= new StringTokenizer(asciiDaten, Typ.SEPARATOR_ASCII_STRINGARRAY);
        stk.nextToken();  // erster Wert wird uebersprungen
        String[] wert= new String[stk.countTokens()];
        for (int i1=0;  i1<wert.length;  i1++) {
            wert[i1]= stk.nextToken();
            if (wert[i1].equals(Typ.NIX)) wert[i1]="";
        }
        return wert;
    }
    public static double[][] leseASCII_doubleArray2 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l=  (new Integer(stk.nextToken())).intValue();
        int l0= (new Integer(stk.nextToken())).intValue();
        double[][] wert= new double[l][l0];
        for (int i1=0;  i1<l;  i1++)
            for (int i2=0;  i2<l0;  i2++)
                wert[i1][i2]= (new Double(stk.nextToken())).doubleValue();
        return wert;
    }
    public static int[][] leseASCII_intArray2 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l=  (new Integer(stk.nextToken())).intValue();
        int l0= (new Integer(stk.nextToken())).intValue();
        int[][] wert= new int[l][l0];
        for (int i1=0;  i1<l;  i1++)
            for (int i2=0;  i2<l0;  i2++)
                wert[i1][i2]= (new Integer(stk.nextToken())).intValue();
        return wert;
    }
    public static boolean[][] leseASCII_booleanArray2 (String ascii) {
        StringTokenizer stk= new StringTokenizer(ascii, " ");
        stk.nextToken();  // 1.Eintrag ist ID-String --> wird uebersprungen
        int l=  (new Integer(stk.nextToken())).intValue();
        int l0= (new Integer(stk.nextToken())).intValue();
        boolean[][] wert= new boolean[l][l0];
        for (int i1=0;  i1<l;  i1++)
            for (int i2=0;  i2<l0;  i2++)
                wert[i1][i2]= (new Boolean(stk.nextToken())).booleanValue();
        return wert;
    }



    // ************************************************************************************
    // Hilfsfunktionen:

    // Aenderungen in relativen und absoluten Pfaden werden beruecksichtigt -->


    // datnamAbsolutIPES   ... neuer aktueller absoluter pfad+name der *.ipes-Datei, die die Topologie/Schaltungssimulation enthaelt
    // datnamAbsLoadIPES   ... gespeicherter pfad+name der *.ipes-Datei, die die Topologie/Schaltungssimulation enthaelt
    // datnamAbsLoadDETAIL ... gespeicherter pfad+name einer Datei, die Zusatzinfo enhaelt, zB. Halbleiter-Info oder Ersatzmodelle wie TH_MODUL
    // return-String       ... neuer aktueller absoluter pfad+name der Datei mit den Zusatzinfos
    //
    public static String lokalisiereRelativenPfad (String datnamAbsolutIPES, String datnamAbsLoadDETAIL) {
        String datnamAbsLoadIPES= Typ.datnamAbsLoadIPES;
        //-------------------------
        // (1) Ist die Pfadstruktur unveraendert? Kann man den alten (gespeicherten) absoluten Pfad der Zusatzdatei verwenden?
        if ((new File(datnamAbsLoadDETAIL)).exists()) return datnamAbsLoadDETAIL;
        //-------------------------
        // (2) Die alte Datei 'datnamAbsLoadDETAIL' wurde nicht gefunden.
        // Hat sich die Pfadstruktur geaendert und muss der neue Pfad gefunden werden?
        // (a) Durchsuche die Unterverzeichnisses von *.ipes -->
        try {
            String localRootAlt= (new File(datnamAbsLoadIPES)).getParent();
            String localRootNeu= (new File(datnamAbsolutIPES)).getParent();
            String relativerPfadDETAIL= datnamAbsLoadDETAIL.substring(localRootAlt.length());
            String neuerPfadDETAIL= localRootNeu+relativerPfadDETAIL;
            if ((new File(neuerPfadDETAIL)).exists()) return neuerPfadDETAIL;
        } catch (Exception e) {}
        //---------
        // (b) Durchsuche systematisch die Festplatte, ob es eventuell irgendwo die Zusatzinfo-Datei gibt -->
        // ... noch zu implementieren ...
        //-------------------------
        return new String(Typ.DATNAM_NOT_DEFINED);
    }

    public static String[] makeStringArray (String in) {
        //------------------
        int[] zeilenUmbruchZeiger= new int[in.length()];
        int zeilenAnzahl= 0;
        for (int i1=0;  i1<in.length()-1;  i1++)
            if (in.substring(i1).startsWith("\n")) {
                zeilenUmbruchZeiger[zeilenAnzahl]= i1;
                zeilenAnzahl++;
            }
        String[] out= new String[zeilenAnzahl];
        for (int i1=0;  i1<zeilenAnzahl-1;  i1++)
            out[i1]= in.substring(zeilenUmbruchZeiger[i1]+1,zeilenUmbruchZeiger[i1+1]);
        out[zeilenAnzahl-1]= in.substring(zeilenUmbruchZeiger[zeilenAnzahl-1]+1);
        //------------------
        return out;
    }



}





