/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.allg;


import java.io.File;
import java.net.URL;

/**
 * Factor out all Typ path information here.
 * @todo Clean this up -> Merged with Andy's get rid of the variables etc.
 * @todo _unsaved belongs in Style.
 * @todo Maybe we want diffrent stuff for default and current fname?
 * @todo I suspect _undefined should be gotten rid of when time allows.
 */
public class GetJarPath {

    private static boolean  _initialized;
    private static String   _JARpath;
    private static String   _JARFilePath;
    private static  URL     _PICS;
    private static String   _picPath;
    private static String   _defaultFilename;// Pfad und Name der beim letzten Mal geladenen Datei fuer die Schaltungssimulation (*.ipes):
    private static String   _datnamAbsLoadIPES;
    public static final String _undefined = "not_defined";
    public static final String _untitled = "Untitled";
    private static Class _refToCallingPackage;

    //public static final String spTitle = "  ";
    //public static final String spTitleX = "  -  ";

    /**
     * we moved the class to another package - therefore the whole getJarPath-Procedure
     * needs to have a class from the calling package as initialization.
       therefore private constructor.
     * @param c
     */
    public GetJarPath (Class c) {
        _refToCallingPackage = c;
        _initialized = true;
        setJarPath();
        _defaultFilename = _undefined;
    }

    /**
     * \warning have no idea what htis variable should be.
     * @return
     */
    public static String getAbsLoadIPES()
    {
        assert _initialized;
        return _datnamAbsLoadIPES;
    }
    public static String getDefaultFname()
    {
        assert _initialized;

        return _defaultFilename;
    }


    /**
     * This is called from DatenSpeicher.  Anywhere else?
     * @param str
     */
    public static void setAbsLoadIPES(String str)
    {
        assert _initialized;

        _datnamAbsLoadIPES = str;
    }

    public static void setDefaultFname(String str, String extension)
    {
        assert _initialized;
        _defaultFilename = str;
        if (!_defaultFilename.endsWith("."+extension))        {
            _defaultFilename += extension;
        }
    }

    /** this provides and UNCHECKED set methods.  Doesn't check the extension.*/
    public static void setDefaultFname(String str){
        assert _initialized;
        _defaultFilename = str;

    }
    /**
    * This seems to be done in a lot of places:
     *  - is dn null, or "undefined"?
          yes: - if _defaultFilename is set, set dn to _defaultFilename.
     *         - if _defaultFilename is not set, set dn to the Jar path.
     *    no:  leave it unchanged
     * todo I suspect undefined and untititled may be redundant?
     * @param fname
     */
    public static void checkAndSetFname(String fname)
    {
        if ((fname==null)||fname.equals(_undefined)){
            if (_defaultFilename.equals(_undefined)||(_defaultFilename==null)){
                fname = _JARpath;
            }
            else{
                fname = _defaultFilename;
            }
        }
    }

    /** This bit of code seems to occur in a lot of places, so I factor it out here.
     * The commented out try/catch seems unnecessary, but put it back in if it's
     * needed.
     * @return a valid dat name.
     */
    /*
    public static String getDatNam() {
	String retval;
	//try {
	retval = _datName;
	if (retval.equals(DATNAM_NOT_DEFINED)) {
	    retval = JAR;
	}
	return retval;
	//} catch (Exception e) {
	//    dialog.setCurrentDirectory(new File(Paths.JAR));
	// }
    }

*/

    /**
     * i made this function private... it is only used from getJarPath, and returns
     * a mixture between URL and filepath... nobody outside should see and use this here!
     * @return
     */
    private static String getJarPathInsideJAR () {

        String path = _refToCallingPackage.getResource(_refToCallingPackage.getSimpleName() + ".class").toString();
        if (path.startsWith("jar")) {
            path = path.replaceAll("jar:","");
        } else {
            path = path.replaceAll(_refToCallingPackage.getSimpleName() + ".class","");
        }

        // path is coming from an URL - when a space is encountered here, it will
        // be converted to a %20-Character!?!
        path = path.replace("%20", " ");

        return path;
    }



    private void setJarPath() {
        String p = getJarPathInsideJAR();

        // remove preceeding file:-String. Be careful, Linux and Windows require
        // different handling:
        if (p.startsWith("file:")) {
            if(System.getProperty("os.name").toLowerCase().contains("win")) {
                p= p.substring((new String("file:/")).length());
            } else {
                p= p.substring((new String("file:")).length());
            }
        }

        // this indicates that we run the program out of the development IDE,
        // directly from the classes:

        String buildClassesString = "/build/classes";

        if(p.contains(buildClassesString)) {
            int index = p.indexOf(buildClassesString);
            p = p.substring(0, index + buildClassesString.length()+1);
        } else {
            // this program must have been started from a jar-file. Remove
            // everything of the path inside the jar:
            int jarIndex = p.indexOf(".jar!");
            _JARFilePath = p.substring(0, jarIndex + 4);
            p = p.substring(0, jarIndex);
            int lastDelimiterIndex = p.lastIndexOf("/");
            p = p.substring(0, lastDelimiterIndex + 1);
        }


        // finally, make a test if the directory is exisiting. If not write out an
        // error message!
        if(!Typ.IS_APPLET) {
            File testFile = new File(p);
            if(!testFile.isDirectory()) {
                System.err.println("Error: jar-Path is not a directory!");
            }
        }
            _JARpath = p;
    }

    /**
     * If initialized, returns _JARpath.  Otherwise, it determines the correct jarpath.
     * @return path to the DIRECTORY of our own jar-class. If we run the program
     * from the IDE, the corresponding path to the build-directory is returned.
     */
    public static String getJarPath() {
        assert _initialized;
        return _JARpath;
    }


    /**
     * If initialized, returns _JARFilePath - this is the path to the GeckkoCIRCUITS.jar,
     * including the jar-File name. If started from within netbeans, an empty string is
     * returned.
     * @return path to the FILE of our own jar-class. If we run the program
     * from the IDE, an empty string is returned.
     */
    public static String getJarFilePath() {
        assert _initialized;
        return _JARFilePath;
    }


    public URL getPathPICS () {
        URL pfadURL= null;
        try {
            pfadURL= GetJarPath.class.getResource(GetJarPath.class.getSimpleName() + ".class").toURI().toURL();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pfadURL;
    }


    public static String getTextMenuItemRECENT (String txt, int txtSpace) {
        if (txtSpace==-1) return txt;
        try {
            String dat= txt.substring(txt.lastIndexOf(System.getProperty("file.separator"))+1);
            if (dat.length()+5 > txtSpace) return dat;  // nur der Dateiname
            // teilweise wird die Pfadangabe mitgegeben:
            int space= txtSpace -(dat.length()+5);
            String erg= txt.substring(0,space) +" .. "+System.getProperty("file.separator")+" "+dat;
            return erg;
        } catch (Exception e) { return txt; }
    }

}
