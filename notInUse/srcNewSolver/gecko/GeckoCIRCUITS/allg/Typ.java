/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.allg;

import java.awt.Font;
import java.awt.Color;
import java.net.URL;


public class Typ {
    private static Color oldBackground;
    private static Color oldForeGround;
    public static Color colorReluctanceBackground = new Color(210,105,30);
    public static Color colorReluctanceForeGround = new Color(119,49,9);
    public static Color colorConnection;

    public static void setReluctanceColors() {
        oldBackground = Typ.colorBackGroundElement;
        oldForeGround = Typ.colorForeGroundElement;
        Typ.colorBackGroundElement = Typ.colorReluctanceBackground;
        Typ.colorForeGroundElement = Typ.colorReluctanceForeGround;
        Typ.colorConnection = Typ.colorReluctanceForeGround;
        
    }

    public static void resetOldColors() {
        Typ.colorForeGroundElement = oldForeGround;
        Typ.colorBackGroundElement = oldBackground;

    }

    public Typ () {}

    public static boolean IS_BRANDED = false;
    public static final boolean STUDENTENVERSION=  false;
    public static final boolean INCLUDE_GeckoMAGNETICS= false;
    public static final boolean INCLUDE_GeckoHEAT= false;
    public static final boolean INCLUDE_GeckoEMC=  false;
    public static final String  RELEASE_DATE= "2009.10.31";
    public static final String  VERSION= "GeckoCIRCUITS - v1.10";

    public static final boolean IS_CD_DEMO= false;


    public static boolean IS_APPLET= true;  // set at runtime

    public static boolean DO_COMPRESSION = false;

    // wie sollen die Symbole im SchematicEntry dargestellt werden -->
    public static final int VIEW_SYMBOLS_DE= 1;
    public static final int VIEW_SYMBOLS_USA= 2;
    public static int view_symbols= VIEW_SYMBOLS_USA;  // default


    public static final int OPERATINGMODE_STANDALONE= 1;
    public static final int OPERATINGMODE_SIMULINK= 2;
    public static int operatingmode= OPERATINGMODE_STANDALONE;  // default
    public static final double DT_INIT_SIMULINK= 1.2372e-9;


    public static final int LOCATION_BY_PLATFORM= 1;
    public static final int LOCATION_BY_PROGRAM_A= 2;
    public static final int mode_location= LOCATION_BY_PLATFORM;


    public static boolean complier_toolsjar_missing= true;
    public static boolean library_java3D_missing= true;


    //--------------------
    // fuer die Zuordnung von Knoten-Labels zu Ankerpunkten auf Elementen bzw. Verbindungen -->
    public static final int ZUORDNUNG_CIRCUIT= 1;
    public static final int ZUORDNUNG_CONTROL= 2;
    public static final int ZUORDNUNG_THERM= 3;
    public static final int ZUORDNUNG_KOMMENTAR= 4;



    // wann wurde das Programm zum letzten Mal geschlossen (Date-Format YYYY.MM.DD) -->
    // zur Ueberpruefung der Zeitlizenzierung ...
    public static String LAST_SAVING;

    // Spracheinstellung; default: Englisch -->
    public static String LANGUAGE= "English";

    // die zuletzt geoeffneten Dateien, RECENT_1 ist der juengste Eintrag
    // die Pfade werden in den Properties gespeichert, und beim Programmstart geladen -->
    public static String RECENT_CIRCUITS_1="", RECENT_CIRCUITS_2="", RECENT_CIRCUITS_3="", RECENT_CIRCUITS_4="";
    public static String RECENT_HEAT_1="", RECENT_HEAT_2="", RECENT_HEAT_3="", RECENT_HEAT_4="";


    // zur Codierung der Datumsangabe im Property-File -->
    public static final int CODE_1=45, CODE_2=92;

    //------------------------
    // Pfad fuer die Ablage aller verwendeten Bilder:
    public static URL PFAD_PICS_URL;  // gleich wie 'PFAD_PICS'
    // Pfad, in dem das aktuelle JAR-File liegt -->    public static String RECENT_HEAT_1="", RECENT_HEAT_2="", RECENT_HEAT_3="", RECENT_HEAT_4="";



    //------------------------
    // Pfad, in dem das aktuelle JAR-File liegt -->
    public static String PFAD_JAR_HOME;


    //------------------------
    // Pfad fuer die Ablage aller verwendeten Bilder:
    //------------------------
    // Pfad und Name der aktuellen Datei fuer die Schaltungssimulation (*.ipes):
    public static String DATNAM;
    // Pfad und Name der beim letzten Mal geladenen Datei fuer die Schaltungssimulation (*.ipes):
    // --> ist wichtig, wenn die Pfadstruktur geaendert wurde --> damit werden lokale Pfade aktualisiert, siehe DatenSpeicher.lokalisiereRelativenPfad()
    public static String DATNAM_NOT_DEFINED= "not_defined";
    public static String datnamAbsLoadIPES;

    // Pixelkoordinaten des SchematicEntry-Fensters bezogen auf den PC-Schirm (Koordinaten jeweils Ecke links oben)
    public static int X_SCREEN,  Y_SCREEN;
    public static int DX_DIALOG, DY_DIALOG;  // Dialogkoordinaten relativ zum Schematic-Entry-Fenster    public static int DX_DIALOG, DY_DIALOG;  // Dialogkoordinaten relativ zum Schematic-Entry-Fenster
    public static int DX_SCOPE, DY_SCOPE;  // relative SCOPE-Position bezogen auf SchematicEntry-Fenster beim Initialisieren
    public static int DX_SCOPE_DIALOG, DY_SCOPE_DIALOG;  // relative Koord. der zu SCOPE gehoerigen Dialoge (bezogen auf individuelle SCOPEs)
    public static int DX_SCOPE_DLG2, DY_SCOPE_DLG2;  // relative Koord. der zu SCOPE gehoerigen INTERNEN Dialoge (bezogen auf das Fenster 'DialogGraferProperties')
    public static int X_3DFEM, Y_3DFEM;  // absolute Koordinaten am Screen des 3D-FEM-Fensters
    public static int DX_3DFEM_DIALOG, DY_3DFEM_DIALOG;  // relative Koord. der zu 3D-FEM gehoerigen Dialoge
    public static int DX_POST_DIALOG, DY_POST_DIALOG;  // relative Koord. der zu 3D-FEM gehoerigen Postprozessor-Dialoge



    // NULL-Symbol ("") fuer Labels --> notwendig, weil " " als Separator bei der ASCII-Speicherung genutzt wird, und "" schwer zum Wiederherstellen ist
    public static final String NIX= "NIX_NIX_NIX";
    public static final String SEPARATOR_ASCII_STRINGARRAY= "/";

    //------------------------
    // Header-Leiste der Fenster (Titel): Abstand vom Text zum Icon -->
    public static final String spTitle= "  ";
    public static final String spTitleX= "  -  ";
    public static final String UNTITLED= "Untitled";  // wird in der Fenster-Leiste angezeigt, wenn eine neue Datei gestartet wird
    public static final String NOCH_NICHT_GESPEICHERT= "*";  // wird in Windows an den Datei-Namen drangehaengt, wenn eine Modifikation noch nicht gespeichert wurde

    //------------------------
    // Status allg. von Elementen und Verbindungen im SchematicEntry:
    public static final int IN_BEARBEITUNG= 0;
    public static final int FERTIG=         1;
    public static final int GELOESCHT=      2;

    //------------------------------------------------------------------------
    // verfuegbare LK-Elemente
    public static final int LK_ERROR= -3;  // wenn fehlerhafte Elemente geladen werden, werden sie durch dieses Minimal-Element ersetzt
    //
    public static final int LK_R= 1;
    public static final int LK_L= 2;
    public static final int LK_C= 3;
    public static final int LK_U= 4;
    public static final int LK_I= 5;
    public static final int LK_D= 6;
    public static final int LK_S= 7;
    public static final int LK_THYR=   8;
    public static final int LK_M=      9;
    public static final int LK_IGBT=  10;
    public static final int LK_LKOP=  11;  // Induktivitaet mit serieller Spannungsquelle zur magnetischen Kopplung
    public static final int LK_LKOP2= 12;  // bei Verwendung dieser Induktivitaet wird die Matrixgleichung um die Induktivitaetsstroeme erweitert --> bessere numerische Stabilitaet

    public static final int LK_LISN=  13;
    public static final int LK_MOTOR= 14;  // DC-Motor, erste Motor-Implementierung
    public static final int LK_MOTOR_PMSM= 15;
    public static final int LK_MOTOR_SMSALIENT= 16;
    public static final int LK_MOTOR_SMROUND= 17;
    public static final int LK_MOTOR_IMA= 18;
    public static final int LK_MOTOR_IMB= 19;
    public static final int LK_MOTOR_IMC= 20;
    public static final int LK_MOTOR_IMSAT= 21;
    //
    public static final int LK_OPV1= 22;

    public static final int LK_GEMC = 23;
    //
    //------------------------------------------------------------------------

    public static final int QUELLE_SIGNALGESTEUERT= 400;
    public static final int QUELLE_DC=       401;
    public static final int QUELLE_SIN=      402;
    public static final int QUELLE_DREIECK=  403;
    public static final int QUELLE_RECHTECK= 404;
    public static final int QUELLE_RANDOM=   405;
    public static final int QUELLE_IMPORT=   406;
    public static final int QUELLE_VOLTAGECONTROLLED_DIRECTLY= 399;

    
    // Orientierung der LK-Elemente
    public static final int SN=501;
    public static final int WO=502;
    public static final int NS=503;
    public static final int OW=504;

    //------------------------------------------------------------------------
    // verfuegbare CONTROL-Elemente
    public static final int C_VOLTMETER= 1;
    public static final int C_AMPMETER= 2;
    public static final int C_CONST= 3;
    public static final int C_SIGNALSOURCE= 4;
    public static final int C_SCOPE= 5;
    public static final int C_SWITCH= 6;
    public static final int C_GAIN= 7;
    public static final int C_PT1= 8;
    public static final int C_PT2= 9;
    public static final int C_PI= 10;
    public static final int C_HYS= 11;
    public static final int C_ADD= 12;
    public static final int C_SUB= 13;
    public static final int C_MUL= 14;
    public static final int C_DIV= 15;
    public static final int C_TEMP= 16;
    public static final int C_FLOW= 17;
    public static final int C_NOT= 18;
    public static final int C_AND= 19;
    public static final int C_OR=  20;
    public static final int C_XOR= 21;
    public static final int C_TO_EXTERNAL= 22;
    public static final int C_FROM_EXTERNAL= 23;
    public static final int C_DIGSOURCE= 24;
    public static final int C_DELAY= 25;
    public static final int C_SAMPLEHOLD= 26;
    public static final int C_LIMIT= 27;
    public static final int C_PD= 29;
    public static final int C_ABS= 32;
    public static final int C_ROUND= 33;
    public static final int C_SIN= 34;
    public static final int C_ASIN= 35;
    public static final int C_COS= 36;
    public static final int C_ACOS= 37;
    public static final int C_TAN= 38;
    public static final int C_ATAN= 39;
    public static final int C_EXP= 40;
    public static final int C_LN= 41;
    public static final int C_SQR= 42;
    public static final int C_SQRT= 43;
    public static final int C_POW= 44;
    public static final int C_GE= 45;
    public static final int C_GT= 46;
    public static final int C_EQ= 47;
    public static final int C_NE= 48;
    public static final int C_MIN= 49;
    public static final int C_MAX= 50;
    public static final int C_SIGN= 51;
    public static final int C_PWM= 52;
    public static final int C_COUNTER= 53;
    public static final int C_LOGIK= 54;
    public static final int C_SETSTATE= 55;
    public static final int C_STATEVAR= 56;
    public static final int C_IFTHEN= 57;
    public static final int C_TIME= 58;
    public static final int C_SPARSEMATRIX= 59;
    public static final int C_CISPR16= 60;
    public static final int C_JAVA_FUNCTION= 61;
    public static final int C_VIEWMOT= 62;
    public static final int C_SPACE_VECTOR= 63;
    public static final int C_INT= 64;
    public static final int C_ABCDQ= 65;
    public static final int C_DQABC= 66;
    public static final int S_TEXTFIELD = 70;
    public static final int C_SCOPE2= 71;
    public static final int C_ThyrControl = 72;

    //
    //-------------
    
    //
    //------------------------------------------------------------------------

    public static final int ANZAHL_TERMINALS_MAX= 50;  // maximal moegliche Terminal-Anzahl


    //------------------------------------------------------------------------
    // Verfuegbare THERM-Elemente:
    //
    public static final int TH_ERROR= -3;
    //
    public static final int TH_PvCHIP= 41;
    public static final int TH_MODUL= 42;
    public static final int TH_KUEHLER= 43;
    public static final int TH_FLOW= 44;
    public static final int TH_TEMP= 45;
    public static final int TH_RTH= 46;
    public static final int TH_CTH= 47;
    public static final int TH_AMBIENT= 48;
    //
    // die 'hinter' einem MODUL (bzw. PvCHIP) generierten TEMP- und FLOW-Elemente werden mit einem vom
    // SchematicEntry aus unerreichbaren Punkt auf einen Bezugpunkt (Potential 'Null') gelegt -->
    public static final int TH_NULLBEZUG_KNOTEN_X= -4711;
    public static final int TH_NULLBEZUG_KNOTEN_Y= -4711;
    //
    public static final String TH_LABEL_NULLBEZUG= "Tzero";
    public static final double RTH_HOCHOHMIG= 1e15;  // zB. Innenwiderstand parallel zu einer FLOW-Quelle
    //------------------------------------------------------------------------


    //------------------------------------------------------------------------
    public static Color farbeGecko= Color.decode("0x99bb33");
    // 228b22 32cd32 808000 9acd32


    public static Color farbeTextLinie= Color.lightGray;  // Verbindungslinie vom Textfeld zum zugehoerigen Element

    public static Color farbeInBearbeitungLK= Color.gray;
    public static Color farbeFertigElementLK= Color.decode("0x00008b");
    public static Color farbeFertigVerbindungLK= Color.blue;
    public static Color farbeLabelLK= Color.decode("0x00008b");
    public static Color farbeParallelLK= Color.decode("0xadd8e6");  // falls eine parallele Linie in Verbindung.zeichne() gezogen wird zur besseren Visualisierung
    public static Color farbeElementLKHintergrund= Color.decode("0xccccff");

    public static Color colorForeGroundElement = Color.decode("0x00008b");
    public static Color colorBackGroundElement = Color.decode("0xccccff");

    public static Color farbeInBearbeitungCONTROL= Color.gray;
    public static Color farbeFertigElementCONTROL= Color.decode("0x006400");
    public static Color farbeFertigVerbindungCONTROL= Color.green;
    public static Color farbeLabelCONTROL= Color.decode("0x006400");
    public static Color farbeParallelCONTROL= Color.decode("0x90ee90");
    public static Color farbeElementCONTROLHintergrund= Color.decode("0xaaffaa");

    public static Color farbeEXTERNAL_TERMINAL= Color.magenta;

    public static Color farbeInBearbeitungTHERM= Color.gray;
    public static Color farbeFertigElementTHERM= Color.decode("0x8b0000");
    public static Color farbeFertigVerbindungTHERM= Color.red;
    public static Color farbeLabelTHERM= Color.red;
    public static Color farbeParallelTHERM= Color.decode("0xffa07a");
    public static Color farbeElementTHERMHintergrund= Color.decode("0xffd7d7");


    public static Color farbeZoomRechteck= Color.red;
    public static Color farbeOPT= Color.magenta;



    public static Color farbeConnectorTestMode= Color.magenta;
    public static Color farbeConnectorTestModeInternal= Color.yellow;

    public static  int RAD_CTM= 7;  // radius of the oval node-marker in connectorTestMode
    //------------------------------------------------------------------------


    public static final int MAX_ANZAHL_ELEMENTE= 999;
    public static final int MAX_ANZAHL_VERBINDUNGEN= 999;
    public static final int MAX_ANZAHL_PKT_PRO_VERBINDUNG= 999;

    public static final int LABEL_ANFANGSKNOTEN= 1;
    public static final int LABEL_ENDKNOTEN= 2;
    public static final int LABEL_VERBINDUNG= 3;

    // Auswahl der Sub-Kreise im SchematicEntry
    public static final int TYP_POWERCIRCUIT= 0;
    public static final int TYP_CONTROL= 1;
    public static final int TYP_THERMAL= 3;

    public static Font foAUSWAHL= new Font("Arial",Font.PLAIN,12);
    public static Font foAUSWAHLueberschrift= new Font("Arial",Font.BOLD,12);



    //------------------------------------------------------------------------
    // einheitlicher Font fuer die Dialoge -->
    //
    public static Font  LAB_FONT_DIALOG_1= new Font("Arial",Font.PLAIN,12);
    public static Color LAB_COLOR_DIALOG_1= Color.black;
    //
    public static Font FORMEL_DIALOG_GROSS= new Font("Times New Roman", Font.ITALIC, 16);
    public static Font FORMEL_DIALOG_KLEIN= new Font("Times New Roman", Font.ITALIC, 12);
    //------------------------------------------------------------------------




}



