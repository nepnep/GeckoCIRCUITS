/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.allg;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import java.text.NumberFormat; 



public class FormatJTextField extends JTextField {


    //-----------------------
    public static final double IS_VARIABLE= -1e95;  // GeckoOPTIMIZER
    //-----------------------
    private boolean numberOK;
    private String techFormatPattern= TechFormat.FORMAT_AUTO;  // default
    private TechFormat cf= new TechFormat();
    private NumberFormat nf= NumberFormat.getNumberInstance(); 
    //-----------------------
    


    public FormatJTextField () { super(); }
    public FormatJTextField (String s) { super(s); }

    public FormatJTextField (double x) {
        this.setNumberToField(x);
    }

    public FormatJTextField (double x, int maximumFractionDigits) {
        this.setNumberToField(x,maximumFractionDigits);
    }

    public FormatJTextField (double x, String pattern) {
        this.techFormatPattern= pattern;
        this.setNumberToField(x);
    }

    public void setTechFormatPattern (String pattern) { this.techFormatPattern=pattern; }

    public void setMaximumDigits (int anzDigits) { cf.setMaximumDigits(anzDigits); }

    public boolean isNumberOK () { return numberOK; }
    
    public void setState (boolean on) {
        this.setEditable(on); 
        this.setEnabled(on);
    }


    public void setNumberToField (double x) {
        String s= cf.formatT(x,techFormatPattern);
        this.setText(s);
    }

    
    public void setNumberToField (double x, int maximumFractionDigits) {
        nf.setMaximumFractionDigits(maximumFractionDigits); 
        String s= nf.format(x);
        this.setText(s);
    }
    

    public double getNumberFromField () {
        String z= this.getText(); 
        if (z.startsWith("$")) return FormatJTextField.IS_VARIABLE;  // GeckoOPTIMIZER
        try {
            double x= cf.parseT(z);
            numberOK= true;
            this.setForeground(Color.black);
            return x;
        } catch (Exception e) {
        try {
            int i2=0; 
            for (int i1=0;  i1<z.length();  i1++) if (z.charAt(i1)!='\'') i2++; 
            char[] zc= new char[i2]; 
            i2=0; 
            for (int i1=0;  i1<z.length();  i1++) if (z.charAt(i1)!='\'') { zc[i2]=z.charAt(i1);  i2++; }
            z= new String(zc); 
            //----
            double x= cf.parseT(z);
            numberOK= true;
            this.setForeground(Color.black);
            return x;
        } catch (Exception e2) {
            numberOK= false;
            this.setForeground(Color.red);
            throw new NumberFormatException("Ungueltiges Zahlenformat in 'FormatJTextField'");
        }}
    }



}




