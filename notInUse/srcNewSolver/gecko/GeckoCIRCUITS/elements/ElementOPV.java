/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.JPanelDCMachine;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 *
 * @author andy
 */
public class ElementOPV extends ElementSubSheet {
    
    public ElementOPV(CircuitSheet sheet) {
        super(sheet);

        _sheet._sheetSizeX.setValue(40);
        _sheet._sheetSizeY.setValue(30);

        _sheet.paintExtensionX = 100;

        JPanelDCMachine dialog = new JPanelDCMachine();

        int dialogHeight = dialog.getPreferredSize().height;
        _sheet.paintExtensionY = dialogHeight;
        int dialogWidth = dialog.getPreferredSize().width;
        dialog.setBounds(0,_sheet._sheetSizeY.getValue() * _sheet._dpix.getValue(),
                dialogWidth,dialogHeight);
        //dialog.setOpaque(false);
        _sheet.add(dialog);


        ElementTerminalCircuit minusTerm = new ElementTerminalCircuit(this, "minus", 6, 22, -1, -2, _sheet);
        ElementTerminalCircuit plusTerm = new ElementTerminalCircuit(this, "plus", 6, 18, 1, -2, _sheet);
        ElementTerminalCircuit outTerm = new ElementTerminalCircuit(this, "out", 32, 18, 0, 3, _sheet);
        outTerm.setOrientation(Orientation.NORTH);

        ElementTerminalCircuit gndTerm = new ElementTerminalCircuit(this, "gnd", 32, 22,-2,1, _sheet);
        gndTerm.setOrientation(Orientation.NORTH);



        ElementResistor rIN = new ElementResistor(_sheet);
        rIN._elementName.setValue("Rin");
        rIN.resistance.setValue(90e3);
        rIN.x.setValue(8);
        rIN.y.setValue(20);
        _sheet.add(rIN);

        ElementVoltage uX = new ElementVoltage(rIN._terminals.get(0), rIN._terminals.get(1), _sheet, 10e3);
        uX.x.setValue(12);
        uX.y.setValue(20);
        _sheet.add(uX);

        ElementResistor rB = new ElementResistor(_sheet);
        rB.resistance.setValue(100.0);
        rB.x.setValue(16);
        rB.y.setValue(20);
        _sheet.add(rB);

        ElementDiode d1 = new ElementDiode(_sheet);
        d1.uForward.setValue(5.0);
        d1.onResistance.setValue(100e-3);
        d1.x.setValue(20);
        d1.y.setValue(20);
        _sheet.add(d1);

        ElementDiode d2 = new ElementDiode(_sheet);
        d2.uForward.setValue(6.0);
        d2.onResistance.setValue(100e-3);
        d2.setOrientation(Orientation.NORTH);
        d2.x.setValue(18);
        d2.y.setValue(20);
        _sheet.add(d2);


        ElementResistor rF = new ElementResistor(_sheet);
        rF.resistance.setValue(1.0);
        rF.x.setValue(22);
        rF.y.setValue(18);
        rF._orientation = Orientation.WEST;
        _sheet.add(rF);

        ElementResistor rA = new ElementResistor(_sheet);
        rA.resistance.setValue(1.0);
        rA.x.setValue(14);
        rA.y.setValue(18);
        rA._orientation = Orientation.WEST;
        _sheet.add(rA);

        ElementCapacitor Cf = new ElementCapacitor(_sheet);
        Cf.capacitance.setValue(1.0 / 50e3);
        Cf.x.setValue(24);
        Cf.y.setValue(20);
        _sheet.add(Cf);

        ElementVoltage uOut = new ElementVoltage(Cf._terminals.get(0), Cf._terminals.get(1) , _sheet, 1);
        uOut._elementName.setValue("Uout");
        uOut.x.setValue(28);
        uOut.y.setValue(20);
        _sheet.add(uOut);


        ElementResistor rOutBig = new ElementResistor(_sheet);
        rOutBig._elementName.setValue("rConnBig");
        rOutBig.resistance.setValue(10e6);
        rOutBig.x.setValue(26);
        rOutBig.y.setValue(22);
        
        rOutBig._orientation = Orientation.WEST;
        _sheet.add(rOutBig);

        
        ElementResistor rOUT = new ElementResistor(_sheet);
        rOUT._elementName.setValue("Rout");
        rOUT.resistance.setValue(120e-3);
        rOUT.x.setValue(30);
        rOUT.y.setValue(18);
        rOUT._orientation = Orientation.WEST;
        _sheet.add(rOUT);

        ElementConnectionCircuit gndCon = new ElementConnectionCircuit(new Point(28,22), new Point(32,22), _sheet);
        _sheet.add(gndCon);

        ElementConnectionCircuit minCon = new ElementConnectionCircuit(new Point(6,22), new Point(24,22), _sheet);
        _sheet.add(minCon);

        ElementConnectionCircuit plusCon = new ElementConnectionCircuit(new Point(6,18), new Point(8,18), _sheet);
        _sheet.add(plusCon);

        ElementConnectionCircuit upCon = new ElementConnectionCircuit(new Point(16,18), new Point(20,18), _sheet);
        _sheet.add(upCon);


    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paintTerminals(g, dpix);

        int[] xPoints = new int[] {0,-2 * dpix,-1 * dpix,-1 * dpix,-1 * dpix,1 * dpix, 1*dpix, 1 * dpix,2 * dpix , 0, 0};
        int[] yPoints = new int[]{(int) (2.5*dpix),-1 * dpix,-1 * dpix,-2 * dpix,-1 * dpix,-1 * dpix,-2*dpix, -1 * dpix,-1 * dpix ,(int) (2.5*dpix), 3*dpix };

        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillPolygon(xPoints, yPoints, xPoints.length);
        g.setColor(Typ.farbeFertigElementLK);
        
        g.drawPolyline( xPoints, yPoints, xPoints.length);

        g.setFont(new Font("Arial", Font.PLAIN, dpix + 2));
        g.drawString("+", (int) (dpix * 0.3), (int) -(dpix * 0.0));
        AffineTransform oldRot = g.getTransform();
        g.rotate(-_orientation.getRotationAngle());
        g.drawString("_", (int) (-dpix * 0.6), (int) (dpix * 0.7));
        
        g.setTransform(oldRot);



    }



}
