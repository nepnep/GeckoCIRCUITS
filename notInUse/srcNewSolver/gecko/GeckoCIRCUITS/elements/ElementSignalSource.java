/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOptionalIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.ACCESSIBLE;
import gecko.geckocircuitsnew.circuit.TimeFunction;
import gecko.geckocircuitsnew.circuit.TimeFunctionRect;
import gecko.geckocircuitsnew.circuit.TimeFunctionSine;
import gecko.geckocircuitsnew.circuit.TimeFunctionTri;
import gecko.geckocircuitsnew.circuit.Synchronizable;
import gecko.geckocircuitsnew.circuit.StepSynchronizer;

/**
 *
 * @author andy
 */
public class ElementSignalSource extends ElementControl implements Synchronizable {

    TerminalControlOut _outTerm;
    @ACCESSIBLE
    public ModelMVC<Boolean> parameterFromExternal = new ModelMVC<Boolean>(false);

    //ModelMVC versions of signal source parameters
    @ACCESSIBLE
    public final ModelMVC<Double> amplitude = new ModelMVC<Double>(10.0,"amplMAX");
    @ACCESSIBLE
    public final ModelMVC<Double> frequency = new ModelMVC<Double>(50.0,"f","Hz");
    @ACCESSIBLE
    public final ModelMVC<Double> offset = new ModelMVC<Double>(0.0,"offset");
    @ACCESSIBLE
    public final ModelMVC<Double> phase = new ModelMVC<Double>(0.0,"phase","\u00B0");
    @ACCESSIBLE
    public final ModelMVC<Double> dutyratio = new ModelMVC<Double>(0.5,"duty cycle");

    public TimeFunctionSine functionSIN = new TimeFunctionSine(10.0,50.0,0.0,0.0);
    public TimeFunctionRect functionRECT = new TimeFunctionRect(10.0,50.0,0.0,0.0,0.5,steps_saved);
    public TimeFunctionTri functionTRI = new TimeFunctionTri(10.0,50.0,0.0,0.0,0.5,steps_saved);;
    
    //for synchronization, machine epsilon for Java double type
    public static final double ROUND_ERROR = 2.220446049250313e-16;

    /*private double amplitudeAC;
    private double frequenz;
    private double anteilDC;
    private double phase;
    private double tastverhaeltnis;
    private double _dreieck;
    private boolean aufsteigend;*/
    private double randomVal;
    private int typQuelle;
    private double signal;
    //private double dyUP;
    //private double dyDOWN;
    private double tSigStart;
    private double signalDauer;
    private double gomi = -99.99;  // eigentlich ein nicht gesetzter Wert
    private double[][] xy;
    private double[] parameter;
    private boolean displayDetails;



    private TerminalControlOptionalIn acTerminal = new TerminalControlOptionalIn(-1, 2, this);
    private TerminalControlOptionalIn fTerminal = new TerminalControlOptionalIn(-1, 3, this);
    private TerminalControlOptionalIn dcTerminal = new TerminalControlOptionalIn(-1, 4, this);
    private TerminalControlOptionalIn phaseTerminal = new TerminalControlOptionalIn(-1, 5, this);
    private TerminalControlOptionalIn dutyTerminal = new TerminalControlOptionalIn(-1, 6, this);


    public ElementSignalSource(CircuitSheet sheet) {
        super(sheet);
        _outTerm = new TerminalControlOut(2, 0, this);
        _terminals.add(_outTerm);
        _terminals.add(acTerminal);
        _terminals.add(fTerminal);
        _terminals.add(dcTerminal);
        _terminals.add(phaseTerminal);
        _terminals.add(dutyTerminal);

        parameterFromExternal.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                for(Terminal term : _terminals) {
                    if(term instanceof TerminalControlOptionalIn) {
                        ((TerminalControlOptionalIn) term).visible.setValue(parameterFromExternal.getValue());
                    }
                }
            }
        });

        amplitude.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                functionSIN._amplitude = amplitude.getValue();
                functionTRI.amplitude = amplitude.getValue();
                functionRECT.amplitude = amplitude.getValue();
            }
        });

        frequency.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                functionSIN._freq = frequency.getValue();
                functionTRI.frequency = frequency.getValue();
                functionRECT.frequency = frequency.getValue();
            }
        });

       offset.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                functionSIN._offset = offset.getValue();
                functionTRI.offset = offset.getValue();
                functionRECT.offset = offset.getValue();
            }
        });

        phase.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                functionSIN._phase = phase.getValue();
                functionTRI.phase = phase.getValue();
                functionRECT.phase = phase.getValue();
            }
        });

        dutyratio.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                functionTRI.dutyratio = dutyratio.getValue();
                functionRECT.dutyratio = dutyratio.getValue();
            }
        });

        var_history = new double[steps_saved][2];

    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {

    }


    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paint(g, dpix);

        int x1 = -1;
        int y1 = -1;
        double x2 = + br + da;
        double y2 = 0;
        double xe1 =  - 1;
        double ye1 =  + 2;
        double xe2 = xe1;
        double ye2 = ye1 + 1;
        double xe3 = xe1;
        double ye3 = ye1 + 2;
        double xe4 = xe1;
        double ye4 = ye1 + 3;
        double xe5 = xe1;
        double ye5 = ye1 + 4;


        paintElementString(g, dpix, "signal");
        
//        //-----------------
//        if (parameterFromExternal) {  // Steuereingang fuer Block-Parameter
//            g.fillOval((int) (dpix * xe1) - dm / 2, (int) (dpix * ye1) - dm / 2, dm, dm);
//            g.fillOval((int) (dpix * xe2) - dm / 2, (int) (dpix * ye2) - dm / 2, dm, dm);
//            g.fillOval((int) (dpix * xe3) - dm / 2, (int) (dpix * ye3) - dm / 2, dm, dm);
//            g.fillOval((int) (dpix * xe4) - dm / 2, (int) (dpix * ye4) - dm / 2, dm, dm);
//            g.fillOval((int) (dpix * xe5) - dm / 2, (int) (dpix * ye5) - dm / 2, dm, dm);
//            g.drawLine((int) (dpix * xe1), (int) (dpix * (ye1)), (int) (dpix * x), (int) (dpix * (ye1)));
//            g.drawLine((int) (dpix * xe2), (int) (dpix * (ye2)), (int) (dpix * x), (int) (dpix * (ye2)));
//            g.drawLine((int) (dpix * xe3), (int) (dpix * (ye3)), (int) (dpix * x), (int) (dpix * (ye3)));
//            g.drawLine((int) (dpix * xe4), (int) (dpix * (ye4)), (int) (dpix * x), (int) (dpix * (ye4)));
//            g.drawLine((int) (dpix * xe5), (int) (dpix * (ye5)), (int) (dpix * x), (int) (dpix * (ye5)));
//            g.drawLine((int) (dpix * x), (int) (dpix * (y + ho)), (int) (dpix * x), (int) (dpix * (ye5)));
//        }
//        //-----------------
//        // Ansichten -->
//        g.setColor(f1);
//        FontRenderContext frc = ((Graphics2D) g).getFontRenderContext();
//        if ((!parameterFromExternal) && (SchematischeEingabe2.ANSICHT_SHOW_CONTROL_PARAMETER)) {
//            this.zeichneText(g, frc, xTxtKlickMin, yTxtKlickMin);
//            if (SchematischeEingabe2.ANSICHT_SHOW_CONTROL_NAME) {  // falls zusaetzlich auch der Name angezeigt werden soll
//                g.drawString(this.idStringDialog, xTxtKlickMin, yTxtKlickMin);
//                yTxtKlickMin -= SchematischeEingabe2.DY_ZEILENABSTAND_TXT;
//                double lxTxt1 = SchematischeEingabe2.foCONTROL.getStringBounds(this.idStringDialog, frc).getWidth() / dpix;
//                if (lxTxt1 > lxTxt) {
//                    lxTxt = lxTxt1;
//                }
//            }
//        }
//        if ((!SchematischeEingabe2.ANSICHT_SHOW_CONTROL_PARAMETER) && (SchematischeEingabe2.ANSICHT_SHOW_CONTROL_NAME)) {  // nur den Element-Namen anzeigen
//            g.drawString(this.idStringDialog, xTxtKlickMin, yTxtKlickMin);
//            lxTxt = 1.2 * SchematischeEingabe2.foCONTROL.getStringBounds(this.idStringDialog, frc).getWidth() / dpix;
//            yTxtKlickMin -= SchematischeEingabe2.DY_ZEILENABSTAND_TXT;
//            yTxtKlickMax -= SchematischeEingabe2.DY_ZEILENABSTAND_TXT;
//            lyTxt = SchematischeEingabe2.DY_ZEILENABSTAND_TXT * 1.0 / dpix;
//        }
//        //-----------------
//        // "SchematicEntry-Koord." der Ein- u. Ausgangspunkte:
//        xYOUT[0] = (int) x2;
//        yYOUT[0] = (int) y2;
//        if (parameterFromExternal) {
//            int dy = (int) (SchematischeEingabe2.foCONTROL.getStringBounds("xxx", frc).getHeight() * 0.25);
//            g.setColor(Typ.farbeInBearbeitungCONTROL);
//            xXIN[0] = (int) xe1;
//            yXIN[0] = (int) ye1;
//            g.drawString("ac", (int) (dpix * x) + 3, (int) (dpix * ye1) + dy);
//            xXIN[1] = (int) xe2;
//            yXIN[1] = (int) ye2;
//            g.drawString("f", (int) (dpix * x) + 3, (int) (dpix * ye2) + dy);
//            xXIN[2] = (int) xe3;
//            yXIN[2] = (int) ye3;
//            g.drawString("dc", (int) (dpix * x) + 3, (int) (dpix * ye3) + dy);
//            xXIN[3] = (int) xe4;
//            yXIN[3] = (int) ye4;
//            g.drawString("phase", (int) (dpix * x) + 3, (int) (dpix * ye4) + dy);
//            xXIN[4] = (int) xe5;
//            yXIN[4] = (int) ye5;
//            g.drawString("duty", (int) (dpix * x) + 3, (int) (dpix * ye5) + dy);
//            g.setColor(f1);
//        } else {  // setze alle derzeit nicht verwendeten Punkte in die Mitte des Regler-Symbols, damit man sie nicht anklicken kann
//            xXIN[0] = (int) x;
//            yXIN[0] = (int) y;
//            xXIN[1] = (int) x;
//            yXIN[1] = (int) y;
//            xXIN[2] = (int) x;
//            yXIN[2] = (int) y;
//            xXIN[3] = (int) x;
//            yXIN[3] = (int) y;
//            xXIN[4] = (int) x;
//            yXIN[4] = (int) y;
//        }


    }

    @Override
    public ControlType getType() {
        return ControlType.SOURCE;
    }

    /*
    private void ermittleStartwertBeiPhasenverschiebung() {
        //---------------------
        if (tastverhaeltnis < 0) {
            tastverhaeltnis = 0;
        }
        if (tastverhaeltnis > 1) {
            tastverhaeltnis = 1;
        }
        double tx = 0, txEnd = 1.0 / frequenz, dtx = txEnd / 1e3, phaseX = phase;
        while (phaseX > (2 * Math.PI)) {
            phaseX -= (2 * Math.PI);
        }
        while (phaseX < 0) {
            phaseX += (2 * Math.PI);
        }
        _dreieck= 0;
        aufsteigend = true;
        //------
        if (typQuelle == Typ.QUELLE_DREIECK) {
            while (tx < (txEnd * phaseX / (2 * Math.PI))) {
                double dyUPx = (amplitudeAC * 2 * frequenz * dtx) / tastverhaeltnis;
                double dyDOWNx = (amplitudeAC * 2 * frequenz * dtx) / (1 - tastverhaeltnis);
                if (aufsteigend) {
                    _dreieck += dyUPx;
                } else {
                    _dreieck -= dyDOWNx;
                }
                if (amplitudeAC != 0) {  // bei t==0 kann es hier Verwirrung geben!
                    if (_dreieck >= +amplitudeAC) {
                        _dreieck = +amplitudeAC;
                        aufsteigend = false;
                    } else if (_dreieck <= -amplitudeAC) {
                        _dreieck = -amplitudeAC;
                        aufsteigend = true;
                    }
                }
                tx += dtx;
            }
            _dreieck = -_dreieck;
        } else if (typQuelle == Typ.QUELLE_RECHTECK) {
            double txE = txEnd * phaseX / (2 * Math.PI) - (1 - 2 * tastverhaeltnis) / (4 * frequenz);
            if (txE < 0) {
                txE += txEnd;
            }
            while (tx < txE) {
                double dyUPx = (2 * frequenz * dtx) / 0.5;
                double dyDOWNx = (2 * frequenz * dtx) / (1 - 0.5);
                if (aufsteigend) {
                    _dreieck += dyUPx;
                } else {
                    _dreieck -= dyDOWNx;
                }

                if (_dreieck >= (+1)) {
                    _dreieck = (+1);
                    aufsteigend = false;
                } else if (_dreieck <= (-1)) {
                    _dreieck = (-1);
                    aufsteigend = true;
                }
                tx += dtx;
            }
            _dreieck = -_dreieck;
        }
        // 'dreieck' und 'aufsteigend' haben jetzt die korrekten Werte fuer den Simulationsstart bei gegebener Phase
        //---------------------
    }*/


    @Override
    public final void calculateOutput(double time, double dt) {

        /*if(time == 0) {
            ermittleStartwertBeiPhasenverschiebung();
        }*/
        TimeFunction.saveHistory = _model.saveHistory;
        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = signal;
        }

        if (parameterFromExternal.getValue()) {
            amplitude.setValue(acTerminal.getValue());
            frequency.setValue(fTerminal.getValue());
            offset.setValue(dcTerminal.getValue());
            phase.setValue(Math.toRadians(phaseTerminal.getValue()));
            dutyratio.setValue(dutyTerminal.getValue());
            if (time == dt) {
                this.init();  // initialer Verzoegerungsfehler, wenn externe Parameter in Verwendung
            }
        }
        /*
        if (tastverhaeltnis <= 0) {
            tastverhaeltnis = 1e-6;
        }
        if (tastverhaeltnis >= 1) {
            tastverhaeltnis = 1 - 1e-6;
        }*/



        //----------------------------------------------
        switch (typQuelle) {
            case Typ.QUELLE_DC:  // derzeit nicht implementiert
                signal = offset.getValue();
                break;
            case Typ.QUELLE_SIN:  // 'tastverhaeltnis' wird nicht verwendet!
                //signal = amplitudeAC * Math.sin(2 * Math.PI * frequenz * time - phase) + anteilDC;
                signal = functionSIN.calculate(time, dt);
                break;
            case Typ.QUELLE_DREIECK:
                //signal= amplitudeAC*(2/Math.PI*Math.asin(Math.sin(2*Math.PI*frequenz*t -phase))) +anteilDC;
                /*dyUP = (amplitudeAC * 2 * frequenz * dt) / tastverhaeltnis;
                dyDOWN = (amplitudeAC * 2 * frequenz * dt) / (1 - tastverhaeltnis);
                if (aufsteigend) {
                    _dreieck += dyUP;
                } else {
                    _dreieck -= dyDOWN;
                }
                if (amplitudeAC != 0) {  // bei t==0 kann es hier Verwirrung geben!
                    if (_dreieck >= +amplitudeAC) {
                        _dreieck = +amplitudeAC;
                        aufsteigend = false;
                    } else if (_dreieck <= -amplitudeAC) {
                        _dreieck = -amplitudeAC;
                        aufsteigend = true;
                    }
                }
                signal = _dreieck + anteilDC;*/
                signal = functionTRI.calculate(time, dt);
                break;
            case Typ.QUELLE_RECHTECK:
                //double dphi= Math.PI*(0.5-tastverhaeltnis);  // Korrekturwinkel, damit Rechteck-Signal immer im Ursprung beginnt
                //double fdr= 1/Math.PI*Math.asin(Math.sin(2*Math.PI*frequenz*t -phase +dphi)) +0.5;  // [0...1]
                /*dyUP = 4 * frequenz * dt;
                dyDOWN = 4 * frequenz * dt;
                if (aufsteigend) {
                    _dreieck += dyUP;
                } else {
                    _dreieck -= dyDOWN;
                }
                if (_dreieck >= (+1)) {
                    _dreieck = (+1);
                    aufsteigend = false;
                } else if (_dreieck <= (-1)) {
                    _dreieck = (-1);
                    aufsteigend = true;
                }
                if (_dreieck > 1 - 2 * tastverhaeltnis) {
                    signal = amplitudeAC + anteilDC;
                } else {
                    signal = anteilDC;
                }*/
                signal = functionRECT.calculate(time, dt);
                break;
            case Typ.QUELLE_RANDOM:
                randomVal += (1 - 2 * Math.random());  // [-1 .. +1]
                signal = randomVal;
                break;
            case Typ.QUELLE_IMPORT:
                //--------------
                if (_model.saveHistory && !stepped_back && (time > 0))
                {
                  var_history[0][2] = tSigStart;
                }
                if (tSigStart > time) {
                    tSigStart = 0;  // zB. bei Neustarten der Simulation
                }
                while (tSigStart + signalDauer < time) {
                    tSigStart += signalDauer;  // tLokal 'zeigt' immer auf den Zeitpunkt des Beginns einer Signal-Periode
                }                // ungefaehre Position bestimmen:
                int zeiger = (int) (xy[0].length * ((time - tSigStart) / signalDauer));
                // Feinadjustierung: jetzt die exakte Posistion bestimmen
                if (tSigStart + xy[0][zeiger] < time) {
                    while ((zeiger < xy[0].length - 1) && (tSigStart + xy[0][zeiger] < time)) {
                        zeiger++;  // Erg. -->  xy[0][zeiger] >= t  oder  zeiger ==> xy[0][end]
                    }
                } else {
                    while ((zeiger > 0) && (tSigStart + xy[0][zeiger] > time)) {
                        zeiger--;  // Erg. -->  xy[0][zeiger] <= t  oder  zeiger ==> xy[0][0]
                    }
                }
                if (zeiger == 0) {
                    signal = xy[1][0];
                    //} else if (zeiger==xy[0].length-1) {
                    //    signal= xy[1][xy[0].length-1];
                } else if (tSigStart + xy[0][zeiger] >= time) {
                    double t1 = xy[0][zeiger - 1], t2 = xy[0][zeiger], y1 = xy[1][zeiger - 1], y2 = xy[1][zeiger];
                    signal = y1 + (y2 - y1) * (time - tSigStart - t1) / (t2 - t1);
                } else {
                    double t1 = xy[0][zeiger], t2 = xy[0][zeiger + 1], y1 = xy[1][zeiger], y2 = xy[1][zeiger + 1];
                    signal = y1 + (y2 - y1) * (time - tSigStart - t1) / (t2 - t1);
                }
                break;
            //--------------
            default:
                System.out.println("Fehler: Signalquelle nicht spezifiziert [owergni049]");
                break;
        }
        _outTerm.writeValue(signal, time);
        //System.out.println("signal source executed " + time + " " + dt + " " + stepped_back);
        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _outTerm.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            signal = var_history[0][1];
            if (typQuelle == Typ.QUELLE_DREIECK)
                functionTRI.stepBack();
            else if (typQuelle == Typ.QUELLE_RECHTECK)
                functionRECT.stepBack();
            else if (typQuelle == Typ.QUELLE_IMPORT)
                tSigStart = var_history[0][2];
            stepped_back = true;
            steps_reversed++;
        }
    }


    public void init() {
        //------------------
        // this.ermittleStartwertBeiPhasenverschiebung();
        randomVal = 0;
    }

    

    @Override
    public void setParameter(double[] reglerParameter) {
        //-------------
        // Nachtraeglich werden weitere Parameter zugefuegt, alte Versionen sollen aber trotzdem funktionieren:
        if (reglerParameter.length == 7) {
            this.parameter = new double[8];
            for (int i2 = 0; i2 < reglerParameter.length; i2++) {
                this.parameter[i2] = reglerParameter[i2];
            }
        } else {
            this.parameter = reglerParameter;
        }
        //-------------
        typQuelle = (int) parameter[0];
        amplitude.setValue(parameter[1]);
        if (typQuelle == Typ.QUELLE_DC) {
            frequency.setValue(gomi);
            offset.setValue(gomi);
            dutyratio.setValue(gomi);
            phase.setValue(gomi);
        } else {
            frequency.setValue(parameter[2]);
            offset.setValue(parameter[3]);
            phase.setValue(parameter[4]);
            dutyratio.setValue(parameter[5]);
        }
        parameterFromExternal.setValue((reglerParameter[6] == 0) ? false : true);  // reglerParameter[6] ist 0 oder 1
        displayDetails = (parameter[7] == 0) ? false : true;
    }

    public int getSourceType()
    {
        return typQuelle;
    }
    
    //synchronization functions
    public void addSynchronizer(StepSynchronizer syncer) {
        functionTRI.addSynchronizer(syncer);
        functionRECT.addSynchronizer(syncer);
    }
    
    public void makeSynchronizable(boolean sync) {
        functionTRI.makeSynchronizable(sync);
        functionRECT.makeSynchronizable(sync);
    }


}
