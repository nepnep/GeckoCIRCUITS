/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.IGBT;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;
import gecko.geckocircuitsnew.circuit.SwitchState;

public class ElementIGBT extends ElementCircuit {

    public final ModelMVC<Double> resistance = new ModelMVC<Double>(1000.0);
    @OPTIMIZABLE
    public final ModelMVC<Double> uForward = new ModelMVC<Double>(CircuitComponent.DEFAULT_U_FORWARD,"UF","V");
    @OPTIMIZABLE
    public final ModelMVC<Double> rON = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_ON,"Ron","ohm");
    @OPTIMIZABLE
    public final ModelMVC<Double> rOFF = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_OFF,"Roff","ohm");

    private final IGBT _igbt;

    public ElementIGBT(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        _igbt = new IGBT(_terminals.get(0), _terminals.get(1), this);
        circuitComponents.add(_igbt);

        uForward.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _igbt.setUForward(uForward.getValue());
            }
        });

        rOFF.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _igbt.setROff(rOFF.getValue());
            }
        });

        rON.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _igbt.setROn(rON.getValue());
            }
        });


    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg = 2.0; double br = 0.7; double ho = 0.4; double p1 = 8; double p2 = 1;
        double dp = 1.5;

        g.drawPolyline(
                new int[]{0, 0, (int) (-dpix * br), (int) (-dpix * br), 0, 0},
                new int[]{(int) (-dpix * lg), (int) (dpix * ( - lg / 2)), (int) (dpix * ( - lg / 2)), (int) (dpix * ( + lg / 2)), (int) (dpix * ( + lg / 2)), (int) (dpix * ( + lg))}, 6);
        g.drawPolyline(
                new int[]{(int) (dpix * ( - lg / 2)), (int) (dpix * ( - lg / 2)), (int) (dpix * ( - lg / 2 - ho))},
                new int[]{(int) (dpix * ( - lg / 2)), (int) (dpix * ( + lg / 2)), (int) (dpix * ( + lg / 2))}, 3);
        g.fillPolygon(
                new int[]{(int) (dpix * ( - br)), (int) (dpix * ( - br)), 0},
                new int[]{(int) (dpix * ( + lg / 2) - p1), (int) (dpix * ( + lg / 2)), (int) (dpix * ( + lg / 2))}, 3);

    }

    @Override
    protected void importASCII_Individual(String ascii[]) {
    }

   

    @Override
    protected void setParameter(double[] parameter) {
        resistance.setValue(parameter[0]);
        uForward.setValue(parameter[1]);
        rON.setValue(parameter[2]);
        rOFF.setValue(parameter[3]);
    }
    
    public SwitchState getState(double time) {
        return _igbt.getState(time);
    }

    
}
