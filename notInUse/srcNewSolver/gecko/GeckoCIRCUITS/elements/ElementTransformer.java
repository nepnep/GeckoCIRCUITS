/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.JPanelDCMachine;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 *
 * @author andy
 */
public class ElementTransformer extends ElementSubSheet {

    public ElementTransformer(CircuitSheet sheet) {
        super(sheet);

        _sheet._sheetSizeX.setValue(40);
        _sheet._sheetSizeY.setValue(30);

        _sheet.paintExtensionX = 100;

        JPanelDCMachine dialog = new JPanelDCMachine();

        int dialogHeight = dialog.getPreferredSize().height;
        _sheet.paintExtensionY = dialogHeight;
        int dialogWidth = dialog.getPreferredSize().width;
        dialog.setBounds(0,_sheet._sheetSizeY.getValue() * _sheet._dpix.getValue(),
                dialogWidth,dialogHeight);
        //dialog.setOpaque(false);
        _sheet.add(dialog);


        ElementTerminalCircuit minusTerm = new ElementTerminalCircuit(this, "P1", 6, 12, -1, -2, _sheet);
        ElementTerminalCircuit plusTerm = new ElementTerminalCircuit(this, "P2", 6, 8, 1, -2, _sheet);
        ElementTerminalCircuit outTerm = new ElementTerminalCircuit(this, "S1", 22, 8, 0, 3, _sheet);
        outTerm.setOrientation(Orientation.NORTH);

        ElementTerminalCircuit gndTerm = new ElementTerminalCircuit(this, "S2", 22, 12,-2,1, _sheet);
        gndTerm.setOrientation(Orientation.NORTH);



        ElementResistor rP = new ElementResistor(_sheet);
        rP._elementName.setValue("Rp");
        rP.resistance.setValue(0.01);
        rP.setOrientation(Orientation.WEST);
        rP.x.setValue(10);
        rP.y.setValue(8);
        _sheet.add(rP);

        ElementInductor LMain = new ElementInductor(sheet,true);
        LMain._elementName.setValue("LSp");
        LMain.inductance.setValue(10e-3);
        LMain.setOrientation(Orientation.SOUTH);
        LMain.x.setValue(12);
        LMain.y.setValue(10);
        _sheet.add(LMain);

        ElementInductor LSec = new ElementInductor(sheet,true);
        LSec._elementName.setValue("Lsec");
        LSec.inductance.setValue(2e-3);
        LSec.setOrientation(Orientation.SOUTH);
        LSec.x.setValue(16);
        LSec.y.setValue(10);
        _sheet.add(LSec);

        ElementResistor rS = new ElementResistor(_sheet);
        rS._elementName.setValue("Rs");
        rS.resistance.setValue(0.02);
        rS.setOrientation(Orientation.WEST);
        rS.x.setValue(18);
        rS.y.setValue(8);
        _sheet.add(rS);

        ElementResistor RCon = new ElementResistor(_sheet);
        RCon._elementName.setValue("Rs");
        RCon.resistance.setValue(1E6);
        RCon.setOrientation(Orientation.WEST);
        RCon.x.setValue(14);
        RCon.y.setValue(12);
        _sheet.add(RCon);

        
        ElementMutualInductance mut = new ElementMutualInductance(_sheet);
        mut.x.setValue(14);
        mut.y.setValue(5);
        _sheet.add(mut);
        
        ElementConnectionCircuit gndCon = new ElementConnectionCircuit(new Point(6,8), new Point(8,8), _sheet);
        _sheet.add(gndCon);

        ElementConnectionCircuit minCon = new ElementConnectionCircuit(new Point(6,12), new Point(12,12), _sheet);
        _sheet.add(minCon);

        ElementConnectionCircuit secDown = new ElementConnectionCircuit(new Point(16,12), new Point(22,12), _sheet);
        _sheet.add(secDown);

        ElementConnectionCircuit secUp = new ElementConnectionCircuit(new Point(20,8), new Point(22,8), _sheet);
        _sheet.add(secUp);


    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paintTerminals(g, dpix);

        int[] xPoints = new int[] {0,-2 * dpix,-1 * dpix,-1 * dpix,-1 * dpix,1 * dpix, 1*dpix, 1 * dpix,2 * dpix , 0, 0};
        int[] yPoints = new int[]{(int) (2.5*dpix),-1 * dpix,-1 * dpix,-2 * dpix,-1 * dpix,-1 * dpix,-2*dpix, -1 * dpix,-1 * dpix ,(int) (2.5*dpix), 3*dpix };

        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillPolygon(xPoints, yPoints, xPoints.length);
        g.setColor(Typ.farbeFertigElementLK);

        g.drawPolyline( xPoints, yPoints, xPoints.length);

        g.setFont(new Font("Arial", Font.PLAIN, dpix + 2));
        g.drawString("+", (int) (dpix * 0.3), (int) -(dpix * 0.0));
        AffineTransform oldRot = g.getTransform();
        g.rotate(-_orientation.getRotationAngle());
        g.drawString("_", (int) (-dpix * 0.6), (int) (dpix * 0.7));

        g.setTransform(oldRot);



    }



}
