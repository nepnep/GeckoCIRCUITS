/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.Capacitor;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.Resistor;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalInvisibleSubCircuit;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import javax.swing.JButton;

/**
 *
 * @author andy
 */
public class ElementSubSheet extends ElementSubCircuit implements SubSheetable {

    
    protected CircuitSheet _sheet = new CircuitSheet(300, 300);

    public ElementSubSheet(CircuitSheet sheet) {
        super(sheet);
        _sheet.sheetModel = sheet.sheetModel;
        JButton upButton = new JButton("Level up");
        upButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CircuitSheet.setActiveSheet(CircuitSheet.superSheet);
            }
        });

//        _sheet.add(new ElementTerminalCircuit(this));
//
        upButton.setSize(100, 20);
        upButton.setBounds(0,0,100,20);
        upButton.setOpaque(false);
        _sheet.add(upButton);

        JButton addTerminalButton = new JButton("Add Terminal");
        addTerminalButton.setSize(90, 20);
        addTerminalButton.setBounds(100, 0, 180, 20);
        _sheet.add(addTerminalButton);

    }




    @Override
    protected void importASCII_Individual(String[] asciiBlock) {

    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int scaling) {
    }

    public CircuitSheet getSubSheet() {
        return _sheet;
    }



}
