/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.geckocircuitsnew.circuit.Inductor;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.InductorSingle;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;
import geckocircuitsnew.ACCESSIBLE;

public class ElementInductor extends ElementCircuit {

    @OPTIMIZABLE
    public final ModelMVC<Double> inductance = new ModelMVC<Double>(1000.0,"L","H");
    @ACCESSIBLE
    public final ModelMVC<Double> initCurrent = new ModelMVC<Double>(0.0,"iL(0)","A");
    public final ModelMVC<Boolean> coupled = new ModelMVC<Boolean>(false);
    public /*final*/ Inductor inductor;

    public ElementInductor(CircuitSheet sheet, boolean _coupled) {
        super(sheet);
        super.initTwoTerminalComponent();

        coupled.setValue(_coupled); //remove later if this is included in the setParameters method
        if (_coupled) {
            inductor = new InductorCoupling(_terminals.get(0), _terminals.get(1), this);
        }
        else {
            inductor =  new InductorSingle(_terminals.get(0), _terminals.get(1), this);
        }

        inductance.addModelListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                inductor.setInductance(inductance.getValue());
            }
        });

        initCurrent.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                inductor.setInitialCurrent(initCurrent.getValue());
            }
        });
        
        coupled.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                changeInductorCoupling(coupled.getValue());
            }
        });

        circuitComponents.add(inductor);

    }
    //perhaps could be done in a better way
    private void changeInductorCoupling(boolean isCoupled) {
        if (isCoupled) {
            inductor = new InductorCoupling(_terminals.get(0), _terminals.get(1), this);
        }
        else {
            inductor =  new InductorSingle(_terminals.get(0), _terminals.get(1), this);
        }
        inductor.setInductance(inductance.getValue());
        inductor.setInitialCurrent(initCurrent.getValue());
    }
    

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {
        double lg=2.0;   double da=0.5;   int d=4;   int dm=5;   double br=0.35;   double ho=0.8;

        double rq = Math.round(dpix * (2 * ho) / 7.0);
        int yq = -1;
        

        g.setStroke(new BasicStroke((float) 2.0));
        for (yq = (int) (dpix * (- ho)); yq <= (int) (dpix * ( + ho)); yq += (int) (2 * rq)) {
            g.drawArc((int) (- 2.0 * rq), (int) (yq - rq), (int) (4 * rq), (int) (2 * rq), 90, 180);
        }
        g.setStroke(new BasicStroke((float) 1.0));
        g.drawLine(0, (int) (dpix * ( - lg)), 0, (int) (-dpix * ho - rq));
        g.drawLine(0, (int) (yq - rq), 0, (int) (dpix * lg));


    }

    @Override
    protected void importASCII_Individual(String ascii[]) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        inductance.setValue(parameter[0]);
        initCurrent.setValue(parameter[1]);
    }

    
}
