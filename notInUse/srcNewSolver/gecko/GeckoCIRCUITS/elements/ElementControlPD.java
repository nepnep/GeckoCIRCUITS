/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ElementControlPD extends ElementControlSingleInOut {

    private double yalt = 0;
    private double xalt = 0;
    private double xaltInit = 0;
    private double a1;  // default: G(s)= a1*s
    private double inValue = 0;
    private double outValue = 0;
    
    private double _xaltMaxAbs = 0;
    
    private double xaltalt = 0;

    //ModelMVC<T> versions of all ControlPD fields
    @ACCESSIBLE
    public final ModelMVC<Double> A1 = new ModelMVC<Double>(1.0,"a1");

    public ElementControlPD(CircuitSheet sheet) {
        super(sheet);
        _baseName = "PD";
        var_history = new double[steps_saved][4];
        A1.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                a1 = A1.getValue();
            }
        });
    }

    @Override
    protected void setParameter(double[] parameter) {
        a1 = parameter[0];
    }
    
    @Override
    public void init() {
        yalt = 0;
        xalt = xaltInit;
        xaltInit = 0;
    }


    @Override
    public void calculateOutput(double time, double dt) {
        if (_model.saveHistory && !stepped_back && time > 0) {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = outValue;
            var_history[0][2] = yalt;
            var_history[0][3] = xalt;
        }
        inValue = _termIn.getValue();
        outValue = a1 / dt * (inValue - xalt);  // vereinfachte Formel ohne yalt --> wird numerisch viel robuster
        
        xaltalt = xalt;
        
        xalt = inValue;
        _termOut.writeValue(outValue, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
        
        if (Math.abs(xalt) > _xaltMaxAbs) {
            _xaltMaxAbs = Math.abs(xalt);
        }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            outValue = var_history[0][1];
            yalt = var_history[0][2];
            xalt = var_history[0][3];
            steps_reversed++;
            stepped_back = true;
        }
    }
    
    @Override
    public boolean hasInternalStates()
    {
        return true;
    }
    
    @Override
    public double[] getState()
    {
        //return new double[] {xalt};
        return new double[] {xaltalt};
    }
    
    @Override
    public void setState(double[] state)
    {
        xaltInit = state[0];
        _xaltMaxAbs = Math.abs(xaltInit);
    }
    
    @Override 
    public double getStateVar(int index)
    {
        if (index == 0)
            return xalt;
        else
            return 0;
    }
    
    @Override 
    public void setStateVar(double value, int index)
    {
        if (index == 0) {
            xaltInit = value;
            _xaltMaxAbs = Math.abs(xaltInit);
        }
    }
    
    public double getStateVarMaxAbs(int index) {
        if (index == 0) {
            return _xaltMaxAbs;
        }
        else {
            return 0;
        }
    }
}
