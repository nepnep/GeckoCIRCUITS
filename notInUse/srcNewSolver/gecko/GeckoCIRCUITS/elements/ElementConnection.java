/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.animation.CurrentState;
import gecko.geckocircuitsnew.animation.AbstractAnimate;
import gecko.geckocircuitsnew.animation.AnimateConnector;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminable;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import geckocircuitsnew.DatenSpeicher;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public abstract class ElementConnection extends ElementInterface implements Serializable, Terminable {

    private boolean geradeGezogen = false;  // wird nur zur De-selektion mittels ESCAPE verwendet
    private int pktMAX;  // maximale Punktzahl einer Verbindung
    protected ArrayList<Integer> xPix = new ArrayList<Integer>();
    protected ArrayList<Integer> yPix = new ArrayList<Integer>();  // Koordinaten im Raster
    private int zeigerAktuell = 0;
    private int nr;  // ID-Nummer der Verbindung
    private boolean bewegungNS, bewegungWO;  // Bewegungsrichtung mit der Maus beim Ziehen der Verbindung
    private int[] netzlistenKnotenNummer;  // bei den Elementen definierte Knoten [wird 'Netzliste.java' gesetzt]
    private int zeigerNetzlisteKnotenNr;
    private boolean knotenBereitsZusammengefasst = false;
    public final ModelMVC<String> _label = new ModelMVC<String>("");
    //----------------------------------
    private int _minX = 100000;
    private int _minY = 10000;
    private int _maxX = -100000;
    private int _maxY = -10000;
    // --- Animation Parameters ----
    public ArrayList<AnimateConnector> _animation = new ArrayList<AnimateConnector>();
    private int[] _relX = new int[3];
    private int[] _relY = new int[3];
    private ArrayList<Point> _trimmedCoords = new ArrayList<Point>();
    public List<Point> _subPaths = new ArrayList<Point>();
    private static final int INITSTATE = 0;

    public static void fabric(String elementName, final String[] ascii, int[] zeilenZeiger, final CircuitSheet sheet) {


        if (!elementName.startsWith("verbindungLK")) {
            if (!elementName.startsWith("verbindungCONTROL")) {
                if (!elementName.startsWith("verbindungTHERM")) {
                    return;
                }
            }

        }

        zeilenZeiger[0] += 2;
        String txt = "";

        boolean reluctanceFlag = false;
        while (!ascii[zeilenZeiger[0]].startsWith("<\\Verbindung>")) {
            txt += "\n" + ascii[zeilenZeiger[0]];
            if (ascii[zeilenZeiger[0]].startsWith("label ")) {
                String labelString = DatenSpeicher.leseASCII_String(ascii[zeilenZeiger[0]]);
                if (labelString.startsWith("_")) {
                    reluctanceFlag = true;
                }
            }
            zeilenZeiger[0]++;
        }



        ElementConnection con = null;
        if (elementName.startsWith("verbindungLK")) {
            if (reluctanceFlag) {
                con = new ElementConnectionReluctance(txt, sheet);
            } else {
                con = new ElementConnectionCircuit(txt, sheet);
            }
        }

        if (elementName.startsWith("verbindungCONTROL ")) {
            con = new ElementConnectionControl(txt, sheet);
        }

        if (elementName.startsWith("verbindungTHERM ")) {
            con = new ElementConnectionTherm(txt, sheet);
        }

    }

    /**
     *
     * @param start startpoint on the circuit sheet in sheet coordinates
     * @param stop startpoint on the circuit sheet in sheet coordinates
     */
    public ElementConnection(Point start, Point stop, CircuitSheet sheet) {
        super(sheet);

        int distance = (int) start.distance(stop);

        int posX = start.x;
        int posY = start.y;
        int incrX = (int) ((stop.x - start.x) / distance);
        int incrY = (int) ((stop.y - start.y) / distance);

        for (int i = 0; i <= distance; i++) {
            xPix.add(posX);
            yPix.add(posY);
            posX += incrX;
            posY += incrY;
        }

        _terminals.add(new TerminalConnector(this, true));
        _terminals.add(new TerminalConnector(this, false));

        determineBounds();
        this.trimCoordinates();
    }

    @Override
    public ArrayList<Terminal> getTerminals() {
        return _terminals;
    }

    public ElementConnection(String ascii, CircuitSheet sheet) {
        super(sheet);

        this.importASCII(ascii);
        _terminals.add(new TerminalConnector(this, true));
        _terminals.add(new TerminalConnector(this, false));
        determineBounds();
        this.trimCoordinates();
    }

    public int getID() {
        return nr;
    }

    public void setIDnr(int nr) {
        this.nr = nr;
    }

    public ArrayList<Integer> getXPix() {
        return xPix;
    }

    public ArrayList<Integer> getYPix() {
        return yPix;
    }

    public boolean wirdGeradeGezogen() {
        return geradeGezogen;
    }

    public int getAktuelleLaenge() {
        return zeigerAktuell;
    }

    public int[] getAnfangsKnoten() {
        return new int[]{xPix.get(0), yPix.get(0)};
    }

    public int[] getEndKnoten() {
        return new int[]{xPix.get(xPix.size() - 1), yPix.get(yPix.size() - 1)};
    }

    public ArrayList<Point> getAllPoints() {
        ArrayList<Point> allPoints = new ArrayList<Point>();

        for (int i = 0; i < xPix.size(); i++) {
            allPoints.add(new Point(xPix.get(i), yPix.get(i)));
        }
        return allPoints;
    }

    public void setNetzlistenKnotenNummer(int knotenNr) {
        netzlistenKnotenNummer[zeigerNetzlisteKnotenNr] = knotenNr;
        zeigerNetzlisteKnotenNr++;
    }

    public int[] getNetzlistenKnotenNummer() {
        int[] nlKNr = new int[zeigerNetzlisteKnotenNr];
        System.arraycopy(netzlistenKnotenNummer, 0, nlKNr, 0, zeigerNetzlisteKnotenNr);
        return nlKNr;
    }

    public boolean getKnotenBereitsZusammengefasst() {
        return knotenBereitsZusammengefasst;
    }

    public void setKnotenBereitsZusammengefasst() {
        knotenBereitsZusammengefasst = true;
    }

    public void setzeStartKnoten(int x, int y) {
        geradeGezogen = true;
        xPix.add(x);
        xPix.add(y);
    }

    @Override
    public void paintComponent(Graphics g) {
        setPaintColors();

        Graphics2D g2d = (Graphics2D) g;
        AffineTransform oldTransform = g2d.getTransform();

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.translate(10, 10);
        if (this instanceof ElementConnectionReluctance) {
            Typ.setReluctanceColors();
        }
        g.setColor(Typ.colorConnection);
        paint(g2d, CircuitSheet._dpix.getValue());
        g2d.setTransform(oldTransform);
    }

    @Override
    public final void paint(final Graphics2D graphic, final int dpix) {
        super.paint(graphic, dpix);
        int[] xPaint = new int[_trimmedCoords.size()];
        int[] yPaint = new int[_trimmedCoords.size()];


        for (int i = 0; i < _trimmedCoords.size(); i++) {
            xPaint[i] = (_trimmedCoords.get(i).x - x.getValue()) * dpix;
            yPaint[i] = (_trimmedCoords.get(i).y - y.getValue()) * dpix;

        }


        graphic.drawPolyline(xPaint, yPaint, xPaint.length);

        for (AnimateConnector aniPart : _animation) {
            aniPart.paint(graphic, dpix);
        }


//        int dm = 5;
        /*
         * // eventuelle Labels der beiden Knoten ausgeben: if (fFertig.equals(Typ.farbeFertigVerbindungLK))
         * g.setColor(Typ.farbeLabelLK); else if (fFertig.equals(Typ.farbeFertigVerbindungCONTROL))
         * g.setColor(Typ.farbeLabelCONTROL); g.setFont(fo); if ((!label.equals("")) && ((xPix[0]!=xLabel)&&(yPix[0]!=yLabel))
         * &&((xPix[zeigerAktuell-1]!=xLabel)&&(yPix[zeigerAktuell-1]!=yLabel)))
         */
        //g.drawString(_label.getValue(), xPaint[0] + 3, yPaint[0] - 3);

        //--------------------------------
    }

    @Override
    public boolean contains(Point p) {
        return contains(p.x, p.y);
    }

    @Override
    public boolean contains(int ptx, int pty) {
        return false;
//        int dpix = CircuitSheet._dpix.getValue();
//        //Shape containShape = new Rectangle(x.getValue()*dpix-5, y.getValue()*dpix-5, 10, 1);
//        boolean returnValue = containShape.contains(ptx, pty);
//
//        return returnValue;
    }

    // zum Speichern im ASCII-Format (anstatt als Object-Stream) -->
    public void exportASCII(StringBuffer ascii) {
//        ascii.append("<Verbindung>");
//        //------------------
//        DatenSpeicher.appendAsString(ascii.append("\nlabel"), label);
//        DatenSpeicher.appendAsString(ascii.append("\nnr"), nr);
//        DatenSpeicher.appendAsString(ascii.append("\nmodus"), modus);
//        DatenSpeicher.appendAsString(ascii.append("\ndpix"), dpix);
//        DatenSpeicher.appendAsString(ascii.append("\nzeigerAktuell"), zeigerAktuell);
//        DatenSpeicher.appendAsString(ascii.append("\nzeigerNetzlisteKnotenNr"), zeigerNetzlisteKnotenNr);
//        DatenSpeicher.appendAsString(ascii.append("\nxLabel"), xLabel);
//        DatenSpeicher.appendAsString(ascii.append("\nyLabel"), yLabel);
//        DatenSpeicher.appendAsString(ascii.append("\nlabelKnotenNr"), labelKnotenNr);
//        DatenSpeicher.appendAsString(ascii.append("\nxLabelVorVerschieben"), xLabelVorVerschieben);
//        DatenSpeicher.appendAsString(ascii.append("\nyLabelVorVerschieben"), yLabelVorVerschieben);
//        DatenSpeicher.appendAsString(ascii.append("\nx"), this.reduziereArray(x, zeigerAktuell));
//        DatenSpeicher.appendAsString(ascii.append("\ny"), this.reduziereArray(y, zeigerAktuell));
//        DatenSpeicher.appendAsString(ascii.append("\nxPix"), this.reduziereArray(xPix, zeigerAktuell));
//        DatenSpeicher.appendAsString(ascii.append("\nyPix"), this.reduziereArray(yPix, zeigerAktuell));
//        DatenSpeicher.appendAsString(ascii.append("\nnetzlistenKnotenNummer"), netzlistenKnotenNummer);
//        DatenSpeicher.appendAsString(ascii.append("\nxVorVerschieben"), this.reduziereArray(xVorVerschieben, zeigerAktuell));
//        DatenSpeicher.appendAsString(ascii.append("\nyVorVerschieben"), this.reduziereArray(yVorVerschieben, zeigerAktuell));
//        //-----------
//        ascii.append(new StringBuffer("\n<\\Verbindung>\n"));
        //------------------
    }

    // vor dem Abspeichern werden die Felder nur soweit gespeichert, wie Daten vorhanden sind -->
    private int[] reduziereArray(int[] k, int max) {
        int[] kRed = new int[max];
        System.arraycopy(k, 0, kRed, 0, max);
        return kRed;
    }

    public void importASCII(String asciiBlock) {
        String[] ascii = DatenSpeicher.makeStringArray(asciiBlock);
        //System.out.println(asciiBlock+"\n********************");
        //------------------
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("modus ")) {
//                modus = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("zeigerAktuell ")) {
//                zeigerAktuell = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("nr ")) {
//                nr = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("zeigerNetzlisteKnotenNr ")) {
//                zeigerNetzlisteKnotenNr = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("label ")) {
                String labelString = DatenSpeicher.leseASCII_String(ascii[i1]);
                if (labelString.equals(Typ.NIX)) {
                    labelString = "";
                }
                _label.setValue(labelString);
            }
            if (ascii[i1].startsWith("xLabel ")) {
//                xLabel = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("yLabel ")) {
//                yLabel = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("labelKnotenNr ")) {
//                labelKnotenNr = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("xLabelVorVerschieben ")) {
//                xLabelVorVerschieben = DatenSpeicher.leseASCII_int(ascii[i1]);
            }
            if (ascii[i1].startsWith("yLabelVorVerschieben ")) {
//                yLabelVorVerschieben = DatenSpeicher.leseASCII_int(ascii[i1]);
            }

            if (ascii[i1].startsWith("x[] ")) {
                int[] tmpxPix = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
                for (int x : tmpxPix) {
                    xPix.add(x);
                }
            }

            if (ascii[i1].startsWith("y[] ")) {
                int[] tmpyPix = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
                for (int y : tmpyPix) {
                    yPix.add(y);
                }

            }


            if (ascii[i1].startsWith("xVorVerschieben[] ")) {
//                xVorVerschieben = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("yVorVerschieben[] ")) {
//                yVorVerschieben = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            }
            if (ascii[i1].startsWith("netzlistenKnotenNummer[] ")) {
//                netzlistenKnotenNummer = DatenSpeicher.leseASCII_intArray1(ascii[i1]);
            }
        }
        //------------------
    }

    private void determineBounds() {


        assert xPix.size() == yPix.size() : xPix.size() + "!=" + yPix.size();

        for (int i = 0; i < xPix.size(); i++) {
            _minY = Math.min(yPix.get(i), _minY);
            _maxY = Math.max(yPix.get(i), _maxY);
            _minX = Math.min(xPix.get(i), _minX);
            _maxX = Math.max(xPix.get(i), _maxX);
        }

        y.setValue(_minY);
        x.setValue(_minX);

        int dpix = CircuitSheet._dpix.getValue();
        boundsHeight = (_maxY - _minY) * dpix;
        _boundsWidth = (_maxX - _minX) * dpix;

        setBounds(x.getValue() * dpix - 10, y.getValue() * dpix - 10, 200 + 2 * _boundsWidth, 200 + 2 * boundsHeight);

    }

    public void trimCoordinates() {
        if (xPix.size() > 1) {
            _trimmedCoords.add(new Point(xPix.get(0), yPix.get(0)));

            if (xPix.get(0) == xPix.get(1)) { // then y changes
                for (int j = 0; j < xPix.size() - 1; j++) {
                    if (yPix.get(j) == yPix.get(j + 1)) {
                        _trimmedCoords.add(new Point(xPix.get(j), yPix.get(j)));
                        break;
                    }
                }
            } else {
                for (int j = 0; j < yPix.size() - 1; j++) {
                    if (xPix.get(j) == xPix.get(j + 1)) {
                        _trimmedCoords.add(new Point(xPix.get(j), yPix.get(j)));
                        break;
                    }
                }
            }

            _trimmedCoords.add(new Point(xPix.get(xPix.size() - 1), yPix.get(yPix.size() - 1)));
            
           
        }
    }

    public final ArrayList<Point> getTrimmedCoords() {
        return _trimmedCoords;
    }

    public String getPeecLabelAnfangsKnoten(int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getPeecLabelEndKnoten(int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setPeecLabelAnfangsKnoten(String q, int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setPeecLabelEndKnoten(String q, int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private int[] generateRelCoords(final Point start, final Point end) {
        int[] result = new int[4];
        result[0] = start.x - x.getValue();
        result[1] = start.y - y.getValue();
        result[2] = end.x - x.getValue();
        result[3] = end.y - y.getValue();

//        System.out.println(_trimmedCoords + "\t-->\t" + start + end);
//        System.out.println("\t" + result[0] + ", " + result[1] + ", " + result[2] + ", " + result[3]);
//        System.out.println("\t" + x.getValue() + ", " + y.getValue());

        return result;
    }

    public final void initAnimationParts() {
        if (!_animation.isEmpty()) {
            return;
        }

        final List<Point> pathPoints = createAnimationPath();

        for (int i = 0; i < pathPoints.size() - 1; i++) {
            final int[] relCoords = generateRelCoords(pathPoints.get(i), pathPoints.get(i + 1));

            final AnimateConnector aniPart = new AnimateConnector(this, relCoords);
            aniPart.setAnimationState(CurrentState.ZERO);
            aniPart.setAnimationLocation(pathPoints.get(i), pathPoints.get(i + 1));
            _animation.add(aniPart);
            
        }

    }



    private List<Point> createAnimationPath() {
        final List<Point> path = new ArrayList<Point>();

        if (_trimmedCoords.size() > 1) {
            Point curPoint = _trimmedCoords.get(0);
            path.add(curPoint);

            if (_trimmedCoords.size() == 2) {
                for (Point subPoint : _subPaths) {
                    path.add(subPoint);
                }
            }

            if (_trimmedCoords.size() == 3) {
                if (_subPaths.isEmpty()) {
                    path.add(_trimmedCoords.get(1));
                } else {
                    for (Point subPoint : _subPaths) {
                        if ((curPoint.x != subPoint.x) && (curPoint.y != subPoint.y)) {
                            path.add(_trimmedCoords.get(1));
                            curPoint = _trimmedCoords.get(1);
                        }
                        path.add(subPoint);
                    }
                }
            }

            path.add(_trimmedCoords.get(_trimmedCoords.size() - 1));

        }

//        System.out.println("Animationspfad" + path);
        return path;
    }
}
