/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
public class ElementConstant extends ElementControlSingleOutput {

    @ACCESSIBLE
    public final ModelMVC<Double> constant = new ModelMVC<Double>(1.0,"const");

    public ElementConstant(CircuitSheet sheet) {
        super(sheet);
        _baseName = "const";

        constant.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value0 = constant.getValue();
            }
        });
    }    

    @Override
    public final void calculateOutput(double time, double dt) {
        _termOut.writeValue(value0, time);
    }

    public void stepBack() { }

}
