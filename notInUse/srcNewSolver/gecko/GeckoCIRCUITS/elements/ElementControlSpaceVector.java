/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.DatenSpeicher;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import java.util.ArrayList;

/**
 *
 * @author andy
 */
class ElementControlSpaceVector extends ElementControl {

    private ArrayList<TerminalControlIn> terminals = new ArrayList<TerminalControlIn>();
    private SpaceVectorDisplay svd = new SpaceVectorDisplay(this);

    private double[][] svdBuffer;
    private int bufferSize = 2;
    private boolean bufferFilled = false;
    private int buffered = 0;

    public ElementControlSpaceVector(CircuitSheet sheet) {
        super(sheet);
        svd.setVisible(true);

        terminals.add(new TerminalControlIn(-2, -1, this));
        terminals.add(new TerminalControlIn(-2, 0, this));
        terminals.add(new TerminalControlIn(-2, 1, this));

        terminals.add(new TerminalControlIn(-2, 2, this));
        terminals.add(new TerminalControlIn(-2, 3, this));
        terminals.add(new TerminalControlIn(-2, 4, this));

        terminals.add(new TerminalControlIn(-2, 5, this));
        terminals.add(new TerminalControlIn(-2, 6, this));
        terminals.add(new TerminalControlIn(-2, 7, this));

        _terminals.addAll(terminals);

        svdBuffer = new double[bufferSize][_terminals.size()+1];
    }

    @Override
    public ControlType getType() {
        return ControlType.SINK;
    }

    @Override
    public void calculateOutput(double time, double dt) {
        if (!_model.testStep) {

            if (!_model.saveHistory) {
                svd.drawVector(time, terminals); }
            else
            {
                if ((time>0) && !stepped_back)
                {
                    for (int i = 1; i < bufferSize; i++)
                        System.arraycopy(svdBuffer[i], 0, svdBuffer[i-1], 0, svdBuffer[i].length);
                }

                svdBuffer[bufferSize - 1][0] = time;
                for (int j = 1; j < _terminals.size()+2; j++)
                    svdBuffer[bufferSize - 1][j] = ((TerminalControlIn) _terminals.get(j-1)).getValue();

                if (!bufferFilled) {
                    buffered++;

                    if (buffered == bufferSize)
                        bufferFilled = true; }

                if (bufferFilled && !stepped_back)
                    svd.drawVector(svdBuffer[0][0], svdBuffer[0]);
                else if (stepped_back)
                    stepped_back = false;
            }
        }
    }

    @Override
    protected void importASCII_Individual(String[] ascii) {
        for (int i1 = 0; i1 < ascii.length; i1++) {

            if (ascii[i1].startsWith("scale1")) {
                svd.jSpinnerLength1.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }
            if (ascii[i1].startsWith("scale2")) {
                svd.jSpinnerLength2.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }
            if (ascii[i1].startsWith("scale3")) {
                svd.jSpinnerLength3.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }

            if (ascii[i1].startsWith("average1")) {
                svd.jSpinnerAverage1.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }
            if (ascii[i1].startsWith("average2")) {
                svd.jSpinnerAverage2.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }
            if (ascii[i1].startsWith("average3")) {
                svd.jSpinnerAverage3.setValue(new Float(DatenSpeicher.leseASCII_double(ascii[i1])));
            }
        }

    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    public void stepBack() {
        if (!_model.testStep)
            stepped_back = true;
    }
}
