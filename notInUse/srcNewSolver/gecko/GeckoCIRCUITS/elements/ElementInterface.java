/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.MatrixSolver.SolverType;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminable;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import geckocircuitsnew.ACCESSIBLE;
import geckocircuitsnew.GuiAction;
import geckocircuitsnew.Model;
import geckocircuitsnew.Orientation;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.UUID;
import javax.swing.JLabel;

// mit Implementierung dieses Interface koennen die Klassen '' und '' eine Verknuepfunge mit 'Verbindung'
// erstellen und eine entsprechende Netzliste erzweugen:
public abstract class ElementInterface extends JLabel {

    public static final ArrayList<ElementInterface> allElements = new ArrayList<ElementInterface>();

    @ACCESSIBLE 
    public final ModelMVC<String> _elementName = new ModelMVC<String>("");
    public final ArrayList<Terminal> _terminals = new ArrayList<Terminal>();
    protected Orientation _orientation = Orientation.SOUTH;
    // pixel size on the sheet
    public final ModelMVC<Integer> x = new ModelMVC<Integer>(-1);
    public final ModelMVC<Integer> y = new ModelMVC<Integer>(-1);
    private UUID _uuid;
    protected CircuitSheet _circuitSheet;
    protected Model _model; //associate each element with a Model (Andrija)
    public final ModelMVC<Boolean> enabled = new ModelMVC<Boolean>(true);
    protected int _boundsWidth = 100;
    protected int boundsHeight = 100;
    protected Shape containShape = new Rectangle(_boundsWidth / 2, boundsHeight / 2, 50, 50);
    protected final GuiAction _guiAction;
    protected String[] _parameterString;

    protected void setParameterString(String[] parameterString) {
        _parameterString = parameterString;
    }

    @Override
    public boolean contains(Point p) {
        return contains(p.x, p.y);
    }

    @Override
    public boolean contains(int ptx, int pty) {
        int dpix = CircuitSheet._dpix.getValue();
        //Shape containShape = new Rectangle(x.getValue()*dpix-5, y.getValue()*dpix-5, 10, 1);
        boolean returnValue = containShape.contains(ptx, pty);

        return returnValue;
    }

    
    @Override
    public String getName() {
        return _elementName.getValue();
    }
    

    
    protected ElementInterface(CircuitSheet sheet) {
        allElements.add(this);
        _circuitSheet = sheet;
        _model = sheet.sheetModel; //set this element's model to be that of the sheet it belongs to (Andrija)
        //add element to sheet in element constructor (Andrija)
        sheet.add(this);
        
        _model.modelElements.add(this); //add element to associated model's element list (Andrija)
        _uuid = UUID.randomUUID();

        
        final ElementInterface athis = this;
        ActionListener boundsListener = new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                setBounds(x.getValue() * CircuitSheet._dpix.getValue() - _boundsWidth / 2,
                        y.getValue() * CircuitSheet._dpix.getValue() - boundsHeight / 2, _boundsWidth, boundsHeight);

            }
        };

        x.addModelListener(boundsListener);
        y.addModelListener(boundsListener);

        CircuitSheet._dpix.addModelListener(boundsListener);

        _guiAction = new GuiAction(this);
        this.addMouseListener(_guiAction);
        this.addMouseMotionListener(_guiAction);
        
    }
       
    

    protected void setPaintColors() {
        if (this instanceof Terminable) {
            Terminable terminable = (Terminable) this;
            switch (terminable.getConnectionType()) {
                case POWERCIRCUIT:
                    Typ.colorBackGroundElement = Typ.farbeElementLKHintergrund;
                    Typ.colorForeGroundElement = Typ.farbeFertigElementLK;
                    Typ.colorConnection = Typ.farbeFertigVerbindungLK;
                    break;
                case RELUCTANCE:
                    Typ.colorBackGroundElement = Typ.colorReluctanceBackground;
                    Typ.colorForeGroundElement = Typ.colorForeGroundElement;
                    Typ.colorConnection = Typ.colorForeGroundElement;
                    break;
                case THERMALCIRCUIT:
                    Typ.colorForeGroundElement = Typ.farbeFertigElementTHERM;
                    Typ.colorBackGroundElement = Typ.farbeElementTHERMHintergrund;
                    Typ.colorConnection = Typ.farbeFertigVerbindungTHERM;
                    break;
                case CONTROL:
                    Typ.colorForeGroundElement = Typ.farbeFertigVerbindungCONTROL;
                    Typ.colorBackGroundElement = Typ.farbeElementCONTROLHintergrund;
                    Typ.colorConnection = Typ.farbeFertigVerbindungCONTROL;
                    break;
                default:
                    assert false;
            }
        }

    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        AffineTransform oldTransform = g2d.getTransform();
        g2d.translate(_boundsWidth / 2, boundsHeight / 2);
        g2d.rotate(_orientation.getRotationAngle());

        setPaintColors();
        
        paint(g2d, CircuitSheet._dpix.getValue());
        g2d.setTransform(oldTransform);

    }

    public void paint(Graphics2D g, int scaling) {
        setPaintColors();
        if (_guiAction.getModus() == GuiAction.ElementMode.IN_EDIT) {
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 2 * 0.1f));
        }

        paintTerminals(g, scaling);

        AffineTransform oldtransform = g.getTransform();

        g.setFont(new Font("Arial", Font.PLAIN, scaling - 1));
        FontRenderContext frc = ((Graphics2D) g).getFontRenderContext();
        g.rotate(-_orientation.getRotationAngle(), x.getValue(), y.getValue());
        g.drawString(_elementName.getValue(), 1 * scaling, -1 * scaling);
        
        g.setTransform(oldtransform);
    }

    public void paintTerminals(Graphics2D g, int scaling) {

        for (Terminal term : _terminals) {
            term.paint(g, scaling);
        }
    }

    public void setSheet(CircuitSheet sheet) {
        if (_circuitSheet != null) {
            _circuitSheet.remove(this);
        }

        _circuitSheet = sheet;
        _circuitSheet.add(this);
    }

    public CircuitSheet getSheet() {
        return _circuitSheet;
    }

    public String getPeecLabelAnfangsKnoten(int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getPeecLabelEndKnoten(int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setPeecLabelAnfangsKnoten(String q, int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setPeecLabelEndKnoten(String q, int knotenIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Orientation getOrientation() {
        return _orientation;
    }

    protected abstract void importASCII_Individual(String asciiBlock[]);

    protected abstract void setParameter(double[] parameter);

    public void setOrientation(Orientation orientation) {
        _orientation = orientation;
    }

    public UUID getUUID() {
        return _uuid;
    }

    void setNodeLabels(String[] labelAnfangsKnoten, String[] labelEndKnoten) {
        if (labelEndKnoten.length == 1 && labelEndKnoten.length == 1) {
            if (_terminals.size() == 2) {
                _terminals.get(0).setLabel(labelAnfangsKnoten[0]);
                _terminals.get(1).setLabel(labelEndKnoten[0]);
            }
        }
    }
    
    //return the solver associated with this model
    public SolverType getSolverType() {
        return _model.solverType.getValue();
    }
}
