/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CurrentSource;
import gecko.geckocircuitsnew.circuit.TimeFunction;
import gecko.geckocircuitsnew.circuit.TimeFunctionConstant;
import gecko.geckocircuitsnew.circuit.TimeFunctionSine;
import gecko.geckocircuitsnew.circuit.AbstractVoltageSource;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Color;
import java.awt.Graphics2D;
import geckocircuitsnew.ACCESSIBLE;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
public class ElementCurrentSource extends ElementCircuit {

    @ACCESSIBLE
    public final ModelMVC<Double> currentAmplitude = new ModelMVC<Double>(10.0,"iMAX","A");
    @ACCESSIBLE
    public final ModelMVC<Double> currentDC = new ModelMVC<Double>(20.0,"IDC","A");
    @ACCESSIBLE
    public final ModelMVC<Double> frequency = new ModelMVC<Double>(50.0,"f","Hz");
    @ACCESSIBLE
    public final ModelMVC<Double> offset = new ModelMVC<Double>(0.0,"offset","A");
    @ACCESSIBLE
    public final ModelMVC<Double> phase = new ModelMVC<Double>(0.0,"phase","\u00B0");
    public final CurrentSource currentSource;
    private TimeFunctionConstant _const_function = new TimeFunctionConstant(20);
    private TimeFunctionSine _sin_function = new TimeFunctionSine(10,50,0,0);
    public TimeFunction function = _const_function;
    private int _sourceType;

    public ElementCurrentSource(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        currentSource = new CurrentSource(_terminals.get(0), _terminals.get(1), this);
        circuitComponents.add(currentSource);

        currentAmplitude.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._amplitude = currentAmplitude.getValue();
            }
        });

        frequency.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._freq = frequency.getValue();
            }
        });

        offset.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._offset = offset.getValue();
            }
        });

        phase.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._phase = phase.getValue();
            }
        });

        currentDC.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _const_function._value = currentDC.getValue();
            }
        });
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {


        int typInt = (int) parameter[0];
        _sourceType = typInt;

        switch (typInt) {
            case Typ.QUELLE_DC:
                _const_function = new TimeFunctionConstant(parameter[1]);
                function = _const_function;
                currentDC.setValue(parameter[1]);
                break;
            case Typ.QUELLE_SIN:
                _sin_function = new TimeFunctionSine(parameter[1], parameter[2], parameter[3], parameter[4]);
                function = _sin_function;
                currentAmplitude.setValue(parameter[1]);
                frequency.setValue(parameter[2]);
                offset.setValue(parameter[3]);
                phase.setValue(parameter[4]);
                break;
            case Typ.QUELLE_SIGNALGESTEUERT:
                System.err.println("current source from signal not yet implemented");
                break;
            default:
                assert false;
        }

        currentSource.setFunction(function);
        

    }

    public int getSourceType()
    {
        return _sourceType;
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {
        double lg = 2.0;
        double br = 0.9;

        double iq1 = 0.8 * br, iq2 = 0.4 * br, ir1 = 0.7 * br, ir2 = 0.1 * br;
        g.setColor(Typ.colorForeGroundElement);

        g.drawLine(0, (int) (dpix * (+lg)), 0, (int) (dpix * (-lg)));
        g.setColor(Typ.colorBackGroundElement);
        g.fillOval((int) (dpix * (-br)), (int) (dpix * (-br)), (int) (dpix * (2 * br)), (int) (dpix * (2 * br)));
        g.setColor(Typ.colorForeGroundElement);
        g.drawOval((int) (dpix * (-br)), (int) (dpix * (-br)), (int) (dpix * (2 * br)), (int) (dpix * (2 * br)));
        g.fillPolygon(new int[]{(int) (dpix * (0)), (int) (dpix * (+iq2)), (int) (dpix * (-iq2))}, new int[]{(int) (dpix * (+iq1)), 0, 0}, 3);
        g.fillRect((int) (dpix * (-ir2) + 1), (int) (dpix * (-ir1) + 1), (int) (dpix * (2 * ir2)), (int) (dpix * (ir1)));

    }

    public void setFunction(TimeFunction func) {
        function = func;
        currentSource.setFunction(function);
    }

    public TimeFunction getTimeFunction() {
        return function;
    }

}
