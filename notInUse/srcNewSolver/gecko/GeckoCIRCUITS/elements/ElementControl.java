/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.ConnectionType;
import gecko.GeckoCIRCUITS.terminal.Terminable;
import geckocircuitsnew.DatenSpeicher;
import geckocircuitsnew.Orientation;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOptionalIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author andy
 */
public abstract class ElementControl extends ElementInterface implements Terminable {

    public ElementControl(CircuitSheet sheet) {
        super(sheet);
    }

    public static enum ControlType {

        SOURCE,
        SINK,
        TRANSFER
    };
    
    protected String _baseName = "";
    protected double br = 1.5, ho = 0.5, da = 0.5;
    protected int dm = 5, d = 5;
    protected double startRectX = -1.5;
    protected double stopRectX = 1.5;
    protected double startRectY = -0.5;
    protected double stopRectY = 0.5;

    //for stepping back in history
    protected double prev_time = -1;
    protected boolean stepped_back = false;
    //double array to hold history - previous two steps
    protected double[][] var_history;
    protected int steps_saved = 2;
    protected int steps_reversed = 0;

    public abstract ControlType getType();

    public static ElementInterface fabric(String txt, CircuitSheet sheet) {
        StringTokenizer stk = null;
        String[] ascii = DatenSpeicher.makeStringArray(txt);

        Orientation orientation = Orientation.SOUTH;
        int typ = -1;
        int tmpX = 10;
        int tmpY = 10;

        double[] parameter = null;
        String[] parameterString = null;
        String tmpName = "";

        String[] labelAnfangsKnoten = null;
        String[] labelEndKnoten = null;

        for (String zeile : ascii) {
            if (zeile.startsWith("typ ")) {
                typ = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("orientierung ")) {
                orientation = Orientation.getOri(DatenSpeicher.leseASCII_int(zeile));
            } else if (zeile.startsWith("x ")) {
                tmpX = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("y ")) {
                tmpY = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("parameter[] ")) {
                parameter = DatenSpeicher.leseASCII_doubleArray1(zeile);
            } else if (zeile.startsWith("idStringDialog")) {
                tmpName = DatenSpeicher.leseASCII_String(zeile);
            } else if (zeile.startsWith("parameterString[] ")) {
                parameterString = DatenSpeicher.leseASCII_StringArray1(zeile);
            } else if (zeile.startsWith("labelAnfangsKnoten[] ")) {
                labelAnfangsKnoten = DatenSpeicher.leseASCII_StringArray1(zeile);
            } else if (zeile.startsWith("labelEndKnoten[] ")) {
                labelEndKnoten = DatenSpeicher.leseASCII_StringArray1(zeile);
            }
        }


        ElementInterface returnValue = null;

        switch (typ) {
            case C_VOLTMETER:
                returnValue = new ElementVoltMeter(sheet);
                break;
            case C_AMPMETER:
                returnValue = new ElementAmperemeter(sheet);
                break;
            case C_SCOPE:
                ElementScope scope = new ElementScope(sheet);
                returnValue = scope;
                break;
            case C_SIGNALSOURCE:
                returnValue = new ElementSignalSource(sheet);
                break;
            case C_SWITCH:
                returnValue = new ElementGate(sheet);
                break;
            case C_CONST:
                returnValue = new ElementConstant(sheet);
                break;
            case C_DELAY:
                returnValue = new ElementDelay(sheet);
                break;
            case C_MUL:
                returnValue = new ElementControlMult(sheet);
                break;
            case C_NOT:
                returnValue = new ElementControlNot(sheet);
                break;
            case C_ABS:
                returnValue = new ElementControlAbs(sheet);
                break;
            case C_SUB:
                returnValue = new ElementControlSub(sheet);
                break;
            case C_ADD:
                returnValue = new ElementControlAdd(sheet);
                break;
            case C_GAIN:
                returnValue = new ElementControlGain(sheet);
                break;
            case C_PT1:
                returnValue = new ElementControlPT1(sheet);
                break;
            case C_HYS:
                returnValue = new ElementControlHysteresis(sheet);
                break;
            case C_GE:
                returnValue = new ElementControlGreaterEqual(sheet);
                break;
            case C_COUNTER:
                returnValue = new ElementControlCounter(sheet);
                break;
            case C_AND:
                returnValue = new ElementControlAnd(sheet);
                break;
            case C_OR:
                returnValue = new ElementControlOr(sheet);
                break;
            case C_SQR:
                returnValue = new ElementControlSQR(sheet);
                break;
            case C_GT:
                returnValue = new ElementControlGreaterThan(sheet);
                break;
            case C_INT:
                returnValue = new ElementControlIntegrator(sheet);
                break;
            case C_DIV:
                returnValue = new ElementControlDivision(sheet);
                break;
            case S_TEXTFIELD:
                returnValue = new TextFieldBlock(sheet);
                break;
            case C_SPACE_VECTOR:
                returnValue = new ElementControlSpaceVector(sheet);
                break;
            case C_ThyrControl:
                returnValue = new ElementControlThyristorSync(sheet);
                break;
            case C_PI:
                returnValue = new ElementControlPI(sheet);
                break;
            case C_LIMIT:
                returnValue = new ElementControlLimiter(sheet);
                break;
            case C_JAVA_FUNCTION:
                returnValue = new ElementControlJava(sheet);
                break;
            case C_PD:
                returnValue = new ElementControlPD(sheet);
                break;
            case C_FLOW:
                returnValue = new ElementAmperemeter(sheet);
                break;
            case C_TEMP:
                returnValue = new ElementVoltMeter(sheet);
                break;
            case C_SPARSEMATRIX:
                returnValue = new ElementControlSparseMatrix(sheet);
                break;
            case C_XOR:
                returnValue = new ElementControlXOr(sheet);
                break;
            case C_SIGN:
                returnValue = new ElementControlSignum(sheet);
                break;
            default:
                System.out.println("unknown control type " + typ);
        }

        if (returnValue != null) {

            returnValue._elementName.setValue(tmpName);
            returnValue.x.setValue(tmpX);
            returnValue.y.setValue(tmpY);
            returnValue.setParameter(parameter);
            returnValue.setParameterString(parameterString);
            returnValue.setOrientation(orientation);
            returnValue.setSheet(sheet);
            returnValue.importASCII_Individual(ascii);
            returnValue.setNodeLabels(labelAnfangsKnoten, labelEndKnoten);

        }

        return returnValue;
    }

        

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paint(g, dpix);

        for (Terminal term : _terminals) {
            if (term instanceof TerminalControlOptionalIn) {
                continue;
            }
            if (term instanceof TerminalControlIn) {
                startRectX = term.getRelX() + 0.5;

            }
            if (term instanceof TerminalControlOut) {
                stopRectX = term.getRelX() - 0.5;

            }

            startRectY = Math.min(startRectY, term.getRelY() - 0.5);
            stopRectY = Math.max(stopRectY, term.getRelY() + 0.5);
        }

        g.setColor(Typ.farbeElementCONTROLHintergrund);
        g.fillRect((int) (dpix * (startRectX)), (int) (dpix * startRectY), (int) (dpix * (stopRectX - startRectX)),
                (int) (dpix * (stopRectY - startRectY)));

        g.setColor(Typ.farbeFertigElementCONTROL);
        g.drawRect((int) (dpix * (startRectX)), (int) (dpix * startRectY), (int) (dpix * (stopRectX - startRectX)), (int) (dpix * (stopRectY - startRectY)));

        if (!_baseName.isEmpty()) {
            paintElementString(g, dpix, _baseName);
        }

    }

    void paintElementString(Graphics2D g, int dpix, String string) {
        g.setFont(new Font("Arial", Font.PLAIN, (int) (dpix * 0.75)));
        g.drawString(string, (int) (dpix * startRectX + d), (int) (dpix / 2 - 2));
    }
    
    public void init() { }

    public abstract void calculateOutput(double time, double dt);
    // verfuegbare CONTROL-Elemente
    private static final int C_VOLTMETER = 1;
    private static final int C_AMPMETER = 2;
    private static final int C_CONST = 3;
    private static final int C_SIGNALSOURCE = 4;
    private static final int C_SCOPE = 5;
    private static final int C_SWITCH = 6;
    private static final int C_GAIN = 7;
    private static final int C_PT1 = 8;
    private static final int C_PT2 = 9;
    private static final int C_PI = 10;
    private static final int C_HYS = 11;
    private static final int C_ADD = 12;
    private static final int C_SUB = 13;
    private static final int C_MUL = 14;
    private static final int C_DIV = 15;
    private static final int C_TEMP = 16;
    private static final int C_FLOW = 17;
    private static final int C_NOT = 18;
    private static final int C_AND = 19;
    private static final int C_OR = 20;
    private static final int C_XOR = 21;
    private static final int C_TO_EXTERNAL = 22;
    private static final int C_FROM_EXTERNAL = 23;
    private static final int C_DIGSOURCE = 24;
    private static final int C_DELAY = 25;
    private static final int C_SAMPLEHOLD = 26;
    private static final int C_LIMIT = 27;
    private static final int C_PD = 29;
    private static final int C_ABS = 32;
    private static final int C_ROUND = 33;
    private static final int C_SIN = 34;
    private static final int C_ASIN = 35;
    private static final int C_COS = 36;
    private static final int C_ACOS = 37;
    private static final int C_TAN = 38;
    private static final int C_ATAN = 39;
    private static final int C_EXP = 40;
    private static final int C_LN = 41;
    private static final int C_SQR = 42;
    private static final int C_SQRT = 43;
    private static final int C_POW = 44;
    private static final int C_GE = 45;
    private static final int C_GT = 46;
    private static final int C_EQ = 47;
    private static final int C_NE = 48;
    private static final int C_MIN = 49;
    private static final int C_MAX = 50;
    private static final int C_SIGN = 51;
    private static final int C_PWM = 52;
    private static final int C_COUNTER = 53;
    private static final int C_LOGIK = 54;
    private static final int C_SETSTATE = 55;
    private static final int C_STATEVAR = 56;
    private static final int C_IFTHEN = 57;
    private static final int C_TIME = 58;
    private static final int C_SPARSEMATRIX = 59;
    private static final int C_CISPR16 = 60;
    private static final int C_JAVA_FUNCTION = 61;
    private static final int C_VIEWMOT = 62;
    private static final int C_SPACE_VECTOR = 63;
    private static final int C_INT = 64;
    private static final int C_ABCDQ = 65;
    private static final int C_DQABC = 66;
    private static final int S_TEXTFIELD = 70;
    private static final int C_SCOPE2 = 71;
    private static final int C_ThyrControl = 72;

    void setNodeLabels(String[] labelAnfangsKnoten, String[] labelEndKnoten) {

        ArrayList<TerminalControlIn> inputTerminals = new ArrayList<TerminalControlIn>();
        ArrayList<TerminalControlOut> outputTerminals = new ArrayList<TerminalControlOut>();

        for (Terminal term : _terminals) {
            if (term instanceof TerminalControlIn) {
                inputTerminals.add((TerminalControlIn) term);
            }

            if (term instanceof TerminalControlOut) {
                outputTerminals.add((TerminalControlOut) term);
            }
        }

        int i = 0;
        for (TerminalControlIn in : inputTerminals) {
            if (!labelAnfangsKnoten[i].isEmpty()) {
                in.setLabel(labelAnfangsKnoten[i]);
            }
            i++;
        }

        i = 0;
        for (TerminalControlOut out : outputTerminals) {
            if (!labelEndKnoten[i].isEmpty()) {
                out.setLabel(labelEndKnoten[i]);
            }
            i++;
        }

    }

    public ArrayList<Terminal> getTerminals() {
        return _terminals;
    }

    public ConnectionType getConnectionType() {
        return ConnectionType.CONTROL;
    }

    //to when going forward a timestep: copy the data of the previous timestep into the data from 2 previous timesteps ago
    protected void historyForward()
    {
      for (int j = var_history.length - 1; j > 0; j--)
        for (int i = 0; i < var_history[0].length; i++)
            var_history[j][i] = var_history[j-1][i];
    }

    public abstract void stepBack();

    protected void historyBackward()
    {
      for (int j = var_history.length - 1; j > 0; j--)
        for (int i = 0; i < var_history[0].length; i++)
            var_history[j-1][i] = var_history[j][i];
    }
    
    
    
    //for setting states for steady-state computation
    
    public boolean hasInternalStates() {
        return false; //default is false - implement otherwise in child classes if there are internal states (e.g. PI, PD blocks)
    }
    
    public double[] getState() {
        return null;
    }
    
    public void setState(double[] state) {  
    }
    
    public double getStateVar(int index) {
        return 0;
    }
    
    public void setStateVar(double value, int index) {   
    }
    
    public double getStateVarMaxAbs(int index) {
        return 0;
    }
}
