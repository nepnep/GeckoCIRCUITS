/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.Capacitor;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.Resistor;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalInvisibleSubCircuit;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class ElementLISN extends ElementSubCircuit {

    public ElementLISN(CircuitSheet sheet) {
        super(sheet);
        // TODO fix the following code*
//        _terminals.add(new TerminalComponent(this, -3, -1));
//        _terminals.add(new TerminalComponent(this, -3, -0));
//        _terminals.add(new TerminalComponent(this, -3, 1));
//
//        _terminals.add(new TerminalComponent(this, -2, 3));
//
//        Terminal termA = new TerminalComponent(this, -1, 3);
//        Terminal termB = new TerminalComponent(this, 0, 3);
//        Terminal termC = new TerminalComponent(this, 1, 3);
//
//        _terminals.add(termA);
//        _terminals.add(termB);
//        _terminals.add(termC);
//
//        _terminals.add(new TerminalComponent(this, 2, -1));
//        _terminals.add(new TerminalComponent(this, 2, 0));
//        _terminals.add(new TerminalComponent(this, 2, 1));
//
//
//        Terminal R1T = new TerminalInvisibleSubCircuit(sheet);
//        Terminal S1T = new TerminalInvisibleSubCircuit(sheet);
//        Terminal T1T = new TerminalInvisibleSubCircuit(sheet);
//
//        _terminals.add(R1T);
//        _terminals.add(S1T);
//        _terminals.add(T1T);
//
//        InductorCoupling R1Ind = new InductorCoupling(_terminals.get(0), R1T);
//        InductorCoupling S1Ind = new InductorCoupling(_terminals.get(0), S1T);
//        InductorCoupling T1Ind = new InductorCoupling(_terminals.get(0), T1T);
//
//        double leftInductance = 50e-6;
//        R1Ind.setInductance(leftInductance);
//        S1Ind.setInductance(leftInductance);
//        T1Ind.setInductance(leftInductance);
//
//        Terminal R2T = new TerminalInvisibleSubCircuit(sheet);
//        Terminal S2T = new TerminalInvisibleSubCircuit(sheet);
//        Terminal T2T = new TerminalInvisibleSubCircuit(sheet);
//
//        _terminals.add(R2T);
//        _terminals.add(S2T);
//        _terminals.add(T2T);
//
//        InductorCoupling R2Ind = new InductorCoupling(R2T, R1T);
//        InductorCoupling S2Ind = new InductorCoupling(S2T, S1T);
//        InductorCoupling T2Ind = new InductorCoupling(T2T, T1T);
//
//        double rightInductance = 300e-9;
//        R2Ind.setInductance(rightInductance);
//        S2Ind.setInductance(rightInductance);
//        T2Ind.setInductance(rightInductance);
//
//        Resistor RRes = new Resistor(R2T, _terminals.get(7));
//        Resistor SRes = new Resistor(S2T, _terminals.get(8));
//        Resistor TRes = new Resistor(T2T, _terminals.get(9));
//
//        double rightRes = 20e-3;
//        RRes.setResistance(rightRes);
//        SRes.setResistance(rightRes);
//        TRes.setResistance(rightRes);
//
//        _terminals.add(termA);
//        _terminals.add(termB);
//        _terminals.add(termC);
//
//        double capValue = 250e-9;
//        Capacitor capA = new Capacitor(termA, R1T);
//        Capacitor capB = new Capacitor(termB, S1T);
//        Capacitor capC = new Capacitor(termC, T1T);
//
//        capA.setCapacitance(capValue);
//        capB.setCapacitance(capValue);
//        capC.setCapacitance(capValue);
//
//        Resistor resGNDA = new Resistor(termA, _terminals.get(3));
//        Resistor resGNDB = new Resistor(termB, _terminals.get(3));
//        Resistor resGNDC = new Resistor(termC, _terminals.get(3));
//
//        double gndResistance = 50;
//        resGNDA.setResistance(gndResistance);
//        resGNDB.setResistance(gndResistance);
//        resGNDC.setResistance(gndResistance);
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {

    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int scaling) {

        g.drawRect(0, 0, 10, 10);
        
    }



}
