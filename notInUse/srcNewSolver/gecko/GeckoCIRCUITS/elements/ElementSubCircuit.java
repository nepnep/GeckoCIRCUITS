/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public abstract class ElementSubCircuit extends ElementCircuit {
    private double startRectX = 100;
    private double stopRectX = -100;
    private double startRectY = 100;
    private double stopRectY = -100;

    public ElementSubCircuit(CircuitSheet sheet) {
        super(sheet);
    }




    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paint(g, dpix);

        for(Terminal term : _terminals) {
            if(term instanceof TerminalComponent) {
                startRectX = Math.min(term.getRelX()+0.5, startRectX);
                stopRectX = Math.max(term.getRelX()-0.5, stopRectX);
                startRectY = Math.min(startRectY, term.getRelY()-0.5);
                stopRectY = Math.max(stopRectY, term.getRelY()-0.5);
            }

        }

        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillRect((int)(dpix*(startRectX)),(int)(dpix*startRectY), (int)(dpix*(stopRectX-startRectX)),
                (int)(dpix*(stopRectY - startRectY)));

        g.setColor(Typ.farbeFertigElementCONTROL);
        g.drawRect((int)(dpix*(startRectX)),(int)(dpix*startRectY), (int)(dpix*(stopRectX-startRectX)),(int)(dpix*(stopRectY - startRectY)));

    }

}
