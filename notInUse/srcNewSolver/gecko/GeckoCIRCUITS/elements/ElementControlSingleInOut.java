/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;

/**
 *
 * @author andy
 */
public abstract class ElementControlSingleInOut extends ElementControl {

    protected double value0;
    protected double value1;
    protected double value2;

    protected TerminalControlOut _termOut = new TerminalControlOut(2, 0, this);
    protected TerminalControlIn _termIn = new TerminalControlIn(-2, 0, this);

    public ElementControlSingleInOut(CircuitSheet sheet) {
        super(sheet);
        _terminals.add(_termOut);
        _terminals.add(_termIn);
        _termOut.setRelX(1);
    }

    @Override
    public ControlType getType() {
        return ControlType.TRANSFER;
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {

    }

    @Override
    protected void setParameter(double[] parameter) {
        if(parameter.length > 0) value0 = parameter[0];
        if(parameter.length > 1) value1 = parameter[1];
        if(parameter.length > 2) value2 = parameter[2];
    }




}
