/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.ConnectionType;
import java.awt.Point;

/**
 *
 * @author andy
 */
public final class ElementConnectionCircuit extends ElementConnection {

    public ElementConnectionCircuit(final Point start, final Point stop, final CircuitSheet sheet) {
        super(start, stop, sheet);
    }

    public ElementConnectionCircuit(final String ascii, final CircuitSheet sheet) {
        super(ascii, sheet);
    }

    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.POWERCIRCUIT;
    }



}
