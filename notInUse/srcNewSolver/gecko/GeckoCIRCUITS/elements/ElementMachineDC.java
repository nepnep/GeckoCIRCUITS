/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.JPanelDCMachine;
import gecko.geckocircuitsnew.circuit.JPanelOPVDialog;
import gecko.geckocircuitsnew.circuit.VoltageSourceDCMachine;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

/**
 *
 * @author andy
 */
class ElementMachineDC extends ElementSubSheet {
    private final ElementResistor rA;
    private final ElementInductor lA;
    private final ElementResistor rE;
    private final ElementResistor rConBig;
    private final ElementInductor lE;
    private final ElementVoltage uInd;

    public final ModelMVC<Double> frictionCoefficient = new ModelMVC<Double>(100e-3);
    public final ModelMVC<Double> inertia_J = new ModelMVC<Double>(1.0);
    public final ModelMVC<Double> torque = new ModelMVC<Double>(0.0);
    public final ModelMVC<Double> Ne = new ModelMVC<Double>(100.0);
    public final ModelMVC<Double> cM = new ModelMVC<Double>(1.0);
    private final VoltageSourceDCMachine _udcmachine;

    public ElementMachineDC(CircuitSheet sheet) {
        super(sheet);
    _sheet._sheetSizeX.setValue(40);
        _sheet._sheetSizeY.setValue(20);

        _sheet.paintExtensionX = 100;

        JPanelDCMachine dialog = new JPanelDCMachine();

        int dialogHeight = dialog.getPreferredSize().height;
        _sheet.paintExtensionY = dialogHeight;
        int dialogWidth = dialog.getPreferredSize().width;
        dialog.setBounds(0,_sheet._sheetSizeY.getValue() * _sheet._dpix.getValue(),
                dialogWidth,dialogHeight);
        //dialog.setOpaque(false);
        _sheet.add(dialog);


        ElementTerminalCircuit anchor_n = new ElementTerminalCircuit(this, "A_n", 8, 12, 2, 0, _sheet);
        ElementTerminalCircuit anchor_p = new ElementTerminalCircuit(this, "A_p", 8, 8, -2, 0, _sheet);
        ElementTerminalCircuit erreger_n = new ElementTerminalCircuit(this, "E_n", 24, 12, -2, -1, _sheet);
        ElementTerminalCircuit erreger_p = new ElementTerminalCircuit(this, "E_n", 24, 8, -2, -2, _sheet);
        erreger_n.setOrientation(Orientation.NORTH);
        erreger_p.setOrientation(Orientation.NORTH);


        rA = new ElementResistor(_sheet);
        rA._elementName.setValue("Ra");
        rA.resistance.setValue(10e-3);
        rA.setOrientation(Orientation.WEST);
        rA.x.setValue(10);
        rA.y.setValue(8);
        _sheet.add(rA);

        lA = new ElementInductor(_sheet,true);
        lA._elementName.setValue("La");
        lA.setOrientation(Orientation.WEST);
        lA.inductance.setValue(2e-3);
        lA.x.setValue(14);
        lA.y.setValue(8);
        _sheet.add(lA);
        
        rConBig = new ElementResistor(_sheet);
        rConBig._elementName.setValue("RConn");
        rConBig.resistance.setValue(1E7);
        rConBig.setOrientation(Orientation.WEST);
        rConBig.x.setValue(18);
        rConBig.y.setValue(12);
        _sheet.add(rConBig);

        rE = new ElementResistor(_sheet);
        rE._elementName.setValue("Re");
        rE.resistance.setValue(20e-3);
        rE.setOrientation(Orientation.WEST);
        rE.x.setValue(22);
        rE.y.setValue(8);
        _sheet.add(rE);

        lE = new ElementInductor(_sheet,true);
        lE._elementName.setValue("Le");
        lE.inductance.setValue(5e-3);
        lE.x.setValue(20);
        lE.y.setValue(10);
        _sheet.add(lE);
        
        uInd = new ElementVoltage(_sheet, (InductorCoupling) lE.circuitComponents.get(0), (InductorCoupling) lA.circuitComponents.get(0));
        uInd._elementName.setValue("Uind");
        uInd.x.setValue(16);
        uInd.y.setValue(10);
        _sheet.add(uInd);

        _udcmachine = (VoltageSourceDCMachine) uInd.circuitComponents.get(0);
        
        ElementConnectionCircuit conA_n = new ElementConnectionCircuit(new Point(8,12), new Point(16,12), _sheet);
        _sheet.add(conA_n);

        ElementConnectionCircuit conE_n = new ElementConnectionCircuit(new Point(20,12), new Point(24,12), _sheet);
        _sheet.add(conE_n);

        frictionCoefficient.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _udcmachine.setFr(frictionCoefficient.getValue());
            }
        });

        inertia_J.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _udcmachine.setInertia(inertia_J.getValue());
            }
        });

        torque.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _udcmachine.setTorque(torque.getValue());
            }
        });

        Ne.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _udcmachine.setNe(Ne.getValue());
            }
        });

        cM.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _udcmachine.setCm(cM.getValue());
            }
        });

    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        //parameter= new double[]{ia,ie, omega,drehzahl, phi,emk, momentElektr, dummy1,dummy2, La,Ra,Le,Re, Ne,cM,Fr,J, momentLast, ia0,ie0,omega0};
        double ia = parameter[0];
        double ie = parameter[1];
        double omega = parameter[2];
        double drehzahl = parameter[3];
        double phi = parameter[4];
        double emk = parameter[5];
        double momentElektr = parameter[6];
        double dummy1 = parameter[7];
        double dummy2 = parameter[8];

        lA.inductance.setValue(parameter[9]);
        rA.resistance.setValue(parameter[10]);
        lE.inductance.setValue(parameter[11]);
        rE.resistance.setValue(parameter[12]);
        Ne.setValue(parameter[13]);
        cM.setValue(parameter[14]);

        System.out.println("para. " + parameter[13]);
        frictionCoefficient.setValue(parameter[15]);
        inertia_J.setValue(parameter[16]);

        lA.initCurrent.setValue(parameter[18]);
        lE.initCurrent.setValue(parameter[19]);


    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paintTerminals(g, dpix);

//-----------------
        double x1=-3;
        double y1=0;
        double x2=+3;
        double y2=0;
        double x3=-2;
        double y3=-3;
        double x4=-2;
        double y4=-2;

        double rM=1.5, hMx=2.0, hMy=0.35;
        boolean verwendeKleinesSymbol=true;

        if (verwendeKleinesSymbol) {
            x1=-2;   y1=0;   x2=+2;   y2=0;   x3=-2;    y3=-2;   x4=-2;   y4=-1;
            rM=1.0;   hMx=1.3;   hMy=0.2;
        }

        if (verwendeKleinesSymbol) {
            g.drawLine((int)(dpix*x1),(int)(dpix*y1), (int)(dpix*x2),(int)(dpix*y2));
            g.drawLine((int)(dpix*x3),(int)(dpix*y3), 0,(int)(dpix*y3));
            g.drawLine((int)(dpix*x4),(int)(dpix*y4), (int)(dpix*(-1)),(int)(dpix*y4));
            g.drawLine((int)(dpix*(-1)),(int)(dpix*(y4-0.5)), 0,(int)(dpix*(y4-0.5)));
            g.drawLine((int)(dpix*(-1)),(int)(dpix*y4), (int)(dpix*(-1)),(int)(dpix*(y4-0.5)));
            g.fillRect((int)(dpix*(-hMx)),(int)(dpix*(-hMy)), (int)(dpix*2*hMx),(int)(dpix*2*hMy));
            g.fillRect((int)(dpix*(-hMy)),(int)(dpix*y3), (int)(dpix*2*hMy),(int)(dpix*0.5));
            g.setColor(Typ.farbeFertigElementLK);
            g.drawRect((int)(dpix*(-hMx)),(int)(dpix*(-hMy)), (int)(dpix*2*hMx),(int)(dpix*2*hMy));
            g.drawRect((int)(dpix*(-hMy)),(int)(dpix*y3), (int)(dpix*2*hMy),(int)(dpix*0.5));
            g.setColor(Typ.farbeElementLKHintergrund);
            g.fillOval((int)(dpix*(-rM)),(int)(dpix*(-rM)), (int)(dpix*2*rM),(int)(dpix*2*rM));
            g.setColor(Typ.farbeFertigElementLK);
            g.drawOval((int)(dpix*(-rM)),(int)(dpix*(-rM)), (int)(dpix*2*rM),(int)(dpix*2*rM));
        } else {
            g.drawLine((int)(dpix*x1),(int)(dpix*y1), (int)(dpix*x2),(int)(dpix*y2));
            g.drawLine((int)(dpix*x3),(int)(dpix*y3), 0,(int)(dpix*y3));
            g.drawLine((int)(dpix*x4),(int)(dpix*y4), 0,(int)(dpix*y4));
            g.fillRect((int)(dpix*(-hMx)),(int)(dpix*(-hMy)), (int)(dpix*2*hMx),(int)(dpix*2*hMy));
            g.fillRect((int)(dpix*(-hMy)),(int)(dpix*y3), (int)(dpix*2*hMy),(int)(dpix*1));
            g.setColor(Typ.farbeFertigElementLK);
            g.drawRect((int)(dpix*(-hMx)),(int)(dpix*(-hMy)), (int)(dpix*2*hMx),(int)(dpix*2*hMy));
            g.drawRect((int)(dpix*(-hMy)),(int)(dpix*y3), (int)(dpix*2*hMy),(int)(dpix*1));
            g.setColor(Typ.farbeElementLKHintergrund);
            g.fillOval((int)(dpix*(-rM)),(int)(dpix*(-rM)), (int)(dpix*2*rM),(int)(dpix*2*rM));
            g.setColor(Typ.farbeFertigElementLK);
            g.drawOval((int)(dpix*(-rM)),(int)(dpix*(-rM)), (int)(dpix*2*rM),(int)(dpix*2*rM));
        }

    }


}
