/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.MutualCoupling;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.Orientation;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import geckocircuitsnew.OPTIMIZABLE;

/**
 *
 * @author andy
 */
public class ElementMutualInductance extends ElementCircuit {

    @OPTIMIZABLE
    private ModelMVC<Double> _k = new ModelMVC<Double>(0.99);
    public final ModelMVC<Boolean> showLines = new ModelMVC<Boolean>(false);

    public ElementMutualInductance(CircuitSheet sheet) {
        super(sheet);
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
        this.setOrientation(Orientation.SOUTH);
    }

    public MutualCoupling getMutualCoupling(ArrayList<ElementInterface> allElements) {

        InductorCoupling ind1 = null;
        InductorCoupling ind2 = null;


        for (ElementInterface element : allElements) {
            if (element._elementName.getValue().equals(_parameterString[0])) {
                ind1 = (InductorCoupling) ((ElementCircuit) element).circuitComponents.get(0);
            }

            if (element._elementName.getValue().equals(_parameterString[1])) {
                ind2 = (InductorCoupling) ((ElementCircuit) element).circuitComponents.get(0);
            }

        }

        assert ind1 != null;
        assert ind2 != null;

        return new MutualCoupling(ind1, ind2, _k.getValue());
    }

    @Override
    protected void setParameter(double[] parameter) {
        _k.setValue(parameter[0]);
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {
        double ho = 0.75;
        double br = 0.75;
        //-----------------
        double x1 = x.getValue();
        double y1 = y.getValue();
        double x2 = x.getValue();
        double y2 = y.getValue();  // keine spezielle Orientierung beim Kopplungselement
        //-----------------
        // Verbindungslinien zu den verkoppelten Induktivitaeten:
        //if (showLines.getValue()) {
        //    g.setColor(Color.green);
        //g.drawLine((int) (dpix * x), (int) (dpix * y), (int) (dpix * this.parameter[1]), (int) (dpix * this.parameter[2]));  // Verbindungslinie zu L1
        //g.drawLine((int) (dpix * x), (int) (dpix * y), (int) (dpix * this.parameter[3]), (int) (dpix * this.parameter[4]));  // Verbindungslinie zu L2
        //}

        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillRect((int) (dpix * (-br)), (int) (dpix * (-ho)), (int) (dpix * (2 * br)), (int) (dpix * (2 * ho)));
        g.setColor(Typ.farbeFertigElementLK);
        g.drawRect((int) (dpix * (-br)), (int) (dpix * (-ho)), (int) (dpix * (2 * br)), (int) (dpix * (2 * ho)));
        g.drawString("k", (int) (- 2), (int) (+5));


    }
}
