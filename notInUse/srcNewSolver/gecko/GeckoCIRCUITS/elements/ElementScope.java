/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.scope.Scope;
import gecko.GeckoCIRCUITS.scope.ScopeSettings;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.DatenSpeicher;
import geckocircuitsnew.GuiAction;
import geckocircuitsnew.Model;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import gecko.GeckoCIRCUITS.scope.DataContainer;
import gecko.GeckoCIRCUITS.scope.DataContainerCompressable;
import gecko.GeckoCIRCUITS.scope.DataContainerSimple;
import gecko.GeckoCIRCUITS.scope.DataContainerSimpleJunkable;
import gecko.GeckoCIRCUITS.scope.TimeSerie;
import gecko.GeckoCIRCUITS.scope.TimeSeriesArray;
import gecko.GeckoCIRCUITS.scope.TimeSeriesConstantDt;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Timer;
import java.util.TimerTask;
import java.util.LinkedList;

// Anzahl der Intervalle auf der x-Achse, in denen Hi- und Lo-Werte zwecks Datenkompression ermittelt werden

/**
 *
 * @author andy
 */
public class ElementScope extends ElementControl {

    public static final int MAX_SCOPE_RESOLUTION = 2000;
    private int tn = 4;
    private ScopeSettings scopeSettings;
    public Scope scope;
    private DataContainer zvDaten;
    private String[] header;
    private int zvCounter;
    private int zvCounterRAM;
    private int zvCounterDISK;
    private boolean initDone;
    private boolean doHiLoInScope;
    private int[] zvCounterRef;
    private double[] xHi;
    private double[] xLo;
    private double[] yHi;
    private double[] yLo;
    private DataContainer zvDatenRAM;
    private double reduktorRAM = 1;
    private int reduktorDISK;
    private String datnamDefault;
    private BufferedWriter fkaku;
    private boolean fkakuOpen = false;
    private StringBuffer sb;
    private boolean datenspeicherungIsActive = false;
    private TimeSerie _timeSeries;
    private double[] scopeBuffer;
    private double[] newDataToBuffer;
    private int bufferSize = 2;
    private boolean bufferFilled = false;
    private int buffered = 0;
    private int buffered_previously = 0;
    private int steps_to_write_to_scope = 1;
    private boolean newDataToPlot = false;
    private Timer timer = new Timer();
    private boolean useBuffer = false;
    private LinkedList<double[]> bufferList;

    public ElementScope(CircuitSheet sheet) {
        super(sheet);
        br = 0.5;
        da = 0.5;
        ho = 0.6;
        d = 4;
        dm = 5;

        for (int i = 0; i < tn; i++) {
            _terminals.add(new TerminalControlIn(-2, i, this));

            doHiLoInScope = true;
        }

        timer.schedule(new UpdateScopeTask(), 500, 500);



    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    public int getTerminalNumber() {
        return tn;
    }

    public DataContainer getZVDatenImRAM() {
        return zvDatenRAM;
    }
    
    @Override
    public void init() {
        initScope();
    }

    public void initScope() {
        // wird einmalig bei der SCOPE-Initialisierung aufgerufen
        // Referenzen fuer SCOPE werden angemeldet ...

        zvDaten = new DataContainerSimple(1 + tn, 2 + 2 * MAX_SCOPE_RESOLUTION);

        header = new String[1 + tn];
        for (int i1 = 0; i1 < header.length; i1++) {
            if (i1 == 0) {
                header[i1] = "t";
            } else {
                header[i1] = "sg." + i1;
            }
        }
        zvCounter = 0;
        zvCounterRAM = 0;
        zvCounterDISK = 0;
        initDone = false;
        zvCounterRef = new int[]{zvCounter};
        scope.setReferenzAufRegelBlock(this);
        scope.setzeSignalDaten(zvDaten, header, zvCounterRef);  // hier wird die Referenz auf die Daten einmalig zugewiesen
        scope.setTitle(Typ.spTitle + _elementName.getValue());
        xHi = new double[tn];
        xLo = new double[tn];
        yHi = new double[tn];
        yLo = new double[tn];  // Zwischenspeicher zur Datenkompression:

        //if (_model.saveHistory)
        scopeBuffer = new double[_terminals.size() + 2];
        newDataToBuffer = new double[_terminals.size() + 2];
        bufferFilled = false;
        buffered = 0;
        bufferList = new LinkedList();
        
        if (_model.saveHistory)
            useBuffer = true;
        else
            useBuffer = false;

    }

    public void setTerminalNumber(int tn) {
        this.tn = tn;
        if (_terminals.size() != tn) {
            while (_terminals.size() > tn) {
                _terminals.remove(_terminals.size() - 1);
            }

            while (_terminals.size() < tn) {
                _terminals.add(new TerminalControlIn(-2, _terminals.size(), this));
            }

            _circuitSheet.repaint();
        }

        int dpix = CircuitSheet._dpix.getValue();
        containShape = new Rectangle((int) (_boundsWidth / 2 - dpix * br), (int) (boundsHeight / 2 - dpix * (br + ho)),
                (int) (dpix * 2 * br), (int) (dpix * (tn + 2 * ho)));

    }

    @Override
    public void paint(Graphics2D g, int dpix) {

        if (_guiAction.getModus() == GuiAction.ElementMode.IN_EDIT) {
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 2 * 0.1f));
        }

        super.paintTerminals(g, dpix);

        g.setColor(Typ.farbeElementCONTROLHintergrund);
        g.fillRect((int) (-dpix * br), (int) (-dpix * br), (int) (dpix * 2 * br), (int) (dpix * (1.0 * tn)));

        g.setColor(Typ.farbeFertigElementCONTROL);
        g.drawRect((int) (dpix * (-br)), (int) (dpix * (-br)), (int) (dpix * (2 * br)), (int) (dpix * (1.0 * tn)));
        int di = 2;  // Innen-Rechteck
        g.drawRect((int) (dpix * (-br)) + di, (int) (dpix * (-br)) + 2 * di, (int) (dpix * (2 * br)) - 2 * di, (int) (dpix * (1.0 * tn)) - 4 * di);

        // Rote Dreiecke zum Klicken --> Aenderung der Terminal-Anzahl:
        g.setColor(Color.red);
        int delta = 3;  // Abstand vom roten Dreieck vom SCOPE-Block (nach oben bzw. nach unten)
        int xd0 = 0, xd1 = (int) (d), xd2 = (int) -d;

        int yp0 = (int) (dpix * (-br - ho) - delta), yp1 = (int) (dpix * (-br) - delta);
        int ym1 = (int) (dpix * (-br + tn) + delta), ym0 = (int) (dpix * (-br + tn + ho) + delta);
        g.fillPolygon(new int[]{xd0, xd1, xd2}, new int[]{yp0, yp1, yp1}, 3);
        g.fillPolygon(new int[]{xd0, xd1, xd2}, new int[]{ym0, ym1, ym1}, 3);

    }

    @Override
    protected void importASCII_Individual(String asciiBlock[]) {
        boolean[] detailOK = new boolean[1];
        for (int i1 = 0; i1 < detailOK.length; i1++) {
            detailOK[i1] = false;
        }
        //--------------------------        
        for (int i1 = 0; i1 < asciiBlock.length; i1++) {
            if (asciiBlock[i1].startsWith("tn ")) {
                setTerminalNumber(DatenSpeicher.leseASCII_int(asciiBlock[i1]));
                detailOK[0] = true;
            }

            if (asciiBlock[i1].startsWith("<scopeSettings>")) {
                String asciiSC = "";
                while (!asciiBlock[i1].startsWith("<\\scopeSettings>")) {
                    asciiSC += ("\n" + asciiBlock[i1]);
                    i1++;
                }
                scopeSettings = new ScopeSettings();
                scopeSettings.importASCII(asciiSC);
                scope = new Scope(scopeSettings);  // Scope wird auch gleich initialisiert

                initScope();

                scope.setVisible(true);  // vorerst unsichtbar


            }
        }
        //------------------
        boolean ladenOK = true;
        for (int i1 = 0; i1 < detailOK.length; i1++) {
            if (!detailOK[i1]) {
                ladenOK = false;
            }
        }

    }

    @Override
    public ControlType getType() {
        return ControlType.SINK;
    }
    int counter = 0;

    @Override
    public final void calculateOutput(double time, double dt) {
        //System.out.println("run scope: " + time);
        if (!_model.testStep) {
            //System.out.println("useBuffer: " + useBuffer + ": " + time);
            if (useBuffer) //in order to allow for stepping back when we reset to dt_min during a switching action, buffer scope input
            {
                //System.out.println("_model.saveHistory: " + _model.saveHistory + ": " + time);
                if (_model.saveHistory) //first control step executed, which is <= circuit step
                {
                    
                    //if it is the first step in the simulation, we do nothing
                    //if it is the second *circuit* step in the simulation, we now note that the buffer has been filled
                    //we know which step it is, because _model.saveHistory is true only during the first control step after a circuit step, i.e. once per circuit step
                    if (!bufferFilled && (buffered > 0))
                    {
                        bufferFilled = true;
                        buffered_previously = buffered;
                        buffered = 0;
                    }
                    //if some step beyond second *circuit* step, we simply reset counters
                    else if (bufferFilled)
                    {
                        //it is possible that previously, more steps were buffered than now (i.e. difference in circuit step)
                        //therefore at some point might need to empty out buffer into scope
                        if (buffered_previously > buffered)
                            steps_to_write_to_scope = buffered_previously - buffered + 1;
                        else
                            steps_to_write_to_scope = 1;
                        buffered_previously = buffered;
                        buffered = 0;
                     }
                    //System.out.println("bufferFilled: " + bufferFilled + ": " + time);
                }

                newDataToBuffer[0] = time;
                newDataToBuffer[1] = dt;
                for (int j = 2; j < _terminals.size() + 2; j++) {
                    newDataToBuffer[j] = ((TerminalControlIn) _terminals.get(j - 2)).getValue();
                }

                bufferList.add(newDataToBuffer);

                buffered++;
                }
            

            //zvCounterRAM = 0;
            //if(1 > 0) return;
//        System.out.print("scope: ");
//        for (Terminal term : _terminals) {
//            System.out.print(((TerminalControlIn) term).getValue() + " ");
//        }
//        System.out.println("");

            //if (dt==Typ.DT_INIT_SIMULINK) {  // eventuell problematisch beim Starten mit Simulink
            //   return ausgangssignal;
            //=======================
            // ZV-Punkte werden fuer die SCOPE-Darstellung komprimiert gespeichert -->
            //=======================
            if (!useBuffer || (useBuffer && bufferFilled && !stepped_back && (buffered <= buffered_previously))) {


                //in case we need to flush buffer as explained above
                for (int i = 0; i < steps_to_write_to_scope; i++) {
                
                

                if (useBuffer) {
                    //System.out.println("fetch from bufferList: " + time);
                    scopeBuffer = bufferList.remove();
                    time = scopeBuffer[0];
                    dt = scopeBuffer[1];
                }


                if (datenspeicherungIsActive) {
                    if (reduktorDISK == 1) {  // Daten passen unkomprimiert auf die Festplatte ...
                        sb = new StringBuffer("" + (float) time);
                        for (int i1 = 0; i1 < _terminals.size(); i1++) {
                            if (useBuffer) {
                                sb.append(" " + ((float) scopeBuffer[i1 + 2]));
                            } else {
                                sb.append(" " + (float) ((TerminalControlIn) _terminals.get(i1)).getValue());
                            }
                        }
                        sb.append("\n");
                        try {
                            fkaku.write(sb.toString());
                        } catch (Exception e) {
                            System.out.println(e + "  ouiuh890");
                        }
                    } else {
                        if ((int) (time / dt) % reduktorDISK == 0) {
                            zvCounterDISK++;
                            sb = new StringBuffer("" + (float) time);
                            for (int i1 = 0; i1 < _terminals.size(); i1++) {
                                if (useBuffer) {
                                    sb.append(" " + ((float) scopeBuffer[i1 + 2]));
                                } else {
                                    sb.append(" " + ((TerminalControlIn) _terminals.get(i1)).getValue());
                                }
                            }
                            sb.append("\n");
                            try {
                                fkaku.write(sb.toString());
                            } catch (Exception e) {
                                System.out.println(e + "  04gvnekkk0");
                            }
                        }
                    }
                }

                if (doHiLoInScope) {
                    // DATENKOMPRESSION fuer die SCOPE-Darstellung ->
                    // durch die Definition von xPixelNEU wird die an sich kontinuierliche x-Achse in diskrete Intervalle zerlegt
                    // in jedem dieser Intervalle wird der minimale und maximale y-Wert (yLo & yHi) ermittelt plus zugehoeriger x-Wert
                    // --> effizientere Darstellung der Kurve im SCOPE, Vermeidung von OUT_OF_MEMORY
                    // zu jeder Kurve gibt es pro Intervall einen yHi- und yLo-Werte, diese werden alle dem gleichen x-Werten xHi und xLo (gleichmaessig innerhalb dem Intervall) zugeteilt
                    //
                    // INIT -->
                    //-----------------
                    if (!initDone) {
                        initDone = true;
                        zvDatenRAM = null;
                        int arraySize = (int) ((_model.tEnd.getValue() - 0) / dt / reduktorRAM) + 5;

                        if (Typ.DO_COMPRESSION) {

                            zvDatenRAM = new DataContainerCompressable(1 + tn);
                        } else {
                            zvDatenRAM = new DataContainerSimpleJunkable(1 + tn);
                            //zvDatenRAM = new DataContainerSimple(1 + tn, arraySize);
                        }

                        //
                        zvDaten.setValue(0, 0, zvCounter);
                        for (int i1 = 0; i1 < tn; i1++) {
                            if (useBuffer) {
                                zvDaten.setValue(scopeBuffer[i1 + 2], i1 + 1, zvCounter);
                            } else {
                                zvDaten.setValue(((TerminalControlIn) _terminals.get(i1)).getValue(), i1 + 1, zvCounter);
                            }
                            yHi[i1] = zvDaten.getValue(i1 + 1, zvCounter);
                            yLo[i1] = zvDaten.getValue(i1 + 1, zvCounter);
                        }
                    }
                    //-----------------
                    double dtSCOPE = (_model.tEnd.getValue()) / MAX_SCOPE_RESOLUTION;
                    try {
                        if ((int) ((time - 0) / dtSCOPE) > (int) ((zvDaten.getValue(0, zvCounter) - 0) / dtSCOPE)) {
                            // neues Intervall beginnt mit neuen x- u. y-Werten, die noch bestimmt werden muessen -->
                            //-------------
                            // (0) Hi- und Lo-Werte des vorherigen Intervalls speichern
                            zvCounter++;
                            zvDaten.setValue((time + zvDaten.getValue(0, zvCounter - 1)) / 2, 0, zvCounter);
                            zvDaten.setValue(time, 0, zvCounter + 1);
                            for (int i1 = 0; i1 < tn; i1++) {
                                if (xLo[i1] < xHi[i1]) {
                                    zvDaten.setValue(yLo[i1], i1 + 1, zvCounter);
                                    zvDaten.setValue(yHi[i1], i1 + 1, zvCounter + 1);
                                } else {
                                    zvDaten.setValue(yHi[i1], i1 + 1, zvCounter);
                                    zvDaten.setValue(yLo[i1], i1 + 1, zvCounter + 1);
                                }
                            }
                            zvCounter++;
                            // (1) Initialisierung der Hi- u. Lo-Werte im neuen Intervall
                            for (int i1 = 0; i1 < tn; i1++) {
                                if (useBuffer) {
                                    yHi[i1] = scopeBuffer[i1 + 2];
                                } else {
                                    yHi[i1] = ((TerminalControlIn) _terminals.get(i1)).getValue();
                                }

                                yLo[i1] = yHi[i1];
                            }
                            zvCounterRef[0] = zvCounter;
                            //-------------
                        } else {
                            // Hi- und Lo-Werte werden in diesem Intervall laufend aktualisiert -->
                            //-------------
                            for (int i1 = 0; i1 < tn; i1++) {
                                double yIntervall;
                                if (useBuffer) {
                                    yIntervall = scopeBuffer[i1 + 2];
                                } else {
                                    yIntervall = ((TerminalControlIn) _terminals.get(i1)).getValue();
                                }


                                if (yIntervall > yHi[i1]) {
                                    yHi[i1] = yIntervall;
                                    xHi[i1] = time;
                                }
                                if (yIntervall < yLo[i1]) {
                                    yLo[i1] = yIntervall;
                                    xLo[i1] = time;
                                }
                            }
                            //-------------
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e + "  uiz889uuu");
                    }
                    //-----------------
                } else {
                    //-----------------
                    // Die Simulationsdaten sind so wenige, dass man die einzelnen simulierten Punkte im Scope sehen kann.
                    // Deswegen werden nur die simulierten Punkte dargestellt, also nicht die daraus berechneten HiLo-Punkte,
                    // weil das sonst Verwirrung stiften wuerde.
                    //
                    // INIT -->
                    //-----------------
                    if (!initDone) {
                        initDone = true;
                        zvDatenRAM = null;
                        zvDatenRAM = new DataContainerSimple(1 + tn, (int) ((_model.tEnd.getValue()) / dt / reduktorRAM) + 5);
                    }
                    //-----------------
                    zvDaten.setValue((float) time, 0, zvCounter);
                    for (int i1 = 0; i1 < tn; i1++) {
                        if (useBuffer) {
                            zvDaten.setValue((float) scopeBuffer[i1 + 2], i1 + 1, zvCounter);
                        } else {
                            zvDaten.setValue((float) ((TerminalControlIn) _terminals.get(i1)).getValue(), i1 + 1, zvCounter);
                        }
                    }
                    zvCounterRef[0] = zvCounter;
                    //System.out.print("Datenspeicherung im SCOPE: "+zvDaten[0][zvCounter]+"\t");
                    //for (int i1=0;  i1<tn;  i1++) System.out.print(zvDaten[i1+1][zvCounter]+"\t"); System.out.println();
                    zvCounter++;
                    //-----------------
                }
                //=======================
                // ZV-Punkte werden (eventuell komprimiert) im vorhandenen RAM-Speicher gespeichert -->
                // ... fuer hoehere Aufloesung bei Zoom, Fourier usw.
                //=======================
                if (reduktorRAM < 1.1) {  // Daten passen unkomprimiert in den Arbeitsspeicher ...
                    zvDatenRAM.setValue(time, 0, zvCounterRAM);
                    for (int i1 = 0; i1 < tn; i1++) {
                        double value;
                        if (useBuffer) {
                            value = scopeBuffer[i1 + 2];
                        } else {
                            value = ((TerminalControlIn) _terminals.get(i1)).getValue();
                        }
                        zvDatenRAM.setValue(value, i1 + 1, zvCounterRAM);
                    }
                    zvCounterRAM++;
                } else {  // Daten muessen komprimiert werden ...
                    if ((zvCounterRAM < zvDatenRAM.getColumnLength() - 1) && ((int) (time / dt) % reduktorRAM == 0)) {
                        zvCounterRAM++;
                        zvDatenRAM.setValue(time, 0, zvCounterRAM);
                        for (int i1 = 0; i1 < tn; i1++) {
                            double value;
                            if (useBuffer) {
                                value = scopeBuffer[i1 + 2];
                            } else {
                                value = ((TerminalControlIn) _terminals.get(i1)).getValue();
                            }
                            assert value == value : "value is " + value;
                            zvDatenRAM.setValue(value, i1 + 1, zvCounterRAM);
                        }
                    }
                }

                counter++;
                newDataToPlot = true;

                //assert time < 0 : "scope time negative";
              }
            } else if (stepped_back) {
                if (steps_reversed > 0)
                    steps_reversed--;
                if (steps_reversed == 0)
                    stepped_back = false;

            }
        }
        /*else
        {
        System.out.println("scope not executed at " + time + "for time step " + dt);
        }*/
    }

    class UpdateScopeTask extends TimerTask {

        public void run() {
            if (newDataToPlot) {
                scope.updateScope(0, _model.tEnd.getValue());
                newDataToPlot = false;
            }
        }
    }

    public void stepBack() {
        if (!_model.testStep) {
            stepped_back = true;
            //removed buffered data that has been stepped back
            for (int i = 0; i < buffered; i++)
                bufferList.removeLast();
            steps_reversed = buffered;
        }
    }

    public void aktiviereZVDatenSpeicherung(boolean isActive) {
        this.datenspeicherungIsActive = isActive;
        try {
            fkaku.close();
            fkakuOpen = false;
        } catch (Exception e) {
        }   // als erstes wird eine eventuell offene Datei geschlossen
        if (datenspeicherungIsActive) {
            try {
                fkaku = new BufferedWriter(new FileWriter(new File(datnamDefault)));  // Stream auf Datei wird geoeffnet
                fkakuOpen = true;
                sb = new StringBuffer();
                sb.append("time");
                for (int i1 = 0; i1 < header.length; i1++) {
                    sb.append(" " + header[i1]);
                }
                sb.append("\n");  // Zeilenumbruch
            } catch (Exception e) {
            }
        }
    }

    public boolean speichereZVDaten(String datnam) {
        try {

            if (fkakuOpen) {
                fkaku.flush();
                fkaku.close();
            }

            fkakuOpen = false;
            File datei = new File(datnamDefault);
            File dateiSchonVorhanden = new File(datnam);
            if (dateiSchonVorhanden.exists()) {
                dateiSchonVorhanden.delete();  // Ueberschreiben einer eventuell vorhandenen Datei
            }
            datei.renameTo(new File(datnam));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e + "   efogin2");
            return false;
        }
        return true;
    }

    public boolean getDatenspeicherungIsActive() {
        return datenspeicherungIsActive;
    }

    public void loescheZVDatenImRAM() {
        zvDatenRAM = null;
    }
}
