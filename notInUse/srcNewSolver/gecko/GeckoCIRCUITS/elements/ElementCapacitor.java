/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.geckocircuitsnew.circuit.Capacitor;
import gecko.geckocircuitsnew.circuit.CapacitanceCharacteristic;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;
import geckocircuitsnew.ACCESSIBLE;
import geckocircuitsnew.DatenSpeicher;

public class ElementCapacitor extends ElementCircuit {

    @OPTIMIZABLE
    public final ModelMVC<Double> capacitance = new ModelMVC<Double>(1E-7,"C","F");
    @ACCESSIBLE
    public final ModelMVC<Double> initialValue = new ModelMVC<Double>(0.0,"uC(0)","V");
    public final Capacitor capacitor;

    public final ModelMVC<Boolean> isNonLinear = new ModelMVC(false,"use non-linear capacitance characteristic");

    private CapacitanceCharacteristic varyingC;
    
    public ElementCapacitor(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        capacitor = new Capacitor(_terminals.get(0), _terminals.get(1), this);


        capacitance.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                capacitor.setCapacitance(capacitance.getValue());
            }
        });

        initialValue.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                capacitor.setInitialValue(initialValue.getValue());
            }
        });

        isNonLinear.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                capacitor.setNonLinear(isNonLinear.getValue());

                if (!isNonLinear.getValue())
                    capacitor.setCapacitance(capacitance.getValue());
            }
        });

        circuitComponents.add(capacitor);

    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {        
        double lg = 2.0;
        double da = 0.5;
        int d = 4;
        int dm = 5;
        double br = 0.9;
        double ho = 0.4;
            
        double hoi = 0.2;
        
        g.drawLine(0, (int) (dpix * lg), 0, (int) (-dpix * lg));
        g.fillRect((int) (dpix * (- br)), (int) (dpix * (- ho)), (int) (dpix * (2 * br)), (int) (dpix * (2 * ho)));
        Color oldColor = g.getColor();
        g.setColor(Color.white);
        g.fillRect((int) (dpix * (- br)), (int) (dpix * (- hoi)), (int) (dpix * (2 * br)), (int) (dpix * (2 * hoi - 0.05)));
        g.setColor(oldColor);
        
    }

    @Override
    protected void importASCII_Individual(String ascii[]) {
        double[] nonlinx = null, nonliny = null;
        for (String zeile : ascii) {
             if (zeile.startsWith("isNonlinear ")) {
                 isNonLinear.setValue(DatenSpeicher.leseASCII_boolean(zeile));
             }
             else if (zeile.startsWith("nonlinX[] ")) {
                 nonlinx = DatenSpeicher.leseASCII_doubleArray1(zeile);
             }
             else if (zeile.startsWith("nonlinY[] ")) {
                 nonliny = DatenSpeicher.leseASCII_doubleArray1(zeile);
             }

             
        }

        if (nonlinx != null && nonliny != null)
        {
             varyingC = new CapacitanceCharacteristic(nonliny,nonlinx,true);
        }
        else
        {
             varyingC = new CapacitanceCharacteristic();
        }

        capacitor.setCapacitanceCharacteristic(varyingC);

    }

    @Override
    protected void setParameter(double[] parameter) {
        capacitance.setValue(parameter[0]);
        initialValue.setValue(parameter[1]);        
    }

    

}
