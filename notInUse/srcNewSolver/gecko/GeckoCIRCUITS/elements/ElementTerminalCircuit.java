/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalConnector;
import gecko.GeckoCIRCUITS.terminal.TerminalSubsheet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class ElementTerminalCircuit extends ElementCircuit {

    private final ElementSubSheet _subSheet;
    public final TerminalSubsheet term1;
    public final TerminalSubsheet term2;

    public ElementTerminalCircuit(ElementSubSheet subCircuit, String name, int xInternal,
            int yInternal, int xExternal, int yExternal) {
            super(subCircuit.getSheet());
        _elementName.setValue(name);
        x.setValue(xInternal);
        y.setValue(yInternal);
        _subSheet = subCircuit;

        // terminal on "outer" sheet:
        term1 = new TerminalSubsheet(_subSheet, this, xExternal, yExternal);
        term1.setLabel(name);
        _subSheet._terminals.add(term1);

        term2 = new TerminalSubsheet(this, this, 0, 0);
        term2.setLabel(name);
        _terminals.add(term2);
        _subSheet.getSubSheet().add(this);

    }

    ElementTerminalCircuit(ElementSubSheet subCircuit, String name, int xInternal,
            int yInternal, int xExternal, int yExternal, CircuitSheet sheet) {
            this(subCircuit, name, xInternal, yInternal, xExternal, yExternal);
            _circuitSheet = sheet;
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int _dpix) {

        int x = -3;
        double y = -0.1;
        double br = 1.9;
        double ho = 0.8;
        double dax = 1;
        g.setColor(Typ.farbeElementLKHintergrund);

        g.fillPolygon(
                new int[]{(int) (_dpix * (x - br)), (int) (_dpix * (x + br)), (int) (_dpix * (x + br + dax)), (int) (_dpix * (x + br)), (int) (_dpix * (x - br))},
                new int[]{(int) (_dpix * (y + ho)), (int) (_dpix * (y + ho)), (int) (_dpix * (y)), (int) (_dpix * (y - ho)), (int) (_dpix * (y - ho))}, 5);
        g.setColor(Typ.farbeFertigElementLK);
        g.drawPolygon(
                new int[]{(int) (_dpix * (x - br)), (int) (_dpix * (x + br)), (int) (_dpix * (x + br + dax)), (int) (_dpix * (x + br)), (int) (_dpix * (x - br))},
                new int[]{(int) (_dpix * (y + ho)), (int) (_dpix * (y + ho)), (int) (_dpix * (y)), (int) (_dpix * (y - ho)), (int) (_dpix * (y - ho))}, 5);
        g.setFont(new Font("Arial", Font.PLAIN, (int) (_dpix *0.75 +2)));
        double d = 3;
        //g.drawString(_elementName.getValue(), (int) (_dpix * (x - br) + d), (int) (_dpix * (y) + (10 / 2 - 1)));
        //g.setFont(SchematischeEingabe2.foTHERM);

    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {

    }




}
