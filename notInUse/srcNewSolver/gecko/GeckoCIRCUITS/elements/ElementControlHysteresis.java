/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOptionalIn;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
class ElementControlHysteresis extends ElementControlSingleInOut {
    private double _youtStore;
    private TerminalControlOptionalIn _optTerm = new TerminalControlOptionalIn(-1, 2, this);
    private double inValue;

    @ACCESSIBLE
    public final ModelMVC<Double> H = new ModelMVC<Double>(1.5,"h");

    public ElementControlHysteresis(CircuitSheet sheet) {
        super(sheet);
        var_history = new double[steps_saved][2];
        _baseName = "HYS";
       _terminals.add(_optTerm);

       H.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value0 = H.getValue();
            }
        });
    }

    protected void setParameter(double[] parameter) {
        super.setParameter(parameter);
        if(parameter[1] > 0.5) {
            _optTerm.visible.setValue(true);
        }
    }
    @Override
    public void calculateOutput(double time, double dt) {
        
        if (_model.saveHistory && !stepped_back && (time > 0)) {
            
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] =  _youtStore;
        }

        inValue = _termIn.getValue();

        if(_optTerm.visible.getValue()) {
            if (inValue>=+ _optTerm.getValue()) _youtStore= +1.0;
            if (inValue<=- _optTerm.getValue()) _youtStore= -1.0;
        } else {
            if (inValue>=+value0) _youtStore= +1.0;
            if (inValue<=-value0) _youtStore= -1.0;
        }
        _termOut.writeValue(_youtStore, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }

    }

    public void stepBack()
    {
       if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            _youtStore = var_history[0][1];
            stepped_back = true;
            steps_reversed++;
        }
    }

}
