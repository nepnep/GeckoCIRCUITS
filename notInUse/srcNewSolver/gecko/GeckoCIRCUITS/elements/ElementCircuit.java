/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.animation.CurrentState;
import gecko.geckocircuitsnew.circuit.LossCalculatable;
import gecko.geckocircuitsnew.circuit.VerlustBerechnung;
import gecko.geckocircuitsnew.animation.AbstractAnimate;
import gecko.geckocircuitsnew.animation.AnimateComponent;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.ConnectionType;
import gecko.GeckoCIRCUITS.terminal.Terminable;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalComponent;
import gecko.GeckoCIRCUITS.terminal.TerminalComponentReluctance;
import geckocircuitsnew.DatenSpeicher;
import geckocircuitsnew.Orientation;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author andy
 */
public abstract class ElementCircuit extends ElementInterface 
implements Terminable {

    public final ArrayList<CircuitComponent> circuitComponents = new ArrayList<CircuitComponent>();
    private VerlustBerechnung _lossCalculation;
    public AnimateComponent _animation;

    public ElementCircuit(CircuitSheet sheet) {
        super(sheet);
    }

    public static ElementInterface fabric(String txt, CircuitSheet sheet) {
        StringTokenizer stk = null;
        String[] ascii = DatenSpeicher.makeStringArray(txt);

        Orientation orientation = Orientation.SOUTH;
        int typ = -1;
        int tmpX = 10;
        int tmpY = 10;

        String[] parameterString = null;
        String[] labelAnfangsKnoten = null;
        String[] labelEndKnoten = null;

        double[] parameter = null;
        String tmpName = "";

        VerlustBerechnung verluste = null;
        int lineCounter = -1;
        for (String zeile : ascii) {
            lineCounter++;
            if (zeile.startsWith("typ ")) {
                typ = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("orientierung ")) {
                orientation = Orientation.getOri(DatenSpeicher.leseASCII_int(zeile));
            } else if (zeile.startsWith("x ")) {
                tmpX = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("y ")) {
                tmpY = DatenSpeicher.leseASCII_int(zeile);
            } else if (zeile.startsWith("parameter[] ")) {
                parameter = DatenSpeicher.leseASCII_doubleArray1(zeile);
            } else if (zeile.startsWith("idStringDialog")) {
                tmpName = DatenSpeicher.leseASCII_String(zeile);
            } else if (zeile.startsWith("parameterString[] ")) {
                parameterString = DatenSpeicher.leseASCII_StringArray1(zeile);
            } else if (zeile.startsWith("labelAnfangsKnoten[] ")) {
                labelAnfangsKnoten = DatenSpeicher.leseASCII_StringArray1(zeile);
            } else if (zeile.startsWith("labelEndKnoten[] ")) {
                labelEndKnoten = DatenSpeicher.leseASCII_StringArray1(zeile);
            } else if (zeile.startsWith("<Verluste>")) {
                verluste = new VerlustBerechnung(ascii, lineCounter);
            }
        }

        ElementInterface returnValue = null;

        switch (typ) {
            case LK_R:
                if (tmpName.startsWith("_")) {
                    returnValue = new ElementResistorReluctance(sheet);
                } else {
                    returnValue = new ElementResistor(sheet);
                }

                break;
            case LK_L:
                returnValue = new ElementInductor(sheet, false);
                break;
            case LK_C:
                returnValue = new ElementCapacitor(sheet);
                break;
            case LK_U:
                returnValue = new ElementVoltage(sheet);
                break;
            case LK_I:
                returnValue = new ElementCurrentSource(sheet);
                break;
            case LK_D:
                returnValue = new ElementDiode(sheet);
                break;
            case LK_S:
                returnValue = new ElementIdealSwitch(sheet);
                break;
            case LK_THYR:
                returnValue = new ElementThyristor(sheet);
                break;
            case LK_IGBT:
                returnValue = new ElementIGBT(sheet);
                break;
            case LK_LKOP2:
                if (tmpName.startsWith("_")) {
                    returnValue = new ElementInductorReluctance(sheet);
                } else {
                    returnValue = new ElementInductor(sheet, true);
                }

                break;
            case LK_LISN:
                returnValue = new ElementSubSheet(sheet);
                break;
            case LK_M:
                returnValue = new ElementMutualInductance(sheet);
                break;
            case LK_OPV1:
                returnValue = new ElementOPV(sheet);
                break;
            case LK_MACHINE_DC:
                returnValue = new ElementMachineDC(sheet);
                break;
            case TH_RTH:
                returnValue = new ElementResistorTherm(sheet);
                break;
            case TH_CTH:
                returnValue = new ElementCapacitorTherm(sheet);
                break;
            case TH_TEMP:
                returnValue = new ElementVoltageTherm(sheet);
                break;
            case TH_MODUL:
                System.err.println("thermal power module not yet implemented!");
                break;
            case TH_FLOW:
                returnValue = new ElementThermalFlow(sheet);
                break;
            case TH_AMBIENT:
                returnValue = new ElementThermalAmbient(sheet);
                break;
            case TH_PvCHIP:
                returnValue = new ElementThermalPowerLoss(sheet);
                break;
            default:
                System.err.println("unknown circuit type: " + typ);
        }

        if (returnValue != null) {
            returnValue.x.setValue(tmpX);
            returnValue.y.setValue(tmpY);
            returnValue.setParameter(parameter);
            returnValue.setParameterString(parameterString);
            returnValue.setOrientation(orientation);
            returnValue.setSheet(sheet);
            returnValue._elementName.setValue(tmpName);
            returnValue.setNodeLabels(labelAnfangsKnoten, labelEndKnoten);
            returnValue.importASCII_Individual(ascii);

            if (returnValue instanceof ElementCircuit && verluste != null) {
                ((ElementCircuit) returnValue).setLossCalculation(verluste);
            }

        }

        return returnValue;

    }

    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.POWERCIRCUIT;
    }

    public void setLossCalculation(VerlustBerechnung lossCalculation) {
        _lossCalculation = lossCalculation;
        if (circuitComponents.get(0) != null) {
            CircuitComponent comp = circuitComponents.get(0);
            if (comp instanceof LossCalculatable) {
                ((LossCalculatable) comp).setLossCalculation(lossCalculation);
            }
        }
    }


    public void initAnimation(int[] relCoords) {
        _animation = new AnimateComponent(this, relCoords);
    }

    public void initTwoTerminalComponent() {
        switch (getConnectionType()) {
            case POWERCIRCUIT:
                _terminals.add(new TerminalComponent(this, 0, -2));
                _terminals.add(new TerminalComponent(this, 0, 2));
                break;
            case RELUCTANCE:
                _terminals.add(new TerminalComponentReluctance(this, 0, -2));
                _terminals.add(new TerminalComponentReluctance(this, 0, 2));
                break;
            case THERMALCIRCUIT:
                _terminals.add(new TerminalComponentTherm(this, 0, -2));
                _terminals.add(new TerminalComponentTherm(this, 0, 2));
                break;
            default:
                assert false;
        }

    }

    public ArrayList<Terminal> getTerminals() {
        ArrayList<Terminal> returnValue = new ArrayList<Terminal>();

        returnValue.addAll(_terminals);

        for (CircuitComponent comp : circuitComponents) {
            for (Terminal term : comp.getTerminals()) {
                if (!returnValue.contains(term)) {
                    returnValue.add(term);
                }
            }
        }

        return returnValue;
    }

    @Override
    public void paint(Graphics2D g, int scaling) {
        // Template-Method pattern
        g.setColor(Typ.farbeFertigElementLK);
        paintCircuitComponent(g, scaling);
        paintAnimation(g, scaling);
        super.paint(g, scaling);
    }

    abstract void paintCircuitComponent(Graphics2D g, int scaling);

    private void paintAnimation(Graphics2D g, int scaling) {

        if(_animation != null) {            
            _animation.paint(g, scaling);            
        }
    }
    // just for backwards compatibility
    private static final int LK_R = 1;
    private static final int LK_L = 2;
    private static final int LK_C = 3;
    private static final int LK_U = 4;
    private static final int LK_I = 5;
    private static final int LK_D = 6;
    private static final int LK_S = 7;
    private static final int LK_THYR = 8;
    private static final int LK_M = 9;
    private static final int LK_IGBT = 10;
    private static final int LK_LKOP2 = 12;
    private static final int LK_LISN = 13;
    private static final int LK_OPV1 = 22;
    private static final int LK_MACHINE_DC = 14;
    //
    private static final int TH_PvCHIP = 41;
    private static final int TH_MODUL = 42;
    private static final int TH_KUEHLER = 43;
    private static final int TH_FLOW = 44;
    private static final int TH_TEMP = 45;
    private static final int TH_RTH = 46;
    private static final int TH_CTH = 47;
    private static final int TH_AMBIENT = 48;

    public void setAnimationState(CurrentState currentState) {
//        System.out.println(this + " setting state: " + currentState.getState());
        _animation.setAnimationState(currentState);
    }

//    public int[] generateRelCoords(final Point start, final Point end) {
//        int[] result = new int[4];
//        result[0] = start.x - x.getValue();
//        result[1] = start.y - y.getValue();
//        result[2] = end.x - x.getValue();
//        result[3] = end.y - y.getValue();
//
////        System.out.println(_trimmedCoords + "\t-->\t" + start + end);
////        System.out.println("\t" + result[0] + ", " + result[1] + ", " + result[2] + ", " + result[3]);
////        System.out.println("\t" + x.getValue() + ", " + y.getValue());
//
//        return result;
//    }
}
