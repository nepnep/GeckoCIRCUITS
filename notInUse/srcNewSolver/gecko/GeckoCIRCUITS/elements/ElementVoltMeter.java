/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class ElementVoltMeter extends ElementControl {

    private TerminalControlOut _termOut;
    private CircuitComponent comp1 = null;
    private CircuitComponent comp2 = null;
    private int comp1TerminalIndex = -1;
    private int comp2TerminalIndex = -1;

    public ElementVoltMeter(CircuitSheet sheet) {
        super(sheet);
        _termOut = new TerminalControlOut(2, 0, this);
        _terminals.add(_termOut);
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paint(g, dpix);

        paintElementString(g, dpix, "VOLT");

    }

    @Override
    public ControlType getType() {
        return ControlType.SOURCE;
    }

    @Override
    public void calculateOutput(double time, double dt) {


        if (comp1 == null) {
            //for(ElementInterface elem : allElements) {
            for(ElementInterface elem : _model.modelElements) {
                if (elem instanceof ElementCircuit) {
                    ElementCircuit elemC = (ElementCircuit) elem;


                    for (Terminal term : elemC._terminals) {
                        if (term.getLabel().equals(_parameterString[0]) && !term.getLabel().isEmpty()) {
                            for (CircuitComponent comp : elemC.circuitComponents) {
                                if (comp.getTerminalIndex(0) == term.getIndex()) {
                                    comp1 = comp;
                                    comp1TerminalIndex = 0;
                                } else if (comp.getTerminalIndex(1) == term.getIndex()) {
                                    comp1 = comp;
                                    comp1TerminalIndex = 1;
                                }

                            }
                        }

                        if (term.getLabel().equals(_parameterString[1]) && !term.getLabel().isEmpty()) {
                            for (CircuitComponent comp : elemC.circuitComponents) {
                                if (comp.getTerminalIndex(0) == term.getIndex()) {
                                    comp2 = comp;
                                    comp2TerminalIndex = 0;
                                } else if (comp.getTerminalIndex(1) == term.getIndex()) {
                                    comp2 = comp;
                                    comp2TerminalIndex = 1;
                                }

                            }

                        }
                    }
                }
            }
        }

        assert comp1 != null : _parameterString[0];
        double value = comp1.getPotential(comp1TerminalIndex) - comp2.getPotential(comp2TerminalIndex);
        _termOut.writeValue(value, time);

    }

    public void stepBack()
    {

    }
}
