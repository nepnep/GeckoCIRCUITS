/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;

/**
 *
 * @author andy
 */
class ElementControlCounter extends ElementControlTwoInputOneOutput {

    private double lastValue;
    private int internalCount;

    public ElementControlCounter(CircuitSheet sheet) {
        super(sheet);
        _baseName = "CNT";
        var_history = new double[steps_saved][3];
    }
    
    @Override
    public void init() {
        lastValue = 0;
        internalCount = 0;
    }

    @Override
    public void calculateOutput(double time, double dt) {

        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = lastValue;
            var_history[0][2] = internalCount;
        }

        double inValue = _termIn1.getValue();
        if ((inValue >= 0.5) && (lastValue < 0.5)) {
            internalCount++;
        }
        if (_termIn2.getValue() > 0.5) {
            internalCount = 0;
        }
        // Logik-Schwelle --> 0.5;  RESET bei Input '1' (somit braucht man den Anschluss nicht extra mit einem const=1 - Block belegen, damit der Counter laeuft)
        lastValue = _termIn1.getValue();
        _termOut.writeValue(internalCount, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][2],var_history[0][0]);
            prev_time = var_history[0][0];
            lastValue = var_history[0][1];
            internalCount = (int) var_history[0][2];
            stepped_back = true;
            steps_reversed++;
        }
    }
}
