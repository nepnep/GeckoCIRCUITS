/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.Diode;
import gecko.GeckoCIRCUITS.elements.ElementInterface;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import geckocircuitsnew.OPTIMIZABLE;
import gecko.geckocircuitsnew.circuit.SwitchState;

/**
 *
 * @author andy
 */
public class ElementDiode extends ElementCircuit {

    @OPTIMIZABLE
    public final ModelMVC<Double> onResistance = new ModelMVC<Double>(1e-2,"Ron","ohm");
    @OPTIMIZABLE
    public final ModelMVC<Double> offResistance = new ModelMVC<Double>(1E5,"Roff","ohm");
    @OPTIMIZABLE
    public final ModelMVC<Double> uForward = new ModelMVC<Double>(0.6,"UF","V");

    private final Diode diode;

    public ElementDiode(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        diode = new Diode(_terminals.get(0), _terminals.get(1), this);
        circuitComponents.add(diode);

        uForward.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                diode.setUForward(uForward.getValue());
            }
        });


        onResistance.addModelListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                diode.setOnResistance(onResistance.getValue());
            }
        });

        offResistance.addModelListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                diode.setOffResistance(offResistance.getValue());
            }
        });

    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg = 2.0,   br = 0.5,   ho = 0.6,   da = 0.5;
        int dm = 4, d = 2;
        Color oldColor = g.getColor();
        g.drawLine((int) 0, (int) (dpix * lg), 0, (int) (-dpix * lg));
        
        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillPolygon(new int[]{(int) (dpix * (0)), (int) (dpix * ( - br)), (int) (dpix * ( + br))}, new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 3);

        g.setColor(oldColor);
        g.drawPolygon(new int[]{(int) (dpix * (0)), (int) (dpix * ( - br)), (int) (dpix * ( + br))}, new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 3);
        g.fillRect((int) (dpix * ( - br)), (int) (dpix * ( + ho) - d), (int) (dpix * (2 * br)), d);

    }

    @Override
    protected void importASCII_Individual(String ascii[]) {
                      
    }

    @Override
    protected void setParameter(double[] parameter) {
        uForward.setValue(parameter[1]);
        onResistance.setValue(parameter[2]);
        offResistance.setValue(parameter[3]);
    }
    
    public SwitchState getState(double time) {
        return diode.getState(time);
    }

    

}
