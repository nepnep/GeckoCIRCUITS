/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ElementControlPI extends ElementControlSingleInOut {

    private double y1alt = 0;  // Speicherung des I-Anteils
    private double xalt = 0, y11 = -1;
    private double y1altInit = 0, xaltInit = 0;
    private double inValue;
    private double outValue;
    
    private double _xaltMaxAbs = 0;
    private double _y1altMaxAbs = 0;
    
    private double y1altalt = 0;
    private double xaltalt = 0;

    //ModelMVC versions of PI fields
    @ACCESSIBLE
    public final ModelMVC<Double> R0 = new ModelMVC<Double>(2.0,"r0");
    @ACCESSIBLE
    public final ModelMVC<Double> A1 = new ModelMVC<Double>(1.0,"a1");
    
    public ElementControlPI(CircuitSheet sheet) {
        super(sheet);
        _baseName = "PI";
        var_history = new double[steps_saved][5];
        R0.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value0 = R0.getValue();
            }
        });

        A1.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value1 = A1.getValue();
            }
        });
    }
    
    @Override
    public void init() {
        xalt = xaltInit;
        y1alt = y1altInit;
        xaltInit = 0;
        y1altInit = 0;
    }
    
    
    @Override
    public void calculateOutput(double time, double dt) {
        if (_model.saveHistory && !stepped_back && (time > 0)) {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = outValue;
            var_history[0][2] = y1alt;
            var_history[0][3] = xalt;
            var_history[0][4] = y11;
        }

        inValue = _termIn.getValue();

        y11 = y1alt + 0.5 * value1 * dt * (inValue + xalt);
        outValue = y11 + value0 * inValue;
        
        xaltalt = xalt;
        y1altalt = y1alt;
        
        xalt = inValue;
        y1alt = y11;
        _termOut.writeValue(outValue, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
        
        if (Math.abs(xalt) > _xaltMaxAbs) {
            _xaltMaxAbs = Math.abs(xalt);
        }
        
        if (Math.abs(y1alt) > _y1altMaxAbs) {
            _y1altMaxAbs = Math.abs(y1alt);
        }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            outValue = var_history[0][1];
            y1alt = var_history[0][2];
            xalt = var_history[0][3];
            y11 = var_history[0][4];
            steps_reversed++;
            stepped_back = true;
        }
    }
    
    @Override
    public boolean hasInternalStates()
    {
        return true;
    }
    
    @Override
    public double[] getState()
    {
        //return new double[] {xalt, y1alt};
        return new double[]{xaltalt, y1altalt};
    }
    
    @Override
    public void setState(double[] state)
    {
        xaltInit = state[0];
        y1altInit = state[1];
        _xaltMaxAbs = Math.abs(xaltInit);
        _y1altMaxAbs = Math.abs(y1altInit);
    }
    
    @Override 
    public double getStateVar(int index)
    {
        if (index == 0)
            return xalt;
        else if (index == 1)
            return y1alt;
        else
            return 0;
    }
    
    @Override 
    public void setStateVar(double value, int index)
    {
        if (index == 0) {
            xaltInit = value;
            _xaltMaxAbs = Math.abs(xaltInit);
        }
        else if (index == 1) {
            y1altInit = value;
            _y1altMaxAbs = Math.abs(y1altInit);
        }
    }
    
    @Override
    public double getStateVarMaxAbs(int index) {
        
        if (index == 0) {
            return _xaltMaxAbs;
        }
        else if (index == 1) {
            return _y1altMaxAbs;
        }
        else {
            return 0;
        }
    }
}
