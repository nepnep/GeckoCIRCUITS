/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.AbstractSwitch;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class ElementGate extends ElementControl {

    private TerminalControlIn _termIn;
    private AbstractSwitch _switchable;
    private boolean _oldState;
    private boolean _newState;

    public ElementGate(CircuitSheet sheet) {
        super(sheet);
        _termIn = new TerminalControlIn(-2, 0, this);
        _terminals.add(_termIn);
        _baseName = "GATE";
        var_history = new double[steps_saved][3];
    }

    @Override
    public ControlType getType() {
        return ControlType.SINK;
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }
    

    @Override
    public void calculateOutput(double time, double dt) {

        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = (int) (_oldState ? 1:0);
            var_history[0][2] = (int) (_newState ? 1:0);
        }

        double value = _termIn.getValue();        
        _newState = value > 0.5;
        if (_switchable == null) {
            //for(ElementInterface elem : allElements) {
            for(ElementInterface elem : _model.modelElements) {
                if (elem._elementName.getValue().equals(_parameterString[0])) {
                    _switchable = (AbstractSwitch) ((ElementCircuit) elem).circuitComponents.get(0);
                }
            }
        }

        if (_oldState != _newState) {
            //System.out.println("Setting gate signal for " + _switchable.getOwningElementName() + " at t = " + time);
            _switchable.setGateSignal(_newState);
        }
        _oldState = _newState;

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            prev_time = var_history[0][0];
            _newState = (((int)var_history[0][2]) == 1);
            if (_oldState != _newState) 
                 _switchable.setGateSignal(_newState);
            _oldState = (((int)var_history[0][1]) == 1);
            stepped_back = true;
            steps_reversed++;
        }
    }
}
