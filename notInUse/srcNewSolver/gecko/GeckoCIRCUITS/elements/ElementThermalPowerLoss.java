/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CurrentSource;
import gecko.geckocircuitsnew.circuit.TimeFunctionConstant;
import gecko.geckocircuitsnew.circuit.TimeFunctionPowerLoss;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.ConnectionType;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalGND;
import geckocircuitsnew.Orientation;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
class ElementThermalPowerLoss extends ElementCircuit {

    private final CurrentSource currentSource;
    private final Terminal terminalOut = new TerminalComponentTherm(this, 2, 0);
    private final TerminalGND terminalGND = new TerminalGND(this);

    private final TimeFunctionPowerLoss lossFunction = new TimeFunctionPowerLoss();

    public ElementThermalPowerLoss(CircuitSheet sheet) {
        super(sheet);
        currentSource = new CurrentSource(terminalGND, terminalOut, this);
        _terminals.add(terminalOut);
        circuitComponents.add(currentSource);
        currentSource.setFunction(lossFunction);
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    protected void setParameterString(String[] parameterString) {
        super.setParameterString(parameterString);
        lossFunction.setLossElementName(_parameterString[0]);
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double br = 1.2;
        double ho = 0.6;
        double da = 0.8;
        double d = 4;
        double dm = 5;

        double dax = 0.5 * da;
        g.setColor(Typ.colorForeGroundElement);
        g.drawLine((int) (dpix * ( - br - da)), 0, (int) (dpix * ( + br + da)), 0);
        g.drawLine((int) (dpix * ( - br - da)), 0, (int) (dpix * ( - br - da)), (int) (dpix * 0.5) );
        g.setColor(Typ.colorBackGroundElement);
        g.fillPolygon(
                new int[]{(int) (dpix * ( - br)), (int) (dpix * ( + br)), (int) (dpix * ( + br + dax)), (int) (dpix * ( + br)), (int) (dpix * ( - br))},
                new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( + ho)), 0, (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 5);
        g.setColor(Typ.colorForeGroundElement);
        g.drawPolygon(
                new int[]{(int) (dpix * ( - br)), (int) (dpix * ( + br)), (int) (dpix * ( + br + dax)), (int) (dpix * ( + br)), (int) (dpix * ( - br))},
                new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( + ho)), 0, (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 5);
        // 'Erde'  -->
        double erX = 0.5;
        int erY = 3;
        g.fillRect((int) (dpix * ( - br - da - erX)), (int) (dpix * ( + ho)), (int) (dpix * (2 * erX)), erY);
    }



    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.THERMALCIRCUIT;
    }

    @Override
    public void setOrientation(Orientation orientation) {
    }



}
