/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;

/**
 *
 * @author andy
 */
public class ElementAmperemeter extends ElementControlSingleOutput {

    private CircuitComponent _measureElement = null;
    private double measurement = 0;
    
    public ElementAmperemeter(CircuitSheet sheet) {
        super(sheet);
        _baseName = "AMP";
    }
    

    @Override
    protected void setParameter(double[] parameter) {
    }


    @Override
    public void calculateOutput(double time, double dt) {

     if(_measureElement == null) {            
         //for(ElementInterface elem : allElements) {
         for(ElementInterface elem : _model.modelElements) {
             if(elem instanceof ElementCircuit)
             if(elem._elementName.getValue().equals(_parameterString[0])) {
                 _measureElement = ((ElementCircuit)elem).circuitComponents.get(0);
             }
         }
     }

     assert _measureElement != null : _parameterString[0];
     measurement = _measureElement.getCurrent();
        

     _termOut.writeValue(measurement, time);

    }

    public void stepBack()
    {

    }
    
}