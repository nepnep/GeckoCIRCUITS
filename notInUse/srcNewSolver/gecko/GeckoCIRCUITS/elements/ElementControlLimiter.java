/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOptionalIn;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ElementControlLimiter extends ElementControlSingleInOut {
    private TerminalControlOptionalIn _optTermMax = new TerminalControlOptionalIn(-1, 3, this);
    private TerminalControlOptionalIn _optTermMin = new TerminalControlOptionalIn(-1, 2, this);
    private double youtMIN;
    private double youtMAX;
    private double in;

    //ModelMVC versions of limiter fields
    @ACCESSIBLE
    public final ModelMVC<Double> MIN = new ModelMVC<Double>(-1.0,"min");
    @ACCESSIBLE
    public final ModelMVC<Double> MAX = new ModelMVC<Double>(1.0,"max");

    public ElementControlLimiter(CircuitSheet sheet) {
        super(sheet);
        var_history = new double[steps_saved][4];
        _baseName = "LIM";
        _terminals.add(_optTermMax);
        _terminals.add(_optTermMin);

        MIN.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                youtMIN = MIN.getValue();
            }
        });

        MAX.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                youtMAX = MAX.getValue();
            }
        });

    }

    protected void setParameter(double[] parameter) {
        super.setParameter(parameter);
        if(parameter[2] > 0.5) {
            _optTermMin.visible.setValue(true);
            _optTermMax.visible.setValue(true);
        }
        youtMAX = value1;
        youtMIN = value0;
    }

    @Override
    public void calculateOutput(double time, double dt) {
        
        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = youtMIN;
            var_history[0][2] = youtMAX;
            var_history[0][3] = _termOut._potArea._value;
        }

        in = _termIn.getValue();
        if (_optTermMax.visible.getValue()) {
                youtMIN = _optTermMin.getValue();
                youtMAX = _optTermMax.getValue();
        }
               
        double outValue = in;
       
        if (outValue >= youtMAX) {
            outValue = youtMAX;
        } else
        if (outValue <= youtMIN) {
            outValue = youtMIN;
        }
        _termOut.writeValue(outValue, time);

        if (_model.saveHistory) {
            prev_time = time;

            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
        
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
          if (stepped_back)
              historyBackward();
          _termOut.writeValue(var_history[0][3], var_history[0][0]);
          prev_time = var_history[0][0];
          youtMIN = var_history[0][1];
          youtMAX = var_history[0][2];
          steps_reversed++;
          stepped_back = true;
        }
    }
}
