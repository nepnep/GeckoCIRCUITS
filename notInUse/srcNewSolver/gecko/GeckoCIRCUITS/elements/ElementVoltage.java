/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.InductorCoupling;
import gecko.geckocircuitsnew.circuit.TimeFunction;
import gecko.geckocircuitsnew.circuit.TimeFunctionConstant;
import gecko.geckocircuitsnew.circuit.TimeFunctionSine;
import gecko.geckocircuitsnew.circuit.AbstractVoltageSource;
import gecko.geckocircuitsnew.circuit.VoltageSource;
import gecko.geckocircuitsnew.circuit.VoltageSourceDCMachine;
import gecko.geckocircuitsnew.circuit.VoltageSourceDirect;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import geckocircuitsnew.ACCESSIBLE;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ElementVoltage extends ElementCircuit {

    @ACCESSIBLE
    public final ModelMVC<Double> voltageAmplitude = new ModelMVC<Double>(327.0,"uMAX","V");
    @ACCESSIBLE
    public final ModelMVC<Double> voltageDC = new ModelMVC<Double>(20.0,"UDC","V");
    @ACCESSIBLE
    public final ModelMVC<Double> frequency = new ModelMVC<Double>(50.0,"f","Hz");
    @ACCESSIBLE
    public final ModelMVC<Double> offset = new ModelMVC<Double>(0.0,"offset","V");
    @ACCESSIBLE
    public final ModelMVC<Double> phase = new ModelMVC<Double>(0.0,"phase","\u00B0");
    public VoltageSource voltageSource;
    private TimeFunctionConstant _const_function = new TimeFunctionConstant(20);
    private TimeFunctionSine _sin_function = new TimeFunctionSine(327,50,0,0);
    private TimeFunction _function  = _const_function;
    private double[] _parameter;

    public ElementVoltage(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        voltageSource = new VoltageSource(_function, _terminals.get(0), _terminals.get(1), this);
        circuitComponents.add(voltageSource);

        voltageAmplitude.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._amplitude = voltageAmplitude.getValue();
            }
        });

        frequency.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._freq = frequency.getValue();
            }
        });

        offset.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._offset = offset.getValue();
            }
        });

        phase.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _sin_function._phase = phase.getValue();
            }
        });

        voltageDC.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _const_function._value = voltageDC.getValue();
            }
        });
    }

    public ElementVoltage(CircuitSheet sheet, InductorCoupling le, InductorCoupling la) {
        super(sheet);
        super.initTwoTerminalComponent();

        voltageSource = new VoltageSourceDCMachine(new TimeFunctionConstant(0), _terminals.get(0), _terminals.get(1), le, la, this);
        circuitComponents.add(voltageSource);
    }


    public ElementVoltage(Terminal direct1, Terminal direct2, CircuitSheet sheet, double gain) {
        super(sheet);
        super.initTwoTerminalComponent();
        voltageSource = new VoltageSourceDirect(_function, _terminals.get(0), _terminals.get(1), direct1, direct2, gain, this);
        circuitComponents.add(voltageSource);
    }


    public void setFunction(TimeFunction function) {
        _function = function;
        voltageSource.setFunction(_function);
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg = 2.0;
        double ho = 0.6;
        double da = 0.5;
        int d = 4;
        int dm = 5;
        double br = 0.9;
        double abstand = br + 0.4;
        double dp = 0.2;
        int p1 = 9;
        int p2 = 3;

        double x1 = 0;
        double y1 = - lg;
        double x2 = 0;
        double y2 = lg;

        Color oldColor = g.getColor();

        g.drawLine(0, (int) (dpix * (+lg)), 0, (int) (dpix * (-lg)));
        g.setColor(Typ.colorBackGroundElement);
        g.fillOval((int) (-dpix * br), (int) (-dpix - br), (int) (dpix * 2 * br), (int) (dpix * 2 * br));
        g.setColor(oldColor);
        g.drawOval((int) (-dpix * br), (int) (-dpix - br), (int) (dpix * 2 * br), (int) (dpix * 2 * br));
        //g.drawPolyline(new int[]{(int) (dpix * (- abstand)), (int) (dpix * (- abstand)), (int) (dpix * (- abstand) - p2), (int) (dpix * ( - abstand))}, new int[]{(int) (dpix * ( - br - dp)), (int) (dpix * ( + br + dp)), (int) (dpix * ( + br + dp) - p1), (int) (dpix * ( + br + dp) - p1)}, 4);
        int lgq = (int) (dpix * br / 2) + 2, dx1 = 8, dx2 = 2;
        g.drawLine((int) - dx1, (int) (dpix * y1) + lgq, (int)  - dx1 - lgq, (int) (dpix * y1) + lgq);
        g.drawLine((int) - dx1 - lgq / 2, (int) (dpix * y1) + (int) (1.5 * lgq), (int)  - dx1 - lgq / 2, (int) (dpix * y1) + lgq / 2);
        g.drawLine((int) - dx1 - dx2, (int) (dpix * y2) - lgq, (int) - dx1 - lgq, (int) (dpix * y2) - lgq);
    }

    @Override
    protected void importASCII_Individual(String ascii[]) {
    }

    @Override
    protected void setParameterString(String[] parameterString) {
        super.setParameterString(parameterString);


        int typInt = (int) _parameter[0];

        switch(typInt) {
            case Typ.QUELLE_DC:
                _const_function = new TimeFunctionConstant(_parameter[1]);
                _function = _const_function;
                voltageDC.setValue(_parameter[1]);
                break;
            case Typ.QUELLE_SIN:
                _sin_function = new TimeFunctionSine(_parameter[1], _parameter[2], _parameter[3],  _parameter[4]);
                _function = _sin_function;
                voltageAmplitude.setValue(_parameter[1]);
                frequency.setValue(_parameter[2]);
                offset.setValue(_parameter[3]);
                phase.setValue(_parameter[4]);
                break;
            case Typ.QUELLE_SIGNALGESTEUERT:
                _function = new TimeFunctionSignal(_parameterString[0]);
                break;
            case Typ.QUELLE_RECHTECK:
                //function = new TimeFunctionRect(parameter[1], parameter[2], parameter[4], parameter[3]);
            case Typ.QUELLE_VOLTAGECONTROLLED_DIRECTLY:
                voltageSource = new VoltageSourceDirect(_function, _terminals.get(0), _terminals.get(1), parameterString[1], _parameter[11], this);
                circuitComponents.clear();
                circuitComponents.add(voltageSource);
                break;
            default:
                assert false;
        }

        voltageSource.setFunction(_function);

        

    }



    @Override
    protected void setParameter(double[] parameter) {
        _parameter = parameter;
    }

    public TimeFunction getTimeFunction() {
        return _function;
    }

    public int getSourceType()
    {
        return ((int) _parameter[0]);
    }

    
}
