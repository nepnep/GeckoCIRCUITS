/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.DatenSpeicher;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;

/**
 *
 * @author muesinga
 */
public class TextFieldBlock extends ElementInterface {
    // Referenz (statisch), welche ein update des Worksheets erlaubt, falls
    // sich die eigentschaften dieser Klasse geändert haben
    private double br, ho, da, d, dm, dxTxt, dyTxt, lxTxt, lyTxt;
    /**
     * the following properties have to be saved in the .ipes-File:
     */
    private String _titleText = new String("Title");
    private String _contentsText = new String("");
    private Color _backGroundColor = new Color(245,245,245);
    private Color _fontColor = new Color(80,80,80);
    //----------


    public TextFieldBlock (CircuitSheet sheet) {
        super(sheet);
        br=5;   ho=2;   da=0.5;   d=1;   dm=5;
        dxTxt=-1.5;   dyTxt=-0.8;   lxTxt=0;   lyTxt=0;
    }

    @Override
    public void paint(Graphics2D g, int dpix) {
        super.paint(g, dpix);

        g.setColor(_backGroundColor);
        g.fillRect((int)(dpix*(-br)),(int)(dpix*(-ho)), (int)(dpix*(1.333*br)),(int)(dpix*(3.6*ho)));
        g.setColor(_fontColor);
        g.drawRect((int)(dpix*(-br)),(int)(dpix*(-ho)), (int)(dpix*(1.333*br)),(int)(dpix*(3.6*ho)));

        g.drawString(_titleText, (int)(dpix*(-br)+d),(int)(dpix*(( - ho) - 0.2)));

         String split[];
         split = _contentsText.split("\\n");

         int lineCounter = 0;
         for(String lineString : split) {
            g.drawString(lineString, (int)(dpix*(-br)+d),-15 + lineCounter * (g.getFont().getSize() + 3) + (int)(dpix*(( - ho) + 2)));
            lineCounter++;
         }

    }


    public Color getBackGroundColor() {
        return _backGroundColor;
    }

    public void setBackGroundColor(Color _backGroundColor) {
        this._backGroundColor = _backGroundColor;
        repaint();
    }

    public Color getFontColor() {
        return _fontColor;
    }

    public void setFontColor(Color _fontColor) {
        this._fontColor = _fontColor;
        repaint();
    }
    //----------

    public String getContentsText() {
        return _contentsText;
    }

    public void setContentsText(String contentsText) {
        this._contentsText = contentsText;
        repaint();
    }

    public String getTitleText() {
        return _titleText;
    }

    public void setTitleText(String titleText) {
        this._titleText = titleText;
        repaint();
    }

    public void setBreite(int newValue) {
        br = newValue;
        repaint();
    }

    public int getIntBreite() {
        return (int) br;
    }

    public double getIntHoehe() {
        return ho;
    }

    public void setHoehe(double newValue) {
        ho = newValue;
        repaint();
    }

    @Override
    protected void importASCII_Individual(String[] ascii) {


        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("back_color")) {
                _backGroundColor = new Color(DatenSpeicher.leseASCII_int(ascii[i1]));
            }
            if (ascii[i1].startsWith("front_color")) {
                _fontColor = new Color(DatenSpeicher.leseASCII_int(ascii[i1]));
            }

            if (ascii[i1].startsWith("box_breite")) {
                br = DatenSpeicher.leseASCII_int(ascii[i1]);
            }

            if (ascii[i1].startsWith("hoehe")) {
                ho = DatenSpeicher.leseASCII_double(ascii[i1]);
            }
            if (ascii[i1].startsWith("title_string")) {
                _titleText = DatenSpeicher.leseASCII_TextBlock(ascii[i1]);
            }

            if (ascii[i1].startsWith("contents_string")) {
                _contentsText = DatenSpeicher.leseASCII_TextBlock(ascii[i1]);
            }
        }

    }

    @Override
    protected void setParameter(double[] parameter) {

    }

}
