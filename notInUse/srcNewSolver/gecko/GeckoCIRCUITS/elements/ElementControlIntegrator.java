/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author andy
 */
class ElementControlIntegrator extends ElementControlTwoInputOneOutput {
    private double xalt;
    private double a1;
    private double y0;
    private double min;
    private double max;
    private double y1alt;
    private double y11;
    private double in1;
    private double in2;
    
    private double xaltInit = 0;
    private double y1altInit = 0;
    private double _xaltMaxAbs = 0;
    private double _y1altMaxAbs = 0;

    //ModelMVC versions of Integrator parameters
    @ACCESSIBLE
    public final ModelMVC<Double> A1 = new ModelMVC<Double>(1.0,"a1");
    @ACCESSIBLE
    public final ModelMVC<Double> Y0 = new ModelMVC<Double>(0.0,"y0");
    @ACCESSIBLE
    public final ModelMVC<Double> MIN = new ModelMVC<Double>(-1.0,"min");
    @ACCESSIBLE
    public final ModelMVC<Double> MAX = new ModelMVC<Double>(1.0,"max");

    
    
    public ElementControlIntegrator(CircuitSheet sheet) {
        super(sheet);
        var_history = new double[steps_saved][5];
        _baseName = "INT";

        A1.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                a1 = A1.getValue();
            }
        });

        Y0.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                y0 = Y0.getValue();
            }
        });

        MIN.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                min = MIN.getValue();
            }
        });

        MAX.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                max = MAX.getValue();
            }
        });
    }

    @Override
    protected void setParameter(double[] parameter) {
        a1 = parameter[0];
        y0 = parameter[1];
        min = parameter[2];
        max = parameter[3];

    }
    
    @Override
    public void init() {
        xalt = xaltInit;
        if (y1altInit == 0) {
            y1alt = y0;
        }
        else {
            y1alt = y1altInit; 
        }
        xaltInit = 0;
        y1altInit = 0;
    }


    @Override
    public void calculateOutput(double time, double dt) {
        double outValue = 0;
        if (_model.saveHistory && !stepped_back && time > 0)
        {
            //write existing values into history
            //do so only if we have not stepped back; otherwise they are already there
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = _termOut._potArea._value;
            var_history[0][2] = xalt;
            var_history[0][3] = y1alt;
            var_history[0][4] = y11;

        }


        in1 = _termIn1.getValue();
        in2 = _termIn2.getValue();

      

        if (in2 >= 1) {  // reset: alles auf Null bzw. Init
            xalt= 0;
            y1alt= y0;
            outValue = y0;
        } else {  // normaler Betrieb - Integration
            y11 = y1alt +0.5*a1*dt*(in1 + xalt);
            if (y11 <= min) y11=min;
            if (y11 >= max) y11=max;
            outValue = y11;
            xalt= in1;
            y1alt= y11;
        }

        if (_model.saveHistory) {
            prev_time = time;

            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
        
        if (Math.abs(xalt) > _xaltMaxAbs) {
            _xaltMaxAbs = Math.abs(xalt);
        }
        
        if (Math.abs(y1alt) > _y1altMaxAbs) {
            _y1altMaxAbs = Math.abs(y1alt);
        }

        _termOut.writeValue(outValue, time);
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1], var_history[0][0]);
            prev_time = var_history[0][0];
            xalt = var_history[0][2];
            y1alt = var_history[0][3];
            y11 = var_history[0][4];
            stepped_back = true;
            steps_reversed++;
        }
    }
    
    @Override
    public boolean hasInternalStates()
    {
        return true;
    }
    
    @Override
    public double[] getState()
    {
        return new double[] {xalt, y1alt};
    }
    
    @Override
    public void setState(double[] state)
    {
        xaltInit = state[0];
        y1altInit = state[1];
        _xaltMaxAbs = Math.abs(xaltInit);
        _y1altMaxAbs = Math.abs(y1altInit);
    }
    
    @Override 
    public double getStateVar(int index)
    {
        if (index == 0)
            return xalt;
        else if (index == 1)
            return y1alt;
        else
            return 0;
    }
    
    @Override 
    public void setStateVar(double value, int index)
    {
        if (index == 0) {
            xaltInit = value;
            _xaltMaxAbs = Math.abs(xaltInit);
        }
        else if (index == 1) {
            y1altInit = value;
            _y1altMaxAbs = Math.abs(y1altInit);
        }
    }
    
    @Override
    public double getStateVarMaxAbs(int index) {
        
        if (index == 0) {
            return _xaltMaxAbs;
        }
        else if (index == 1) {
            return _y1altMaxAbs;
        }
        else {
            return 0;
        }
    }



}
