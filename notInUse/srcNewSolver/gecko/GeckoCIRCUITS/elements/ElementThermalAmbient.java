/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.TimeFunctionConstant;
import gecko.geckocircuitsnew.circuit.VoltageSource;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.ConnectionType;
import gecko.GeckoCIRCUITS.terminal.TerminalGND;
import java.awt.Graphics2D;

/**
 *
 * @author andy
 */
public class ElementThermalAmbient extends ElementCircuit {

    private final VoltageSource voltageSource;

    public ElementThermalAmbient(CircuitSheet sheet) {
        super(sheet);
        _terminals.add(new TerminalComponentTherm(this, 0, -1));
        _terminals.add(new TerminalGND(this));

        voltageSource = new VoltageSource(new TimeFunctionConstant(0), _terminals.get(0), _terminals.get(1), this);
        circuitComponents.add(voltageSource);
        
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }

    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.THERMALCIRCUIT;
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double br=0.9,   ho=0.6,   da=0.5,   d=4,   dm=5,   lg=1.0;

        double x1=0;
        double y1=-lg;
        double x2=0;
        double y2=+lg;

        double abstand=br+0.4, dp=0.2, p1=9, p2=3, dlt=0.25;
        g.setColor(Typ.colorForeGroundElement);

        g.drawLine(0,(int)(dpix*(+0.7*lg)), 0,(int)(dpix*(-lg)));
        //-----------------
        // 'Erde'  -->
        double erX=0.5;
        int erY=3;
        g.fillRect((int)(dpix*(-erX)), (int)(dpix*(+0.7*lg)), (int)(dpix*(2*erX)), erY);

    }



}
