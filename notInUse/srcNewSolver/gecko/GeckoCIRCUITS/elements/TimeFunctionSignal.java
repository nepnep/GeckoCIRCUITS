/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.geckocircuitsnew.circuit.TimeFunction;
import gecko.GeckoCIRCUITS.controlNew.ControlPotentialValue;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControl;

/**
 *
 * @author andy
 */
class TimeFunctionSignal extends TimeFunction {
    private final String _signalLabel;
    private ControlPotentialValue _measurePotential;

    public TimeFunctionSignal(String signalLabel) {
        _signalLabel = signalLabel;
    }

    @Override
    public double calculate(double t, double dt) {
        if(_measurePotential == null) {
            for(ElementInterface elem : ElementInterface.allElements) {
                if(elem instanceof ElementControl) {
                    for(Terminal term : elem._terminals) {
                        if(term.getLabel().equals(_signalLabel)) {
                            _measurePotential = ((TerminalControl) term)._potArea;
                        }
                    }
                }
            }
        }
        return _measurePotential._value;
    }

    @Override
    public String toString() {
        return super.toString() + " " + _signalLabel;
    }

    public void stepBack() { }



}
