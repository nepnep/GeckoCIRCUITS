/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.DatenSpeicher;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import javax.tools.*;

import javax.tools.JavaFileObject.Kind;
import javax.tools.JavaCompiler.CompilationTask;

public class ElementControlJava extends ElementControl {

    private CodeWindow codeWindow = new CodeWindow(this);
    private ArrayList<TerminalControlIn> _inputTerminals = new ArrayList<TerminalControlIn>();
    private ArrayList<TerminalControlOut> _outputTerminals = new ArrayList<TerminalControlOut>();
    private boolean hasStaticVariables = false;
    private Object[][] staticVarHistory;
    private Field[] staticPrimitiveVars;
    private boolean saveStaticVarHistory = false;
    private boolean _showComponentName = true;

    public ElementControlJava(CircuitSheet sheet) {
        super(sheet);
        this.addMouseListener(new MouseListener() {

            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    codeWindow.loadSourcesText();
                    codeWindow.setVisible(true);
                }
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });

    }

    @Override
    public ControlType getType() {
        return ControlType.TRANSFER;
    }

    @Override
    public void calculateOutput(double time, double dt) {


        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            for (int i = 0; i < _outputTerminals.size(); i++)
                var_history[0][i+1] = _outputTerminals.get(i)._potArea._value;
            if (saveStaticVarHistory)
                populateStaticVarHistory();
        }

        for(int i = 0; i < xIN.length; i++) {
            xIN[i] = _inputTerminals.get(i).getValue();
        }
        berechneYOUT(xIN, dt, time);

        for(int i = 0; i < _outputTerminals.size(); i++) {
            _outputTerminals.get(i).writeValue(ausgangssignal[i], time);
        }

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }

    }

    @Override
    protected void importASCII_Individual(String[] ascii) {
        
        for (int i1 = 0; i1 < ascii.length; i1++) {
            if (ascii[i1].startsWith("tnX")) {
                tnX = DatenSpeicher.leseASCII_int(ascii[i1]);
                for (int i = 0; i < tnX; i++) {
                    TerminalControlIn in = new TerminalControlIn(-2, i-1, this);
                    _inputTerminals.add(in);
                    _terminals.add(in);
                }

                xIN = new double[tnX];

            }
            if (ascii[i1].startsWith("tnY")) {
                tnY = DatenSpeicher.leseASCII_int(ascii[i1]);

                for (int i = 0; i < tnY; i++) {
                    TerminalControlOut out = new TerminalControlOut(2, i-1, this);
                    _outputTerminals.add(out);
                    _terminals.add(out);
                }
                ausgangssignal = new double[tnY];

            }
            if (ascii[i1].startsWith("<sourceCode>")) {
                _javaSourceCode = "";
                i1++;
                while (!ascii[i1].startsWith("<\\sourceCode>")) {
                    _javaSourceCode += (ascii[i1] + "\n");
                    i1++;
                }
            }
            if (ascii[i1].startsWith("<staticCode>")) {
                _javaStaticCode = "";
                i1++;
                while (!ascii[i1].startsWith("<\\staticCode>")) {
                    _javaStaticCode += (ascii[i1] + "\n");
                    i1++;
                }
            }
            if (ascii[i1].startsWith("<importCode>")) {
                _javaImportCode = "";
                i1++;
                while (!ascii[i1].startsWith("<\\importCode>")) {
                    _javaImportCode += (ascii[i1] + "\n");
                    i1++;
                }
            }
            if (ascii[i1].startsWith("<staticVariables>")) {
                _javaStaticVariables = "";
                i1++;
                while (!ascii[i1].startsWith("<\\staticVariables>")) {
                    _javaStaticVariables += (ascii[i1] + "\n");
                    i1++;
                }
            }
        }
    }

    @Override
    protected void setParameter(double[] parameter) {
    }
    //-------------------------------------------------------------------
    // fields contain source code, that is also saved in the JAVA-object .ipes stuff
    private String _javaSourceCode = "return yOUT;";
    private String _javaImportCode = "";
    private String _javaStaticCode = "";
    private String _javaStaticVariables = "";
    //
    private String _compilerMessage = "";
    //-------------------------------------------------------------------
    // directory where class file will be located.
    private String _workingDirectory;
    private Method _externYOUT;
    private Method _initExterYOUT;
    private String idString = "JAVA_FUNCTION";
    private int tnX, tnY;  // Nummer der Terminals fuer Signal-Anschluss
    private int xKlickMinTerminal, xKlickMaxTerminal, yKlickMinTerminalADD, yKlickMaxTerminalADD, yKlickMinTerminalSUB, yKlickMaxTerminalSUB;  // Klickbereiche fuer rote Dreiecke --> Aenderung der Terminal-Anzahl
    private int xKlickMinTerminal_OUT, xKlickMaxTerminal_OUT, yKlickMinTerminalADD_OUT, yKlickMaxTerminalADD_OUT, yKlickMinTerminalSUB_OUT, yKlickMaxTerminalSUB_OUT;  // Klickbereiche fuer rote Dreiecke --> Aenderung der Terminal-Anzahl
    private String[] header;
    //
    private double[] ausgangssignal = new double[10];
    private int lineCounter = 0;
    private String _sourceString = new String();
    // needed for counting the java block objects;
    private static int _objectCounter = 0;
    private String _className;
    //

    private double[] xIN;
    
    public static enum COMPILESTATUS {

        NOT_COMPILED, COMPILED_SUCCESSFULL, COMPILE_ERROR
    };
    private COMPILESTATUS _compileStatus = COMPILESTATUS.NOT_COMPILED;
    //-------------------------------------------------------------------

    public COMPILESTATUS getCompileStatus() {
        return _compileStatus;
    }

     @SuppressWarnings("unchecked")
    public double[] berechneYOUT(double[] xIN, double dt, double time) {

        try {
            if (_compileStatus == COMPILESTATUS.NOT_COMPILED) {
                doCompilation();
            } else if (_compileStatus == COMPILESTATUS.COMPILE_ERROR) {
                return ausgangssignal;
            }

            if (time == 0) {
                _initExterYOUT.invoke(null, new Object[]{});
            }

            ausgangssignal = (double[]) _externYOUT.invoke(null, new Object[]{xIN, time, dt});

        } catch (InvocationTargetException ex) {
            // Exception in the main method that we just tried to run

            //showMsg("Exception in main: " + ex.getTargetException());
            ex.getTargetException().printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("Error: Could not invoke external Java method!");
            _compileStatus = COMPILESTATUS.COMPILE_ERROR;
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
        return ausgangssignal;
    }

//    public boolean istAngeklickt(int mx, int my) {
//        if ((xKlickMin <= mx) && (mx <= xKlickMax) && (yKlickMin <= my) && (my <= yKlickMax)) {
//            return true;  // SCOPE-Symbol ist angeklickt worden --> Dialog oder Bearbeitungs-Modus
//        } else {
//            if ((xKlickMinTerminal <= mx) && (mx <= xKlickMaxTerminal) && (yKlickMinTerminalADD <= my) && (my <= yKlickMaxTerminalADD)) {
//                // erhoehe Zahl der Terminals um Eins und aktualisiere SCOPE
//                tnX++;
//                xXIN = new int[tnX];
//                yXIN = new int[tnX];
//                //----------------
//                header = new String[1 + tnX];
//                element.setLabelAnfangsKnoten(new String[tnX]);
//            } else if ((xKlickMinTerminal_OUT <= mx) && (mx <= xKlickMaxTerminal_OUT) && (yKlickMinTerminalADD_OUT <= my) && (my <= yKlickMaxTerminalADD_OUT)) {
//                // erhoehe Zahl der Terminals um Eins und aktualisiere SCOPE
//                tnY++;
//                xYOUT = new int[tnY];
//                yYOUT = new int[tnY];
//                //----------------
//                element.setLabelEndKnoten(new String[tnY]);
//                // force recompile of external class
//                _compileStatus = COMPILESTATUS.NOT_COMPILED;
//            } else if ((xKlickMinTerminal <= mx) && (mx <= xKlickMaxTerminal) && (yKlickMinTerminalSUB <= my) && (my <= yKlickMaxTerminalSUB)) {
//                // reduziere Zahl der Terminals um Eins und aktualisiere SCOPE
//                if (tnX >= 1) {  // mindestens ein Terminal-Anschluss zwingend
//                    tnX--;
//                    xXIN = new int[tnX];
//                    yXIN = new int[tnX];
//                }
//                //----------------
//                header = new String[1 + tnX];
//                element.setLabelAnfangsKnoten(new String[tnX]);
//            } else if ((xKlickMinTerminal_OUT <= mx) && (mx <= xKlickMaxTerminal_OUT) && (yKlickMinTerminalSUB_OUT <= my) && (my <= yKlickMaxTerminalSUB_OUT)) {
//                // reduziere Zahl der Terminals um Eins und aktualisiere SCOPE
//                if (tnX > 0) {
//                    tnY--;
//                    xYOUT = new int[tnY];
//                    yYOUT = new int[tnY];
//                    // force recompile of external class
//                    _compileStatus = COMPILESTATUS.NOT_COMPILED;
//                }
//                //----------------
//                element.setLabelEndKnoten(new String[tnY]);
//            }
//            /*
//            element.setLabelAnfangsKnoten(new String[tn]);
//            element.getScope().setzeSignalDaten(zvDaten, header, zvCounterRef);
//            // default-maessig werden neu dazugeklickte Signale nicht dargestellt -->
//            for (int i1=0;  i1<GraferImplementation.ANZ_DIAGRAM_MAX;  i1++)
//            if (tn>1)  // weil sonst Error bei nur 1 Terminalanschluss
//            element.getScope().set_crvAchsenTyp_ConnectionMatrix(i1,tn,GraferImplementation.ZUORDNUNG_NIX);
//             */
//            return false;  // SCOPE-Symbol ist nicht angeklickt worden, daher 'false'
//        }
//    }
//    //==============================
//    @Override
//    protected void exportASCII_IndividualCONTROL(StringBuffer ascii) {
//        ascii.append("<detail>");
//        DatenSpeicher.appendAsString(ascii.append("\ntnX"), tnX);
//        DatenSpeicher.appendAsString(ascii.append("\ntnY"), tnY);
//        ascii.append("\n<sourceCode>\n");
//        ascii.append(_javaSourceCode);
//        ascii.append("\n<\\sourceCode>");
//        ascii.append("\n<staticCode>\n");
//        ascii.append(_javaStaticCode);
//        ascii.append("\n<\\staticCode>");
//        ascii.append("\n<importCode>\n");
//        ascii.append(_javaImportCode);
//        ascii.append("\n<\\importCode>");
//        ascii.append("\n<staticVariables>\n");
//        ascii.append(_javaStaticVariables);
//        ascii.append("\n<\\staticVariables>");
//        ascii.append("\n<\\detail>");
//    }
    public String getSourceCode() {
        return _javaSourceCode;
    }

    public String getImportCode() {
        return _javaImportCode;
    }

    public String getStaticInitCode() {
        return _javaStaticCode;
    }

    public String getCompilerMessage() {
        return _compilerMessage;
    }
    
    public String getCompilerSource() {
        return _sourceString;
    }

    public String getStaticVariables() {
        return _javaStaticVariables;
    }

    public void setStaticVariables(String staticVarCode) {
        _javaStaticVariables = staticVarCode;
    }

    public void setSourceCode(String javaSourceCode) {
        _javaSourceCode = javaSourceCode;
        _compileStatus = COMPILESTATUS.NOT_COMPILED;
    }

    public void setImportCode(final String importCode) {
        _javaImportCode = importCode;
        _compileStatus = COMPILESTATUS.NOT_COMPILED;
    }

    public void setStaticInitCode(final String staticCode) {
        _javaStaticCode = staticCode;
        _compileStatus = COMPILESTATUS.NOT_COMPILED;
    }
    
    public void setShowName(boolean value) {
        _showComponentName = value;
    }

    public boolean getShowName() {
        return _showComponentName;
    }

    /**
     * Compiles and runs the short code segment.
     *
     * @throws IOException if there was an error creating the source file.
     */
    public void doCompilation() throws IOException {
        try {
            _compilerMessage = "starting compilation....\n";
            _compilerMessage += "------------------------------- Source file text: ------------------------------------\n";

            _className = "tmpJav" + _objectCounter;
            _objectCounter++;
            String classFileName = _className + ".java";

            createSourceCode(_className);


            _compilerMessage += "------------------------------- Compiler output ------------------------------------\n";

            JavaCompiler compiler =
                    ToolProvider.getSystemJavaCompiler();

            final Map<String, JavaFileObject> output =
                    new HashMap<String, JavaFileObject>();

            DiagnosticCollector<JavaFileObject> diagnostics =
                    new DiagnosticCollector<JavaFileObject>();

            JavaFileManager jfm = new ForwardingJavaFileManager<StandardJavaFileManager>(compiler.getStandardFileManager(diagnostics, null, null)) {

                @Override
                public JavaFileObject getJavaFileForOutput(Location location,
                        String name,
                        Kind kind,
                        FileObject sibling)
                        throws IOException {
                    JavaFileObject jfo = new RAMJavaFileObject(name, kind);
                    output.put(name, jfo);
                    return jfo;
                }
            };


            OutputStream outStream = new OutputStream() {

                public void write(byte[] b) {
                }

                public void write(byte[] b, int off, int len) {
                    _compilerMessage += new String(b).substring(off, len);
                }

                public void write(int b) {
                }
            };

            PrintWriter compilerWriter = new PrintWriter(outStream, true);

            // Compile
            //_workingDirectory = (new File(Typ.DATNAM)).getParent();
            if (_workingDirectory == null) {
                _workingDirectory = System.getProperty("user.dir");
            }

              ArrayList<String> opt = new ArrayList<String>();
              opt.add("-classpath");


            if (System.getProperty("os.name").toLowerCase().contains("linux")) {
                opt.add(System.getProperty("user.dir") + ":" + _workingDirectory);
            } else {
                opt.add(System.getProperty("user.dir") + ";" + _workingDirectory);
            }

            CompilationTask task = compiler.getTask(
                    compilerWriter, jfm, diagnostics, opt, null,
                    Arrays.asList(generateJavaSourceCode(_sourceString, classFileName)));

            if (!task.call()) {
                for (Diagnostic dm : diagnostics.getDiagnostics()) {
                    compilerWriter.println(dm);
                }
                _compileStatus = COMPILESTATUS.COMPILE_ERROR;
                _compilerMessage += "Compile status: ERROR";

            } else {
                _compileStatus = COMPILESTATUS.COMPILED_SUCCESSFULL;
                _compilerMessage += "\n \tCOMPILATION FINISHED SUCESSFULLY!";
                // Try to access the class and run its main method

                ClassLoader cl = new ClassLoader() {

                    public void extendClassPath() {
                        if (!Typ.IS_APPLET) {
                            URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
                            Class<URLClassLoader> sysclass = URLClassLoader.class;

			    // careful! this seems unncessary, but when a local path for the file is given,
                            // directory will be "null!".
                            File tmpfile = new File(Typ.DATNAM);
                            File file = new File(tmpfile.getAbsolutePath());
                            File directory = file.getParentFile();

                            if (directory.isDirectory()) {
                                try {
                                    String path = directory.getAbsolutePath();
                                    URL url = new URL("file://" + path + "/");
                                    Method method = sysclass.getDeclaredMethod("addURL", URL.class);
                                    method.setAccessible(true);
                                    method.invoke(sysloader, new Object[]{url});
                                } catch (IllegalAccessException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (IllegalArgumentException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (InvocationTargetException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (NoSuchMethodException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (SecurityException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (MalformedURLException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    }

                    @Override
                    protected Class<?> findClass(String name) throws
                            ClassNotFoundException {

                        extendClassPath();

                        JavaFileObject jfo = output.get(name);
                        if (jfo != null) {
                            byte[] bytes = ((RAMJavaFileObject) jfo).baos.toByteArray();
                            return defineClass(name, bytes, 0, bytes.length);
                        }

                        Class<?> cl = super.findClass(name);
                        return cl;
                    }
                };

                Class<?> c = Class.forName(_className, false, cl);
                Class clazz = c;// Class.forName(_className, true, urlCl);

                Class[] partypes = new Class[3];
                Class[] initpartypes = new Class[0];
                double[] tmp = new double[]{1, 2, 3};
                partypes[0] = tmp.getClass();
                partypes[1] = Double.TYPE;
                partypes[2] = Double.TYPE;
                try {

                    _externYOUT = clazz.getMethod("berechneYOUT", partypes);
                    _initExterYOUT = clazz.getMethod("init", initpartypes);

                    if (_externYOUT == null) {
                        System.err.println("could not set extern Java code method!");
                        _compileStatus = COMPILESTATUS.COMPILE_ERROR;
                    }

                    //get static fields (variables)
                    if (hasStaticVariables) {
                        Field[] fields = clazz.getDeclaredFields();
                        boolean hasStaticNonPrimitiveType = false;
                        ArrayList<Field> staticFields = new ArrayList<Field>();
                        int field_modifier;
                        for (int i = 0; i < fields.length; i++)
                        {
                            field_modifier = fields[i].getModifiers();
                            if (java.lang.reflect.Modifier.isStatic(field_modifier))
                            {
                                staticFields.add(fields[i]);
                                if (!(fields[i].getType().isPrimitive()))
                                {
                                    if (!(fields[i].getName().equals("yOUT"))) {
                                    hasStaticNonPrimitiveType = true;
                                    System.out.println(fields[i].getType().getSimpleName() + " " + fields[i].getName() + " " + _baseName + " " + _elementName);
                                    break; //cannot do stepping back with primitive types
                                    }
                                }
                            }

                            if (!hasStaticNonPrimitiveType)
                            {
                                hasStaticVariables = false; //all the static variables are primitive, so we can step them back
                                staticPrimitiveVars = (Field[]) staticFields.toArray(new Field[1]);
                                staticVarHistory = new Object[steps_saved][staticFields.size()];
                                saveStaticVarHistory = true;
                            }
                        }
                }

                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }

            }

            // repaint schematic entry - because color of JavaCode-Block could change
            repaint();
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        var_history = new double[steps_saved][_outputTerminals.size()+1];
    }

    public static JavaFileObject generateJavaSourceCode(final String sourceText, final String className) {

        return new SimpleJavaFileObject(toURI(className), JavaFileObject.Kind.SOURCE) {

            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors)
                    throws IOException, IllegalStateException,
                    UnsupportedOperationException {
                return sourceText;
            }
        };
    }

    class RAMJavaFileObject extends SimpleJavaFileObject {

        RAMJavaFileObject(String name, Kind kind) {
            super(toURI(name), kind);
        }
        ByteArrayOutputStream baos;

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors)
                throws IOException, IllegalStateException,
                UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

        @Override
        public InputStream openInputStream() throws IOException,
                IllegalStateException, UnsupportedOperationException {
            return new ByteArrayInputStream(baos.toByteArray());
        }

        @Override
        public OutputStream openOutputStream() throws IOException,
                IllegalStateException, UnsupportedOperationException {
            return baos = new ByteArrayOutputStream();
        }
    }

    private static URI toURI(String name) {
        try {
            return new URI(name);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private void createSourceCode(final String className) {
        try {
            lineCounter = 0;
            _sourceString = "";

            String strLine;
            BufferedReader reader = new BufferedReader(new StringReader(_javaImportCode));
            while ((strLine = reader.readLine()) != null) {
                appendSourcLine("\t\t" + strLine);
            }

            appendSourcLine("/**");
            appendSourcLine(" * Source created on " + new Date());
            appendSourcLine(" */");
            appendSourcLine("public class " + className + " { ");
            appendSourcLine("// static variables: ");

            reader = new BufferedReader(new StringReader(_javaStaticVariables));
            if (!_javaStaticVariables.equals("") || !_javaStaticVariables.isEmpty())
                hasStaticVariables = true;
            while ((strLine = reader.readLine()) != null) {
                appendSourcLine("\t\t" + strLine);
            }
            appendSourcLine("private static double[] yOUT = new double[" + tnY + "];");
            appendSourcLine("public static void init() {");
            reader = new BufferedReader(new StringReader(_javaStaticCode));
            while ((strLine = reader.readLine()) != null) {
                appendSourcLine("\t\t" + strLine);
            }
            appendSourcLine("}");
            appendSourcLine("    public static double[] berechneYOUT(final double[] xIN, final double time, final double dt) throws Exception {");
            appendSourcLine("// Your short code segment");
            appendSourcLine("// ****************** your code segment **********************");
            reader = new BufferedReader(new StringReader(_javaSourceCode));
            while ((strLine = reader.readLine()) != null) {
                appendSourcLine("\t\t" + strLine);
            }

            appendSourcLine("// ****************** end of code segment **********************");
            appendSourcLine("    }");
            appendSourcLine("}");
            //
            //System.out.println("createSourceCode() --> \n\n_compilerMessage= \n"+_compilerMessage+"\n\n===========\n_sourceString= \n"+_sourceString+"\n\n===========\n");
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void appendSourcLine(String newLine) {
        lineCounter++;
        //_compilerMessage += lineCounter + " " + newLine + "\n";
        _sourceString += newLine + "\n";

    }

    public void stepBack()
    {
        if (!hasStaticVariables && ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved))))
        {
            if (stepped_back)
                historyBackward();
            for (int i = 0; i < _outputTerminals.size(); i++)
                _outputTerminals.get(i).writeValue(var_history[0][i+1],var_history[0][0]);
            prev_time = var_history[0][0];
            if (saveStaticVarHistory)
                readStaticVarHistory();
            stepped_back = true;
            steps_reversed++;
        }
        else if (hasStaticVariables) {
            System.out.println("Cannot use variable step width with Java Blocks that have static non-primitive variables at the moment!");
        }
    }

    private void populateStaticVarHistory()
    {
        for (int i = 0; i < staticVarHistory[0].length; i++)
            staticVarHistory[0][i] = createNewStaticFieldObject(staticPrimitiveVars[i]);
    }

    private Object createNewStaticFieldObject(Field field)
    {
        Object primitive_wrapper = null;
        String name = field.getType().getSimpleName();
        try {
        if (name.equals("int"))
            primitive_wrapper = new Integer(field.getInt(null));
        else if (name.equals("long"))
            primitive_wrapper = new Long(field.getLong(null));
        else if (name.equals("double"))
            primitive_wrapper = new Double(field.getDouble(null));
        else if (name.equals("byte"))
            primitive_wrapper = new Byte(field.getByte(null));
        else if (name.equals("char"))
            primitive_wrapper = new Character(field.getChar(null));
        else if (name.equals("short"))
            primitive_wrapper = new Short(field.getShort(null));
        else if (name.equals("float"))
            primitive_wrapper = new Float(field.getFloat(null));
        else if (name.equals("boolean"))
            primitive_wrapper = new Boolean(field.getBoolean(null));
        }
        catch (Exception ex)
        {
            System.err.println(ex);
            System.out.println("Can't identify static primitive variable type in java block: " + name);
        }
        
        return primitive_wrapper;
    }
    
    private void readStaticVarHistory()
    {
        try {
        String type;
        for (int i = 0; i < staticVarHistory[0].length; i++)
        {
            type = staticPrimitiveVars[i].getType().getSimpleName();
            if (type.equals("int"))
                staticPrimitiveVars[i].setInt(null, ((Integer)staticVarHistory[0][i]).intValue());
            else if (type.equals("long"))
                staticPrimitiveVars[i].setLong(null, ((Long)staticVarHistory[0][i]).longValue());
            else if (type.equals("double"))
                staticPrimitiveVars[i].setDouble(null, ((Double)staticVarHistory[0][i]).doubleValue());
            else if (type.equals("byte"))
                staticPrimitiveVars[i].setByte(null, ((Byte)staticVarHistory[0][i]).byteValue());
            else if (type.equals("char"))
                staticPrimitiveVars[i].setChar(null, ((Character)staticVarHistory[0][i]).charValue());
            else if (type.equals("short"))
                staticPrimitiveVars[i].setShort(null, ((Short)staticVarHistory[0][i]).shortValue());
            else if (type.equals("float"))
                staticPrimitiveVars[i].setFloat(null, ((Float)staticVarHistory[0][i]).floatValue());
            else if (type.equals("boolean"))
                staticPrimitiveVars[i].setBoolean(null, ((Boolean)staticVarHistory[0][i]).booleanValue());
        }
        }
        catch (Exception ex)
        {
            System.err.println(ex);
            System.out.println("cannot access static variables history");
        }
    }

    @Override
    protected void historyForward()
    {
      for (int j = var_history.length - 1; j > 0; j--)
        for (int i = 0; i < var_history[0].length; i++)
            var_history[j][i] = var_history[j-1][i];

      if (saveStaticVarHistory) {
      for (int j = staticVarHistory.length - 1; j > 0; j--)
        for (int i = 0; i < staticVarHistory[0].length; i++)
            staticVarHistory[j][i] = staticVarHistory[j-1][i]; }
    }

    @Override
    protected void historyBackward()
    {
      for (int j = var_history.length - 1; j > 0; j--)
        for (int i = 0; i < var_history[0].length; i++)
            var_history[j-1][i] = var_history[j][i];

      if (saveStaticVarHistory) {
      for (int j = staticVarHistory.length - 1; j > 0; j--)
        for (int i = 0; i < staticVarHistory[0].length; i++)
            staticVarHistory[j-1][i] = staticVarHistory[j][i]; }
    }
}
