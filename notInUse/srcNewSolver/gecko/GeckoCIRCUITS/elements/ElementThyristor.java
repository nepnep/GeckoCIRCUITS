/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.Thyristor;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;
import gecko.geckocircuitsnew.circuit.SwitchState;

/**
 *
 * @author andy
 */
public class ElementThyristor extends ElementCircuit {
    @OPTIMIZABLE
    public ModelMVC<Double> _rON = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_ON,"Ron","ohm");
    @OPTIMIZABLE
    public ModelMVC<Double> _rOFF = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_OFF,"Roff","ohm");
    @OPTIMIZABLE
    public ModelMVC<Double> _uForward = new ModelMVC<Double>(CircuitComponent.DEFAULT_U_FORWARD,"UF","V");
    @OPTIMIZABLE
    public ModelMVC<Double> _trRR = new ModelMVC<Double>(0.0,"t_rr","sec");
    private final Thyristor _thyristor;

    public ElementThyristor(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        _thyristor = new Thyristor(_terminals.get(0), _terminals.get(1), this);

        _rON.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _thyristor.setROn(_rON.getValue());
            }
        });


        _uForward.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _thyristor.setUForward(_uForward.getValue());
            }
        });

        _rOFF.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _thyristor.setROff(_rOFF.getValue());
            }
        });


        _trRR.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                _thyristor.setTRR(_trRR.getValue());
            }
        });

        circuitComponents.add(_thyristor);

    }


    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        _rON.setValue(parameter[2]);
        _rOFF.setValue(parameter[3]);
        _trRR.setValue(parameter[9]);
        _uForward.setValue(parameter[1]);
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg = 2.0,   br = 0.5,   ho = 0.6,   da = 0.5;
        int dm = 4, d = 2;
        Color oldColor = g.getColor();
        g.fillRect(0, (int) (-dpix * 0.1), (int) (dpix * 0.7), 2);
        g.drawLine((int) 0, (int) (dpix * lg), 0, (int) (-dpix * lg));
        
        g.setColor(Typ.farbeElementLKHintergrund);
        g.fillPolygon(new int[]{(int) (dpix * (0)), (int) (dpix * ( - br)), (int) (dpix * ( + br))}, new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 3);

        g.setColor(oldColor);
        g.drawPolygon(new int[]{(int) (dpix * (0)), (int) (dpix * ( - br)), (int) (dpix * ( + br))}, new int[]{(int) (dpix * ( + ho)), (int) (dpix * ( - ho)), (int) (dpix * ( - ho))}, 3);
        g.fillRect((int) (dpix * ( - br)), (int) (dpix * ( + ho) - d), (int) (dpix * (2 * br)), d);

    }
    
    public SwitchState getState(double time) {
        return _thyristor.getState(time);
    }
    
    

}
