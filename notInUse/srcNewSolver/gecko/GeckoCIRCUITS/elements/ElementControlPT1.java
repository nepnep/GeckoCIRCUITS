/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ElementControlPT1 extends ElementControlSingleInOut {
    private double _xOld;
    private double _yOld;
    private double inValue;
    
    private double _xOldInit = 0;
    private double _yOldInit = 0;
    private double _xOldMaxAbs = 0, _yOldMaxAbs = 0;

    //ModelMVC versions of PT-1 fields
    @ACCESSIBLE
    public final ModelMVC<Double> pT = new ModelMVC<Double>(50e-6,"T");
    @ACCESSIBLE
    public final ModelMVC<Double> A1 = new ModelMVC<Double>(1.0,"a1");

    public ElementControlPT1(CircuitSheet sheet) {
        super(sheet);
        var_history = new double[steps_saved][4];
        _baseName = "PT1";

        pT.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value0 = pT.getValue();
            }
        });

        A1.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value1 = A1.getValue();
            }
        });
    }
    
    @Override
    public void init() {
        _xOld = _xOldInit;
        _yOld = _yOldInit;
        _xOldInit = 0;
        _yOldInit = 0;
    }

    @Override
    public void calculateOutput(double time, double dt) {

        if (_model.saveHistory && !stepped_back && (time > 0))
        {
           historyForward();
           var_history[0][0] = prev_time;
           var_history[0][1] = _termOut._potArea._value;
           var_history[0][2] = _xOld;
           var_history[0][3] = _yOld;
        }

        inValue = _termIn.getValue();

        final double outValue = _yOld*(2*value0-dt)/(2*value0+dt) + value1/(1+2*value0/dt)*(inValue+_xOld);
        
        _xOld= inValue;
        _yOld= outValue;
        _termOut.writeValue(outValue, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
        
        if (Math.abs(_xOld) > _xOldMaxAbs) {
            _xOldMaxAbs = Math.abs(_xOld);
        }
        
        if (Math.abs(_yOld) > _yOldMaxAbs) {
            _yOldMaxAbs = Math.abs(_yOld);
        }

    }

     public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            _xOld = var_history[0][2];
            _yOld = var_history[0][3];
            stepped_back = true;
            steps_reversed++;
        }
    }
     
    @Override
    public boolean hasInternalStates()
    {
        return true;
    }
    
    @Override
    public double[] getState()
    {
        return new double[] {_xOld, _yOld};
    }
    
    @Override
    public void setState(double[] state)
    {
        _xOldInit = state[0];
        _xOldInit = state[1];
        _xOldMaxAbs = Math.abs(_xOldInit);
        _yOldMaxAbs = Math.abs(_xOldInit);
    }
    
    @Override 
    public double getStateVar(int index)
    {
        if (index == 0)
            return _xOld;
        else if (index == 1)
            return _yOld;
        else
            return 0;
    }
    
    @Override 
    public void setStateVar(double value, int index)
    {
        if (index == 0) {
            _xOldInit = value;
            _xOldMaxAbs = Math.abs(_xOldInit);
        }
        else if (index == 1) {
            _yOldInit = value;
            _yOldMaxAbs = Math.abs(_yOldInit);
        }
    }
    
    @Override
    public double getStateVarMaxAbs(int index) {
        
        if (index == 0) {
            return _xOldMaxAbs;
        }
        else if (index == 1) {
            return _yOldMaxAbs;
        }
        else {
            return 0;
        }
    }
     

}
