/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.Resistor;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;

public class ElementResistor extends ElementCircuit {

    @OPTIMIZABLE
    public final ModelMVC<Double> resistance = new ModelMVC<Double>(1000.0,"R","ohm");
    private final Resistor resistor;

    
    public ElementResistor(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();

        resistor = new Resistor(_terminals.get(0), _terminals.get(1), this);
        resistance.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                resistor.setResistance(resistance.getValue());
            }
        });
        circuitComponents.add(resistor);
    }

    
    @Override
    protected void importASCII_Individual(String ascii[]) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        resistance.setValue(parameter[0]);
    }

    @Override
    public String toString() {
        return super.toString() + " " + resistance.getValue();
    }

    @Override
    void paintCircuitComponent(Graphics2D g, int dpix) {
        
        int lg = 2;
        double da = 0.5;
        double d = 4;
        double dm = 5;
        double br = 0.35;
        double ho = 0.8;
        g.drawLine(0, -dpix * lg, 0, dpix * lg);
        Color oldColor = g.getColor();


        g.setColor(Typ.colorBackGroundElement);
        g.fillRect((int) (-dpix * br), (int) (-dpix * ho), (int) (dpix * 2 * br), (int) (dpix * 2 * ho));

        g.setColor(Typ.colorForeGroundElement);
        g.drawRect((int) (-dpix * br), (int) (-dpix * ho), (int) (dpix * 2 * br), (int) (dpix * 2 * ho));

    }



}
