/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author andy
 */
public class ElementControlThyristorSync extends ElementControl {

    private final TerminalControlIn _inputSignalAlpha;
    private final TerminalControlIn _synchr;
    private double _time;
    private TerminalControlOut[] _output = new TerminalControlOut[6];

    public ElementControlThyristorSync(CircuitSheet sheet) {
        super(sheet);
        _baseName = "THYR";
        var_history = new double[steps_saved][19];
        for (int i = 0; i < 6; i++) {
            _output[i] = new TerminalControlOut(1, -1 + i, this);
            _terminals.add(_output[i]);
        }

        _inputSignalAlpha = new TerminalControlIn(-2, -1, this);
        _synchr = new TerminalControlIn(-2, 0, this);
        _terminals.add(_inputSignalAlpha);
        _terminals.add(_synchr);


    }

    @Override
    public ControlType getType() {
        return ControlType.TRANSFER;
    }

    @Override
    public final void calculateOutput(final double time, final double dt) {

        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            oldGateEvents.addFirst((ArrayList<GateEvent>) gateEvents.clone());
            if (oldGateEvents.size() > steps_saved)
                oldGateEvents.removeLast();
            var_history[0][0] = prev_time;
            var_history[0][1] = _time;
            var_history[0][2] = _synchFreq;
            var_history[0][3] = lastOnTimePoint[0];
            var_history[0][4] = lastOnTimePoint[1];
            var_history[0][5] = lastOnTimePoint[2];
            var_history[0][6] = lastOnTimePoint[3];
            var_history[0][7] = lastOnTimePoint[4];
            var_history[0][8] = lastOnTimePoint[5];
            var_history[0][9] = lastFallingZero;
            var_history[0][10] = lastRisingZero;
            var_history[0][11] = synchTime;
            var_history[0][12] = synchOld;
            var_history[0][13] = _output[0]._potArea._value;
            var_history[0][14] = _output[1]._potArea._value;
            var_history[0][15] = _output[2]._potArea._value;
            var_history[0][16] = _output[3]._potArea._value;
            var_history[0][17] = _output[4]._potArea._value;
            var_history[0][18] = _output[5]._potArea._value;
        }


        _time = time;
        if (time < 1e-20) {
            _synchFreq = _initFreq;
            gateEvents.clear();
            lastOnTimePoint = new double[]{-1, -1, -1, -1, -1, -1};
            lastFallingZero = -1;
            lastRisingZero = -1;
            synchTime = 0;
            synchOld = 0;
        }

        double signal = _synchr.getValue();

        if (synchOld <= 0 && signal >= 0 && synchOld != signal) {
            synchTime = time;


            if (lastFallingZero > 0 && (time - lastFallingZero) != 0) {
                _synchFreq = 1.0 / (time - lastFallingZero);
            }

            if (lastRisingZero > lastFallingZero) {
                _synchFreq = 0.5 / (time - lastRisingZero);
            }

            lastFallingZero = time;
        }


        if (time == 0) {
            for (int i = 0; i < 6; i++) {
                stopTimes[i] = 0;
            }
        }


        double alpha = (_phaseShift + _inputSignalAlpha.getValue()) / 180 * Math.PI;
        int n = (int) (time / (1 / _synchFreq));

        for (int i = 0; i < 6; i++) {
            double onTimePoint = onTimePoint = (synchTime + 1 / _synchFreq * alpha / (2 * Math.PI) + (i - 6) * 1 / (3 * _synchFreq));


            if (i > 2) {
                onTimePoint -= 1.5 / _synchFreq;
            }

            if (lastOnTimePoint[i] != onTimePoint) {
                gateEvents.add(new GateEvent(onTimePoint, onTimePoint + _onTime, i));
            }

            lastOnTimePoint[i] = onTimePoint;
        }

        ArrayList<GateEvent> removeEvents = new ArrayList<GateEvent>();
        for (GateEvent ge : gateEvents) {
            GateEvent toRemove = ge.processEvent(time);
            if (toRemove != null) {
                removeEvents.add(toRemove);
            }
        }
        gateEvents.removeAll(removeEvents);

        synchOld = signal;

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back) {
                historyBackward();
                oldGateEvents.removeFirst();
            }
            prev_time = var_history[0][0];
            _output[0].writeValue(var_history[0][13], var_history[0][0]);
            _output[1].writeValue(var_history[0][14], var_history[0][0]);
            _output[2].writeValue(var_history[0][15], var_history[0][0]);
            _output[3].writeValue(var_history[0][16], var_history[0][0]);
            _output[4].writeValue(var_history[0][17], var_history[0][0]);
            _output[5].writeValue(var_history[0][18], var_history[0][0]);
            _time = var_history[0][1];
            _synchFreq = var_history[0][2];
            lastOnTimePoint[0] = var_history[0][3];
            lastOnTimePoint[1] = var_history[0][4];
            lastOnTimePoint[2] = var_history[0][5];
            lastOnTimePoint[3] = var_history[0][6];
            lastOnTimePoint[4] = var_history[0][7];
            lastOnTimePoint[5] = var_history[0][8];
            lastFallingZero = var_history[0][9];
            lastRisingZero = var_history[0][10];
            synchTime = var_history[0][11];
            synchOld = var_history[0][12];
            gateEvents = oldGateEvents.getFirst();
            stepped_back = true;
            steps_reversed++;
        }
    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
    }
    private DialogThyristorControl dtc;
    private int tnX = 2, tnY = 6;  // Nummer der Terminals fuer Signal-Anschluss
    private double[] ausgangssignal = new double[10];
    private double _initFreq = 50;
    private double _synchFreq;
    private double _onTime = 4e-3;
    private double[] stopTimes = new double[6];
    private double _phaseShift = 30;
    private double lastFallingZero = -1;
    private double lastRisingZero = -1;
    private double synchTime;
    private double synchOld = 0;
    private ArrayList<GateEvent> gateEvents = new ArrayList<GateEvent>();
    private LinkedList<ArrayList<GateEvent>> oldGateEvents = new LinkedList<ArrayList<GateEvent>>();
    private double[] lastOnTimePoint = new double[]{-1, -1, -1, -1, -1, -1};

    public void setInitFreq(double initFreq) {
        System.out.println("new freq");
        _initFreq = initFreq;
    }

    public double getInitFreq() {
        return _initFreq;
    }

    public void setPhaseShift(double phaseShift) {
        _phaseShift = phaseShift;
    }

    public double getPhaseShift() {
        return _phaseShift;
    }

    public double getOnTime() {
        return _onTime;
    }

    public void setOnTime(double onTime) {
        _onTime = onTime;
    }

    class GateEvent {

        private double _onTime;
        private double _offTime;
        private int _gateNumber;
        private final double EPSILON = 1e-10;

        public GateEvent(double onTime, double offTime, int gateNumber) {
            _onTime = onTime;
            _offTime = offTime;

            if (_onTime <= 0 && _time == 0) {
                gateEvents.add(new GateEvent(1e-20, offTime - onTime, gateNumber));
            }

            while (_time > _onTime + EPSILON) {
                _onTime += 1.0 / _synchFreq;
                _offTime += 1.0 / _synchFreq;
            }

            _gateNumber = gateNumber;
        }

        private boolean onWritten = false;

        public GateEvent processEvent(double time) {

            if (time > _onTime /*&& !onWritten*/) {
                onWritten = true;
                _output[_gateNumber].writeValue(1, time);
            }

            if (time > _offTime) {
                _output[_gateNumber].writeValue(0, time);
                return this;
            }
            return null;
        }
    }

    public void showWindow() {
        if (dtc == null) {
            dtc = new DialogThyristorControl(this);
        }
        dtc.setVisible(true);
    }
}
