/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.CircuitComponent;
import gecko.geckocircuitsnew.circuit.IdealSwitch;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import geckocircuitsnew.OPTIMIZABLE;
import gecko.geckocircuitsnew.circuit.SwitchState;

/**
 *
 * @author andy
 */
public class ElementIdealSwitch  extends ElementCircuit {

    @OPTIMIZABLE
    public final ModelMVC<Double> _rON = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_ON,"Ron","ohm");
    @OPTIMIZABLE
    public final ModelMVC<Double> _rOFF = new ModelMVC<Double>(CircuitComponent.DEFAULT_R_OFF,"Roff","ohm");

    private final IdealSwitch iswitch;

    public ElementIdealSwitch(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();
        iswitch = new IdealSwitch(_terminals.get(0), _terminals.get(1), this);

        _rON.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                iswitch.setROn(_rON.getValue());
            }
        });


        _rOFF.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                iswitch.setROff(_rOFF.getValue());
            }
        });

        circuitComponents.add(iswitch);

    }
    

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        _rON.setValue(parameter[1]);
        _rOFF.setValue(parameter[2]);
    }

    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg=2.0;   double br=1.6;   double ho=0.8;


        double dd=0.25, alpha= Math.atan(ho/br), ddx=dd*Math.cos(alpha), ddy=dd*Math.sin(alpha);
            g.drawLine(0,(int)(-dpix*lg), 0,(int)(-dpix*br/2));
            g.drawLine(0,(int)(dpix*lg), 0,(int)(dpix*br/2));
            g.fillPolygon(
                new int[]{0,(int)(dpix*ddx),(int)(dpix*(ho+ddx)),(int)(dpix*(ho))},
                new int[]{(int)(dpix*br/2),(int)(dpix*(br/2+ddy)),(int)(dpix*(-br/2+ddy)),(int)(-dpix*br/2)}, 4);

    }
    
    public SwitchState getState(double time) {
        return iswitch.getState(time);
    }

    

}
