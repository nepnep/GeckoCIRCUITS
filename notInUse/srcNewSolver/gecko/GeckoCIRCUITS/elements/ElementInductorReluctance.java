/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import Utilities.ModelMVC.ModelMVC;
import gecko.GeckoCIRCUITS.allg.Typ;
import gecko.geckocircuitsnew.circuit.TimeFunctionConstant;
import gecko.geckocircuitsnew.circuit.AbstractVoltageSource;
import gecko.geckocircuitsnew.circuit.VoltageSourceCurrentControlled;
import gecko.geckocircuitsnew.circuit.VoltageSourceDIDTControlled;
import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalComponentReluctance;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
class ElementInductorReluctance extends ElementCircuit {

    public final ModelMVC<Double> windings = new ModelMVC<Double>(1.0);

    private final TerminalComponentReluctance terminalReluctance1;
    private final TerminalComponentReluctance terminalReluctance2;
    //private final VoltageSourceDIDTControlled component1;
    //private final VoltageSourceCurrentControlled component2;

    public ElementInductorReluctance(CircuitSheet sheet) {
        super(sheet);
        super.initTwoTerminalComponent();

        terminalReluctance1 = new TerminalComponentReluctance(this, -2, 2);
        terminalReluctance2 = new TerminalComponentReluctance(this, -2, -2);

        final VoltageSourceDIDTControlled component1 = new VoltageSourceDIDTControlled( _terminals.get(0), _terminals.get(1), this);
        final VoltageSourceCurrentControlled component2 = new VoltageSourceCurrentControlled(terminalReluctance1, terminalReluctance2, this);

        component2.setCurrentControlComponent(component1);
        component1.setCurrentControlComponent(component2);

        circuitComponents.add(component1);
        circuitComponents.add(component2);
        _terminals.add(terminalReluctance1);
        _terminals.add(terminalReluctance2);

        windings.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                component1.setGain(windings.getValue());
                component2.setGain(windings.getValue());
            }
        });

    }


    @Override
    public void paintCircuitComponent(Graphics2D g, int dpix) {

        double lg=2.0;   double da=0.5;   int d=4;   int dm=5;   double br=0.35;   double ho=0.8;

        double rq = Math.round(dpix * (2 * ho) / 7.0);
        int yq = -1;

        BasicStroke stroke = new BasicStroke((float) 2.0);
        g.setStroke(stroke);
        g.setPaint(Typ.colorReluctanceForeGround);
        g.drawLine(-2 * dpix, dpix * 2, -2 * dpix, (int) (-dpix * 2));

        g.setPaint(Typ.farbeFertigElementLK);
        g.setStroke(new BasicStroke((float) 2.0));
        for (yq = (int) (dpix * (- ho)); yq <= (int) (dpix * ( + ho)); yq += (int) (2 * rq)) {
            g.drawArc((int) (-dpix * 1.8 + - 2.0 * rq), (int) (yq - rq), (int) (4 * rq), (int) (2 * rq), 90, 180);
        }
        g.setStroke(new BasicStroke((float) 1.0));
        g.drawLine(0, (int) (dpix * ( - lg)), 0, (int) (-dpix * ho - rq));
        g.drawLine(0, (int) (yq - rq), 0, (int) (dpix * lg));

        g.drawLine(-2 * dpix, (int) (dpix * lg-dpix), 0, (int) (dpix * lg-dpix));
        g.drawLine(-2 * dpix, (int) (-dpix * ho - rq), 0, (int) (-dpix * ho - rq));

        
        

    }

    @Override
    protected void importASCII_Individual(String[] asciiBlock) {
    }

    @Override
    protected void setParameter(double[] parameter) {
        windings.setValue(parameter[0]);
    }

}
