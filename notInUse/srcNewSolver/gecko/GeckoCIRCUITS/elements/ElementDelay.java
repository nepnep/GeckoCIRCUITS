/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.util.LinkedList;
import geckocircuitsnew.ACCESSIBLE;
import Utilities.ModelMVC.ModelMVC;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author andy
 */
public class ElementDelay extends ElementControlSingleInOut {
    
    @ACCESSIBLE
    public final ModelMVC<Double> delay = new ModelMVC<Double>(10e-6,"T_delay","sec");

    private LinkedList<TimeValues> fifo = new LinkedList<TimeValues>();
    private LinkedList<TimeValues> fifo_history = new LinkedList<TimeValues>();

    public ElementDelay(CircuitSheet sheet) {
        super(sheet);
        _baseName = "del";
        var_history = new double[steps_saved][3];
        delay.addModelListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                value0 = delay.getValue();
            }
        });
    }
    
    @Override
    public void calculateOutput(double time, double dt) {

        if (_model.saveHistory && !stepped_back && (time > 0))
        {
            historyForward();
            var_history[0][0] = prev_time;
            var_history[0][1] = _termOut._potArea._value;
        }

        double inValue = _termIn.getValue();

        if (dt < value0)
        {
            if (fifo.size() > 0)
                fifo.removeAll(fifo);
            _termOut.writeValue(inValue, time);
        }
        else
        {
            fifo.add(new TimeValues((float) time, (float) inValue));
            TimeValues startValue = fifo.getFirst();
            boolean stepHistoryEmpty = true;
            while (startValue.time <= time - value0) {
                    TimeValues removedValue = fifo.removeFirst();
                    _termOut.writeValue(startValue.value.doubleValue(), time);
                    startValue = fifo.getFirst();
                    if (_model.saveHistory && !stepped_back && (time > 0))
                    {
                        fifo_history.add(removedValue);
                        if (stepHistoryEmpty)
                        {
                            //index where first element removed in this step is stored in the history
                            var_history[0][3] = fifo_history.indexOf(removedValue);
                            stepHistoryEmpty = false;
                        }

                    }
                
            }
        }

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            fifo.removeLast();
            TimeValues restoredValue = fifo_history.getLast();
            while (fifo_history.indexOf(restoredValue) >= var_history[0][3])
            {
                fifo.addFirst(fifo_history.removeLast());
                restoredValue = fifo_history.getLast();
            }
            stepped_back = true;
            steps_reversed++;
        }
    }

    class TimeValues{
        public TimeValues(float time, float value) {
            this.time = time;
            this.value = value;
        }
        float time;
        Number value;
    }
   
}
