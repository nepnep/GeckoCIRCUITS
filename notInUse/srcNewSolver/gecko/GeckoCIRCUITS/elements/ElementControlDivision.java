/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.sheet.CircuitSheet;

/**
 *
 * @author andy
 */
class ElementControlDivision extends ElementControlTwoInputOneOutput {

    double zaehler;
    double nenner;

    public ElementControlDivision(CircuitSheet sheet) {
        super(sheet);
        _baseName = "DIV";
        var_history = new double[steps_saved][2];
    }

    @Override
    public void calculateOutput(double time, double dt) {
        if (_model.saveHistory && !stepped_back && time > 0)
        {
           historyForward();
           var_history[0][0] = prev_time;
           var_history[0][1] = _termOut._potArea._value;
           
        }

        zaehler = _termIn1.getValue();
        nenner = _termIn2.getValue();

        double outValue = 0;
        if ((zaehler == 0)&&( nenner == 0 ))  outValue = 0;  // Div. "0/0"
        else if (nenner == 0)  outValue = 1e18;  // eigentlich Unendlich
        else outValue= zaehler/nenner;  // korrekte Division
        _termOut.writeValue(outValue, time);

        if (_model.saveHistory) {
            prev_time = time;
            if (stepped_back)
                stepped_back = false;
            if (steps_reversed != 0)
                steps_reversed--; }
    }

    public void stepBack()
    {
        if ((!stepped_back && (steps_reversed == 0)) || (stepped_back && (steps_reversed < steps_saved)))
        {
            if (stepped_back)
                historyBackward();
            _termOut.writeValue(var_history[0][1],var_history[0][0]);
            prev_time = var_history[0][0];
            stepped_back = true;
            steps_reversed++;
        }
    }

}
