/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.elements;

import gecko.GeckoCIRCUITS.allg.Typ;
import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author andy
 */
public class ElementLabel extends JLabel {

    private Font _font = new Font("Arial", Font.PLAIN, 9);


    public ElementLabel() { 
        super("<html>das ist ein <P> Text</html>");
        setBounds(300, 300, 100, 50);
        setForeground(Typ.farbeFertigElementLK);
        setFont(_font);
        
    }



}
