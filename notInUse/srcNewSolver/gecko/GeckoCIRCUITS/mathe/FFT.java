/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.mathe;

import java.util.Vector; 



public class FFT extends Object {

    //------------------------------------------------
    //private static final double ln10= Math.log(10);
    //------------------------------------------------


    public FFT () {
       test(); 
    }

    
    
    // Replaces data[1..2*nn] by its discrete Fourier transform, if isign is input as 1; or replaces
    // data[1..2*nn] by nn times its inverse discrete Fourier transform, if isign is input as −1.
    // data is a complex array of length nn or, equivalently, a real array of length 2*nn. nn MUST
    // be an integer power of 2 (this is not checked for!).
    // 
    public static void four1 (float data[], int nn, int isign) {
        int n,mmax,m,j,istep,i;
        double wtemp,wr,wpr,wpi,wi,theta;  // Double precision for the trigonometric recurrences. 
        float tempr,tempi; 
        n=nn << 1;
        j=1;
        for (i=1;  i<n;  i+=2) {  // This is the bit-reversal section of the routine.
            if (j > i) {  // Exchange the two complex numbers. 
                float temp; 
                temp=data[j];    data[j]=data[i];      data[i]=temp; 
                temp=data[j+1];  data[j+1]=data[i+1];  data[i+1]=temp; 
            }
            m= nn;
            while ((m >= 2) && (j > m)) {
                j -= m;
                m >>= 1;
            }
            j += m;
        }
        // Here begins the Danielson-Lanczos section of the routine.
        mmax=2;
        while (n > mmax) {  // Outer loop executed log2 nn times.
            istep= mmax << 1;
            theta= isign*(6.28318530717959/mmax);  // Initialize the trigonometric recurrence.
            wtemp= Math.sin(0.5*theta);
            wpr = -2.0*wtemp*wtemp;
            wpi= Math.sin(theta);
            wr= 1.0;
            wi= 0.0;
            for (m=1;  m<mmax;  m+=2) {  // Here are the two nested inner loops.
                for (i=m;  i<=n;  i+=istep) {
                    j=i+mmax;  // This is the Danielson-Lanczos for mula:
                    tempr= (float)(wr*data[j]-wi*data[j+1]); 
                    tempi= (float)(wr*data[j+1]+wi*data[j]);
                    data[j]= data[i]-tempr;
                    data[j+1]= data[i+1]-tempi;
                    data[i] += tempr;
                    data[i+1] += tempi;
                }
                wr= (wtemp=wr)*wpr-wi*wpi+wr;  // Trigonometric recurrence.
                wi= wi*wpr+wtemp*wpi+wi;
            }
            mmax=istep;
        }
    }
    
    
        
    // double-Version von four1()
    // 
    public static void dfour1 (double data[], int nn, int isign) {
        int n,mmax,m,j,istep,i;
        double wtemp,wr,wpr,wpi,wi,theta;  // Double precision for the trigonometric recurrences. 
        double tempr,tempi; 
        n=nn << 1;
        j=1;
        for (i=1;  i<n;  i+=2) {  // This is the bit-reversal section of the routine.
            if (j > i) {  // Exchange the two complex numbers. 
                double temp; 
                temp=data[j];    data[j]=data[i];      data[i]=temp; 
                temp=data[j+1];  data[j+1]=data[i+1];  data[i+1]=temp; 
            }
            m= nn;
            while ((m >= 2) && (j > m)) {
                j -= m;
                m >>= 1;
            }
            j += m;
        }
        // Here begins the Danielson-Lanczos section of the routine.
        mmax=2;
        while (n > mmax) {  // Outer loop executed log2 nn times.
            istep= mmax << 1;
            theta= isign*(6.28318530717959/mmax);  // Initialize the trigonometric recurrence.
            wtemp= Math.sin(0.5*theta);
            wpr = -2.0*wtemp*wtemp;
            wpi= Math.sin(theta);
            wr= 1.0;
            wi= 0.0;
            for (m=1;  m<mmax;  m+=2) {  // Here are the two nested inner loops.
                for (i=m;  i<=n;  i+=istep) {
                    j=i+mmax;  // This is the Danielson-Lanczos for mula:
                    tempr= (wr*data[j]-wi*data[j+1]); 
                    tempi= (wr*data[j+1]+wi*data[j]);
                    data[j]= data[i]-tempr;
                    data[j+1]= data[i+1]-tempi;
                    data[i] += tempr;
                    data[i+1] += tempi;
                }
                wr= (wtemp=wr)*wpr-wi*wpi+wr;  // Trigonometric recurrence.
                wi= wi*wpr+wtemp*wpi+wi;
            }
            mmax=istep;
        }
    }
    

    
        

    // cooley-tukey fft algorithm (base-4)
    // input vector : [real part][imaginary part]
    // length of input vector has to be a magnitude of 2
    // 
    public static double[][] giezendannerFFT (double[][] inputVector, boolean inverse){
        int n = inputVector.length;
        double [][] f = new double[n][2];
        //-------------------------
        if(!((n > 0) && ((n & (n - 1)) == 0))){
            System.out.println("n not a power of two: "+n);
            return null;
        }
        //-------------------------
        if (n == 0) return null;
        if (n == 1) {
            f[0][0] = inputVector[0][0];
            f[0][1] = inputVector[0][1];
        }
        if (n == 2) {
            f[0][0] = inputVector[0][0]+inputVector[1][0];
            f[0][1] = inputVector[0][1]+inputVector[1][1];
            f[1][0] = inputVector[0][0]-inputVector[1][0];
            f[1][1] = inputVector[0][1]-inputVector[1][1];
        }
        //-------------------------
        double cosT = 0;
        double sinT = 0;
        double[][] w = new double [n][2];
        if (!inverse){
            cosT = Math.cos(2.0*Math.PI/n);
            sinT = Math.sin(2.0*Math.PI/n);
        } else{
            cosT = Math.cos(2.0*Math.PI/-n);
            sinT = Math.sin(2.0*Math.PI/-n);
        }
        w[0][0] = 1.0;
        w[0][1] = 0.0;
        for (int i = 1; i < n; i++) {
            w[i][0] = w[i-1][0]*cosT + w[i-1][1]*sinT;
            w[i][1] = w[i-1][1]*cosT - w[i-1][0]*sinT;
        }
        //-------------------------
        int j = 0;
        int k = 0;
        for (int i = 0; i<n; i++) {
            f[i][0] = inputVector[j][0];
            f[i][1] = inputVector[j][1];
            k = n >> 1;
            while (j >= k && k > 0) {
                j -= k; k >>= 1;
            }
            j += k;
        }
        double[] X1 = new double[2], X2 = new double[2], X3 = new double[2], X4 = new double[2], X5 = new double[2], X6 = new double[2];
        //-------------------------
        for (int i = 0; i < n; i += 4) {
            X1[0] = f[i][0]+f[i+1][0];
            X1[1] = f[i][1]+f[i+1][1];
            X2[0] = f[i+2][0]+f[i+3][0];
            X2[1] = f[i+2][1]+f[i+3][1];
            X3[0] = f[i][0]-f[i+1][0];
            X3[1] = f[i][1]-f[i+1][1];
            X4[0] = f[i+2][0]-f[i+3][0];
            X4[1] = f[i+2][1]-f[i+3][1];
            X5[0] = X3[0]-X4[1];
            X5[1] = X3[1]+X4[0];
            X6[0] = X3[0]+X4[1];
            X6[1] = X3[1]-X4[0];
            f[i][0] = X1[0]+X2[0];
            f[i][1] = X1[1]+X2[1];
            f[i+2][0] = X1[0]-X2[0];
            f[i+2][1] = X1[1]-X2[1];
            if(!inverse){
                f[i+1][0] = X6[0];
                f[i+1][1] = X6[1];
                f[i+3][0] = X5[0];
                f[i+3][1] = X5[1];
            } else{
                //inverse:
                f[i+1][0] = X5[0];
                f[i+1][1] = X5[1];
                f[i+3][0] = X6[0];
                f[i+3][1] = X6[1];
            }
        }
        //-------------------------
        int m = 0;
        double[] tmp = new double[2];
        //-------------------------
        for (int i = 4; i < n; i <<= 1) {
            m = n/(i<<1);
            for (j = 0; j < n; j += i<<1) {
                for (k = 0; k < i; k++) {
                    tmp[0] = f[i+j+k][0]*w[k*m][0]-f[i+j+k][1]*w[k*m][1];
                    tmp[1] = f[i+j+k][0]*w[k*m][1]+f[i+j+k][1]*w[k*m][0];
                    f[i+j+k][0] = f[j+k][0] - tmp[0];
                    f[i+j+k][1] = f[j+k][1] - tmp[1];
                    f[j+k][0] += tmp[0];
                    f[j+k][1] += tmp[1];
                }
            }
        }
        //-------------------------
        return f;
    }




    
    public static void calcSpektrum (float zv[][], Vector<Float> cn, Vector<Float> pn) {
        //------------------------------
        // Verschiebung der Zeitdaten, sodass Punktzahl 2^pkt betraegt --> effiziente FFT 
        int pktZV= zv[0].length; 
        int exLg2= (int)(Math.log(pktZV)/Math.log(2))+1; 
        int pktLg2= (int)Math.pow(2,exLg2); 
        float[][] zvLg2= new float[2][pktLg2]; 
        float dtLg2= (float)((zv[0][zv[0].length-1]-zv[0][0])/(pktLg2-1)); 
        for (int i1=0;  i1<pktLg2;  i1++) {
            zvLg2[0][i1]= i1*dtLg2; 
            zvLg2[1][i1]= (float)zv[1][(int)(i1*1.0/pktLg2*pktZV)]; 
        }
        //------------------------------
        // Aufbereitung fuer FFT & Durchfuehrung --> 
        int nn= pktLg2; 
        int isign= +1; 
        float[] a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]= (float)zvLg2[1][i1/2];  // Re
            a[i1+1]= 0;  // Im
        }
        //
        FFT.four1(a,nn,isign); 
        //
        for (int i1=1;  i1<a.length/2;  i1+=2) {
            float cN= (float)Math.sqrt(a[i1]*a[i1]+a[i1+1]*a[i1+1])/(nn/2); 
            float pN= (float)Math.atan2(a[i1+1],a[i1]); 
            if (i1==1) { cN *= 0.5;   pN=0; }  // Gleichanteil 
            cn.add(cN); 
            pn.add(pN); 
        }
        //------------------------------
    }
        
     
    
    
    public static void calcSpektrum (double zv[][], Vector<Float> cn, Vector<Float> pn) {
        //------------------------------
        // Verschiebung der Zeitdaten, sodass Punktzahl 2^pkt betraegt --> effiziente FFT 
        int pktZV= zv[0].length; 
        int exLg2= (int)(Math.log(pktZV)/Math.log(2))+1; 
        int pktLg2= (int)Math.pow(2,exLg2); 
        double[][] zvLg2= new double[2][pktLg2]; 
        double dtLg2= (zv[0][zv[0].length-1]-zv[0][0])/(pktLg2-1); 
        for (int i1=0;  i1<pktLg2;  i1++) {
            zvLg2[0][i1]= i1*dtLg2; 
            zvLg2[1][i1]= zv[1][(int)(i1*1.0/pktLg2*pktZV)]; 
        }
        //------------------------------
        // Aufbereitung fuer FFT & Durchfuehrung --> 
        int nn= pktLg2; 
        int isign= +1; 
        float[] a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]= (float)zvLg2[1][i1/2];  // Re
            a[i1+1]= 0;  // Im
        }
        //
        FFT.four1(a,nn,isign); 
        //
        for (int i1=1;  i1<a.length/2;  i1+=2) {
            float cN= (float)Math.sqrt(a[i1]*a[i1]+a[i1+1]*a[i1+1])/(nn/2); 
            float pN= (float)Math.atan2(a[i1+1],a[i1]); 
            if (i1==1) { cN *= 0.5;   pN=0; }  // Gleichanteil 
            cn.add(cN); 
            pn.add(pN); 
        }
        //------------------------------
    }
        
     
    
    
    public static void calcSpektrum (double zv[][], Vector<Float> aVec) throws OutOfMemoryError {
        //------------------------------
        // Verschiebung der Zeitdaten, sodass Punktzahl 2^pkt betraegt --> effiziente FFT 
        int pktZV= zv[0].length; 
        int exLg2= (int)(Math.log(pktZV)/Math.log(2))+1; 
        int pktLg2= (int)Math.pow(2,exLg2); 
        float[][] zvLg2= new float[2][pktLg2]; 
        float dtLg2= (float)((zv[0][zv[0].length-1]-zv[0][0])/(pktLg2-1)); 
        int index; 
        for (int i1=0;  i1<pktLg2;  i1++) {
            zvLg2[0][i1]= i1*dtLg2; 
            //
            index= (int)(i1*1.0/pktLg2*pktZV); 
            if (zv[0][index]==zvLg2[0][i1]) { // OK, nix machen 
            } else if (zv[0][index]>zvLg2[0][i1]) {
                while ((zv[0][index]>zvLg2[0][i1])&&(index>0)) index--;
            } else if (zv[0][index]<zvLg2[0][i1]) {
                while ((zv[0][index]<zvLg2[0][i1])&&(index<zv[0].length-1)) index++;
                if ((zv[0][index]!=zvLg2[0][i1])&&(index>0)) index--; 
            }
            // somit ist nun zv[0][index] entweder kleiner oder gleich zvLg2[0][i1] ... und weiter geht's --> 
            if (((index>0)&&(index<zv[0].length-1)) && (zv[0][index+1]-zv[0][index] != 0))
                zvLg2[1][i1]= (float)(zv[1][index] +(zv[1][index+1]-zv[1][index])*(zvLg2[0][i1]-zv[0][index])/(zv[0][index+1]-zv[0][index])); 
            else 
                zvLg2[1][i1]= (float)zv[1][(int)(i1*1.0/pktLg2*pktZV)]; 
            //
            //System.out.println(i1+"\t"+zvLg2[0][i1]+"\t"+zvLg2[1][i1]); 
            //zvLg2[1][i1]= (float)zv[1][(int)(i1*1.0/pktLg2*pktZV)]; 
        }
        //------------------------------
        // Aufbereitung fuer FFT & Durchfuehrung --> 
        int nn= pktLg2; 
        int isign= +1; 
        float[] a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]= (float)zvLg2[1][i1/2];  // Re
            a[i1+1]= 0;  // Im
            //System.out.println(i1+"\t"+a[i1]+"\t"+a[i1+1]); 
        }
        //
        FFT.four1(a,nn,isign); 
        //------------------------------
        for (int i1=0;  i1<a.length;  i1++) {
            aVec.add(a[i1]/(nn/2));
            //System.out.println(i1+"\t"+(a[i1]/(nn/2))); 
        } 
    }
        
     
    
    
    // Aus Amplituden und Phasen der Oberschwingungen wird wieder ein Zeitverlauf zusammengesetzt und als zvRec[][]
    // mit {zeit,wert}-Paaren zurueckgegeben; 
    // Die Grundschwingungsdauer T1Rec und der numerische Zeitschritt dtRec (Aufloesung) muessen vorgegeben werden 
    // 
    public static void reconstructTimeBehaviour (
            float[] cn, float[] pn, double T1Rec, double dtRec, Vector<Float> zvRecZeit, Vector<Float> zvRecWert
            ) {
        //------------------------------
        // Verschiebung der Zeitdaten, sodass Punktzahl 2^pkt betraegt --> effiziente FFT 
        int pktZVRec= (int)(T1Rec/dtRec)+1; 
        int exLg2= (int)(Math.log(pktZVRec)/Math.log(2))+1; 
        int pktLg2= (int)Math.pow(2,exLg2); 
        float[][] zvLg2= new float[2][pktLg2]; 
        //------------------------------
        // Aufbereitung fuer FFT & Durchfuehrung --> 
        int nn= pktLg2; 
        int isign= -1; 
        double[] re= new double[nn]; 
        double[] im= new double[nn]; 
        for (int i1=0;  i1<re.length;  i1++) {
            if (i1<cn.length) {
                re[i1]= cn[i1]*Math.cos(pn[i1]); 
                im[i1]= cn[i1]*Math.sin(pn[i1]); 
            } else {
                re[i1]= 0; 
                im[i1]= 0; 
            }
        }
        float[] a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]=   (float)re[i1/2];  // Re
            a[i1+1]= (float)im[i1/2];  // Im
        }
        //--------------------
        FFT.four1(a,nn,isign); 
        //--------------------
        for (int i1=0;  i1<nn;  i1++) {
            double t= i1*1.0/nn*T1Rec; 
            zvLg2[0][i1]= (float)t; 
            zvLg2[1][i1]= a[2*i1+1]; 
        }
        //------------------------------
        // jetzt wird der berechnete ZV von PKT=2^N auf eine vorgegebene Aufloesung mit dtRec transformiert --> 
        //zvRec= new float[2][pktZVRec]; 
        for (int i1=0;  i1<pktZVRec;  i1++) {
            zvRecZeit.add((float)(dtRec*i1)); 
            zvRecWert.add(zvLg2[1][(int)(i1*1.0/pktZVRec*pktLg2)]); 
        }
        //------------------------------
    } 

        
        
    
    public static void reconstructTimeBehaviour (
            float[] a, double T1Rec, Vector<Float> zvRecZeit, Vector<Float> zvRecWert
            ) {
        //------------------------------
        // Aufbereitung fuer FFT & Durchfuehrung --> 
        int nn= (a.length-1)/2; 
        int isign= -1; 
        //------------------------------
        FFT.four1(a,nn,isign); 
        //--------------------
        for (int i1=0;  i1<nn;  i1++) {
            double t= i1*1.0/nn*T1Rec; 
            zvRecZeit.add((float)t); 
            zvRecWert.add(a[2*i1+1]); 
        }
    } 

        
        
    
    public static void test () {
        //===================
        // Spektralzerlegung einer Test-Schwingung --> 
        //
        int nn= 32; 
        double T= 20e-3; 
        double[][] zv= new double[2][nn]; 
        for (int i1=0;  i1<nn;  i1++) {
            double t= i1*1.0/nn*T; 
            zv[0][i1]= t; 
            zv[1][i1]= 1.7 +5*Math.cos(2*Math.PI/T*1.0*t -1.11) +3*Math.cos(2*Math.PI/T*3.0*t +2.22) +1.5*Math.cos(2*Math.PI/T*7.0*t -0.77); 
        }
        //--------------------
        // Aufbereitung fuer FFT --> 
        int isign= +1; 
        float[] a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]= (float)zv[1][i1/2];  // Re
            a[i1+1]= 0;  // Im
        }
        //--------------------
        FFT.four1(a,nn,isign); 
        //--------------------
        double[] cn= new double[a.length/4]; 
        double[] pn= new double[cn.length]; 
        for (int i1=1;  i1<a.length/2;  i1+=2) {
            cn[i1/2]= Math.sqrt(a[i1]*a[i1]+a[i1+1]*a[i1+1])/(nn/2); 
            pn[i1/2]= Math.atan2(a[i1+1],a[i1]); 
            if (cn[i1/2]<1e-3) { cn[i1/2]=0; pn[i1/2]=0; } 
        }
        // Gleichanteil: 
        cn[0] *= 0.5;   pn[0]= 0; 
        //
        for (int i1=0;  i1<cn.length;  i1++) System.out.println("nach >> "+i1+"\tc[]= "+cn[i1]+"\tp[]="+pn[i1]); 
        //
        //===================
        // Test Ruecktransformation in den Zeitbereich --> 
        //
        T= 20e-3;  // muss vorgegeben werden 
        double[] re= new double[nn]; 
        double[] im= new double[nn]; 
        for (int i1=0;  i1<re.length;  i1++) {
            if (i1<cn.length) {
                re[i1]= cn[i1]*Math.cos(pn[i1]); 
                im[i1]= cn[i1]*Math.sin(pn[i1]); 
            } else {
                re[i1]= 0; 
                im[i1]= 0; 
            }
        }
        //
        isign= -1; 
        a= new float[2*nn+1];  // a[0] wird nie verwendet! 
        for (int i1=1;  i1<a.length;  i1+=2) {
            a[i1]=   (float)re[i1/2];  // Re
            a[i1+1]= (float)im[i1/2];  // Im
        }
        //--------------------
        FFT.four1(a,nn,isign); 
        //--------------------
        double[][] zvRec= new double[2][nn]; 
        for (int i1=0;  i1<nn;  i1++) {
            double t= i1*1.0/nn*T; 
            zvRec[0][i1]= t; 
            zvRec[1][i1]= a[2*i1+1] +0*0.01*Math.sqrt(2)*Math.cos(2*Math.PI/T*11.0*t -0.0); 
        }
        //--------------------
        // Vergleich mit der Vorgabe: 
        double dzv=0; 
        double dt= T/nn; 
        for (int i1=0;  i1<zv[0].length;  i1++) dzv += (dt*(zvRec[1][i1]-zv[1][i1])*(zvRec[1][i1]-zv[1][i1])); 
        dzv /= T;   dzv= Math.sqrt(dzv); 
        System.out.println("Abweichung gewichtet= "+dzv); 
        //===================
    }


    
    
    
    
}



