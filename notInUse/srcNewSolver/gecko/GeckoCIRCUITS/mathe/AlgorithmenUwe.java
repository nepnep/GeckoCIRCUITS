/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.mathe;


public class AlgorithmenUwe {

    //------------------------------------------------
    private static final double TINY_AMOEBA=1e-10;
    private static final int NMAX=5000;  // Maximum allowed number of function evaluations
    private static final double ln10= Math.log(10);
    //------------------------------------------------


    public AlgorithmenUwe () {
        //this.testen_powell();
    }


    //==================================================================================
    //  10.4 Downhill Simplex Method in Multidimensions
    //==================================================================================

    /*
    Multidimensional minimization of the function funk(x) where x[1..ndim] is a vector in ndim
    dimensions, by the downhill simplex method of Nelder and Mead. The matrix p[1..ndim+1]
    [1..ndim] is input. Its ndim+1 rows are ndim-dimensional vectors which are the vertices of
    the starting simplex. Also input is the vector y[1..ndim+1], whose components must be preinitialized
    to the values of funk evaluated at the ndim+1 vertices (rows) of p; and ftol the
    fractional convergence tolerance to be achieved in the function value (n.b.!). On output, p and
    y will have been reset to ndim+1 new points all within ftol of a minimum function value, and
    nfunk gives the number of function evaluations taken.
    //
    nfunk= new int[1];  --> eigentlich ein 'int', aber hier fuer die Parameteruebergabe in Java als Array konzipiert
    */

    public void amoeba (double[][] p1, double[] y1, int ndim, double ftol, MathFunktion funk, int[] nfunk) {
        //======================
        // kleiner Einschub zur einfacheren Datenhandhabung:
        // p1= {{r1opt,r2opt,r3opt, c1opt,c2opt,c3opt}, {r1min,r2min,r3min, c1min,c2min,c3min}, {r1max,r2max,r3max, c1max,c2max,c3max}}
        // p1[0] --> Rueckgabewert
        ndim= 6;
        double r1min= p1[1][0], r2min= p1[1][1], r3min= p1[1][2];  // minimale Rth-Werte
        double r1max= p1[2][0], r2max= p1[2][1], r3max= p1[2][2];  // maximale Rth-Werte
        double c1min= p1[1][3], c2min= p1[1][4], c3min= p1[1][5];  // minimale Cth-Werte
        double c1max= p1[2][3], c2max= p1[2][4], c3max= p1[2][5];  // maximale Cth-Werte
        //
        double[][] p= new double[ndim+1][ndim];
        p[0]= new double[]{r1min,r2min,r3min, c1min,c2min,c3min};
        p[1]= new double[]{r1max,r2min,r3min, c1min,c2min,c3min};
        p[2]= new double[]{r1min,r2max,r3min, c1min,c2min,c3min};
        p[3]= new double[]{r1min,r2min,r3max, c1min,c2min,c3min};
        p[4]= new double[]{r1min,r2min,r3min, c1max,c2min,c3min};
        p[5]= new double[]{r1min,r2min,r3min, c1min,c2max,c3min};
        p[6]= new double[]{r1min,r2min,r3min, c1min,c2min,c3max};
        //
        double[] y= new double[ndim+1];
        for (int i1=0;  i1<y.length;  i1++)  y[i1]= funk.calc(p[i1])[0];  // Vor-Initialisierung
        //======================
        int i, ihi, ilo, inhi, j, mpts=ndim+1;
        double rtol, sum, swap, ysave, ytry;
        double[] psum= new double[ndim];  // psum= vector(1,ndim);
        //
        nfunk[0]= 0;
        //-----------------------
        // GET_PSUM -->
        for (j=0;  j<ndim;  j++) {
            sum=0;
            for (i=0;  i<mpts;  i++)  sum += p[i][j];
            psum[j]= sum;
        }
        //-----------------------
        for (;;) {
            ilo=0;
            // First we must determine which point is the highest (worst), next-highest, and lowest (best), by looping over the points in the simplex.
            if (y[0]>y[1]) { inhi=1;  ihi=0; } else { inhi=0;  ihi=1; }
            for (i=0;  i<mpts;  i++) {
                if (y[i] <= y[ilo])  ilo=i;
                if (y[i] > y[ihi]) { inhi=ihi;   ihi=i; }
                else  if ((y[i] > y[inhi]) && (i!=ihi))  inhi=i;
            }
            rtol= 2.0*Math.abs(y[ihi]-y[ilo])/(Math.abs(y[ihi]) +Math.abs(y[ilo])+TINY_AMOEBA);
            // Compute the fractional range from highest to lowest and return if satisfactory.
            if (rtol < ftol) {  // If returning, put best point and value in slot 1.
                //-----------------------
                // SWAP(y[1],y[ilo])-->
                swap= y[0];
                y[0]= y[ilo];
                y[ilo]= swap;
                //-----------------------
                for (i=0;  i<ndim;  i++) {
                    //-----------------------
                    // SWAP(p[1][i],p[ilo][i]) -->
                    swap= p[0][i];
                    p[0][i]= p[ilo][i];
                    p[ilo][i]= swap;
                    //-----------------------
                }
                break;
            }
            if (nfunk[0] >= NMAX) { System.out.println("Fehler: NMAX exceeded");  return; }
            nfunk[0] += 2;
            //
            // Begin a new iteration. First extrapolate by a factor -1 through the face of the simplex
            // across from the high point, i.e., reflect the simplex from the high point.
            ytry= amotry(p,y,psum,ndim,funk,ihi,-1.0);
            if (ytry <= y[ilo])
                // Gives a result better than the best point, so try an additional extrapolation by a factor 2.
                ytry= amotry(p,y,psum,ndim,funk,ihi,2.0);
            else if (ytry >= y[inhi]) {
                // The reflected point is worse than the second-highest, so look for an intermediate
                // lower point, i.e., do a one-dimensional contraction.
                ysave= y[ihi];
                ytry= amotry(p,y,psum,ndim,funk,ihi,0.5);
                if (ytry >= ysave) {  // Can't seem to get rid of that high point. Better contract around the lowest (best) point.
                    for (i=0;  i<mpts;  i++) {
                        if (i != ilo) {
                            for (j=0;  j<ndim;  j++)
                                p[i][j]=psum[j]= 0.5*(p[i][j]+p[ilo][j]);
                            y[i]= funk.calc(psum)[0];  // (*funk)(psum);
                        }
                    }
                    nfunk[0] += ndim;  // Keep track of function evaluations.
                    //-----------------------
                    // GET_PSUM Recompute psum.
                    for (j=0;  j<ndim;  j++) {
                        sum=0;
                        for (i=0;  i<mpts;  i++)  sum += p[i][j];
                        psum[j]= sum;
                    }
                    //-----------------------
                }
            } else
                nfunk[0]--;  // Correct the evaluation count.
        }  // Go back for the test of doneness and the next iteration.
        //======================
        // Aufbereitung der Rueckgabewerte:
        for (int i1=0;  i1<ndim;  i1++)
            p1[0][i1]= p[0][i1];
        y1[0]= y[0];
        //======================
    }


    // Extrapolates by a factor fac through the face of the simplex across from the high point, tries
    // it, and replaces the high point if the new point is better.
    private double amotry (double[][] p, double[] y, double[] psum, int ndim, MathFunktion funk, int ihi, double fac) {
        int j;
        double fac1, fac2, ytry;
        double[] ptry= new double[ndim];  // ptry= vector(1,ndim);
        fac1= (1.0-fac)/ndim;
        fac2= fac1-fac;
        for (j=0;  j<ndim;  j++)  ptry[j]= psum[j]*fac1-p[ihi][j]*fac2;
        ytry= funk.calc(ptry)[0];  // (*funk)(ptry);  // Evaluate the function at the trial point.
        if (ytry < y[ihi]) {  // If it's better than the highest, then replace the highest.
            y[ihi]= ytry;
            for (j=0;  j<ndim;  j++) {
                psum[j] += ptry[j]-p[ihi][j];
                p[ihi][j]= ptry[j];
            }
        }
        return ytry;
    }



    private void testen_amoeba () {
        //---------------------------
        // Testfunktion:
        double ftol= 1e-4;
        int ndim= 2;  // Funktion fkt hat 2 Input-Parameter
        MathFunktion fkt= new TestFkt_Amoeba();  // Definition siehe unten
        //
        // Startpunkte grenzen den Suchraum ein:
        double[][] p= new double[][]{{10,-10}, {-10,-10}, {-1,8}};       // p[1..ndim+1][1..ndim]
        double[] y= new double[p.length];                                // y[1..ndim+1] --> hier wird das Ergebnis abgelegt
        for (int i1=0;  i1<y.length;  i1++)  y[i1]= fkt.calc(p[i1])[0];  // Vor-Initialisierung
        int[] nfunk= new int[]{0};                                       // hier wird die Anzahl der Iterationen abgelegt
        //---------------------------
        this.amoeba(p,y,ndim,ftol,fkt,nfunk);
        //---------------------------
        System.out.println("------------------\np[][] -->");
        for (int i1=0;  i1<p.length;  i1++) {
            for (int i2=0;  i2<p[0].length;  i2++) System.out.print(p[i1][i2]+"\t");
            System.out.println();
        }
        System.out.println("y[] -->");
        for (int i1=0;  i1<y.length;  i1++)  System.out.println(y[i1]);
        System.out.println("Zahl der Iterationen:  "+nfunk[0]+"\n------------");
        //---------------------------
    }
    class TestFkt_Amoeba extends MathFunktion {
        public double[] calc (double[] par) {
            //===========================
            double x1= par[0], x2= par[1];
            double z= (x1-5)*(x1-5) +(x2+1)*(x2+1);  // Schuessel (nein, nicht der Kanzler) mit Minimum bei Punkt (5/-1)
            erg= new double[]{z};
            //===========================
            return erg;
        }
    }


    //==================================================================================
    //  Eigenbau, ein bischen hirnlos (Brute-Force)
    //==================================================================================


    public void doBereichseinschraenkendeSuche (double[][] p, double[] y, int ndim, double ftol, MathFunktion fkt, int[] nfunk) {
        int tg= nfunk[1];  // Teilungen pro Intervall
        double r1min= p[1][0], r2min= p[1][1], r3min= p[1][2];  // minimale Rth-Werte
        double r1max= p[2][0], r2max= p[2][1], r3max= p[2][2];  // maximale Rth-Werte
        double c1min= p[1][3], c2min= p[1][4], c3min= p[1][5];  // minimale Cth-Werte
        double c1max= p[2][3], c2max= p[2][4], c3max= p[2][5];  // maximale Cth-Werte
        double fehlerGlobal= y[0], fehlerLokal=2*fehlerGlobal;
        double[][] pLokal= new double[][]{
            {-1,-1,-1, -1,-1,-1},  // zu ermittelnde Parameter r1,r2,r3,c1,c2,c3
            {r1min,r2min,r3min, c1min,c2min,c3min},
            {r1max,r2max,r3max, c1max,c2max,c3max}};
        int count=0, countMAX=nfunk[2];
        //--------------------------------------------------
        while (count<countMAX) {
            this.doBruteForce_eingeschraenkterBereich(p,y,-1,-1,fkt,nfunk);
            fehlerGlobal= y[0];
            count++;
            System.out.println("Neuer Durchgang :  "+count+"   "+fehlerGlobal);
        }
        //--------------------------------------------------
    }

    public void doBruteForce_eingeschraenkterBereich (double[][] p, double[] y, int ndim, double ftol, MathFunktion fkt, int[] nfunk) {
        // Format:  p= { {Rth1OPT, Rth2OPT, Rth3OPT, Cth1OPT, Cth2OPT, Cth3OPT},
        //               {Rth1MIN, Rth2MIN, Rth3MIN, Cth1MIN, Cth2MIN, Cth3MIN},
        //               {Rth1MAX, Rth2MAX, Rth3MAX, Cth1MAX, Cth2MAX, Cth3MAX} }
        //          nfunk[1] --> Anzeahl der Teilungen pro Parameter
        //          y[0]     --> Fehler-Differenz
        //--------------------------------------------------
        int tg= nfunk[1];  // Teilungen pro Intervall
        int zaehler= 0;
        double[] par= new double[6], parOPT= new double[6];  // RthCth, so wie es der Funktion fkt uebergeben wird
        double fehlerMIN= y[0];  // globaler Fehler, soll minimiert werden
        //
        double dr1= (p[2][0]-p[1][0])/tg;  // Schrittweite der Parameter-Variation
        double dr2= (p[2][1]-p[1][1])/tg;
        double dr3= (p[2][2]-p[1][2])/tg;
        double r1min= p[1][0], r2min= p[1][1], r3min= p[1][2];  // minimale Rth-Werte
        double r1max= p[2][0], r2max= p[2][1], r3max= p[2][2];  // minimale Rth-Werte
        double r1=r1min, r2=r2min, r3=r3min;
        double c1= p[1][3], c2= p[1][4], c3= p[1][5];  // minimale Cth-Werte
        int i4OPT=0, i5OPT=0, i6OPT=0;
        //--------------------------------------------------
        Ticker ticker= new Ticker(1000);  // damit die Ausgaben der Zwischenstaende nur zu definierten Zeiten erfolgen!
        ticker.setPriority(Thread.MIN_PRIORITY);
        ticker.start();
        //--------------------------------------------------
        int schrittzahlGesamt= tg*tg*tg * tg*tg*tg;
        r1= r1min;
        while (r1 <= r1max) {
            r2= r2min;
            while (r2 <= r2max) {
                r3= r3min;
                while (r3 <= r3max) {
                    for (int i4=1;  i4<=tg;  i4++) {
                        for (int i5=1;  i5<=tg;  i5++) {
                            for (int i6=1;  i6<=tg;  i6++) {
                                //-------------------------
                                ticker.setAktuelleWerte((zaehler*100.0/schrittzahlGesamt),tg,fehlerMIN,true);
                                //System.out.println("["+(zaehler*100.0/Math.pow(10,tg))+" %]\t("+zaehler+")\t"+r1+"   "+r2+"   "+r3+"   "+c1+"   "+c2+"   "+c3);
                                zaehler++;
                                par[0]=r1;  par[1]=r2;  par[2]=r3;  par[3]=c1;  par[4]=c2;  par[5]=c3;
                                double fehler= Math.abs(fkt.calc(par)[0]);
                                if (fehler<fehlerMIN) {
                                    fehlerMIN= fehler;
                                    parOPT[0]=r1;  parOPT[1]=r2;  parOPT[2]=r3;  parOPT[3]=c1;  parOPT[4]=c2;  parOPT[5]=c3;
                                    i4OPT=i4;   i5OPT=i5;   i6OPT=i6;
                                }
                                //-------------------------
                                c3= p[1][5]*Math.pow((p[2][5]/p[1][5]), (i6*1.0/tg));
                            }
                            c2= p[1][4]*Math.pow((p[2][4]/p[1][4]), (i5*1.0/tg));
                        }
                        c1= p[1][3]*Math.pow((p[2][3]/p[1][3]), (i4*1.0/tg));
                        // xAufzubereiten[i1]= tSTART*Math.pow((tEND/tSTART), (i1*1.0/anzTeilungen));
                    }
                    r3 += dr3;
                }
                r2 += dr2;
            }
            r1 += dr1;
        }
        //--------------------------------------------------
        ticker.setAktuelleWerte(zaehler,tg,fehlerMIN,false);
        System.out.println("Iterationen insgesamt: "+zaehler);
        //
        System.arraycopy(parOPT,0,p[0],0,6);  // neuer optimaler Punkt
        y[0]= fehlerMIN;
        nfunk[0]= zaehler;
        // Bestimmung der neuen, engeren Bereichsgrenzen:
        // Widerstaende:
        p[1][0]= parOPT[0]-dr1;   p[2][0]= parOPT[0]+dr1;
        p[1][1]= parOPT[1]-dr2;   p[2][1]= parOPT[1]+dr2;
        p[1][2]= parOPT[2]-dr3;   p[2][2]= parOPT[2]+dr3;
        for (int i1=0;  i1<3;  i1++)
            if (p[1][i1]<=0) p[1][i1]= 0.1*p[0][i1];
        // Kapazitaeten:
        p[1][3]= p[1][3]*Math.pow((p[2][3]/p[1][3]),((i4OPT-2)*1.0/tg));   p[2][3]= p[1][3]*Math.pow((p[2][3]/p[1][3]),((i4OPT)*1.0/tg));
        p[1][4]= p[1][4]*Math.pow((p[2][4]/p[1][4]),((i5OPT-2)*1.0/tg));   p[2][4]= p[1][4]*Math.pow((p[2][4]/p[1][4]),((i5OPT)*1.0/tg));
        p[1][5]= p[1][5]*Math.pow((p[2][5]/p[1][5]),((i6OPT-2)*1.0/tg));   p[2][5]= p[1][5]*Math.pow((p[2][5]/p[1][5]),((i6OPT)*1.0/tg));
        //--------------------------------------------------
    }


    class Ticker extends Thread {
        private double z,tg;
        private double f;
        private boolean tickerLaeuft= true;
        long dt_MS=10000;  // default: 1 Update pro 10 Sekunden
        public Ticker (long dt_MS) { this.dt_MS=dt_MS; }
        public void run () {
            while (tickerLaeuft) {
                try { Thread.sleep(dt_MS); } catch (InterruptedException ie) {}
                System.out.println(z+"% (100%)\t\t"+f);
            }
        }
        public void setAktuelleWerte (double z, double tg, double f, boolean tL) { this.z=z;  this.tg=tg;  this.f=f;  this.tickerLaeuft=tL; }
    };



    //==================================================================================
    //  10.5 Direction Set (Powell's) Methods in Multidimensions
    //==================================================================================


    // aus 'nrutil.h':
    private static double fktSQR (double a) { return (a*a); }
    private static double fktSIGN (double a, double b) { return ((b>=0)? Math.abs(a): -Math.abs(a)); }
    private static double fktFMAX (double a, double b) { return ((a>b)? a: b); }

    /*
    Minimization of a function func of n variables. Input consists of an initial starting point
    p[1..n]; an initial matrix xi[1..n][1..n], whose columns contain the initial set of directions
    (usually the n unit vectors); and ftol, the fractional tolerance in the function value
    such that failure to decrease by more than this amount on one iteration signals doneness. On
    output, p is set to the best point found, xi is the then-current direction set, fret is the returned
    function value at p, and iter is the number of iterations taken. The routine linmin is used.
    */

    private static final double TINY_POWELL= 1.0e-25;
    private static final int ITMAX_POWELL =200;

    //void powell(double p[], double **xi, int n, double ftol, int *iter, double *fret, double (*func)(double []))
    //void linmin(double p[], double xi[], int n, double *fret, double (*func)(double []));
    //
    public void powell (double[] p, double[][] xi, int n, double ftol, int[] iter, double[] fret, MathFunktion func) {
        int i,ibig,j;
        double del,fp,fptt,t;
        double[] pt,ptt,xit;
        pt= new double[n];
        ptt= new double[n];
        xit= new double[n];
        fret[0]= func.calc(p)[0];
        for (j=0;  j<n;  j++) pt[j]=p[j];  // Save the initial point.
        for (iter[0]=0;  ;  (iter[0])++) {
            fp= fret[0];
            ibig= 0;
            del= 0.0;  // Will be the biggest function decrease.
            for (i=0;  i<n;  i++) {  // In each iteration, loop over all directions in the set.
                for (j=0;  j<n;  j++) xit[j]=xi[j][i];  // Copy the direction,
                fptt= fret[0];
                linmin(p,xit,n,fret,func);  // minimize along it,
                if (fptt-fret[0] > del) {   // and record it if it is the largest decrease so far.
                    del= fptt-fret[0];
                    ibig= i;
                }
            }
            if (2.0*(fp-fret[0]) <= ftol*(Math.abs(fp)+Math.abs(fret[0]))+TINY_POWELL) {  // Termination criterion.
                return;
            }
            if (iter[0] == ITMAX_POWELL) System.out.println("powell exceeding maximum iterations.");
            for (j=0;  j<n;  j++) {  // Construct the extrapolated point and the average direction moved. Save the old starting point.
                ptt[j]= 2.0*p[j]-pt[j];
                xit[j]= p[j]-pt[j];
                pt[j]= p[j];
            }
            fptt= func.calc(ptt)[0];  // Function value at extrapolated point.
            if (fptt < fp) {
                t= 2.0*(fp-2.0*fret[0]+fptt)*fktSQR(fp-fret[0]-del)-del*fktSQR(fp-fptt);
                if (t < 0.0) {
                    linmin(p,xit,n,fret,func);  // Move to the minimum of the new direction, and save the new direction.
                    for (j=0;  j<n;  j++) {
                        xi[j][ibig]= xi[j][n-1];
                        xi[j][n-1]= xit[j];
                    }
                }
            }
            System.out.println("MinimumSuche --> fret[0]= "+fret[0]);
        }  // Back for another iteration.
    }


    /*
    Given an n-dimensional point p[1..n] and an n-dimensional direction xi[1..n], moves and
    resets p to where the function func(p) takes on a minimum along the direction xi from p,
    and replaces xi by the actual vector displacement that p was moved. Also returns as fret
    the value of func at the returned location p. This is actually all accomplished by calling the
    routines mnbrak and brent.
    */

    private static final double TOL_LINMIN= 2.0e-4;  // Tolerance passed to brent.
    //---------------------------
    // Global variables communicate with f1dim:
    private int ncom;
    private double[] pcom,xicom;  // urspruenglich globale Zeiger
    private MathFunktion nrfunc;
    //---------------------------

    // public void linmin (double p[], double xi[], int n, double *fret, double (*func)(double [])) {
    // double brent(double ax, double bx, double cx, double (*f)(double), double tol, double *xmin);
    // double f1dim(double x);
    // void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc, double (*func)(double));
    //
    public void linmin (double p[], double xi[], int n, double[] fret, MathFunktion func) {
        int j;
        // Umschreiben auf Felder, damit man als Zeiger uebergeben kann:
        double[] xx=new double[1], bx=new double[1], ax=new double[1], xmin=new double[1];
        double[] fx=new double[1], fb=new double[1], fa=new double[1];
        //-----------------------
        // Define the global variables:
        ncom= n;
        pcom= new double[n];
        xicom= new double[n];
        nrfunc= func;
        //-----------------------
        for (j=0;  j<n;  j++) {
            pcom[j]= p[j];
            xicom[j]= xi[j];
        }
        ax[0]= 0.0;  // Initial guess for brackets.
        xx[0]= 1.0;
        mnbrak(ax,xx,bx,fa,fx,fb);
        fret[0]= brent(ax[0],xx[0],bx[0],TOL_LINMIN,xmin);
        for (j=0;  j<n;  j++) {  // Construct the vector results to return.
            xi[j] *= xmin[0];
            p[j] += xi[j];
        }
    }

    /*
    Must accompany linmin.
    */
    private double f1dim (double x) {
        int j;
        double f;
        double[] xt;
        xt= new double[ncom];
        for (j=0;  j<ncom;  j++) xt[j]=pcom[j]+x*xicom[j];
        f= nrfunc.calc(xt)[0];
        return f;
    }


    /*
    Here GOLD_MNBRAK is the default ratio by which successive intervals are magnified; GLIMIT_MNBRAK is the
    maximum magnification allowed for a parabolic-fit step.
    //
    Given a function func, and given distinct initial points ax and bx, this routine searches in
    the downhill direction (defined by the function as evaluated at the initial points) and returns
    new points ax, bx, cx that bracket a minimum of the function. Also returned are the function
    values at the three points, fa, fb, and fc.
    */

    private static final double GOLD_MNBRAK= 1.618034;
    private static final double GLIMIT_MNBRAK= 100.0;
    private static final double TINY_MNBRAK= 1.0e-20;
    // #define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);  --> direkt im Java-Code ersetzt


    //void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc, double (*func)(double))
    //
    public void mnbrak (double[] ax, double[] bx, double[] cx, double[] fa, double[] fb, double[] fc) {
        // 'double (*func)(double)' in der Parameter-Uebergabeliste von mnbrak() wird innerhalb
        // der Methode direkt durch f1dim() ersetzt
        double ulim,u,r,q,fu,dum;
        fa[0]= f1dim(ax[0]);
        fb[0]= f1dim(bx[0]);
        if (fb[0] > fa[0]) {  // Switch roles of a and b so that we can go downhill in the direction from a to b.
            dum=ax[0];   ax[0]=bx[0];   bx[0]=dum;  // SHFT(dum,ax,bx,dum);
            dum=fb[0];   fb[0]=fa[0];   fa[0]=dum;  // SHFT(dum,fb,fa,dum);
        }
        cx[0]= (bx[0])+GOLD_MNBRAK*(bx[0]-ax[0]);  // First guess for c.
        fc[0]= f1dim(cx[0]);
        while (fb[0] > fc[0]) {  // Keep returning here until we bracket.
            r= (bx[0]-ax[0])*(fb[0]-fc[0]);  // Compute u by parabolic extrapolation from a, b, c. TINY_MNBRAK is used to prevent any possible division by zero.
            q= (bx[0]-cx[0])*(fb[0]-fa[0]);
            u= (bx[0])-((bx[0]-cx[0])*q-(bx[0]-ax[0])*r)/(2.0*fktSIGN(fktFMAX(Math.abs(q-r),TINY_MNBRAK),q-r));
            ulim= (bx[0])+GLIMIT_MNBRAK*(cx[0]-bx[0]);
            // We won't go farther than this. Test various possibilities:
            if ((bx[0]-u)*(u-cx[0]) > 0.0) {  // Parabolic u is between b and c: try it.
                fu= f1dim(u);
                if (fu < fc[0]) {  // Got a minimum between b and c.
                    ax[0]= (bx[0]);
                    bx[0]= u;
                    fa[0]= (fb[0]);
                    fb[0]= fu;
                    return;
                } else if (fu > fb[0]) {  // Got a minimum between between a and u.
                    cx[0]= u;
                    fc[0]= fu;
                    return;
                }
                u= (cx[0])+GOLD_MNBRAK*(cx[0]-bx[0]);  // Parabolic fit was no use. Use default magnification.
                fu= f1dim(u);
            } else if ((cx[0]-u)*(u-ulim) > 0.0) {  // Parabolic fit is between c and its allowed limit.
                fu= f1dim(u);
                if (fu < fc[0]) {
                    bx[0]=cx[0];   cx[0]=u;    u=cx[0]+GOLD_MNBRAK*(cx[0]-bx[0]);  // SHFT(*bx,*cx,u,*cx+GOLD_MNBRAK*(*cx-*bx));
                    fb[0]=fc[0];   fc[0]=fu;   fu=f1dim(u);                        // SHFT(*fb,*fc,fu,(*func)(u));
                }
            } else if ((u-ulim)*(ulim-cx[0]) >= 0.0) {  // Limit parabolic u to maximum allowed value.
                u= ulim;
                fu= f1dim(u);
            } else {  // Reject parabolic u, use default magnification.
                u= (cx[0])+GOLD_MNBRAK*(cx[0]-bx[0]);
                fu= f1dim(u);
            }
            // Eliminate oldest point and continue:
            ax[0]=bx[0];   bx[0]=cx[0];   cx[0]=u;   // SHFT(*ax,*bx,*cx,u);
            fa[0]=fb[0];   fb[0]=fc[0];   fc[0]=fu;  // SHFT(*fa,*fb,*fc,fu);
        }
    }


    private static final double ITMAX_BRENT= 100;
    private static final double CGOLD_BRENT= 0.3819660;
    private static final double ZEPS_BRENT= 1.0e-10;
    // #define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

    /*
    Here ITMAX is the maximum allowed number of iterations; CGOLD is the golden ratio; ZEPS is
    a small number that protects against trying to achieve fractional accuracy for a minimum that
    happens to be exactly zero.
    //
    Given a function f, and given a bracketing triplet of abscissas ax, bx, cx (such that bx is
    between ax and cx, and f(bx) is less than both f(ax) and f(cx)), this routine isolates
    the minimum to a fractional precision of about tol using Brent's method. The abscissa of
    the minimum is returned as xmin, and the minimum function value is returned as brent, the
    returned function value.
    */

    // double brent (double ax, double bx, double cx, double (*f)(double), double tol, double *xmin) {
    //
    double brent (double ax, double bx, double cx, double tol, double[] xmin) {
        // 'double (*f)(double)' in der Parameter-Uebergabeliste von mnbrak() wird innerhalb
        // der Methode direkt durch f1dim() ersetzt
        double a,b,d=0,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm;
        double e=0.0;  // This will be the distance moved on the step before last.
        a= (ax < cx ? ax : cx);  // a and b must be in ascending order, but input abscissas need not be.
        b= (ax > cx ? ax : cx);
        x=bx;   w=bx;   v=bx;  // Initializations...
        fw=f1dim(x);   fv=fw;   fx=fw;
        for (int iter=0;  iter<ITMAX_BRENT;  iter++) {  // Main program loop.
            xm= 0.5*(a+b);
            tol2= 2.0*(tol1= tol*Math.abs(x)+ZEPS_BRENT);
            if (Math.abs(x-xm) <= (tol2-0.5*(b-a))) {  // Test for done here.
                xmin[0]=x;
                return fx;
            }
            if (Math.abs(e) > tol1) {  // Construct a trial parabolic fit.
                r= (x-w)*(fx-fv);
                q= (x-v)*(fx-fw);
                p= (x-v)*q-(x-w)*r;
                q= 2.0*(q-r);
                if (q > 0.0) p = -p;
                q= Math.abs(q);
                etemp= e;
                e= d;
                if ( (Math.abs(p) >= Math.abs(0.5*q*etemp)) || (p <= q*(a-x)) || (p >= q*(b-x)) ) {
                    d= CGOLD_BRENT*(e= (x >= xm ? a-x : b-x));
                // The above conditions determine the acceptability of the parabolic fit. Here we take the golden section step into the larger of the two segments.
                } else {
                    d= p/q;  // Take the parabolic step.
                    u= x+d;
                    if (u-a < tol2 || b-u < tol2)
                        d= fktSIGN(tol1,xm-x);
                }
            } else {
                d= CGOLD_BRENT*(e= (x >= xm ? a-x : b-x));
            }
            u= (Math.abs(d) >= tol1 ? x+d : x+fktSIGN(tol1,d));
            fu= f1dim(u);
            // This is the one function evaluation per iteration.
            if (fu <= fx) {  // Now decide what to do with our function evaluation.
                if (u >= x) a=x; else b=x;
                // Housekeeping follows:
                v=w;     w=x;     x=u;    // SHFT(v,w,x,u)
                fv=fw;   fw=fx;   fx=fu;  // SHFT(fv,fw,fx,fu)
            } else {
                if (u < x) a=u; else b=u;
                if ((fu <= fw) || (w == x)) {
                    v=w;   w=u;   fv=fw;   fw=fu;
                } else if ((fu <= fv) || (v == x) || (v == w)) {
                    v=u;   fv=fu;
                }
            }  // Done with housekeeping. Back for another iteration.
        }
        System.out.println("'brent()'-error: Too many iterations in brent");
        xmin[0]= x;  // Never get here.
        return fx;
    }


    private void testen_powell () {
        //---------------------------
        // Testfunktion:
        MathFunktion fkt= new TestFkt_Powell();  // Definition siehe unten
        // INPUT:
        double ftol= 1e-2;  // Fehlertoleranz
        int n= 2;  // Ordnung des Suchraums - Funktion fkt hat 2 Input-Parameter
        double[] p= new double[]{-10,0};  // Startpunkt
        double[][] xi= new double[][]{{1,0},{0,1}};  // n x n -Matrix mit dem Start-Richtungen der Suche, typ. n Einheitsvektoren
        // OUTPUT:
        // --> p[] enthaelt den gefundenen Minimum-Punkt
        int[] iter= new int[]{-1};   // Anzahl der benoetigten Iterationen
        double[] fret= new double[]{-1};  // Funktionswert am gefundenen Minimum p[]
        //---------------------------
        this.powell(p,xi,n,ftol,iter,fret,fkt);
        //---------------------------
        System.out.println("Minimum bei:  ( "+p[0]+" / "+p[1]+" )  mit Fehler:  "+fret[0]);
        System.out.println("Anzahl Iterationen:  "+iter[0]);
        //---------------------------
    }
    class TestFkt_Powell extends MathFunktion {
        public double[] calc (double[] par) {
            //===========================
            double x1= par[0], x2= par[1];
            double z= (x1-5)*(x1-5) +(x2+1)*(x2+1);  // Schuessel (nein, nicht der Kanzler) mit Minimum bei Punkt (5/-1)
            erg= new double[]{z};
            //===========================
            return erg;
        }
    }



    //==================================================================================
    //  15.5 Nonlinear Models / Levenberg-Marquardt Method
    //==================================================================================


    /*
    Levenberg-Marquardt method, attempting to reduce the value x^2 of a fit between a set of data
    points x[1..ndata], y[1..ndata] with individual standard deviations sig[1..ndata],
    and a nonlinear function dependent on ma coeficients a[1..ma]. The input array ia[1..ma]
    indicates by nonzero entries those components of a that should be fitted for, and by zero
    entries those components that should be held fixed at their input values. The program returns
    current best-fit values for the parameters a[1..ma], and x^2 = chisq. The arrays
    covar[1..ma][1..ma], alpha[1..ma][1..ma] are used as working space during most
    iterations. Supply a routine funcs(x,a,yfit,dyda,ma) that evaluates the fitting function
    yfit, and its derivatives dyda[1..ma] with respect to the fitting parameters a at x. On
    the first call provide an initial guess for the parameters a, and set alamda<0 for initialization
    (which then sets alamda=.001). If a step succeeds chisq becomes smaller and alamda decreases
    by a factor of 10. If a step fails alamda grows by a factor of 10. You must call this
    routine repeatedly until convergence is achieved. Then, make one final call with alamda=0, so
    that covar[1..ma][1..ma] returns the covariance matrix, and alpha the curvature matrix.
    (Parameters held fixed will return zero covariances.)
    */


    //------------------------------
    // globales 'Gedaechtnis' der Methode 'mrqmin()':
    private int mfit_stat=-1;
    private double ochisq_stat=-1;
    private double[] atry_stat=null, beta_stat=null, da_stat=null;
    private double[][] oneda_stat=null;  // --> weiter unten dann korrekt initialisiert!
    //------------------------------


    // public void mrqmin (double x[], double y[], double sig[], int ndata, double a[], int ia[],
    //     int ma, double **covar, double **alpha, double *chisq,
    //     void (*funcs)(double, double [], double *, double [], int), double *alamda) {
    //
    // void covsrt (double **covar, int ma, int ia[], int mfit);
    // void gaussj (double **a, int n, double **b, int m);
    // void mrqcof (double x[], double y[], double sig[], int ndata, double a[],
    //     int ia[], int ma, double **alpha, double beta[], double *chisq,
    //     void (*funcs)(double, double [], double *, double [], int));
    //
    public void mrqmin (double[] x, double[] y, double[] sig, int ndata, double[] a, int[] ia,
        int ma, double[][] covar, double[][] alpha, double[] chisq,
        MathFunktion funcs, double[] alamda) {
        //
        int j,k,l;
        if (alamda[0] < 0.0) {  // Initialization.
            atry_stat= new double[ma];
            beta_stat= new double[ma];
            da_stat= new double[ma];
            for (mfit_stat=0,j=0;  j<ma;  j++)
                if (ia[j]==1) mfit_stat++;
            oneda_stat= new double[mfit_stat][1];  // oneda= matrix(1,mfit_stat,1,1);
            alamda[0]= 0.001;
            mrqcof(x,y,sig,ndata,a,ia,ma,alpha,beta_stat,chisq,funcs);  // Vergleich der Naeherungskurve charakterisiert durch a[] mit den Originalwerten
            ochisq_stat= chisq[0];
System.out.println("chisq= "+chisq[0]);
            for (j=0;  j<ma;  j++) atry_stat[j]=a[j];
        }
        for (j=0;  j<mfit_stat;  j++) {  // Alter linearized fitting matrix, by augmenting diagonal elements.
            for (k=0;  k<mfit_stat;  k++) covar[j][k]=alpha[j][k];
            covar[j][j]= alpha[j][j]*(1.0 +alamda[0]);
            oneda_stat[j][0]= beta_stat[j];
        }
System.out.println("----------------\ncovar[][]:");
for (int i1=0;  i1<covar.length;  i1++) for (int i2=0;  i2<covar[0].length;  i2++) System.out.print(covar[i1][i2]+"\t"+(i2==covar[0].length-1 ? "\n": ""));
System.out.println("----------------\noneda_stat[][]:");
for (int i1=0;  i1<oneda_stat.length;  i1++) for (int i2=0;  i2<oneda_stat[0].length;  i2++) System.out.print(oneda_stat[i1][i2]+"\t"+(i2==oneda_stat[0].length-1 ? "\n": ""));
System.out.println("mfit_stat= "+mfit_stat);
        gaussj(covar,mfit_stat,oneda_stat,1);  // Matrix solution.
System.out.println("----------------\ncovar[][]:");
for (int i1=0;  i1<covar.length;  i1++) for (int i2=0;  i2<covar[0].length;  i2++) System.out.print(covar[i1][i2]+"\t"+(i2==covar[0].length-1 ? "\n": ""));
System.out.println("----------------\noneda_stat[][]:");
for (int i1=0;  i1<oneda_stat.length;  i1++) for (int i2=0;  i2<oneda_stat[0].length;  i2++) System.out.print(oneda_stat[i1][i2]+"\t"+(i2==oneda_stat[0].length-1 ? "\n": ""));
System.out.println("mfit_stat= "+mfit_stat);
        for (j=0;  j<mfit_stat;  j++) da_stat[j]=oneda_stat[j][0];
        if (alamda[0] == 0.0) {  // Once converged, evaluate covariance matrix.
            covsrt(covar,ma,ia,mfit_stat);
            covsrt(alpha,ma,ia,mfit_stat);  // Spread out alpha to its full size too.
            return;
        }
        for (j=-1,l=0;  l<ma;  l++) {  // Did the trial succeed?
            if (ia[l]==1) atry_stat[l]=a[l]+da_stat[++j];
//System.out.println(atry_stat[l]);
        }
        mrqcof(x,y,sig,ndata,atry_stat,ia,ma,covar,da_stat,chisq,funcs);
System.out.println("------\nchisq= "+chisq[0]);
        if (chisq[0] < ochisq_stat) {  // Success, accept the new solution.
            alamda[0] *= 0.1;
            ochisq_stat= chisq[0];
            for (j=0;  j<mfit_stat;  j++) {
                for (k=0;  k<mfit_stat;  k++) alpha[j][k]=covar[j][k];
                beta_stat[j]=da_stat[j];
            }
            for (l=0;  l<ma;  l++) a[l]=atry_stat[l];
        } else {  // Failure, increase alamda and return.
            alamda[0] *= 10.0;
            chisq[0]= ochisq_stat;
        }
    }


    /*
    Used by mrqmin to evaluate the linearized fitting matrix alpha, and vector beta as in (15.5.8), and calculate x^2.
    */

    // public void mrqcof (double x[], double y[], double sig[], int ndata, double a[], int ia[],
    //     int ma, double **alpha, double beta[], double *chisq,
    //     void (*funcs)(double, double [], double *, double [], int)) {
    //
    public void mrqcof (double[] x, double[] y, double[] sig, int ndata, double[] a, int[] ia,
        int ma, double[][] alpha, double[] beta, double[] chisq, MathFunktion funcs) {
        //
        int i,j,k,l,m,mfit=0;
        double wt,sig2i,dy;
        double[] ymod= new double[]{-1};  // wegen Datenuebergabe als Referenz
        double[] dyda= new double[ma];
        for (j=0;  j<ma;  j++)
            if (ia[j]==1) mfit++;
        for (j=0;  j<mfit;  j++) {  // Initialize (symmetric) alpha, beta.
            for (k=0;  k<j;  k++) alpha[j][k]=0.0;
            beta[j]=0.0;
        }
        chisq[0]= 0.0;
        for (i=0;  i<ndata;  i++) {  // Summation loop over all data.
            funcs.calc(x[i],a,ymod,dyda,ma);  // (*funcs)(x[i],a,&ymod,dyda,ma);
            sig2i= 1.0/(sig[i]*sig[i]);
            dy= y[i]-ymod[0];
            for (j=-1,l=0;  l<ma;  l++) {
                if (ia[l]==1) {
                    wt=dyda[l]*sig2i;
                    for (j++,k=-1,m=0;  m<l;  m++)
                        if (ia[m]==1) alpha[j][++k] += wt*dyda[m];
                    beta[j] += dy*wt;
                }
            }
            chisq[0] += dy*dy*sig2i;  // And find x^2.
        }
        for (j=1;  j<mfit;  j++)  // Fill in the symmetric side.
            for (k=0;  k<j;  k++) alpha[k][j]=alpha[j][k];
    }


    /*
    Expand in storage the covariance matrix covar, so as to take into account parameters that are
    being held fixed. (For the latter, return zero covariances.)
    */

    // #define SWAP(a,b) {swap=(a);(a)=(b);(b)=swap;}
    // public void covsrt (double **covar, int ma, int ia[], int mfit) {
    //
    public void covsrt (double[][] covar, int ma, int[] ia, int mfit) {
        int i,j,k;
        for (i=mfit;  i<ma;  i++)
            for (j=0;  j<=i;  j++) covar[i][j]=covar[j][i]=0.0;
        k=mfit;
        for (j=ma-1;  j>=0;  j--) {
            if (ia[j]==1) {
                for (i=0;  i<ma;  i++) {
                    double swap=covar[i][k];   covar[i][k]=covar[i][j];   covar[i][j]=swap;  // SWAP(covar[i][k],covar[i][j])
                }
                for (i=0;  i<ma;  i++) {
                    double swap=covar[k][i];   covar[k][i]=covar[j][i];   covar[j][i]=swap;  // SWAP(covar[k][i],covar[j][i])
                }
                k--;
            }
        }
    }


    /*
    Linear equation solution by Gauss-Jordan elimination, equation (2.1.1) above. a[1..n][1..n]
    is the input matrix. b[1..n][1..m] is input containing the m right-hand side vectors. On
    output, a is replaced by its matrix inverse, and b is replaced by the corresponding set of solution vectors.
    */

    // #define SWAP(a,b) {temp=(a);(a)=(b);(b)=temp;}
    // public void gaussj (double **a, int n, double **b, int m) {
    //
    public void gaussj (double[][] a, int n, double[][] b, int m) {
        int[] indxc,indxr,ipiv;
        int i,icol=0,irow=0,j,k,l,ll;
        double big,dum,pivinv,temp;
        indxc= new int[n];  // The integer arrays ipiv, indxr, and indxc are used for bookkeeping on the pivoting.
        indxr= new int[n];
        ipiv= new int[n];
        for (j=0;  j<n;  j++) ipiv[j]=0;
        for (i=0;  i<n;  i++) {  // This is the main loop over the columns to be reduced.
            big=0.0;
            for (j=0;  j<n;  j++)  // This is the outer loop of the search for a pivot element.
                if (ipiv[j] != 1)
                    for (k=0;  k<n;  k++) {
                        if (ipiv[k] == 0) {
                            if (Math.abs(a[j][k]) >= big) {
                                big=Math.abs(a[j][k]);
                                irow=j;
                                icol=k;
                            }
                        }
                    }
            ++(ipiv[icol]);
            // We now have the pivot element, so we interchange rows, if needed, to put the pivot
            // element on the diagonal. The columns are not physically interchanged, only relabeled:
            // indxc[i], the column of the ith pivot element, is the ith column that is reduced, while
            // indxr[i] is the row in which that pivot element was originally located. If indxr[i] _=
            // indxc[i] there is an implied column interchange. With this form of bookkeeping, the
            // solution b's will end up in the correct order, and the inverse matrix will be scrambled by columns.
            if (irow != icol) {
                for (l=0;  l<n;  l++) {
                    double swap=a[irow][l];   a[irow][l]=a[icol][l];   a[icol][l]=swap;  // SWAP(a[irow][l],a[icol][l]);
                }
                for (l=0;  l<m;  l++) {
                    double swap=b[irow][l];   b[irow][l]=b[icol][l];   b[icol][l]=swap;  // SWAP(b[irow][l],b[icol][l])
                }
            }
            indxr[i]= irow;  // We are now ready to divide the pivot row by the pivot element, located at irow and icol.
            indxc[i]= icol;
            if (a[icol][icol] == 0.0) System.out.println("gaussj: Singular Matrix");
            pivinv= 1.0/a[icol][icol];
            a[icol][icol]=1.0;
            for (l=0;  l<n;  l++) a[icol][l] *= pivinv;
            for (l=0;  l<m;  l++) b[icol][l] *= pivinv;
            for (ll=0;  ll<n;  ll++)  // Next, we reduce the rows...
                if (ll != icol) {  // ...except for the pivot one, of course.
                    dum= a[ll][icol];
                    a[ll][icol]=0.0;
                    for (l=0;  l<n;  l++) a[ll][l] -= a[icol][l]*dum;
                    for (l=0;  l<m;  l++) b[ll][l] -= b[icol][l]*dum;
                }
        }
        // This is the end of the main loop over columns of the reduction. It only remains to unscramble
        // the solution in view of the column interchanges. We do this by interchanging pairs of
        // columns in the reverse order that the permutation was built up.
        for (l=n-1;  l>=0;  l--) {
            if (indxr[l] != indxc[l])
                for (k=0;  k<n;  k++) {
                    double swap=a[k][indxr[l]];   a[k][indxr[l]]=a[k][indxc[l]];   a[k][indxc[l]]=swap;  // SWAP(a[k][indxr[l]],a[k][indxc[l]]);
                }
        }  // And we are done.
    }

    //==================================================================================
    //  15.2 Fitting Data to a Straight Line
    //==================================================================================

    /*
    Given a set of data points x[1..ndata],y[1..ndata] with individual standard deviations
    sig[1..ndata], fit them to a straight line y = a + bx by minimization. Returned are
    a,b and their respective probable uncertainties siga and sigb, the chi-square chi2, and the
    goodness-of-fit probability q (that the fit would have q2 this large or larger). If mwt=0 on
    input, then the standard deviations are assumed to be unavailable: q is returned as 1.0 and
    the normalization of chi2 is to unit standard deviation on all points.
    */

    // public void fit (double x[], double y[], int ndata, double sig[], int mwt, double *a,
    //     double *b, double *siga, double *sigb, double *chi2, double *q) {
    //
    public void fit (double x[], double y[], int ndata, double sig[], int mwt, double[] a,
        double[] b, double[] siga, double[] sigb, double[] chi2, double[] q) {
        //
        int i;
        double wt,t,sxoss,sx=0.0,sy=0.0,st2=0.0,ss,sigdat;
        b[0]= 0.0;
        if (mwt==1) {  // Accumulate sums ...
            ss=0.0;
            for (i=0;  i<ndata;  i++) {  // ...with weights
                wt= 1.0/(sig[i]*sig[i]);
                ss += wt;
                sx += x[i]*wt;
                sy += y[i]*wt;
            }
        } else {
            for (i=0;  i<ndata;  i++) {  // ...or without weights.
                sx += x[i];
                sy += y[i];
            }
            ss=ndata;
        }
        sxoss=sx/ss;
        if (mwt==1) {
            for (i=0;  i<ndata;  i++) {
                t=(x[i]-sxoss)/sig[i];
                st2 += t*t;
                b[0] += t*y[i]/sig[i];
            }
        } else {
            for (i=0;  i<ndata;  i++) {
                t=x[i]-sxoss;
                st2 += t*t;
                b[0] += t*y[i];
            }
        }
        b[0] /= st2;  // Solve for a, b, sig_a, and sig_b.
        a[0]= (sy-sx*b[0])/ss;
        siga[0]= Math.sqrt((1.0+sx*sx/(ss*st2))/ss);
        sigb[0]= Math.sqrt(1.0/st2);
        chi2[0]= 0.0;  // Calculate chi^2.
        q[0]= 1.0;
        if (mwt == 0) {
            for (i=0;  i<ndata;  i++)
                chi2[0] += ((y[i]-a[0]-b[0]*x[i])*(y[i]-a[0]-b[0]*x[i]));
            sigdat= Math.sqrt(chi2[0]/(ndata-2));  // For unweighted data evaluate typical sig using chi2, and adjust the standard deviations.
            siga[0] *= sigdat;
            sigb[0] *= sigdat;
        } else {
            for (i=0;  i<ndata;  i++)
                chi2[0] += (((y[i]-a[0]-b[0]*x[i])/sig[i])*((y[i]-a[0]-b[0]*x[i])/sig[i]));
            if (ndata>2) q[0]= gammq(0.5*(ndata-2),0.5*chi2[0]);  // Equation (15.2.12).
        }
    }


    /*
    Returns the incomplete gamma function P(a, x).
    */
    public double gammp (double a, double x) {
        // void gcf(double *gammcf, double a, double x, double *gln);
        // void gser(double *gamser, double a, double x, double *gln);
        double[] gamser=new double[1], gammcf=new double[1], gln=new double[1];
        if ((x < 0.0) || (a <= 0.0)) System.out.println("Invalid arguments in routine gammp");
        if (x < (a+1.0)) {  // Use the series representation.
            gser(gamser,a,x,gln);
            return gamser[0];
        } else {  // Use the continued fraction representation
            gcf(gammcf,a,x,gln);
        return (1.0-gammcf[0]);  // and take its complement.
        }
    }

    /*
    Returns the incomplete gamma function Q(a, x) = 1 - P(a, x).
    */
    public double gammq (double a, double x) {
        // void gcf(double *gammcf, double a, double x, double *gln);
        // void gser(double *gamser, double a, double x, double *gln);
        double[] gamser=new double[1], gammcf=new double[1], gln=new double[1];
        if ((x < 0.0) || (a <= 0.0)) System.out.println("Invalid arguments in routine gammq");
        if (x < (a+1.0)) {  // Use the series representation
            gser(gamser,a,x,gln);
            return (1.0-gamser[0]);  // and take its complement.
        } else {  // Use the continued fraction representation.
            gcf(gammcf,a,x,gln);
        return gammcf[0];
        }
    }



    private static final int ITMAX_GSER= 100;
    private static final double EPS_GSER= 3.0e-7;

    /*
    Returns the incomplete gamma function P(a, x) evaluated by its series representation as gamser.
    Also returns ln (a) as gln.
    */

    // public void gser (double *gamser, double a, double x, double *gln) {
    //
    public void gser (double[] gamser, double a, double x, double[] gln) {
        // double gammln(double xx);
        int n;
        double sum,del,ap;
        gln[0]= gammln(a);
        if (x <= 0.0) {
            if (x < 0.0) System.out.println("x less than 0 in routine gser");
            gamser[0]= 0.0;
            return;
        } else {
            ap=a;
            del=sum=1.0/a;
            for (n=0;  n<ITMAX_GSER;  n++) {
                ++ap;
                del *= x/ap;
                sum += del;
                if (Math.abs(del) < Math.abs(sum)*EPS_GSER) {
                    gamser[0]= sum*Math.exp(-x+a*Math.log(x)-gln[0]);
                    return;
                }
            }
            System.out.println("a too large, ITMAX too small in routine gser");
            return;
        }
    }


    private static final int ITMAX_GFC= 100;  // Maximum allowed number of iterations.
    private static final double EPS_GFC= 3.0e-7;  // Relative accuracy.
    private static final double FPMIN_GFC= 1.0e-30;  // Number near the smallest representable doubleing-point number.

    /*
    Returns the incomplete gamma function Q(a, x) evaluated by its continued fraction representation
    as gammcf. Also returns ln(a) as gln.
    */

    // public void gcf(double *gammcf, double a, double x, double *gln)
    //
    public void gcf (double[] gammcf, double a, double x, double[] gln) {
        //double gammln(double xx);
        int i;
        double an,b,c,d,del,h;
        gln[0]= gammln(a);
        b=x+1.0-a;  // Set up for evaluating continued fraction by modified Lentz's method (5.2) with b0 = 0.
        c=1.0/FPMIN_GFC;
        d=1.0/b;
        h=d;
        for (i=0;  i<ITMAX_GFC;  i++) {  // Iterate to convergence.
            an = -i*(i-a);
            b += 2.0;
            d=an*d+b;
            if (Math.abs(d) < FPMIN_GFC) d=FPMIN_GFC;
            c=b+an/c;
            if (Math.abs(c) < FPMIN_GFC) c=FPMIN_GFC;
            d=1.0/d;
            del=d*c;
            h *= del;
            if (Math.abs(del-1.0) < EPS_GFC) break;
        }
        if (i > ITMAX_GFC) System.out.println("a too large, ITMAX too small in gcf");
        gammcf[0]= Math.exp(-x+a*Math.log(x)-gln[0])*h;  // Put factors in front.
    }


    //------------------------------
    // globales 'Gedaechtnis' der Methode 'gammln()':
    private double[] cof_stat= new double[]{
        76.18009172947146,-86.50532032941677, 24.01409824083091,-1.231739572450155, 0.1208650973866179e-2,-0.5395239384953e-5
    };
    //------------------------------

    /*
    Returns the value ln[(xx)] for xx > 0.
    */

    public double gammln (double xx) {
        // Internal arithmetic will be done in double precision, a nicety that you can omit if five-figure accuracy is good enough.
        double x=xx, y=xx, tmp=x+5.5, ser=1.000000000190015;
        int j;
        tmp -= (x+0.5)*Math.log(tmp);
        for (j=0;  j<=5;  j++) ser += cof_stat[j]/++y;
        return (-tmp+Math.log(2.5066282746310005*ser/x));
    }

    //==================================================================================
    //==================================================================================


}



