/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.mathe;


public class FourierTransform extends Object {

    //------------------------------------------------
    private static final double ln10= Math.log(10);
    //------------------------------------------------


    public FourierTransform () {}

    
    

    // INPUT: Zeitverlauf >>   zv[2][t.length] --> {zeit, wert}
    // OUTPUT: Spektrum   >>   sp[2][n2+1] --> {an, bn}
    // Annahme: die Grundfrequenz ist durch den Zeitbereich gegeben: T1= zv[0][t.length-1] -zv[0][0]
    // ACHTUNG: Rueckgegebenes Spektrum hat Index gleich der Nummer der Harmonischen 
    // 
    public static float[][] calcSpektrum (double zv[][], int n1, int n2) {
        //-------------
        float[][] erg= new float[2][n2+1]; 
        double f1= 1/(zv[0][zv[0].length-1]-zv[0][0]); 
        double dt= zv[0][1]-zv[0][0]; 
        //-------------
        for (int i1=0;  i1<zv[0].length;  i1++) {
            for (int n=n1;  n<n2+1;  n++) {
                double phiN= (2*Math.PI*f1)*n*zv[0][i1]; 
                erg[0][n] += (float)(zv[1][i1]*Math.cos(phiN)*dt);  // an
                erg[1][n] += (float)(zv[1][i1]*Math.sin(phiN)*dt);  // bn
            }
        }
        //-------------
        for (int n=n1;  n<n2+1;  n++) {
            erg[0][n] *= (float)(2*f1);  // an
            erg[1][n] *= (float)(2*f1);  // bn
        }
        // eventueller Gleichanteil: 
        erg[0][0] *= (float)0.5; 
        erg[1][0] =  (float)0; 
        //-------------
        return erg; 
    }

    

    // INPUT: Spektrum und erste Harmonische ungleich Null und Grundfrequenz und Zeitschritt-Aufloesung >>   sp[2][n2] --> {an, bn} 
    // OUTPUT: Zeitverlauf  >>    zv[2][t.length] --> {zeit, wert}
    // 
    public static double[][] reconstructZV (float sp[][], int n1, double f1, double dt) {
        //-------------
        int zeitpunkte= (int)(1/f1/dt);  // soviele Stuetzpunkte soll die rekonstruierte Kurve haben
        double[][] zv= new double[2][zeitpunkte]; 
        //-------------
        for (int i1=0;  i1<zeitpunkte;  i1++) {
            double zeit= i1*dt; 
            double wert= 0; 
            for (int n=n1;  n<sp[0].length;  n++) {
                double cN= Math.sqrt(sp[0][n]*sp[0][n]+sp[1][n]*sp[1][n]); 
                double phiN= Math.atan2(sp[1][n],sp[0][n]); 
                wert += cN*Math.cos(2*Math.PI*f1*n*zeit -phiN); 
            }
            zv[0][i1]= zeit; 
            zv[1][i1]= wert; 
        }
        //-------------
        return zv; 
    }


}





