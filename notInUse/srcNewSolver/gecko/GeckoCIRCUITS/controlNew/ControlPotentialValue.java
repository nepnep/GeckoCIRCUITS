/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gecko.GeckoCIRCUITS.controlNew;

/**
 *
 * @author andy
 */
public class ControlPotentialValue {

    // public, since we read this very often!
    public double _value;

    // just for checking, remove later
    private double _lastTime = -1;
    private final int _potIndex;

    public void setValue(double value, double time){
        if(_lastTime == time) {
            //System.err.println("Output terminal written twice in time: " + time);
        }
        _value = value;
        _lastTime = time;
    }

    public ControlPotentialValue(int potIndex) {
        _potIndex = potIndex;
    }

    public int getPotIndex() {
        return _potIndex;
    }

}
