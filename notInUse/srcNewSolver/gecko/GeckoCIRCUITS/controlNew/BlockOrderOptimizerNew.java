/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.GeckoCIRCUITS.controlNew;

import gecko.GeckoCIRCUITS.elements.ElementControl;
import gecko.GeckoCIRCUITS.elements.ElementControl.ControlType;
import gecko.GeckoCIRCUITS.terminal.Terminal;
import gecko.GeckoCIRCUITS.terminal.TerminalControlIn;
import gecko.GeckoCIRCUITS.terminal.TerminalControlOut;
import java.util.*;

// basiert auf einem Code, den Ivane im Sept. 2007 speziell fuer dieses Problem geschrieben hat
// Achtung: Funktioniert nur, wenn ein Pfad einen Quellen- und einen Senkenblock aufweist -->
// das ist aber sowieso die Voraussetzung fuer die Simulation
/**
 *
 * @author andreas I mostly reworked the mess of Ivana/Uwe. It should still do the same as the original Ivana-Code, but now it is
 * a bit easier to read! Following comment: The alrogithm uses a tree-structure for the detection of loops in the control
 * structure. Note: by "loop", I don't only mean a real loop in the control circuit. Depending on the execution order of the
 * different blocks, "time-dependent" loops are generated, i.e. a block depends on the execution result of a previously executed
 * block. The optimization-algorithm should try to find an order, for which the total number of time-dependent loops is
 * minimized. Additional comment: since "real" loops cannot be avoided, they should maybe not be taken into account for the loop
 * number reduction State of the code: at the moment, a loop detection tree is built for every source control block. Then, based
 * on this tree, a order is determined with smaller amount of loops. However, the "global" optimiziation does not take into
 * account (or mix) the different trees with different sources as root node. Therefore, we could still improve this in the
 * future, if the execution order of the control blocks is an issue. Up to now, this algorithm worked quite well for all known
 * simulation models.
 *
 *
 */
public final class BlockOrderOptimizerNew {

    private final List<Node> _sourcesList = new ArrayList<Node>();
    private final List<Node> _transferList = new ArrayList<Node>();
    private final List<Node> _sinkList = new ArrayList<Node>();
    /**
     * the non-optimized output list, where sources are at the beginning, transfer blocks intermediate and sink blocks at the
     * end!
     */
    private List<ElementControl> _allControlsInput;
    private List<Node> _optimizedList;

    public BlockOrderOptimizerNew(final List<ElementControl> regler) {
        initialize(regler);
        optimize();
    }

    public void initialize(final List<ElementControl> allControls) {
        this._allControlsInput = Collections.unmodifiableList(allControls);

        for (ElementControl element1 : _allControlsInput) {
            final Set<ElementControl> predecessorBlocks = findPredecessorBlocks(element1);
            sortControlBlocksToNodes(element1, predecessorBlocks);

        }
    }

    public List<ElementControl> getOptimierteAbarbeitungsListe() {

        if (_optimizedList == null) {
            return createSimpleOutputList();
        }

        // maybe for later: the check should be re-enabled again???
        // Checksumme:
////////////        int checksum = 0, base = 0;
////////////        for (int i1 = 0; i1 < optList.size(); i1++) {
////////////            base += i1;  // wenn alles korrekt gelaufen ist, dann hat 'checksum' diesen Wert
////////////            checksum += abarbeitungsListe[i1];
////////////        }
////////////        if (base != checksum) {
////////////            System.out.println("Checksum-Error: oierwng0qgg");
////////////            for (int i1 = 0; i1 < _allControlsAsInputGiven.size(); i1++) {
////////////                abarbeitungsListe[i1] = i1;
////////////            }
////////////            ArrayList<ElementControl> outList = new ArrayList<ElementControl>();
////////////
////////////            for (int i = 0; i < abarbeitungsListe.length; i++) {
////////////                outList.add(_allControlsAsInputGiven.get(abarbeitungsListe[i]));
////////////            }
////////////
////////////            return outList;
////////////
////////////        }
        //------------
        // System.out.println("abarbeitungsListe[] OK");        

        final List<ElementControl> outList = new ArrayList<ElementControl>();

        for (int i = 0; i < _allControlsInput.size(); i++) {
            outList.add(_optimizedList.get(i).getElementControl());
        }

//      // use this for refactoring, to enshure the same output order!  
//        long hashValue = 0;
//        for (int i = 0; i < outList.size(); i++) {
//            hashValue += (i + 2) * outList.get(i).getName().hashCode();
//        }
//        System.out.println("hash:  " + hashValue);
//        assert hashValue == -1387413339;

        return outList;
    }

    public void optimize() {

        int minimumNumLoops = Integer.MAX_VALUE;

        // finding the best ordered array for each source            
        for (int sourceIndex = 0; sourceIndex < _sourcesList.size(); sourceIndex++) {

            final List<Node> allNodes = new ArrayList<Node>();
            allNodes.addAll(_sourcesList);
            allNodes.addAll(_transferList);
            allNodes.addAll(_sinkList);

            for (Node node : allNodes) {
                node.clearDistanceToSource();
            }

            final BuildTree initialBTree = new BuildTree(_sourcesList, _transferList, _sinkList, sourceIndex, allNodes);
            final List<Node> listOrder = initialBTree.findBestOrder(allNodes);
            final int curNumLoops = BuildTree.countNewNumLoops(listOrder);
            if (curNumLoops < minimumNumLoops) {
                _optimizedList = listOrder;
                minimumNumLoops = curNumLoops;
            }
        }
    }

    private List<ElementControl> createSimpleOutputList() {
        final List<ElementControl> nonOptimizedList = new ArrayList<ElementControl>();
        for (Node sourceNode : _sourcesList) {
            nonOptimizedList.add(sourceNode.getElementControl());
        }
        for (Node transferNode : _transferList) {
            nonOptimizedList.add(transferNode.getElementControl());
        }
        for (Node sinkNode : _sinkList) {
            nonOptimizedList.add(sinkNode.getElementControl());
        }
        return Collections.unmodifiableList(nonOptimizedList);
    }

    private void sortControlBlocksToNodes(final ElementControl element1, final Set<ElementControl> predecessorBlocks) {
        switch (element1.getType()) {
            case SOURCE:
                _sourcesList.add(Node.sourceNodeFabric(element1));
                break;
            case SINK:
                _sinkList.add(Node.sinkNodeFabric(element1, predecessorBlocks));
                break;
            case TRANSFER:
                _transferList.add(Node.transferNodeFabric(element1, predecessorBlocks));
                break;
            default:
                assert false;
        }
    }

    private Set<ElementControl> findPredecessorBlocks(final ElementControl element1) {

        final List<Integer> nodeIndexIn = new ArrayList<Integer>();
        for (Terminal term : element1._terminals) {
            if (term instanceof TerminalControlIn) {
                nodeIndexIn.add(term.getIndex());
            }
        }
        

        final Set<ElementControl> returnValue = new LinkedHashSet<ElementControl>();

        for (ElementControl element2 : _allControlsInput) {
            if (element2 == element1) {
                continue;
            }

            for (Terminal term : element2.getTerminals()) {
                if (term instanceof TerminalControlOut) {
                    final int nodeIndex = term.getIndex();
                    for (int i4 = 0; i4 < nodeIndexIn.size(); i4++) {
                        if (nodeIndex == nodeIndexIn.get(i4)) {
                            returnValue.add(element2);
                        }
                    }
                }
            }
        }
        return returnValue;
    }
}

final class Node {

    private int _priority;
    private final Set<ElementControl> _inputs;
    private final ElementControl _elementControl;
    private final List<Integer> _distToSource = new ArrayList<Integer>();

    public static Node sinkNodeFabric(final ElementControl elementControl, final Set<ElementControl> inputs) {
        assert elementControl.getType() == ControlType.SINK;
        final Node returnValue = new Node(elementControl, Collections.unmodifiableSet(inputs));
        returnValue.setPriority(Integer.MAX_VALUE);
        return returnValue;
    }

    public static Node sourceNodeFabric(final ElementControl elementControl) {
        assert elementControl.getType() == ControlType.SOURCE;
        final Set<ElementControl> emptySet = Collections.emptySet();
        final Node returnValue = new Node(elementControl, emptySet); // Source elements don't have inputs        
        returnValue.setPriority(0);
        return returnValue;
    }

    public static Node transferNodeFabric(final ElementControl elementControl, final Set<ElementControl> inputs) {
        assert elementControl.getType() == ControlType.TRANSFER;
        final Node returnValue = new Node(elementControl, Collections.unmodifiableSet(inputs));
        returnValue.setPriority(1);
        return returnValue;
    }

    public ControlType getControlType() {
        return _elementControl.getType();
    }

    /*
     * use fabric methods for object construction!
     */
    private Node(final ElementControl elementControl, final Set<ElementControl> inputs) {
        _elementControl = elementControl;
        this._inputs = inputs;
    }

    public ElementControl getElementControl() {
        return _elementControl;
    }

    public Set<ElementControl> getInputs() {
        return _inputs;
    }

    public int getPriority() {

        if (getDistanceToSource() == null) {
            return _priority;
        } else {
            return _distToSource.get(0);
        }

    }

    private void setPriority(final int priority) {
        _priority = priority;
    }

    void addDistanceToSource(final int level) {
        _distToSource.add(level);
    }

    void clearDistanceToSource() {
        _distToSource.clear();
    }

    Integer getDistanceToSource() {
        if (_distToSource.isEmpty()) {
            return null;
        } else {
            return _distToSource.get(0);
        }
    }
}

final class BuildTree {

    private final List<Node> _rootNodes = new ArrayList<Node>();
    private final List<Node> _nonRootNodes = new ArrayList<Node>();
    private final List<List<Node>> _qTree;

    public BuildTree(final List<Node> sourceElems, final List<Node> transferElems, final List<Node> sinkElems,
            final int sourceIndex, final List<Node> allNodes) {

        _rootNodes.addAll(sourceElems);
        _nonRootNodes.addAll(transferElems);
        _nonRootNodes.addAll(sinkElems);
        _qTree = makeAndInsertNode(new LinkedHashSet<Node>(), _rootNodes.get(sourceIndex), 0, new ArrayList<List<Node>>());

        buildQTree(allNodes, _qTree);
    }

    public void buildQTree(final List<Node> allNodes, final List<List<Node>> brTree) {
        for (Node node : allNodes) {
            for (int level = 1; level < brTree.size(); level++) {
                if (node.getElementControl().getType() != ControlType.TRANSFER) {
                    continue;
                }
                if (brTree.get(level).contains(node)) {
                    node.addDistanceToSource(level);
                }
            }
        }
    }

    /**
     * Builds the tree recursively
     *
     * @param allParentNodes all nodes that are parents - IMPORTANT note: not only direct parents should be listed, also parents
     * of parents, and so on.
     * @param node
     * @param level
     * @param elements
     * @return input-output-parameter, elements is given as parameter, but also returned, since a node was added
     */
    private List<List<Node>> makeAndInsertNode(final Set<Node> allParentNodes, final Node node, final int level,
            final List<List<Node>> elements) {

        if (level >= elements.size()) {
            elements.add(new ArrayList<Node>());
        }

        for (Node nonRootNode : _nonRootNodes) {
            if (isInputOfElement(nonRootNode, node)) {
                allParentNodes.add(node);
                if (!allParentNodes.contains(nonRootNode)) { // if no loop is present in graph
                    makeAndInsertNode(allParentNodes, nonRootNode, level + 1, elements);
                }
            }
        }

        elements.get(level).add(node);
        return elements;
    }

    public List<List<Node>> getTree() {
        return _qTree;
    }

    public static boolean isInputOfElement(final Node node, final Node possibleInput) {
        for (ElementControl bInput : node.getInputs()) {
            if (bInput == possibleInput.getElementControl()) {
                return true;
            }
        }
        return false;
    }

    /**
     * count a number of loops in new ordered list of elements it should be equal to Min Number of loops evaluted from Initial
     * tree -->
     */
    public static int countNewNumLoops(final List<Node> elements) {
        int loop = 0;
        for (int j = 0; j < elements.size() - 1; j++) {
            for (int ji = j; ji < elements.size(); ji++) {
                if (isInputOfElement(elements.get(j), elements.get(ji))) {
                    loop++;
                }
            }
        }
        return loop;
    }

    public List<Node> findBestOrder(final List<Node> elements) {

        final List<Node> orderList = new LinkedList<Node>();

        for (Node insertionNode : elements) {
            final int position = findInsertPosition(insertionNode, orderList);

            if (position < orderList.size()) {
                orderList.add(position, insertionNode);
            } else {
                orderList.add(insertionNode);
            }
        }

        return orderList;
    }

    private static int findInsertPosition(final Node insertElement, final List<Node> insertList) {
        for (int ie = 0; ie < insertList.size();) {
            final Node compareElement = insertList.get(ie);
            if (insertElement.getPriority() > compareElement.getPriority()) {
                ie++;
                continue;
            }

            if (insertElement.getPriority() == compareElement.getPriority()) {
                if (isInputOfElement(compareElement, insertElement)) {
                    return ie;
                } else {
                    ie++;
                }
                continue;
            }

            if (insertElement.getPriority() < compareElement.getPriority()) {
                return ie;
            }
        }

        // if no insert position could be found, return
        // size of list, which indicates to add the element at the
        // List's end.
        return insertList.size();
    }
}
