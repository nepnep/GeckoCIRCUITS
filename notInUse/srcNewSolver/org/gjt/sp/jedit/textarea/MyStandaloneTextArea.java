/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gjt.sp.jedit.textarea;

//{{{ Imports
import gecko.GeckoCIRCUITS.allg.GetJarPath;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import org.gjt.sp.jedit.IPropertyManager;
import org.gjt.sp.jedit.JEditActionSet;
import org.gjt.sp.jedit.JEditBeanShellAction;
import org.gjt.sp.jedit.Mode;
import org.gjt.sp.jedit.buffer.DefaultFoldHandlerProvider;
import org.gjt.sp.jedit.buffer.FoldHandler;
import org.gjt.sp.jedit.buffer.IndentFoldHandler;
import org.gjt.sp.jedit.buffer.JEditBuffer;
import org.gjt.sp.jedit.buffer.KillRing;
import org.gjt.sp.jedit.syntax.ModeProvider;
import org.gjt.sp.jedit.syntax.ParserRuleSet;
import org.gjt.sp.jedit.syntax.TokenMarker;
import org.gjt.sp.util.IOUtilities;
import org.gjt.sp.util.Log;
import org.gjt.sp.util.SyntaxUtilities;


public class MyStandaloneTextArea extends StandaloneTextArea {

    
    public MyStandaloneTextArea(IPropertyManager propertyManager) {

        super(propertyManager);

        initInputHandler();


        JEditActionSet<JEditBeanShellAction> actionSet = new StandaloneActionSet(propertyManager, this, TextArea.class.getResource("textarea.actions.xml"));

        addActionSet(actionSet);
        actionSet.load();
        actionSet.initKeyBindings();

        //{{{ init Style property manager
        if (SyntaxUtilities.propertyManager == null) {
            SyntaxUtilities.propertyManager = propertyManager;
        }
        //}}}

        
        
        DefaultFoldHandlerProvider foldHandlerProvider = new DefaultFoldHandlerProvider() {

            @Override
            public FoldHandler getFoldHandler(String name) {
                return super.getFoldHandler("indent");
            }
        };

        FoldHandler.foldHandlerProvider = foldHandlerProvider;
        foldHandlerProvider.addFoldHandler(new IndentFoldHandler());
        JEditBuffer buffer = new JEditBuffer();
        TokenMarker tokenMarker = new TokenMarker();
        tokenMarker.addRuleSet(new ParserRuleSet("text", "MAIN"));
        buffer.setTokenMarker(tokenMarker);
        setBuffer(buffer);
        String property = propertyManager.getProperty("buffer.undoCount");
        int undoCount = 100;
        if (property != null) {
            try {
                undoCount = Integer.parseInt(property);
            } catch (NumberFormatException e) {
            }
        }
        this.buffer.setUndoLimit(undoCount);
        Mode mode = new Mode("text");
        mode.setTokenMarker(tokenMarker);
        ModeProvider.instance.addMode(mode);
        KillRing.setInstance(new KillRing());
        KillRing.getInstance().propertiesChanged(100);

    } //}}}

//    
//
//    //{{{ initGutter() method
//    private void initGutter() {
//        Gutter gutter = getGutter();
//        gutter.setExpanded(getBooleanProperty(
//                "view.gutter.lineNumbers"));
//        int interval = getIntegerProperty(
//                "view.gutter.highlightInterval", 5);
//        gutter.setHighlightInterval(interval);
//        gutter.setCurrentLineHighlightEnabled(getBooleanProperty(
//                "view.gutter.highlightCurrentLine"));
//        gutter.setStructureHighlightEnabled(getBooleanProperty(
//                "view.gutter.structureHighlight"));
//        gutter.setStructureHighlightColor(
//                getColorProperty("view.gutter.structureHighlightColor"));
//        gutter.setBackground(
//                getColorProperty("view.gutter.bgColor"));
//        gutter.setForeground(
//                getColorProperty("view.gutter.fgColor"));
//        gutter.setHighlightedForeground(
//                getColorProperty("view.gutter.highlightColor"));
//        gutter.setFoldColor(
//                getColorProperty("view.gutter.foldColor"));
//        gutter.setCurrentLineForeground(
//                getColorProperty("view.gutter.currentLineColor"));
//        String alignment = getProperty(
//                "view.gutter.numberAlignment");
//        if ("right".equals(alignment)) {
//            gutter.setLineNumberAlignment(Gutter.RIGHT);
//        } else if ("center".equals(alignment)) {
//            gutter.setLineNumberAlignment(Gutter.CENTER);
//        } else // left == default case
//        {
//            gutter.setLineNumberAlignment(Gutter.LEFT);
//        }
//
//        gutter.setFont(getFontProperty("view.gutter.font"));
//
//        int width = getIntegerProperty(
//                "view.gutter.borderWidth", 3);
//        gutter.setBorder(width,
//                getColorProperty("view.gutter.focusBorderColor"),
//                getColorProperty("view.gutter.noFocusBorderColor"),
//                painter.getBackground());
//    } //}}}
//
//    
    

    

    //{{{ createTextArea() method
    /**
     * Create a standalone TextArea.
     * If you want to use it in jEdit, please use {@link JEditEmbeddedTextArea#JEditEmbeddedTextArea()}
     *
     * @return a textarea
     * @since 4.3pre13
     */
    public static MyStandaloneTextArea createTextArea() {
        try{
        final Properties props = new Properties();
        props.putAll(loadProperties("/org/gjt/sp/jedit/jedit_keys.props"));
        props.putAll(loadProperties("/org/gjt/sp/jedit/jedit.props"));
        props.setProperty("view.gutter.lineNumbers", "true");
        props.setProperty("folding", "indent");
        MyStandaloneTextArea textArea = new MyStandaloneTextArea(new IPropertyManager() {

            public String getProperty(String name) {
                return props.getProperty(name);
            }
        });


        Mode mode = new Mode("xml");
        GetJarPath jp = new GetJarPath(MyStandaloneTextArea.class);
        String javaXMLFilePath = jp.getJarPath() + "lib/java.xml";                    
        mode.setProperty("file", javaXMLFilePath);
        ModeProvider.instance.addMode(mode);
        textArea.getBuffer().setMode(mode);
        return textArea;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    } // }}}

    private static Properties loadProperties(String fileName) {
        Properties props = new Properties();
        InputStream in = TextArea.class.getResourceAsStream(fileName);
        try {
            props.load(in);
        } catch (IOException e) {
            Log.log(Log.ERROR, TextArea.class, e);
        } finally {
            IOUtilities.closeQuietly(in);
        }
        return props;
    } //}}}

    
    
}
