/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * -This class defines two HashMaps needed for assigning keys to values and
 * values to keys for a specific language.
 * -A new instance of this class needs to be defined for each language used.
 */
package admintool;

import java.util.HashMap;

public class DoubleMap {
    
    /**
     * maps keys to values, e.g. "GeckoFrame.jMenuItem17.text" to "Export"
     */
    public HashMap<String,String> KeytoValue;
    
    /**
     * maps values to keys, e.g. "Export" to "GeckoFrame.jMenuItem17.text"
     */
    public HashMap<String,String> ValuetoKey;
    

    /**
     * Constructor
     */
    public DoubleMap() {
        KeytoValue = new HashMap<String,String>();
        ValuetoKey = new HashMap<String,String>();
    }

    /**
     * Method to add entries to the DoubleMap
     * @param key Key to add
     * @param value Value to add
     */
    public void putPair(String key, String value) {
        KeytoValue.put(key, value);
        ValuetoKey.put(value, key);
    }
    
    /**
     * Method to remove entries from the DoubleMap
     * @param key Key to remove
     * @param value Value to remove
     */
    public void removePair(String key, String value) {
        KeytoValue.remove(key);
        ValuetoKey.remove(value);
    }
   
}