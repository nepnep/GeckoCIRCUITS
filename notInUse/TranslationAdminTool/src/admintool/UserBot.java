/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * -User Bot class
 */
package admintool;

import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import net.sourceforge.jwbf.core.contentRep.SimpleArticle;

import java.util.HashMap;

public class UserBot {
        
    private MediaWikiBot b;
    private static int progress = 0; // Login progress (percent)
    /**
     * Username of the user
     */
    public static String user;
    /**
     * Password of the user
     */
    public static String pword;

    /**
     * Creates a new User Bot and logs in.
     * @param username Username to log in
     * @param password Password
     */
    public UserBot(String username, String password) {
        user = username;
        progress = 2; // update progress
        pword = password;
        progress = 3; // update progress
        login(user,pword);
        progress = 100; // indicate completion
    }
    
    /*
     * Method to create a new User Bot and log in.
     */
    private void login(String username, String password) {
        try {
            // print DEBUG messages in console if DEBUG_MODE is turned on
            if (InitParameters.DEBUG_MODE) {
                org.apache.log4j.BasicConfigurator.configure(); // configure log4j
            }
            b = new MediaWikiBot(InitParameters.WIKI_URL);
            progress = 55; // update progress
            b.login(username, password);
            progress = 78; // update progress
            user = username;
            pword = password;
        } catch (Exception e) {
            e.printStackTrace();
            new Dialog().setVisible(true);
        }
    }
    
    /**
     * Method to check if bot is logged in
     * @return True if logged in, False otherwise
     */
    public boolean isLoggedIn() {
        if (b.isLoggedIn()) {
            return true;
        } else {
            return false;
        }
    } 
    
    /**
     * Method to get progress
     * - To be called while executing instructions from a separate thread
     * @return Login progress (percentage complete)
     */
    public static int getProgress() {
        return progress;
    }
}
