/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * Admin Toolbox GUI class.
 */
package admintool;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ProgressMonitor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.SwingWorker;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;

public class Tool extends javax.swing.JFrame implements PropertyChangeListener {

    private String language;
    private DoubleMap englishMap_single;
    /**
     * Multiple-line English DoubleMap
     */
    public static DoubleMap englishMap_multiple; // also used in DLbot class
    private DoubleMap transMap_single;
    private DoubleMap transMap_multiple;
    private Integer counterSingle;
    private Integer counterMultiple;
    private int maxCounterSingle;
    private int maxCounterMultiple;
    private HashMap<Integer,String> keysSingle;
    private HashMap<Integer,String> keysMultiple;
    private String[] suggestionsSingle;
    private String[] suggestionsMultiple;
    private String[] commentsPagesSingle;
    private String[] commentsPagesMultiple;
    private String codeSingle;
    private String codeMultiple;
    private Integer selectedIndex; // used for single-line
    private String validatedSuggestion;
    private String badSuggestion;
    private String comment;
    private String triggerName = ""; // used to determine instructions for background task thread
    private int currentIndex; // used for multiple-line
    private int maxCurrentIndex; // used for multiple-line
    private String currentSuggestionMultiple;
    private String currentCommentMultiple;
    private static ProgressMonitor progressMonitor; // Progress Monitor GUI
    private Task task; // Background Task Thread
    private Progress progress; // getProgress Thread
    
    /*
     * Inner class used to execute download and upload instructions
     * from a separate thread to avoid freezing up.
     */
    private class Task extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            // Execute background thread method for trigger
            if (triggerName.equals(InitParameters.GET_SUGGESTIONS_BUTTON)) {
                backgroundGetSuggestions(); // execute method for "Get Suggestions" button
            } else if (triggerName.equals(InitParameters.VALIDATE_SINGLE_BUTTON)) {
                backgroundValidateSingle(); // execute method for "Validate" (single-line) button
            } else if (triggerName.equals(InitParameters.REMOVE_SINGLE_BUTTON)) {
                backgroundRemoveSingle(); // execute method for "Remove" (single-line) button
            } else if (triggerName.equals(InitParameters.COMMENT_SINGLE)) {
                backgroundCommentSingle(); // execute method for COMMENT_SINGLE trigger
            } else if (triggerName.equals(InitParameters.GET_NEXT_SUGG_BUTTON_MULTIPLE) || triggerName.equals(InitParameters.GET_PREV_SUGG_BUTTON_MULTIPLE) || triggerName.equals(InitParameters.NEXT_ITEM_BUTTON_MULTIPLE) || triggerName.equals(InitParameters.PREV_ITEM_BUTTON_MULTIPLE)){
                backgroundSuggestionMultiple(); // execute method to get a multiple-line suggestion & comment from the Wiki                
            } else if (triggerName.equals(InitParameters.VALIDATE_MULTIPLE_BUTTON)) {
                backgroundValidateMultiple(); // execute method for "Validate" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.REMOVE_MULTIPLE_BUTTON)) {
                backgroundRemoveMultiple(); // execute method for "Remove" (multiple-line) button
            }
            return null;
        }
        
        @Override
        public void done() {
            // Execute done thread method for trigger
            if (triggerName.equals(InitParameters.GET_SUGGESTIONS_BUTTON)) {
                doneGetSuggestions(); // execute method for "Get Suggestions" button
            } else if (triggerName.equals(InitParameters.VALIDATE_SINGLE_BUTTON)) {
                doneValidateSingle(); // execute method for "Validate" (single-line) button                
            } else if (triggerName.equals(InitParameters.REMOVE_SINGLE_BUTTON)) {
                doneRemoveSingle(); // execute method for "Remove" (single-line) button                    
            } else if (triggerName.equals(InitParameters.COMMENT_SINGLE)) {
                doneCommentSingle(); // execute method for COMMENT_SINGLE trigger                
            } else if (triggerName.equals(InitParameters.GET_NEXT_SUGG_BUTTON_MULTIPLE)) {
                doneNextSMultiple(); // execute method for "Get Next Suggestion" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.GET_PREV_SUGG_BUTTON_MULTIPLE)) {
                donePrevSMultiple(); // execute method for "Get Previous Suggestion" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.NEXT_ITEM_BUTTON_MULTIPLE)) {
                doneNextIMultiple(); // execute method for "Next Item" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.PREV_ITEM_BUTTON_MULTIPLE)) {
                donePrevIMultiple(); // execute method for "Previous Item" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.VALIDATE_MULTIPLE_BUTTON)) {
                doneValidateMultiple(); // execute method for "Validate" (multiple-line) button                
            } else if (triggerName.equals(InitParameters.REMOVE_MULTIPLE_BUTTON)) {
                doneRemoveMultiple(); // execute method for "Remove" (multiple-line) button
            }
        }
    }
    
    /*
     * Inner class used to acquire progress information from the download
     * or upload bot class (Bot) from a separate thread to avoid freezing up.
     */
    private class Progress extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            setProgress(0);
            // wait for task thread to get going
            try {
                Thread.sleep(1500);
            } catch (Exception e) {};
            // keep updating progress until task is finished
            while (!task.isDone()) {
                setProgress(Bot.getProgress()); // update progress
            }
            setProgress(100); // update progress
            return null;
        }
        
        @Override
        public void done() {} // do nothing
    }

    /**
     * Creates new form Tool
     */
    public Tool() {
        initComponents();
        setTitle(InitParameters.ADMIN_TOOL_TITLE);
        setLocationRelativeTo(null); // display in center screen
        setResizable(false);    
        
        jTextField13.setEditable(false); // "Items Remaining" single-line
        jTextField14.setEditable(false); // "Original English" single-line
        jTextField15.setEditable(false); // "Current Translation" single-line
        jTextArea1.setEditable(false); // "Comment" single-line
        jTextField16.setEditable(false); // "Items Remaining" multiple-line
        jTextArea4.setEditable(false); // "Original English" multiple-line
        jTextArea3.setEditable(false); // "Current Translation" multiple-line
        jTextArea2.setEditable(false); // "Comment" multiple-line
        jTextArea5.setEditable(false); // "Suggestion" multiple-line
        
        disableButtons();
        jButton3.setEnabled(true); // "Get Suggestions" button
        
        // "Done" button
        jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        // "Get Suggestions" button
        jButton3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                language = jComboBox1.getSelectedItem().toString();
                triggerName = InitParameters.GET_SUGGESTIONS_BUTTON;
                
                // initialize progressMonitor
                progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_DL, "", 0, 100);
                progressMonitor.setProgress(0); // initialize progress
         
                // Create new threads
                task = new Task();
                progress = new Progress();
                progress.addPropertyChangeListener(Tool.this);
                
                // Run threads
                task.execute();
                progress.execute();
                
                disableButtons();
            }
        });
        
        // "Validate" button single-line
        jButton1.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               validatedSuggestion = jList2.getSelectedValue().toString();
               triggerName = InitParameters.VALIDATE_SINGLE_BUTTON;
               
               // initialize progressMonitor
               progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_CHANGE, "", 0, 100);
               progressMonitor.setProgress(0); // initialize progress
               
               // Create new threads
               task = new Task();
               progress = new Progress();
               progress.addPropertyChangeListener(Tool.this);
               
               // Run threads
               task.execute();
               progress.execute();
               
               disableButtons();
           } 
        });
        
        // "Validate" button multiple-line
        jButton11.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                triggerName = InitParameters.VALIDATE_MULTIPLE_BUTTON;
                
                // initialize progressMonitor
                progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_CHANGE, "", 0, 100);
                progressMonitor.setProgress(0); // initialize progress
               
                // Create new threads
                task = new Task();
                progress = new Progress();
                progress.addPropertyChangeListener(Tool.this);
               
                // Run threads
                task.execute();
                progress.execute();
               
                disableButtons();
            }
        });
        
        // "Next Item" button single-line
        jButton12.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (counterSingle > 1) {
                    counterSingle = counterSingle - 1; // decrement counter
                    // "Items Remaining" text field
                    jTextField13.setText(counterSingle.toString());
                    // "Original English" text field
                    jTextField14.setText(englishMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
                    // "Current Translation" text field
                    jTextField15.setText(transMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
                    getData_single(); // get the suggestions and comments pages
                    // "Suggestions" list
                    jList2.setModel(new javax.swing.AbstractListModel() {
                        String[] strings = suggestionsSingle;
                        public int getSize() { return strings.length; }
                        public Object getElementAt(int i) { return strings[i]; }
                    });
                    jTextArea1.setText("");
                    jButton1.setEnabled(false); // "Validate" button single-line
                    jButton4.setEnabled(false); // "Remove" button single-line
                }               
            }
        });
        
        // "Next Item" button multiple-line
        jButton14.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (counterMultiple > 1) {
                    counterMultiple = counterMultiple - 1; // decrement counter
                    currentIndex = 0; // reinitialize index
                    getData_multiple(); // get the suggestions and comments pages
                    // "Suggestion" text area (multiple-line)
                    jTextArea5.setText("");
                    // "Comment" text area (multiple-line)
                    jTextArea2.setText("");
                    if (maxCurrentIndex >= 0) {
                        // get the first suggestion for the next key
                        triggerName = InitParameters.NEXT_ITEM_BUTTON_MULTIPLE;
                    
                        // initialize progressMonitor
                        progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_GET, "", 0, 100);
                        progressMonitor.setProgress(0); // initialize progress
               
                        // Create new threads
                        task = new Task();
                        progress = new Progress();
                        progress.addPropertyChangeListener(Tool.this);
               
                        // Run threads
                        task.execute();
                        progress.execute();
               
                        disableButtons();
                    } else {
                        jButton11.setEnabled(false); // "Validate" button multiple-line
                        jButton16.setEnabled(false); // "Remove" button multiple-line
                    }
                    // "Items Remaining" text field (multiple-line)
                    jTextField16.setText(counterMultiple.toString());
                    // "Original English" text area (multiple-line)
                    jTextArea4.setText(englishMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
                    // "Current Translation" text area (multiple-line)
                    jTextArea3.setText(transMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
                }
            }
        });
                
        // "Previous Item" button single-line
        jButton13.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               if (counterSingle < maxCounterSingle) {
                   counterSingle = counterSingle + 1; // increment counter
                   // "Items Remaining" text field
                   jTextField13.setText(counterSingle.toString());
                   // "Original English" text field
                   jTextField14.setText(englishMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
                   // "Current Translation" text field
                   jTextField15.setText(transMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
                   getData_single(); // get the suggestions and comments pages
                   // "Suggestions" list
                   jList2.setModel(new javax.swing.AbstractListModel() {
                       String[] strings = suggestionsSingle;
                       public int getSize() { return strings.length; }
                       public Object getElementAt(int i) { return strings[i]; }
                   });
                   jTextArea1.setText("");
                   jButton1.setEnabled(false); // "Validate" button
                   jButton4.setEnabled(false); // "Remove" button
               }        
           } 
        });
        
        // "Previous Item" button multiple-line
        jButton15.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (counterMultiple < maxCounterMultiple) {
                   counterMultiple = counterMultiple + 1; // increment counter
                   currentIndex = 0; // reinitialize index
                   getData_multiple(); // get the suggestions and comments pages
                   // "Suggestion" text area (multiple-line)
                   jTextArea5.setText("");
                   // "Comment" text area (multiple-line)
                   jTextArea2.setText("");
                   if (maxCurrentIndex >= 0) {
                        // get the first suggestion for the previous key
                        triggerName = InitParameters.PREV_ITEM_BUTTON_MULTIPLE;
                    
                        // initialize progressMonitor
                        progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_GET, "", 0, 100);
                        progressMonitor.setProgress(0); // initialize progress
               
                        // Create new threads
                        task = new Task();
                        progress = new Progress();
                        progress.addPropertyChangeListener(Tool.this);
               
                        // Run threads
                        task.execute();
                        progress.execute();
               
                        disableButtons();   
                   } else {
                       jButton11.setEnabled(false); // "Validate" button multiple-line
                       jButton16.setEnabled(false); // "Remove" button multiple-line
                   }
                   // "Items Remaining" text field (multiple-line)
                   jTextField16.setText(counterMultiple.toString());
                   // "Original English" text area (multiple-line)
                   jTextArea4.setText(englishMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
                   // "Current Translation" text area (multiple-line)
                   jTextArea3.setText(transMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
               }        
            }
        });
        
        // "Get Next Suggestion" button multiple-line
        jButton7.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (currentIndex < maxCurrentIndex) {
                    // "Suggestion" text area (multiple-line)
                    jTextArea5.setText("");
                    // "Comment" text area (multiple-line)
                    jTextArea2.setText("");
                    currentIndex = currentIndex + 1; // increment current index
                    // get the next suggestion
                    triggerName = InitParameters.GET_NEXT_SUGG_BUTTON_MULTIPLE;
                    
                    // initialize progressMonitor
                    progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_GET, "", 0, 100);
                    progressMonitor.setProgress(0); // initialize progress
               
                    // Create new threads
                    task = new Task();
                    progress = new Progress();
                    progress.addPropertyChangeListener(Tool.this);
               
                    // Run threads
                    task.execute();
                    progress.execute();
               
                    disableButtons();
                }
            }
        });
        
        // "Get Previous Suggestion" button multiple-line
        jButton8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (currentIndex > 0) {
                    // "Suggestion" text area (multiple-line)
                    jTextArea5.setText("");
                    // "Comment" text area (multiple-line)
                    jTextArea2.setText("");
                    currentIndex = currentIndex - 1; // decrement current index
                    // get the previous suggestion
                    triggerName = InitParameters.GET_PREV_SUGG_BUTTON_MULTIPLE;
                    
                    // initialize progressMonitor
                    progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_GET, "", 0, 100);
                    progressMonitor.setProgress(0); // initialize progress
               
                    // Create new threads
                    task = new Task();
                    progress = new Progress();
                    progress.addPropertyChangeListener(Tool.this);
               
                    // Run threads
                    task.execute();
                    progress.execute();
               
                    disableButtons();
                }
            }
        });
        
        // "Remove" button single-line
        jButton4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               badSuggestion = jList2.getSelectedValue().toString();
               triggerName = InitParameters.REMOVE_SINGLE_BUTTON;
               
               // initialize progressMonitor
               progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_DELETE, "", 0, 100);
               progressMonitor.setProgress(0); // initialize progress
               
               // Create new threads
               task = new Task();
               progress = new Progress();
               progress.addPropertyChangeListener(Tool.this);
               
               // Run threads
               task.execute();
               progress.execute();
               
               disableButtons();
            }
        });
        
        // "Remove" button multiple-line
        jButton16.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                triggerName = InitParameters.REMOVE_MULTIPLE_BUTTON;
                              
                // initialize progressMonitor
                progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_DELETE, "", 0, 100);
                progressMonitor.setProgress(0); // initialize progress
               
                // Create new threads
                task = new Task();
                progress = new Progress();
                progress.addPropertyChangeListener(Tool.this);
               
                // Run threads
                task.execute();
                progress.execute();
               
                disableButtons();
            }
        });
                
        // "Suggestions" jList single-line
        jList2.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    jTextArea1.setText("");
                    try {
                        selectedIndex = jList2.getSelectedIndex();
                        String selected = (String) jList2.getSelectedValue();
                        if (!commentsPagesSingle[selectedIndex].equals(InitParameters.NO_COMMENT)) {
                            // get the comment
                            triggerName = InitParameters.COMMENT_SINGLE;
                
                            // initialize progressMonitor
                            progressMonitor = new ProgressMonitor(Tool.this, InitParameters.P_BAR_MESSAGE_GET_COMMENT, "", 0, 100);
                            progressMonitor.setProgress(0); // initialize progress
                
                            // Create new threads
                            task = new Task();
                            progress = new Progress();
                            progress.addPropertyChangeListener(Tool.this);
                
                            // Run threads
                            task.execute();
                            progress.execute();
                
                            disableButtons();
                        }
                        if (!selected.isEmpty()) {
                            jButton1.setEnabled(true); // "Validate" button
                            jButton4.setEnabled(true); // "Remove" button
                        }
                    } catch (NullPointerException exc) {
                        jButton1.setEnabled(false); // "Validate" button
                        jButton4.setEnabled(false); // "Remove" button                    
                    }
                } else {
                    jButton1.setEnabled(false); // "Validate" button
                    jButton4.setEnabled(false); // "Remove" button   
                }
            }
        });
    }
    
    /*
     * Method to implement PropertyChangeListener
     */
    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            int prog = (Integer) evt.getNewValue(); // get updated progress
            progressMonitor.setProgress(prog); // update progress
            String info = "Completed " + prog + "%"; // information for the user
            progressMonitor.setNote(info); // update the message
            // check if canceled
            if (progressMonitor.isCanceled()) {
                // cancel threads
                progress.cancel(true);
                task.cancel(true);
                // enable buttons
                if (triggerName.equals(InitParameters.GET_SUGGESTIONS_BUTTON)) {
                    jButton3.setEnabled(true); // "Get Suggestions" button
                    jButton2.setEnabled(true); // "Done" button
                } else {
                    enableButtons();
                }
                Bot.resetProgress();
            }
            // check if threads done
            if (progress.isDone()) {
                Bot.resetProgress();
            }
        }
    }
    
    // ********** Get Data Methods **********
    
    /*
     * Sets single-line suggestions and comments pages for the given key.
     */
    private void getData_single() {
        String[] split = codeSingle.split(InitParameters.START_DELIMITER + keysSingle.get(counterSingle) + " =");
        String[] lines = split[1].split("\n");
        suggestionsSingle = new String[100];
        commentsPagesSingle = new String[100];
        // iterate through lines
        for (int j=1; j<lines.length; j++) {
            if (!lines[j].contains(InitParameters.END_DELIMITER) && j<=100) {
                String[] split2 = lines[j].split(InitParameters.SEPARATOR);
                suggestionsSingle[j-1] = split2[0];
                commentsPagesSingle[j-1] = split2[1]; // same index as in suggestions      
            } else {
                break;
            }
        }
    }
    
    /*
     * Sets multiple-line suggestions and comments pages for the given key.
     */
    private void getData_multiple() {   
        maxCurrentIndex = -1; // initialize maximum index
        String[] split = codeMultiple.split(InitParameters.START_DELIMITER + keysMultiple.get(counterMultiple) + " =");
        String[] lines = split[1].split("\n");
        suggestionsMultiple = new String[100];
        commentsPagesMultiple = new String[100];
        currentSuggestionMultiple = ""; // initialize current suggestion
        currentCommentMultiple = ""; // initialize current comment
        // iterate through lines
        for (int j=1; j<lines.length; j++) {
            if (!lines[j].contains(InitParameters.END_DELIMITER) && j<=100) {
                String[] split2 = lines[j].split(InitParameters.SEPARATOR);
                maxCurrentIndex = maxCurrentIndex + 1; // increment maximum index
                suggestionsMultiple[j-1] = split2[0];
                commentsPagesMultiple[j-1] = split2[1]; // same index as in suggestions      
            } else {
                break;
            }
        }
    }
    
    // ********** Background Task Thread Methods **********
    
    /*
     * Background Task thread method for "Get Suggestions" button.
     * -Gets translations and suggestions from the Wiki.
     */
    private void backgroundGetSuggestions() {
        englishMap_single = EnglishMapper.initEnglishMap_single(); // initialize English DoubleMap (single-line)
        englishMap_multiple = EnglishMapper.initEnglishMap_multiple(); // initialize English DoubleMap (multiple-line)
        codeSingle = Bot.getSuggestionsCode_single(language);
        codeMultiple = Bot.getSuggestionsCode_multiple(language);
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            transMap_single = Bot.getTranslations_single(language);
            cont = Bot.getConnectionStatus(); // see if it worked
            if (cont) {
                transMap_multiple = Bot.getTranslations_multiple(language);
                cont = Bot.getConnectionStatus(); // see if it worked
                if (cont) {
                    maxCounterSingle = englishMap_single.KeytoValue.size();
                    maxCounterMultiple = englishMap_multiple.KeytoValue.size();
                    counterSingle = maxCounterSingle; // initialize single-line counter
                    counterMultiple = maxCounterMultiple; // initialize multiple-line counter
                    currentIndex = 0; // initialize multiple-line index
                    // "Items Remaining" text field (single-line)
                    jTextField13.setText(counterSingle.toString());
                    // "Items Remaining" text field (multiple-line)
                    jTextField16.setText(counterMultiple.toString());
                    keysSingle = new HashMap<Integer,String>(); // create the keysSingle HashMap
                    keysMultiple = new HashMap<Integer,String>(); // create the keysMultiple HashMap
                    int i = counterSingle;
                    // initialize keysSingle HashMap
                    for (String key : englishMap_single.KeytoValue.keySet()) {
                        keysSingle.put(i, key);
                        i = i - 1;
                    }
                    i = counterMultiple;
                    // initialize keysMultiple HashMap
                    for (String key : englishMap_multiple.KeytoValue.keySet()) {
                        keysMultiple.put(i, key);
                        i = i - 1;
                    }
                    getData_single(); // get the suggestions and comments pages (single-line)
                    getData_multiple(); // get a suggestion and comment page (multiple-line)     
                    // get first suggestion if it exists
                    if (maxCurrentIndex >= 0) {
                        currentSuggestionMultiple = Bot.getPageCode(suggestionsMultiple[0]);
                        cont = Bot.getConnectionStatus(); // see if it worked
                        if (cont) {
                            if (!commentsPagesMultiple[currentIndex].equals(InitParameters.NO_COMMENT)) {
                                currentCommentMultiple = Bot.getPageCode(commentsPagesMultiple[0]);
                            } else {
                                currentCommentMultiple = "";
                            }
                        }
                    }
                }
            }
        }
    }
    
    /*
     * Background Task thread method for "Validate" (single-line) button.
     * -Validates a single-line suggestion and removes it from the suggestions.
     */
    private void backgroundValidateSingle() {
        Bot.validateTranslation_single(language, keysSingle.get(counterSingle), validatedSuggestion);
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            Bot.removeSuggestion_single(language, codeSingle, keysSingle.get(counterSingle), validatedSuggestion + InitParameters.SEPARATOR + commentsPagesSingle[selectedIndex]); // remove the validated suggestion 
            cont = Bot.getConnectionStatus(); // see if it worked
            if (cont) {
                // delete comment page if it exists
                if (!commentsPagesSingle[selectedIndex].equals(InitParameters.NO_COMMENT)) {
                    Bot.removePage(commentsPagesSingle[selectedIndex]);
                }
            }
        }
    }
    
    /*
     * Background Task thread method for "Remove" (single-line) button.
     * -Removes a single-line suggestion along with its comment (if it has one).
     */
    private void backgroundRemoveSingle() {
        Bot.removeSuggestion_single(language, codeSingle, keysSingle.get(counterSingle), badSuggestion + InitParameters.SEPARATOR + commentsPagesSingle[selectedIndex]); // remove selected suggestion
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            // delete comment page if it exists
            if (!commentsPagesSingle[selectedIndex].equals(InitParameters.NO_COMMENT)) {
                Bot.removePage(commentsPagesSingle[selectedIndex]);
            }
        }
    }
    
    /*
     * Background Task thread method for COMMENT_SINGLE trigger. 
     * -Gets a single-line suggestion comment.
     */
    private void backgroundCommentSingle() {
        comment = Bot.getPageCode(commentsPagesSingle[selectedIndex]);
        boolean cont = Bot.getConnectionStatus();
        if (cont) {
            jTextArea1.setText(comment);
        }
    }
    
    /*
     * Background Task thread method for getting a multiple-line suggestion.
     * -Gets a multiple-line 
     */
    private void backgroundSuggestionMultiple() {
        currentSuggestionMultiple = Bot.getPageCode(suggestionsMultiple[currentIndex]);
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            currentCommentMultiple = Bot.getPageCode(commentsPagesMultiple[currentIndex]);
        }
    }
    
    /*
     * Background Task thread method for "Validate" (multiple-line) button.
     * -Validates a multiple-line suggestion and removes it from the suggestions.
     */
    private void backgroundValidateMultiple() {
        Bot.validateTranslation_multiple(language, keysMultiple.get(counterMultiple), currentSuggestionMultiple); // validate the suggestion
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            Bot.removeSuggestion_multiple(language, codeMultiple, keysMultiple.get(counterMultiple), suggestionsMultiple[currentIndex] + InitParameters.SEPARATOR + commentsPagesMultiple[currentIndex]); // remove current suggestion
            cont = Bot.getConnectionStatus(); // see if it worked
            if (cont) {
                Bot.removePage(suggestionsMultiple[currentIndex]); // delete the suggestion
                cont = Bot.getConnectionStatus(); // see if it worked
                if (cont) {
                    // delete comment page if it exists
                    if (!commentsPagesMultiple[currentIndex].equals(InitParameters.NO_COMMENT)) {
                        Bot.removePage(commentsPagesMultiple[currentIndex]); // delete the comment
                        cont = Bot.getConnectionStatus(); // see if it worked
                        if (cont) {
                            // Make the change in memory
                            String oldValue = transMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple));
                            transMap_multiple.KeytoValue.remove(keysMultiple.get(counterMultiple));
                            transMap_multiple.KeytoValue.put(keysMultiple.get(counterMultiple), currentSuggestionMultiple);
                            transMap_multiple.ValuetoKey.remove(oldValue);
                            transMap_multiple.ValuetoKey.put(currentSuggestionMultiple, keysMultiple.get(counterMultiple));
                            codeMultiple = Bot.getSuggestionsCode_multiple(language); // get updated wiki code
                            cont = Bot.getConnectionStatus();
                            if (cont) {
                                currentIndex = 0; // reinitialize index
                                getData_multiple(); // get the suggestions and comments pages
                                    
                                // get first suggestion if it exists
                                if (maxCurrentIndex >= 0) {
                                    currentSuggestionMultiple = Bot.getPageCode(suggestionsMultiple[0]);
                                    cont = Bot.getConnectionStatus(); // see if it worked
                                    if (cont) {
                                        if (!commentsPagesMultiple[currentIndex].equals(InitParameters.NO_COMMENT)) {
                                            currentCommentMultiple = Bot.getPageCode(commentsPagesMultiple[0]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /*
     * Background Task thread method for "Remove" (multiple-line) button.
     * -Removes a multiple-line suggestion along with its comment (if it has one).
     */
    private void backgroundRemoveMultiple() {
        Bot.removeSuggestion_multiple(language, codeMultiple, keysMultiple.get(counterMultiple), suggestionsMultiple[currentIndex] + InitParameters.SEPARATOR + commentsPagesMultiple[currentIndex]); // remove current suggestion
        boolean cont = Bot.getConnectionStatus(); // see if it worked
        if (cont) {
            Bot.removePage(suggestionsMultiple[currentIndex]); // delete the suggestion
            cont = Bot.getConnectionStatus(); // see if it worked
            if (cont) {
                // delete comment page if it exists
                if (!commentsPagesMultiple[currentIndex].equals(InitParameters.NO_COMMENT)) {
                    Bot.removePage(commentsPagesMultiple[currentIndex]); // delete the comment
                    cont = Bot.getConnectionStatus(); // see if it worked
                    if (cont) {
                        codeMultiple = Bot.getSuggestionsCode_multiple(language); // get updated wiki code
                        cont = Bot.getConnectionStatus();
                        if (cont) {
                            currentIndex = 0; // reinitialize index
                            getData_multiple(); // get the suggestions and comments pages
                                    
                            // get first suggestion if it exists
                            if (maxCurrentIndex >= 0) {
                                currentSuggestionMultiple = Bot.getPageCode(suggestionsMultiple[0]);
                                cont = Bot.getConnectionStatus(); // see if it worked
                                if (cont) {
                                    if (!commentsPagesMultiple[currentIndex].equals(InitParameters.NO_COMMENT)) {
                                        currentCommentMultiple = Bot.getPageCode(commentsPagesMultiple[0]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // ********** Done Task Thread Methods **********
    
    /*
     * Done Task thread method for "Get Suggestions" button.
     * -Sets up GUI with data retrieved from the Wiki database.
     */
    private void doneGetSuggestions() {
        Bot.resetProgress();
        jButton2.setEnabled(true); // "Done" button
        jButton3.setEnabled(true); //"Get Suggestions" button
        jTextArea1.setText("");
        boolean connected = Bot.getConnectionStatus();
        if (connected) {        
            // "Original English" text field (single-line)
            jTextField14.setText(englishMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
            // "Current Translation" text field (single-line)
            jTextField15.setText(transMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
            // "Original English" text area (multiple-line)
            jTextArea4.setText(englishMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple))); 
            // "Current Translation" text area (multiple-line)
            jTextArea3.setText(transMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
            // "Suggestion" text area (multiple-line)
            jTextArea5.setText(currentSuggestionMultiple);
            // "Comment" text area (multiple-line)
            jTextArea2.setText(currentCommentMultiple); 
            try {
                // "Suggestions" list
                jList2.setModel(new javax.swing.AbstractListModel() {
                    String[] strings = suggestionsSingle;
                    public int getSize() { return strings.length; }
                    public Object getElementAt(int i) { return strings[i]; }
                });
            } catch (java.lang.NullPointerException exc) {} // do nothing
            // enable buttons
            if (maxCurrentIndex > 0) {
                jButton7.setEnabled(true); // "Get Next Suggestion" button multiple-line
            }                    
            enableButtons();
        }
    }
    
    /*
     * Done Task thread method for "Validate" (single-line) button.
     * -Resets GUI.
     */
    private void doneValidateSingle() {
        enableButtons();
        jTextArea1.setText("");
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            codeSingle = Bot.getWikiCode_single(); // update wiki code
            getData_single(); // get the suggestions and comments pages
            // Make the change in memory
            String oldValue = transMap_single.KeytoValue.get(keysSingle.get(counterSingle));
            transMap_single.KeytoValue.remove(keysSingle.get(counterSingle));
            transMap_single.KeytoValue.put(keysSingle.get(counterSingle), validatedSuggestion);
            transMap_single.ValuetoKey.remove(oldValue);
            transMap_single.ValuetoKey.put(validatedSuggestion, keysSingle.get(counterSingle));
            // "Current Translation" text field
            jTextField15.setText(transMap_single.KeytoValue.get(keysSingle.get(counterSingle)));
            // "Suggestions" list
            jList2.setModel(new javax.swing.AbstractListModel() {
                String[] strings = suggestionsSingle;
                public int getSize() { return strings.length; }
                public Object getElementAt(int i) { return strings[i]; }
            });
        }    
    }
    
    /*
     * Done Task thread method for "Remove" (single-line) button.
     * -Resets GUI.
     */
    private void doneRemoveSingle() {
        enableButtons();
        jTextArea1.setText("");
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            codeSingle = Bot.getWikiCode_single(); // update wiki code
            getData_single(); // get the suggestions and comments pages
            // "Suggestions" list
            jList2.setModel(new javax.swing.AbstractListModel() {
                String[] strings = suggestionsSingle;
                public int getSize() { return strings.length; }
                public Object getElementAt(int i) { return strings[i]; }
            });
        }  
    }
    
    /*
     * Done Task thread method for COMMENT_SINGLE trigger.
     * -Resets GUI.
     */
    private void doneCommentSingle() {
        enableButtons();
    }
    
    /*
     * Done Task thread method for "Validate" (multiple-line) button.
     * -Resets GUI.
     */
    private void doneValidateMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) { 
            if (maxCurrentIndex >= 0) {
                jButton11.setEnabled(true); // "Validate" button multiple-line
                jButton16.setEnabled(true); // "Remove" button multiple-line
                // "Suggestion" text area (multiple-line)
                jTextArea5.setText(currentSuggestionMultiple); 
                // "Comment" text area (multiple-line)
                jTextArea2.setText(currentCommentMultiple);   
            } else {
                // "Suggestion" text area (multiple-line)
                jTextArea5.setText("");
                // "Comment" text area (multiple-line)
                jTextArea2.setText("");  
            }
            // "Current Translation" text area (multiple-line)
            jTextArea3.setText(transMap_multiple.KeytoValue.get(keysMultiple.get(counterMultiple)));
        }
    }
    
    /*
     * Done Task thread method for "Remove" (multiple-line) button.
     * -Resets GUI.
     */
    private void doneRemoveMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            if (maxCurrentIndex >= 0) {
                jButton11.setEnabled(true); // "Validate" button multiple-line
                jButton16.setEnabled(true); // "Remove" button multiple-line
                // "Suggestion" text area (multiple-line)
                jTextArea5.setText(currentSuggestionMultiple);
                // "Comment" text area (multiple-line)
                jTextArea2.setText(currentCommentMultiple);
            } else {
                // "Suggestion" text area (multiple-line)
                jTextArea5.setText("");
                // "Comment" text area (multiple-line)
                jTextArea2.setText("");  
            }
        }
    }
    
    /*
     * Done Task thread method for "Previous Item" (multiple-line) button
     * -Resets GUI.
     */
    private void donePrevIMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            // "Suggestion" text area (multiple-line)
            jTextArea5.setText(currentSuggestionMultiple);
            // "Comment" text area (multiple-line)
            jTextArea2.setText(currentCommentMultiple);
            jButton11.setEnabled(true); // "Validate" button multiple-line
            jButton16.setEnabled(true); // "Remove" button multiple-line
        }
    }
    
    /*
     * Done Task thread method for "Next Item" (multiple-line) button.
     * -Resets GUI.
     */
    private void doneNextIMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            // "Suggestion" text area (multiple-line)
            jTextArea5.setText(currentSuggestionMultiple);
            // "Comment" text area (multiple-line)
            jTextArea2.setText(currentCommentMultiple);
            jButton11.setEnabled(true); // "Validate" button multiple-line
            jButton16.setEnabled(true); // "Remove" button multiple-line
        }
    }
    
    /*
     * Done Task thread method for "Get Previous Suggestion" (multiple-line)
     * button.
     * -Resets GUI.
     */
    private void donePrevSMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            // "Suggestion" text area (multiple-line)
            jTextArea5.setText(currentSuggestionMultiple);
            // "Comment" text area (multiple-line)
            jTextArea2.setText(currentCommentMultiple);
            jButton11.setEnabled(true); // "Validate" button multiple-line
            jButton16.setEnabled(true); // "Remove" button multiple-line
        }
    }
    
    /*
     * Done Task thread method for "Get Next Suggestion" (multiple-line)
     * button.
     * -Resets GUI.
     */
    private void doneNextSMultiple() {
        enableButtons();
        boolean connected = Bot.getConnectionStatus();
        if (connected) {
            // "Suggestion" text area (multiple-line)
            jTextArea5.setText(currentSuggestionMultiple);
            // "Comment" text area (multiple-line)
            jTextArea2.setText(currentCommentMultiple);
            jButton11.setEnabled(true); // "Validate" button multiple-line
            jButton16.setEnabled(true); // "Remove" button multiple-line
        }
    }
    
    /*
     * Method to enable all buttons except "Validate" and "Remove" buttons
     */
    private void enableButtons() {
        jButton3.setEnabled(true); // "Get Suggestions" button
        jButton12.setEnabled(true); // "Next Item" button single-line
        jButton13.setEnabled(true); // "Previous Item" button single-line
        jButton14.setEnabled(true); // "Next Item" button multiple-line
        jButton15.setEnabled(true); // "Previous Item" button multiple-line
        jButton7.setEnabled(true); // "Get Next Suggestion" button multiple-line
        jButton8.setEnabled(true); // "Get Previous Suggestion" button multiple-line
    }
    
    /*
     * Method to disable all buttons
     */
    private void disableButtons() {
        jButton3.setEnabled(false); // "Get Suggestions" button
        jButton12.setEnabled(false); // "Next Item" button single-line
        jButton13.setEnabled(false); // "Previous Item" button single-line
        jButton14.setEnabled(false); // "Next Item" button multiple-line
        jButton15.setEnabled(false); // "Previous Item" button multiple-line
        jButton7.setEnabled(false); // "Get Next Suggestion" button multiple-line
        jButton8.setEnabled(false); // "Get Previous Suggestion" button multiple-line
        jButton1.setEnabled(false); // "Validate" button single-line
        jButton4.setEnabled(false); // "Remove" button single-line
        jButton11.setEnabled(false); // "Validate" button multiple-line
        jButton16.setEnabled(false); // "Remove" button multiple-line
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jTextField15 = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jButton4 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jSeparator15 = new javax.swing.JSeparator();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        jLabel19 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jSeparator16 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea5 = new javax.swing.JTextArea();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jButton8 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton16 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Please choose a language:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Deutsch", "Français", "Español", "русский", "日本語", "中文" }));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("Items Remaining:");

        jLabel14.setText("Original English:");

        jLabel15.setText("Current Translation:");

        jButton12.setText("Next Item");

        jButton13.setText("Previous Item");

        jLabel5.setText("Please validate a suggestion:");

        jButton1.setText("Validate");

        jScrollPane1.setViewportView(jList2);

        jButton4.setText("Remove");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Suggestions");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Comment");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(236, 236, 236)
                .addComponent(jButton13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton12)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator11)
                    .addComponent(jSeparator10)
                    .addComponent(jSeparator9, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField14)
                            .addComponent(jTextField15)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(162, 162, 162)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jButton4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton1)))
                                .addGap(4, 4, 4))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(95, 95, 95))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton12, jButton13});

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton1, jButton4});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton4))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton13)
                    .addComponent(jButton12))
                .addContainerGap(271, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jScrollPane1, jScrollPane2});

        jTabbedPane3.addTab("Single-Line Translations", jPanel5);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Items Remaining:");

        jLabel18.setText("Original English:");

        jTextArea4.setColumns(20);
        jTextArea4.setRows(5);
        jScrollPane3.setViewportView(jTextArea4);

        jLabel19.setText("Current Translation:");

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jScrollPane8.setViewportView(jTextArea3);

        jLabel11.setText("Please validate a suggestion:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("Suggestion");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Comment");

        jTextArea5.setColumns(20);
        jTextArea5.setRows(5);
        jScrollPane9.setViewportView(jTextArea5);

        jTextArea2.setColumns(20);
        jTextArea2.setRows(5);
        jScrollPane10.setViewportView(jTextArea2);

        jButton8.setText("Get Previous Suggestion");

        jButton7.setText("Get Next Suggestion");

        jButton11.setText("Validate");

        jButton16.setText("Remove");

        jButton14.setText("Next Item");

        jButton15.setText("Previous Item");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator15)
                    .addComponent(jScrollPane3)
                    .addComponent(jSeparator16)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel18)
                            .addComponent(jLabel19)
                            .addComponent(jLabel11))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel13)
                .addGap(94, 94, 94))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton11)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(239, 239, 239)
                .addComponent(jButton15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton14)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton14, jButton15});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButton7, jButton8});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator15, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator16, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton8)
                    .addComponent(jButton7)
                    .addComponent(jButton11)
                    .addComponent(jButton16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton14)
                    .addComponent(jButton15))
                .addContainerGap(57, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jScrollPane3, jScrollPane8});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane3.addTab("Multiple-Line Translations", jPanel1);

        jButton2.setText("Done");

        jButton3.setText("Get Suggestions");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2))
                    .addComponent(jTabbedPane3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList jList2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea5;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    // End of variables declaration//GEN-END:variables
}
