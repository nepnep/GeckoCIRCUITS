/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * This class defines all constant parameters for the Admin Tool.
 * -Note: Changing values here without changing the same respective values in 
 * the User Application (class: i18n.InitParameters) will cause the i18n
 * toolkit to malfunction.
 */
package admintool;

public class InitParameters {
    
    private InitParameters(){} // prevents instantiation
    
    // **********   To Be Adjusted:    **********
    
    /**
     * Set this to "true" if you want database connection DEBUG messages to appear in the console
     */
    public static final boolean DEBUG_MODE = false;
    /**
     * URL of the Database Wiki
     */
    public static final String WIKI_URL = "http://www.translation.gecko-research.org/wiki/";
    /**
     * Wiki page from which to get single-line translations
     */
    public static final String SINGLE_PAGE = "Presentation";
    /**
     * Wiki page from which to get multiple-line translations
     */
    public static final String MULTIPLE_PAGE = "PresentationMul";
    /**
     * Wiki page for single-line suggestions
     */
    public static final String SINGLE_SUGGESTIONS_PAGE = "GeckoSuggestions";
    /**
     * Wiki page for multiple-line suggestions
     */
    public static final String MULTIPLE_SUGGESTIONS_PAGE = "GeckoSuggestionsMul";
    
    // supported languages with their respective language codes
    
    /**
     * German language name
     */
    public static final String GERMAN = "Deutsch";
    private static final String GERMAN_CODE = "de"; // German language code
    /**
     * French language name
     */
    public static final String FRENCH = "Français";
    private static final String FRENCH_CODE = "fr"; // French language code
    /**
     * Spanish language name
     */
    public static final String SPANISH = "Español";
    private static final String SPANISH_CODE = "es"; // Spanish language code
    /**
     * Russian language name
     */
    public static final String RUSSIAN = "русский";
    private static final String RUSSIAN_CODE = "ru"; // Russian language code
    /**
     * Japanese language name
     */
    public static final String JAPANESE = "日本語";
    private static final String JAPANESE_CODE = "ja"; // Japanese language code
    /**
     * Chinese language name
     */
    public static final String CHINESE = "中文";
    private static final String CHINESE_CODE = "zh"; // Chinese language code

    /**
     * Method to get language code (if the language != English)
     * @param language Language for which to get language code
     * @return Language code of the given language
     */
    public static final String getLanguageCode(String language) {
        if (language.equals(GERMAN)) {
            return GERMAN_CODE;
        } else if (language.equals(FRENCH)) {
            return FRENCH_CODE;
        } else if (language.equals(JAPANESE)) {
            return JAPANESE_CODE;
        } else if (language.equals(CHINESE)) {
            return CHINESE_CODE;
        } else if (language.equals(SPANISH)) {
            return SPANISH_CODE;
        } else if (language.equals(RUSSIAN)) {
            return RUSSIAN_CODE;
        }
        return "";
    }
    
    // **********   No Need To Change:    **********
    
    /**
     * Used to delimit suggestion lists
     */
    public static final String START_DELIMITER = "-START-";
    /**
     * Used to delimit suggestion lists
     */
    public static final String END_DELIMITER = "-END-";
    /**
     * Used to separate suggestions from comments pages 
     * IMPORTANT: This has to be a regex expression!)
     */
    public static final String SEPARATOR = "#::#:::##:";
    /**
     * Information message while downloading
     */
    public static final String P_BAR_MESSAGE_DL = "Downloading Data..";
    /**
     * Information message while logging in
     */
    public static final String P_BAR_MESSAGE_LOGIN = "Logging In..";
    /**
     * Information message while changing translations
     */
    public static final String P_BAR_MESSAGE_CHANGE = "Making the Change..";
    /**
     * Information message while getting suggestion (multiple-line)
     */
    public static final String P_BAR_MESSAGE_GET = "Getting Suggestion..";
    /**
     * Information message while deleting suggestion
     */
    public static final String P_BAR_MESSAGE_DELETE = "Deleting Suggestion..";
    /**
     * Information message while getting comment
     */
    public static final String P_BAR_MESSAGE_GET_COMMENT = "Getting Comment..";
    /**
     * Title of login GUI
     */
    public static final String LOGIN = "Login";
    /**
     * Title of admin tool GUI
     */
    public static final String ADMIN_TOOL_TITLE = "Admin Tool";
    /**
     * Connection error message 1
     */
    public static final String DATABASE_ERROR_MESSAGEa = "Failed to communicate with database!!";
    /**
     * Connection error message 3
     */
    public static final String DATABASE_ERROR_MESSAGEc = "   Please re-establish connection and try again.";
    /**
     * used to indicate that a suggestion has no comment
     */
    public static final String NO_COMMENT = "NO_COMMENT";
    /**
     * Get Suggestions button identifier
     */
    public static final String GET_SUGGESTIONS_BUTTON = "GS";
    /**
     * Validate (single-line) button identifier
     */
    public static final String VALIDATE_SINGLE_BUTTON = "V_single";
    /**
     * Validate (multiple-line) button identifier
     */
    public static final String VALIDATE_MULTIPLE_BUTTON = "V_multiple"; 
    /**
     * Remove (single-line) button identifier
     */
    public static final String REMOVE_SINGLE_BUTTON = "R_single";
    /**
     * Remove (multiple-line) button identifier
     */
    public static final String REMOVE_MULTIPLE_BUTTON = "R_multiple";
    /**
     * Trigger name for getting single-line comments
     */
    public static final String COMMENT_SINGLE = "C_single";
    /**
     * Get Next Suggestion (multiple-line) button identifier
     */
    public static final String GET_NEXT_SUGG_BUTTON_MULTIPLE = "GNS_multiple";
    /**
     * Get Previous Suggestion (multiple-line) button identifier
     */
    public static final String GET_PREV_SUGG_BUTTON_MULTIPLE = "GPS_multiple";
    /**
     * Next Item (multiple-line) button identifier
     */
    public static final String NEXT_ITEM_BUTTON_MULTIPLE = "NI_multiple";
    /**
     * Previous Item (multiple-lines) button identifier
     */
    public static final String PREV_ITEM_BUTTON_MULTIPLE = "PI_multiple";
}

