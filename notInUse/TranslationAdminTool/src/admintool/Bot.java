/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Research GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * -Download bot class.  This bot is used to download current translations
 * off of the Wiki database.  It has its own dedicated login on the Wiki:
 * USER = "DLbot", PASSWORD = "download"
 */
package admintool;

import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;
import net.sourceforge.jwbf.core.contentRep.SimpleArticle;

public class Bot {
    
    private static String wikiCodeSingle;
    private static boolean connected = false; // Wiki connection status indicator
    private static int progress = 0; // download progress (percent)
    
    /**
     * Creates a new bot with DLbot credentials, logs in and returns it
     * - Throws an exception if communication with the wiki failed
     */
    private static MediaWikiBot initBot() throws Exception {
        // print DEBUG messages in console if DEBUG_MODE is turned on
        if (InitParameters.DEBUG_MODE) {
            org.apache.log4j.BasicConfigurator.configure(); // configure log4j
        }
        progress = Math.min(progress + 2, 99); // update progress
        MediaWikiBot b = new MediaWikiBot(InitParameters.WIKI_URL);
        progress = Math.min(progress + 2, 99); // update progress
        b.login(UserBot.user, UserBot.pword);
        progress = Math.min(progress + 42, 99); // update progress
        return b;        
    }
    
    /**
     * Downloads all current single-line translations for the chosen language 
     * off of the Wiki database
     * @param language Language in which to get the translations
     * @return DoubleMap containing all single-line translations for the language
     */
    public static DoubleMap getTranslations_single(String language) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 6, 99); // update progress
            SimpleArticle sa = b.readData(InitParameters.SINGLE_PAGE + "/" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 12, 99); // update progress
            String wikiCode = sa.getText();
            progress = Math.min(progress + 6, 99); // update progress
            DoubleMap dm = getDoubleMap(wikiCode);     
            progress = Math.min(progress + 34, 99); // update progress
            connected = true;
            return dm;
        } catch(Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
            return null;
        }
    }
    
    /**
     * Downloads all current multiple-line translations for the chosen language
     * off of the Wiki database
     * @param language Language in which to get the translations
     * @return DoubleMap containing all multiple-line translations for the language
     */
    public static DoubleMap getTranslations_multiple(String language) {
        try {
            MediaWikiBot b = initBot();
            DoubleMap dm = new DoubleMap();
            SimpleArticle sa;
            
            // Get all multiple-line translations
            for (String key : Tool.englishMap_multiple.KeytoValue.keySet()) {
                sa = b.readData("Translations:" + InitParameters.MULTIPLE_PAGE + "/" + key + "/" + InitParameters.getLanguageCode(language));
                progress = Math.min(progress + 5, 99); // update progress
                String value = sa.getText();
                dm.putPair(key, value);               
            }
            connected = true;
            return dm;
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
            return null;
        }
    }
    
    /**
     * Removes the given (toRemove) single-line suggestion along with its
     * comment from the suggestions page dedicated to "language"
     * @param language Language from which to remove the suggestion
     * @param code Wiki code of the suggestions page for the language
     * @param key Key of the suggestion to remove
     * @param toRemove String to remove from suggestions page
     */
    public static void removeSuggestion_single(String language, String code, String key, String toRemove) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 10, 99); // update progress
            SimpleArticle sa = new SimpleArticle();
            progress = Math.min(progress + 2, 99); // update progress
            sa.setTitle(InitParameters.SINGLE_SUGGESTIONS_PAGE + "_" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 1, 99); // update progress
            String[] split = code.split(InitParameters.START_DELIMITER + key + " =");
            String[] lines = split[1].split("\n");
            String newSuggestions = "";
            String oldSuggestions = "";
            String[] suggestions = new String[100];
            progress = Math.min(progress + 8, 99); // update progress
            // iterate through lines
            for (int j=1; j<lines.length; j++) {
                if(!lines[j].contains(InitParameters.END_DELIMITER) && j<=100) {
                    oldSuggestions += (lines[j] + "\n");
                    if (!lines[j].equals(toRemove)) {
                       newSuggestions += (lines[j] + "\n");// updated suggestions
                       suggestions[j-1] = lines[j];
                    }                        
                } else {
                    break;
                }
            }
            progress = Math.min(progress + 10, 99); // update progress
            code = code.replace(oldSuggestions, newSuggestions);
            progress = Math.min(progress + 1, 99); // update progress
            sa.setText(code);
            progress = Math.min(progress + 2, 99); // update progress
            wikiCodeSingle = code;
            b.writeContent(sa);
            progress = Math.min(progress + 45, 99); // update progress
            connected = true;
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
        }        
    }
    
    /**
     * Removes the given (toRemove) multiple-line suggestion along with its
     * comment from the suggestions page dedicated to "language"
     * @param language Language from which to remove the suggestion
     * @param code Wiki code of the suggestions page for the language
     * @param key Key of the suggestion to remove
     * @param toRemove String to remove from suggestions page
     */
    public static void removeSuggestion_multiple(String language, String code, String key, String toRemove) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 10, 99); // update progress
            SimpleArticle sa = new SimpleArticle();
            progress = Math.min(progress + 2, 99); // update progress
            sa.setTitle(InitParameters.MULTIPLE_SUGGESTIONS_PAGE + "_" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 1, 99); // update progress
            String[] split = code.split(InitParameters.START_DELIMITER + key + " =");
            String[] lines = split[1].split("\n");
            String newSuggestions = "";
            String oldSuggestions = "";
            String[] suggestions = new String[100];
            progress = Math.min(progress + 8, 99); // update progress
            // iterate through lines
            for (int j=1; j<lines.length; j++) {
                if(!lines[j].contains(InitParameters.END_DELIMITER) && j<=100) {
                    oldSuggestions += (lines[j] + "\n");
                    if (!lines[j].equals(toRemove)) {
                       newSuggestions += (lines[j] + "\n");// updated suggestions
                       suggestions[j-1] = lines[j];
                    }                        
                } else {
                    break;
                }
            }
            progress = Math.min(progress + 10, 99); // update progress
            code = code.replace(oldSuggestions, newSuggestions);
            progress = Math.min(progress + 1, 99); // update progress
            sa.setText(code);
            progress = Math.min(progress + 2, 99); // update progress
            b.writeContent(sa);
            progress = Math.min(progress + 45, 99); // update progress
            connected = true;
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
        }        
    }
    
    /**
     * Deletes page from Wiki
     * @param pageTitle Title of the page to remove
     */
    public static void removePage(String pageTitle) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 45, 99); // update progress
            b.postDelete(pageTitle);
            progress = Math.min(progress + 12, 99); // update progress
            connected = true;
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
        }
    }
    
    /**
     * Overwrites current single-line translation with "translation" parameter  
     * @param language Language of the translation
     * @param key Key of the translation
     * @param translation New (single-line) translation to use
     */
    public static void validateTranslation_single(String language, String key, String translation) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 21, 99); // update progress
            SimpleArticle sa = new SimpleArticle();
            progress = Math.min(progress + 1, 99); // update progress
            sa.setTitle("Translations:" + InitParameters.SINGLE_PAGE + "/" + key + "/" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 2, 99); // update progress
            sa.setText(translation);
            progress = Math.min(progress + 3, 99); // update progress
            b.writeContent(sa);
            progress = Math.min(progress + 55, 99); // update progress
            connected = true;
        } catch(Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
        }
    }
    
    /**
     * Overwrites current multiple-line translation with "translation" parameter.
     * @param language Language of the translation
     * @param key Key of the translation
     * @param translation New (multiple-line) translation to use
     */
    public static void validateTranslation_multiple(String language, String key, String translation) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 21, 99); // update progress
            SimpleArticle sa = new SimpleArticle();
            progress = Math.min(progress + 1, 99); // update progress
            sa.setTitle("Translations:" + InitParameters.MULTIPLE_PAGE + "/" + key + "/" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 2, 99); // update progress
            sa.setText(translation);
            progress = Math.min(progress + 3, 99); // update progress
            b.writeContent(sa);
            progress = Math.min(progress + 55, 99); // update progress
            connected = true;
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
        }
    }
    
    /**
     * Method to get connection status
     * - To be called after connection attempts
     * @return True if connection succeeded, False otherwise
     */
    public static boolean getConnectionStatus() {
        return connected;
    }

    /**
     * Method to get download or upload progress
     * - To be called while downloading or uploading from a separate thread
     * @return Download or upload progress (percentage complete)
     */
    public static int getProgress() {
        return progress;
    }

    /**
     * Method to reinitialize progress
     * - To be called after completing download or upload instructions
     */
    public static void resetProgress() {
        progress = 0;
    }

    /**
     * Returns private class variable "wikiCodeSingle" which gets written in the
     * removeSuggestion_single method
     * - To be called after a call to removeSuggestion_single
     * @return Updated Wiki code of the suggestions page (single-line)
     */
    public static String getWikiCode_single(){
        return wikiCodeSingle;
    }
    
    /**
     * Returns wikiCode of the suggestions page for single lines corresponding
     * to the given language
     * @param language Language from which to get the suggestions code
     * @return Wiki code of the corresponding (single-line) suggestions page
     */
    public static String getSuggestionsCode_single(String language) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 10, 99); // update progress
            SimpleArticle sa = b.readData(InitParameters.SINGLE_SUGGESTIONS_PAGE + "_" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 22, 99); // update progress
            connected = true;
            return sa.getText();
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
            return null;            
        }
    }

    /**
     * Returns wikiCode of the suggestions page for multiple lines corresponding
     * to the given language
     * @param language from which to get the suggestions code
     * @return Wiki code of the corresponding (multiple-line) suggestions page
     */
    public static String getSuggestionsCode_multiple(String language) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 10, 99); // update progress
            SimpleArticle sa = b.readData(InitParameters.MULTIPLE_SUGGESTIONS_PAGE + "_" + InitParameters.getLanguageCode(language));
            progress = Math.min(progress + 22, 99); // update progress
            connected = true;
            return sa.getText();
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
            return null;            
        }
    }

    /**
     * Method to get Wiki page code
     * @param title Title of the page to get
     * @return Wiki code of the page
     */
    public static String getPageCode(String title) {
        try {
            MediaWikiBot b = initBot();
            progress = Math.min(progress + 24, 99); // update progress
            SimpleArticle sa = b.readData(title);
            progress = Math.min(progress + 87, 99); // update progress
            connected = true;
            return sa.getText();
        } catch (Exception e) {
            new TranslationDialog(InitParameters.DATABASE_ERROR_MESSAGEa,InitParameters.DATABASE_ERROR_MESSAGEc).setVisible(true);
            connected = false;
            return ""; 
        }
    }
    
    /*
     * This method parses wikiCode to key-value pairs which get stored in a 
     * DoubleMap and returned.
     */
    private static DoubleMap getDoubleMap(String wikiCode) {
        DoubleMap dm = new DoubleMap(); // create the DoubleMap
        String[] lines = wikiCode.split("\n");
        // iterate through all lines
        for (int i=0; i<lines.length; i=i+2) {
            String key  = lines[i].substring(0, lines[i].length()-2);
            String value = lines[i+1];
            dm.KeytoValue.put(key, value);
            dm.ValuetoKey.put(value, key);            
        }       
        return dm;
    }
}
