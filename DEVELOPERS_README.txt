This file gives some hints and comments for developers.


First some words about the history of the GeckoCIRCUITS sources. The program
development was started at ETH Zurich from electrical engineers. The first
developer and founder was not a programmer, therefore the original sourcecode
was not in the very best condition.
During the last years, we tried to clean up the code as much as possible. 
At initial project phases, tens of thousands of sourcecode lines were created via
copy & paste, which finally generated many problems. In case you find .java
files where most variable names are still in German, there is a high probability
that this is an original source file that has not yet been refactored and
improved by us.
Since the files with "bad programming style" was really large, we were first
thinking about starting the project from scratch. However, since there were
already many users that have existing model files, which they want to be
usable in the future, too, we decided not to start from scratch, but to
improve the sourcecode gradually.

For you, as developer, this implies the following:

- whenever you work with the sourcecode, try to improve it. And there is really
a lot to improve. Even if you just read through source parts that you just want
to understand, don't leave these parts unchanged. Use an IDE for find and change
variable names so that the readabilty of the program improves. Try to reduce
code duplication (DRY: Don't Repeat Yourself!)

- There are probably two levels of possible improvements:
	1.) General code quality improvements (readabilty, duplications, ...)
	2.) Structural changes and Object-orientation

At the moment, GeckoCIRCUITS does not have the best possible structure, i.e.
there are tight package dependencies, and those dependencies mostly bidirectional.
Inheritance is used too often, abstraction is not used where it should be
used. Whenever you believe that you can re-organise the code in a way that
the complexity improves, please do it. In our opinion for the moment 1.) has 
higher priority than 2.), since a better readable and more DRY code can later
be refactored more easily to a better objectoriented structure.

- Comments in the sources: My philosophy here is: Comments are extremely useful
when they help to clarify and improve the readability of the program. But: What
benefit gives you the following comment:
	/**
	  * @returns the number of simulation steps
	  **/
	public int getNumberSimulationSteps()
This comment is nothing else than clutter, the getter name includes already
all information that is needed. Comments tend to get outdated and "rot". Therefore,
before you write any source comment, think first twice if doing refactoring of a
piece of code, e.g. to a method with a proper name, is more effective than 
introducing a new comment.

- Code Quality Tools: Since the code was in quite a bad state (and some 
parts are still) in the past, we became fan of some code quality tools available
for the Netbeans IDE. These tools helped us a lot to find problematic pieces
of codes and still helps us to keep a consistent programming style. Whenever
you do contribute to this project, we would recommend you to install both
tools Checkstyle and PMD. 
Nowadays, PMD and Checstyle seem to get fully integrated into Netbeans. However 
we made the experience that using the external plugins is more effective. You
can install them in the following way:
Netbeans -> Tools -> Plugins -> Settings -> Add
Insert the following link:
SQE (Software Quality Environment)
http://deadlock.netbeans.org/hudson/job/sqe/lastStableBuild/artifact/build/full-sqe-updatecenter/updates.xml
Ok and go to "available plugins". Check for updates. Now you can select and install
PMD and Checkstyle. Please enshure that you select here the SQE plugins, and not the PMD
version that is already included in Netbeans.
The probject folder ./nbproject/ already contains configuration properties
for PMD (pmd.settings) and Checkstyle (checkstyle.xml). The path to the checkstyle.xml
has to be set in the Netbeans properties to become effective. Therefore, please
go to GeckoCIRCUITS-> Properties -> Quality -> Checkstyle and select your
checkstyle.xml file as setting.
Then, in your netbeans code window, there will be shown many warning messages,
e.g. as "method cyclomatic complexity is too high (54)".

- Old code / new code: The worst files and packages in GeckoCIRCUITS are the main
window ("Fenster.java") or the solver files ("Simulationskern.java", "LKMatrices.java").
The solver has already been re-implemented from scratch. Up to now, the new solver
is not yet included in the main project. This is still work to do. You can find the 
source files for the new solver in the srcNewSolver directory.
The "scope" has already been re-programmed from scratch. However, the old scope is
still used in the curve visualization for nonlinear components or loss curves.
These should be replaced in the future, too, with the new scope code.

- Obfuscation: We use proguard for obfuscating the .jar file. The reason for 
obfuscation is not to make a re-engineering impossible, but to shrink the size
of the executable .jar-file to about 1.5 MB. This improves the loading speed
for GeckoCIRCUITS as online (browser) applet a lot.

- Bugzilla: There is a bugzilla page available at www.bugs.gecko-simulations.org .

- GIT: We use GIT as version control system. However, our git repository is not
yet accessible for the public. Whenever you have some improvements or patches,
please send us the changes per email. When a contributor submits us some useful
code, we will hive him access to our git repository on request, then.

- Testing: There are some JUnit tests included (source folder "tests"). At the
moment, only control blocks are covered. Additionally, we set up some model
files which do some system tests. Please run the test from time to time to
enshure that you did not introduce bugs or break anything important within
the code.

- Internationalization: We did some effort for preparing GeckoCIRCUITS for
translation into multiple languages. The source files are already included
in the project, but not yet used. This is still some work to do.
