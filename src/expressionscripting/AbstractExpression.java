package expressionscripting;

import javax.script.ScriptException;

abstract class AbstractExpression {

    protected final Object _nameable;
    final String _scriptingCodeGivenFromUser;

    abstract Double evaluate() throws ScriptException;

    public static AbstractExpression newInstance(final Object nameable, final String expression) 
    throws ScriptException {
        VariableExpression possibleReturnValue = new VariableExpression(nameable, expression);
        if (possibleReturnValue.hasNoVariable()) {
            return new ConstantExpression(nameable, possibleReturnValue);
        }
        return possibleReturnValue;
    }

    AbstractExpression(final Object nameable, final String expression) {
        _nameable = nameable;
        _scriptingCodeGivenFromUser = expression;
    }

    public boolean nameMatchesFirstTest(final String testName) {
        return _nameable.toString().equals(testName);
    }    
    
    @Override
    public final String toString() {
        return _nameable.toString() + " = " + _scriptingCodeGivenFromUser;
    }
}