package gecko.geckocircuits.newscope;

public enum GeckoLineType {
    /**
     * WARNING: Don't change the order of the enumeration constants, ordinal() is used!
     */
    CONNECT_NEIGHBOURS(0, "Connect neigbours"),
    BAR(1, "Bar to x-Axis");    

    static GeckoLineStyle getFromOrdinal(final int ordinal) {
        for (GeckoLineStyle val : GeckoLineStyle.values()) {
            if (val.ordinal() == ordinal) {
                return val;
            }
        }
        assert false;
        return null;
    }
    private final int _code;
    private final String _description;
    
    GeckoLineType(final int code, final String description) {
        this._code = code;
        _description = description;
    }

    public int code() {
        return _code;
    }        

    public static GeckoLineStyle setzeLinienstilSelektiert(final int ordinal) {
        for (GeckoLineStyle val : GeckoLineStyle.values()) {
            if (val.ordinal() == ordinal) {
                return val;
            }
        }
        assert false;
        return null;
    }
    

    public static GeckoLineType getFromCode(final int gLSCode) {
        for (GeckoLineType val : GeckoLineType.values()) {
            if (val._code == gLSCode) {
                return val;
            }
        }
        
        // default:
        return GeckoLineType.CONNECT_NEIGHBOURS;
    }

    @Override
    public String toString() {
        return _description;
    }
    
    
}
