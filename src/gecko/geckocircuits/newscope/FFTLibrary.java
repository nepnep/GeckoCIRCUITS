/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.newscope;

import edu.emory.mathcs.jtransforms.fft.FloatFFT_1D;

/**
 *
 * @author andy
 */
class FFTLibrary {

    static void calculateForwardFFT(float[] data) {
        FloatFFT_1D forwardFFT = new FloatFFT_1D(data.length);
        forwardFFT.realForward(data);
    }

    static void calculateInverseFFT(float[] data) {
        FloatFFT_1D inverseFFT = new FloatFFT_1D(data.length);
        inverseFFT.realInverse(data, false);
    }    
}
