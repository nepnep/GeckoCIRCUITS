//package gecko.geckocircuits.newscope;
//
//import gecko.geckocircuits.datacontainer.DataContainerExternalWrapper;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Stack;
//
///**
// * Contains the currently defined external signals within the current data
// * container.
// */
//public final class DefinedExternalSignals{
//  private final List<ExternalSignal> _scopeExternalSignals = new ArrayList<ExternalSignal>();
//  private DataContainerExternalWrapper _externalWrapper;
//  private GraferV4 _grafer;
//  private final Stack<ScopeSignalRegular> _origScopeSignals;
//
//  /**
//   * Constructor saving the signals contained in the current scope before adding
//   * any external signals.
//   * @param origScopeSignals The original scope signals.
//   */
//  public DefinedExternalSignals(final Stack<ScopeSignalRegular> origScopeSignals){
//    this._origScopeSignals = origScopeSignals;
//  }
//
//  public void setGrafer(final GraferV4 grafer){
//    this._grafer = grafer;
//  }
//
//  /**
//   * Returns the signal at index.
//   * @param index The index of the external signal to return.
//   * @return The signal at index as an ExternalSignal object.
//   */
//  public ExternalSignal get(final int index){
//    return this._scopeExternalSignals.get(index);
//  }
//
//  public int size(){
//    return this._scopeExternalSignals.size();
//  }
//
//  public Stack<ScopeSignalRegular> getOrigScopeSignals(){
//    return _origScopeSignals;
//  }
//
//  /**
//   * Register all external signals in the external wrapper.
//   * @param externalWrapper The wrapper for which the external signals should be
//   * registered.
//   */
//  public void registerIndices(final DataContainerExternalWrapper externalWrapper){
//    externalWrapper.defineExternalSignals(this._scopeExternalSignals);
//    this._externalWrapper = externalWrapper;
//  }
//
//  /**
//   * Add an external signal to the list of defined external signals.
//   * @param index The signal to add.
//   */
//  public void defineNewExternalSignal(final ExternalSignal newSignal){
//    this._grafer.getManager().addExternalSignal(newSignal);
//    if(!this._scopeExternalSignals.contains(newSignal)){
//      this._scopeExternalSignals.add(newSignal);
//    }
//    if(this._externalWrapper != null){  // when no simulation was done before, this could be null!
//      this.registerIndices(this._externalWrapper);
//    }
//  }
//
//  /**
//   * Remove an external signal from the list of defined external signals.
//   * @param toDelete A reference to the external signal to delete.
//   */
//  public void unDefineExternalSignal(final ExternalSignal toDelete){
//    this._grafer.getManager().removeExternalSignal(toDelete);
//    this._externalWrapper.removeSignal(this._scopeExternalSignals.size() - this._scopeExternalSignals.indexOf(toDelete) - 1);
//    this._scopeExternalSignals.remove(toDelete);
//    this.registerIndices(this._externalWrapper);
//  }
//}
