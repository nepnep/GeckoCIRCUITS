package gecko.geckocircuits.allg;

import gecko.GeckoSim;
import gecko.geckocircuits.control.QuasiPeakCalculator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;

public class StartupWindow extends javax.swing.JDialog {

    private static final String DONATE_URL = "http://www.gecko-simulations.com/open_source/donate.html";
    private static final String SUPPORT_URL = "http://www.gecko-simulations.com/open_source/support.html";

    public static boolean testDialogOpenSourceVersion(final String featureName) {
        try {
            QuasiPeakCalculator.class.getName();
        } catch (NoClassDefFoundError err) {
            StartupWindow window = StartupWindow.fabricWithoutDonationHint(featureName);
            window.setVisible(true);
            return true;            
        }
        return false;
    }
    
    public static boolean testOpenSourceVersion() {
        try {
            QuasiPeakCalculator.class.getName();
        } catch (NoClassDefFoundError err) {
            return true;            
        }
        return false;
    }
    
    private long DISPOSE_WAIT_TIME_MILLIS = 5000;
    private static final long START_DELAY_TIME_MILLIS = 10000;
    private static final Random rand = new Random(System.currentTimeMillis());
    private static int WINDOW_PROBABILITY = 10;
    public static final String DONATE_CODE_KEY = "DONATE_CODE";
   
    private final Timer disposeTimer = new Timer();
    private final static Timer delayWindowTimer = new Timer();

    static void fabricBlockingSometimes() {
    
        if (donateCodeValid()) {
            return;
        }
        if (rand.nextInt() % WINDOW_PROBABILITY == 0) {
            StartupWindow window = new StartupWindow(GeckoSim._win);
            window.setVisible(true);
        } else {
            return;
        }
    }

    static void fabricUnBlocking() {
        
        if (donateCodeValid()) {
            return;
        }

        StartupWindow window = new StartupWindow(GeckoSim._win);
        delayWindowTimer.schedule(new DelayWindowVisibleTask(window), START_DELAY_TIME_MILLIS);

    }

    public StartupWindow(final JFrame parentFrame) {
        super(parentFrame, true);
        initComponents();
        this.setTitle("GeckoCIRCUITS Open-Source Information");
        this.setLocationRelativeTo(parentFrame);        
    }
    
    public static StartupWindow fabricWithoutDonationHint(final String featureName) {
        StartupWindow returnValue = new StartupWindow(null);
        returnValue.jLabelDonate.setVisible(false);
        returnValue.jLabelDontationText.setText("<html><font color='red'><b>" + featureName + 
                ": This feature of GeckoCIRCUITS is not included in the "
                + "open-source release.</b></html>");        
        returnValue.jButtonEnterCode.setVisible(false);
        returnValue.DISPOSE_WAIT_TIME_MILLIS = 0;
        returnValue.pack();
        return returnValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonOk = new javax.swing.JButton();
        jLabelDonate = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabelSupport = new javax.swing.JLabel();
        jLabelDontationText = new javax.swing.JLabel();
        jButtonEnterCode = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jButtonOk.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jButtonOk.setText("Ok");
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        jLabelDonate.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabelDonate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelDonate.setText("<html><a href=\"www.gecko-simulations.com/open_source/donate.html\">www.gecko-simulations.com/open_source/donate.html</a></html>");
        jLabelDonate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelDonateMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("<html><body width='750'>This software is published as open-source software under the Gnu Public License (GPL version 3).  For the detailed license text,   please refer to the Licensing Dialog  \"Help -> Licensing\".  Please note that two different releases of GeckoCIRCUITS are available:  <ul> \t  <li> The open-source release that you have currently running, which you can use without any cost.  <li> A professional release, which includes many bug-fixes and additional features. The additional features in the GeckoCIRCUITS professional release are the following: <ul> \t <li> GeckoSCRIPT: A scripting interface which allows full control over the circuit solver.  Using GeckoSCRIPT, \t you can set all circuit parameters, run optimizations from within MATLAB or Java. \t  <li> Matlab/Simulink interface: GeckoCIRCUITS can be combined with Simulink models \t  <li> EMI testreceiver control block \t</ul>   In order to access the professional release of GeckoCIRCUITS including above mentioned features, you have to  set up a support and maintenance contract with Gecko-Simulations AG. For more information, please contact us via email (contact@gecko-simulations.com)  or visit the following link: </ul></html>  ");

        jLabelSupport.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabelSupport.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSupport.setText("<html><a href=\"www.gecko-simulations.com/open_source/donate.html\">www.gecko-simulations.com/open_source/support.html</a></html>");
        jLabelSupport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelSupportMouseClicked(evt);
            }
        });

        jLabelDontationText.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabelDontationText.setText("<html><body width='750'> If a support contract with Gecko-Simulations is not an option for you, please consider to make a donation\n and, by doing so, support the further development of the tool. After the donation receipt, we will send you an email with instructions \non how to block this donation-pop-up window in GeckoCIRCUITS. \nPlease follow this link to give a donation:");

        jButtonEnterCode.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jButtonEnterCode.setText("Enter Donation Code");
        jButtonEnterCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnterCodeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabelSupport, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(12, 12, 12)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 744, Short.MAX_VALUE)
                                .addComponent(jLabelDontationText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelDonate, javax.swing.GroupLayout.PREFERRED_SIZE, 744, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(277, 277, 277)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButtonEnterCode, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonOk, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jButtonEnterCode, jButtonOk});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelSupport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelDontationText, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelDonate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonEnterCode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonOk)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabelDonateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelDonateMouseClicked
        LaunchBrowser.launch(DONATE_URL);
    }//GEN-LAST:event_jLabelDonateMouseClicked

    private void jLabelSupportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSupportMouseClicked
        LaunchBrowser.launch(SUPPORT_URL);
    }//GEN-LAST:event_jLabelSupportMouseClicked

    private final class SleepTastDispose extends TimerTask {

        public void run() {
            disposeTimer.cancel();
            StartupWindow.this.dispose();
        }
    }

    private static final class DelayWindowVisibleTask extends TimerTask {

        private final StartupWindow _window;

        public DelayWindowVisibleTask(StartupWindow window) {
            _window = window;
        }

        public void run() {
            _window.setVisible(true);
        }
    }

    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        jButtonOk.setText("Closing Window...");
        jButtonOk.repaint();
        disposeTimer.schedule(new SleepTastDispose(), DISPOSE_WAIT_TIME_MILLIS);

    }//GEN-LAST:event_jButtonOkActionPerformed

    private void jButtonEnterCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEnterCodeActionPerformed
        try {
            Thread.sleep(2000);
            JDialog dialog = new DialogEnterDonateCode(GeckoSim._win, true);
            dialog.setVisible(true);
        } catch (InterruptedException ex) {
            Logger.getLogger(StartupWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButtonEnterCodeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEnterCode;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelDonate;
    private javax.swing.JLabel jLabelDontationText;
    private javax.swing.JLabel jLabelSupport;
    // End of variables declaration//GEN-END:variables

    private static boolean donateCodeValid() {
        try {
        String donateProperyString = GeckoSim.applicationProps.getProperty(DONATE_CODE_KEY);
        if (donateProperyString == null) {
            return false;
        }
        
        long endDateMillisToTest = endDateMillis(donateProperyString.trim());
        
        long days = getValidDays(endDateMillisToTest);
        
        if(days > 20000) { // in case the result is too far in the future - somebody might have hacked!
            return false;
        }
        
        if (days > 0) {
            return true;
        } else {
            return false;
        }
        } catch(Throwable error) {
            return false;
        }
    }

    private static long encriptMillisPlusOneYear() {
        long now = System.currentTimeMillis();
        long milliSecondsInYear = (long) (1000.0 * 400.0 * 24.0 * 60.0 * 60.0);
        long in1Year = now + milliSecondsInYear;
        
        for (int i = 0; i < 10000; i++) {
            long testResult = encryptLong(in1Year + i);

            if (testResult % 2141 == 0) {                                
                System.out.println("Code is valid for " + getValidDays(decryptLong(testResult)) + " dayss");
                return testResult;
            }            
        }
        throw new Error("Could not generate valid code!!!");
    }

    

    private static long endDateMillis(final String encrypted) {
        long numericEncripted = Long.parseLong(encrypted);
        if (numericEncripted % 2141 != 0) {
            return 1;
        }
        long returnValue = decryptLong(numericEncripted);        
        return returnValue;        
    }

    private static long encryptLong(long time) {
        long result = time;
        final long longThird = Long.MAX_VALUE / 3;

        for (int i = 10; i <= 20; i++) {
            result += longThird * i;
        }

        return -result;
    }

    private static long decryptLong(long time) {
        long result = -time;
        final long longThird = Long.MAX_VALUE / 3;

        for (int i = 20; i >= 10; i--) {
            result -= longThird * i;
        }        
        return result;
    }    
    
    private static long getValidDays(long endDateMillisToTest) {
        long millisLeft = endDateMillisToTest - System.currentTimeMillis();                
        double daysLeft = millisLeft / 1000.0 / 24 / 3600;        
        return (long) daysLeft;
    }            
    
    public static void main(String[] args) {
        System.out.println("xxxxxxx " + encriptMillisPlusOneYear());
    }
}