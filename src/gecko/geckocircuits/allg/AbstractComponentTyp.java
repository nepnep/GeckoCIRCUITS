package gecko.geckocircuits.allg;

import gecko.geckocircuits.circuit.AbstractTypeInfo;

public interface AbstractComponentTyp {  
    int getTypeNumber(); 
    AbstractTypeInfo getTypeInfo();
}
