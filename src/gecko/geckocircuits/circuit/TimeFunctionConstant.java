package gecko.geckocircuits.circuit;

public class TimeFunctionConstant extends TimeFunction {

    public double _value;

    public TimeFunctionConstant(double value) {
        _value = value;
    }


    public final void setValue(double value) {
        _value = value;
    }

    @Override
    public double calculate(double t, double dt) {
        return _value;
    }

    public void stepBack() { }

}
