/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.circuit;

import gecko.geckocircuits.circuit.circuitcomponents.AbstractCircuitTypeInfo;
import gecko.i18n.resources.I18nKeys;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andy
 */
public class CircuitTypeInfo extends AbstractCircuitTypeInfo {

    public CircuitTypeInfo(Class<? extends AbstractBlockInterface> typeClass, String idString, I18nKeys typeDescription) {
        super(typeClass, idString, typeDescription);
    }

    public CircuitTypeInfo(Class<? extends AbstractBlockInterface> typeClass, String idString, I18nKeys typeDescription, I18nKeys typeDescriptionVerbose) {
        super(typeClass, idString, typeDescription, typeDescriptionVerbose);
    }

    @Override
    public ConnectorType getSimulationDomain() {
        return ConnectorType.LK;
    }    
}
