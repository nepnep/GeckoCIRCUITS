package gecko.geckocircuits.circuit.losscalculation;


/**
 * Whenever a loss calculation can be split into conduction and switching losses,
 * the conduction loss class should implement this interface.
 * @author andy
 */

public interface LossCalculationSplittable {
    double getSwitchingLoss();
    double getConductionLoss();
}
