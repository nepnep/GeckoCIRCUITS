/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations AG
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuits.circuit.losscalculation;

import java.util.List;

public class DetailedLossLookupTable {

    static DetailedLossLookupTable fabric(final List<? extends LossCurve> messkurvePvSWITCH, 
            final int dataIndex) {
        double[][] currentValues = new double[messkurvePvSWITCH.size()][];
        double[][] energyValues = new double[messkurvePvSWITCH.size()][];
        double[] temperatures = new double[messkurvePvSWITCH.size()];
        for(int i = 0; i < messkurvePvSWITCH.size(); i++) {
            LossCurve curve = messkurvePvSWITCH.get(i);
            currentValues[i] = curve.data[0];            
            energyValues[i] = new double[curve.data[dataIndex].length];            
            System.arraycopy(curve.data[dataIndex], 0, energyValues[i], 0, energyValues[i].length);                        
            if(messkurvePvSWITCH.get(0) instanceof SwitchingLossCurve) {
                for(int j = 0; j < energyValues[i].length; j++) {
                    double curveBlockingVoltage = ((SwitchingLossCurve) curve)._uBlock.getDoubleValue();
                    energyValues[i][j] /= curveBlockingVoltage;                    
                }
            }
            temperatures[i] = curve.tj.getDoubleValue();
        }
        
        return new DetailedLossLookupTable(currentValues, energyValues, temperatures);
    }

    private final double[][] _energyValues;
    private final double[][] _currentValues;
    private final double[] _temperatures;
    
    private DetailedLossLookupTable(final double[][] currentValues, final double[][] energyValues, final double[] temperatures) {
        _energyValues = energyValues;
        _currentValues = currentValues;
        _temperatures = temperatures;
        assert _temperatures.length == _currentValues.length;
        assert _energyValues.length == _temperatures.length;        
    }

    public double getInterpolatedYValue(double temp, double current) {
        int upperTempIndex = 0;
        while (upperTempIndex < _temperatures.length - 1 && temp > _temperatures[upperTempIndex]) {
            upperTempIndex++;
        }

        int lowerTempIndex = upperTempIndex - 1;
        if (lowerTempIndex < 0) {
            lowerTempIndex = 0;
        }
                
        double[] data = _energyValues[upperTempIndex];
        double upperTempEnergy = findLossValueOnCurve(current,  _currentValues[upperTempIndex], data);

        if (upperTempIndex == lowerTempIndex) {
            return upperTempEnergy;
        }
        
        data = _energyValues[lowerTempIndex];
        double lowerTempEnergy = findLossValueOnCurve(current, _currentValues[lowerTempIndex], data);

        double upperTemp = _temperatures[upperTempIndex];
        double lowerTemp = _temperatures[lowerTempIndex];

        double wheigt2 = (temp - lowerTemp) / (upperTemp - lowerTemp);
        double wheigt1 = (upperTemp - temp) / (upperTemp - lowerTemp);

        assert wheigt1 + wheigt2 < 1.01 && wheigt1 + wheigt2 > 0.99;
        double returnValue = (lowerTempEnergy * wheigt1 + upperTempEnergy * wheigt2);
        // only return positive energies!        
        return Math.max(returnValue, 0);
    }
    
    public double getInterpolatedXValue(double temp, double yValue) {
        int upperTempIndex = 0;
        while (upperTempIndex < _temperatures.length - 1 && temp > _temperatures[upperTempIndex]) {
            upperTempIndex++;
        }

        int lowerTempIndex = upperTempIndex - 1;
        if (lowerTempIndex < 0) {
            lowerTempIndex = 0;
        }
                
        double[] data = _currentValues[upperTempIndex];
        double upperTempEnergy = findLossValueOnCurve(yValue,  _energyValues[upperTempIndex], data);

        if (upperTempIndex == lowerTempIndex) {
            return upperTempEnergy;
        }
        
        data = _currentValues[lowerTempIndex];
        double lowerTempEnergy = findLossValueOnCurve(yValue, _energyValues[lowerTempIndex], data);

        double upperTemp = _temperatures[upperTempIndex];
        double lowerTemp = _temperatures[lowerTempIndex];

        double wheigt2 = (temp - lowerTemp) / (upperTemp - lowerTemp);
        double wheigt1 = (upperTemp - temp) / (upperTemp - lowerTemp);

        assert wheigt1 + wheigt2 < 1.01 && wheigt1 + wheigt2 > 0.99;
        double returnValue = (lowerTempEnergy * wheigt1 + upperTempEnergy * wheigt2);
        // only return positive energies!                                
        return Math.max(returnValue, 0);
    }

    private double findLossValueOnCurve(double current, double[] currents, double[] data) {
        int rightCurrentIndex = 0;

        while (currents[rightCurrentIndex] <= current && rightCurrentIndex < currents.length - 1) {
            rightCurrentIndex++;
        }

        int leftCurrentIndex = rightCurrentIndex - 1;

        leftCurrentIndex = Math.max(leftCurrentIndex, 0);

        double en1 = data[leftCurrentIndex];

        if (leftCurrentIndex == rightCurrentIndex) {
            return en1;
        }

        double en2 = data[rightCurrentIndex];
        double cur1 = currents[leftCurrentIndex];
        double cur2 = currents[rightCurrentIndex];

        double wheigt2 = (current - cur1) / (cur2 - cur1);
        double wheigt1 = (cur2 - current) / (cur2 - cur1);

        assert wheigt1 + wheigt2 < 1.01 && wheigt1 + wheigt2 > 0.99;
        double returnValue = (en1 * wheigt1 + en2 * wheigt2);                
        // only return positive energies!
        return Math.max(returnValue, 0);
    }
        
}
