/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.circuit.losscalculation;

/**
 * This is an enum to differentiate between different loss components being calculated:
 * To allow the user to see only switching, or only conduction losses, or both.
 * @author anstupar
 */
public enum LossComponent {
    TOTAL, CONDUCTION, SWITCHING;
    
    private final static String saveStringTotal = "total";
    private final static String saveStringConduction = "conduction";
    private final static String saveStringSwitching = "switching";
    
    @Override
    public String toString() {
        String description;
        switch(this) {
            case TOTAL:
                description = "Total losses";
                break;
            case CONDUCTION:
                description = "Conduction losses";
                break;
            case SWITCHING:
                description = "Switching losses";
                break;
            default:
                description = "";
                break;
        }
        return description;
    }
    
    public String getSaveString() {
        String saveString;
         switch(this) {
            case CONDUCTION:
                saveString = saveStringConduction;
                break;
            case SWITCHING:
                saveString = saveStringSwitching;
                break;
            case TOTAL:
            default:
                saveString = saveStringTotal;
                break;

        }
        return saveString;
    }
    
    
    public static LossComponent getEnumFromSaveString(final String saveString) {
        if (saveStringConduction.equals(saveString)) {
            return CONDUCTION;
        }
        else if (saveStringSwitching.equals(saveString)) {
            return SWITCHING;          
        }
        else {
            return TOTAL;
        }
    }
    
}
