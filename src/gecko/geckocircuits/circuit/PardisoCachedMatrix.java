package gecko.geckocircuits.circuit;

import java.util.Random;

/**
 *
 * @author andy
 */
public class PardisoCachedMatrix extends AbstractCachedMatrix {
    private final int N;
    private SymmetricSparseMatrix sparseMatrix;

    public PardisoCachedMatrix(double[][] matrix) {
        super(matrix);
        N = matrix.length;
    }

    @Override
    void initLUDecomp() {                
        
        SymmetricDoubleSparseMatrix sysMatrix = new SymmetricDoubleSparseMatrix(N);

        for (int i = 0; i < N; i++) {
            for (int j = i; j < N; j++) {
                if (i == 0 || j == 0) {
                    if (i == 0 && j == 0) {
                        sysMatrix.setValue(i, j, 1);
                    }
                } else {
                    if (_originalMatrix[i][j] != 0) {
                        sysMatrix.setValue(i, j, _originalMatrix[i][j]);
                        if(_originalMatrix[j][i] != _originalMatrix[i][j]) {
                            System.err.println("nonsymmetric: " + i + " " + j + " " + _originalMatrix[i][j] + " " + _originalMatrix[j][i]);
                            throw new RuntimeException("Error in sparse matrix solver: system matrix is not symmetric!");
                        }
                    }
                }

            }
        }
        
        sparseMatrix = new SymmetricSparseMatrix(sysMatrix);

        sparseMatrix.factorize(sysMatrix, N, N);        
    }

    @Override
    public void deleteCache() {
        sparseMatrix = null;
    }


    @Override
    public double[] solve(double[] bVector) {
        return sparseMatrix.solve(bVector);
    }
}
