package gecko.geckocircuits.circuit;

import gecko.geckocircuits.allg.TechFormat;
import javax.swing.table.DefaultTableCellRenderer;

class MyTableCellRenderer extends DefaultTableCellRenderer {
    private final TechFormat tf = new TechFormat();
    public MyTableCellRenderer() {
    }

    public void setValue(Object value) {
        if(value == null) {
            setText("");
        } else {
            setText(tf.formatENG(Double.parseDouble(value.toString()), 3));        
        }        
    }    
}