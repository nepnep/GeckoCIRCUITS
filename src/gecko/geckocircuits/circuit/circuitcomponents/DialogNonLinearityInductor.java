package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.DialogNonLinearity;

class DialogNonLinearityInductor extends DialogNonLinearity {

    public DialogNonLinearityInductor(final AbstractNonLinearCircuitComponent parent) {
        super(parent);
    }

    @Override
    public boolean isYAxisPlotLogarithmic() {
        return false;
    }
    
}
