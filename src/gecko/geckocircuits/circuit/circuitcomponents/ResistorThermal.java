package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class ResistorThermal extends AbstractResistor {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(ResistorThermal.class, "Rth", I18nKeys.RESISTOR_RTH_K_W);    
}
