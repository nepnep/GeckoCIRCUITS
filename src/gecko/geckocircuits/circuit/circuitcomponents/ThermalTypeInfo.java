package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractBlockInterface;
import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.ConnectorType;
import gecko.geckocircuits.circuit.SpecialTyp;
import gecko.i18n.resources.I18nKeys;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThermalTypeInfo extends AbstractTypeInfo {

    public ThermalTypeInfo(final Class<? extends AbstractBlockInterface> typeClass, 
            final String idString, final I18nKeys typeDescription) {
        super(typeClass, idString, typeDescription);
    }

    public ThermalTypeInfo(final Class<? extends AbstractBlockInterface> typeClass, final String idString, 
            final I18nKeys typeDescription, final I18nKeys typeDescriptionVerbose) {
        super(typeClass, idString, typeDescription, typeDescriptionVerbose);
    }

    @Override
    public final ConnectorType getSimulationDomain() {
        return ConnectorType.THERMAL;
    }

    @Override
    public final String getExportImportCharacters() {
        return "eTH";
    }    

    @Override
    public final String getSaveIdentifier() {
        return "ElementTHERM";
    }
    
    @Override
    public final AbstractBlockInterface fabric() {
        try {
            return _typeClass.newInstance();
        } catch (Throwable ex) {
            System.err.println("error: " + _typeClass);
            Logger.getLogger(SpecialTyp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    
}
