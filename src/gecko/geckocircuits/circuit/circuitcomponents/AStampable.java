/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.circuit.circuitcomponents;

/**
 *
 * @author andy
 */
interface AStampable {
    void stampMatrixA(final double[][] matrix, final double deltaT);
}
