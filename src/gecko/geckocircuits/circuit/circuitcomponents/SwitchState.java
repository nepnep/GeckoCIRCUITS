package gecko.geckocircuits.circuit.circuitcomponents;

//this class allows us to get information about a switching device's state - when it changed into the present state and what is it
public class SwitchState {
    
    public enum State {ON, OFF};
    
    private final State _state;
    private final AbstractCircuitBlockInterface _circuitElement;
    private final double _time;
    
    public SwitchState(AbstractCircuitBlockInterface elem, State switchState, double switchTime) {
        _state = switchState;
        _circuitElement = elem;
        _time = switchTime;
    }
    
    public State getState() {
        return _state;
    }
    
    public AbstractCircuitBlockInterface getElement() {
        return _circuitElement;
    }
    
    public String getElementName() {
        return _circuitElement.getStringID();
    }
    
    public double getTime() {
        return _time;
    }
    
    public String toString() {
        return getElementName() + ": " + _state;
    }       
}
