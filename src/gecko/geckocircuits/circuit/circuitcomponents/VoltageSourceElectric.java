package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.CircuitTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class VoltageSourceElectric extends AbstractVoltageSource {
    static final AbstractTypeInfo TYPE_INFO = 
            new CircuitTypeInfo(VoltageSourceElectric.class, "U", I18nKeys.VOLTAGE_SOURCE_U_V);        
}
