package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class CapacitorThermal extends AbstractCapacitor {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(CapacitorThermal.class, "Cth", I18nKeys.CAPACITOR_CTH_JK);
}
