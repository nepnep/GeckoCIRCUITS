package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class VoltageSourceReluctanceMMF extends AbstractVoltageSource {
    public static final AbstractTypeInfo TYPE_INFO = 
            new ReluctanceTypeInfo(VoltageSourceReluctanceMMF.class, "MMF", I18nKeys.MMF_AMP_TURNS);
    
}
