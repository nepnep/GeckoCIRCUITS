package gecko.geckocircuits.circuit.circuitcomponents;


public class IdealSwitchCalculator extends AbstractSwitchCalculator implements HistoryUpdatable {

    public IdealSwitchCalculator(final IdealSwitch parent) {
        super(parent);
    }

    @Override
    public void stampMatrixA(double[][] matrix, double dt) {
        double aW = 1.0 / _rDt;  //  +1/R        
        assert aW < 1E60;
        matrix[matrixIndices[0]][matrixIndices[0]] += (+aW);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+aW);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-aW);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-aW);
    }

    @Override
    public final void stampVectorB(double[] b, double t, double dt) {
    }


    public boolean isBasisStampable() {
        return true;
    }

    @Override
    public void updateHistory(double[] p) {

        _potential1 = p[matrixIndices[0]];
        _potential2 = p[matrixIndices[1]];

        // old current is not needed for Resistor, but the current has not yet been
        // calculated:
        _current = (p[matrixIndices[0]] - p[matrixIndices[1]]) / _rDt;
        //_oldCurrent = _current;
    }
}
