package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.CircuitTypeInfo;
import gecko.i18n.resources.I18nKeys;

class CurrentSourceCircuit extends AbstractCurrentSource {
    static final AbstractTypeInfo TYPE_INFO = 
            new CircuitTypeInfo(CurrentSourceCircuit.class, "I", I18nKeys.CURRENT_SOURCE_I_A);    
}
