package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.CircuitTypeInfo;
import gecko.i18n.resources.I18nKeys;
import java.util.List;

class CircuitGlobalTerminal extends AbstractCircuitGlobalTerminal {
    static final AbstractTypeInfo TYPE_INFO = 
            new CircuitTypeInfo(CircuitGlobalTerminal.class, "CKT_GLOBAL", I18nKeys.GLOBAL_CIRCUIT_TERMINAL);        
    
}
