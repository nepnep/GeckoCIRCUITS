package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class VoltageSourceThermalTemperature extends AbstractVoltageSource {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(VoltageSourceThermalTemperature.class, "DT", I18nKeys.DEFINED_TEMPERATURE_C);    
}
