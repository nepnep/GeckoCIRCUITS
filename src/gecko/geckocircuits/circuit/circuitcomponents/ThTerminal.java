package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

class ThTerminal extends AbstractCircuitTerminal {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(ThTerminal.class, "THERM_TERMINAL", I18nKeys.THERMAL_TERMINAL);    
}
