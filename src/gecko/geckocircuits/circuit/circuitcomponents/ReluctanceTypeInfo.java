package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractBlockInterface;
import gecko.geckocircuits.circuit.ConnectorType;
import gecko.i18n.resources.I18nKeys;

public class ReluctanceTypeInfo extends AbstractCircuitTypeInfo {

    public ReluctanceTypeInfo(Class<? extends AbstractBlockInterface> typeClass, String idString, I18nKeys typeDescription) {
        super(typeClass, idString, typeDescription);
    }

    public ReluctanceTypeInfo(Class<? extends AbstractBlockInterface> typeClass, String idString, I18nKeys typeDescription, I18nKeys typeDescriptionVerbose) {
        super(typeClass, idString, typeDescription, typeDescriptionVerbose);
    }        

    @Override
    public ConnectorType getSimulationDomain() {
        return ConnectorType.RELUCTANCE;
    }
    
}
