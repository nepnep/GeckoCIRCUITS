package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.CircuitTypeInfo;
import gecko.i18n.resources.I18nKeys;

public class ResistorCircuit extends AbstractResistor {
    static final AbstractTypeInfo TYPE_INFO = new CircuitTypeInfo(ResistorCircuit.class, "R", I18nKeys.RESISTOR_R_OHM);
}
