package gecko.geckocircuits.circuit.circuitcomponents;

public interface CurrentCalculatable {
    void calculateCurrent(final double[] potVector, final double deltaT, final double time);
    double getCurrent();
}
