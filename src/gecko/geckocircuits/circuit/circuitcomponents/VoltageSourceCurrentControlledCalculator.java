package gecko.geckocircuits.circuit.circuitcomponents;

public final class VoltageSourceCurrentControlledCalculator
        extends AbstractVoltageSourceControlledCalculator implements HistoryUpdatable {

    public VoltageSourceCurrentControlledCalculator(final AbstractVoltageSource parent) {
        super(parent);
    }

    @Override
    public void stampMatrixA(final double[][] matrix, final double deltaT) {
        assert _z > 0;
        super.stampMatrixA(matrix, deltaT);
        matrix[_z][_currentControl.getZValue()] = -_gain;
    }
}