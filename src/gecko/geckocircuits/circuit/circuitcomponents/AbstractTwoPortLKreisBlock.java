package gecko.geckocircuits.circuit.circuitcomponents;

import static gecko.geckocircuits.circuit.AbstractCircuitSheetComponent.dpix;
import gecko.geckocircuits.circuit.TerminalTwoPortComponent;
import java.awt.Graphics2D;

public abstract class AbstractTwoPortLKreisBlock extends AbstractCircuitBlockInterface {
    private static final int TWO_PORT_DIST = 2;            

    public AbstractTwoPortLKreisBlock() {
        createTwoPortTerminals();
    } 
    
    @Override
    protected void drawConnectorLines(final Graphics2D graphics) {
        graphics.drawLine(0, dpix * 2, 0, -dpix * 2);        
    }
        
    private void createTwoPortTerminals() {
        XIN.add(new TerminalTwoPortComponent(this, -TWO_PORT_DIST));
        final TerminalTwoPortComponent outTerminal = new TerminalTwoPortComponent(this, TWO_PORT_DIST);
        outTerminal.setIsFlowSymbolTerminal(true);
        YOUT.add(outTerminal);

    }
    
}
