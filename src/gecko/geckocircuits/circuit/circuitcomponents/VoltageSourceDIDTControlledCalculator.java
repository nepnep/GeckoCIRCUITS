package gecko.geckocircuits.circuit.circuitcomponents;


public final class VoltageSourceDIDTControlledCalculator extends AbstractVoltageSourceControlledCalculator
    implements HistoryUpdatable, BStampable {

    public VoltageSourceDIDTControlledCalculator(final AbstractVoltageSource parent) {
        super(parent);
    }

    @Override
    public void stampMatrixA(final double[][] matrix, final double deltaT) {
        assert _z > 0;
        super.stampMatrixA(matrix, deltaT);
        matrix[_z][_currentControl.getZValue()] += _gain / deltaT;

    }

    @Override
    public void stampVectorB(final double[] bVector, final double time, final double deltaT) {
            double value = _gain * _currentControl.getCurrent() / deltaT;
            bVector[_z] += value;//_function.calculate(t, dt);
    }

    @Override
    public boolean isBasisStampable() {
        return false;
    }

    @Override
    public void registerBVector(final BVector bvector) {
        // nothing todo??? 
    }

}
