package gecko.geckocircuits.circuit.circuitcomponents;


public final class ResistorCalculator extends CircuitComponent<AbstractResistor> implements AStampable, HistoryUpdatable {

    private static final double FAST_NULL_R = 1e-9;

    private double _resistance = 1;

    public ResistorCalculator(final AbstractResistor parent) {
        super(parent);        
        _resistance = parent._resistance.getValue();
    }

    public void setResistance(final double resistance) {
        if(_resistance < FAST_NULL_R) {
            _resistance = FAST_NULL_R;
        } else {
            _resistance = resistance;
        }
    }    

    @Override
    public void stampMatrixA(final double[][] matrix, final double deltaT) {
        final double addValue = 1.0 / _resistance;  //  +1/R        
        matrix[matrixIndices[0]][matrixIndices[0]] += (+addValue);
        matrix[matrixIndices[1]][matrixIndices[1]] += (+addValue);
        matrix[matrixIndices[0]][matrixIndices[1]] += (-addValue);
        matrix[matrixIndices[1]][matrixIndices[0]] += (-addValue);
    }


    @Override
    public String toString() {
        return super.toString() + getClass().getName() + "  " + _resistance + " " + matrixIndices[0] + " " + matrixIndices[1];
    }

    @Override
    public void updateHistory(final double[] potentials) {
        _potential1 = potentials[matrixIndices[0]];
        _potential2 = potentials[matrixIndices[1]];
        _voltage = _potential1 - _potential2;
        // old current is not needed for Resistor, but the current has not yet been
        // calculated:
        _current = (_potential1 - _potential2) / _resistance;
        //_oldCurrent = _current;
    }
    
}
