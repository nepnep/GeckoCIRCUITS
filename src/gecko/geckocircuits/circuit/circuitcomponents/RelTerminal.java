package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.circuitcomponents.AbstractCircuitTerminal;
import gecko.i18n.resources.I18nKeys;

class RelTerminal extends AbstractCircuitTerminal {        
    public static final AbstractTypeInfo TYPE_INFO = new ReluctanceTypeInfo(RelTerminal.class, "RELUCTANCE_TERMINAL", I18nKeys.RELUCTANCE_TERMINAL);
    
}
