package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;
import java.util.List;

class HeatFlowCurrentSource  extends AbstractCurrentSource {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(HeatFlowCurrentSource.class, "HEAT-SOURCE", I18nKeys.HEAT_SOURCE_W);    
    
}
