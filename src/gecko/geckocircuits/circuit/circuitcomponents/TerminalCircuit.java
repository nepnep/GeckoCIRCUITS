package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.CircuitTypeInfo;
import gecko.geckocircuits.circuit.circuitcomponents.AbstractCircuitTerminal;
import gecko.i18n.resources.I18nKeys;
import java.util.List;

public class TerminalCircuit extends AbstractCircuitTerminal {
    public static final AbstractTypeInfo TYPE_INFO = new CircuitTypeInfo(TerminalCircuit.class, "CIRCUIT_TERMINAL", I18nKeys.CIRCUIT_TERMINAL);            
}
