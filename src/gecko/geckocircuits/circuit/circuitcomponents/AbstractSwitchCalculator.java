package gecko.geckocircuits.circuit.circuitcomponents;


public abstract class AbstractSwitchCalculator extends CircuitComponent implements AStampable, BStampable {

    
    protected static final double NEARLY_ZERO_R = 1e-9;
    private static final double DEFAULT_TEMP = 25;
    
    protected double _rON = DEFAULT_R_ON;
    protected double _rOFF = DEFAULT_R_OFF;
    // variable resistance
    protected double _rDt = DEFAULT_R_OFF;
    protected double _uForward = DEFAULT_U_FORWARD;

    public static boolean switchAction = false;
    public static boolean switchActionOccurred = false;
    protected BVector _bVector;


    protected boolean _gateValue = false;

    public AbstractSwitchCalculator(final AbstractSwitch parent) {
        super(parent);
    }


    /*
     * maybe unclean coding: Thyristor overrides this method with a different implementation,
     * all other switches use this piece of code.
     */
    public void setGateSignal(final boolean value) {
        
        _gateValue = value;
        switchAction = true;
        
        if (_gateValue) {
            _rDt = _rON;
        } else {
            _rDt = _rOFF;
        }

        _rDt = Math.max(_rDt, NEARLY_ZERO_R);
        if(_bVector != null) {
            _bVector.setUpdateAllFlag();
        }
        
        //System.out.println("Gate signal set to " + _gateValue);
        
    }

    public final boolean isGateSignalOn() {
        return _gateValue;
    }

    public boolean isBasisStampable() {
        return true;
    }

    @Override
    public final void registerBVector(final BVector bvector) {
       _bVector = bvector;
    }

    public final void setROn(final double value) {
        _rON = value;
    }

    public final void setROff(final double value) {
        _rOFF = value;
    }

    public final void setUForward(final double value) {
        _uForward = value;
    }
    
    
    public SwitchState getState(double time) {
        
        SwitchState state;
        SwitchState.State componentState;
        
        if (_rDt > _rON) {
            componentState = SwitchState.State.OFF;
        }
        else {
            componentState = SwitchState.State.ON;
        }
        
        state = new SwitchState(_parent,componentState,time);
        
        return state;
        
    }

}
