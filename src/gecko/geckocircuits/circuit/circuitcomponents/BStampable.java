package gecko.geckocircuits.circuit.circuitcomponents;

public interface BStampable {

    void stampVectorB(double[] bVector, double time, final double deltaT);
    void registerBVector(BVector bVector);
    /**
     * tells if the component has the same stamp over several timesteps
     * @return true if stamp will not change all the time
     */
    boolean isBasisStampable();   
}
