package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

final class ResistorReluctance extends AbstractResistor {
    static final AbstractTypeInfo TYPE_INFO = new ReluctanceTypeInfo(ResistorReluctance.class, "Rel", I18nKeys.RELUCTANCE);    
}
