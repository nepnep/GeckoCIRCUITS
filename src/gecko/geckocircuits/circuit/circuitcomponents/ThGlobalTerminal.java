package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.i18n.resources.I18nKeys;

class ThGlobalTerminal extends AbstractCircuitGlobalTerminal {
    static final AbstractTypeInfo TYPE_INFO = 
            new ThermalTypeInfo(ThGlobalTerminal.class, "TH_GLOBAL", I18nKeys.GLOBAL_THERMAL_TERMINAL);    
}
