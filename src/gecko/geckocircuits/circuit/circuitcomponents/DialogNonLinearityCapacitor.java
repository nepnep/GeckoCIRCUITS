package gecko.geckocircuits.circuit.circuitcomponents;

import gecko.geckocircuits.circuit.DialogNonLinearity;

class DialogNonLinearityCapacitor extends DialogNonLinearity {

    public DialogNonLinearityCapacitor(final AbstractNonLinearCircuitComponent parent) {
        super(parent);
    }

    @Override
    public boolean isYAxisPlotLogarithmic() {
        return true;
    }
    
    
}
