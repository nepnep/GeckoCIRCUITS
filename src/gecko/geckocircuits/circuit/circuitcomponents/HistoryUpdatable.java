/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.circuit.circuitcomponents;

public interface HistoryUpdatable {
    public abstract void updateHistory(double[] p);
}
