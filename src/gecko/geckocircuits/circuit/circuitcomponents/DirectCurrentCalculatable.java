package gecko.geckocircuits.circuit.circuitcomponents;

/**
 * All Circuit components, where the current is needed in the
 * simulation should be DirectCurrrentCalculatable. This interface
 * adds a n new row/column into the solver system equations
 * @author andy
 */
public interface DirectCurrentCalculatable {
    void setZValue(int index);
    int getZValue();
    double getCurrent();
}
