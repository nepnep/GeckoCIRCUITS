package gecko.geckocircuits.circuit.circuitcomponents;

public interface PostProcessable {
    public void doPostProcess(double dt, double time);
}
