package gecko.geckocircuits.circuit.circuitcomponents;


import java.util.List;

public class MutualCouplingCalculator {
    private double _M;
    private double _k;
    private InductorCouplingCalculator _l1;
    private InductorCouplingCalculator _l2;

    
    public MutualCouplingCalculator(InductorCouplingCalculator l1, InductorCouplingCalculator l2, double k) {
        _k = k;

        _l1 = l1;
        _l2 = l2;

    }

    public MutualCouplingCalculator() {
    }

    public InductorCouplingCalculator getL1() {
        return _l1;
    }

    public InductorCouplingCalculator getL2() {
        return _l2;
    }

    private double getMutualInductance() {
        return _k * Math.sqrt(_l1.getInductance() * _l2.getInductance() );
    }


    void stampInductanceMatrix(double[][] inductanceMatrix, final List<InductorCouplingCalculator> allInductors) {
        int index1 = allInductors.indexOf(_l1);
        int index2 = allInductors.indexOf(_l2);
        inductanceMatrix[index1][index2] = getMutualInductance();
        inductanceMatrix[index2][index1] = getMutualInductance();
    }


}
