package gecko.geckocircuits.circuit.circuitcomponents;


public abstract class AbstractVoltageSourceCalculator extends CircuitComponent<AbstractTwoPortLKreisBlock>
    implements AStampable, DirectCurrentCalculatable, HistoryUpdatable {

    protected int _z = -1;


    public AbstractVoltageSourceCalculator(final AbstractTwoPortLKreisBlock parent) {
        super(parent);
    }
    
    
    @Override
    public void stampMatrixA(final double[][] matrix, final double deltaT) {
        assert _z > 0;
        matrix[matrixIndices[0]][_z] += (+1.0);
        matrix[matrixIndices[1]][_z] += (-1.0);
        matrix[_z][matrixIndices[0]] += (+1.0);
        matrix[_z][matrixIndices[1]] += (-1.0);

    }

    @Override
    public final int getZValue() {
        return _z;
    }

    @Override
    public final void setZValue(final int value) {
        _z = value;
    }

    @Override
    public final void updateHistory(final double[] potentials) {
        // System.out.println("function: " + _function + " " + _z);
        _current = potentials[_z];  // SpgQuellen-Stroeme stehen als Unbekannte ausnahmsweise auch im Knotenpotetial-Vektor
        _potential1 = potentials[matrixIndices[0]];
        _potential2 = potentials[matrixIndices[1]];
    }            
}
