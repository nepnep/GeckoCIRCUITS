/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.circuit;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author andy
 */
class MyTableComparator implements Comparator<List<Double>> {

    public MyTableComparator() {
    }

    @Override
    public int compare(List<Double> o1, List<Double> o2) {
        assert o1.size() == o2.size();
        if(o1.get(0) == null) {
            return 1;
        }
        if(o2.get(0) == null) {
            return -1;            
        }
        if(o1.get(0) == null && o2.get(0) == 0) {
            return 0;
        }
        if(o1.get(0) < o2.get(0)) {
            return -1;
        }
        
        if(o1.get(0) > o2.get(0)) {
            return 1;
        }
        
        if((double) o1.get(0) == (double) o2.get(0)) {
            return 0;
        }
                
        assert false : o1;
        return -1;
    }

    
    
}
