package gecko.geckocircuits.circuit;

abstract class AbstractCachedMatrix {
    protected int _hashCode = -1;
    protected static final int HASH_13 = 13;
    protected static final int HASH_7 = 7;
    protected static final int HASH_17 = 17;
    protected static final int HASH_23 = 23;
    protected static final int HASH_37 = 37;
    private static final int INT_LENGTH = 32;
    private double _latestAccessTime = -1;
    private int _accessCounter = 0;
    
    protected double[][] _originalMatrix;

    public AbstractCachedMatrix(final double[][] matrix) {
        _originalMatrix = matrix;        
    }

    
    abstract void initLUDecomp();
    abstract public void deleteCache();
    abstract public double[] solve(final double[] bVector);
    
    @Override
    public final int hashCode() {
        if (_hashCode == -1) {
            long newHashCode = HASH_13;
            for (int i = 0; i < _originalMatrix.length; i++) {
                final int iValue = HASH_13 * i - HASH_7;
                for (int j = 0; j < _originalMatrix[0].length; j++) {
                    final double matrixValue = _originalMatrix[i][j];
                    if (matrixValue != 0) {
                        final long matrixBits = Double.doubleToLongBits(matrixValue);
                        newHashCode += -HASH_17 + HASH_37 * i - HASH_17 * j + iValue
                                * (HASH_23 * j + HASH_13) * ((int) (matrixBits ^ (matrixBits >>> INT_LENGTH)));
                    }
                }
            }
            _hashCode = (int) ((int) (newHashCode ^ (newHashCode >>> INT_LENGTH)));
        }
        return _hashCode;
    }
    
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractCachedMatrix other = (AbstractCachedMatrix) obj;

        if (other._originalMatrix.length != this._originalMatrix.length) {
            return false;
        }

        for (int i = 0; i < _originalMatrix.length; i++) {
            for (int j = 0; j < _originalMatrix[0].length; j++) {
                if (_originalMatrix[i][j] != other._originalMatrix[i][j]) {
                    return false;
                }
            }
        }

        return true;
    }
    
    protected void setAccess(final double time) {
        _accessCounter++;
        _latestAccessTime = time;
    }

    protected int getAccessCounter() {
        return _accessCounter;
    }

    protected double getLatestAccessTime() {
        return _latestAccessTime;
    }
}
