package gecko.geckocircuits.circuit;

import gecko.geckocircuits.allg.FormatJTextField;
import java.awt.Component;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

class MyTableCellEditor extends AbstractCellEditor implements TableCellEditor {

    FormatJTextField component = new FormatJTextField();

  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
      int rowIndex, int vColIndex) {      
      if(value != null) {
          component.setNumberToField((Double) value);
      } else {
          component.setText("");
      }
        
    return component;
  }

  public Object getCellEditorValue() {
      if(component.getText().isEmpty()) {
          return null;
      }
    return component.getNumberFromField();
  }
}
    
