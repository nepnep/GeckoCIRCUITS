package gecko.geckocircuits.control.calculators;

import gecko.geckocircuits.circuit.ControlSourceType;
import static gecko.geckocircuits.control.calculators.AbstractControlCalculatable._time;
import static gecko.geckocircuits.control.calculators.AbstractSignalCalculator.TWO_PI;
import gecko.geckocircuits.newscope.Cispr16Fft;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class SmallSignalCalculator extends AbstractControlCalculatable {

    //static boolean isSimulationDC;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int NOFREQSMAX = 50;
    
    /* 
     * bode[0] = Simulationsfrequenzen
     * bode[1] = Magnitude
     * bode[2] = Phase
     */
    public static double[][] _bode = new double[THREE][];
    
    private final ControlSourceType _signalType;
    private final double _amplitude;
    private final double _freqStart;
    private final double _freqEnd;
    private final double _tPre;
    private final double _deltaT;
    
    private final int _nMin = 0;
    private final int _nMax;
    private final List<Float> _valueData;
    private final List<Float> _smallSignalData;
    private final List<Double> _timeData;
    private final double _tEnd;
    private boolean _checker = true;
    private boolean _storeFFTDataFinished = false;
    private AbstractSignalCalculatorPeriodic _signalTypeCalculator;
    private int _noFreqs;
    private float[] saVals;
    private float[] sbVals;

    public SmallSignalCalculator(final double amplitude, final double freqLow, final double freqHigh,
            final double tPre, final double deltaT, final ControlSourceType signalType, final int noInputs, final int noOutput) {
        super(noInputs, noOutput);

        _amplitude = amplitude;
        _freqStart = freqLow;
        _freqEnd = freqHigh;
        _tPre = tPre;
        _deltaT = deltaT;
        _tEnd = 1 / freqLow;
        _signalType = signalType;

        _valueData = new ArrayList<Float>();
        _smallSignalData = new ArrayList<Float>();
        _timeData = new ArrayList<Double>();
        
        _noFreqs = NOFREQSMAX;
        calculateSimFreqs();
        _nMax = (int) Math.round(_bode[0][_bode[0].length - 1] / _freqStart);

        switch (_signalType) {
            case QUELLE_SIN:
                break;
            case QUELLE_RECHTECK:
                _signalTypeCalculator = new SignalCalculatorRectangle(1, 2 * _amplitude, _freqStart, 0, -_amplitude, 0.5);
                _signalTypeCalculator.initializeAtSimulationStart(_deltaT);
                break;
            case QUELLE_DREIECK:
                _signalTypeCalculator = new SignalCalculatorTriangle(1, _amplitude, _freqStart, 0, 0, 0.5);
                _signalTypeCalculator.initializeAtSimulationStart(_deltaT);
                break;
            default:
                assert false;
        }
    }

    @Override
    public void berechneYOUT(final double deltaT) {
        calculateSmallSignal(deltaT);
        if (_time >= _tPre) {
            storeFFTData();
        }
        if (_storeFFTDataFinished && _checker) {
             
            float[][][] erg;

            try {
                erg = calculateFourier();
            } catch (java.lang.OutOfMemoryError er) {
                throw new RuntimeException("Could not allocate enough memory for Fourier transformation!");
            }
            calculateBode(erg);

            // Provisorisch: Bode-Diagramm wird in ein Textfile geschrieben.
            try {
                printResults("TestBode.txt");
            } catch (Exception e) {
                System.out.println("Fehler aufgetreten!");
            }

            _checker = false;

        }
    }

    private void calculateSimFreqs() {
        // Calculation of possible simulation frequencies
        int noPossibleFreqs = 0;
        switch (_signalType) {
            case QUELLE_SIN:
                noPossibleFreqs = (int) Math.floor(_freqEnd / _freqStart);
                break;
            case QUELLE_RECHTECK:
                noPossibleFreqs = (int) Math.ceil((Math.floor(_freqEnd / _freqStart)) / 2);
                break;
            case QUELLE_DREIECK:
                noPossibleFreqs = (int) Math.ceil((Math.floor(_freqEnd / _freqStart)) / 2);
                break;
            default:
                assert false;
        }

        double[] possibleSimFreqs = new double[noPossibleFreqs];
        for (int i = 0; i < noPossibleFreqs; i++) {
            switch (_signalType) {
                case QUELLE_SIN:
                    possibleSimFreqs[i] = _freqStart * (i + 1);
                    break;
                case QUELLE_RECHTECK:
                    possibleSimFreqs[i] = _freqStart * (2 * i + 1);
                    break;
                case QUELLE_DREIECK:
                    possibleSimFreqs[i] = _freqStart * (2 * i + 1);
                    break;
                default:
                    assert false;
            }
        }

        // Calculation of logarithmically spaced index
        int[] index1 = new int[_noFreqs];
        for (int i = 0; i < index1.length; i++) {
            index1[i] = (int) Math.pow(noPossibleFreqs, ((double) i / (_noFreqs - 1))) - 1;
        }

        // Make sure that no index appears more than once
        int[] index2 = removeDuplicates(index1, noPossibleFreqs);

        // Calculation of logarithmically spaced simulation frequencies
        _noFreqs = index2.length;
        _bode[0] = new double[_noFreqs];
        for (int i = 0; i < _bode[0].length; i++) {
            _bode[0][i] = possibleSimFreqs[index2[i]];
        }
    }

    private void calculateSmallSignal(final double deltaT) {
        switch (_signalType) {
            case QUELLE_SIN:
                double output = 0;
                for (int i = 0; i < _bode[0].length; i++) {
                    output += _amplitude * Math.cos(TWO_PI * _bode[0][i] * _time);
                }
                _outputSignal[0][0] = output;
                break;
            case QUELLE_RECHTECK:
                _signalTypeCalculator.berechneYOUT(deltaT);
                _outputSignal[0][0] = _signalTypeCalculator._outputSignal[0][0];
                break;
            case QUELLE_DREIECK:
                _signalTypeCalculator.berechneYOUT(deltaT);
                _outputSignal[0][0] = _signalTypeCalculator._outputSignal[0][0];
                break;
            default:
                assert false;
        }
        _smallSignalData.add((float) _outputSignal[0][0]);
        
    }

    private void storeFFTData() {
        _valueData.add((float) _inputSignal[0][0]);
        _timeData.add(_time);
        
        // Abbruchbedingung erfüllt, wenn letzter Simulationspunkt vor dem Ende erreicht ist.
        if ((_time + _deltaT) > (_tEnd + _tPre)) {
            _storeFFTDataFinished = true;
        }
    }

    /* [coefficient][channel][value for nth harmonic]
     *  coefficient = 0 --> a_n
     *  coefficient = 1 --> b_n
     *  coefficient = 2 --> c_n
     *  coefficient = 3 --> phi_n
     */
    private float[][][] calculateFourier() {
        final float[][] anVals = new float[1][_nMax - _nMin + 1];
        final float[][] bnVals = new float[1][_nMax - _nMin + 1];
        final float[][] cnVals = new float[1][_nMax - _nMin + 1];  // Amplitude
        final float[][] jnVals = new float[1][_nMax - _nMin + 1];  // Winkel [rad]

        saVals = new float[_nMax - _nMin + 1];
        sbVals = new float[_nMax - _nMin + 1];
        
        final int startIndex = findStartIndex();
        final int stopIndex = findStopIndex(startIndex);

        // Maybe wrong in FourierGUIless. numberOfSamples should be equal to _dataFFT.size()
        final int numberOfSamples = stopIndex - startIndex;

        int nValues = 1;
        while (nValues < numberOfSamples) {
            nValues *= 2;
        }

        if (nValues > numberOfSamples) {
            nValues /= 2;
        }

        final float stopTime = (float) getTimeValue(stopIndex);
        final float startTime = (float) getTimeValue(startIndex);

        final double timeSpan = stopTime - startTime;

        float[] data = new float[nValues];
        float[] smallSignalData = new float[nValues];

        int jjj = startIndex;
        for (int i = 0; i < nValues; i++) {
            while (getTimeValue(jjj) < startTime + i * timeSpan / nValues) {
                jjj++;
            }
            data[i] = (float) getValue(jjj);
            smallSignalData[i] = (float) _smallSignalData.get(jjj);
        }

        Cispr16Fft.realft(data, 1);
        Cispr16Fft.realft(smallSignalData, 1);
        
        for (int n = _nMin; n <= _nMax; n++) {
            anVals[0][n - _nMin] = 2 * data[2 * n] / nValues;
            bnVals[0][n - _nMin] = 2 * data[2 * n + 1] / nValues;
            saVals[n - _nMin] = 2 * smallSignalData[2 * n] / nValues;
            sbVals[n - _nMin] = 2 * smallSignalData[2 * n+1] / nValues;
        }

        return evaluate(anVals, bnVals, cnVals, jnVals);

    }

    private float[][][] evaluate(final float[][] anVals, final float[][] bnVals,
            final float[][] cnVals, final float[][] jnVals) {

        for (int n = _nMin; n <= _nMax; n++) {

            if (n == 0) {  // DC-Gleichanteil
                cnVals[0][n - _nMin] = anVals[0][n - _nMin] / 2.0f;
                jnVals[0][n - _nMin] = 0;
            } else {
                cnVals[0][n - _nMin] = (float) Math.sqrt(anVals[0][n - _nMin] * anVals[0][n - _nMin]
                        + bnVals[0][n - _nMin] * bnVals[0][n - _nMin]);
                jnVals[0][n - _nMin] = (float) Math.atan2(bnVals[0][n - _nMin], anVals[0][n - _nMin]);
            }
        }


        float[][][] erg = new float[FOUR][][];
        int index = 0;
        erg[0] = anVals;
        erg[++index] = bnVals;
        erg[++index] = cnVals;
        erg[++index] = jnVals;                
        return erg;
    }

    private void calculateBode(final float[][][] fourierCoeffs) {        

        int harmonic;
        _bode[1] = new double[_bode[0].length];
        _bode[2] = new double[_bode[0].length];
        for (int p = 0; p < _bode[0].length; p++) {
            harmonic = (int) Math.round(_bode[0][p] / _freqStart);
            double ampl = 0;
            double phase = 0;

            switch (_signalType) {
                case QUELLE_SIN:
                    ampl = _amplitude;
                    break;
                case QUELLE_RECHTECK:
                    ampl = 4 / Math.PI * _amplitude * 1 / harmonic;                    
                    
                    break;
                case QUELLE_DREIECK:
                    ampl = 8 / Math.pow(Math.PI, 2) * _amplitude * 1 / Math.pow(harmonic, 2);                    
                    // Jede zweite ungerade Harmonische hat eine Anfangsphase von 180°
                    if((harmonic % 4) == 3){
                         phase = Math.PI;
                    }                                           
                    break;
                default:
                    assert false;
            }
            
            _bode[1][p] = fourierCoeffs[2][0][harmonic] / ampl;
            _bode[2][p] = fourierCoeffs[3][0][harmonic] - phase;
            
            
            float amplNew = (float) Math.sqrt(sbVals[harmonic] * sbVals[harmonic] + saVals[harmonic] * saVals[harmonic]);
            
            _bode[1][p] = fourierCoeffs[2][0][harmonic] / amplNew;
                        
            double a = fourierCoeffs[0][0][harmonic];
            double b = fourierCoeffs[1][0][harmonic];
            double c = saVals[harmonic];
            double d = sbVals[harmonic];
            
            double x = (a*c+b*d) / (c*c-d*d);
            double y = (c*b-a*d) / (c*c-d*d);            
            _bode[2][p] = Math.atan2(y, x);  
            //System.out.println("compare " + saVals[harmonic] + " " + sbVals[harmonic] + " " + fourierCoeffs[0][0][harmonic] + " " + fourierCoeffs[1][0][harmonic] + " " + x + " " + y);
        }
        
    }

    public static void main(String[] args) {
        System.out.println("atan "  + Math.atan2(0, 1));
    }
    
    private void printResults(final String path) throws Exception {
        PrintWriter writer = new PrintWriter(new FileWriter(path));
        if (writer != null) {
            for (int i = 0; i < _bode[0].length; i++) {
                writer.print(_bode[0][i]);
                writer.print(" ");
                writer.print(_bode[1][i]);
                writer.print(" ");
                writer.print(_bode[2][i]);
                writer.println();
            }
        }
        if (writer != null) {
            writer.close();
        }
    }

    private int findStartIndex() {
        int index1 = 0; // Startpunkt finden:
        while (getTimeValue(index1) <= _tPre) {
            index1++;
        }
        return index1;
    }

    private int findStopIndex(final int startIndex) {
        int index1 = startIndex;
        int returnValue = 0;
        while ((index1 < getMaximumTimeIndex())
                && (getTimeValue(index1 + 1) > getTimeValue(index1))
                && (_tPre <= getTimeValue(index1))
                && (getTimeValue(index1) <= (_tPre + _tEnd))) {  // Schleife Zeitbereich [t1...t2]
            returnValue = index1;
            index1++;
        }
        return returnValue;
    }

    private double getValue(final int index) {
        return _valueData.get(index).doubleValue();
    }

    private double getTimeValue(final int index) {
        return _timeData.get(index).doubleValue();
    }

    private int getMaximumTimeIndex() {
        return _valueData.size() - 1;
    }
    /*
    public static void setEnabledDC(final boolean enabled) {
        isSimulationDC = enabled;
    }

    
    public static boolean getEnabledDC() {
        return isSimulationDC;
    }
    */
    private int[] removeDuplicates(final int[] arr, final int noValues) {
        boolean[] set = new boolean[noValues]; //values must default to false
        int totalItems = 0;

        for (int i = 0; i < arr.length; ++i) {
            if (set[arr[i]] == false) {
                set[arr[i]] = true;
                totalItems++;
            }
        }

        int[] ret = new int[totalItems];
        int c = 0;
        for (int i = 0; i < set.length; ++i) {
            if (set[i] == true) {
                ret[c++] = i;
            }
        }
        return ret;
    }

    /*
    public static double[][] getMagnitude() {
        double[][] ch = new double[2][];

        ch[0] = _bode[0];
        ch[1] = _bode[1];

        return ch;
    }
    */ 
}
