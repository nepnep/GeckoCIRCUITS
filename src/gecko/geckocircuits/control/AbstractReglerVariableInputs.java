/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuits.control;

import java.awt.Window;

/**
 * For some components, it makes sense to have a variable input number, e.g. adding, logic-or, multiplication...
 * @author andreas
 */
public abstract class AbstractReglerVariableInputs extends RegelBlock implements VariableTerminalNumber {

    public AbstractReglerVariableInputs(final int defaultInputs) {
        super(defaultInputs, 1);        
    }
    
    @Override
    public final void setInputTerminalNumber(final int number) {
        super.setInputTerminalNumber(number);
    }

    @Override
    public final void setOutputTerminalNumber(final int number) {
        setOutputTerminalNumber(number, 1);
    }
    
    @Override
    protected Window openDialogWindow() {        
        return new DialogReglerVariableInputs(this);        
    }
        
        
}
