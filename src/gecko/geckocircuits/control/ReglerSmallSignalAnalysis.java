package gecko.geckocircuits.control;

import gecko.geckocircuits.allg.UserParameter;
import gecko.geckocircuits.circuit.ControlSourceType;
import gecko.geckocircuits.circuit.circuitcomponents.TextInfoType;
import gecko.geckocircuits.control.calculators.AbstractControlCalculatable;
import gecko.geckocircuits.control.calculators.SmallSignalCalculator;
import gecko.i18n.resources.I18nKeys;
import java.awt.Window;

public final class ReglerSmallSignalAnalysis extends AbstractReglerVariableInputs {
    
    private static final double DEFAULT_AMPLITUDE = 0.005;
    private static final double DEFAULT_FREQ_START = 50.0;
    private static final double DEFAULT_FREQ_END = 10000.0;
    private static final double DEFAULT_TPRE = 50e-3;
    private static final double DEFAULT_DELTAT = 1e-7;
    
    public static final ControlTypeInfo TYPE_INFO = 
            new ControlTypeInfo(ReglerSmallSignalAnalysis.class, "ANALYSIS", I18nKeys.SMALL_SIGNAL_ANALYIS);
    
    final UserParameter<Double> _amplitude = UserParameter.Builder.<Double>start("Amplitude", DEFAULT_AMPLITUDE).
            longName(I18nKeys.AMPLITUDE).
            shortName("Amplitude").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<Double> _freqLow = UserParameter.Builder.<Double>start("StartFreq", DEFAULT_FREQ_START).
            longName(I18nKeys.FREQ_START).
            shortName("LowFreq").
            unit("Hz").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<Double> _freqHigh = UserParameter.Builder.<Double>start("EndFreq", DEFAULT_FREQ_END).
            longName(I18nKeys.FREQ_END).
            shortName("HighFreq").
            unit("Hz").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<Double> _tPre = UserParameter.Builder.<Double>start("tPre", DEFAULT_TPRE).
            longName(I18nKeys.TPRE_SSA).
            shortName("tPre").
            unit("s").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<Double> _deltaT = UserParameter.Builder.<Double>start("deltaT", DEFAULT_DELTAT).
            longName(I18nKeys.DELTAT_SSA).
            shortName("deltaT").
            unit("s").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<ControlSourceType> _signalType = UserParameter.Builder.<ControlSourceType>start("signalType", ControlSourceType.QUELLE_RECHTECK).
            longName(I18nKeys.SIGNALTYPE_SSA).
            shortName("SignalType").
            showInTextInfo(TextInfoType.SHOW_WHEN_DISPLAYPARAMETERS).
            arrayIndex(this, -1).
            build();
    
    final UserParameter<Integer> _numberSweepPoints = UserParameter.Builder.<Integer>start("numberPoints", 10).
            longName(I18nKeys.DELTAT_SSA).
            shortName("deltaT").
            unit("s").
            showInTextInfo(TextInfoType.SHOW_NEVER).
            arrayIndex(this, -1).
            build();
    
    
    public ReglerSmallSignalAnalysis() {
        super(1);
    }
    
    @Override
    // Is called if SSA block is on worksheet and simulation is initiated and running
    public AbstractControlCalculatable getInternalControlCalculatableForSimulationStart() {
        return new SmallSignalCalculator(_amplitude.getValue(), _freqLow.getValue(), 
                _freqHigh.getValue(), _tPre.getValue(), _deltaT.getValue(), _signalType.getValue(), 1, 1);
    }
    
    @Override
    protected String getCenteredDrawString() {
        return "SSA";
    }                

    @Override
    public String[] getOutputNames() {
        return new String[]{"SmallSignal"};
    }

    @Override
    public I18nKeys[] getOutputDescription() {
        return new I18nKeys[] {I18nKeys.SMALL_SIGNAL_OUTPUT_FOR_ANALYSIS};
    }

    @Override
    // Is called if one drags the SSA block on the worksheet
    protected Window openDialogWindow() {
        return new DialogSmallSignalAnalysis(this);
    }
    
}