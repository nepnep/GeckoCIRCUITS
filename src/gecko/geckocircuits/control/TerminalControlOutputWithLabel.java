package gecko.geckocircuits.control;

import gecko.geckocircuits.circuit.AbstractBlockInterface;
import gecko.geckocircuits.circuit.TerminalControlOutput;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

class TerminalControlOutputWithLabel extends TerminalControlOutput {
    private final String _magentaLabel;

    public TerminalControlOutputWithLabel(AbstractBlockInterface parentComponent, int posX, int posY, String label) {
        super(parentComponent, posX, posY);
        _magentaLabel = label;
    }    

    @Override
    public void paintLabelString(final Graphics2D graphics) {
        final int dpix = AbstractBlockInterface.dpix;
        super.paintLabelString(graphics); //To change body of generated methods, choose Tools | Templates.
                
        final Font orig = graphics.getFont();
        graphics.setFont(TerminalControlInputWithLabel.getReducedSizeFont(orig));
        
        int rightAlignShift = graphics.getFontMetrics().stringWidth(_magentaLabel);
        final int xPos = (int) (dpix * getPosition().x + dpix * 0.75) - 5 - rightAlignShift + graphics.getFont().getSize() / 3;
        final int yPos = TerminalControlInputWithLabel.getYFontPosition(this, graphics);
                
        final Color origColor = graphics.getColor();
        graphics.setColor(Color.magenta);
        
        
        graphics.drawString(_magentaLabel, xPos - 20 , yPos);
        graphics.setColor(origColor);                
        graphics.setFont(orig);
    }                        
}
