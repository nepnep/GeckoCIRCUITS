package gecko.geckocircuits.control;

import gecko.geckocircuits.allg.AbstractComponentTyp;
import static gecko.geckocircuits.circuit.AbstractCircuitSheetComponent.dpix;
import gecko.geckocircuits.control.calculators.AbstractControlCalculatable;
import gecko.i18n.resources.I18nKeys;
import java.awt.Window;

public class ReglerControlDebug extends RegelBlock {
    public static final ControlTypeInfo tinfo = new ControlTypeInfo(ReglerControlDebug.class,"CTRL_DEBUG", I18nKeys.DEBUGGING_STEP);
    private ControlDebugWindow _debugWindow;
    
    public ReglerControlDebug() {
        super(0, 0);        
        XIN.add(new TerminalControlInputWithLabel(this, -4, 0, "1"));
        XIN.add(new TerminalControlInputWithLabel(this, -4, -1, "2"));
        XIN.add(new TerminalControlInputWithLabel(this, -4, -2, "3"));
        XIN.add(new TerminalControlInputWithLabel(this, -4, -3, "Trig"));
    }

    @Override
    public AbstractControlCalculatable getInternalControlCalculatableForSimulationStart() {
        return new AbstractControlCalculatable(0, 0) {
            @Override
            public void berechneYOUT(double deltaT) {
                
            }
        };
    }

    @Override
    public String[] getOutputNames() {
        return new String[0];
    }

    @Override
    public I18nKeys[] getOutputDescription() {
        return new I18nKeys[0];
    }

    @Override
    protected Window openDialogWindow() {
        if(_debugWindow == null) {
            _debugWindow = new ControlDebugWindow();
        }        
        _debugWindow.setVisible(true);
        _debugWindow.toFront();
        return _debugWindow;
    }
    
    
    
    @Override
    public int getBlockHeight() {
        return dpix * 4;
    }

    @Override
    public int getBlockWidth() {
        return dpix * 6;
    }       
}
