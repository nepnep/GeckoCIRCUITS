/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuits.control;

import gecko.geckocircuits.circuit.ComponentCoupling;
import gecko.geckocircuits.circuit.ConnectorType;
import gecko.geckocircuits.circuit.Enabled;
import gecko.geckocircuits.circuit.PotentialCoupling;
import gecko.geckocircuits.circuit.SchematischeEingabe2;
import gecko.geckocircuits.circuit.circuitcomponents.AbstractCircuitBlockInterface;
import gecko.geckocircuits.control.calculators.AbstractControlCalculatable;
import gecko.geckocircuits.control.calculators.NothingToDoCalculator;
import java.awt.Window;

/**
 *
 * @author andreas
 */
public abstract class AbstractPotentialMeasurement extends RegelBlock implements gecko.geckocircuits.circuit.PotentialCoupable,
        gecko.geckocircuits.circuit.ComponentCoupable {

    final PotentialCoupling _coupling;
    final ComponentCoupling _componentCoupling = new ComponentCoupling(1, this, new int[]{2});

    public AbstractPotentialMeasurement(final ConnectorType connectorType) {
        super(0, 1);
        _coupling = new PotentialCoupling(this, new int[]{0, 1}, connectorType);
    }

    @Override
    public final ComponentCoupling getComponentCoupling() {
        return _componentCoupling;
    }

    @Override
    public final PotentialCoupling getPotentialCoupling() {
        return _coupling;
    }    

    @Override
    public AbstractControlCalculatable getInternalControlCalculatableForSimulationStart() {        
        return new NothingToDoCalculator(0, 1);
    }

    @Override
    protected final void addTextInfoParameters() {
        super.addTextInfoParameters();
        
        final AbstractCircuitBlockInterface coupledElement = (AbstractCircuitBlockInterface) getComponentCoupling()._coupledElements[0];
        final String label1 = getPotentialCoupling().getLabels()[0];
        final String label2 = getPotentialCoupling().getLabels()[1];
        
        if (SchematischeEingabe2._controlDisplayMode.showParameter) {
            String parStr = label1 + " @ " + label2;
            if ((label1.isEmpty() || label2.isEmpty())
                    && (coupledElement == null)) {
                parStr = "not defined";
                _textInfo.addErrorValue(parStr);
            } else {
                if (coupledElement != null) {
                    parStr = coupledElement.getStringID();
                }
                _textInfo.addParameter(parStr);
            }
        }
    }

    @Override
    protected final Window openDialogWindow() {
        return new ReglerVOLTDialog(this);
    }
}
