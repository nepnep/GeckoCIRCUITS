package gecko.geckocircuits.control;

import gecko.geckocircuits.newscope.BodePlot;
import gecko.GeckoExternal;
import gecko.geckocircuits.circuit.ControlSourceType;
import gecko.geckocircuits.control.calculators.SmallSignalCalculator;
import gecko.i18n.GuiFabric;
import gecko.i18n.resources.I18nKeys;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.JButton;
//import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public final class DialogSmallSignalAnalysis extends DialogElementCONTROL<ReglerSmallSignalAnalysis> {
    private JSpinner _spinnerInputNumber;
    private static final int NORTH_GRID_SIZE = 3;
    private JButton _jButtonCalculate;
    private JButton _jButtonAbort;
    private JButton _jButtonShowResult;
    //private JCheckBox _jCheckAnalysisDC;
    private JComboBox _jComboSignal;
    //public static final SolverSettings _solverSettings = new SolverSettings();
    private Thread _calculationThread;
    //private int _noSimulations;
    //private boolean isDCEnabled;
    
    public DialogSmallSignalAnalysis(final ReglerSmallSignalAnalysis parent) {
        super(parent);
    }    
        
    @Override
    void baueGuiIndividual() {
        SpinnerModel spinnerModel = new javax.swing.SpinnerNumberModel(Integer.valueOf(element.XIN.size()),
                Integer.valueOf(1), null, Integer.valueOf(1));
        _spinnerInputNumber = new JSpinner(spinnerModel);
        JLabel jLabNoInputs = new JLabel("Number of inputs:");
        jPanelName.add(jLabNoInputs);
        jPanelName.add(_spinnerInputNumber);
        jpM.setLayout(new GridLayout(6, 2));

        final JPanel pAmplitude = createParameterPanel(element._amplitude);
        jpM.add(pAmplitude);

        final JPanel pFreqLow = createParameterPanel(element._freqLow);
        jpM.add(pFreqLow, BorderLayout.CENTER);

        final JPanel pFreqHigh = createParameterPanel(element._freqHigh);
        jpM.add(pFreqHigh, BorderLayout.CENTER);

        final JPanel pTpre = createParameterPanel(element._tPre);
        jpM.add(pTpre, BorderLayout.CENTER);

        final JPanel pDeltaT = createParameterPanel(element._deltaT);
        jpM.add(pDeltaT, BorderLayout.CENTER);
        
        final JPanel pSignalType = new JPanel();
        final ControlSourceType[] signalType = new ControlSourceType[3];
        signalType[0] = ControlSourceType.QUELLE_SIN;
        signalType[1] = ControlSourceType.QUELLE_RECHTECK;
        signalType[2] = ControlSourceType.QUELLE_DREIECK;
        _jComboSignal = new JComboBox(signalType);
        _jComboSignal.setSelectedItem(element._signalType.getValue());
        _jComboSignal.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(final ActionEvent actionEvent){
                if(_jComboSignal.getSelectedItem().equals(signalType[0])){
                    element._signalType.setUserValue(ControlSourceType.QUELLE_SIN);
                }
                if(_jComboSignal.getSelectedItem().equals(signalType[1])){
                    element._signalType.setUserValue(ControlSourceType.QUELLE_RECHTECK);
                }
                if(_jComboSignal.getSelectedItem().equals(signalType[2])){
                    element._signalType.setUserValue(ControlSourceType.QUELLE_DREIECK);
                }
            }
        
        });
        pSignalType.add(_jComboSignal);
        jpM.add(pSignalType, BorderLayout.SOUTH);
        
        /*
        final JPanel boxAnalysisDC = new JPanel();
        _jCheckAnalysisDC = new JCheckBox("DC Analysis");
        _jCheckAnalysisDC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent actionEvent) {
                if (_jCheckAnalysisDC.isSelected()) {
                    isDCEnabled = true;                   
                } else {
                    isDCEnabled = false;
                }
            }
        });
        boxAnalysisDC.add(_jCheckAnalysisDC);
        jpM.add(boxAnalysisDC, BorderLayout.SOUTH);
        */
        
        final JPanel buttonCalculatePanel = new JPanel();
        _jButtonCalculate = GuiFabric.getJButton(I18nKeys.CALCULATE);
        _jButtonCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent actionEvent) {
                _calculationThread = new Thread() {
                        @Override
                        public void run() {
                            _jButtonCalculate.setEnabled(false);
                            _jButtonShowResult.setEnabled(false);
                            _jButtonAbort.setEnabled(true);
                            runCalculation();
                            _jButtonShowResult.setEnabled(true);
                            _jButtonCalculate.setEnabled(true);
                            _jButtonAbort.setEnabled(false);
                        }
                    };
                    _calculationThread.setPriority(Thread.MIN_PRIORITY);
                    _calculationThread.start();
                
                /*
                if(isDCEnabled){
                    for(int i=0;i<2;i++){
                        if(i==0){
                            SmallSignalCalculator.setEnabledDC(true);
                        calculationThread = new Thread() {
                        @Override
                        public void run() {
                            _jButtonCalculate.setEnabled(false);
                            _jButtonAbort.setEnabled(true);
                            runCalculation();
                            _jButtonCalculate.setEnabled(true);
                            _jButtonAbort.setEnabled(false);
                        }
                    };
                    calculationThread.setPriority(Thread.MIN_PRIORITY);
                    calculationThread.start();
                        }
                        if(i==1){
                            SmallSignalCalculator.setEnabledDC(false);
                        calculationThread = new Thread() {
                        @Override
                        public void run() {
                            _jButtonCalculate.setEnabled(false);
                            _jButtonAbort.setEnabled(true);
                            runCalculation();
                            _jButtonCalculate.setEnabled(true);
                            _jButtonAbort.setEnabled(false);
                        }
                    };
                    calculationThread.setPriority(Thread.MIN_PRIORITY);
                    calculationThread.start();
                        }
                    
                    }
                    
                
                } else {
                    
                    calculationThread = new Thread() {
                        @Override
                        public void run() {
                            _jButtonCalculate.setEnabled(false);
                            _jButtonAbort.setEnabled(true);
                            runCalculation();
                            _jButtonCalculate.setEnabled(true);
                            _jButtonAbort.setEnabled(false);
                        }
                    };
                    calculationThread.setPriority(Thread.MIN_PRIORITY);
                    calculationThread.start();
                }
                */
                
            }
        });
        buttonCalculatePanel.add(_jButtonCalculate);
        jpM.add(buttonCalculatePanel, BorderLayout.EAST);

        final JPanel buttonAbortPanel = new JPanel();
        _jButtonAbort = GuiFabric.getJButton(I18nKeys.ABORT_SIMULATION);
        _jButtonAbort.setEnabled(false);
        _jButtonAbort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent actionEvent) {
                if (_calculationThread != null && _calculationThread.isAlive()) {
                    _calculationThread.stop();
                }
            }
        });
        buttonAbortPanel.add(_jButtonAbort);
        jpM.add(buttonAbortPanel, BorderLayout.EAST);
        
        final JPanel buttonShowResultPanel = new JPanel();
        _jButtonShowResult = GuiFabric.getJButton(I18nKeys.SHOW_RESULT);
        _jButtonShowResult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent actionEvent) {
                DialogSmallSignalAnalysis.this.setVisible(false);
                BodePlot bp = new BodePlot();
                bp.insertData(SmallSignalCalculator._bode);
                bp.setVisible(true);
                /*
                DialogSSAPlot dialog = new DialogSSAPlot(SmallSignalCalculator.getMagnitude());
                dialog.setVisible(true);
                */
            }
        });
        buttonShowResultPanel.add(_jButtonShowResult);
        jpM.add(buttonShowResultPanel, BorderLayout.EAST);
        
    }
        
    @Override
    protected void processInputs() {
        ((AbstractReglerVariableInputs) element).setInputTerminalNumber((Integer) _spinnerInputNumber.getValue());
    }
    
    @Override
    public int getRequiredGridSize() {
        return NORTH_GRID_SIZE;
    }
    
    private void runCalculation() {
        //GeckoExternal.set_dt_pre(1e-6);
        GeckoExternal.set_dt(element._deltaT.getValue());
        GeckoExternal.set_dt_pre(element._deltaT.getValue());
        GeckoExternal.set_Tend_pre(element._tPre.getValue());
        GeckoExternal.set_Tend(1 / element._freqLow.getValue());
        GeckoExternal.runSimulation();
    }
    
}