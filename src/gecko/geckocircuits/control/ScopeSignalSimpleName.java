package gecko.geckocircuits.control;

import gecko.geckocircuits.newscope.AbstractScopeSignal;

public class ScopeSignalSimpleName extends AbstractScopeSignal {
    private final String _name;

    public ScopeSignalSimpleName(final String name) {
        _name = name;
    }

    @Override
    public String getSignalName() {
        return _name;
    }
    
}
