/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuits.control;

import gecko.geckocircuits.allg.AbstractComponentTyp;
import gecko.geckocircuits.control.calculators.InitializableAtSimulationStart;
import gecko.geckocircuits.circuit.AbstractBlockInterface;
import gecko.geckocircuits.circuit.TokenMap;
import gecko.geckocircuits.control.StateSpaceCalculator.StateVariables;
import gecko.geckocircuits.control.calculators.AbstractControlCalculatable;
import gecko.i18n.resources.I18nKeys;
import java.awt.Window;
import java.util.List;

public final class ReglerTransferFunction extends AbstractReglerSingleInputSingleOutput {
    public static final ControlTypeInfo tinfo = new ControlTypeInfo(ReglerTransferFunction.class,"TF", I18nKeys.TRANSFER_FUNKTION_H_S, I18nKeys.DEFINES_A_TRANSFER_FUNCTION);

    private double[] _zeros = new double[MAX_ARRAY_SIZE];
    private double[] _poles = new double[MAX_ARRAY_SIZE];
    private boolean _inPolynomMode = false;
    private double[] _numeratorPolynom = new double[ReglerTransferFunction.MAX_ARRAY_SIZE];
    private double[] _denomPolynom = new double[ReglerTransferFunction.MAX_ARRAY_SIZE];
    private boolean _useInitialState = false;
    private double _constantFactor = 1;
    private StateSpaceCalculator _stateSpaceCalc;
    public static final int MAX_ARRAY_SIZE = 20;
    private StateVariables _savedState;
    private DialogTransferFunction _dialog;

    public ReglerTransferFunction() {
        super();

        _numeratorPolynom[0] = 1;
        _denomPolynom[0] = 1;
        _zeros[0] = 1;
        _poles[0] = 2;
    }

    @Override
    public String[] getOutputNames() {
        return new String[]{"y"};
    }

    @Override
    public I18nKeys[] getOutputDescription() {
        return new I18nKeys[]{I18nKeys.TRANSFER_FUNCTION_OUTPUT_SIGNAL};
    }
    
    public boolean isInPolynomialMode() {
        return _inPolynomMode;
    }

    public void setInPolynomialMode(final boolean value) {
        _inPolynomMode = value;
    }

    public boolean isUseInitialState() {
        return _useInitialState;
    }

    public void setUseInitialState(final boolean useInitState) {
        _useInitialState = useInitState;
    }

    private class TransferFunctionCalculator extends AbstractControlCalculatable implements InitializableAtSimulationStart, IsDtChangeSensitive {

        public TransferFunctionCalculator() {
            super(1, 1);
        }

        @Override
        public void initializeAtSimulationStart(final double deltaT) {
            _stateSpaceCalc = new StateSpaceCalculator(deltaT, _numeratorPolynom, _denomPolynom);
            if (_useInitialState && _savedState != null) {
                _stateSpaceCalc.setInitialState(_savedState);
            }
        }

        @Override
        public void berechneYOUT(final double deltaT) {
            _stateSpaceCalc.calculateTimeStep(_inputSignal, deltaT, _outputSignal, _time);
        }

        @Override
        public void initWithNewDt(final double dt) {
            _stateSpaceCalc.initializeWithNewDt(dt);
        }
    }

    @Override
    public AbstractControlCalculatable getInternalControlCalculatableForSimulationStart() {
        return new TransferFunctionCalculator();
    }

    @Override
    protected String getCenteredDrawString() {
        return "H(s)";
    }

    @Override
    protected void exportAsciiIndividual(final StringBuffer ascii) {
        ascii.append("\nnominatorPoles ");
        for (int i = 0; i < _poles.length; i++) {
            ascii.append(_poles[i]);
            ascii.append(' ');
        }

        ascii.append("\ndenominatorZeros ");
        for (int i = 0; i < _zeros.length; i++) {
            ascii.append(_zeros[i]);
            ascii.append(' ');
        }

        ascii.append("\nnominatorPolynom ");
        for (int i = 0; i < _numeratorPolynom.length; i++) {
            ascii.append(_numeratorPolynom[i]);
            ascii.append(' ');
        }

        ascii.append("\ndenominatorPolynom ");
        for (int i = 0; i < _denomPolynom.length; i++) {
            ascii.append(_denomPolynom[i]);
            ascii.append(' ');
        }

        if (_savedState != null) {
            _savedState.exportIndividualCONTROL(ascii);
        }

        ascii.append("\npolynomMode ");
        ascii.append(_inPolynomMode);

        ascii.append("\nconstant ");
        ascii.append(_constantFactor);

        ascii.append("\nuseInitState ");
        ascii.append(_useInitialState);

    }

    public double[] getPoles() {
        final double[] returnValue = new double[_poles.length];
        System.arraycopy(_poles, 0, returnValue, 0, _poles.length);
        return returnValue;
    }

    public void setPole(final double value, final int index) {
        _poles[index] = value;
    }

    public double[] getZeros() {
        final double[] returnValue = new double[_zeros.length];
        System.arraycopy(_zeros, 0, returnValue, 0, _zeros.length);
        return returnValue;
    }

    public void setConstant(final double value) {
        _constantFactor = value;
    }

    public double getConstant() {
        return _constantFactor;
    }

    public void setZero(final double value, final int index) {
        _zeros[index] = value;
    }

    public void setNumeratorPolynom(final List<Double> values) {
        for (int i = 0; i < _numeratorPolynom.length; i++) {
            _numeratorPolynom[i] = 0;
        }

        for (int i = 0; i < Math.min(values.size(), _numeratorPolynom.length); i++) {
            _numeratorPolynom[i] = values.get(i);
        }
    }

    public void setDeNominatorPolynom(final List<Double> values) {
        for (int i = 0; i < _denomPolynom.length; i++) {
            _denomPolynom[i] = 0;
        }

        for (int i = 0; i < Math.min(values.size(), _denomPolynom.length); i++) {
            _denomPolynom[i] = values.get(i);
        }
    }

    public double getNumeratorCoefficient(final int index) {
        return _numeratorPolynom[index];
    }

    public int getNumeratorSize() {
        return PolynomTools.getMaxPolynomialDegree(_numeratorPolynom) + 1;
    }

    public double getDenominatorCoefficients(final int index) {
        return _denomPolynom[index];
    }

    public int getDenominatorSize() {
        return PolynomTools.getMaxPolynomialDegree(_denomPolynom) + 1;
    }

    @Override
    public void copyAdditionalParameters(final AbstractBlockInterface originalBlock) {
        super.copyAdditionalParameters(originalBlock);
        final ReglerTransferFunction other = (ReglerTransferFunction) originalBlock;

        this._poles = new double[other._poles.length];
        System.arraycopy(other._poles, 0, this._poles, 0, _poles.length);

        this._zeros = new double[other._zeros.length];
        System.arraycopy(other._zeros, 0, this._zeros, 0, _zeros.length);

        this._numeratorPolynom = new double[_numeratorPolynom.length];
        System.arraycopy(other._numeratorPolynom, 0, this._numeratorPolynom, 0, _numeratorPolynom.length);

        this._denomPolynom = new double[_denomPolynom.length];
        System.arraycopy(other._denomPolynom, 0, this._denomPolynom, 0, _denomPolynom.length);

        this._constantFactor = other._constantFactor;

        this._inPolynomMode = other._inPolynomMode;
        this._useInitialState = other._useInitialState;
        this._savedState = other._savedState;

    }

    @Override
    protected void importIndividual(final TokenMap tokenMap) {

        _inPolynomMode = tokenMap.readDataLine("polynomMode", _inPolynomMode);
        _poles = tokenMap.readDataLine("nominatorPoles", _poles);
        _zeros = tokenMap.readDataLine("denominatorZeros", _zeros);
        _numeratorPolynom = tokenMap.readDataLine("nominatorPolynom", _numeratorPolynom);
        _denomPolynom = tokenMap.readDataLine("denominatorPolynom", _denomPolynom);

        _constantFactor = tokenMap.readDataLine("constant", _constantFactor);
        _useInitialState = tokenMap.readDataLine("useInitState", _useInitialState);

        if (_useInitialState) {
            _savedState = new StateVariables(tokenMap);
        }
    }

    public void clearPolesAndZeros() {
        for (int i = 0; i < _poles.length; i++) {
            _poles[i] = 0;
            setZero(0, i);
        }

        for (int i = 0; i < _denomPolynom.length; i++) {
            _denomPolynom[i] = 0;
            _numeratorPolynom[i] = 0;
        }
    }

    void saveState() {
        _savedState = _stateSpaceCalc.getStateVariables();
    }        
    

    @Override
    protected final Window openDialogWindow() {
        if (_dialog == null) {
            _dialog = new DialogTransferFunction((ReglerTransferFunction) this, this);
        }
        return _dialog;
    }
}
