package gecko.geckocircuits.control;

import gecko.geckocircuits.datacontainer.ContainerStatus;
import gecko.geckocircuits.datacontainer.DataContainerSimple;
import gecko.geckocircuits.newscope.Cispr16Fft;

class TestReceiverCalculation {                
    
    private final Cispr16Settings _settings;
    final Cispr16Fft _fftOrig;

    public TestReceiverCalculation(final DataContainerSimple dataContainer, final Cispr16Settings settings) {
        if (dataContainer == null) {
            throw new RuntimeException("Error: no data available for testreceiver calculation!");
        }

        if (dataContainer.getContainerStatus() != ContainerStatus.FINISHED) {
            throw new RuntimeException("Error: cannot start testreceiver calculation during simulation!");
        }
        
        _settings = settings;        
        _fftOrig = new Cispr16Fft(dataContainer, _settings._useBlackman.getValue());        
    }

    
    private int getIndexFromFrequency(final double frequency) {
        if (frequency > _fftOrig.baseFrequency * _fftOrig._resampledN / 2.5) {
            throw new RuntimeException("Error: frequency " + frequency + " out of range. Please Decrease your simulation stepwidth.");
        }
        return (int) Math.round(frequency / _fftOrig.baseFrequency);
    }

    double calculateQpAtFrequency(final double frequency) {
        final QuasiPeakCalculator thread = new QuasiPeakCalculator(getIndexFromFrequency(frequency), _fftOrig, _settings);
        return thread._quasiPeak;
    }

    double[] calculateAtFrequency(final double frequency) {
        final QuasiPeakCalculator thread = new QuasiPeakCalculator(getIndexFromFrequency(frequency), _fftOrig, _settings);
        return new double[]{thread._peakValue, thread._quasiPeak, thread._avgValue};
    }

    double calculateAvgAtFrequency(final double frequency) {
        QuasiPeakCalculator thread = new QuasiPeakCalculator(getIndexFromFrequency(frequency), _fftOrig, _settings);
        return thread._avgValue;
    }

    double calculatePeakAtFrequency(final double frequency) {
        final QuasiPeakCalculator thread = new QuasiPeakCalculator(getIndexFromFrequency(frequency), _fftOrig, _settings);
        return thread._peakValue;
    }
    
}
