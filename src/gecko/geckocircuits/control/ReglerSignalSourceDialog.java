/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations GmbH
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko.geckocircuits.control;

import gecko.geckocircuits.allg.Fenster;
import gecko.geckocircuits.allg.FormatJTextField;
import gecko.geckocircuits.allg.GeckoFileManagerWindow;
import gecko.geckocircuits.allg.GlobalColors;
import gecko.geckocircuits.allg.GlobalFilePathes;
import gecko.geckocircuits.allg.GlobalFonts;
import gecko.geckocircuits.circuit.ControlSourceType;
import gecko.i18n.GuiFabric;
import gecko.i18n.resources.I18nKeys;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

class ReglerSignalSourceDialog extends AbstractDialogWithExternalOption<ReglerSignalSource> {
    
    private JCheckBox jcbV;  // wieviel Info soll im SchematicEntry angezeigt werden (Uebersicht vs. Info)?
    // Unterdialoge fuer Preview-SIGNAL -->     
    private JDialog previewWindow;
    private JTabbedPane tabberSIG;
    private FormatJTextField jtfImportStatus;  // fuer das Importieren externer Zeitverlaeufe, die dann vom SIGNAL-Block ausgegeben werden    
    private boolean displayDetails = false;  // wieviel Info soll im SchematicEntry angezeigt werden (Uebersicht vs. Info)?
    private final FormatJTextField[] tfSIN = new FormatJTextField[6];
    private final FormatJTextField[] tfREC = new FormatJTextField[6];
    private final FormatJTextField[] tfTRI = new FormatJTextField[6];
    private final GridBagConstraints gbc = new GridBagConstraints();
        

    public ReglerSignalSourceDialog(final ReglerSignalSource reglerSignal) {
        super(reglerSignal);
        gbc.fill = GridBagConstraints.BOTH;
    }

    @Override
    protected void baueGuiIndividual() {
        tabberSIG = new JTabbedPane();
        String[] parTxt = new String[]{"-1", "amplMAX =  ", "f [Hz] =  ", "offset =  ", "phase [°] =  ", "duty cycle =  "};  // parTxt[0] nicht belegt
        for (int i1 = 1; i1 < 6; i1++) {
            double val = element.getParameter()[i1];
            tfSIN[i1] = new FormatJTextField();
            tfSIN[i1].setNumberToField(val);
            tfSIN[i1].addActionListener(okActionListener);
            tfSIN[i1].setColumns(NO_TF_COLS);
        }
        JPanel jpSIN = new JPanel();
        jpSIN.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Sinusoidal", TitledBorder.LEFT, TitledBorder.TOP));
        jpSIN.setLayout(new GridBagLayout());
        JLabel[] jlSIN = new JLabel[6];
        for (int i1 = 1; i1 < jlSIN.length - 1; i1++) {  // [0] nicht belegt, duty cycle [5] beim Sinus nicht verwendet
            jlSIN[i1] = new JLabel(parTxt[i1]);
            jlSIN[i1].setFont(GlobalFonts.LAB_FONT_DIALOG_1);
            jlSIN[i1].setForeground(GlobalColors.LAB_COLOR_DIALOG_1);
            gbc.gridx = 0;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpSIN.add(jlSIN[i1], gbc);
            gbc.gridx = 1;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpSIN.add(tfSIN[i1], gbc);
        }
        gbc.gridx = 0;
        gbc.gridy = jlSIN.length - 2;
        gbc.gridwidth = 3;
        jpSIN.add(new JLabel(" "), gbc);  // Platzhalter
        JButton jbutSINshow = GuiFabric.getJButton(I18nKeys.SHOW);
        jbutSINshow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                previewWindow = new PreviewDialogSine(ReglerSignalSourceDialog.this);                
                previewWindow.setVisible(true);
            }
        });
        JLabel jlSL = new JLabel("View Curve  >>  ");
        gbc.gridx = 0;
        gbc.gridy = jlSIN.length - 1;
        gbc.gridwidth = 1;
        jpSIN.add(jlSL, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlSIN.length - 1;
        gbc.gridwidth = 1;
        jpSIN.add(jbutSINshow, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlSIN.length - 0;
        gbc.gridwidth = 1;
        //----------
        JPanel jpSIN1 = new JPanel();
        jpSIN1.setLayout(new BorderLayout());
        jpSIN1.add(jpSIN, BorderLayout.CENTER);
        tabberSIG.addTab("SIN", jpSIN1);
        //---------------------------------------
        for (int i1 = 1; i1 < 6; i1++) {
            double val = element.getParameter()[i1];
            tfREC[i1] = new FormatJTextField();
            tfREC[i1].setNumberToField(val);
            tfREC[i1].addActionListener(okActionListener);
            tfREC[i1].setColumns(NO_TF_COLS);
        }
        JPanel jpRECHT = new JPanel();
        jpRECHT.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Rectangular", TitledBorder.LEFT, TitledBorder.TOP));
        jpRECHT.setLayout(new GridBagLayout());
        JLabel[] jlRECHT = new JLabel[6];
        for (int i1 = 1; i1 < jlRECHT.length; i1++) {  // [0] nicht belegt
            jlRECHT[i1] = new JLabel(parTxt[i1]);
            jlRECHT[i1].setFont(GlobalFonts.LAB_FONT_DIALOG_1);
            jlRECHT[i1].setForeground(GlobalColors.LAB_COLOR_DIALOG_1);
            gbc.gridx = 0;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpRECHT.add(jlRECHT[i1], gbc);
            gbc.gridx = 1;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpRECHT.add(tfREC[i1], gbc);
        }
        JButton jbutRECHTshow = GuiFabric.getJButton(I18nKeys.SHOW);
        jbutRECHTshow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                previewWindow = new PreviewDialogRectangular(ReglerSignalSourceDialog.this);
                previewWindow.setVisible(true);
            }
        });
        
        JLabel jlSL2 = new JLabel("View Curve  >>   ");
        gbc.gridx = 0;
        gbc.gridy = jlRECHT.length - 1;
        gbc.gridwidth = 1;
        jpRECHT.add(jlSL2, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlRECHT.length - 1;
        gbc.gridwidth = 1;
        jpRECHT.add(jbutRECHTshow, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlRECHT.length - 0;
        gbc.gridwidth = 1;

        JPanel jpRECHT1 = new JPanel();
        jpRECHT1.setLayout(new BorderLayout());
        jpRECHT1.add(jpRECHT, BorderLayout.CENTER);
        tabberSIG.addTab("REC", jpRECHT1);
        //---------------------------------------
        for (int i1 = 1; i1 < 6; i1++) {
            double val = element.getParameter()[i1];
            tfTRI[i1] = new FormatJTextField();
            tfTRI[i1].setNumberToField(val);
            tfTRI[i1].addActionListener(okActionListener);
            tfTRI[i1].setColumns(NO_TF_COLS);
        }
        JPanel jpDREI = new JPanel();
        jpDREI.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Triangular", TitledBorder.LEFT, TitledBorder.TOP));
        jpDREI.setLayout(new GridBagLayout());
        JLabel[] jlDREI = new JLabel[6];
        for (int i1 = 1; i1 < jlDREI.length; i1++) {  // [0] nicht belegt
            jlDREI[i1] = new JLabel(parTxt[i1]);
            jlDREI[i1].setFont(GlobalFonts.LAB_FONT_DIALOG_1);
            jlDREI[i1].setForeground(GlobalColors.LAB_COLOR_DIALOG_1);
            gbc.gridx = 0;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpDREI.add(jlDREI[i1], gbc);
            gbc.gridx = 1;
            gbc.gridy = i1 - 1;
            gbc.gridwidth = 1;
            jpDREI.add(tfTRI[i1], gbc);
        }
        JButton jbutDREIshow = GuiFabric.getJButton(I18nKeys.SHOW);
        jbutDREIshow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                previewWindow = new PreviewDialogTriangle(ReglerSignalSourceDialog.this);                       
                previewWindow.setVisible(true);
            }
        });
        JLabel jlSL3 = new JLabel("View Curve  >>   ");
        gbc.gridx = 0;
        gbc.gridy = jlDREI.length - 1;
        gbc.gridwidth = 1;
        jpDREI.add(jlSL3, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlDREI.length - 1;
        gbc.gridwidth = 1;
        jpDREI.add(jbutDREIshow, gbc);
        gbc.gridx = 1;
        gbc.gridy = jlDREI.length - 0;
        gbc.gridwidth = 1;
        //----------
        JPanel jpDREI1 = new JPanel();
        jpDREI1.setLayout(new BorderLayout());
        jpDREI1.add(jpDREI, BorderLayout.CENTER);
        tabberSIG.addTab("TRI", jpDREI1);
        //---------------------------------------
        JPanel jpRAND = new JPanel();
        jpRAND.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Random Walk", TitledBorder.LEFT, TitledBorder.TOP));
        jpRAND.setLayout(new GridLayout(5, 2));
        
        JPanel jpRAND1 = new JPanel();
        jpRAND1.setLayout(new BorderLayout());
        jpRAND1.add(jpRAND, BorderLayout.CENTER);
        tabberSIG.addTab("RND", jpRAND1);
        //---------------------------------------
        JPanel jpIMPORT = new JPanel();
        jpIMPORT.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Import ASCII File", TitledBorder.LEFT, TitledBorder.TOP));
        jpIMPORT.setLayout(new BorderLayout());
        // 
        JTextArea jtx = new JTextArea();
        jtx.setForeground(GlobalColors.LAB_COLOR_DIALOG_1);
        jtx.setText("Data Format (Space-Separator)[ time  -  value ]");
        jtx.setLineWrap(true);
        jtx.setWrapStyleWord(true);
        jtx.setBackground(Color.white);
        jtx.setEditable(false);
        /*
         * JLabel jlIN1= new JLabel(TxtI.ti_ctrSIGd1_DialogElementCONTROL); jlIN1.setFont(TxtI.ti_Font_A);
         * jlIN1.setForeground(GlobalColors.LAB_COLOR_DIALOG_1); jpIMPORT.add(jlIN1); JLabel jlIN2= new
         * JLabel(TxtI.ti_ctrSIGd2_DialogElementCONTROL); jlIN2.setFont(TxtI.ti_Font_A);
         * jlIN2.setForeground(GlobalColors.LAB_COLOR_DIALOG_1); jpIMPORT.add(jlIN2); JLabel jlIN3= new JLabel("");
         * jlIN3.setFont(TxtI.ti_Font_A); jlIN3.setForeground(GlobalColors.LAB_COLOR_DIALOG_1); //jpIMPORT.add(jlIN3);
         */
        JButton jbImport = GuiFabric.getJButton(I18nKeys.IMPORT_DATA);
        final DialogElementCONTROL ich = this;
        final FileFilter filter = new FileFilter() {
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                if ((f.getName().endsWith(".txt")) || (f.getName().endsWith(".dat"))) {
                    return true;
                } else {
                    return false;
                }
            }

            public String getDescription() {
                return new String("Data File, Space-Spr. (*.dat, *.txt)");
            }
        };
        //-----------
        jbImport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {

                GeckoFileManagerWindow fileManager = new GeckoFileManagerWindow(element, ".dat", "Data file, space-separated", true);
                fileManager.setVisible(true);
                //------------
                String datnam = ((ReglerSignalSource) element).getDatnam();
                if (datnam == null) {
                    datnam = GlobalFilePathes.DATNAM_NOT_DEFINED;
                }
                if (datnam.equals(GlobalFilePathes.DATNAM_NOT_DEFINED)) {
                    jtfImportStatus.setText("No external data file specified");
                    jtfImportStatus.setForeground(Color.red);
                } else {
                    jtfImportStatus.setText(datnam);
                    jtfImportStatus.setForeground(Color.decode("0x006400"));
                }
                //-----------
            }
        });
        JPanel jpCheck = new JPanel();
        jpCheck.setLayout(new BorderLayout());
        jtfImportStatus = new FormatJTextField();
        jtfImportStatus.setColumns(25);
        String datnam = ((ReglerSignalSource) element).getDatnam();
        if (datnam == null) {
            datnam = GlobalFilePathes.DATNAM_NOT_DEFINED;
        }
        if (datnam.equals(GlobalFilePathes.DATNAM_NOT_DEFINED)) {
            jtfImportStatus.setText("No external data file specified");
            jtfImportStatus.setForeground(Color.red);
        } else {
            jtfImportStatus.setText(datnam);
            jtfImportStatus.setForeground(Color.decode("0x006400"));
        }
        JPanel jpSL4 = new JPanel();
        jpSL4.setLayout(new BorderLayout());
        JPanel jpSL5 = new JPanel();
        jpSL5.add(jbImport);
        jpSL4.add(jpSL5, BorderLayout.NORTH);
        jpSL4.add(jtfImportStatus, BorderLayout.CENTER);
        jpIMPORT.add(jtx, BorderLayout.CENTER);
        jpIMPORT.add(jpSL4, BorderLayout.SOUTH);
        //
        JPanel jpIMPORT1 = new JPanel();
        jpIMPORT1.setLayout(new BorderLayout());
        jpIMPORT1.add(jpIMPORT, BorderLayout.CENTER);
        tabberSIG.addTab("DAT", jpIMPORT1);
        if (Fenster.IS_APPLET) {
            tabberSIG.setEnabledAt(4, false);
        }
        switch (element._typQuelle.getValue()) {
            case QUELLE_SIN:
                tabberSIG.setSelectedIndex(0);
                break;
            case QUELLE_RECHTECK:
                tabberSIG.setSelectedIndex(1);
                break;
            case QUELLE_DREIECK:
                tabberSIG.setSelectedIndex(2);
                break;
            case QUELLE_RANDOM:
                tabberSIG.setSelectedIndex(3);
                break;
            case QUELLE_IMPORT:
                tabberSIG.setSelectedIndex(4);
                break;
            default:
                break;
        }
                                
        tabberSIG.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent evt) {
                if (tabberSIG.getSelectedIndex() < 3) {
                    _jCheckBoxUseExternal.setEnabled(true);
                } else {
                    _jCheckBoxUseExternal.setSelected(false);
                    _jCheckBoxUseExternal.setEnabled(false);
                }
            }
        });

        jcbV = new JCheckBox("Display Details");
        if (element.getParameter()[7] == 0) {  // init
            displayDetails = false;
            jcbV.setSelected(false);
        } else {
            displayDetails = true;
            jcbV.setSelected(true);
        }
        jcbV.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (jcbV.isSelected()) {
                    displayDetails = true;
                } else {
                    displayDetails = false;
                }
            }
        });
        //------------
        jpM = new JPanel();
        jpM.setLayout(new BorderLayout());
        jpM.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Source Type", TitledBorder.LEFT, TitledBorder.TOP));
        JPanel jpMM = new JPanel();
        jpMM.setLayout(new BorderLayout());
        jpMM.add(_jCheckBoxUseExternal, BorderLayout.NORTH);
        jpMM.add(jcbV, BorderLayout.CENTER);
        jpMM.add(new JLabel(" "), BorderLayout.SOUTH);  // Abstand 
        jpM.add(jpMM, BorderLayout.NORTH);
        jpM.add(tabberSIG, BorderLayout.CENTER);
    }           

    @Override
    protected void processInputs() {
        super.processInputs();
        
        switch (tabberSIG.getSelectedIndex()) {
            case 0:
                element._typQuelle.setUserValue(ControlSourceType.QUELLE_SIN);
                setValueFromTextField(element._amplitudeAC, tfSIN[1]);
                setValueFromTextField(element._frequency, tfSIN[2]);
                setValueFromTextField(element._offsetDC, tfSIN[3]);
                setValueFromTextField(element._phase, tfSIN[4]);
                break;
            case 1:
                element._typQuelle.setUserValue(ControlSourceType.QUELLE_RECHTECK);
                setValueFromTextField(element._amplitudeAC, tfREC[1]);
                setValueFromTextField(element._frequency, tfREC[2]);
                setValueFromTextField(element._offsetDC, tfREC[3]);
                setValueFromTextField(element._phase, tfREC[4]);
                setValueFromTextField(element._dutyRatio, tfREC[5]);
                break;
            case 2:
                element._typQuelle.setUserValue(ControlSourceType.QUELLE_DREIECK);
                setValueFromTextField(element._amplitudeAC, tfTRI[1]);
                setValueFromTextField(element._frequency, tfTRI[2]);    
                setValueFromTextField(element._offsetDC, tfTRI[3]);
                setValueFromTextField(element._phase, tfTRI[4]);
                setValueFromTextField(element._dutyRatio, tfTRI[5]);
                break;
            case 3:
                element._typQuelle.setUserValue(ControlSourceType.QUELLE_RANDOM);
                break;
            case 4:
                element._typQuelle.setUserValue(ControlSourceType.QUELLE_IMPORT);
                break;
            default:
                break;
        }

        element._displayDetails.setUserValue(displayDetails);                
    }

    @Override
    public void schliesseFenster() {
        if (previewWindow != null) {
            previewWindow.dispose();
        }
        super.schliesseFenster();
    }

    @Override
    JComponent[] getComponentsDisabledExternal() {
        List<JComponent> returnValue = new ArrayList<JComponent>();
        for(JComponent comp : tfSIN) {
            if(comp != null) {
                returnValue.add(comp);
            }
        }
        
        for(JComponent comp : tfREC) {
            if(comp != null) {
                returnValue.add(comp);
            }
        }
        
        for(JComponent comp : tfTRI) {
            if(comp != null) {
                returnValue.add(comp);
            }
        }
        return returnValue.toArray(new JComponent[returnValue.size()]);
    }        
}
