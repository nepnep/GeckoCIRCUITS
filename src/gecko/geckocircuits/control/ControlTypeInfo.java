/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckocircuits.control;

import gecko.geckocircuits.circuit.AbstractBlockInterface;
import gecko.geckocircuits.circuit.AbstractTypeInfo;
import gecko.geckocircuits.circuit.ConnectorType;
import gecko.geckocircuits.circuit.SpecialTyp;
import gecko.i18n.resources.I18nKeys;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ControlTypeInfo extends AbstractTypeInfo {

    public ControlTypeInfo(final Class<? extends AbstractBlockInterface> typeClass, final String idString, final I18nKeys typeDescription, final I18nKeys typeDescriptionVerbose) {
        super(typeClass, idString, typeDescription);
    }

    public ControlTypeInfo(final Class<? extends AbstractBlockInterface> typeClass, final String idString, final I18nKeys typeDescription) {
        super(typeClass, idString, typeDescription);
    }

    @Override
    public ConnectorType getSimulationDomain() {
        return ConnectorType.CONTROL;
    }

    @Override
    public String getExportImportCharacters() {
        return "c";
    }

    @Override
    public String getSaveIdentifier() {
        return "ElementCONTROL";
    }

    @Override
    public AbstractBlockInterface fabric() {
        try {
            return _typeClass.newInstance();
        } catch (Throwable ex) {
            System.err.println("error: " + _typeClass);
            ex.printStackTrace();
            Logger.getLogger(SpecialTyp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}