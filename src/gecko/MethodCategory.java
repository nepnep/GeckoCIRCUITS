package gecko;

import gecko.i18n.resources.I18nKeys;

public enum MethodCategory {
    SIMULATION_START(I18nKeys.SIM_START_CATEGORY),
    LOAD_SAVE_MODEL(I18nKeys.LOAD_SAVE_CATEGORY),
    SIGNAL_PROCESSING(I18nKeys.SIGNAL_PROCESSING),
    COMPONENT_PROPERTIES(I18nKeys.COMPONENT_PROPERTIES),
    COMPONENT_CREATION_LISTING(I18nKeys.COMPONENT_CREATION),
    ALL_CATEGORIES(I18nKeys.ALL_CATEGORIES);
    
    private I18nKeys _tranlsationKey;

    private MethodCategory(final I18nKeys translationKey) {
        _tranlsationKey = translationKey;
    }    

    @Override
    public String toString() {
        return _tranlsationKey.getTranslation();
    }
    
    
    
}
