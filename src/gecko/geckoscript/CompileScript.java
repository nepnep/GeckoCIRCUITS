/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gecko.geckoscript;

import gecko.geckocircuits.allg.Fenster;
import gecko.geckocircuits.allg.GlobalFilePathes;
import gecko.geckocircuits.control.javablock.CodeWindow;
import gecko.geckocircuits.control.javablock.CompileObject;
import gecko.geckocircuits.control.javablock.CompileStatus;
import gecko.geckocircuits.control.javablock.ReglerJavaFunction;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

/**
 *
 * @author andy
 */
public class CompileScript {

    
    static class scriptRAMJavaFileObject extends SimpleJavaFileObject {

        scriptRAMJavaFileObject(String name, Kind kind) {
            super(toURI(name), kind);
        }
        ByteArrayOutputStream baos;

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors)
                throws IOException, IllegalStateException,
                UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

        @Override
        public InputStream openInputStream() throws IOException,
                IllegalStateException, UnsupportedOperationException {
            return new ByteArrayInputStream(baos.toByteArray());
        }

        @Override
        public OutputStream openOutputStream() throws IOException,
                IllegalStateException, UnsupportedOperationException {
            return baos = new ByteArrayOutputStream();
        }
    }
    
    static void compile(final ScriptWindow sw) {
        sw._compileStatus = CompileStatus.NOT_COMPILED;
        sw._declarations = sw._declarationsTextArea.getText();
        sw._className = "GeckoCustom" + sw._nameGenerator.nextInt(100000) + "";

        sw._sourceCode = "";
        sw._compileSourceCode = "";


        //create source code from user input
        try {


            String strLine;
            sw.addSourceLine("import gecko.geckoscript.AbstractGeckoCustom;");
            sw.addSourceLine("import gecko.geckoscript.SimulationAccess;");
            sw.addSourceLine("import javax.swing.JTextArea;");
            BufferedReader reader = new BufferedReader(new StringReader(sw._importsTextArea.getText()));
            while ((strLine = reader.readLine()) != null) {
                sw.addSourceLine(strLine);
            }
            sw.addSourceLine("");
            sw.addSourceLine("/**");
            sw.addSourceLine(" * Source created on " + new Date());
            sw.addSourceLine(" */");
            sw.addSourceLine("");
            sw.addSourceLine("public class " + sw._className + " extends AbstractGeckoCustom { ");
            sw.addSourceLine("");
            reader = new BufferedReader(new StringReader(sw._declarations));
            while ((strLine = reader.readLine()) != null) {
                sw.addSourceLine("\t\t" + strLine);
            }
            if (sw._advancedOption) {
                reader = new BufferedReader(new StringReader(sw._advancedVariables));
                while ((strLine = reader.readLine()) != null) {
                    sw.addSourceLine("\t\t" + strLine);
                }
            }
            sw.addSourceLine("");
            if (sw._advancedOption) {
                sw.addSourceLine("    public " + sw._className + "(SimulationAccess simaccess, JTextArea outputArea, HashMap element_map) {");
            } else {
                sw.addSourceLine("    public " + sw._className + "(SimulationAccess simaccess, JTextArea outputArea) {");
            }
            sw.addSourceLine("\t\t     super(simaccess, outputArea);");
            if (sw._advancedOption) {
                reader = new BufferedReader(new StringReader(sw._advancedConstructor));
                while ((strLine = reader.readLine()) != null) {
                    sw.addSourceLine("\t\t     " + strLine);
                }
            }
            sw.addSourceLine("    }");
            sw.addSourceLine("");
            sw.addSourceLine("    public void runScript() {");
            sw.addSourceLine("    try {");
            sw.addSourceLine("// ****************** your code segment **********************");
            reader = new BufferedReader(new StringReader(sw._codeTextArea.getText()));
            while ((strLine = reader.readLine()) != null) {
                sw.addSourceLine("\t\t" + strLine);
            }
            sw.addSourceLine("// ****************** end of code segment **********************");
            sw.addSourceLine("    } catch(Throwable ex) { writeOutputLn(\"An error occured during script execution:\");");
            sw.addSourceLine("\t\tendScript();");
            sw.addSourceLine("\t\tthrow new RuntimeException(ex);");
            sw.addSourceLine("    }");
            sw.addSourceLine("  }");
            sw.addSourceLine("}");
            //
            //System.out.println("createSourceCode() --> \n\n_compilerMessage= \n"+_compilerMessage+"\n\n===========\n_sourceString= \n"+_sourceString+"\n\n===========\n");
        } catch (IOException ex) {
            Logger.getLogger(ScriptWindow.class.getName()).log(Level.SEVERE, null, ex);
        }


        String classFileName = sw._className + ".java";

        sw._compileSourceCode += sw._sourceCode + "\n";


        //compile the constructed source code into a new class
        try {
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

            if (compiler == null) { // this fixes the java 1.7 compilation problem
                try {
                    try {
                        compiler = (JavaCompiler) Class.forName("com.sun.tools.javac.api.JavacTool").newInstance();
                    } catch (InstantiationException ex) {
                        Logger.getLogger(ReglerJavaFunction.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(ReglerJavaFunction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ReglerJavaFunction.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (compiler == null) {
                    JOptionPane.showMessageDialog(null, "No Java compiler found. Aborting", "Error!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            final Map<String, JavaFileObject> output = new HashMap<String, JavaFileObject>();

            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();

            JavaFileManager jfm = new ForwardingJavaFileManager<StandardJavaFileManager>(compiler.getStandardFileManager(diagnostics, null, null)) {

                @Override
                public JavaFileObject getJavaFileForOutput(Location location,
                        String name,
                        Kind kind,
                        FileObject sibling)
                        throws IOException {
                    JavaFileObject jfo = new scriptRAMJavaFileObject(name, kind);
                    output.put(name, jfo);
                    return jfo;
                }
            };

            OutputStream outStream = new OutputStream() {

                public void write(byte[] b) {
                }

                public void write(byte[] b, int off, int len) {
                    sw.compilerMessages += new String(b).substring(off, len);
                }

                public void write(int b) {
                }
            };
            PrintWriter compilerWriter = new PrintWriter(outStream, true);

            if (sw._workingDirectory == null) {
                sw._workingDirectory = System.getProperty("user.dir");
            }

            ArrayList<String> options = new ArrayList<String>();
            options.add("-classpath");

            if (System.getProperty("os.name").toLowerCase().contains("linux")) {
                options.add(System.getProperty("user.dir") + ":" + sw._workingDirectory);
            } else {
                options.add(System.getProperty("user.dir") + ";" + sw._workingDirectory);
            }

            //add classpath to running program to be able to access all gecko classes from the newly created class
            options.addAll(Arrays.asList("-classpath", System.getProperty("java.class.path")));


            CompilationTask task = compiler.getTask(compilerWriter, jfm, diagnostics, options, null,
                    Arrays.asList(CompileObject.generateJavaSourceCode(sw._sourceCode, classFileName)));

            //compile the java file containing the new class:
            boolean compiledsuccessfully = task.call();
            sw.compilerMessages = "";

            if (!compiledsuccessfully) {
                for (Diagnostic dm : diagnostics.getDiagnostics()) {
                    compilerWriter.println(dm);
                }
                sw._compileStatus = CompileStatus.COMPILE_ERROR;
                sw.compilerMessages += "Compile status: ERROR";
                sw.compilerMessages = CodeWindow.checkForOldCompiler(sw.compilerMessages);

            } else {
                sw._compileStatus = CompileStatus.COMPILED_SUCCESSFULL;
                sw.compilerMessages += "\n \tCOMPILATION FINISHED SUCESSFULLY!";

                //now load the class, and obtain a pointer to its constructor
                ClassLoader loader = new ClassLoader() {

                    public void extendClassPath() {
                        if (!Fenster.IS_APPLET) {
                            URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
                            Class<URLClassLoader> sysclass = URLClassLoader.class;

                            File file = new File(GlobalFilePathes.DATNAM);
                            File directory = file.getParentFile();
                            if (directory.isDirectory()) {
                                try {
                                    String path = directory.getAbsolutePath();
                                    URL url = new URL("file://" + path + "/");
                                    Method method = sysclass.getDeclaredMethod("addURL", URL.class);
                                    method.setAccessible(true);
                                    method.invoke(sysloader, new Object[]{url});
                                } catch (IllegalAccessException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (IllegalArgumentException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (InvocationTargetException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (NoSuchMethodException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (SecurityException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                } catch (MalformedURLException ex) {
                                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    }

                    @Override
                    protected Class<?> findClass(String name) throws
                            ClassNotFoundException {

                        extendClassPath();

                        JavaFileObject jfo = output.get(name);
                        if (jfo != null) {
                            byte[] bytes = ((scriptRAMJavaFileObject) jfo).baos.toByteArray();
                            return defineClass(name, bytes, 0, bytes.length);
                        }

                        Class<?> cl = super.findClass(name);
                        return cl;
                    }
                };

                Class<?> c = Class.forName(sw._className, false, loader);
                Class customClass = c;

                try {

                    Constructor constructorlist[] = customClass.getConstructors();
                    sw._classConstructor = constructorlist[0];
                    if (sw._advancedOption) {
                        sw._scriptObject = (AbstractGeckoCustom) sw._classConstructor.newInstance(new Object[]{sw._circuit, 
                            sw.jTextAreaOutput, sw._advancedObjects});
                    } else {
                        sw._scriptObject = (AbstractGeckoCustom) sw._classConstructor.newInstance(new Object[]{sw._circuit, 
                            sw.jTextAreaOutput});
                    }

                } catch (InstantiationException ex) {
                    Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
                }


            }

        } catch (IllegalArgumentException ex) {
            Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CompileScript.class.getName()).log(Level.SEVERE, null, ex);
        }

        sw._compMessagesTextArea.setText(sw.compilerMessages);
        sw._sourceCodeCompilerTextArea.setText(sw._compileSourceCode);
    }
    
    
    private static URI toURI(String name) {
        try {
            return new URI(name);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    
}
