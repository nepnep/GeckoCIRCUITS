package gecko;

import java.lang.reflect.Method;

/**
 * pure utility class - no constructor. This utility function checks wheter the
 * methods of "checkMethods" are contained with the identical method signature
 * inside "containsMethodSignature".
 *
 * @author andy
 */
final class MethodNameChecker {

    private MethodNameChecker() {
        super();
    }

    static MethodNameChecker checkFabric(final Class checkMethods,
            final Class<GeckoRemoteInterface> containsMethodSignature) {
        try {
            assert false; // immediately return when assertions are turned off
            // we don't want to spend time in this check when users open GeckoCIRCUITS.
            return null;
        } catch (AssertionError err) { // we go here, when the JVM-flag "-ea" is set!
            for (Method toTest : checkMethods.getMethods()) {
                try {
                    assert containsMethodSignature.getMethod(toTest.getName(), toTest.getParameterTypes()) != null;                    
                } catch (Throwable ex) {
                    assert false : "Method in geckoRemoteInterface not found: " + toTest;
                }
            }
        }
        return null;
    }
}
