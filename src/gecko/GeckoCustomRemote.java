/*  This file is part of GeckoCIRCUITS. Copyright (C) ETH Zurich, Gecko-Simulations AG
 *
 *  GeckoCIRCUITS is free software: you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any later version.
 *
 *  GeckoCIRCUITS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  GeckoCIRCUITS.  If not, see <http://www.gnu.org/licenses/>.
 */
package gecko;

import gecko.geckoscript.AbstractGeckoCustom;
import gecko.geckoscript.SimulationAccess;
import gecko.i18n.resources.I18nKeys;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is an implementation of GeckoCustom, that is to be used for remote
 * method invocation with GeckoRemote.
 *
 * @author anstupar
 */
public final class GeckoCustomRemote extends AbstractGeckoCustom implements GeckoRemoteInterface, CallbackServerInterface {

    private boolean _free = true; //denotes if this instance of GeckoCIRCUITS is free for a remote connection
    private long _sessionID = 0;
    public static CallbackClientInterface client;

    public GeckoCustomRemote(final SimulationAccess access) {
        super(access, null);
    }

    public static void printErrorLn(final String message) {
        if (client != null) {
            try {
                client.printErrorMessage(message);
            } catch (RemoteException ex) {
                Logger.getLogger(GeckoCustomRemote.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void printLn(final String message) {
        if (client != null) {
            try {
                client.printSystemMessage(message);
            } catch (RemoteException ex) {
                Logger.getLogger(GeckoCustomRemote.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void runScript() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isFree() {
        return _free;
    }

    @Override
    public long getSessionID() {
        return _sessionID;
    }

    @Override
    //here we generate a session ID and return it to the sender
    public long connect() {
        final long connectionID = System.currentTimeMillis();
        _free = false;
        _sessionID = connectionID;
        return connectionID;
    }

    @Override
    public void disconnect(final long remoteSessionID) {
        
        if (_sessionID == remoteSessionID) {
            try {
                if(client != null) {
                    client.printSystemMessage("GeckoREMOTE session closed.");                
                }                
            } catch (RemoteException ex) {
                Logger.getLogger(GeckoCustomRemote.class.getName()).log(Level.SEVERE, null, ex);
            }
            _sessionID = 0;                        
            _free = true;            
            client = null;
        }
    }

    @Override
    public void registerForCallback(
            final CallbackClientInterface callbackClientObject)
            throws java.rmi.RemoteException {
        client = callbackClientObject;
    }

    public static String pingRemoteClient() {
        if(client == null) {
            return null;
        }
        String pong = null;
        try {
            pong = client.ping();
        } catch (RemoteException ex) {
            System.err.println(I18nKeys.CONNECTION_TEST_FAILED.getTranslation());
        }
        return pong;
    }

    public static String getClientInfo() {
        if (client == null) {
            return "\n  " + I18nKeys.NO_CONNECTION_ESTABLISHED.getTranslation();
        } else {
            final StringBuilder returnValue = new StringBuilder();
            String pong = pingRemoteClient();
            if (pong == null) {
                returnValue.append(I18nKeys.ERROR_CLIENT_IS_REGISTERED_BUT_CANNOT_BE_CONTACTED.getTranslation());
                returnValue.append(client.toString());
            } else {
                returnValue.append(I18nKeys.STATUS_OF_CONNECTION_OK.getTranslation() + "\n");
                returnValue.append(pong);
            }
            return returnValue.toString();
        }
    }
}
